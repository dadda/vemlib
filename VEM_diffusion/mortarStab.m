function K = mortarStab( edgeLengths, k, weights1d, D )
% - D   : matrix of the derivatives of the interpolating spectral basis
%         functions \hat\psi_i on [0,1]. D(i,j) = \hat\psi_i(\hat x_j).

Nver   = length(edgeLengths);
preMap = [Nver 1:Nver-1];

invEdgeLengths = 1./edgeLengths;

dim = k*Nver; % Dimension of the boundary VEM space

%
% Stiffness and mass matrices
%
R = zeros( dim );
M = zeros( dim );

DD = D' * bsxfun(@times, weights1d, D);

iIdx = 1:Nver;

jIdx = 1:Nver;
R( iIdx + (jIdx-1)*dim ) = ( DD(1) * invEdgeLengths ) ...
                           + ( DD(end) * invEdgeLengths(preMap) );
M( iIdx + (jIdx-1)*dim ) = weights1d(1) * ( edgeLengths + edgeLengths(preMap) );

jIdx = [2:Nver 1];
R( iIdx + (jIdx-1)*dim ) = DD(1,end) * invEdgeLengths;
R( jIdx + (iIdx-1)*dim ) = R( iIdx + (jIdx-1)*dim );

jIdx = [Nver 1:Nver-1];
R( iIdx + (jIdx-1)*dim ) = DD(1,end) * invEdgeLengths(preMap); % DD = DD'
R( jIdx + (iIdx-1)*dim ) = R( iIdx + (jIdx-1)*dim );

for e = 1:Nver
    iIdx = e * ones(1,k-1);
    
    jIdx = Nver + (k-1)*(e-1) + (1:k-1);
    R( iIdx + (jIdx-1)*dim ) = invEdgeLengths(e) * DD(1,2:end-1);
    R( jIdx + (iIdx-1)*dim ) = R( iIdx + (jIdx-1)*dim );

    jIdx = Nver + (k-1)*(preMap(e)-1) + (1:k-1);
    R( iIdx + (jIdx-1)*dim ) = invEdgeLengths(preMap(e)) * DD(end,2:end-1);
    R( jIdx + (iIdx-1)*dim ) = R( iIdx + (jIdx-1)*dim );
    
    iIdxDD = kron( 2:k, ones(1,k-1) );
    jIdxDD = kron( ones(1,k-1), 2:k );
    iIdx = Nver + (k-1)*(e-1) + iIdxDD - 1;
    jIdx = Nver + (k-1)*(e-1) + jIdxDD - 1;
    R( iIdx + (jIdx-1)*dim ) = invEdgeLengths(e) * DD( iIdxDD + (jIdxDD-1)*(k+1) );
    
    iIdx = Nver + (k-1)*(e-1) + (1:k-1);
    M( iIdx + (iIdx-1)*dim ) = edgeLengths(e) * weights1d(2:k);
end

%
% Compute mortar stabilization matrix
%
sqrtM = sqrtm(M);
K = sqrtM * sqrtm( sqrtM \ (R / sqrtM) ) * sqrtM;

end
