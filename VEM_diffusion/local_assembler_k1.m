function [ S_E, row_dofs_for_S_E, col_dofs_for_S_E, rhs_E, dofs_for_rhs_E, P0s_by_ele_E, PNs ] = ...
    local_assembler_k1(elements_E, h_E, Nx_E, Ny_E, wqd_E, basis_on_qd_E, basis_on_bnd_E, f_E, varargin)
% Computation of the local stiffness matrix and right hand side for k = 1.
%    [ S_E, ROW_DOFS_FOR_S_E, COL_DOFS_FOR_S_E, RHS_E, DOFS_FOR_RHS_E, P0_BY_ELE_E ] = ...
%      LOCAL_ASSEMBLER_K1(ELEMENTS_E, H_E, NX_E, NY_E, WQD_E, BASIS_ON_QD_E, BASIS_ON_BND_E, F_E)
%    computes local stiffness matrix, rhs, and L^2 projection of VEM basis
%    functions for a VEM space of degree k = 1. Hitchhicker's monomial
%    basis functions are used.
%    [ S_E, ROW_DOFS_FOR_S_E, COL_DOFS_FOR_S_E, RHS_E, DOFS_FOR_RHS_E, P0_BY_ELE_E ] = ...
%      LOCAL_ASSEMBLER_K1(ELEMENTS_E, H_E, NX_E, NY_E, WQD_E, BASIS_ON_QD_E, BASIS_ON_BND_E, F_E, BASISL2NORMS_E)
%    computes local stiffness matrix, rhs, and L^2 projection of VEM basis
%    functions for a VEM space of degree k = 1. Scaled Hitchhicker's monomial
%    basis functions are used.
%
%    Inputs:
%
%   - ELEMENTS_E : a [1 x Nver_max+1] vector containing, in order: the number of
%       vertices of element E, the indices of its vertices in counterclockwise
%       order, followed by as many zeros as needed to have a vector of
%       length Nver_max+1, where Nver_max is the maximum number of vertices
%       for a polygon in the mesh.
%
%   - H_E : diameter of element E.
%
%   - NX_E, NY_E : [Nver_loc+1 x 1] arrays with the x and y components,
%       respectively, of the outward normal vectors for element E,
%       normalized so that the euclidean nor of each normal equals the
%       length of the corresponding edge. NX_E(1), NY_E(1) contain the
%       normal to the last edge of E, whereas NX_E(2:end), NY_E(2:end)
%       contain normal vectors from the first edge to the last one.
%       Nver_loc is the number of vertices (or edges) of element E.
%
%   - WQD_E : [Nqd_loc x 1] array with internal quadrature weights (WQD_E)
%       on polygon E.
%
%   - BASIS_ON_QD_E : [Nqd_loc x d2] matrix containing evaluation of
%       monomial basis functions on internal quadrature nodes for each
%       element, with d2 = dim(P^k(E)).
%       
%   - BASIS_ON_BND_E : [Nver_loc x d2] matrix containing evaluation of
%       monomial basis functions at the vertices of element E.
%
%   - F_E : [Nqd_loc x 1] array with evaluation of the loading function on
%       the internal quadrature nodes of element E.
%
%   - BASISL2NORMS_E : [1 x d2] array containing the L^2
%       norms of the original monomial basis functions on the element E.
%
%   Outputs:
%
%   - S_E : [Ndofs_loc^2 x 1] vector containing the local stiffness matrix.
%         Ndofs_loc = Nver_loc is the # of dofs of the local VEM space for
%         k = 1.
%
%   - ROW_DOFS_FOR_S_E, COL_DOFS_FOR_S_E : [Ndofs_loc^2 x 1] vectors that
%         contain row and column degrees of freedom corresponding to the local
%         stiffness matrix.
%
%   - RHS_E : [Ndofs_loc x 1] vector containing the local right hand side.
%
%   - DOFS_FOR_RHS_E : [Ndofs_loc x 1] vector that contains the degrees of
%         freedom corresponding to the local right hand side.
%
%   - P0S_BY_ELE_E : [d2 x Ndofs_loc] matrix containing
%         the L^2 projection of the local VEM basis functions in terms of
%         the local monomial basis.
%

Nver_loc = elements_E(1);

D = basis_on_bnd_E;

% We use equation (3.29) to build matrix B

B = zeros(3, Nver_loc);
B(1,:) = 1./Nver_loc;

if ~isempty(varargin)
    % Use scaled monomial basis functions (varargin{1} = basisL2norms_E)
    B(2,:) = (Nx_E(1:Nver_loc) + Nx_E(2:Nver_loc+1))'/(2.*h_E * varargin{1}(2));
    B(3,:) = (Ny_E(1:Nver_loc) + Ny_E(2:Nver_loc+1))'/(2.*h_E * varargin{1}(3));
else
    B(2,:) = (Nx_E(1:Nver_loc) + Nx_E(2:Nver_loc+1))'/(2.*h_E);
    B(3,:) = (Ny_E(1:Nver_loc) + Ny_E(2:Nver_loc+1))'/(2.*h_E);    
end

G   = B*D;
G(2:end,2:end) = (G(2:end,2:end) + G(2:end,2:end)') / 2;
G(2:end,1) = 0;
PNs = G\B;
PN  = D*PNs;

Gt      = G;
Gt(1,:) = 0.;

S_E = PNs'*Gt*PNs + (eye(Nver_loc) - PN)'*(eye(Nver_loc) - PN);
S_E = (S_E + S_E')/2;
S_E = S_E(:);

% For k = 1, the L^2 projector coincides with the Pi-Nabla projector
P0s_by_ele_E = PNs;

rhs_E          = PNs'*(basis_on_qd_E'*(wqd_E.*f_E));
dofs_for_rhs_E = elements_E(2:Nver_loc+1)';

tmp = repmat(dofs_for_rhs_E, 1, Nver_loc);
row_dofs_for_S_E = tmp(:);
tmp = tmp';
col_dofs_for_S_E = tmp(:);

end
