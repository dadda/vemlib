function sol = compute_VEM_sol_parallel( exactsol, k, mesh, ...
    verDofs, edgeInternalDofs, edgePerm, eleInternalDofs, dofsLayout, ...
    areas, xqd, yqd, wqd, basis_on_qd, formula1d, use_mp, varargin )
% SOL = COMPUTE_VEM_SOL_PARALLEL( EXACTSOL, K, MESH, XQD, YQD, WQD, BASIS_ON_QD, FORMULA1D, USE_MP )
% projects the solution on the global VEM space of degree k. Internal
% degrees of freedom of the projected solution are computed in parallel.
% SOL = COMPUTE_VEM_SOL_PARALLEL( K, MESH, XQD, YQD, WQD, BASIS_ON_QD, FORMULA1D, USE_MP, BASISL2NORM )
% projects the solution on the global VEM space of degree k. Internal
% degrees of freedom of the projected solution are computed in parallel.
% Hitchhicker's basis functions scaled by their L^2 norm are used.
%
% Inputs:
%
% - EXACTSOL: function pointer for evaluating the exact solution.
%
% - K : degree of the VEM space.
%
% - MESH : extended mesh data structure (see loadmesh.m for a description
%       of this data structure).
%
% - AREAS : [1 x Nelt] array with the elements areas.
%
% - XQD, YQD, WQD : [Nelt x 1] cells of [Nqd_loc x 1] (not Nqd_max !!)
%       column arrays containing quadrature nodes and weights of cubature
%       rules on each polygon, where Nelt is the number of elements in the
%       mesh and Nqd_loc is the local number of internal quadrature points for a
%       polygon.
%
% - BASIS_ON_QD : [Nelt x 1] cell of matrices containing evaluation of
%       monomial basis functions on internal quadrature nodes for each
%       element obtained by rescaling the original functions proposed in
%       the Hitchhicker's guide by their L^2 norm.
%
% - FORMULA1D : barycentric coordinates (FORMULA1D(:,1), FORMULA1D(:,2))
%       and weights (FORMULA1D(:,3)) of the Gauss-Lobatto
%       quadrature rule of order K+1.
%
% - USE_MP : flag telling whether the Multiprecision Toolbox
%       (www.advanpix.com) should be used (USE_MP=1) or not
%       (USE_MP=0).
%
% - BASISL2NORM: [Nelt x d2] array, where the E-th row contains the L^2
%       norms of the original monomial basis functions on the element E.
%
% Degrees of freedom of a function v_h in the global virtual element space of degree k
% are organized as follows:
% * the value of v_h at all the vertices of the mesh;
% * the value of v_h at the k-1 internal points of the (k+1)-point
%   Gauss-Lobatto quadrature rule on the first edge, the second edge,
%   and so on.
% * the moments up to order k-2 of v_h in the first element, the second
%   element, and so on.
%

d2m2 = (k*(k-1))/2;

if k == 1
    sol = exactsol(mesh.coordinates(:,1), mesh.coordinates(:,2), use_mp);
else
    if dofsLayout == 1
        x = mesh.coordinates(:,1); x = x(mesh.edges(:,1:2)');
        y = mesh.coordinates(:,2); y = y(mesh.edges(:,1:2)');
    else
        x = zeros(2, mesh.Nfc);
        y = zeros(2, mesh.Nfc);
        for i = 1:mesh.Nfc
            if edgePerm(i) == 1
                x(:,i) = mesh.coordinates(mesh.edges(i,[1 2]),1);
                y(:,i) = mesh.coordinates(mesh.edges(i,[1 2]),2);
            else
                x(:,i) = mesh.coordinates(mesh.edges(i,[2 1]),1);
                y(:,i) = mesh.coordinates(mesh.edges(i,[2 1]),2);
            end
        end
    end

    x = formula1d(2:end-1,1:2)*x;
    x = [ mesh.coordinates(:,1); x(:) ];
    
    y = formula1d(2:end-1,1:2)*y;
    y = [ mesh.coordinates(:,2); y(:) ];

    sol = exactsol(x, y, use_mp);

    internal_sol = cell(mesh.Nelt,1);

    if ~isempty(varargin)
        % Use scaled monomial basis functions (varargin{1} = basisL2norms_E)
        tmp2 = varargin{1};
        parfor ( E = 1:mesh.Nelt, getParforArg() )
            tmp = basis_on_qd{E};
            internal_sol{E} = (tmp2(E,1:d2m2)'.* (tmp(:,1:d2m2)'*(wqd{E} .* exactsol(xqd{E}, yqd{E}, use_mp))))/areas(E);
        end
    else
        parfor ( E = 1:mesh.Nelt, getParforArg() )
            tmp = basis_on_qd{E};
            internal_sol{E} = (tmp(:,1:d2m2)'*(wqd{E} .* exactsol(xqd{E}, yqd{E}, use_mp)))/areas(E);
        end
    end
        
    sol = [sol;
           vertcat(internal_sol{:})];

    if dofsLayout ~= 1
        tmp = edgeInternalDofs';
        tmp = [ verDofs; tmp(:); horzcat(eleInternalDofs{:})' ];
        sol(tmp) = sol;
    end
end

end
