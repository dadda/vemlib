function errinf = compute_errinf( exactsol, U, coordinates, verDofs, use_mp )

sol    = exactsol( coordinates(:,1), coordinates(:,2), use_mp );
errinf = max( abs( U(verDofs) - sol ) ) / max( abs( sol ) );

end
