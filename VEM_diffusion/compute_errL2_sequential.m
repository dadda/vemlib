function errL2 = compute_errL2_sequential( U, k, Nelt, Nver, Nfc, elements, edgebyele, perm, ...
                                xqd, yqd, wqd, basis_on_qd, P0s_by_ele, use_mp )
% L^2 norm of the approximation error.
%    ERRL2 = COMPUTE_ERRL2_SEQUENTIAL( U, K, NELT, NVER, NFC, ELEMENTS, EDGEBYELE, PERM, ...
%                                XQD, YQD, WQD, BASIS_ON_QD, P0S_BY_ELE, USE_MP )
%    computes the L^2 norm of the approximation error (u - u_h), where u is
%    the exact solution and u_h is the corresponding VEM approximation.
%    This function uses the L^2-projection of the VEM basis functions in
%    terms of the monomial basis. The norm of the error is computed with a
%    sequential loop over all the elements.
%
%    Inputs:
%
%    - U : column vector with the degrees of freedom of the VEM approximation
%          to the exact solution.
%
%    - K : degree of the VEM space.
%
%    - NELT : total number of elements (polygons) of the mesh.
%
%    - NVER : total number of vertices of the mesh.
%
%    - NFC : total number of edges of the mesh.
%
%    - ELEMENTS : a NELT x (NVER_MAX+1) matrix. For any E = 1, ..., NELT,
%          ELEMENTS(E,:) contains, in order: the number of vertices of
%          element E, the indices of the vertices of E in counterclockwise
%          order, followed by as many zeros as needed to accomodate the
%          presence of elements with higher number of vertices in the mesh.
%          NVER_MAX is the maximum number of vertices per polygon in the
%          mesh.
%
%    - EDGEBYELE: a NELT x NVER_MAX matrix, whose E-th row
%          contains the number of edges that make up \partial E, followed by as
%          many zeros as needed to accomodate the presence of elements with
%          higher number of vertices in the mesh.
%
%    - PERM: a NELT x NVER_MAX matrix, whose E-th row indicates
%          what permutations are needed (1 (no permutation) or 2 (switch the
%          order)) for the edges of element E to match their global orientation
%          provided by the field EDGES in the extended mesh data structure.
%
%   - XQD, YQD, WQD : [Nqd_max x Nelt] matrices representing
%          quadrature nodes (XQD, YQD), weights (WQD)
%          of the quadrature rules on each polygon of the mesh.
%          Nqd_max is the maximum number of internal cubature nodes over
%          all the polygons of the mesh.
%          Since mesh polygons could have different numbers of vertices from each other,
%          numbers of cubature nodes could differ. Hence, for
%          each column E = 1, ..., Nelt, only the following entries are
%          meaningful:
%          * XQD([1:Nqd_loc], E)
%          * similarly for the other matrices.
%
%   - BASIS_ON_QD : [Nqd_max x Nelt x d2] matrix containing evaluation of
%          basis functions on internal cubature nodes for each element. For
%          each E = 1, ..., Nelt, entries BASIS_ON_QD(Nqd_loc+1:Nqd_max, E, :)
%          are meaningless.
%
%   - P0S_BY_ELE : column vector containing the L^2 projections of the
%          basis functions, P0s, for each element of the mesh, one after the
%          other.
%
%   - USE_MP : flag telling whether the Multiprecision Toolbox
%          (www.advanpix.com) should be used (USE_MP=1) or not (USE_MP=0).
%
%   Degrees of freedom of a function v_h in the virtual element space of degree k on
%   the whole domain are organized as follows:
%   * the value of v_h at all the vertices of the mesh;
%   * the value of v_h at the k-1 internal points of the (k+1)-point
%     Gauss-Lobatto quadrature rule on the first edge, the second edge,
%     and so on.
%   * the moments up to order k-2 of v_h in the first element, the second
%     element, and so on.
%

d2          = ((k+1)*(k+2))/2;
d2m2        = ((k-1)*k)/2;
sol         = exactsol(xqd, yqd, use_mp);
basis_on_qd = permute(basis_on_qd, [1 3 2]);
errL2       = 0.;

cumdofs_for_P0s_by_ele = 0;    
for E = 1:Nelt
    if k == 1
        local_dofs = elements(E,2:elements(E,1)+1)';
    elseif k == 2
        local_dofs  = [elements(E,2:elements(E,1)+1)';
                       Nver + edgebyele(E,1:elements(E,1))';
                       Nver + Nfc + E];
    else
        face_dofs = bsxfun(@plus, (1:k-1)', ...
            Nver + (edgebyele(E,1:elements(E,1))-1)*(k-1));

        face_dofs(:, perm(E,1:elements(E,1)) == 2) = ...
            flipud(face_dofs(:, perm(E,1:elements(E,1)) == 2));

        local_dofs = ...
            [elements(E,2:elements(E,1)+1)';
             ...
             face_dofs(:);
             ...
             Nver + (k-1)*Nfc + (E-1)*d2m2 + (1:d2m2)'];
    end

    Uloc = U(local_dofs);

    nb_local_dofs = length(local_dofs);
    size_local_P0s = d2 * nb_local_dofs;

    errL2 = errL2  + wqd(:,E)' * (sol(:,E) - basis_on_qd(:,:,E) * ...
                    (reshape(P0s_by_ele(cumdofs_for_P0s_by_ele+1:cumdofs_for_P0s_by_ele+size_local_P0s), ...
                         [d2 nb_local_dofs]) * Uloc)).^2;

    cumdofs_for_P0s_by_ele = cumdofs_for_P0s_by_ele + size_local_P0s;
end
errL2 = sqrt(errL2);

end
