function [U, errS, errinf, errL2, mesh] = VEM_d_sequential(mesh, k, eoa, use_mp)
% VEM_D_SEQUENTIAL  Virtual Element Method for the Poisson equation in 2D
%                 (sequential version)
%
%    [U, ERRS, ERRINF, ERRL2, MESH] = VEM_D_SEQUENTIAL(MESH, K, EOA, USE_MP)
%    solves the Poisson equation in 2D with the Virtual Element Method.
%
%    Inputs:
%
%   - MESH : extended mesh data structure (see loadmesh.m for a description
%        of this data structure).
%
%   - K : degree of the VEM space.
%
%   - EOA : extra order of accuracy for 1D and 2D quadrature formulas.
%
%   - USE_MP : flag telling whether the Multiprecision Toolbox
%        (www.advanpix.com) should be used (USE_MP=1) or not
%        (USE_MP=0).
%
%   The load is given as a function "load" from the domain into Reals^2;
%   non-hom dirichlet BCs are given through function "dirdata".
%
%   Outputs:
%
%   - U : column vector with the degrees of freedom of the VEM approximation
%        to the exact solution.
%
%   - errS, errinf, errL2 : energy norm, inf norm, and L^2 norm of the
%        approximation error.
%
%   - MESH : extended mesh structure with four new additional fields:
%        * HE     : [1 x Nelt] array with the elements diameters.
%        * AREAS  : [1 x Nelt] array with the elements areas.
%        * XB, YB : [1 x Nelt] arrays with the coordinates of the elements
%                   centroids.
%       Nelt is the number of elements of the mesh.
%

d2m2                        = ((k-1)*k)/2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build coordinate matrices and compute some geometric quantities

[ mesh, xbnd, ybnd, Xi_bnd, ...
    xqd, yqd, wqd, ...
    Nx, Ny, formula1d ] = build_coordinate_matrices_sequential( mesh, k, eoa, use_mp );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute local matrices and assemble global system

% Assemble local matrices and right hand sides
[K, row_dofs_for_K, col_dofs_for_K, rhs, dofs_for_rhs, P0s_by_ele, basis_on_qd] = ...
    assemble_local_matrices_and_rhs_sequential(k, mesh, ...
                            xbnd, ybnd, Xi_bnd, ...
                            xqd, yqd, wqd, ...
                            Nx, Ny, ...
                            formula1d, use_mp);

% Assemble global stiffness matrix
S = sparse(row_dofs_for_K, col_dofs_for_K, K);

% Assemble global right hand side
F = sparse(dofs_for_rhs, ones([length(dofs_for_rhs) 1]), rhs);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute Dirichlet boundary conditions

[Udir, diridofs] = compute_dirichlet(mesh, k, formula1d, use_mp);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Solve the linear system

disp('Solving the linear system ... ');

N = mesh.Nver + (k-1)*mesh.Nfc + d2m2*mesh.Nelt;
U = zeros([N 1]);
dofs = setdiff(1:double(N), diridofs);

% Resolution
S11 = S(dofs, dofs);
S12 = S(dofs, diridofs);
U(dofs) = S11\(F(dofs) - S12*Udir(:));

U(diridofs) = Udir;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Error computation

if nargout > 1
    sol = compute_VEM_sol_sequential( k, mesh, xqd, yqd, wqd, basis_on_qd, formula1d, use_mp );

    %-----------------------------------
    % ERROR in discrete energy norm
    %
    % To avoid indexing S, which would cause some overhead since S is a
    % sparse matrix, we use the same partitioning used above to solve the
    % linear system.
    %-----------------------------------

    tmp = U-sol;
    errS = sqrt( abs( [tmp(dofs); tmp(diridofs)]'*[S11*tmp(dofs)+S12*tmp(diridofs); S12'*tmp(dofs)+tmp(diridofs)] ) ...
        / abs( [sol(dofs); sol(diridofs)]'*[S11*sol(dofs)+S12*sol(diridofs); S12'*sol(dofs)+sol(diridofs)] ) );

    %---------------------------------
    %  ERROR IN L-INFINITY
    %---------------------------------

    errinf = max(abs(tmp))/max(abs(sol));
    
    %---------------------------------
    %  ERROR IN L2
    %---------------------------------

    errL2 = compute_errL2_sequential( U, k, mesh.Nelt, mesh.Nver, mesh.Nfc, ...
                                      mesh.elements, mesh.edgebyele, mesh.perm, ...
                                      xqd, yqd, wqd, basis_on_qd, P0s_by_ele, use_mp );
end





