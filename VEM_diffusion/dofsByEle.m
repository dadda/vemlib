function [ verDofs, edgeInternalDofs, edgePerm, eleDofs, eleInternalDofs ] = dofsByEle( mesh, k )

Ned  = mesh.Nfc;
d2m2 = k * (k-1) / 2;

verDofs = zeros(mesh.Nver, 1);

edgeInternalDofs = zeros(Ned, k-1);
edgePerm         = zeros(Ned, 1);

eleDofs         = cell(mesh.Nelt, 1);
eleInternalDofs = cell(mesh.Nelt, 1);

%
% Run metis to determine the order in which elements are processed
%

nIntEdges = mesh.Nfc-length(mesh.dirfaces);
fid = fopen('tmp.metis', 'w');
fprintf(fid, '%d %d\n', mesh.Nelt, nIntEdges);
for K = 1:mesh.Nelt
    Nver_loc = mesh.elements(K, 1);
    for i = 1:Nver_loc
        eIdx = mesh.edgebyele(K,i);
        if mesh.elebyedge( eIdx, 2 ) ~= 0
            if mesh.elebyedge( eIdx, 1 ) == K
                fprintf(fid, '   %d', mesh.elebyedge( eIdx, 2 ));
            else
                fprintf(fid, '   %d', mesh.elebyedge( eIdx, 1 ));
            end
        end
    end
    fprintf(fid, '\n');
end
fclose(fid);

eval(sprintf(['!/home/daniele/Documents/codes/metis-5.1.0/build/Linux-x86_64/programs/gpmetis ' ...
              '-ptype=rb tmp.metis %i'], ...
             mesh.Nelt));
!rm tmp.metis

eleIndices = zeros(mesh.Nelt,1);
fid = fopen(['tmp.metis.part.' num2str(mesh.Nelt)]);
for K = 1:mesh.Nelt
    eleIndices(K) = fscanf(fid, '%i', 1) + 1;
end
fclose(fid);
eval(sprintf('!rm tmp.metis.part.%i', mesh.Nelt));

[~, sortedElements] = sort(eleIndices);

% figure()
% axis equal; hold on;
% 
% for K = sortedElements'
%     NedLoc = mesh.elements(K, 1);
%     myBoundary = mesh.coordinates(mesh.elements(K,2:NedLoc+1), :);
%     patch(myBoundary(:,1), myBoundary(:,2), 'b');
%     pause;
% end

counter = 0;

for K = sortedElements'
    Nver_loc       = mesh.elements(K, 1);
    numLocalDofs = k*Nver_loc + d2m2;
    localDofs    = zeros(1, numLocalDofs);
    
    for e = 1:Nver_loc
        % Boundary vertex dof
        vIdx1 = mesh.elements(K, 1+e);
        if verDofs(vIdx1) == 0
            counter = counter + 1;
            verDofs(vIdx1) = counter;
        end
        localDofs(e) = verDofs(vIdx1);
        
        if k > 1
            % Boundary internal edge dofs
            vIdx2 = mesh.elements(K, mod(e,Nver_loc)+2);
            if vIdx1 < vIdx2
                sortedEdge  = [vIdx1 vIdx2];
                orientation = 1;
            else
                sortedEdge  = [vIdx2 vIdx1];
                orientation = -1;
            end
            eIdx = find( ( mesh.edges(:,1) == sortedEdge(1) ) & ( mesh.edges(:,2) == sortedEdge(2) ) );
            flag = edgePerm(eIdx);
            if flag ~= 0
                % The current edge has already been processed
                localDofs(Nver_loc + (e-1)*(k-1) + (1:k-1)) = edgeInternalDofs(eIdx, end:-1:1);
            else
                edgeInternalDofs(eIdx, :) = counter + (1:k-1);
                localDofs(Nver_loc + (e-1)*(k-1) + (1:k-1)) = edgeInternalDofs(eIdx, :);
                counter = counter + k - 1;
                edgePerm(eIdx) = orientation;
            end
        end
    end

    if k > 1
        % Internal dofs
        localDofs(k*Nver_loc+1:end) = counter + (1:d2m2);
        eleInternalDofs{K} = localDofs(k*Nver_loc+1:end);
        counter = counter + d2m2;
    end

    eleDofs{K} = localDofs;
end

end
