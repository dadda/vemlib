function [Sc,Ss]=GlobS(mesh)
% ------------
% builds global stiffness matrix using the local ones. 
% Diffusion problem 
% Takes mesh (struct) as input, see meshstruct.m file
% ------------

mm = size(mesh.P,1);  % number of elements

Sc = sparse(size(mesh.V,1),size(mesh.V,1));  % number of dofs 
Ss = sparse(size(mesh.V,1),size(mesh.V,1)); 

for k=1:mm          % cycle on elements
    m = mesh.P(k,1);     % number of vertexes of element 
    X = mesh.V(mesh.P(k,2:m+1),1:2);     % build X with vertex coordinates
    [Slc,Sls]=LocS(X);            % build local stiffness matrix    
    vertvec = mesh.P(k,2:m+1);      % vector of vertex index numbers    
    Sc(vertvec,vertvec) = Sc(vertvec,vertvec) + Slc ; 
    Ss(vertvec,vertvec) = Ss(vertvec,vertvec) + Sls ;    
end 

