function [out]=loadcal(v);
% loading function
x=v(1);
y=v(2);
out = sin(x*pi)*sin(y*pi);