function [out]=solcal(v);
% exact solution function
x=v(1);
y=v(2);
out = sin(x*pi)*sin(y*pi)/(2*pi^2); 
