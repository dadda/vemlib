function [uu,errene,errinf]=VEM_d(mesh);
% function [uu]=VEM(mesh);
% ------------
% solves the diffusion problem with nodal VEM (GENERAL MESHES)
% mesh has data for mesh, as described in meshstruct.m file
% the load is given as a function "load" from the domain into Reals^2
% the non-hom dirichlet BCs are given through function "dirdata"
% ------------  
% USES DIRICHLET BOUNDARY CONDITIONS (NON HOMOGENEOUS)
% ------------
addpath(['./../../mesh_handlers']);
addpath(['./../../quadrature']);

[Sc,Ss]=GlobS(mesh); % stiffness matrix  
S=Sc+Ss;    % consistent and stability parts

ss = size(mesh.V,1);   % number of nodes   
mm = size(mesh.P,1);   % number of elements
%
% --- load term construction ---
f=zeros(ss,1);
for k=1:mm          % cicle on elements
    m = mesh.P(k,1);     % number of vertexes of element 
    X = mesh.V(mesh.P(k,2:m+1),1:2);     % build X with vertex coordinates    
    [E,BB,w]=baric(X);  % calculate area E, baricenter BB, weight vector w 
    % (w vector of weights associated to each ordered vertex 1,2,3....)        
    % ---- build global load vector --------
    for j=1:size(X,1)
        load = loadcal(mesh.V(mesh.P(k,j+1),1:2));   
        f(mesh.P(k,j+1)) = ...
        f(mesh.P(k,j+1)) + w(j)*load;  
    end           
    % ---------------    
end
% --- option for Dirichlet boundary conditions ---
[E]=mesh.E;
[V]=mesh.B;
% meshplot(E,P)
dim = max(size(S));   % here below I modify the stiffness matrix for
for i=1:length(V)      
    S(V(i),:) = [sparse(1,V(i)-1),1,sparse(1,dim-V(i))];   
    f(V(i)) = dirdata(mesh.V(V(i),:)); 
    % dirdata function of boundary data (vector valued)   
end

%----------
uu = S\f;     % resolution
%----------

%-----------------------------------
% ERROR in discrete energy norm
%-----------------------------------
for k=1:ss
    sol(k)=solcal(mesh.V(k,1:2));
end

errene = sqrt( (uu'-sol)*S*(uu-sol')  / (sol*S*sol') )   


%---------------------------------
%  ERROR IN L-INFINITY
%---------------------------------

errinf = max(abs(sol'-uu))/max(abs(sol'))  


%-----------------------------------
% ENERGY evaluations
% TENTATIVO di valutare l'errore locale in modo euristico ...
% ... col pezzo di energia di stability
%-----------------------------------

% percentage of consistent energy (global)
percentage = ((uu')*Ss*uu)  / ((uu')*Ss*uu + (uu')*Sc*uu) 

for k=1:mm
    m = mesh.P(k,1);     % number of vertexes of element 
    X = mesh.V(mesh.P(k,2:m+1),1:2);     % build X with vertex coordinates
    [a,b]=baric(X);  bar(k,:)=b;
    vertvec = mesh.P(k,2:m+1);  % local index numbers
    [Slc,Sls]=LocS(X);  % compute local matrixes
    Sl= Slc+Sls;
    % estimator(k)=uu(vertvec)'*Sls*uu(vertvec);
    estimator(k)=(uu(vertvec)'*Sls*uu(vertvec))/(uu(vertvec)'*Sl*uu(vertvec));
    loc_err(k)= (uu(vertvec)'-sol(vertvec))*Sl*(uu(vertvec)-sol(vertvec)');
end

% -----------------------------------------
%    OPTION ONE
% -----------------------------------------

% PERC=0.70;
% maxerr=max(loc_err);
% maxest=max(estimator);
% 
% figure(1)
% hold on
% for k=1:mm
%     if loc_err(k) > PERC*maxerr
%         plot(bar(k,1),bar(k,2),'*')
%     end    
% end    
% 
% hold off
% figure(2)
% hold on
% for k=1:mm
%     if estimator(k) > PERC*maxest
%         plot(bar(k,1),bar(k,2),'*r')
%     end    
% end    


% -----------------------------------------
%    OPTION TWO
% -----------------------------------------

[a,I]=sort(loc_err);
PERC=0.70;  % percentuale di quelli che non marko!

figure(1)
hold on
for i=floor(mm*PERC):mm
    k=I(i);
    plot(bar(k,1),bar(k,2),'*')   
end    

hold off
figure(2)
hold on
[a,I]=sort(estimator);
for i=floor(mm*PERC):mm
    k=I(i);
    plot(bar(k,1),bar(k,2),'*r')   
end    




%---------------------------------
% Option for a rough point-plot
%---------------------------------
% X=mesh.V(:,1);
% Y=mesh.V(:,2);    
% plot3(X,Y,uu,'*') 









    