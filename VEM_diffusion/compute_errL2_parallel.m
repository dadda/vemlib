function errL2 = compute_errL2_parallel( exactsol, U, k, Nelt, Nver, Nfc, elements, edgebyele, perm, ...
                                         xqd, yqd, wqd, basis_on_qd, P0s_by_ele, ...
                                         eleDofs, dofsLayout, use_mp )
% L^2 norm of the approximation error.
%    ERRL2 = COMPUTE_ERRL2_PARALLEL( EXACTSOL, U, K, NELT, NVER, NFC, ELEMENTS, EDGEBYELE, PERM, ...
%                                XQD, YQD, WQD, BASIS_ON_QD, P0S_BY_ELE, USE_MP )
%    computes the L^2 norm of the approximation error (u - u_h), where u is
%    the exact solution and u_h is the corresponding VEM approximation.
%    This function uses the L^2-projection of the VEM basis functions in
%    terms of the monomial basis. The norm of the error is computed with a
%    parallel loop over all the elements.
%
%    Inputs:
%
%    - EXACTSOL: function pointer for evaluating the exact solution.
%
%    - U : column vector with the degrees of freedom of the VEM approximation
%          to the exact solution.
%
%    - K : degree of the VEM space.
%
%    - NELT : total number of elements (polygons) of the mesh.
%
%    - NVER : total number of vertices of the mesh.
%
%    - NFC : total number of edges of the mesh.
%
%    - ELEMENTS : a NELT x (NVER_MAX+1) matrix. For any E = 1, ..., NELT,
%          ELEMENTS(E,:) contains, in order: the number of vertices of
%          element E, the indices of the vertices of E in counterclockwise
%          order, followed by as many zeros as needed to accomodate the
%          presence of elements with higher number of vertices in the mesh.
%          NVER_MAX is the maximum number of vertices per polygon in the
%          mesh.
%
%    - EDGEBYELE: a NELT x NVER_MAX matrix, whose E-th row
%          contains the number of edges that make up \partial E, followed by as
%          many zeros as needed to accomodate the presence of elements with
%          higher number of vertices in the mesh.
%
%    - PERM: a NELT x NVER_MAX matrix, whose E-th row indicates
%          what permutations are needed (1 (no permutation) or 2 (switch the
%          order)) for the edges of element E to match their global orientation
%          provided by the field EDGES in the extended mesh data structure.
%
%   - XQD, YQD, WQD : [NELT x 1] cells of [Nqd_loc x 1] (not Nqd_max !!)
%          column arrays containing quadrature nodes and weights of cubature
%          rules on each polygon.
%
%   - BASIS_ON_QD : [Nelt x 1] cell of matrices containing evaluation of
%          basis functions on internal cubature nodes for each element.
%
%   - P0S_BY_ELE : [Nelt x 1] cell of [d2 x Ndof_loc] matrices representing
%          the L^2 projection of the basis functions, P0s, for each element
%          of the mesh, where d2 = dim(P^k), Ndof_loc = # of d.o.f.s of the local
%          VEM space.
%
%   - USE_MP : flag telling whether the Multiprecision Toolbox
%          (www.advanpix.com) should be used (USE_MP=1) or not (USE_MP=0).
%
%   Degrees of freedom of a function v_h in the virtual element space of degree k on
%   the whole domain are organized as follows:
%   * the value of v_h at all the vertices of the mesh;
%   * the value of v_h at the k-1 internal points of the (k+1)-point
%     Gauss-Lobatto quadrature rule on the first edge, the second edge,
%     and so on.
%   * the moments up to order k-2 of v_h in the first element, the second
%     element, and so on.
%

errL2     = 0.;
solL2norm = 0.;

if k == 1
    for E = 1:Nelt
        sol = exactsol(xqd{E}, yqd{E}, use_mp);
        
        if dofsLayout == 1
            elements_E = elements(E,:);
            local_dofs = elements_E(2:elements_E(1)+1)';

            Uloc = U(local_dofs);
        else
            Uloc = U(eleDofs{E});
        end

        errL2     = errL2 + wqd{E}' * (sol - basis_on_qd{E}*(P0s_by_ele{E}*Uloc)).^2;
        solL2norm = solL2norm + wqd{E}' * sol.^2;
    end
elseif k == 2
    for E = 1:Nelt
        sol = exactsol(xqd{E}, yqd{E}, use_mp);

        if dofsLayout == 1
            elements_E  = elements(E,:);
            edgebyele_E = edgebyele(E,:);

            local_dofs  = [elements_E(2:elements_E(1)+1)';
                           Nver + edgebyele_E(1:elements_E(1))';
                           Nver + Nfc + E];

            Uloc = U(local_dofs);
        else
            Uloc = U(eleDofs{E});
        end
        errL2     = errL2 + wqd{E}' * (sol - basis_on_qd{E}*(P0s_by_ele{E}*Uloc)).^2;
        solL2norm = solL2norm + wqd{E}' * sol.^2;
    end
else
    d2m2 = ((k-1)*k)/2;

    for E = 1:Nelt
        sol = exactsol(xqd{E}, yqd{E}, use_mp);

        if dofsLayout == 1
            elements_E  = elements(E,:);
            edgebyele_E = edgebyele(E,:);
            perm_E      = perm(E,:);

            face_dofs = bsxfun(@plus, (1:k-1)', ...
                Nver + (edgebyele_E(1:elements_E(1))-1)*(k-1));

            face_dofs(:, perm_E(1:elements_E(1)) == 2) = ...
                flipud(face_dofs(:, perm_E(1:elements_E(1)) == 2));

            local_dofs = ...
                [elements_E(2:elements_E(1)+1)';
                 ...
                 face_dofs(:);
                 ...
                 Nver + (k-1)*Nfc + (E-1)*d2m2 + (1:d2m2)'];

            Uloc = U(local_dofs);
        else
            Uloc = U(eleDofs{E});
        end

        errL2     = errL2 + wqd{E}' * (sol - basis_on_qd{E}*(P0s_by_ele{E}*Uloc)).^2;
        solL2norm = solL2norm + wqd{E}' * sol.^2;
    end
    
end

errL2 = sqrt( errL2 / solL2norm );

end
