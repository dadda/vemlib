function [Sx] = prod_Schur(A,x,i0,iB)
% realizza il prodotto matrice Schur complement * x
%


Sx=A(iB,iB)*x;

w=A(i0,iB)*x;
w1=w\A(i0,i0);
Sx=Sx-A(iB,i0)*w1;


end

