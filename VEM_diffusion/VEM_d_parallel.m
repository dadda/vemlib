function [U, errH1, errL2, hE, S, F, verDofs, diridofs, kappa] = ...
    VEM_d_parallel(mesh, k, eoa, compressquad, basis_type, use_mp, i_F, rho, dofsLayout)
% VEM_D_PARALLEL  Virtual Element Method for the Poisson equation in 2D
%                 (parallel version)
%
%    [U, ERRS, ERRINF, ERRL2, MESH] = VEM_D_PARALLEL(MESH, K, EOA, COMPRESSQUAD, BASIS_TYPE, USE_MP)
%    solves the Poisson equation in 2D with the Virtual Element Method.
%
%    Inputs:
%
%   - MESH : extended mesh data structure (see loadmesh.m for a description
%        of this data structure).
%
%   - K : degree of the VEM space.
%
%   - EOA : extra order of accuracy for 1D and 2D quadrature formulas.
%
%   - COMPRESSQUAD : compress quadrature rule to have a rule with less
%        nodes/weights (equal to the dimension of the desidered polynomial
%        space) but the same algebraic degree of exactness.
%
%   - BASIS_TYPE : type of monomial basis functions to be used. BASIS_TYPE=1
%        for the monomial basis functions defined in the Hitchhicker's paper. 
%        BASIS_TYPE=2 for the Hitchhicker's basis functions scaled by their
%        L^2 norm.
%
%   - USE_MP : flag telling whether the Multiprecision Toolbox
%        (www.advanpix.com) should be used (USE_MP=1) or not
%        (USE_MP=0).
%
%   - I_F : flag for choosing type of load term.
%           * I_F = 1, compute exact load term
%           * I_F = 2, load term from the uniform distribution on [-1,1].
%
%   The load is given as a function "loadcal" from the domain into Reals^2;
%   non-hom dirichlet BCs are given through function "dirdata".
%
%   Outputs:
%
%   - U : column vector with the degrees of freedom of the VEM approximation
%        to the exact solution.
%
%   - ERRS, ERRINF, ERRL2 : energy norm, inf norm, and L^2 norm of the
%        approximation error.
%
%   - MESH : extended mesh structure with four new additional fields:
%        * HE     : [1 x Nelt] array with the elements diameters.
%        * XB, YB : [1 x Nelt] arrays with the coordinates of the elements
%                   centroids.
%       Nelt is the number of elements of the mesh.
%

d2m2                        = ((k-1)*k)/2;

[xqd1, wqd1] = lobatto(k+1, use_mp);
formula1d = [(1.-xqd1)/2. (xqd1+1.)/2. wqd1/2.];
clear xqd1 wqd1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute local matrices and assemble global system

% Assemble local matrices and right hand sides
[ K, row_dofs_for_K, col_dofs_for_K, rhs, dofs_for_rhs, P0s_by_ele, ...
  verDofs, edgeInternalDofs, edgePerm, eleDofs, ~, ...
  hE, areas, xb, yb, xqd, yqd, wqd, basis_on_qd] = ...
    assemble_local_matrices_and_rhs_parallel(k, eoa, compressquad, mesh, formula1d, basis_type, use_mp, i_F, @loadcal, rho, dofsLayout);

% Assemble global stiffness matrix
S = sparse(row_dofs_for_K, col_dofs_for_K, K);

% Assemble global right hand side
F = sparse(dofs_for_rhs, ones([length(dofs_for_rhs) 1]), rhs);

N = mesh.Nver + (k-1)*mesh.Nfc + d2m2*mesh.Nelt;
if length(F) ~= N
    error('The global system does not have the expected dimension');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute Dirichlet boundary conditions

[Udir, diridofs] = compute_dirichlet(mesh, k, formula1d, ...
                                     verDofs, edgeInternalDofs, edgePerm, ...
                                     dofsLayout, use_mp);

dofs        = setdiff(1:double(N), diridofs);
F(diridofs) = Udir;
F(dofs)     = F(dofs) - S(dofs, diridofs)*F(diridofs);

S( diridofs, : ) = 0;
S( :, diridofs ) = 0;
S( diridofs + N*(diridofs-1) ) = 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Solve the linear system

fprintf('Solving the linear system ...');
U = S\F;
fprintf(' done\n');
% U = [];

% U = zeros([N 1]);
% S11 = S(dofs, dofs);
% S12 = S(dofs, diridofs);
% U(dofs) = S11\(F(dofs) - S12*Udir(:));
% U(diridofs) = Udir;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Error computation

if nargout > 1

    %---------------------------------
    %  ERROR ESTIMATOR IN THE H^1 SEMINORM
    %---------------------------------

    errH1 = compute_errH1( @exactsol, U, k, mesh.Nver, mesh.Nfc, mesh.Nelt, ...
                           mesh.coordinates, mesh.elements, mesh.edgebyele, mesh.perm, ...
                           formula1d, xqd, yqd, wqd, basis_on_qd, hE, areas, xb, yb, ...
                           eleDofs, dofsLayout, basis_type, use_mp );

    %---------------------------------
    %  ERROR ESTIMATOR IN THE L^2 NORM
    %---------------------------------

    errL2 = compute_errL2_parallel( @exactsol, U, k, mesh.Nelt, mesh.Nver, mesh.Nfc, ...
                                    mesh.elements, mesh.edgebyele, mesh.perm, xqd, yqd, wqd, basis_on_qd, ...
                                    P0s_by_ele, eleDofs, dofsLayout, use_mp );

    %---------------------------------
    % ESTIMATE 1-NORM CONDITION NUMBER
    %---------------------------------

    if nargout >= 9
        kappa = condest(S);
    end

end
