function [ xbnd_E, ybnd_E, xqd_E, yqd_E, wqd_E, Nx_E, Ny_E, ...
    h_E, area_E, xb_E, yb_E ] = build_coordinate_matrices_parallel( k, eoa, compressquad, ...
                                                                    elements_E, coordinates, ...
                                                                    formula1d, use_mp )
% Geometric data structures for the parallel version of the VEM code.
%   [ xbnd_E, ybnd_E, xqd_E, yqd_E, wqd_E, Nx_E, Ny_E, ...
%     h_E, area_E, xb_E, yb_E ] = build_coordinate_matrices_parallel( k, eoa, elements_E, ...
%                                                              coordinates, formula1d, use_mp )
%   computes geometric data structures for a single polygon as required by
%   the parallel version of the VEM code.
%
%   Inputs:
%
%   - K : degree of the VEM space.
%
%   - EOA : extra order of accuracy for 1D and 2D quadrature formulas.
%
%   - COMPRESSQUAD : compress quadrature rule to have a rule with less
%       nodes/weights (equal to the dimension of the desidered polynomial
%       space) but the same algebraic degree of exactness.
%
%   - ELEMENTS_E : a [1 x Nver_max+1] vector containing, in order: the number of
%       vertices of element E, the indices of its vertices in counterclockwise
%       order, followed by as many zeros as needed to have a vector of
%       length Nver_max+1, where Nver_max is the maximum number of vertices
%       for a polygon in the mesh.
%
%   - COORDINATES : a [Nver x 2] matrix with the coordinates of all the
%       vertices of the mesh.
%
%   - FORMULA1D : barycentric coordinates (FORMULA1D(:,1), FORMULA1D(:,2))
%       and weights (FORMULA1D(:,3)) of the Gauss-Lobatto quadrature rule
%       of order K+1.
%
%   - USE_MP : flag telling whether the Multiprecision Toolbox
%       (www.advanpix.com) should be used (USE_MP=1) or not (USE_MP=0).
%
%   Outputs:
%
%   - XBND_E, YBND_E : [K*Nver_loc x 1] arrays with the coordinates of the
%       nodes corresponding to the boundary dofs of element E. Nver_loc is the
%       number of vertices of element E.
%
%   - XQD_E, YQD_E, WQD_E : [Nqd_loc x 1] arrays with quadrature nodes
%       (XQD_E, YQD_E) and weights (WQD_E) of the quadrature rule on
%       polygon E.
%
%   - NX_E, NY_E : [Nver_loc+1 x 1] arrays with the x and y components,
%       respectively, of the outward normal vectors for element E,
%       normalized so that the euclidean nor of each normal equals the
%       length of the corresponding edge. NX_E(1), NY_E(1) contain the
%       normal to the last edge of E, whereas NX_E(2:end), NY_E(2:end)
%       contain normal vectors from the first edge to the last one.
%
%   - H_E, AREA_E, XB_E, YB_E : diameter, area, and centroid coordinates of
%       element E.
%

Nver_loc = elements_E(1);

% Matrices associated to boundary degrees of freedom
xbnd_E = zeros(k*Nver_loc,1);
ybnd_E = xbnd_E;

xE = [coordinates(elements_E(2:Nver_loc+1),1)';
      coordinates(elements_E([3:Nver_loc+1,2]),1)'];
yE = [coordinates(elements_E(2:Nver_loc+1),2)';
      coordinates(elements_E([3:Nver_loc+1,2]),2)'];

xbnd_E(1:Nver_loc) = xE(1,:)';
ybnd_E(1:Nver_loc) = yE(1,:)';

% Arrays with outer normals

Nx_E = ybnd_E([1:Nver_loc 1]) - ybnd_E([Nver_loc 1:Nver_loc]);
Ny_E = xbnd_E([Nver_loc 1:Nver_loc]) - xbnd_E([1:Nver_loc 1]);
    
% k-1 internal points of the (k+1)-point Gauss-Lobatto quadrature rule
% on the edges of E
xbnd_E(Nver_loc+1:k*Nver_loc) = ...
    reshape(formula1d(2:k,1:2)*xE, Nver_loc*(k-1), 1);
    
ybnd_E(Nver_loc+1:k*Nver_loc) = ...
    reshape(formula1d(2:k,1:2)*yE, Nver_loc*(k-1), 1);

% Internal quadrature points and element diameter
if use_mp
    [xqd_E, yqd_E, wqd_E, h_E] = polygauss2013Mex( int32( 2*k+eoa ), ...
        double( [xbnd_E([1:Nver_loc 1]) ybnd_E([1:Nver_loc 1])] ) );
    xqd_E = mp(xqd_E); yqd_E = mp(yqd_E); wqd_E = mp(wqd_E); h_E = mp(h_E);
else
%     [xqd_E, yqd_E, wqd_E, h_E] = polygauss_2013(2*k+eoa, ...
%         [xbnd_E([1:Nver_loc 1]) ybnd_E([1:Nver_loc 1])], use_mp);
    [xqd_E, yqd_E, wqd_E, h_E] = polygauss2013Mex( int32( 2*k+eoa ), ...
        [xbnd_E([1:Nver_loc 1]) ybnd_E([1:Nver_loc 1])]);
end

if compressquad
    [pts, wqd_E] = comprexcub(2*k+eoa, [xqd_E, yqd_E], wqd_E, 1);
    xqd_E = pts(:,1);
    yqd_E = pts(:,2);
end

% Area and centroid coordinates
area_E = (Nx_E(2:end)'*xbnd_E(1:Nver_loc) + Ny_E(2:end)'*ybnd_E(1:Nver_loc))/2.0;
% area_E = sum(wqd_E);

% xb_E   = (wqd_E/area_E)' * xqd_E;
% yb_E   = (wqd_E/area_E)' * yqd_E;
xb_E   = (wqd_E' * xqd_E) / area_E;
yb_E   = (wqd_E' * yqd_E) / area_E;

end
