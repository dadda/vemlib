function [Slocs, row_dofs_for_Slocs, col_dofs_for_Slocs, rhs_locs, dofs_for_rhs_locs, P0s_by_ele, ...
          basis_on_qd, B, D] = ...
    assemble_local_matrices_and_rhs_sequential(k, mesh, ...
                            xbnd, ybnd, Xi_bnd, ...
                            xqd, yqd, wqd, ...
                            Nx, Ny, ...
                            formula1d, use_mp)
% Partially sequential and partially vectorial assemblage of local stiffness matrices and rhs.
%   [ SLOCS, ROW_DOFS_FOR_SLOCS, COL_DOFS_FOR_SLOCS, RHS_LOCS, DOFS_FOR_RHS_LOCS, P0S_BY_ELE, ...
%     BASIS_ON_QD, B, D ] = ...
%       ASSEMBLE_LOCAL_MATRICES_AND_RHS_SEQUENTIAL(K, MESH, XBND, YBND, XI_BND, ...
%          XQD, YQD, WQD, XI_QD, NX, NY, FORMULA1D, USE_MP)
%   computes all the local stiffness matrices and right hand sides for the
%   VEM for the Poisson equation.
%   Inputs:
%
%   - K : degree of the VEM space.
%
%   - MESH : extended mesh data structure (see loadmesh.m for a description
%         of this data structure.
%
%   - XBND, YBND, XI_BND : [K*Nver_max x Nelt] matrices representing
%         coordinates (XBND, YBND) and indicator function (XI_BND) of the
%         points associated to boundary degrees of freedom of each element
%         of the mesh. Nver_max is the maximum number of vertices for a
%         polygon over all the polygons of the mesh. Since mesh polygons
%         could have different numbers of vertices from each other, for
%         each column E = 1, ..., Nelt, only the following entries are
%         meaningful:
%         * XBND([1:Nelt_loc Nver_max+1:Nver_max+(K-1)*Nver_loc], E)
%         * similarly for YBND, XI_BND.
%
%         Observe the different use of Nver_loc/Nver_max.
%
%   - XQD, YQD, WQD: [Nqd x Nelt] matrices representing
%         quadrature nodes (XQD, YQD) and weights (WQD)
%         of the cubature rules on each polygon of the mesh.
%
%   - NX, NY : [Nver_max+1 x Nelt] matrices with the x and y components,
%         respectively, of the outward normal vectors for the elements,
%         normalized so that the euclidean norm of each normal equals the
%         length of the corresponding edge.
%         For each column E = 1, ..., Nelt, NX(1,E) and NY(1,E) contain the
%         normal to the last edge of element E, whereas NX(2:Nver_loc+1,E) and
%         NY(2:Nver_loc+1,E) contain from the first up to the last normal
%         vectors. Entries N*(Nver_loc+2:end,E) are meaningless.
%
%   - FORMULA1D : barycentric coordinates (FORMULA1D(:,1), FORMULA1D(:,2))
%         and weights (FORMULA1D(:,3)) of the Gauss-Lobatto quadrature rule
%         of order K+1.
%
%   - USE_MP : flag telling whether the Multiprecision Toolbox
%         (www.advanpix.com) should be used (USE_MP=1) or not (USE_MP=0).
%
%   Outputs (CAREFUL: this list is sligthly different from the sequential version of this function):
%
%   - SLOCS : 1D column vector collecting all the local stiffness matrices
%         one after the other.
%
%   - ROW_DOFS_FOR_SLOCS, COL_DOFS_FOR_SLOCS : 1D column vectors that
%         contain row and column degrees of freedom corresponding to local
%         stiffness matrices. These be used to generate the global
%         stiffness matrix with the command:
%
%         S = sparse(ROW_DOFS_FOR_SLOCS, COL_DOFS_FOR_SLOCS, SLOCS);
%
%   - RHS_LOCS : 1D column vector collecting all the local right hand sides
%         one after the other.
%
%   - DOFS_FOR_RHS_LOCS : 1D column vector that contain all the degrees of
%         freedom corresponding to the local right hand sides. It can be
%         used together with RHS_LOCS to assemble the global right hand
%         with the command:
%
%         F = sparse(DOFS_FOR_RHS_LOCS, ones([length(DOFS_FOR_RHS_LOCS) 1]), RHS_LOCS);
%
%         Remark: using accumarray instead of sparse for assembling F would
%         be more compact, but, unfortunately, the Multiprecision Toolbox
%         does not implement accumarray as of 2016/08/13.
%
%   - P0S_BY_ELE : column vector containing the L^2 projections of the
%         basis functions, P0s, for each element of the mesh, one after the
%         other.
%
%   - BASIS_ON_QD : [Nqd_max x Nelt x d2] matrix containing evaluation of
%         basis functions on internal cubature nodes for each element. For
%         each E = 1, ..., Nelt, entries BASIS_ON_QD(Nqd_loc+1:Nqd_max, E, :)
%         are meaningless.
%
%   - B, D : [d2 x Ndof_max x Nelt] and [Ndof_max x d2 x Nelt] matrices as
%         defined in equations 3.14 and 3.17, respectively, of Hitchhiker's
%         paper. For each E = 1, ..., mesh.Nelt, meaningful entries are:
%         * B(:, [1:Nelt_loc Nver_max+1:Nver_max+(K-1)*Nver_loc k*Nver_max+1:k*Nver_max+d2m2], E)
%         * D([1:Nelt_loc Nver_max+1:Nver_max+(K-1)*Nver_loc k*Nver_max+1:k*Nver_max+d2m2], :, E)
%
%   Degrees of freedom of a function v_h in the virtual element space on
%   the whole domain are organized as follows:
%   * the value of v_h at all the vertices of the mesh;
%   * the value of v_h at the k-1 internal points of the (k+1)-point
%     Gauss-Lobatto quadrature rule on the first edge, the second edge,
%     and so on.
%   * the moments up to order k-2 of v_h in the first element, the second
%     element, and so on.
%
%   In order to compute matrices B and D vectorially, thereby avoiding
%   loops on the elements, this function heavily relies on: bsxfun(),
%   permute(), reshape().
%
%   Note on the performance: this function is faster than its parallel
%   counterpart when running the code on a single processor AND when
%   doubles are used. However, the use of bsxfun(), permute(), and
%   reshape() make it a bit cryptic.
%   On the other hand, its parallel counterpart is faster when using at
%   least few processors and even more when the Multiprecision Toolbox is
%   used. Also, its implementation is clearer (for example, no 'ghost'
%   entries are required).
%

d2   = ((k+1)*(k+2))/2;
d2m2 = ((k-1)*k)/2;

basis_on_qd  = monomial_basis(xqd, yqd, k, mesh.xb, mesh.yb, mesh.hE, ones(size(xqd)));
[basis_on_bnd, grad_basis_on_bnd] = monomial_basis(xbnd, ybnd, k, ...
    mesh.xb, mesh.yb, mesh.hE, Xi_bnd);

basis_on_bnd      = permute(basis_on_bnd, [1, 3, 2]);
grad_basis_on_bnd = permute(grad_basis_on_bnd, [3, 1, 2, 4]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Few remarks on the matrices associated to the PiNabla and L2 projectors.
% Matrices are introduced in the order they are created in the code.
%
% Matrix H
% --------
%
% We denote by H a matrix whose form depends on the degree k of the VEM:
% - k = 1: H is not needed because there are no internal degrees of
%          freedom.
% - k = 2: H is the 1 x d2 x Nelt array defined as follows:
%          H(1,i,E) = (1,m_i)|_E, for i = 1, ..., d2, E = 1, ..., Nelt.
%          In this case, H|_E coincides with the last row
%          of matrix D|_E (see 3.17).
% - k >= 3: H is the d2 x d2 x Nelt array defined as follows:
%           H(i,j,E) = (m_i, m_j)|_E (see 5.3). In this case, the last
%           n_{k-2} rows of D will be "extracted" from H, to save
%           computational time.
%
% Matrix D
% --------
%
% This is the matrix defined at (3.17). It is created by stacking the part
% related to the internal degrees of freedom next to the one associated to
% the boundary degrees of freedom. Observe that, if not all the elements have
% the same number of vertices, D will contain several zero entries, just
% like xbnd_scaled and ybnd_scaled, and similarly to xqd_scaled and
% yqd_scaled.
%
% Matrix B
% --------
%
% In the case k = 1, we can explicitly compute the PiNabla projection of
% VEM basis functions. This allows a simplified assemblage of matrix B. In
% the case k >= 2, the assemblage is a bit more involved and it is divided
% into three main steps:
% - assemble first row of B (corresponding to P_0\phi_i = 1/|E|\int_E \phi_i)
% - assemble the right block of B (corresponding to -\int_E \Delta m_\alpha \phi_i)
% - assemble the left block of B (corresponding to
%   \int_{\partial E}\frac{\partial m_\alpha}{\partial n}\phi_i).

if k == 1
    H = [];
    
    D = basis_on_bnd;
    
    B = zeros(3, mesh.Nver_max, mesh.Nelt);
    B(1,:,:) = reshape( ...
                        bsxfun(@times, ...
                               1./sum(Xi_bnd, 1), ...
                        Xi_bnd), ...
               [1, mesh.Nver_max, mesh.Nelt]);
    B(2,:,:) = reshape( ...
                        bsxfun(@times, 1./mesh.hE, 0.5 * (Nx(1:end-1,:) + Nx(2:end,:))), ...
               [1, mesh.Nver_max, mesh.Nelt]);
    B(3,:,:) = reshape( ...
                        bsxfun(@times, 1./mesh.hE, 0.5 * (Ny(1:end-1,:) + Ny(2:end,:))), ...
               [1, mesh.Nver_max, mesh.Nelt]);
    
else
    if k == 2
        H = sum( bsxfun(@times, wqd, basis_on_qd), 1); ... % With bsxfun we are multiplying
                                                       ... % each basis function by the
                                                       ... % quadrature weights at each quadrature
                                                       ... % node on each element. This is
                                                       ... % possible because bsxfun expands
                                                       ... % wqd along its singleton dimension
                                                       ... % (the third one)
                                                       ...
                                                       ... % With the sum function we are computing \sum_{q = 1}^Nqd
                                                       ... % w_q * m_i|_E for each i = 1, ..., d2, and for each
                                                       ... % E = 1, ..., Nelt.    
    
        D = [ basis_on_bnd;
              permute(bsxfun(@times, 1./mesh.areas, H), [1, 3, 2]) ];
    else
        H = zeros(d2, mesh.Nelt, d2);
        for i = 1:d2
            for j = 1:i-1
                H(i,:,j) = sum( wqd.* (basis_on_qd(:,:,i).*basis_on_qd(:,:,j)), 1 );
                H(j,:,i) = H(i,:,j);
            end
            H(i,:,i) = sum( wqd.* basis_on_qd(:,:,i).^2, 1 );
        end
    
        D = [ basis_on_bnd;
              permute(bsxfun(@times, 1./mesh.areas, H(1:d2m2,:,:)), [1,3,2]) ];
        H = permute(H, [1, 3, 2]);
    end
    
    B = zeros([d2 k*mesh.Nver_max+d2m2 mesh.Nelt]);
    
    %%% Matrix B - first row (reference: equations (2.2) and (3.4b))
    B(1, k*mesh.Nver_max+1, :) = 1.;

    %%% Matrix B - bottom right block, corresponding to the first term in
    % equation (3.12).
    
    % Compute coefficients of the laplacians of basis functions as a linear
    % combination of the basis functions themselves.
    dab = llcmb(k, mesh.hE);

    B(2:end, k*mesh.Nver_max+1:end, :) = - bsxfun(@times, ...
        reshape(mesh.areas, [1,1,mesh.Nelt]), dab(2:end,:,:));
                                 
    %%% Matrix B - bottom left block, first Nver_max columns
    
    % This part corresponds to the contribution of the vertices of an
    % element to the second term in (3.12) (see notes).
    
    B(2:end, 1:mesh.Nver_max, :) = formula1d(1,3) * ...
        ( bsxfun(@times, reshape( Nx(1:end-1,:)+Nx(2:end,:), [1,mesh.Nver_max,mesh.Nelt] ), ...
                         grad_basis_on_bnd(2:end, 1:mesh.Nver_max, :, 1)) + ...
          bsxfun(@times, reshape( Ny(1:end-1,:)+Ny(2:end,:), [1,mesh.Nver_max,mesh.Nelt] ), ...
                         grad_basis_on_bnd(2:end, 1:mesh.Nver_max, :, 2)) );

    %%% Matrix B - bottom left block, columns from Nver_max+1 to k*Nver_max
    
    % This part corresponds to the contribution of the internal
    % Gauss-Lobatto quadrature points to the second term in (3.12)
    % (see notes).

    B(2:end, mesh.Nver_max+1:k*mesh.Nver_max, :) = ...
        bsxfun(@times, ...
            reshape( ...
                permute ( ...
                          bsxfun( @times, reshape(formula1d(2:k,3), [1,1,k-1]), repmat(Nx(2:end,:),[1,1,k-1]) ), ...
                [3,1,2]), ...
            [1 (k-1)*mesh.Nver_max mesh.Nelt]), ...
            grad_basis_on_bnd(2:end,mesh.Nver_max+1:end,:,1)) + ...
        ...
        bsxfun(@times, ...
            reshape( ...
                permute ( ...
                          bsxfun( @times, reshape(formula1d(2:k,3), [1,1,k-1]), repmat(Ny(2:end,:),[1,1,k-1]) ), ...
                [3,1,2]), ...
            [1 (k-1)*mesh.Nver_max mesh.Nelt]), ...
            grad_basis_on_bnd(2:end,mesh.Nver_max+1:end,:,2));

end

% Stiffness matrix K
% ------------------

% Remark: the dimension of a local stiffness matrix is Ndof x Ndof =
% Ndof^2, with Ndof = k*Nver_loc + d2m2.

nb_Slocs_dofs      = k^2*sum(mesh.elements(:,1).^2) + 2*k*d2m2*sum(mesh.elements(:,1)) + mesh.Nelt*d2m2^2;
Slocs              = zeros([nb_Slocs_dofs 1]);
row_dofs_for_Slocs = double(Slocs);
col_dofs_for_Slocs = row_dofs_for_Slocs;
cumdofs_for_Slocs  = 0;

% The following vector is needed for computing the L2 error
nb_P0s_by_ele_dofs     = k*d2*sum(mesh.elements(:,1)) + d2*d2m2*mesh.Nelt;
P0s_by_ele             = zeros([nb_P0s_by_ele_dofs 1]);
cumdofs_for_P0s_by_ele = 0;

nb_rhs_dofs     = k*sum(mesh.elements(:,1)) + mesh.Nelt*d2m2;
rhs_locs             = zeros([nb_rhs_dofs 1]);
dofs_for_rhs_locs    = double(rhs_locs);
cumdofs_for_rhs_locs = 0;

% If k <= 2 the Pi-Nabla projectior and the L^2 projectior coincide. In
% this case, we can just use Pi-Nabla to build the right hand side

% Evaluate the load term at the quadrature nodes
f = loadcal(xqd, yqd, use_mp);

if k <= 2
    for E = 1:mesh.Nelt
        
        %%% Stiffness matrix

        % select only dofs from current element
        select_dofs   = [1:mesh.elements(E,1) mesh.Nver_max+1:mesh.Nver_max+(k-1)*mesh.elements(E,1) k*mesh.Nver_max+1:k*mesh.Nver_max+d2m2];
        
        G   = B(:, select_dofs, E) * D(select_dofs ,: , E);
        
        PNs = G\B(:, select_dofs, E);
        PN  = D(select_dofs, :, E)*PNs;

        nb_local_dofs = length(select_dofs);
        Gt      = G;
        Gt(1,:) = 0.;        
        Slocs(cumdofs_for_Slocs+1:cumdofs_for_Slocs+nb_local_dofs^2) = ...
            reshape(PNs'*Gt*PNs + (eye(nb_local_dofs) - PN)'*(eye(nb_local_dofs) - PN), ...
            [nb_local_dofs^2,1]);

        %%% Right hand side - PNs == P0s for k = 1, 2

        size_local_P0s = d2 * nb_local_dofs;
        P0s_by_ele(cumdofs_for_P0s_by_ele+1:cumdofs_for_P0s_by_ele+size_local_P0s) = PNs(:);
        cumdofs_for_P0s_by_ele = cumdofs_for_P0s_by_ele + size_local_P0s;
        
        rhs_locs(cumdofs_for_rhs_locs+1:cumdofs_for_rhs_locs+nb_local_dofs) = ...
            PNs'*(permute(basis_on_qd(:,E,:), [3,1,2])*(wqd(:,E).*f(:,E)));
        
        %%% Dofs vectors for Slocs and rhs_locs
        dofs_for_rhs_locs(cumdofs_for_rhs_locs+1:cumdofs_for_rhs_locs+nb_local_dofs) = ...
            [mesh.elements(E, 2:mesh.elements(E,1)+1)';
             ...
             kron((1:k-1)',(mesh.Nver + mesh.edgebyele(E,1:mesh.elements(E,1))'));
             ...
             mesh.Nver + (k-1)*mesh.Nfc + (E-1)*d2m2 + (1:d2m2)'];

        tmp = repmat(dofs_for_rhs_locs(cumdofs_for_rhs_locs+1:cumdofs_for_rhs_locs+nb_local_dofs), 1, nb_local_dofs);
        row_dofs_for_Slocs(cumdofs_for_Slocs+1:cumdofs_for_Slocs+nb_local_dofs^2) = tmp(:);
        tmp = tmp';
        col_dofs_for_Slocs(cumdofs_for_Slocs+1:cumdofs_for_Slocs+nb_local_dofs^2) = tmp(:);

        cumdofs_for_Slocs = cumdofs_for_Slocs + nb_local_dofs^2;
        cumdofs_for_rhs_locs = cumdofs_for_rhs_locs + nb_local_dofs;
    end
else
    % In this case we have to compute the L2 projection of the basis
    % functions to assemble the right hand side. We use formula (6.1).
    for E = 1:mesh.Nelt
        
        %%% Stiffness matrix

        % select only dofs from current element
        select_dofs   = [1:mesh.elements(E,1) mesh.Nver_max+1:mesh.Nver_max+(k-1)*mesh.elements(E,1) k*mesh.Nver_max+1:k*mesh.Nver_max+d2m2];
        
        G   = B(:, select_dofs, E) * D(select_dofs ,: , E);
        PNs = G\B(:, select_dofs, E);
        PN  = D(select_dofs, :, E)*PNs;
        nb_local_dofs = length(select_dofs);
        Gt      = G;
        Gt(1,:) = 0.;
        Slocs(cumdofs_for_Slocs+1:cumdofs_for_Slocs+nb_local_dofs^2) = ...
            reshape(PNs'*Gt*PNs + (eye(nb_local_dofs) - PN)'*(eye(nb_local_dofs) - PN), ...
            [nb_local_dofs^2,1]);
        
        %%% Right hand side
        
        % Compute matrix C first
        C = zeros([d2 nb_local_dofs]);
        C(1:d2m2, k*mesh.elements(E,1)+1:end) = mesh.areas(E)*eye(d2m2);
        C(d2m2+1:end, :) = H(d2m2+1:end,:,E)*PNs;
        P0s = H(:,:,E)\C;

        size_local_P0s = d2 * nb_local_dofs;
        P0s_by_ele(cumdofs_for_P0s_by_ele+1:cumdofs_for_P0s_by_ele+size_local_P0s) = P0s(:);
        cumdofs_for_P0s_by_ele = cumdofs_for_P0s_by_ele + size_local_P0s;

        % Equation (6.1) is used for the right hand side
        rhs_locs(cumdofs_for_rhs_locs+1:cumdofs_for_rhs_locs+nb_local_dofs) = ...
            P0s'*(permute(basis_on_qd(:,E,:), [3,1,2])*(wqd(:,E).*f(:,E)));

        %%% Dofs vectors for Slocs and rhs_locs
                        
        % Mind the orientation of internal
        % faces!! This becomes critical when k >= 3, because dofs
        % associated to Gauss-Lobatto internal nodes will be >= 2, and so,
        % the orientation of a face (edge) will tell us in which order dofs
        % should be considered in the assemblage


        face_dofs = bsxfun(@plus, (1:k-1)', ...
            mesh.Nver + (mesh.edgebyele(E,1:mesh.elements(E,1))-1)*(k-1));

        face_dofs(:,mesh.perm(E,1:mesh.elements(E,1)) == 2) = ...
            flipud(face_dofs(:,mesh.perm(E,1:mesh.elements(E,1)) == 2));
        
        dofs_for_rhs_locs(cumdofs_for_rhs_locs+1:cumdofs_for_rhs_locs+nb_local_dofs) = ...
            [mesh.elements(E, 2:mesh.elements(E,1)+1)';
             ...
             face_dofs(:);
             ...
             mesh.Nver + (k-1)*mesh.Nfc + (E-1)*d2m2 + (1:d2m2)'];

        tmp = repmat(dofs_for_rhs_locs(cumdofs_for_rhs_locs+1:cumdofs_for_rhs_locs+nb_local_dofs), 1, nb_local_dofs);
        row_dofs_for_Slocs(cumdofs_for_Slocs+1:cumdofs_for_Slocs+nb_local_dofs^2) = tmp(:);
        tmp = tmp';
        col_dofs_for_Slocs(cumdofs_for_Slocs+1:cumdofs_for_Slocs+nb_local_dofs^2) = tmp(:);

        cumdofs_for_Slocs = cumdofs_for_Slocs + nb_local_dofs^2;
        cumdofs_for_rhs_locs = cumdofs_for_rhs_locs + nb_local_dofs;
    end
end


end
