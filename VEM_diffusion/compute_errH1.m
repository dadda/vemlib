function [ errH1, solH1seminorm ] = compute_errH1( exactsol, U, k, Nver, Nfc, Nelt, coordinates, elements, edgebyele, perm, ...
                                                   formula1d, xqd, yqd, wqd, basis_on_qd, hE, areas, xb, yb, ...
                                                   eleDofs, dofsLayout, basis_type, use_mp )

errH1         = 0;
solH1seminorm = 0;

if k == 1
    for E = 1:Nelt
        [ ~, gradSolx, gradSoly ] = exactsol(xqd{E}, yqd{E}, use_mp);

        Nver_loc = elements(E,1);
        xbnd     = coordinates(elements(E,2:Nver_loc+1),1)';
        ybnd     = coordinates(elements(E,2:Nver_loc+1),2)';
        Nx       = ybnd([1:Nver_loc 1]) - ybnd([Nver_loc 1:Nver_loc]);
        Ny       = xbnd([Nver_loc 1:Nver_loc]) - xbnd([1:Nver_loc 1]);

        P0grad = [ Nx(1:Nver_loc)+Nx(2:end) Ny(1:Nver_loc)+Ny(2:end) ] / (2 * areas(E));

        if dofsLayout == 1
            Uloc = U(elements(E,2:Nver_loc+1));
        else
            Uloc = U(eleDofs{E});
        end

        errH1         = errH1 + wqd{E}' * ( (gradSolx - P0grad(1:Nver_loc)*Uloc).^2 + ...
                                            (gradSoly - P0grad(Nver_loc+1:end)*Uloc).^2 );
        solH1seminorm = solH1seminorm + wqd{E}' * (gradSolx.^2 + gradSoly.^2);
    end
else
    dPkm1 = ((k+1)*k) / 2;
    dPkm2 = (k*(k-1)) / 2;

    for E = 1:Nelt
        [ ~, gradSolx, gradSoly ] = exactsol(xqd{E}, yqd{E}, use_mp);

        H = zeros(dPkm1, dPkm1);
        for i = 1:dPkm1
            for j = 1:i-1
                H(i,j) = wqd{E}' * (basis_on_qd{E}(:,i).*basis_on_qd{E}(:,j));
                H(j,i) = H(i,j);
            end
            H(i,i) = wqd{E}' * (basis_on_qd{E}(:,i).^2);
        end

        Nver_loc = elements(E,1);

        % Matrices associated to boundary degrees of freedom
        xbnd = zeros(1, k*Nver_loc);
        ybnd = xbnd;

        xE = [coordinates(elements(E,2:Nver_loc+1),1)';
              coordinates(elements(E,[3:Nver_loc+1,2]),1)'];
        yE = [coordinates(elements(E,2:Nver_loc+1),2)';
              coordinates(elements(E,[3:Nver_loc+1,2]),2)'];

        xbnd(1:Nver_loc) = xE(1,:);
        ybnd(1:Nver_loc) = yE(1,:);

        % Arrays with outer normals

        Nx = ybnd([1:Nver_loc 1]) - ybnd([Nver_loc 1:Nver_loc]);
        Ny = xbnd([Nver_loc 1:Nver_loc]) - xbnd([1:Nver_loc 1]);

        % k-1 internal points of the (k+1)-point Gauss-Lobatto quadrature rule
        % on the edges of E
        xbnd(Nver_loc+1:k*Nver_loc) = ...
            reshape(formula1d(2:k,1:2)*xE, 1, Nver_loc*(k-1));

        ybnd(Nver_loc+1:k*Nver_loc) = ...
            reshape(formula1d(2:k,1:2)*yE, 1, Nver_loc*(k-1));

        basis_on_bnd = monomial_basis(xbnd, ybnd, k-1, ...
            xb(E), yb(E), hE(E), ones(size(xbnd)));
        basis_on_bnd = permute(basis_on_bnd, [3,2,1]);
        
        N  = k*Nver_loc + dPkm2;
        B0 = zeros(dPkm1, 2*N);

        [ xab, yab ] = glcmb( k-1, hE(E) );

        if basis_type == 2
            basisL2norm  = sqrt( wqd{E}' * basis_on_qd{E}(:,1:dPkm1).^2 )';
            basis_on_bnd = bsxfun(@times, 1./basisL2norm, basis_on_bnd);

            B0(:,k*Nver_loc+1:N)     = - areas(E) * bsxfun(@times, 1./basisL2norm, xab);
            B0(:,N+k*Nver_loc+1:end) = - areas(E) * bsxfun(@times, 1./basisL2norm, yab);
        else
            B0(:,k*Nver_loc+1:N)     = - areas(E) * xab;
            B0(:,N+k*Nver_loc+1:end) = - areas(E) * yab;
        end

        B0(:,1:Nver_loc) = bsxfun(@times, (Nx(1:end-1)+Nx(2:end))*formula1d(1,3), basis_on_bnd(:,1:Nver_loc));
        B0(:,Nver_loc+1:k*Nver_loc) = ...
            bsxfun(@times, kron(Nx(2:end), formula1d(2:k,3)'), basis_on_bnd(:,Nver_loc+1:end));

        B0(:,N+1:N+Nver_loc) = bsxfun(@times, (Ny(1:end-1)+Ny(2:end))*formula1d(1,3), basis_on_bnd(:,1:Nver_loc));
        B0(:,N+Nver_loc+1:N+k*Nver_loc) = ...
            bsxfun(@times, kron(Ny(2:end), formula1d(2:k,3)'), basis_on_bnd(:,Nver_loc+1:end));

        P0grad = H\B0;

        if dofsLayout == 1
            face_dofs = bsxfun(@plus, (1:k-1)', ...
                Nver + (edgebyele(E,1:Nver_loc)-1)*(k-1));

            face_dofs(:, perm(E,1:Nver_loc) == 2) = ...
                flipud(face_dofs(:, perm(E,1:Nver_loc) == 2));

            local_dofs = ...
                [elements(E,2:Nver_loc+1)';
                 ...
                 face_dofs(:);
                 ...
                 Nver + (k-1)*Nfc + (E-1)*dPkm2 + (1:dPkm2)'];

            Uloc = U(local_dofs);
        else
            Uloc = U(eleDofs{E});
        end

        errH1         = errH1 + wqd{E}' * ( (gradSolx - basis_on_qd{E}(:,1:dPkm1)*P0grad(:,1:N)*Uloc).^2 + ...
                                            (gradSoly - basis_on_qd{E}(:,1:dPkm1)*P0grad(:,N+1:end)*Uloc).^2 );
        solH1seminorm = solH1seminorm + wqd{E}' * (gradSolx.^2 + gradSoly.^2);
    end
end

errH1         = sqrt( errH1 / solH1seminorm );
solH1seminorm = sqrt( solH1seminorm );

end
