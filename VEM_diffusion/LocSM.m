function [Sl,Ml]=LocSM(X)
% generates the local matrix for the diffusion problem (pure laplacian) 
% the input matrix X is m x 2 and describes the polygon (element) of m vertexes
% in each row, matrix X has x and y (coordinates) of the vertex, ordered anti clock-wise
% this function creates matrix Sl of size m x m (local stiffness matrix)
% -------

%alf=1; % factor for the "out of consistent" part
% -------
m=size(X,1);  % number of vertexes

[E,BB]=baric(X); % calculate area E and baricenter BB of element

% -------
% BASIS for the VEM space is value at first
% vertex, then second vertex, etc ... 
% BASIS for the polynomial space is 
% {1,x,y} with coordinates x,y scaled with the baricenter

% ----- BUILD MATRIX D -----------

D=[ones(m,1),X - ones(m,1)*BB]; 

% ----- BUILD MATRIX B ----------- (initially built as transpose)
B=zeros(m,3);    
for j=1:m     % add contribution of each edge
   % kernel part (first column) - see projection below 
   B(j,1) = (1/m); 
   % non kernel part (second and third columns)
   e = norm( X(j,:) - X(mod(j,m)+1,:) );     % edge length
   n = [ X(mod(j,m)+1,2) - X(j,2) , - X(mod(j,m)+1,1) + X(j,1) ];  % outward normal
   B(j,2:3) = B(j,2:3) + (1/2)*n;
   B(mod(j,m)+1,2:3) = B(mod(j,m)+1,2:3) + (1/2)*n; 
end
B=B'; % transpose 

% --- build projectors
G = B*D;          % G matrix from Hitchhikers paper
PNs = inv(G)*B;   % PiNabla star projector (polynomial basis)
PN = D*PNs;      % PiNabla projector (Vh basis)  
%  --- build local stiffness matrix ---
Gt(1,:)=zeros(1,3);   % matrix G tilde (null on the kernel)
Gt(2:3,:)=G(2:3,:);

Sl1 = PNs'*Gt*PNs;
Sl = Sl1 + (trace(Sl1)/2)*(eye(m)-PN)'*(eye(m)-PN);


%  --- build local mass matrix ---

[mQ]=intP(4,X);   % compute quadrature nodes and weights on the polygon
nQ=mQ(:,1:2);     % quadrature nodes
wQ=mQ(:,3);       % quadrature weights
nw=size(mQ,1);    % # quadrature nodes

% --- BUILD MATRIX of "P1 vs P1" L^2(E) product ---
nQs=nQ-ones(nw,1)*BB;    % shift quadrature nodes
p_nQs=[ones(nw,1) nQs];     % evaluate polynomials on nQs
Gm=zeros(3,3);
for l=1:nw
  for j=1:3
    for i=1:3
       Gm(i,j)=Gm(i,j)+wQ(l)*p_nQs(l,i)*p_nQs(l,j);
    end
  end
end

alf=1;
Ml1 = PNs'*Gm*PNs;
Ml = Ml1 + alf*(trace(Ml1)/3)*(eye(m)-PN)'*(eye(m)-PN);


% -----------------------
% KERNEL PROJECTION
% -----------------------
% Let the vertex based scalar product 
% <v,w> = (1/m) sum_{i=1}^m v(vertex_i)\cdot w(vertex_i) .
% Then, the projector on the kernel is
% P(v) := <1,v>
% -----------------------