function [ sol, gradSolx, gradSoly ] = exactsol(x, y, use_mp)
% exact solution function

% % Constant
% sol      = ones(size(x));
% gradSolx = zeros(size(x));
% gradSoly = zeros(size(x));

% % Linear
% sol      = x + y;
% gradSolx = ones(size(x));
% gradSoly = ones(size(x));

% % Quadratic
% sol      = (x.^2 + y.^2)/4.;
% gradSolx = 0.5 * x;
% gradSoly = 0.5 * y;

% % Cubic
% sol      = (x.^3 + y.^3)/6.;
% gradSolx = 0.5 * x.^2;
% gradSoly = 0.5 * y.^2;

% Cinf, homogeneous
if use_mp
    mypi = mp('pi');
else
    mypi = pi;
end
n        = 4;
C        = 1 / (2 * n^2 * mypi^2);
sol      = C * sin(n*mypi*x) .* sin(n*mypi*y);
gradSolx = C * n * mypi * cos(n*mypi*x) .* sin(n*mypi*y);
gradSoly = C * n * mypi * sin(n*mypi*x) .* cos(n*mypi*y);

% % Cinf, non homogeneous
% if use_mp
%     mypi = mp('pi');
% else
%     mypi = pi;
% end
% n        = 2;
% C        = 1 / (2 * n^2 * mypi^2);
% sol      = C * cos(n*mypi*x) .* cos(n*mypi*y);
% gradSolx = -C * n * mypi * sin(n*mypi*x) .* cos(n*mypi*y);
% gradSoly = -C * n * mypi * cos(n*mypi*x) .* sin(n*mypi*y);

end
