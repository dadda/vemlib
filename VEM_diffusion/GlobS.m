function [S]=GlobS(mesh)
% ------------
% builds global stiffness matrix using the local ones. 
% Diffusion problem 
% Takes mesh (struct) as input, see meshstruct.m file
% ------------

mm = size(mesh.P,1);  % number of elements
S = sparse(size(mesh.V,1),size(mesh.V,1));  % number of dofs --> REMEMBER THAT THEY ARE USING JUST K = 1

for k=1:mm          % cycle on elements
    m = mesh.P(k,1);     % number of vertexes of element 
    X = mesh.V(mesh.P(k,2:m+1),1:2);     % build X with vertex coordinates
    Sl=LocS(X);            % build local stiffness matrix    
    vertvec = mesh.P(k,2:m+1);      % vector of vertex index numbers    
    S(vertvec,vertvec) = S(vertvec,vertvec) + Sl ;    
end 

