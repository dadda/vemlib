function [ Slocs, row_dofs_for_Slocs, col_dofs_for_Slocs, rhs_locs, dofs_for_rhs_locs, P0s_by_ele, ...
           verDofs, edgeInternalDofs, edgePerm, eleDofs, eleInternalDofs, ...
           hE, areas, xb, yb, xqd, yqd, wqd, basis_on_qd ] = ...
    assemble_local_matrices_and_rhs_parallel(k, eoa, compressquad, mesh, formula1d, basis_type, ...
                                             use_mp, i_F, loadcal, rho, dofsLayout)
% Parallel assemblage of local stiffness matrices and rhs.
%   [ SLOCS, ROW_DOFS_FOR_SLOCS, COL_DOFS_FOR_SLOCS, RHS_LOCS, DOFS_FOR_RHS_LOCS, P0S_BY_ELE, ...
%     MESH, XQD, YQD, WQD, BASIS_ON_QD, BASISL2NORM] = ...
%       ASSEMBLE_LOCAL_MATRICES_AND_RHS_PARALLEL(K, EOA, MESH, FORMULA1D, BASIS_TYPE, USE_MP, I_F)
%   computes all the local stiffness matrices and right hand sides for the
%   VEM for the Poisson equation. L^2 norms of the original monomial basis
%   functions are returned.
%
%   Inputs:
%
%   - K : degree of the VEM space.
%
%   - EOA : extra order of accuracy for 1D and 2D quadrature formulas.
%
%   - COMPRESSQUAD : compress quadrature rule to have a rule with less
%            nodes/weights (equal to the dimension of the desidered polynomial
%            space) but the same algebraic degree of exactness.
%
%   - MESH : extended mesh data structure (see loadmesh.m for a description
%            of this data structure).
%
%   - FORMULA1D : barycentric coordinates (FORMULA1D(:,1), FORMULA1D(:,2))
%                 and weights (FORMULA1D(:,3)) of the Gauss-Lobatto
%                 quadrature rule of order K+1.
%
%   - BASIS_TYPE : type of monomial basis functions to be used. BASIS_TYPE=1
%        for the monomial basis functions defined in the Hitchhicker's paper. 
%        BASIS_TYPE=2 for the Hitchhicker's basis functions scaled by their
%        L^2 norm.
%
%   - USE_MP : flag telling whether the Multiprecision Toolbox
%              (www.advanpix.com) should be used (USE_MP=1) or not
%              (USE_MP=0).
%
%   - I_F : flag for choosing type of load term.
%           * I_F = 1, compute exact load term
%           * I_F = 2, load term from the uniform distribution on [-1,1].
%
%   Outputs:
%
%   - SLOCS : 1D column vector collecting all the local stiffness matrices
%         one after the other.
%
%   - ROW_DOFS_FOR_SLOCS, COL_DOFS_FOR_SLOCS : 1D column vectors that
%         contain row and column degrees of freedom corresponding to local
%         stiffness matrices. These be used to generate the global
%         stiffness matrix with the command:
%
%         S = sparse(ROW_DOFS_FOR_SLOCS, COL_DOFS_FOR_SLOCS, SLOCS);
%
%   - RHS_LOCS : 1D column vector collecting all the local right hand sides
%         one after the other.
%
%   - DOFS_FOR_RHS_LOCS : 1D column vector that contain all the degrees of
%         freedom corresponding to the local right hand sides. It can be
%         used together with RHS_LOCS to assemble the global right hand
%         with the command:
%
%         F = sparse(DOFS_FOR_RHS_LOCS, ones([length(DOFS_FOR_RHS_LOCS) 1]), RHS_LOCS);
%
%         Remark: using accumarray instead of sparse for assembling F would
%         be more compact, but, unfortunately, the Multiprecision Toolbox
%         does not implement accumarray as of 2016/08/13.
%
%   - P0S_BY_ELE : [Nelt x 1] cell of [d2 x Ndof_loc] matrices representing
%         the L^2 projection of the basis functions, P0s, for each element
%         of the mesh, where Nelt = # of elements in the mesh, 
%         d2 = dim(P^k), Ndof_loc = # of d.o.f.s of the local VEM space).
%
%   - HE : [1 x Nelt] array with the elements diameters.
%
%   - AREAS : [1 x Nelt] array with the elements areas.
%
%   - XB, YB : [1 x Nelt] arrays with the coordinates of the elements
%              barycenters.
%
%   - XQD, YQD, WQD : [Nelt x 1] cells of [Nqd_loc x 1] (not Nqd_max !!)
%         column arrays containing quadrature nodes and weights of cubature
%         rules for each polygon.
%
%   - BASIS_ON_QD : [Nelt x 1] cell of matrices containing evaluation of
%         monomial basis functions on internal quadrature nodes for each
%         element obtained by rescaling the original functions proposed in
%         the Hitchhicker's guide by their L^2 norm.
%
%   - BASISL2NORM: [Nelt x d2] array, where the E-th row contains the L^2
%         norms of the original monomial basis functions on the element E.
%
%   Degrees of freedom of a function v_h in the global virtual element space of degree k
%   are organized as follows:
%   * the value of v_h at all the vertices of the mesh;
%   * the value of v_h at the k-1 internal points of the (k+1)-point
%     Gauss-Lobatto quadrature rule on the first edge, the second edge,
%     and so on.
%   * the moments up to order k-2 of v_h in the first element, the second
%     element, and so on.
%

% Check input/output consistency
if nargout == 20 && basis_type == 1
    error(['L^2 norms of monomial basis functions are computed only if scaled monomial ', ...
           'basis functions are used'])
end

d2 = ( (k+1)*(k+2) ) / 2;

Slocs              = cell(mesh.Nelt,1);
row_dofs_for_Slocs = cell(mesh.Nelt,1);
col_dofs_for_Slocs = cell(mesh.Nelt,1);
rhs_locs           = cell(mesh.Nelt,1);
dofs_for_rhs_locs  = cell(mesh.Nelt,1);
P0s_by_ele         = cell(mesh.Nelt,1);

xqd         = cell(mesh.Nelt,1);
yqd         = cell(mesh.Nelt,1);
wqd         = cell(mesh.Nelt,1);
basis_on_qd = cell(mesh.Nelt,1);

coordinates = mesh.coordinates;
elements    = mesh.elements;
hE          = zeros(1,mesh.Nelt);
areas       = zeros(1,mesh.Nelt);
xb          = zeros(1,mesh.Nelt);
yb          = zeros(1,mesh.Nelt);
Nver        = mesh.Nver;
Nfc         = mesh.Nfc;

% Clearly, degrees of freedom of local VEM spaces depend on the degree of
% the spaces, k. For performance purposes, the degree of the VEM spaces is
% checked outside the for loops. This, of course, causes some code
% replication.
% Observe also that, in the cases k = 1, 2, the L^2 projector P0s coincides
% with the P-Nabla projector, PNs. This is important when assembling the
% local right hand sides.

if k == 1
    verDofs          = 1:mesh.Nver;
    edgeInternalDofs = [];
    edgePerm         = [];
    eleDofs          = cell(mesh.Nelt,1);
    eleInternalDofs  = [];

    parfor ( E = 1:mesh.Nelt, getParforArg() )
%     for E = 1:mesh.Nelt

        NedLoc     = elements(E,1);
        eleDofs{E} = elements(E,2:NedLoc+1);

        % Compute coordinates of the local boundary and internal degrees of
        % freedom. Also, find the area and the coordinates of the local
        % barycenter.
        [ xbnd_E, ybnd_E, xqd{E}, yqd{E}, wqd{E}, Nx_E, Ny_E, ...
          hE(E), areas(E), xb(E), yb(E) ] = build_coordinate_matrices_parallel( k, eoa, compressquad, ...
                elements(E,:), coordinates, formula1d, use_mp );

        % Evaluate monomial basis on boundary and internal nodes
        basis_on_qd{E} = monomial_basis(xqd{E}, yqd{E}, k, ...
            xb(E), yb(E), hE(E), ones(size(xqd{E})));
        basis_on_qd{E} = permute(basis_on_qd{E}, [1,3,2]);
      
        basis_on_bnd = monomial_basis(xbnd_E, ybnd_E, k, ...
            xb(E), yb(E), hE(E), ones(size(xbnd_E)));
        basis_on_bnd = permute(basis_on_bnd, [1,3,2]);

        % Evaluate load term

        f = []; % Initialization to prevent parfor from issuing a warning about f

        switch i_F
            case 1
                f = loadcal(xqd{E}, yqd{E}, use_mp);
            case 2
                if use_mp
                    f = -1 + 2*rand( size(xqd{E}), 'mp' );
                else
                    f = -1 + 2*rand( size(xqd{E}) );
                end
        end

        if basis_type == 2
            % L2 norm of P^1 basis functions
            basisL2norm = sqrt( wqd{E}' * basis_on_qd{E}.^2 );

            % Normalize basis functions
            basis_on_qd{E} = bsxfun(@times, 1./basisL2norm, basis_on_qd{E});
            basis_on_bnd   = bsxfun(@times, 1./basisL2norm, basis_on_bnd);

            % Assemble local stiffness matrix and right hand side
            [ Slocs{E}, row_dofs_for_Slocs{E}, col_dofs_for_Slocs{E}, ...
              rhs_locs{E}, dofs_for_rhs_locs{E}, P0s_by_ele{E} ] = ...
                local_assembler_k1(elements(E,:), hE(E), Nx_E, Ny_E, ...
                                   wqd{E}, basis_on_qd{E}, basis_on_bnd, f, basisL2norm);
        else
            % Assemble local stiffness matrix and right hand side
            [ Slocs{E}, row_dofs_for_Slocs{E}, col_dofs_for_Slocs{E}, ...
              rhs_locs{E}, dofs_for_rhs_locs{E}, P0s_by_ele{E} ] = ...
                local_assembler_k1(elements(E,:), hE(E), Nx_E, Ny_E, ...
                                   wqd{E}, basis_on_qd{E}, basis_on_bnd, f);                               
        end
        if ~isempty(rho)
            Slocs{E} = Slocs{E} * rho(mesh.subdomainId(E));
            if i_F == 1
                rhs_locs{E} = rhs_locs{E} * rho(mesh.subdomainId(E));
            end
        end
    end
elseif k == 2
    edgebyele = mesh.edgebyele;
    [ verDofs, edgeInternalDofs, edgePerm, eleDofs, eleInternalDofs ] = dofsByEle( mesh, k );

    parfor ( E = 1:mesh.Nelt, getParforArg() )
%     for E = 1:mesh.Nelt
        % Compute coordinates of the local boundary and internal degrees of
        % freedom. Also, find the area and the coordinates of the local
        % barycenter.
        [ xbnd_E, ybnd_E, xqd{E}, yqd{E}, wqd{E}, Nx_E, Ny_E, ...
          hE(E), areas(E), xb(E), yb(E) ] = build_coordinate_matrices_parallel( k, eoa, compressquad, ...
                elements(E,:), coordinates, formula1d, use_mp );
            
        % Evaluate monomial basis on boundary and internal nodes
        basis_on_qd{E} = monomial_basis(xqd{E}, yqd{E}, k, ...
            xb(E), yb(E), hE(E), ones(size(xqd{E})));
        basis_on_qd{E} = permute(basis_on_qd{E}, [1,3,2]);

        [basis_on_bnd, grad_basis_on_bnd] = monomial_basis(xbnd_E, ybnd_E, k, ...
            xb(E), yb(E), hE(E), ones(size(xbnd_E)));

        basis_on_bnd = permute(basis_on_bnd, [1,3,2]);
        grad_basis_on_bnd = permute(grad_basis_on_bnd, [1,3,4,2]);

        % Evaluate load term

        f = []; % Initialization to prevent parfor from issuing a warning about f

        switch i_F
            case 1
                f = loadcal(xqd{E}, yqd{E}, use_mp);
            case 2
                if use_mp
                    f = -1 + 2*rand( size(xqd{E}), 'mp' );
                else
                    f = -1 + 2*rand( size(xqd{E}) );
                end
        end

        if basis_type == 2
            % L2 norm of P^2 basis functions
            basisL2norm = sqrt( wqd{E}' * basis_on_qd{E}.^2 );

            % Normalize basis functions
            basis_on_qd{E}    = bsxfun(@times, 1./basisL2norm, basis_on_qd{E});
            basis_on_bnd      = bsxfun(@times, 1./basisL2norm, basis_on_bnd);
            grad_basis_on_bnd = bsxfun(@times, 1./basisL2norm, grad_basis_on_bnd);

            % Assemble local stiffness matrix and right hand side
            [ Slocs{E}, row_dofs_for_Slocs{E}, col_dofs_for_Slocs{E}, ...
              rhs_locs{E}, dofs_for_rhs_locs{E}, P0s_by_ele{E} ] = ...
                local_assembler_k2(elements(E,:), edgebyele(E,:), hE(E), areas(E), Nx_E, Ny_E, ...
                                   wqd{E}, basis_on_qd{E}, basis_on_bnd, grad_basis_on_bnd, ...
                                   f, formula1d, Nver, Nfc, E, eleDofs{E}, dofsLayout, basisL2norm);
        else
            % Assemble local stiffness matrix and right hand side            
            [ Slocs{E}, row_dofs_for_Slocs{E}, col_dofs_for_Slocs{E}, ...
              rhs_locs{E}, dofs_for_rhs_locs{E}, P0s_by_ele{E} ] = ...
                local_assembler_k2(elements(E,:), edgebyele(E,:), hE(E), areas(E), Nx_E, Ny_E, ...
                                   wqd{E}, basis_on_qd{E}, basis_on_bnd, grad_basis_on_bnd, ...
                                   f, formula1d, Nver, Nfc, E, eleDofs{E}, dofsLayout);
        end
        if ~isempty(rho)
            Slocs{E} = Slocs{E} * rho(mesh.subdomainId(E));
            if i_F == 1
                rhs_locs{E} = rhs_locs{E} * rho(mesh.subdomainId(E));
            end
        end
    end
else
    edgebyele = mesh.edgebyele;
    perm      = mesh.perm;
    [ verDofs, edgeInternalDofs, edgePerm, eleDofs, eleInternalDofs ] = dofsByEle( mesh, k );

    parfor ( E = 1:mesh.Nelt, getParforArg() )
%     for E = 1:mesh.Nelt
        % Compute coordinates of the local boundary and internal degrees of
        % freedom. Also, find the area and the coordinates of the local
        % barycenter.
        [ xbnd_E, ybnd_E, xqd{E}, yqd{E}, wqd{E}, Nx_E, Ny_E, ...
          hE(E), areas(E), xb(E), yb(E) ] = build_coordinate_matrices_parallel( k, eoa, compressquad, ...
                elements(E,:), coordinates, formula1d, use_mp );
            
        % Evaluate monomial basis on boundary and internal nodes
        basis_on_qd{E} = monomial_basis(xqd{E}, yqd{E}, k, xb(E), yb(E), hE(E), ones(size(xqd{E})));
        basis_on_qd{E} = permute(basis_on_qd{E}, [1,3,2]);
        
        [basis_on_bnd, grad_basis_on_bnd] = monomial_basis(xbnd_E, ybnd_E, k, ...
            xb(E), yb(E), hE(E), ones(size(xbnd_E)));
        basis_on_bnd = permute(basis_on_bnd, [1,3,2]);
        grad_basis_on_bnd = permute(grad_basis_on_bnd, [1,3,4,2]);

        % Evaluate load term

        f = []; % Initialization to prevent parfor from issuing a warning about f

        switch i_F
            case 1
                f = loadcal(xqd{E}, yqd{E}, use_mp);
            case 2
                if use_mp
                    f = -1 + 2*rand( size(xqd{E}), 'mp' );
                else
                    f = -1 + 2*rand( size(xqd{E}) );
                end
        end
        
        if basis_type == 2
        
            % L2 norm of P^k basis functions
            basisL2norm = sqrt( wqd{E}' * basis_on_qd{E}.^2 );

            % Normalize basis functions
            basis_on_qd{E}    = bsxfun(@times, 1./basisL2norm, basis_on_qd{E});
            basis_on_bnd      = bsxfun(@times, 1./basisL2norm, basis_on_bnd);
            grad_basis_on_bnd = bsxfun(@times, 1./basisL2norm, grad_basis_on_bnd);

            % Assemble local stiffness matrix and right hand side
            [ Slocs{E}, row_dofs_for_Slocs{E}, col_dofs_for_Slocs{E}, ...
              rhs_locs{E}, dofs_for_rhs_locs{E}, P0s_by_ele{E} ] = ...
                local_assembler_k(k, elements(E,:), edgebyele(E,:), perm(E,:), hE(E), areas(E), Nx_E, Ny_E, ...
                                  wqd{E}, basis_on_qd{E}, basis_on_bnd, grad_basis_on_bnd, ...
                                  f, formula1d, Nver, Nfc, E, eleDofs{E}, dofsLayout, basisL2norm);
        else
            % Assemble local stiffness matrix and right hand side
            [ Slocs{E}, row_dofs_for_Slocs{E}, col_dofs_for_Slocs{E}, ...
              rhs_locs{E}, dofs_for_rhs_locs{E}, P0s_by_ele{E} ] = ...
                local_assembler_k(k, elements(E,:), edgebyele(E,:), perm(E,:), hE(E), areas(E), Nx_E, Ny_E, ...
                                  wqd{E}, basis_on_qd{E}, basis_on_bnd, grad_basis_on_bnd, ...
                                  f, formula1d,  Nver, Nfc, E, eleDofs{E}, dofsLayout);
            
        end
        if ~isempty(rho)
            Slocs{E} = Slocs{E} * rho(mesh.subdomainId(E));
            if i_F == 1
                rhs_locs{E} = rhs_locs{E} * rho(mesh.subdomainId(E));
            end
        end
    end
end

Slocs              = vertcat(Slocs{:});
row_dofs_for_Slocs = vertcat(row_dofs_for_Slocs{:});
col_dofs_for_Slocs = vertcat(col_dofs_for_Slocs{:});
rhs_locs           = vertcat(rhs_locs{:});
dofs_for_rhs_locs  = vertcat(dofs_for_rhs_locs{:});

end
