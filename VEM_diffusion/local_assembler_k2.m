function [ S_E, row_dofs_for_S_E, col_dofs_for_S_E, rhs_E, dofs_for_rhs_E, P0s_by_ele_E, PNs ] = ...
    local_assembler_k2(elements_E, edgebyele_E, h_E, area_E, Nx_E, Ny_E, wqd_E, basis_on_qd_E, ...
                       basis_on_bnd_E, grad_basis_on_bnd_E, f_E, formula1d, ...
                       Nver, Nfc, E, eleDofs, dofsLayout, varargin)
% Computation of the local stiffness matrix and right hand side for k = 2.
%    [ S_E, ROW_DOFS_FOR_S_E, COL_DOFS_FOR_S_E, RHS_E, DOFS_FOR_RHS_E, P0_BY_ELE_E ] = ...
%      LOCAL_ASSEMBLER_K2(ELEMENTS_E, H_E, AREA_E, NX_E, NY_E, WQD_E, BASIS_ON_QD_E, ...
%                         BASIS_ON_BND_E, GRAD_BASIS_ON_BND_E, F_E, FORMULA1D, NVER, NFC, E)
%    computes local stiffness matrix, rhs, and L^2 projection of VEM basis
%    functions for a VEM space of degree k = 2.
%    [ S_E, ROW_DOFS_FOR_S_E, COL_DOFS_FOR_S_E, RHS_E, DOFS_FOR_RHS_E, P0_BY_ELE_E ] = ...
%      LOCAL_ASSEMBLER_K2(ELEMENTS_E, H_E, AREA_E, NX_E, NY_E, WQD_E, BASIS_ON_QD_E, ...
%                         BASIS_ON_BND_E, GRAD_BASIS_ON_BND_E, F_E, FORMULA1D, NVER, NFC, E, BASISL2NORMS_E)
%    computes local stiffness matrix, rhs, and L^2 projection of VEM basis
%    functions for a VEM space of degree k = 2.
%
%    Inputs:
%
%   - ELEMENTS_E : a [1 x Nver_max+1] vector containing, in order: the number of
%       vertices of element E, the indices of its vertices in counterclockwise
%       order, followed by as many zeros as needed to have a vector of
%       length Nver_max+1, where Nver_max is the maximum number of vertices
%       for a polygon in the mesh.
%
%   - H_E , AREA_E: diameter and area of element E.
%
%   - NX_E, NY_E : [Nver_loc+1 x 1] arrays with the x and y components,
%       respectively, of the outward normal vectors for element E,
%       normalized so that the euclidean nor of each normal equals the
%       length of the corresponding edge. NX_E(1), NY_E(1) contain the
%       normal to the last edge of E, whereas NX_E(2:end), NY_E(2:end)
%       contain normal vectors from the first edge to the last one.
%       Nver_loc is the number of vertices (or edges) of element E.
%
%   - WQD_E : [Nqd_loc x 1] array with internal quadrature weights (WQD_E)
%       on polygon E.
%
%   - BASIS_ON_QD_E : [Nqd_loc x d2] matrix containing evaluation of
%       monomial basis functions on internal quadrature nodes for each
%       element, with d2 = dim(P^k(E)).
%       
%   - BASIS_ON_BND_E : [2*Nver_loc x d2] matrix containing evaluation of
%       monomial basis functions at the vertices and the Nver_loc Gauss-Lobatto
%       quadrature nodes on the edges of element E (boundary nodes).
%
%   - GRAD_BASIS_ON_BND_E : [2*Nver_loc x d2 x 2] matrix containing evaluation of
%       the gradient of monomial basis functions at the boundary nodes element E.
%       * GRAD_BASIS_ON_BND_E(:,:,1): \partial_x.
%       * GRAD_BASIS_ON_BND_E(:,:,2): \partial_y.
%
%   - F_E : [Nqd_loc x 1] array with evaluation of the loading function on
%       the internal quadrature nodes of element E.
%
%   - FORMULA1D : barycentric coordinates (FORMULA1D(:,1), FORMULA1D(:,2))
%       and weights (FORMULA1D(:,3)) of the Gauss-Lobatto
%       quadrature rule of order k+1.
%
%   - NVER : total number of vertices of the mesh.
%
%   - NFC : total number of edges of the mesh.
%
%   - E : index of the current element.
%
%   - BASISL2NORMS_E : [1 x d2] array containing the L^2
%       norms of the original monomial basis functions on the element E.
%
%   Outputs:
%
%   - S_E : [Ndofs_loc^2 x 1] vector containing the local stiffness matrix.
%         Ndofs_loc = Nver_loc is the # of dofs of the local VEM space for
%         k = 1.
%
%   - ROW_DOFS_FOR_S_E, COL_DOFS_FOR_S_E : [Ndofs_loc^2 x 1] vectors that
%         contain row and column degrees of freedom corresponding to the local
%         stiffness matrix.
%
%   - RHS_E : [Ndofs_loc x 1] vector containing the local right hand side.
%
%   - DOFS_FOR_RHS_E : [Ndofs_loc x 1] vector that contains the degrees of
%         freedom corresponding to the local right hand side.
%
%   - P0S_BY_ELE_E : [d2 x Ndofs_loc] matrix containing
%         the L^2 projection of the local VEM basis functions in terms of
%         the local monomial basis.
%

Nver_loc = elements_E(1);

% Dimension of P^k
d2 = 6;

D = [ basis_on_bnd_E;
      (wqd_E'*basis_on_qd_E)/area_E ];

B = zeros(d2, 2*Nver_loc+1);

B(1, end) = 1.;

dab = llcmb(2, h_E);

if ~isempty(varargin)
    % Use scaled monomial basis functions (varargin{1} = basisL2norms_E)    
    B(2:end, end) = - area_E * (dab(2:end) ./ varargin{1}(2:end)');
else
    B(2:end, end) = - area_E * dab(2:end);
end

B(2:end, 1:Nver_loc) = ...
    ( bsxfun(@times, Nx_E(1:end-1)+Nx_E(2:end), grad_basis_on_bnd_E(1:Nver_loc,2:end,1)) + ...
      bsxfun(@times, Ny_E(1:end-1)+Ny_E(2:end), grad_basis_on_bnd_E(1:Nver_loc,2:end,2)) )'*formula1d(1,3);

B(2:end, Nver_loc+1:2*Nver_loc) = ...
    ( bsxfun(@times, Nx_E(2:end), grad_basis_on_bnd_E(Nver_loc+1:end,2:end,1)) + ...
      bsxfun(@times, Ny_E(2:end), grad_basis_on_bnd_E(Nver_loc+1:end,2:end,2)) )'*formula1d(2,3);

G   = B*D;
G(2:end,2:end) = (G(2:end,2:end) + G(2:end,2:end)') / 2;
G(2:end,1) = 0;
PNs = G\B;
PN  = D*PNs;

Gt      = G;
Gt(1,:) = 0.;

S_E = PNs'*Gt*PNs + (eye(2*Nver_loc+1) - PN)'*(eye(2*Nver_loc+1) - PN);
S_E = (S_E + S_E')/2;
S_E = S_E(:);

% For k = 2, the L^2 projector coincides with the Pi-Nabla projector
P0s_by_ele_E = PNs;

rhs_E = PNs'*(basis_on_qd_E'*(wqd_E.*f_E));

if dofsLayout == 1
    dofs_for_rhs_E = [elements_E(2:Nver_loc+1)';
                      Nver + edgebyele_E(1:Nver_loc)';
                      Nver + Nfc + E];
else
    dofs_for_rhs_E = eleDofs(:);
end

tmp = repmat(dofs_for_rhs_E, 1, 2*Nver_loc+1);
row_dofs_for_S_E = tmp(:);
tmp = tmp';
col_dofs_for_S_E = tmp(:);

end
