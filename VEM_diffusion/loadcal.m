function out = loadcal(x, y, use_mp)
% loading function, f = -\Delta u

% out = zeros(size(x));
% out = -ones(size(x));
% out = -(x+y);

% Cinf, homogeneous
if use_mp
    mypi = mp('pi');
else
    mypi = pi;
end
n   = 4;
out = sin(n*mypi*x) .* sin(n*mypi*y);

% % Cinf, non homogeneous
% if use_mp
%     mypi = mp('pi');
% else
%     mypi = pi;
% end
% n   = 1;
% out = cos(n*mypi*x) .* cos(n*mypi*y);

end
