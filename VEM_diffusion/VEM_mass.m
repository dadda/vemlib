function [uu,errene,errinf]=VEM_mass(mesh);

% -----------------------------------------
%
%     USED ONLY AS A DEBUGGER !!!!!
%
% -----------------------------------------

% function [uu]=VEM(mesh);
% ------------
% solves the diffusion problem with nodal VEM (GENERAL MESHES)
% mesh has data for mesh, as described in meshstruct.m file
% the load is given as a function "load" from the domain into Reals^2
% the non-hom dirichlet BCs are given through function "dirdata"
% ------------  
% USES DIRICHLET BOUNDARY CONDITIONS (NON HOMOGENEOUS)
% ------------

addpath(['./../mesh_handlers']);
addpath(['./../quadrature']);

[S,M]=GlobSM(mesh); % stiffness matrix  

ss = size(mesh.V,1);   % number of nodes   
mm = size(mesh.P,1);   % number of elements
%
% --- load term construction ---
f=zeros(ss,1);
for k=1:mm          % cicle on elements
    m = mesh.P(k,1);     % number of vertexes of element 
    X = mesh.V(mesh.P(k,2:m+1),1:2);     % build X with vertex coordinates    
    [E,BB,w]=baric(X);  % calculate area E, baricenter BB, weight vector w 
    % (w vector of weights associated to each ordered vertex 1,2,3....)        
    % ---- build global load vector --------
    for j=1:size(X,1)
        load = loadcal(mesh.V(mesh.P(k,j+1),1:2));   
        f(mesh.P(k,j+1)) = ...
        f(mesh.P(k,j+1)) + w(j)*load;  
    end           
    % ---------------    
end
% --- option for Dirichlet boundary conditions ---
[E]=mesh.E;
[V]=mesh.B;

%----------
uu = M\f;     % resolution
%----------

%-----------------------------------
% ERROR in discrete energy norm
%-----------------------------------
for k=1:ss
    sol(k)=solcal(mesh.V(k,1:2));
end
errene = sqrt( (uu'-sol)*M*(uu-sol')  / (sol*M*sol') )   


%---------------------------------
%  ERROR IN L-INFINITY
%---------------------------------
errinf = max(abs(sol'-uu))/max(abs(sol'))

%---------------------------------
% Option for a rough point-plot
%---------------------------------
% X=mesh.V(:,1);
% Y=mesh.V(:,2);    
% plot3(X,Y,uu,'*') 









    