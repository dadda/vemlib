%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute global stiffness matrix S

addpath('./../Basis_Functions');
addpath('./../Mesh_Handlers');
addpath('./../Quadrature');
addpath('./../VEM_diffusion');

k = input('Polynomial degree (>= 1): ');
eoa = input('Extra order of accuracy for quadrature formulas: ');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load one of the meshes by A.Russo

% meshroot = './../Mesh_Handlers/meshfiles_AleRusso';
% % meshname = 'esagoni-regolari/esagoni8x10';
% meshname = 'esagoni-deformati/esagoni26x30d';
% meshname = [meshroot '/' meshname];
% 
% fprintf('Loading mesh %s...\n', meshname);
% mesh = loadmesh(meshname, 0);
% fprintf('...mesh %s loaded\n', meshname);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create either the square or the pentagon mesh described in section 4 of
% Hitchhiker's paper

% Unit square
mesh.Nver        = 4;
mesh.coordinates = [ 0 0; 1. 0; 1. 1.; 0 1. ];
mesh.Nelt        = 1;
mesh.Nver_max    = 4;
mesh.iNver_max   = 1;
mesh.elements    = [ 4 1 2 3 4 ];
mesh.edges       = [ 1 2 1 ; 2 3 1 ; 3 4 1 ; 4 1 1 ];
mesh.Nfc         = 4;
mesh.dirfaces    = (1:4)';
mesh.B           = (1:4)';
mesh.edgebyele   = 1:4;
mesh.perm        = [1 1 1 1];

% % Pentagon
% mesh.Nver        = 5;
% mesh.coordinates = [0, 0; 3, 0; 3, 2; 1.5, 4; 0, 4];
% mesh.Nelt        = 1;
% mesh.Nver_max    = 5;
% mesh.iNver_max   = 1;
% mesh.elements    = [5 1 2 3 4 5];
% mesh.edges       = [ 1 2 1 ; 2 3 1 ; 3 4 1 ; 4 5 1 ; 5 1 1 ];
% mesh.Nfc         = 5;
% mesh.dirfaces    = (1:5)';
% mesh.B           = (1:5)';
% mesh.edgebyele   = 1:5;
% mesh.perm        = [1 1 1 1 1];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[ mesh, xbnd, ybnd, Xi_bnd, ...
    xqd, yqd, wqd, Xi_qd, ...
    Nx, Ny, formula1d ] = build_coordinate_matrices_sequential( mesh, k, eoa, 0 );

% Assemble local matrices and right hand sides
[K, row_dofs_for_K, col_dofs_for_K] = ...
    assemble_local_matrices_and_rhs_sequential(k, mesh, ...
                            xbnd, ybnd, Xi_bnd, ...
                            xqd, yqd, wqd, Xi_qd, ...
                            Nx, Ny, ...
                            formula1d, 0);

% Assemble global stiffness matrix
S = sparse(row_dofs_for_K, col_dofs_for_K, K);





