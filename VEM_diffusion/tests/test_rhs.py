from sympy import *

x, y = symbols('x y')
f = sin(pi*x)*sin(pi*y)

g = -0.5*(x+y)
i1 = integrate(integrate(f*g, (y, 0, 1)), (x, 0, 1))
g = 0.75
i2 = integrate(integrate(f*g, (y, 0, 1)), (x, 0, 1))
print i1+i2

g = 0.5*(x-y)
i1 = integrate(integrate(f*g, (y, 0, 1)), (x, 0, 1))
g = 0.25
i2 = integrate(integrate(f*g, (y, 0, 1)), (x, 0, 1))
print i1+i2

g = 0.5*(x+y)
i1 = integrate(integrate(f*g, (y, 0, 1)), (x, 0, 1))
print i1

g = 0.5*(-x+y)
i1 = integrate(integrate(f*g, (y, 0, 1)), (x, 0, 1))
g = 0.25
i2 = integrate(integrate(f*g, (y, 0, 1)), (x, 0, 1))
print i1+i2
