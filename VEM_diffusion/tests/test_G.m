%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check the correctness of the VEM code by comparing matrices G obtained
% using either equation (3.10) or (3.19).

addpath('./../../Basis_Functions');
addpath('./../../Mesh_Handlers');
addpath('./../../Quadrature');
addpath('./../');

k = input('Polynomial degree (>= 1): ');
eoa = input('Extra order of accuracy for quadrature formulas: ');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load one of the meshes by A.Russo

% meshroot = './../Mesh_Handlers/meshfiles_AleRusso';
% % meshname = 'esagoni-regolari/esagoni8x10';
% meshname = 'esagoni-deformati/esagoni26x30d';
% meshname = [meshroot '/' meshname];
% 
% fprintf('Loading mesh %s...\n', meshname);
% mesh = loadmesh(meshname, 0);
% fprintf('...mesh %s loaded\n', meshname);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create either the square or the pentagon mesh described in section 4 of
% Hitchhiker's paper

% Unit square
mesh.Nver        = 4;
mesh.coordinates = [ 0 0; 1. 0; 1. 1.; 0 1. ];
mesh.Nelt        = 1;
mesh.Nver_max    = 4;
mesh.iNver_max   = 1;
mesh.elements    = [ 4 1 2 3 4 ];
mesh.edges       = [ 1 2 1 ; 2 3 1 ; 3 4 1 ; 4 1 1 ];
mesh.Nfc         = 4;
mesh.dirfaces    = (1:4)';
mesh.B           = (1:4)';
mesh.edgebyele   = 1:4;
mesh.perm        = [1 1 1 1];

% % Pentagon
% mesh.Nver        = 5;
% mesh.coordinates = [0, 0; 3, 0; 3, 2; 1.5, 4; 0, 4];
% mesh.Nelt        = 1;
% mesh.Nver_max    = 5;
% mesh.iNver_max   = 1;
% mesh.elements    = [5 1 2 3 4 5];
% mesh.edges       = [ 1 2 1 ; 2 3 1 ; 3 4 1 ; 4 5 1 ; 5 1 1 ];
% mesh.Nfc         = 5;
% mesh.dirfaces    = (1:5)';
% mesh.B           = (1:5)';
% mesh.edgebyele   = 1:5;
% mesh.perm        = [1 1 1 1 1];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[ mesh, xbnd, ybnd, Xi_bnd, ...
    xqd, yqd, wqd, ...
    Nx, Ny, formula1d ] = build_coordinate_matrices_sequential( mesh, k, eoa, 0 );

[K, row_dofs_for_K, col_dofs_for_K, rhs, dofs_for_rhs, P0s_by_ele, basis_on_qd, B, D] = ...
    assemble_local_matrices_and_rhs_sequential(k, mesh, ...
                            xbnd, ybnd, Xi_bnd, ...
                            xqd, yqd, wqd, ...
                            Nx, Ny, ...
                            formula1d, 0);

% Compute G from B and D (equation 3.19)
d2   = ((k+1)*(k+2))/2;
d2m2 = ((k-1)*k)/2;
G1 = zeros([d2 d2 mesh.Nelt]);

for E = 1:mesh.Nelt
    select_dofs = [1:mesh.elements(E,1) mesh.Nver_max+1:mesh.Nver_max+(k-1)*mesh.elements(E,1) k*mesh.Nver_max+1:k*mesh.Nver_max+d2m2];
    G1(:,:,E) = B(:,select_dofs,E)*D(select_dofs,:,E);
end

% Compute G from equation 3.10
G2 = compute_G_from_first_definition(k, mesh);

errors = zeros(mesh.Nelt, 1);
for E = 1:mesh.Nelt
    errors(E) = norm(G1(:,:,E)-G2(:,:,E), Inf);
end

fprintf('The sup norm of the difference between the two matrices is: %f\n', max(errors));

H = zeros(d2, d2);
for i = 1:d2
    for j = 1:i-1
        H(i,j) = wqd' * (basis_on_qd(:,i).*basis_on_qd(:,j));
        H(j,i) = H(i,j);
    end
    H(i,i) = wqd' * (basis_on_qd(:,i).^2);
end

disp(H)