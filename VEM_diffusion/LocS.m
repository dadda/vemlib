function [Sl]=LocS(X)
% generates the local matrix for the diffusion problem (pure laplacian) 
% the input matrix X is m x 2 and describes the polygon (element) of m vertexes
% in each row, matrix X has x and y (coordinates) of the vertex, ordered
% anti clock-wise.
% K is the degree of the VEM space.
% FORMULA1D Gauss-Lobatto quadrature rule.
% - FORMULA1D(:,1): coordinates of GL quadrature points on [-1,1].
% - FORMULA1D(:,2): weights on [-1,1].
% this function creates matrix Sl (local stiffness matrix)
% -------

alf = 1; % factor for the "out of consistent" part
m   = size(X,1);  % number of vertexes

[~,BB]=baric_original(X); % calculate area E and baricenter BB of element

% -------
% BASIS for the VEM space is value at first
% vertex, then second vertex, etc ... 
% BASIS for the polynomial space is 
% {1,x,y} with coordinates x,y scaled with the baricenter

% ----- BUILD MATRIX D -----------

D=[ones(m,1),X - ones(m,1)*BB]; % These are the functions 'm'. Why aren't they scaled by the dimameter
                                % of the element?

% ----- BUILD MATRIX B ----------- (initially built as transpose)
B=zeros(m,3);    
for j=1:m     % add contribution of each edge
   % kernel part (first column) - see projection below 
   B(j,1) = (1/m); 
   % non kernel part (second and third columns)
   e = norm( X(j,:) - X(mod(j,m)+1,:) );     % edge length
   n = [ X(mod(j,m)+1,2) - X(j,2) , - X(mod(j,m)+1,1) + X(j,1) ];  % outward normal
   B(j,2:3) = B(j,2:3) + (1/2)*n;
   B(mod(j,m)+1,2:3) = B(mod(j,m)+1,2:3) + (1/2)*n; 
end
B=B'; % transpose 

% --- build projectors
G = B*D;          % G matrix from Hitchhikers paper
PNs = G\B;   % PiNabla star projector (polynomial basis)
PN = D*PNs;      % PiNabla projector (Vh basis)  
%  --- build local matrix ---
Gt(1,:)=zeros(1,3);   % matrix G tilde (null on the kernel)
Gt(2:3,:)=G(2:3,:);

M = PNs'*Gt*PNs;  % consistent part
Sl = M + alf*(trace(M)/2)*(eye(m)-PN)'*(eye(m)-PN); % why scaled by the trace?
                                                    % to get something of the same
                                                    % order of magnitude of M?


% -----------------------
% KERNEL PROJECTION
% -----------------------
% Let the vertex based scalar product 
% <v,w> = (1/m) sum_{i=1}^m v(vertex_i)\cdot w(vertex_i) .
% Then, the projector on the kernel is
% P(v) := <1,v>
% -----------------------