function out = dirdata(x, y, use_mp)
% Dirichlet boundary data (vector valued)
%

%  ------ AUTO DIRICHLET ------
out = exactsol(x, y, use_mp);
