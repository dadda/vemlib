function [areas, xb, yb] = baric(xqd, yqd, wqd)
%   [AREAS, XB, YB] = BARIC(XQD, YQD, WQD) computes areas and coordinates
%   of the centroid of a polygon given nodes (XQD,YQD) and weights (WQD) of
%   a quadrature rule over the polygon.
%
%   XQD, YQD, and WQD can be M x N matrices, with column j containing nodes
%   and weights for polygon j, j = 1, ..., N. In this case, AREAS, XB, and
%   YB are [1 x N] row vectors.

areas = sum(wqd, 1);
xb = sum(wqd.*xqd, 1) ./ areas;
yb = sum(wqd.*yqd, 1) ./ areas;

end