function sol = compute_VEM_sol_sequential( k, mesh, xqd, yqd, wqd, basis_on_qd, formula1d, use_mp )
% SOL = COMPUTE_VEM_SOL_SEQUENTIAL( K, MESH, XQD, YQD, WQD, BASIS_ON_QD, FORMULA1D, USE_MP )
% projects the solution on the global VEM space of degree k.
%
% Inputs:
%
% - K : degree of the VEM space.
%
% - MESH : extended mesh data structure (see loadmesh.m for a description
%       of this data structure).
%
% - XQD, YQD, WQD : [Nqd_max x Nelt] matrices representing
%       quadrature nodes (XQD, YQD), and weights (WQD) of the quadrature rules
%       on each polygon of the mesh. Nelt is the number of elements in the
%       mesh, and Nqd_max is the maximum number of internal cubature nodes over
%       all the polygons of the mesh.
%       Since mesh polygons could have different numbers of vertices from each other,
%       numbers of cubature nodes could differ. Hence, for
%       each column E = 1, ..., Nelt, only the following entries are
%       meaningful:
%       * XQD([1:Nqd_loc], E)
%       * similarly for the other matrices.
%
% - BASIS_ON_QD : [Nqd_max x Nelt x d2] matrix containing evaluation of
%       basis functions on internal cubature nodes for each element. For
%       each E = 1, ..., Nelt, entries BASIS_ON_QD(Nqd_loc+1:Nqd_max, E, :)
%       are meaningless.
%
% - FORMULA1D : barycentric coordinates (FORMULA1D(:,1), FORMULA1D(:,2))
%       and weights (FORMULA1D(:,3)) of the Gauss-Lobatto
%       quadrature rule of order K+1.
%
% - USE_MP : flag telling whether the Multiprecision Toolbox
%       (www.advanpix.com) should be used (USE_MP=1) or not
%       (USE_MP=0).
%
% Degrees of freedom of a function v_h in the global virtual element space of degree k
% are organized as follows:
% * the value of v_h at all the vertices of the mesh;
% * the value of v_h at the k-1 internal points of the (k+1)-point
%   Gauss-Lobatto quadrature rule on the first edge, the second edge,
%   and so on.
% * the moments up to order k-2 of v_h in the first element, the second
%   element, and so on.
%

d2m2 = (k*(k-1))/2;

if k == 1
    sol = exactsol(mesh.coordinates(:,1), mesh.coordinates(:,2), use_mp);
else
    x = mesh.coordinates(:,1); x = x(mesh.edges(:,1:2)');
    y = mesh.coordinates(:,2); y = y(mesh.edges(:,1:2)');

    x = formula1d(2:end-1,1:2)*x;
    x = [ mesh.coordinates(:,1); x(:) ];
    
    y = formula1d(2:end-1,1:2)*y;
    y = [ mesh.coordinates(:,2); y(:) ];

    sol = exactsol(x, y, use_mp);
    sol = [sol; reshape( ...
                    bsxfun(@times, 1./mesh.areas, ...
                        permute( ...
                            sum( ...
                                bsxfun(@times, wqd .* exactsol(xqd, yqd, use_mp), basis_on_qd(:,:,1:d2m2)), ...
                            1), ...
                        [3,2,1]) ...
                    ), ...
                [d2m2*mesh.Nelt 1]) ...
          ];
end

end

