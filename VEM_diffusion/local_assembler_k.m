function [ S_E, row_dofs_for_S_E, col_dofs_for_S_E, rhs_E, dofs_for_rhs_E, P0s_by_ele_E, PNs ] = ...
    local_assembler_k(k, elements_E, edgebyele_E, perm_E, h_E, area_E, Nx_E, Ny_E, wqd_E, basis_on_qd_E, ...
                       basis_on_bnd_E, grad_basis_on_bnd_E, f_E, formula1d, ...
                       Nver, Nfc, E, eleDofs, dofsLayout, varargin)
% Computation of the local stiffness matrix and right hand side for arbitrary k.
%    [ S_E, ROW_DOFS_FOR_S_E, COL_DOFS_FOR_S_E, RHS_E, DOFS_FOR_RHS_E, P0_BY_ELE_E ] = ...
%      LOCAL_ASSEMBLER_K(K, ELEMENTS_E, EDGEBYELE_E, PERM_E, H_E, AREA_E, ...
%                        NX_E, NY_E, WQD_E, BASIS_ON_QD_E, ...
%                        BASIS_ON_BND_E, GRAD_BASIS_ON_BND_E, F_E, FORMULA1D, NVER, NFC, E)
%    computes local stiffness matrix, rhs, and L^2 projection of VEM basis
%    functions for a VEM space of arbitrary degree k.
%    [ S_E, ROW_DOFS_FOR_S_E, COL_DOFS_FOR_S_E, RHS_E, DOFS_FOR_RHS_E, P0_BY_ELE_E ] = ...
%      LOCAL_ASSEMBLER_K(K, ELEMENTS_E, EDGEBYELE_E, PERM_E, H_E, AREA_E, ...
%                        NX_E, NY_E, WQD_E, BASIS_ON_QD_E, ...
%                        BASIS_ON_BND_E, GRAD_BASIS_ON_BND_E, F_E, FORMULA1D, NVER, NFC, E, BASISL2NORMS_E)
%    computes local stiffness matrix, rhs, and L^2 projection of VEM basis
%    functions for a VEM space of arbitrary degree k. Scaled Hitchhicker's monomial
%    basis functions are used.
%
%    Inputs:
%
%   - K : degree of the local VEM space.
%
%   - ELEMENTS_E : a [1 x Nver_max+1] vector containing, in order: the number of
%       vertices of element E, the indices of its vertices in counterclockwise
%       order, followed by as many zeros as needed to have a vector of
%       length Nver_max+1, where Nver_max is the maximum number of vertices
%       for a polygon in the mesh.
%
%   - EDGEBYELE_E: a [1 x Nver_max] vector which contains the number of
%       edges that make up \partial E, followed by as
%       many zeros as needed to have a vector of length Nver_max.
%
%   - PERM_E: a [1 x Nver_max] vector containing the permutations
%       needed (1 (no permutation) or 2 (switch the
%       order)) for the edges of element E to match their global orientation
%       provided by field EDGES in the extended mesh data structure.
%
%   - H_E , AREA_E: diameter and area of element E.
%
%   - NX_E, NY_E : [Nver_loc+1 x 1] arrays with the x and y components,
%       respectively, of the outward normal vectors for element E,
%       normalized so that the euclidean nor of each normal equals the
%       length of the corresponding edge. NX_E(1), NY_E(1) contain the
%       normal to the last edge of E, whereas NX_E(2:end), NY_E(2:end)
%       contain normal vectors from the first edge to the last one.
%       Nver_loc is the number of vertices (or edges) of element E.
%
%   - WQD_E : [Nqd_loc x 1] array with internal quadrature weights (WQD_E)
%       on polygon E.
%
%   - BASIS_ON_QD_E : [Nqd_loc x d2] matrix containing evaluation of
%       monomial basis functions on internal quadrature nodes for each
%       element, with d2 = dim(P^k(E)).
%       
%   - BASIS_ON_BND_E : [2*Nver_loc x d2] matrix containing evaluation of
%       monomial basis functions at the vertices and the Nver_loc Gauss-Lobatto
%       quadrature nodes on the edges of element E (boundary nodes).
%
%   - GRAD_BASIS_ON_BND_E : [2*Nver_loc x d2 x 2] matrix containing evaluation of
%       the gradient of monomial basis functions at the boundary nodes element E.
%       * GRAD_BASIS_ON_BND_E(:,:,1): \partial_x.
%       * GRAD_BASIS_ON_BND_E(:,:,2): \partial_y.
%
%   - F_E : [Nqd_loc x 1] array with evaluation of the loading function on
%       the internal quadrature nodes of element E.
%
%   - FORMULA1D : barycentric coordinates (FORMULA1D(:,1), FORMULA1D(:,2))
%       and weights (FORMULA1D(:,3)) of the Gauss-Lobatto
%       quadrature rule of order k+1.
%
%   - NVER : total number of vertices of the mesh.
%
%   - NFC : total number of edges of the mesh.
%
%   - E : index of the current element.
%
%   - BASISL2NORMS_E : [1 x d2] array containing the L^2
%       norms of the original monomial basis functions on the element E.
%
%   Outputs:
%
%   - S_E : [Ndofs_loc^2 x 1] vector containing the local stiffness matrix.
%         Ndofs_loc = Nver_loc is the # of dofs of the local VEM space for
%         k = 1.
%
%   - ROW_DOFS_FOR_S_E, COL_DOFS_FOR_S_E : [Ndofs_loc^2 x 1] vectors that
%         contain row and column degrees of freedom corresponding to the local
%         stiffness matrix.
%
%   - RHS_E : [Ndofs_loc x 1] vector containing the local right hand side.
%
%   - DOFS_FOR_RHS_E : [Ndofs_loc x 1] vector that contains the degrees of
%         freedom corresponding to the local right hand side.
%
%   - P0S_BY_ELE_E : [d2 x Ndofs_loc] matrix containing
%         the L^2 projection of the local VEM basis functions in terms of
%         the local monomial basis.
%

Nver_loc = elements_E(1);

% Dimension of P^k and P^{k-2}
d2   = ((k+1)*(k+2))/2;
d2m2 = ((k-1)*k)/2;

ndofs = k*Nver_loc+d2m2;

% Matrices for P0 projector
H = zeros(d2, d2);
C = zeros(d2, ndofs);

for i = 1:d2
    for j = 1:i-1
        H(i,j) = wqd_E' * (basis_on_qd_E(:,i).*basis_on_qd_E(:,j));
        H(j,i) = H(i,j);
    end
    H(i,i) = wqd_E' * (basis_on_qd_E(:,i).^2);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% P nabla projector and local stiffness matrix

B = zeros(d2, k*Nver_loc+d2m2);

%%% Matrix B - first row (reference: equations (2.2) and (3.4b))
B(1, k*Nver_loc+1) = 1.;

%%% Matrix B - bottom right block, corresponding to the first term in
% equation (3.12).

% Compute coefficients of the laplacians of basis functions as a linear
% combination of the basis functions themselves.
dab = llcmb(k, h_E);

if ~isempty(varargin)
    % Use scaled monomial basis functions (varargin{1} = basisL2norms_E)
    B(2:end, k*Nver_loc+1:end, :) = - area_E * bsxfun(@times, 1./varargin{1}(2:end)', dab(2:end,:) );
    D = [ basis_on_bnd_E;
          bsxfun(@times, varargin{1}(1:d2m2)', H(1:d2m2,:)/area_E ) ];

    % Update also matrix C for P0 projector (avoid extra if later on)
    C(1:d2m2, k*Nver_loc+1:end) = area_E * diag(1./varargin{1}(1:d2m2));    
else
    B(2:end, k*Nver_loc+1:end, :) = - area_E * dab(2:end,:);
    D = [ basis_on_bnd_E;
          H(1:d2m2,:)/area_E];    

    % Update also matrix C for P0 projector (avoid extra if later on)      
    C(1:d2m2, k*Nver_loc+1:end) = area_E * eye(d2m2);
end

%%% Matrix B - bottom left block, first Nver_max columns
    
% This part corresponds to the contribution of the vertices of an
% element to the second term in (3.12) (see notes).

B(2:end, 1:Nver_loc) = ...
    ( bsxfun(@times, Nx_E(1:end-1)+Nx_E(2:end), grad_basis_on_bnd_E(1:Nver_loc,2:end,1)) + ...
      bsxfun(@times, Ny_E(1:end-1)+Ny_E(2:end), grad_basis_on_bnd_E(1:Nver_loc,2:end,2)) )'*formula1d(1,3);

%%% Matrix B - bottom left block, columns from Nver_loc+1 to k*Nver_loc
    
% This part corresponds to the contribution of the internal
% Gauss-Lobatto quadrature points to the second term in (3.12)
% (see notes).
B(2:end, Nver_loc+1:k*Nver_loc) = ...
    ( bsxfun(@times, kron(Nx_E(2:end), formula1d(2:k,3)), ...
                     grad_basis_on_bnd_E(Nver_loc+1:end,2:end,1)) + ...
      bsxfun(@times, kron(Ny_E(2:end), formula1d(2:k,3)), ...
                     grad_basis_on_bnd_E(Nver_loc+1:end,2:end,2)) )';

G   = B*D;
G(2:end,2:end) = (G(2:end,2:end) + G(2:end,2:end)') / 2;
G(2:end,1) = 0;
PNs = G\B;
PN  = D*PNs;

Gt      = G;
Gt(1,:) = 0.;

S_E = PNs'*Gt*PNs + (eye(ndofs) - PN)'*(eye(ndofs) - PN);
S_E = (S_E + S_E')/2;
S_E = S_E(:);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% P0 (L^2) projector

C(d2m2+1:end, :) = H(d2m2+1:end,:)*PNs;
P0s = H\C;

P0s_by_ele_E = P0s;

% Equation (6.1) is used for the right hand side

rhs_E = P0s'*(basis_on_qd_E'*(wqd_E.*f_E));

if dofsLayout == 1
    % Important remark: the global orientation of an edge IS relevant in
    % computing local dofs in the case k > 2.

    face_dofs = bsxfun(@plus, (1:k-1)', ...
        Nver + (edgebyele_E(1:Nver_loc)-1)*(k-1));

    face_dofs(:, perm_E(1:Nver_loc) == 2) = ...
        flipud(face_dofs(:, perm_E(1:Nver_loc) == 2));

    dofs_for_rhs_E = ...
        [elements_E(2:Nver_loc+1)';
         ...
         face_dofs(:);
         ...
         Nver + (k-1)*Nfc + (E-1)*d2m2 + (1:d2m2)'];
else
    dofs_for_rhs_E = double(eleDofs(:));
end
 
tmp = repmat(dofs_for_rhs_E, 1, ndofs);
row_dofs_for_S_E = tmp(:);
tmp = tmp';
col_dofs_for_S_E = tmp(:);

end
