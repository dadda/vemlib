function [ uDir, diridofs ] = dirdata_minsurf(bc, mesh)

diridofs = sort(mesh.B);

switch bc
    case 0
        uDir = evaluateExactSol(0, mesh.coordinates(diridofs,1), mesh.coordinates(diridofs,2));
    case 1
        % Data on [0,1]^2: under proper rotations and
        % translations, it can be used to generate Schwarz D-surface.

        x = mesh.coordinates(:,1);
        x = x(mesh.edges(mesh.dirfaces,1:2)');

        y = mesh.coordinates(:,2);
        y = y(mesh.edges(mesh.dirfaces,1:2)');

        xN = y(2,:) - y(1,:); 
        yN = x(1,:) - x(2,:);
        norms = sqrt( xN.^2 + yN.^2 );
        xN = xN ./ norms;
        yN = yN ./ norms;

        dirfacesWest  = ( abs(xN) >= 0.5 ) & ( x(1,:) < 0.5 );
        dirfacesEast  = ( abs(xN) >= 0.5 ) & ( x(1,:) > 0.5 );
        dirfacesSouth = ( abs(yN) > 0.5 ) & ( y(1,:) < 0.5 );
        dirfacesNorth = ( abs(yN) > 0.5 ) & ( y(1,:) > 0.5 );

        verticesSidx = mesh.edges( mesh.dirfaces(dirfacesSouth), 1:2 );
        verticesSidx = unique(verticesSidx(:));
        dataS = zeros(length(verticesSidx),1);

        verticesEidx = mesh.edges( mesh.dirfaces(dirfacesEast), 1:2 );
        verticesEidx = unique(verticesEidx(:));
        dataE = mesh.coordinates(verticesEidx,2);

        verticesNidx = mesh.edges( mesh.dirfaces(dirfacesNorth), 1:2 );
        verticesNidx = unique(verticesNidx(:));
        dataN = mesh.coordinates(verticesNidx,1);

        verticesWidx = mesh.edges( mesh.dirfaces(dirfacesWest), 1:2 );
        verticesWidx = unique(verticesWidx(:));
        dataW = zeros(length(verticesWidx),1);

        uDir = [ dataS; dataE; dataN; dataW ];
        [~, ix]  = unique([verticesSidx; verticesEidx; verticesNidx; verticesWidx]);
        uDir = uDir(ix);
    case 2
        % Cantor function reflected on the four sides of [0,1]^2
        x = mesh.coordinates(:,1);
        x = x(mesh.edges(mesh.dirfaces,1:2)');

        y = mesh.coordinates(:,2);
        y = y(mesh.edges(mesh.dirfaces,1:2)');

        xN = y(2,:) - y(1,:);
        yN = x(1,:) - x(2,:);
        norms = sqrt( xN.^2 + yN.^2 );
        xN = xN ./ norms;
        yN = yN ./ norms;

        dirfacesWest  = ( abs(xN) >= 0.5 ) & ( x(1,:) < 0.5 );
        dirfacesEast  = ( abs(xN) >= 0.5 ) & ( x(1,:) > 0.5 );
        dirfacesSouth = ( abs(yN) > 0.5 ) & ( y(1,:) < 0.5 );
        dirfacesNorth = ( abs(yN) > 0.5 ) & ( y(1,:) > 0.5 );

        verticesSidx = mesh.edges( mesh.dirfaces(dirfacesSouth), 1:2 );
        verticesSidx = unique(verticesSidx(:));
        [ ~, idx ]   = sort( mesh.coordinates(verticesSidx,1) );
        verticesSidx = verticesSidx(idx);
        dataS        = cantorFunction(mesh.coordinates(verticesSidx,1), 0, 1, 0, 1, 0, 4);

        verticesEidx = mesh.edges( mesh.dirfaces(dirfacesEast), 1:2 );
        verticesEidx = unique(verticesEidx(:));
        [ ~, idx ]   = sort( mesh.coordinates(verticesEidx,2) );
        verticesEidx = verticesEidx(idx);
        dataE        = cantorFunction(mesh.coordinates(verticesEidx,2), 0, 1, 0, 1, 0, 4);
        dataE        = dataE(end:-1:1);

        verticesNidx = mesh.edges( mesh.dirfaces(dirfacesNorth), 1:2 );
        verticesNidx = unique(verticesNidx(:));
        [ ~, idx ]   = sort( mesh.coordinates(verticesNidx,1) );
        verticesNidx = verticesNidx(idx);
        dataN        = cantorFunction(mesh.coordinates(verticesNidx,1), 0, 1, 0, 1, 0, 4);
        dataN        = dataN(end:-1:1);

        verticesWidx = mesh.edges( mesh.dirfaces(dirfacesWest), 1:2 );
        verticesWidx = unique(verticesWidx(:));
        [ ~, idx ]   = sort( mesh.coordinates(verticesWidx,2) );
        verticesWidx = verticesWidx(idx);
        dataW        = cantorFunction(mesh.coordinates(verticesWidx,2), 0, 1, 0, 1, 0, 4);

        uDir = [ dataS; dataE; dataN; dataW ];
        [~, ix]  = unique([verticesSidx; verticesEidx; verticesNidx; verticesWidx]);
        uDir = uDir(ix);
    case 3
        % Minimal surface on the unit disk
        uDir = mesh.coordinates(diridofs,1).^2;
    case 4
        % Example on [0.25, 0.75]^2 from P.Concus paper
        uDir = evaluateExactSol(4, mesh.coordinates(diridofs,1), mesh.coordinates(diridofs,2));
    case 5
        % Catenoid on sector
        uDir = evaluateExactSol(5, mesh.coordinates(diridofs,1), mesh.coordinates(diridofs,2));
    case 6
        % Scherk fifth surface on [-0.8, 0.8]^2
        uDir = evaluateExactSol(6, mesh.coordinates(diridofs,1), mesh.coordinates(diridofs,2));
end

end
