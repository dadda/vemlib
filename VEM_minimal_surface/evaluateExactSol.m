function [ sol, gradSolx, gradSoly ] = evaluateExactSol(bc, x, y)

switch bc
    case 0
        sol      = x + y;
        gradSolx = ones(size(x));
        gradSoly = ones(size(y));
    case 4
        sol      = sqrt( ( cosh(y) ).^2 - x.^2 );
        gradSolx = -x./sqrt(cosh(y).^2 - x.^2);
        gradSoly = (cosh(y).*sinh(y))./sqrt(cosh(y).^2 - x.^2);
    case 5
        a   = 0.75;
        b   = 4;
        rSq = x.^2 + y.^2;
        r   = sqrt( rSq );
        sol = a * log( (b + sqrt(b^2 - a^2)) ./ (r + sqrt(rSq - a^2)) );

        gradSolx = -(a*x)./(r.*sqrt(rSq - a^2));
        gradSoly = -(a*y)./(r.*sqrt(rSq - a^2));
    case 6
        sol      = asin( sinh(x) .* sinh(y) );
        gradSolx = (cosh(x).*sinh(y))./sqrt(1. - sinh(x).^2.*sinh(y).^2);
        gradSoly = (cosh(y).*sinh(x))./sqrt(1. - sinh(x).^2.*sinh(y).^2);
end

end
