function [ errH1, errL2 ] = compute_errors_minsurf( bc, FEMtr, uFEM, uVEM, ...
                                                    Nelt, coordinates, elements, hasAnalyticSolution, ...
                                                    xqd, yqd, wqd, basis_on_qd, areas, P0s_by_ele )

errL2     = 0.;
solL2norm = 0.;

errH1     = 0.;
solH1norm = 0.;

for E = 1:Nelt
    Nver_loc   = elements(E,1);
    local_dofs = elements(E,2:Nver_loc+1)';
    xbnd       = coordinates(elements(E,2:Nver_loc+1),1)';
    ybnd       = coordinates(elements(E,2:Nver_loc+1),2)';
    Nx         = ybnd([1:Nver_loc 1]) - ybnd([Nver_loc 1:Nver_loc]);
    Ny         = xbnd([Nver_loc 1:Nver_loc]) - xbnd([1:Nver_loc 1]);
    P0grad     = [ Nx(1:Nver_loc)+Nx(2:end) Ny(1:Nver_loc)+Ny(2:end) ] / (2 * areas(E));

    Uloc   = basis_on_qd{E}*(P0s_by_ele{E}*uVEM(local_dofs));
    dUlocX = P0grad(1:Nver_loc)*uVEM(local_dofs);
    dUlocY = P0grad(Nver_loc+1:end)*uVEM(local_dofs);

    if hasAnalyticSolution
        [ sol, gradSolx, gradSoly ] = evaluateExactSol(bc, xqd{E}, yqd{E});
    else
        [ sol, gradSolx, gradSoly ] = estimateSol(FEMtr, uFEM, [xqd{E}, yqd{E}]);
        nanDofs           = isnan(sol);
        sol(nanDofs)      = Uloc(nanDofs);
        gradSolx(nanDofs) = dUlocX;
        gradSoly(nanDofs) = dUlocY;
    end

    errL2     = errL2 + wqd{E}' * (sol - Uloc).^2;
    solL2norm = solL2norm + wqd{E}' * sol.^2;

    errH1     = errH1 + wqd{E}' * ( (gradSolx - dUlocX).^2 + (gradSoly - dUlocY).^2 );
    solH1norm = solH1norm + wqd{E}' * (gradSolx.^2 + gradSoly.^2);
end

errH1 = sqrt( errH1 / solH1norm );
errL2 = sqrt( errL2 / solL2norm );

end
