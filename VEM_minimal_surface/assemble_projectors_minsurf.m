function [ stabilityTerm, Sref, rowDofsForS, colDofsForS, gradPN, hmax, areas, ...
           xqd, yqd, wqd, basis_on_qd, P0s_by_ele ] = ...
    assemble_projectors_minsurf(mesh, formula1d, use_mp)

k   = 1;
eoa = 0;
compressquad = 0;

coordinates = mesh.coordinates;
elements    = mesh.elements;

stabilityTerm = cell(mesh.Nelt, 1);
Sref          = cell(mesh.Nelt, 1);
rowDofsForS   = cell(mesh.Nelt,1);
colDofsForS   = cell(mesh.Nelt,1);
gradPN        = cell(mesh.Nelt, 1);
hmax          = zeros(mesh.Nelt, 1);
areas         = zeros(mesh.Nelt, 1);

xqd         = cell(mesh.Nelt,1);
yqd         = cell(mesh.Nelt,1);
wqd         = cell(mesh.Nelt,1);
basis_on_qd = cell(mesh.Nelt,1);
P0s_by_ele  = cell(mesh.Nelt,1);

parfor ( E = 1:mesh.Nelt, getParforArg() )
% for E = 1:mesh.Nelt
    element = elements(E,:);
    NvLoc   = element(1);
    dofs    = element(2:NvLoc+1)';

    [ xbnd, ybnd, xqd{E}, yqd{E}, wqd{E}, Nx, Ny, hmax(E), areas(E), xb, yb] = ...
        build_coordinate_matrices_parallel( k, eoa, compressquad, ...
                                            element, coordinates, formula1d, use_mp );

    % Evaluate monomial basis on boundary and internal nodes
    basis_on_qd{E} = monomial_basis( xqd{E}, yqd{E}, k, ...
                                     xb, yb, hmax(E), ones(size(xqd{E})) );
    basis_on_qd{E} = permute(basis_on_qd{E}, [1,3,2]);

    basis_on_bnd = monomial_basis(xbnd, ybnd, k, xb, yb, hmax(E), ones(size(xbnd)));
    basis_on_bnd = permute(basis_on_bnd, [1,3,2]);

    D = basis_on_bnd;
    B = zeros(3, NvLoc);
    B(1,:) = 1./NvLoc;
    B(2,:) = (Nx(1:NvLoc) + Nx(2:NvLoc+1))'/(2.*hmax(E));
    B(3,:) = (Ny(1:NvLoc) + Ny(2:NvLoc+1))'/(2.*hmax(E));    

    G   = B*D;
    PNs = G\B;
    warnMsg = lastwarn;
    if ~isempty(warnMsg)
        fprintf('There is something misterious here ...\n');
        lastwarn('');
    end
    PN  = D*PNs;

    Gt      = G;
    Gt(1,:) = 0.;

    stabilityTerm{E} = (eye(NvLoc) - PN)'*(eye(NvLoc) - PN);
    Sref{E}          = PNs' * Gt * PNs + stabilityTerm{E};
    Sref{E}          = Sref{E}(:);
    dofs             = repmat(dofs, 1, NvLoc);
    rowDofsForS{E}   = dofs(:);
    dofs             = dofs';
    colDofsForS{E}   = dofs(:);

    gradPN{E}        = B([2,3],:) * (hmax(E)/areas(E));
    P0s_by_ele{E}    = PNs;
end

rowDofsForS = vertcat(rowDofsForS{:});
colDofsForS = vertcat(colDofsForS{:});

end
