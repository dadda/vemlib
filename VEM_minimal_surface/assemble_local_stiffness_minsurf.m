function S = assemble_local_stiffness_minsurf(mesh, stabilityTerm, Sref, gradPN, areas, uOld)

elements = mesh.elements;
S        = cell(mesh.Nelt,1);

parfor ( E = 1:mesh.Nelt, getParforArg() )
% for E = 1:mesh.Nelt
    element = elements(E,:);
    NvLoc   = element(1);

    % Evaluate the area functional
    uOldK = uOld(element(2:NvLoc+1));
    tmp   = gradPN{E} * uOldK;
    fk    = sqrt( 1.  ...
            + tmp(1)^2 + tmp(2)^2 ...
            + ( uOldK' * (stabilityTerm{E} * uOldK) ) / areas(E) );
    S{E} = Sref{E} / fk;
end

S = vertcat(S{:});

end
