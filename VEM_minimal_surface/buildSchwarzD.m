function [] = buildSchwarzD(bc, mesh, x, y, u, Nx, Ny, Nz)

switch bc
    case 1
        % Create base hexagon

        L = length(x);
        data = zeros(3,6*L);
        data(1,1:L) = x;
        data(2,1:L) = y;
        data(3,1:L) = u;

        A = [1; 1; 1];
        B = [1; 0; 0];
        C = [0; 1; 0];

%         figure();
%         hold on;
%         axis equal;
%         for E = 1:mesh.Nelt
%             NvLoc = mesh.elements(E,1);
%             patch('XData', mesh.coordinates(mesh.elements(E,2:NvLoc+1), 1), ...
%                   'YData', mesh.coordinates(mesh.elements(E,2:NvLoc+1), 2), ...
%                   'ZData', u(mesh.elements(E,2:NvLoc+1)), 'FaceColor', 'c');
%         end

        datain = [x'; y'; u'];
        currentEnd            = B;
        nextEndBeforeRotation = C;
        ct = -1; st = 0; % rotation of \pi
        for i = 1:5
            shiftedDataIn = bsxfun(@minus, datain, A);
            N = currentEnd - A; normN = norm(N,2); N = N./normN;
            R = [ ct + N(1)^2*(1-ct),   N(1)*N(2)*(1-ct)-N(3)*st,   N(1)*N(3)*(1-ct)+N(2)*st;
                  N(1)*N(2)*(1-ct)+N(3)*st,   ct + N(2)^2*(1-ct),   N(2)*N(3)*(1-ct)-N(1)*st;
                  N(1)*N(3)*(1-ct)-N(2)*st,   N(2)*N(3)*(1-ct)+N(1)*st,   ct + N(3)^2*(1-ct)];
            newData = R*shiftedDataIn;
            newData = bsxfun(@plus, newData, A);
            data(:,i*L+1:(i+1)*L) = newData;
            tmp                   = currentEnd;
            currentEnd            = R*(nextEndBeforeRotation-A) + A;
            nextEndBeforeRotation = tmp;
            datain                = newData;
%             for E = 1:mesh.Nelt
%                 NvLoc = mesh.elements(E,1);
%                 patch('XData', newData(1,mesh.elements(E,2:NvLoc+1)), ...
%                       'YData', newData(2,mesh.elements(E,2:NvLoc+1)), ...
%                       'ZData', newData(3,mesh.elements(E,2:NvLoc+1)), ...
%                       'FaceColor', 'c');
%             end
        end
        
        % Create unit cell of 4 base hexagons
        unitCell = zeros(3, 6*L*4);
        unitCell(:,1:6*L) = data;
        
        fixed = [2; 2; 2];
        for i = 1:3
            N = zeros(3,1); N(i) = 1;
            shiftedDataIn = bsxfun(@minus, data, fixed);
            R = [ ct + N(1)^2*(1-ct),   N(1)*N(2)*(1-ct)-N(3)*st,   N(1)*N(3)*(1-ct)+N(2)*st;
                  N(1)*N(2)*(1-ct)+N(3)*st,   ct + N(2)^2*(1-ct),   N(2)*N(3)*(1-ct)-N(1)*st;
                  N(1)*N(3)*(1-ct)-N(2)*st,   N(2)*N(3)*(1-ct)+N(1)*st,   ct + N(3)^2*(1-ct)];
            newData = R*shiftedDataIn;
            newData = bsxfun(@plus, newData, fixed);
            unitCell(:,6*L*i+1:6*L*(i+1)) = newData;
        end

        schwarzDx = bsxfun(@plus, unitCell(1,:)', 4*(0:Nx-1));
        schwarzDx = repmat(schwarzDx(:), Ny*Nz, 1);

        schwarzDy = repmat(unitCell(2,:)', Nx, 1);
        schwarzDy = bsxfun(@plus, schwarzDy, 4*(0:Ny-1));
        schwarzDy = repmat(schwarzDy(:), Nz, 1);

        schwarzDz = repmat(unitCell(3,:)', Nx*Ny, 1);
        schwarzDz = bsxfun(@plus, schwarzDz, 4*(0:Nz-1));
        schwarzDz = schwarzDz(:);

        figure();
        hold on;
        axis equal;
        set(gcf, 'color', [1 1 1]);

        counter = 0;
        for k = 1:Nz
            for j = 1:Ny
                for i = 1:Nx
                    for l = 1:24
                        for E = 1:mesh.Nelt
                            NvLoc = mesh.elements(E,1);
                            patch('XData', schwarzDx(counter+mesh.elements(E,2:NvLoc+1)), ...
                                  'YData', schwarzDy(counter+mesh.elements(E,2:NvLoc+1)), ...
                                  'ZData', schwarzDz(counter+mesh.elements(E,2:NvLoc+1)), ...
                                  'CData', schwarzDz(counter+mesh.elements(E,2:NvLoc+1)), ...
                                  'FaceColor', 'interp', 'FaceAlpha', 1);
                        end
                        counter = counter + L;
                    end
                end
            end
        end
end

end
