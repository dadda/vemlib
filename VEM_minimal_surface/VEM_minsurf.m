function [ u, S, Hmax, it, xqd, yqd, wqd, basis_on_qd, areas, P0s_by_ele ] = VEM_minsurf(mesh, bc, rtol, atol, maxit, initialGuess, solver)

k      = 1;
use_mp = 0;

[xqd1, wqd1] = lobatto(k+1, use_mp);
formula1d    = [(1.-xqd1)/2. (xqd1+1.)/2. wqd1/2.];

%
% Data that can be computed once for all
%

% Problem dimension
N = mesh.Nver;

% Projectors and reference stiffness matrices
[ stabilityTerm, Sref, rowDofsForS, colDofsForS, gradPN, hmax, areas, ...
  xqd, yqd, wqd, basis_on_qd, P0s_by_ele ] = ...
    assemble_projectors_minsurf(mesh, formula1d, use_mp);

Hmax = max(hmax);

% Dirichlet boundary conditions
[ uDir, diridofs ] = dirdata_minsurf(bc, mesh);
dofs               = setdiff(1:N, diridofs);

% Right hand side
F = zeros(N,1);

%
% Fixed-point algorithm
%

if isempty(initialGuess)
    uOld = zeros(N,1);
else
    if length(initialGuess) ~= N
        error('You must provide a vector of dimension %i as initial guess', N);
    end
    uOld = initialGuess(:);
end

% Handle the case in which the zero vector is used as initial guess
normOld = norm(uOld,inf);
err = Inf;
tol = 0;
it  = 0;

fprintf('\n');

while ( it < maxit && err >= tol )
    %
    % Compute the next iterate
    %

    fprintf('Iteration %i. ', it+1);

    fprintf('Assembling ... ');

    % Global stiffness matrix and right hand side
    S = assemble_local_stiffness_minsurf(mesh, stabilityTerm, Sref, gradPN, areas, uOld);
    S = sparse(rowDofsForS, colDofsForS, S);
    F(:) = 0;

    % Dirichlet boundary conditions
    F(diridofs)      = uDir;
    F(dofs)          = F(dofs) - S(dofs, diridofs)*F(diridofs);
    S( diridofs, : ) = 0;
    S( :, diridofs ) = 0;
    S( diridofs + N*(diridofs-1) ) = 1;

    fprintf('done. Solving the linear system ... ');
    if solver == 1
        % UMFPACK
        u = S\F;
    elseif solver == 2
        % AGMG
        u = agmg(S, F, 1, 1e-14, 100000, [], [], []);
    end
    fprintf('done. ');

    if normOld == 0
        tol = atol;
        err = norm(u-uOld,inf);
    else
        tol = rtol;
        err = norm(u-uOld,inf)/normOld;
    end

    fprintf('Error estimator: %g\n', err);

    uOld    = u;
    normOld = norm(uOld,inf);

%     plot3(mesh.coordinates(:,1), mesh.coordinates(:,2), u, 'c.')
%     pause;

    it = it + 1;
end

if err < tol
    fprintf('Fixed-point algorithm converged to the desired tolerance %g within %i iterations.\n', ...
        tol, it);
else
    fprintf('Fixed-point algorithm iterated %i times but did not converge.\n', it);
end

end
