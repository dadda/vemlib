function [ sol, gradSolx, gradSoly ] = estimateSol(FEMtr, FEMsol, points)

id       = pointLocation(FEMtr, points);
Npt      = size(points, 1);
sol      = zeros(Npt,1);
gradSolx = zeros(Npt,1);
gradSoly = zeros(Npt,1);

for i = 1:Npt
    if ~isnan(id(i))
        element  = FEMtr.ConnectivityList(id(i),:);
        vertices = FEMtr.Points(element,:);
        normals  = [vertices([2 3 1],2)-vertices(:,2), vertices(:,1)-vertices([2 3 1],1)];
        uE       = FEMsol(element);
        VEMnode  = points(i,:);
        for j = 1:3
	        oppositeNormal = normals(mod(j,3)+1,:);
            L              = norm(oppositeNormal,2);
            oppositeNormal = oppositeNormal'/L;
            sol(i)         = sol(i) ...
                             + (1. - ( ( VEMnode - vertices(j,:) ) * oppositeNormal ) / ...
                                     ( ( vertices(mod(j,3)+1,:) - vertices(j,:) ) * oppositeNormal ) ) ...
                               * uE(j);
            gradSolx(i)    = gradSolx(i) ...
                             - uE(j) * oppositeNormal(1) / ( ( vertices(mod(j,3)+1,:) - vertices(j,:) ) * oppositeNormal );
            gradSoly(i)    = gradSoly(i) ...
                             - uE(j) * oppositeNormal(2) / ( ( vertices(mod(j,3)+1,:) - vertices(j,:) ) * oppositeNormal );
        end
    else
        sol(i)      = nan;
        gradSolx(i) = nan;
        gradSoly(i) = nan;
    end
end

end
