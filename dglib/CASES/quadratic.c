#include "dglib.h"


void dgExact( const double x, const double y, double * const u )
{
  *u = 0.25 * ( x*x + y*y );
}


void dgGradExact( const double x, const double y, double * const gx, double * const gy )
{
  *gx = 0.5 * x;
  *gy = 0.5 * y;
}


double dgLoad( const double x, const double y )
{
  return -1.0;
}


void dgDirichlet( const double x, const double y, double * const u )
{
  dgExact(x, y, u);
}


void dgNeumann( const int label,
		const double x,
		const double y,
		double * const gx,
		double * const gy )
{
  dgGradExact( x, y, gx, gy );
}


/*
 * Label edges of unit square
 *
 * The upper edge is marked as Neumann, whereas the other ones as Dirichlet
 */
void labelEdges( dualMesh2d * const dMesh )
{
  int i, counterD, counterN;

  int *dirEdges = NULL, *neuEdges = NULL;

  double tol = 1e-6;
  double Ny;
  double *v1 = NULL, *v2 = NULL;

  dMesh->eBndMarkers = (int *) calloc (dMesh->Ned, sizeof(int));
  dirEdges           = (int *) malloc (dMesh->Ned * sizeof(int));
  neuEdges           = (int *) malloc (dMesh->Ned * sizeof(int));

  counterD = 0;
  counterN = 0;
  for (i = 0; i < dMesh->Ned; ++i) {
    if ( dMesh->e2p[2*i+1] == -1 ) {
      v1 = dMesh->vertices + 2*dMesh->edges[2*i];
      v2 = dMesh->vertices + 2*dMesh->edges[2*i+1];
      Ny = *v1 - *v2;
      Ny = Ny < 0.0 ? -Ny : Ny;
      if ( ( fabs(Ny - 1.0) < tol ) && (*(v1+1) > 0.5) ) {
	dMesh->eBndMarkers[i] = 2; /* Neumann edge */
	neuEdges[counterN++]  = i;
      } else {
	dMesh->eBndMarkers[i] = 1; /* Dirichlet edge */
	dirEdges[counterD++]  = i;
      }
    }
  }

  if ( counterD > 0 ) {
    dMesh->NedDir   = counterD;
    dMesh->dirEdges = (int *) malloc (counterD * sizeof(int));
    memcpy(dMesh->dirEdges, dirEdges, counterD * sizeof(int));
  }

  if ( counterN > 0 ) {
    dMesh->NedNeu   = counterN;
    dMesh->neuEdges = (int *) malloc (counterN * sizeof(int));
    memcpy(dMesh->neuEdges, neuEdges, counterN * sizeof(int));
  }

  free(dirEdges);
  free(neuEdges);
}
