#include "dglib.h"
#include "stroud.h"

int main()
{
  const char * const basename =
    "/home/daniele/Documents/codes/vem_matlab/dglib/TESTS/widehatW_mesh_matlab";

  const char * const outputfilename =
    "/home/daniele/Documents/codes/vem_matlab/dglib/TESTS/output.out";

  int i, j, k, Nq1d;

  double *quadNodes1d = NULL, *quadWeights1d = NULL;
  double *gradxx = NULL, *gradxy = NULL, *gradyy = NULL, *basis = NULL;

  FILE *fp = NULL;

  primalMesh2d pMesh;

  /*
   *
   */

  k    = 0;
  Nq1d = 2;

  quadNodes1d   = (double *) malloc (2 * Nq1d * sizeof(double));
  quadWeights1d = (double *) malloc (Nq1d * sizeof(double));

  StroudQuadrature( 2*Nq1d-1, 1, quadNodes1d, quadWeights1d );

  primalMesh2dInitialize( &pMesh );
  readTriangle( basename, &pMesh );

  gradxx = (double *) malloc (pMesh.Nver * pMesh.Nver * sizeof(double));
  gradxy = (double *) malloc (pMesh.Nver * pMesh.Nver * sizeof(double));
  gradyy = (double *) malloc (pMesh.Nver * pMesh.Nver * sizeof(double));
  basis = (double *) malloc (pMesh.Nver * (k+1) * sizeof(double));

  computeWidehatW( &pMesh, k, quadNodes1d, quadWeights1d, Nq1d,
		   gradxx, gradxy, gradyy, basis );

  /* Save solution to file */

  fp = fopen(outputfilename, "w");
  for (i = 0; i < pMesh.Nver; ++i) {
    for (j = 0; j < k+1; ++j) {
      fprintf(fp, "   %.16f", basis[pMesh.Nver*j+i]);
    }
    fprintf(fp, "\n");
  }
  fclose(fp);

  free( gradxx );
  free( gradxy );
  free( gradyy );
  free( basis );
  primalMesh2dDestroy( &pMesh );
  free( quadNodes1d );
  free( quadWeights1d );

  return 0;
}
