PMGO_DIR      = /home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/cpp_source
CMAES_DIR     = /home/daniele/Documents/codes/c-cmaes-master/src
UTILITIES_DIR = /home/daniele/Documents/codes/vem_matlab/vemlib/cpp_source/
STROUD_DIR    = /home/daniele/Documents/codes/vem_matlab/Quadrature/cpp_modules
DG_DIR        = /home/daniele/Documents/codes/vem_matlab/dglib/SRC
BASIS_DIR     = /home/daniele/Documents/codes/vem_matlab/Basis_Functions/cpp_source

CC       = gcc
CFLAGS   = -fPIC -Wall -Wextra -ansi -pedantic -g -std=c99 -DDEBUG
CPPFLAGS = -I$(PMGO_DIR) -I$(CMAES_DIR) -I$(UTILITIES_DIR) -I$(STROUD_DIR) -I$(BASIS_DIR) -I$(DG_DIR)
LDFLAGS  = -L$(PMGO_DIR) -L$(UTILITIES_DIR) -L$(STROUD_DIR)

CCLOADER = $(CC)

DGLIB    = libdg.a

LIBS     = -lpolygauss -lpmgo -lutilities -lstroud -llapacke -lblas -lm
