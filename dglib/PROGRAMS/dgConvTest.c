#include "dglib.h"

#define NMESHES 1
#define MESHNAME_MAXLENGTH 512

int main()
{
  int i, j, m, Nk;
  int *kVect = NULL;

  double c0 = 0.3;
  double delta, t, alpha;

  const char *meshNames[NMESHES];
  char meshName[MESHNAME_MAXLENGTH];

  dualMesh2d dMesh;

  double *diameters = NULL, *centroids = NULL;
  double **u = NULL, **lambda = NULL;

  double *eL2 = NULL, *eH1 = NULL, *hVect = NULL;

  /*
   * Random Voronoi
   */

  const char * const meshRoot =
    "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/voronoi-notnested";

  const char * const meshNameShort = "voro";

  meshNames[0] = "randVoro__Nelt_000100__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off";

  /* meshNames[0] = "randVoro__Nelt_002500__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off"; */
  /* meshNames[1] = "randVoro__Nelt_005000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off"; */
  /* meshNames[2] = "randVoro__Nelt_010000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off"; */
  /* meshNames[3] = "randVoro__Nelt_020000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off"; */
  /* meshNames[4] = "randVoro__Nelt_040000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off"; */
  /* meshNames[5] = "randVoro__Nelt_080000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off"; */
  /* meshNames[6] = "randVoro__Nelt_160000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off"; */
  /* meshNames[7] = "randVoro__Nelt_320000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off"; */
  /* meshNames[8] = "randVoro__Nelt_640000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off"; */

  /*
   *
   */

  printf("Insert number of polynomial degrees to be tested: ");
  scanf("%i", &Nk);

  kVect = (int *) malloc (Nk * sizeof(int));
  for (i = 0; i < Nk; ++i) {
    printf("Insert degree: ");
    scanf("%i", kVect+i);
  }

  printf("Insert value of t: ");
  scanf("%lf", &t);

  printf("Insert value of alpha: ");
  scanf("%lf", &alpha);

  dualMesh2dInitialize( &dMesh );

  eL2   = (double *) malloc (NMESHES * sizeof(double));
  eH1   = (double *) malloc (NMESHES * sizeof(double));
  hVect = (double *) malloc (NMESHES * sizeof(double));

  printf("\n");

  for (i = 0; i < Nk; ++i) {

    delta = c0 / (double)(kVect[i]*kVect[i]);

    printf("Running tests for k = %i\n", kVect[i]);
    printf("------------------------\n\n");

    printf("Delta: %.16e\n\n", delta);

    for (j = 0; j < NMESHES; ++j) {

      if ( snprintf( meshName, MESHNAME_MAXLENGTH,
		     "%s/%s", meshRoot, meshNames[j] ) > MESHNAME_MAXLENGTH ) {
	printf( "File %s line %i: encountered error while generating mesh filename, "
		"aborting ...\n", __FILE__, __LINE__ );
	exit(1);
      }

      printf("Loading and labeling mesh %s ... ", meshNames[j] );
      readOFFmesh( meshName, &dMesh );
      edgeConnectivity( &dMesh );
      labelEdges( &dMesh );
      printf("done\n");

      diameters = (double *) malloc ((dMesh.Npol) * sizeof(double));
      centroids = (double *) malloc ((dMesh.Npol) * 2 * sizeof(double));
      u         = (double **) malloc ((dMesh.Npol) * sizeof(double *));
      lambda    = (double **) malloc ((dMesh.Npol) * sizeof(double *));

      dgPoisson( kVect[i], delta, &dMesh, t, alpha,
      		 diameters, centroids, u, lambda, eL2+j, eH1+j );

      /* Compute maximum element diameter */
      hVect[j] = 0.0;
      for (m = 0; m < dMesh.Npol; ++m) if ( diameters[m] > hVect[j] ) hVect[j] = diameters[m];

      printf("Freeing memory ... ");

      for (m = 0; m < dMesh.Npol; ++m) {
      	free( u[m] );
      	free( lambda[m] );
      }
      free( diameters );
      free( centroids );
      free( u );
      free( lambda );
      dualMesh2dDestroy( &dMesh );

      printf("done\n\n");
    }

    printf("\\toprule\n");
    printf("{Mesh}   &   {$h$}   &   {$e_{H^1}^u$}   &   {ecr}   &   {$e_{L^2}^u$}   &   {ecr}\\\\\n");
    printf("\\midrule\n");
    printf("%s$_1$   &   %e   &   %e   &   {-}   %e   &   {-}\\\\\n",
    	   meshNameShort, hVect[0], eH1[0], eL2[0]);
    for (j = 1; j < NMESHES; ++j)
      printf("%s$_{%i}$   &   %e   &   %e   &   %f   %e   &   %f\\\\\n",
    	     meshNameShort, j+1, hVect[0],
    	     eH1[0], log(eH1[j-1]/eH1[j])/log(hVect[j-1]/hVect[j]),
    	     eL2[0], log(eL2[j-1]/eL2[j])/log(hVect[j-1]/hVect[j]));
    printf("\\bottomrule\n\n");
  }

  free( kVect );
  free( eL2 );
  free( eH1 );
  free( hVect );

  return 0;
}
