#include "dglib.h"


void assembleGlobalFluxOperators( const int k,
				  const dualMesh2d * const dMesh,
				  const primalMesh2d * const pMeshHat,
				  const double * const gradxx,
				  const double * const gradxy,
				  const double * const gradyy,
				  const double * const GNeu,
				  const double * const widehatW,
				  /* Legendre polynomials of degree <= k,
				     evaluated at gaussNodes1d, and scaled by gaussWeights1d.
				     Ordering: row-major */
				  const double * const scaledLegendre,
				  /* 1D Stroud quadrature rule with degree of exactness = 2*k.
				     Ordering: col-major */
				  const int Ngauss1d,
				  const double * const gaussNodes1d, 
				  const double * const gaussWeights1d,
				  /* 2D Stroud quadrature rule with degree of exactness = k-1.
				     Ordering: col-major */
				  const int Ngauss2d,
				  const double * const gaussNodes2d,
				  const double * const gaussWeights2d,
				  const double t,
				  const double alpha,
				  double * const diameters,
				  double * const centroids,
				  /* Polygonal quadrature rules with degree of exactness >= 2*k */
				  double ** const xquad2dbyEle,
				  double ** const yquad2dbyEle,
				  double ** const wquad2dbyEle,
				  int * const Nq2dbyEle,
				  double ** const localSolvers,
				  double * const H,
				  double * const RHS )
{
  int i, j, l, deg, NverLoc, dPk, dLambda, dV, dPhi;

  int *localDofs = NULL;

  double area, Nx, Ny, tmp1, x1, y1, x2, y2, xE, yE;
  double *polygon = NULL, *edgeLengths = NULL;
  double *H_E = NULL, *RHS_E = NULL;

  deg  = 2*k;
  dPk  = ((k+1)*(k+2)) / 2;
  dPhi = (dMesh->Ned) * (k+1);

  memset( H, 0, dPhi * dPhi * sizeof(double) );
  memset( RHS, 0, dPhi * sizeof(double) );

  for (i = 0; i < dMesh->Npol; ++i) {

    /* Collect coordinates of vertices of i-th polygon */

    NverLoc     = dMesh->p2vDim[i+1] - dMesh->p2vDim[i];
    polygon     = (double *) malloc (2 * NverLoc * sizeof(double));
    edgeLengths = (double *) malloc (NverLoc * sizeof(double));
    for (j = 0; j < NverLoc; ++j) {
      polygon[j]         = dMesh->vertices[ 2 * dMesh->p2v[ dMesh->p2vDim[i] + j ] ];
      polygon[j+NverLoc] = dMesh->vertices[ 2 * dMesh->p2v[ dMesh->p2vDim[i] + j ] + 1 ];
    }

    /* Compute geometrical data: area, centroid */

    area = 0.0;
    xE   = 0.0;
    yE   = 0.0;
    for (j = 0; j < NverLoc; ++j) {
      x1 = polygon[j];
      y1 = polygon[j+NverLoc];
      x2 = polygon[(j+1)%NverLoc];
      y2 = polygon[(j+1)%NverLoc+NverLoc];
      
      Nx    = y2 - y1;
      Ny    = x1 - x2;

      edgeLengths[j] = sqrt( Nx*Nx + Ny*Ny );
      area          += Nx * x1 + Ny * y1;

      tmp1  = x1*y2 - x2*y1;
      xE   += (x1+x2)*tmp1;
      yE   += (y1+y2)*tmp1;
    }
    area            *= 0.5;
    centroids[2*i]   = xE / (6.0 * area);
    centroids[2*i+1] = yE / (6.0 * area);
    
    /* Create 2d quadrature rule for i-th polygon */

    polygauss2013( deg, polygon, NverLoc,
		   xquad2dbyEle+i, yquad2dbyEle+i, wquad2dbyEle+i, Nq2dbyEle+i, diameters+i);

    /* Assemble local solvers and flux arrays */

    dLambda = NverLoc * (k+1);
    dV      = dPk + dLambda;

    localSolvers[i] = (double *) malloc (dV * (dLambda+1) * sizeof(double));
    localDofs       = (int *) malloc (dLambda * sizeof(double));
    H_E             = (double *) malloc (dLambda * dLambda * sizeof(double));
    RHS_E           = (double *) malloc (dLambda * sizeof(double));

    assembleLocalFluxOperators( dMesh->edges, NverLoc, dMesh->p2v+(dMesh->p2vDim[i]),
    				dMesh->p2e+(dMesh->p2eDim[i]), polygon, edgeLengths,
    				centroids+2*i, diameters[i], k, pMeshHat,
    				gradxx, gradxy, gradyy, GNeu, widehatW, scaledLegendre,
    				Ngauss1d, gaussNodes1d, gaussWeights1d,
    				Ngauss2d, gaussNodes2d, gaussWeights2d,
    				Nq2dbyEle[i], xquad2dbyEle[i], yquad2dbyEle[i], wquad2dbyEle[i],
    				t, alpha, localSolvers[i], localDofs, H_E, RHS_E );

    /* Accumulate local values into global flux matrix H and right hand side RHS */

    /* We are not exploiting the symmetry of H here */
    for (j = 0; j < dLambda; ++j) {
      RHS[localDofs[j]] += RHS_E[j];
      for (l = 0; l < dLambda; ++l) {
    	H[dPhi*localDofs[j]+localDofs[l]] += H_E[dLambda*j+l];
      }
    }

    free(polygon);
    free(edgeLengths);
    free(localDofs);
    free(H_E);
    free(RHS_E);
  }
}


void assembleLocalFluxOperators( const int * const allEdges,
				 const int NvLoc,
				 const int * const p2vLoc,
				 const int * const p2eLoc,
				 const double * const polygon,
				 const double * const edgeLengths,
				 const double * const centroid,
				 const double diameter,
				 const int k,
				 const primalMesh2d * const pMeshHat,
				 const double * const gradxx,
				 const double * const gradxy,
				 const double * const gradyy,
				 const double * const GNeu,
				 const double * const widehatW,
				 /* Legendre polynomials of degree <= k,
				    evaluated at gaussNodes1d, and scaled by gaussWeights1d.
				    Ordering: row-major */
				 const double * const scaledLegendre,
				 /* 1D Stroud quadrature rule with degree of exactness = 2*k.
				    Ordering: col-major */
				 const int Ngauss1d,
				 const double * const gaussNodes1d, 
				 const double * const gaussWeights1d,
				 /* 2D Stroud quadrature rule with degree of exactness = k-1.
				    Ordering: col-major */
				 const int Ngauss2d,
				 const double * const gaussNodes2d,
				 const double * const gaussWeights2d,
				 /* Polygonal quadrature rule with degree of exactness >= 2*k */
				 const int Nq2d_E,
				 const double * const xquad2d_E,
				 const double * const yquad2d_E,
				 const double * const wquad2d_E,
				 const double t,
				 const double alpha,
				 double * const localSolver,
				 int * const dofs,
				 double * const H_E,
				 double * const RHS_E )
{
  int i, j, l, idx, dPk, dLambda, dV;

  double *tmp = NULL;
  double *basis_on_quad_pts1d = NULL;
  double *basis_on_quad_pts2d = NULL;
  double *grad_basis_on_quad_pts2d_x = NULL, *grad_basis_on_quad_pts2d_y = NULL;

  double xq1d, yq1d, dummy1, fIntegral, perimeter = 0.0;
  double v1[2], v2[2];
  double *A_E = NULL, *B_E = NULL, *F_E = NULL, *PHI_E = NULL, *Z_E = NULL;

  lapack_int *ipiv = NULL;

#ifdef DEBUG
  double nx, ny, dux, duy;
  double *uh = NULL, *lambdah = NULL, *tmp1 = NULL, *tmp2 = NULL;
  double *tildeA_E = NULL, *tildeBT_E = NULL, *tildeF_E = NULL;
#endif

  dPk     = ((k+1)*(k+2)) / 2;
  dLambda = NvLoc * (k+1);
  dV      = dPk + dLambda;

  /*
   *
   */

  memset( localSolver, 0, dV * (dLambda+1) * sizeof(double) );

  basis_on_quad_pts2d        = (double *) malloc (dPk * Nq2d_E * sizeof(double));
  grad_basis_on_quad_pts2d_x = (double *) calloc (dPk * Nq2d_E, sizeof(double));
  grad_basis_on_quad_pts2d_y = (double *) calloc (dPk * Nq2d_E, sizeof(double));

  /* Evaluate monomial basis functions and their gradient at 2d quadrature points
   * Row major ordering is used */

  for (j = 0; j < Nq2d_E; ++j) {
    compute_Pk_2d( k, xquad2d_E[j], yquad2d_E[j],
		   centroid[0], centroid[1], diameter,
		   basis_on_quad_pts2d+j*dPk );
    compute_Gk_2d( k-1, xquad2d_E[j], yquad2d_E[j],
		   centroid[0], centroid[1], diameter,
		   grad_basis_on_quad_pts2d_x+j*dPk+1, grad_basis_on_quad_pts2d_y+j*dPk+1 );
  }

  /*
   * Compute \int_E \nabla u^E \cdot \nabla v^E
   */

  A_E = (double *) calloc (dPk * dPk, sizeof(double));
  tmp = (double *) malloc (dPk * Nq2d_E * sizeof(double));

  for (i = 0; i < Nq2d_E; ++i) {
    for (j = 0; j < dPk; ++j) {
      /* Preserve row major ordering */
      idx      = dPk*i + j;
      tmp[idx] = wquad2d_E[i] * grad_basis_on_quad_pts2d_x[idx];
    }
  }

  cblas_dgemm( CblasRowMajor, CblasTrans, CblasNoTrans, dPk, dPk, Nq2d_E, 1.0,
  	       grad_basis_on_quad_pts2d_x, dPk, tmp, dPk, 0.0, A_E, dPk );

  for (i = 0; i < Nq2d_E; ++i) {
    for (j = 0; j < dPk; ++j) {
      /* Preserve row major ordering */
      idx      = dPk*i + j;
      tmp[idx] = wquad2d_E[i] * grad_basis_on_quad_pts2d_y[idx];
    }
  }

  cblas_dgemm( CblasRowMajor, CblasTrans, CblasNoTrans, dPk, dPk, Nq2d_E, 1.0,
  	       grad_basis_on_quad_pts2d_y, dPk, tmp, dPk, 1.0,
  	       A_E, dPk );

  free(tmp);
  free(grad_basis_on_quad_pts2d_x);
  free(grad_basis_on_quad_pts2d_y);

  /*
   * Compute
   *
   * B_E = \int_{\partial E} u^E \mu^E,
   *
   * PHI_E = \int_{\partial E} \phi \mu^E (it is a diagonal matrix,
   * only the main diagonal is stored),
   *
   * and
   *
   * array of degrees of freedom for assemblage
   */

  B_E   = (double *) calloc (dLambda * dPk, sizeof(double));
  PHI_E = (double *) malloc (dLambda * sizeof(double));

  basis_on_quad_pts1d = (double *) malloc (Ngauss1d * dPk * sizeof(double)); /* row-major */

  for (i = 0; i < NvLoc; ++i) {
    perimeter += edgeLengths[i];

    /* B_E */
    for (j = 0; j < Ngauss1d; ++j) {
      xq1d = gaussNodes1d[j] * polygon[i] + gaussNodes1d[j+Ngauss1d] * polygon[(i+1)%NvLoc];
      yq1d = gaussNodes1d[j] * polygon[i+NvLoc] + gaussNodes1d[j+Ngauss1d] * polygon[(i+1)%NvLoc+NvLoc];
      compute_Pk_2d( k, xq1d, yq1d, centroid[0], centroid[1], diameter,
  		     basis_on_quad_pts1d+j*dPk );
    }

    cblas_dgemm( CblasRowMajor, CblasTrans, CblasNoTrans, k+1, dPk, Ngauss1d, edgeLengths[i],
  		 scaledLegendre, k+1, basis_on_quad_pts1d, dPk, 0.0,
  		 B_E+(k+1)*dPk*i, dPk );

    /* PHI_E and dofs */

    /* edge orientation - it is based on the fact that the ordering of p2v mirrors the ordering
       of p2e, which -- should -- be the case given how p2e is computed */
    dummy1  = p2vLoc[i] == allEdges[2*p2eLoc[i]] ? 1.0 : -1.0;

    for (j = 0; j <= k; ++j) {
      idx        = i*(k+1)+j;
      PHI_E[idx] = edgeLengths[i] * ( 1.0 + (dummy1 - 1.0) * (double)(j%2) ) /
	(2.0 * (double)j + 1.0);
      dofs[idx]  = p2eLoc[i]*(k+1)+j;
    }
  }

  free(basis_on_quad_pts1d);

  /*
   * Compute \int_E f v^E
   */

  F_E = (double *) calloc (dPk, sizeof(double));
  tmp = (double *) malloc (Nq2d_E * sizeof(double));
  for (i = 0; i < Nq2d_E; ++i)
    tmp[i] = wquad2d_E[i] * dgLoad(xquad2d_E[i], yquad2d_E[i]);
  cblas_dgemv( CblasRowMajor, CblasTrans, Nq2d_E, dPk, 1.0, basis_on_quad_pts2d, dPk,
  	       tmp, 1, 0.0, F_E, 1 );

  /* fIntegral = F_E[0]; */
  fIntegral = 0.0;

  free(tmp);

  /*
   * Assemblage of arrays Z_E and G_E (use col-major)
   */

  Z_E = (double *) calloc (dV * dV, sizeof(double));

  for (j = 0; j < dPk; ++j) {
    /* A_E is row-major and symmetric, so row-major=col-major */
    memcpy(Z_E+dV*j, A_E+dPk*j, dPk*sizeof(double));
    /* B_E is row-major */
    for (i = 0; i < NvLoc; ++i) {
      for (l = 0; l <= k; ++l) {
  	idx = i*(k+1)+l;
  	Z_E[dV*j+idx+dPk]   = B_E[dPk*idx+j];
  	Z_E[dV*(dPk+idx)+j] = -B_E[dPk*idx+j];
      }
    }
  }

  memcpy( localSolver+dV*dLambda, F_E, dPk * sizeof(double) );

  /* Compute stabilization edge by edge */

#ifdef DEBUG
  tildeA_E  = (double *) calloc (dLambda * dPk, sizeof(double));
  tildeBT_E = (double *) calloc (dLambda * dLambda, sizeof(double));
  tildeF_E  = (double *) calloc (dLambda, sizeof(double));
#endif

  for (i = 0; i < NvLoc; ++i) {
    v1[0] = polygon[i];
    v1[1] = polygon[i+NvLoc];
    v2[0] = polygon[(i+1)%NvLoc];
    v2[1] = polygon[(i+1)%NvLoc+NvLoc];
    stabilizationOnSubTriangle( k, i, NvLoc, v1, v2, centroid, diameter, edgeLengths[i],
  				perimeter, pMeshHat, gradxx, gradxy, gradyy, GNeu,
  				widehatW, fIntegral, gaussNodes1d, gaussWeights1d, Ngauss1d,
  				gaussNodes2d, gaussWeights2d, Ngauss2d, t, alpha,
#ifdef DEBUG
				tildeA_E, tildeBT_E, tildeF_E,
#endif
  				Z_E, localSolver+dV*dLambda );
  }

  /*
   * Initialize right hand side for local solvers
   */

  for (j = 0; j < dLambda; ++j)
    localSolver[dV*j+j+dPk] = PHI_E[j];

#ifdef DEBUG
  printf("\n\nA_E:\n");
  for (i = 0; i < dPk; ++i) {
    for (j = 0; j < dPk; ++j)
      printf("   %.15f", A_E[dPk*i+j]);
    printf("\n");
  }

  printf("\nB_E:\n");
  for (i = 0; i < dLambda; ++i) {
    for (j = 0; j < dPk; ++j)
      printf("   %.15f", B_E[dPk*i+j]);
    printf("\n");
  }

  printf("\nF_E:\n");
  for (i = 0; i < dPk; ++i)
    printf("   %.15f\n", F_E[i]);

  printf("\nPHI_E:\n");
  for (i = 0; i < dLambda; ++i)
    printf("   %.15f\n", PHI_E[i]);

  printf("\nZ_E:\n");

  for (i = 0; i < dV; ++i) {
    for (j = 0; j < dV; ++j)
      printf("  %.7f", Z_E[dV*j+i]);
    printf("\n");
  }

  /*
   * Compute u_h
   */

  tmp1 = (double *) malloc (Nq2d_E * sizeof(double));
  uh   = (double *) calloc (dPk, sizeof(double));
  for (i = 0; i < Nq2d_E; ++i) {
    dgExact( xquad2d_E[i], yquad2d_E[i], tmp1+i );
    tmp1[i] *= wquad2d_E[i];
  }

  cblas_dgemv( CblasRowMajor, CblasTrans, Nq2d_E, dPk, 1.0, basis_on_quad_pts2d, dPk,
  	       tmp1, 1, 0.0, uh, 1 );
  free(tmp1);

  tmp1 = (double *) malloc (Nq2d_E * dPk * sizeof(double));
  for (i = 0; i < Nq2d_E; ++i) {
    for (j = 0; j < dPk; ++j) {
      tmp1[dPk*i+j] = wquad2d_E[i] * basis_on_quad_pts2d[dPk*i+j];
    }
  }

  tmp2 = (double *) calloc (dPk * dPk, sizeof(double));

  cblas_dgemm( CblasRowMajor, CblasTrans, CblasNoTrans, dPk, dPk, Nq2d_E, 1.0,
  	       basis_on_quad_pts2d, dPk, tmp1, dPk, 0.0, tmp2, dPk );

  ipiv = (lapack_int *) malloc (dPk * sizeof(lapack_int));
  /* Exploit symmetry of tmp2 */
  LAPACKE_dgesv( LAPACK_COL_MAJOR, (lapack_int)(dPk), (lapack_int)(1),
  		 tmp2, (lapack_int)(dPk), ipiv, uh, (lapack_int)(dPk) );

  printf("\n\nuh:\n");
  for (i = 0; i < dPk; ++i)
    printf("   %.15f\n", uh[i]);

  if (k == 1)
    printf("\n%.15f, %.15f, %.15f\n", centroid[0]+centroid[1], diameter, diameter);
  else if (k == 2)
    printf("\n%.15f, %.15f, %.15f, %.15f, 0, %.15f\n",
	   0.25*(centroid[0]*centroid[0]+centroid[1]*centroid[1]),
	   0.5*diameter*centroid[0],
	   0.5*diameter*centroid[1],
	   0.25*diameter*diameter,
	   0.25*diameter*diameter);

  free(ipiv);
  free(tmp1);
  free(tmp2);

  /*
   * Compute lambdah
   */

  lambdah = (double *) calloc (dLambda, sizeof(double));
  tmp1    = (double *) malloc (Ngauss1d * sizeof(double));
  for (i = 0; i < NvLoc; ++i) {
    nx = ( polygon[(i+1)%NvLoc+NvLoc] - polygon[i+NvLoc] ) / edgeLengths[i];
    ny = ( polygon[i] - polygon[(i+1)%NvLoc] ) / edgeLengths[i];
    for (j = 0; j < Ngauss1d; ++j) {
      xq1d = gaussNodes1d[j] * polygon[i] + gaussNodes1d[j+Ngauss1d] * polygon[(i+1)%NvLoc];
      yq1d = gaussNodes1d[j] * polygon[i+NvLoc]
	+ gaussNodes1d[j+Ngauss1d] * polygon[(i+1)%NvLoc+NvLoc];
      dgGradExact( xq1d, yq1d, &dux, &duy );
      tmp1[j] = nx*dux + ny*duy;
    }
    cblas_dgemv( CblasRowMajor, CblasTrans, Ngauss1d, k+1, 1.0, scaledLegendre, k+1,
		 tmp1, 1, 0.0, lambdah+(k+1)*i, 1 );
    for (j = 0; j <= k; ++j) lambdah[(k+1)*i+j] *= (double)(2*j+1);

    if (k == 1)
      printf("%.15f, %.15f, %.15f\n", lambdah[(k+1)*i], lambdah[(k+1)*i+1], nx+ny);
  }
  free(tmp1);

  /*
   * Check that A_E * uh - B_E^T * lambdah - F_E = 0
   */

  tmp1 = (double *) calloc (dPk, sizeof(double));

  cblas_dgemv( CblasRowMajor, CblasNoTrans, dPk, dPk, 1.0, A_E, dPk,
  	       uh, 1, 0.0, tmp1, 1 );
  cblas_dgemv( CblasRowMajor, CblasTrans, dLambda, dPk, -1.0, B_E, dPk,
  	       lambdah, 1, 1.0, tmp1, 1 );
  cblas_daxpy( dPk, -1.0, F_E, 1, tmp1, 1 );

  printf("\n\nA_E * uh - B_E^T * lambdah - F_E:\n");
  for (i = 0; i < dPk; ++i)
    printf("%.15f\n", tmp1[i]);
  free(tmp1);

  /*
   * Check tildeA * uh - tildeB^T * lambdah - tildeF
   */

  tmp1 = (double *) calloc (dLambda, sizeof(double));

  cblas_dgemv( CblasRowMajor, CblasNoTrans, dLambda, dPk, 1.0, tildeA_E, dPk,
  	       uh, 1, 0.0, tmp1, 1 );
  cblas_dgemv( CblasRowMajor, CblasNoTrans, dLambda, dLambda, -1.0, tildeBT_E, dLambda,
  	       lambdah, 1, 1.0, tmp1, 1 );
  cblas_daxpy( dLambda, -1.0, tildeF_E, 1, tmp1, 1 );

  printf("\n\ntildeA * uh - tildeB^T * lambdah - tildeF:\n");
  for (i = 0; i < dLambda; ++i)
    printf("%.15f\n", tmp1[i]);
  free(tmp1);

  free(uh);
  free(lambdah);

  free( tildeA_E );
  free( tildeBT_E );
  free( tildeF_E );
#endif

  ipiv = (lapack_int *) malloc (dV * sizeof(lapack_int));

  /* Eliminate u_E, \lambda_E in terms of \phi_{\partial E} */

  LAPACKE_dgesv( LAPACK_COL_MAJOR, (lapack_int)(dV), (lapack_int)(dLambda+1),
  		 Z_E, (lapack_int)(dV), ipiv, localSolver, (lapack_int)(dV) );

  free(ipiv);

  for (j = 0; j < dLambda; ++j) {
    for (i = 0; i < dLambda; ++i) {
      H_E[dLambda*j+i] = PHI_E[i]*localSolver[dV*j+dPk+i];
    }
    RHS_E[j] = -PHI_E[j] * localSolver[dV*dLambda+dPk+j];
  }

  free(basis_on_quad_pts2d);
  free(A_E);
  free(B_E);
  free(F_E);
  free(PHI_E);
  free(Z_E);
}


void applyNeumannBC( const int k,
		     const dualMesh2d * const dMesh,
		     /* Legendre polynomials of degree <= k,
			evaluated at gaussNodes1d, and scaled by gaussWeights1d.
			Ordering: row-major */
		     const double * const scaledLegendre,
		     /* 1D Stroud quadrature rule with degree of exactness = 2*k.
			Ordering: col-major */
		     const int Ngauss1d,
		     const double * const gaussNodes1d,
		     double * const RHS )
{
  int i, j, eIdx, eIdxLoc, label, pIdx;

  double xq1d, yq1d, tmp1, tmp2, orientation, Nx, Ny;
  double v1[2], v2[2];
  double *v1Addr = NULL, *v2Addr = NULL;
  double *tmp = NULL, *neuValues = NULL;

  neuValues = (double *) calloc ((k+1), sizeof(double));
  tmp       = (double *) malloc (Ngauss1d * sizeof(double));

  for (i = 0; i < dMesh->NedNeu; ++i) {
    eIdx   = dMesh->neuEdges[i];
    label  = dMesh->eBndMarkers[eIdx];
    v1Addr = dMesh->vertices + 2*dMesh->edges[2*eIdx];
    v2Addr = dMesh->vertices + 2*dMesh->edges[2*eIdx+1];
    v1[0]  = *v1Addr;
    v1[1]  = *(v1Addr+1);
    v2[0]  = *v2Addr;
    v2[1]  = *(v2Addr+1);

    pIdx = dMesh->e2p[2*eIdx];
    for (j = 0; j < dMesh->p2eDim[pIdx+1]-dMesh->p2eDim[pIdx]; ++j )
      if ( dMesh->p2e[ dMesh->p2eDim[pIdx] + j ] == eIdx ) break;
    eIdxLoc = j;

    orientation =
      dMesh->p2v[dMesh->p2vDim[pIdx]+eIdxLoc] ==
      dMesh->edges[2*dMesh->p2e[dMesh->p2eDim[pIdx]+eIdxLoc]] ? 1.0 : -1.0;

    Nx = orientation * (v2[1] - v1[1]);
    Ny = orientation * (v1[0] - v2[0]);

    for (j = 0; j < Ngauss1d; ++j) {
      xq1d = gaussNodes1d[j] * v1[0] + gaussNodes1d[j+Ngauss1d] * v2[0];
      yq1d = gaussNodes1d[j] * v1[1] + gaussNodes1d[j+Ngauss1d] * v2[1];
      dgNeumann( label, xq1d, yq1d, &tmp1, &tmp2 );
      tmp[j] = tmp1 * Nx + tmp2 * Ny;
    }

    cblas_dgemv( CblasRowMajor, CblasTrans, Ngauss1d, k+1, 1.0, scaledLegendre, k+1,
		 tmp, 1, 0.0, neuValues, 1 );

    for (j = 0; j <= k; ++j)
      RHS[eIdx*(k+1)+j] += neuValues[j];
  }

  free(neuValues);
  free(tmp);
}


void applyDirichletBC( const int k,
		       const dualMesh2d * const dMesh,
		       /* Legendre polynomials of degree <= k,
			  evaluated at gaussNodes1d, and scaled by gaussWeights1d.
			  Ordering: row-major */
		       const double * const scaledLegendre,
		       /* 1D Stroud quadrature rule with degree of exactness = 2*k.
			  Ordering: col-major */
		       const int Ngauss1d,
		       const double * const gaussNodes1d,
		       double * const H,
		       double * const RHS )
{
  int i, j, eIdx, dof, dPhi, dDiri;
  int *isDiri = NULL, *dirDofs = NULL;

  double xq1d, yq1d, tmp1;
  double v1[2], v2[2];
  double *v1Addr = NULL, *v2Addr = NULL;
  double *tmp = NULL, *dirValues = NULL;

  dPhi  = (dMesh->Ned) * (k+1);
  dDiri = (dMesh->NedDir) * (k+1);

  isDiri    = (int *) calloc (dPhi, sizeof(int));
  dirDofs   = (int *) malloc (dDiri * sizeof(int));
  dirValues = (double *) calloc ((k+1), sizeof(double));
  tmp       = (double *) malloc (Ngauss1d * sizeof(double));

  for (i = 0; i < dMesh->NedDir; ++i) {
    eIdx = dMesh->dirEdges[i];
    v1Addr = dMesh->vertices + 2*dMesh->edges[2*eIdx];
    v2Addr = dMesh->vertices + 2*dMesh->edges[2*eIdx+1];
    v1[0]  = *v1Addr;
    v1[1]  = *(v1Addr+1);
    v2[0]  = *v2Addr;
    v2[1]  = *(v2Addr+1);

    for (j = 0; j < Ngauss1d; ++j) {
      xq1d = gaussNodes1d[j] * v1[0] + gaussNodes1d[j+Ngauss1d] * v2[0];
      yq1d = gaussNodes1d[j] * v1[1] + gaussNodes1d[j+Ngauss1d] * v2[1];
      dgDirichlet( xq1d, yq1d, tmp+j );
    }

    cblas_dgemv( CblasRowMajor, CblasTrans, Ngauss1d, k+1, 1.0, scaledLegendre, k+1,
		 tmp, 1, 0.0, dirValues, 1 );

    for (j = 0; j <=k; ++j) {
      dof                = eIdx*(k+1)+j;
      isDiri[dof]        = 1;
      dirDofs[i*(k+1)+j] = dof;
      RHS[dof]           = (2.0 * (double)j + 1.0) * dirValues[j];
    }
  }

  /* "Eliminate" Dirichlet dofs */

  /* Eliminate from RHS */

  for (i = 0; i < dPhi; ++i) {
    if ( isDiri[i] == 0 ) {
      tmp1 = 0.0;
      for (j = 0; j < dDiri; ++j) {
	tmp1 += H[dPhi*dirDofs[j]+i] * RHS[dirDofs[j]];
      }
      RHS[i] -= tmp1;
    }
  }

  /* Eliminate from H */

  for (i = 0; i < dDiri; ++i) {
    for (j = 0; j < dPhi; ++j) {
      H[dPhi*j+dirDofs[i]] = 0.0;
      H[dPhi*dirDofs[i]+j] = 0.0;
    }
    H[dPhi*dirDofs[i]+dirDofs[i]] = 1.0;
  }

  free(isDiri);
  free(dirDofs);
  free(dirValues);
  free(tmp);
}


void reconstructVariables( const int k,
			   const dualMesh2d * const dMesh,
			   double ** const localSolvers,
			   const double * const X,
			   double ** const u,
			   double ** const lambda )
{
  int i, j, NvLoc, dPk, dV, dLambda;

  double *phi_E = NULL, *tmp = NULL;

  dPk = ((k+1)*(k+2)) / 2;

  for (i = 0; i < dMesh->Npol; ++i) {
    NvLoc   = dMesh->p2vDim[i+1] - dMesh->p2vDim[i];
    dLambda = NvLoc * (k+1);
    dV      = dPk + dLambda;

    phi_E     = (double *) malloc (dLambda * sizeof(double));
    tmp       = (double *) calloc (dV, sizeof(double));

    for (j = 0; j < NvLoc; ++j)
      memcpy( phi_E+(k+1)*j, X+(k+1)*dMesh->p2e[dMesh->p2eDim[i]+j], (k+1)*sizeof(double) );

    cblas_dgemv( CblasColMajor, CblasNoTrans, dV, dLambda, 1.0, localSolvers[i], dV,
		 phi_E, 1, 0.0, tmp, 1 );
    cblas_daxpy( dV, 1.0, localSolvers[i]+dV*dLambda, 1, tmp, 1 );

    u[i]      = (double *) malloc (dPk * sizeof(double));
    lambda[i] = (double *) malloc (dLambda * sizeof(double));

    memcpy( u[i], tmp, dPk * sizeof(double) );
    memcpy( lambda[i], tmp+dPk, dLambda * sizeof(double) );

    free(phi_E);
    free(tmp);
  }
}


void computeErrors( const int k,
		    const dualMesh2d * const dMesh,
		    const double * const diameters,
		    const double * const centroids,
		    /* Polygonal quadrature rules with degree of exactness >= 2*k */
		    double ** const xquad2dbyEle,
		    double ** const yquad2dbyEle,
		    double ** const wquad2dbyEle,
		    int * const Nq2dbyEle,
		    double ** const u,
		    double * L2err,
		    double * H1err )
{
  int i, j, dPk;

  double exactSol, grad_exactSol_x, grad_exactSol_y;
  double L2norm, H1seminorm;
  double tmp1, tmp2;

  double *basis_on_quad_pts2d = NULL;
  double *grad_basis_on_quad_pts2d_x = NULL, *grad_basis_on_quad_pts2d_y = NULL;

  dPk = ((k+1)*(k+2)) / 2;

  basis_on_quad_pts2d        = (double *) malloc (dPk * sizeof(double));
  grad_basis_on_quad_pts2d_x = (double *) calloc (dPk, sizeof(double));
  grad_basis_on_quad_pts2d_y = (double *) calloc (dPk, sizeof(double));

  L2norm     = 0.0;
  H1seminorm = 0.0;
  *L2err     = 0.0;
  *H1err     = 0.0;
  for (i = 0; i < dMesh->Npol; ++i) {
    for (j = 0; j < Nq2dbyEle[i]; ++j) {
      dgExact( xquad2dbyEle[i][j], yquad2dbyEle[i][j], &exactSol );
      dgGradExact( xquad2dbyEle[i][j], yquad2dbyEle[i][j], &grad_exactSol_x, &grad_exactSol_y );

      compute_Pk_2d( k, xquad2dbyEle[i][j], yquad2dbyEle[i][j],
		     centroids[2*i], centroids[2*i+1], diameters[i],
		     basis_on_quad_pts2d );
      compute_Gk_2d( k-1, xquad2dbyEle[i][j], yquad2dbyEle[i][j],
		     centroids[2*i], centroids[2*i+1], diameters[i],
		     grad_basis_on_quad_pts2d_x+1, grad_basis_on_quad_pts2d_y+1 );

      L2norm     += wquad2dbyEle[i][j] * exactSol * exactSol;
      H1seminorm += wquad2dbyEle[i][j] *
	( grad_exactSol_x * grad_exactSol_x + grad_exactSol_y * grad_exactSol_y );

      tmp1 = exactSol - cblas_ddot(dPk, basis_on_quad_pts2d, 1, u[i], 1);
      *L2err += wquad2dbyEle[i][j] * tmp1 * tmp1;

      tmp1 = grad_exactSol_x - cblas_ddot(dPk, grad_basis_on_quad_pts2d_x, 1, u[i], 1);
      tmp2 = grad_exactSol_y - cblas_ddot(dPk, grad_basis_on_quad_pts2d_y, 1, u[i], 1);

      *H1err += wquad2dbyEle[i][j] * (tmp1 * tmp1 + tmp2 * tmp2);
    }
  }

  *H1err = sqrt( ( *H1err + *L2err ) / ( L2norm + H1seminorm ) );
  *L2err = sqrt( *L2err / L2norm );

  free(basis_on_quad_pts2d);
  free(grad_basis_on_quad_pts2d_x);
  free(grad_basis_on_quad_pts2d_y);
}


void dgPoisson( const int k,
		const double delta,
		const dualMesh2d * const dMesh,
		const double t,
		const double alpha,
		double * const diameters,
		double * const centroids,
		double ** const u,
		double ** const lambda,
		double * const uL2err,
		double * const uH1err )
{
  int i, j, status, dPhi;
  char cmd[CMD_MAXLENGTH];

  int Ngauss1d, Ngauss2d;
  double *gaussNodes1d = NULL, *gaussWeights1d = NULL;
  double *gaussNodes2d = NULL, *gaussWeights2d = NULL;
  double *scaledLegendre = NULL;
  
  double maxArea;

  primalMesh2d pMesh;
  double *gradxx = NULL, *gradxy = NULL, *gradyy = NULL, *GNeu = NULL, *widehatW = NULL;

  int *Nq2dbyEle = NULL;
  double **xquad2dbyEle = NULL, **yquad2dbyEle = NULL, **wquad2dbyEle = NULL;
  double **localSolvers = NULL;

  double *H = NULL, *RHS = NULL;
  lapack_int *ipiv = NULL;

#ifdef DEBUG
  double checkNorm;
  double *tmp1 = NULL, *tmp2 = NULL;
#endif

  /*
   * Generate reference Stroud quadrature rules
   */

  StroudQuadrature( 2*k, 1, &Ngauss1d, &gaussNodes1d, &gaussWeights1d );
  StroudQuadrature( 2*k, 2, &Ngauss2d, &gaussNodes2d, &gaussWeights2d );

  /* Legendre basis on [0,1] scaled by GL weights */
  scaledLegendre = (double *) malloc (Ngauss1d * (k+1) * sizeof(double));
  for (i = 0; i < Ngauss1d; ++i)
    legendre01( k, gaussNodes1d[i+Ngauss1d], scaledLegendre+i*(k+1) );
  for (i = 0; i < Ngauss1d; ++i) {
    for (j = 0; j <= k; ++j) {
      scaledLegendre[(k+1)*i+j] *= gaussWeights1d[i];
    }
  }
  
  /*
   * Generate mesh of reference triangle
   */

  primalMesh2dInitialize( &pMesh );

  /* Run Triangle */

  printf("Generating reference mesh ...\n");

  maxArea = delta * delta * 0.5;

  if ( snprintf( cmd, CMD_MAXLENGTH,
		 "/home/daniele/Documents/codes/triangle/triangle -Qpq30.0ea%f "
		 "/home/daniele/Documents/codes/triangle/meshes/referenceTriangle.poly",
		 maxArea ) > CMD_MAXLENGTH ) {
    printf( "File %s line %i: encountered error while generating reference triangular mesh, "
	    "aborting ...\n",
	    __FILE__, __LINE__ );
    exit(1);
  } else {
    status = system( cmd );
    printf("Status returned by Triangle: %i\n", status);
  }

  readTriangle( "/home/daniele/Documents/codes/triangle/meshes/referenceTriangle.1", &pMesh );

  /*
   * Compute reference basis
   */

  gradxx   = (double *) malloc (pMesh.Nver * pMesh.Nver * sizeof(double));
  gradxy   = (double *) malloc (pMesh.Nver * pMesh.Nver * sizeof(double));
  gradyy   = (double *) malloc (pMesh.Nver * pMesh.Nver * sizeof(double));
  GNeu     = (double *) malloc (pMesh.Nver * (k+1) * sizeof(double));
  widehatW = (double *) malloc (pMesh.Nver * (k+1) * sizeof(double));

  computeWidehatW( &pMesh, k, gaussNodes1d, gaussWeights1d, Ngauss1d,
  		   gradxx, gradxy, gradyy, GNeu, widehatW );

  /*
   * Assemble flux operators
   */

  xquad2dbyEle = (double **) malloc (dMesh->Npol * sizeof(double *));
  yquad2dbyEle = (double **) malloc (dMesh->Npol * sizeof(double *));
  wquad2dbyEle = (double **) malloc (dMesh->Npol * sizeof(double *));
  Nq2dbyEle    = (int *) malloc (dMesh->Npol * sizeof(int));
  localSolvers = (double **) malloc (dMesh->Npol * sizeof(double *));

  dPhi = dMesh->Ned * (k+1);
  H    = (double *) malloc (dPhi * dPhi * sizeof(double));
  RHS  = (double *) malloc (dPhi * sizeof(double));

  assembleGlobalFluxOperators( k, dMesh, &pMesh, gradxx, gradxy, gradyy, GNeu, widehatW,
  			       scaledLegendre, Ngauss1d, gaussNodes1d, gaussWeights1d,
  			       Ngauss2d, gaussNodes2d, gaussWeights2d,
  			       t, alpha, diameters, centroids,
  			       xquad2dbyEle, yquad2dbyEle, wquad2dbyEle, Nq2dbyEle,
  			       localSolvers, H, RHS );

#ifdef DEBUG
#ifdef EXACT_IS_CONST
  tmp1 = (double *) calloc (dPhi, sizeof(double));
  tmp2 = (double *) calloc (dPhi, sizeof(double));

  for (i = 0; i < dMesh->Ned; ++i) tmp1[i*(k+1)] = 1.0;

  cblas_dgemv( CblasColMajor, CblasNoTrans, dPhi, dPhi, 1.0, H, dPhi, tmp1, 1, 0.0, tmp2, 1 );
  cblas_daxpy( dPhi, -1.0, RHS, 1, tmp2, 1 );
  checkNorm = cblas_dnrm2(dPhi, tmp2, 1);

  printf("Norm of residual before applying B.C.: %.16f\n", checkNorm);
#endif
#endif

  /*
   * Apply boundary conditions
   */

  applyNeumannBC( k, dMesh, scaledLegendre, Ngauss1d, gaussNodes1d, RHS );
  applyDirichletBC( k, dMesh, scaledLegendre, Ngauss1d, gaussNodes1d, H, RHS );

#ifdef DEBUG
#ifdef EXACT_IS_CONST
  cblas_dgemv( CblasColMajor, CblasNoTrans, dPhi, dPhi, 1.0, H, dPhi, tmp1, 1, 0.0, tmp2, 1 );
  cblas_daxpy( dPhi, -1.0, RHS, 1, tmp2, 1 );
  checkNorm = cblas_dnrm2(dPhi, tmp2, 1);

  printf("Norm of residual after applying B.C.: %.16f\n", checkNorm);

  free( tmp1 );
  free( tmp2 );
#endif
#endif

  /*
   * Solve system
   */

  ipiv = (lapack_int *) malloc (dPhi * sizeof(lapack_int));

  LAPACKE_dgesv( LAPACK_COL_MAJOR, (lapack_int)(dPhi),
  		 (lapack_int)(1), H, (lapack_int)(dPhi), ipiv, RHS, (lapack_int)(dPhi) );

  free( ipiv );

  /*
   * Reconstruct local variables
   */

  reconstructVariables( k, dMesh, localSolvers, RHS, u, lambda );

  /*
   * Compute errors
   */

  computeErrors( k, dMesh, diameters, centroids,
  		 xquad2dbyEle, yquad2dbyEle, wquad2dbyEle, Nq2dbyEle,
  		 u, uL2err, uH1err );

  /*
   * Free memory
   */

  for (i = 0; i < dMesh->Npol; ++i) {
    free( xquad2dbyEle[i] );
    free( yquad2dbyEle[i] );
    free( wquad2dbyEle[i] );
    free( localSolvers[i] );
  }

  free( gaussNodes1d );
  free( gaussWeights1d );
  free( scaledLegendre );
  free( gaussNodes2d );
  free( gaussWeights2d );
  free( gradxx );
  free( gradxy );
  free( gradyy );
  free( GNeu );
  free( widehatW );
  free( xquad2dbyEle );
  free( yquad2dbyEle );
  free( wquad2dbyEle );
  free( Nq2dbyEle );
  free( localSolvers );
  free( H );
  free( RHS );

  primalMesh2dDestroy( &pMesh );
}

