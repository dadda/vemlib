#include "dglib.h"


void computeWidehatW( const primalMesh2d * const pMesh,
		      const int k,
		      const double * const quadNodes1d,
		      const double * const quadWeights1d,
		      const int Nq1d,
		      double * const gradxx,
		      double * const gradxy,
		      double * const gradyy,
		      double * const GNeu,
		      double * const widehatW )
{
  int i, j, l, idx, v1Id, v2Id, v3Id;

  /* We are using continuous piecewise-linear finite elements as \widehat V_\delta */
  int N = pMesh->Nver;

  double area, BIT11, BIT12, BIT21, BIT22;
  double tmp1, tmp2, tmp3, tmp4;
  double grad[2][3];

  double *v1Addr = NULL, *v2Addr = NULL, *v3Addr = NULL;

  /* We are assuming that quadNodes1d and quadWeights1d
     define a 1d Gauss-Legendre rule on [0,1] with k+1 nodes */
  double *nodes = NULL, *lambda = NULL;

  double *S = NULL;
  lapack_int *ipiv = NULL;

#ifdef DEBUG
  double *Scheck = NULL;
#endif

  /*
   * Assemble global stiffness matrix on reference triangle
   */

  S = (double *) calloc (N * N, sizeof(double));

  memset(gradxx, 0, N * N * sizeof(double));
  memset(gradxy, 0, N * N * sizeof(double));
  memset(gradyy, 0, N * N * sizeof(double));

  for (i = 0; i < pMesh->Nelt; ++i) {
    v1Id = pMesh->elements[3*i];
    v2Id = pMesh->elements[3*i+1];
    v3Id = pMesh->elements[3*i+2];

    v1Addr = pMesh->vertices + 2*v1Id;
    v2Addr = pMesh->vertices + 2*v2Id;
    v3Addr = pMesh->vertices + 2*v3Id;

    area = 0.5 * fabs( orient2dfast( v1Addr, v2Addr, v3Addr ) );

    /* Inverse transpose of the local Jacobian scaled by 2 * local area */
    BIT11 = *(v3Addr+1) - *(v1Addr+1);
    BIT21 = *v1Addr - *v3Addr;
    BIT12 = *(v1Addr+1) - *(v2Addr+1);
    BIT22 = *v2Addr - *v1Addr;

    grad[0][0] = - ( BIT11 + BIT12 ); grad[0][1] = BIT11; grad[0][2] = BIT12;
    grad[1][0] = - ( BIT21 + BIT22 ); grad[1][1] = BIT21; grad[1][2] = BIT22;

    /* Assemble local contribution into global matrix S */

    /* tmp1 = - ( BIT11 + BIT12 ); */
    /* tmp2 = - ( BIT21 + BIT22 ); */
    /* tmp3 = 1.0 / (4.0 * area); */

    tmp1 = 1.0 / (4.0 * area);

    idx = N*v1Id + v1Id;
    tmp2 = tmp1 * grad[0][0] * grad[0][0];
    tmp3 = tmp1 * grad[1][0] * grad[1][0];
    gradxx[idx] += tmp2;
    gradxy[idx] += tmp1 * 2.0 * grad[0][0] * grad[1][0];
    gradyy[idx] += tmp3;
    S[idx]      += tmp2 + tmp3;

    idx = N*v1Id + v2Id;
    tmp2 = tmp1 * grad[0][1] * grad[0][0];
    tmp3 = tmp1 * (grad[0][1]*grad[1][0] + grad[1][1]*grad[0][0]);
    tmp4 = tmp1 * grad[1][0] * grad[1][1];
    gradxx[idx] += tmp2;
    gradxy[idx] += tmp3;
    gradyy[idx] += tmp4;
    S[idx]      += tmp2 + tmp4;

    idx = N*v2Id + v1Id;
    gradxx[idx] += tmp2;
    gradxy[idx] += tmp3;
    gradyy[idx] += tmp4;
    S[idx]      += tmp2 + tmp4;

    idx = N*v1Id + v3Id;
    tmp2 = tmp1 * grad[0][2] * grad[0][0];
    tmp3 = tmp1 * (grad[0][2]*grad[1][0] + grad[1][2]*grad[0][0]);
    tmp4 = tmp1 * grad[1][0] * grad[1][2];
    gradxx[idx] += tmp2;
    gradxy[idx] += tmp3;
    gradyy[idx] += tmp4;
    S[idx]      += tmp2 + tmp4;

    idx = N*v3Id + v1Id;
    gradxx[idx] += tmp2;
    gradxy[idx] += tmp3;
    gradyy[idx] += tmp4;
    S[idx]      += tmp2 + tmp4;

    idx = N*v2Id + v2Id;
    tmp2 = tmp1 * grad[0][1] * grad[0][1];
    tmp3 = tmp1 * 2.0 * grad[0][1]*grad[1][1];
    tmp4 = tmp1 * grad[1][1] * grad[1][1];
    gradxx[idx] += tmp2;
    gradxy[idx] += tmp3;
    gradyy[idx] += tmp4;
    S[idx]      += tmp2 + tmp4;

    idx = N*v2Id + v3Id;
    tmp2 = tmp1 * grad[0][2] * grad[0][1];
    tmp3 = tmp1 * (grad[0][2]*grad[1][1] + grad[1][2]*grad[0][1]);
    tmp4 = tmp1 * grad[1][2] * grad[1][1];
    gradxx[idx] += tmp2;
    gradxy[idx] += tmp3;
    gradyy[idx] += tmp4;
    S[idx]      += tmp2 + tmp4;

    idx = N*v3Id + v2Id;
    gradxx[idx] += tmp2;
    gradxy[idx] += tmp3;
    gradyy[idx] += tmp4;
    S[idx]      += tmp2 + tmp4;

    idx = N*v3Id + v3Id;
    tmp2 = tmp1 * grad[0][2] * grad[0][2];
    tmp3 = tmp1 * 2.0 * grad[0][2]*grad[1][2];
    tmp4 = tmp1 * grad[1][2] * grad[1][2];
    gradxx[idx] += tmp2;
    gradxy[idx] += tmp3;
    gradyy[idx] += tmp4;
    S[idx]      += tmp2 + tmp4;
  }

  /*
   * Impose Neumann boundary conditions
   *
   * We choose Legendre polynomials as basis for P_k([0,1])
   */

  nodes  = (double *) malloc (Nq1d * sizeof(double));
  lambda = (double *) malloc (Nq1d * (k+1) * sizeof(double));

  memset(widehatW, 0, N * (k+1) * sizeof(double));

  for (i = 0; i < pMesh->Ned; ++i) {
    if ( pMesh->eTopFlags[i] == 2 ) {
      v1Id = pMesh->edges[2*i];
      v2Id = pMesh->edges[2*i+1];

      v1Addr = pMesh->vertices + 2*v1Id;
      v2Addr = pMesh->vertices + 2*v2Id;

      tmp1 = fabs( *v1Addr - *v2Addr ); /* edge length */

      /* 0-order terms, first column of basis array  */
      widehatW[ v1Id ] += 0.5*tmp1;
      widehatW[ v2Id ] += 0.5*tmp1;

      if (k >= 1) {
	for (j = 0; j < Nq1d; ++j) {
	  nodes[j]       = quadNodes1d[j] * (*v1Addr) + quadNodes1d[Nq1d+j] * (*v2Addr);
	  lambda[j]      = 1.0;                  /* order 0 */
	  lambda[Nq1d+j] = 2.0 * nodes[j] - 1.0; /* order 1 */
	}

	for (l = 2; l <= k; ++l) {
	  for (j = 0; j < Nq1d; ++j) {
	    lambda[Nq1d*l+j] = (2.0 * (double)l - 1.0) / ((double)l)
	      * (2.0 * nodes[j] - 1.0) * lambda[Nq1d*(l-1)+j]
	      - ((double)l - 1.0)/((double)l) * lambda[Nq1d*(l-2)+j];
	  }
	}

	for (l = 1; l <= k; ++l) {
	  tmp2 = 0.0; /* temporary variable for widehatW[N*l+v1Id] */
	  tmp3 = 0.0; /* temporary variable for widehatW[N*l+v2Id] */

	  for (j = 0; j < Nq1d; ++j) {
	    tmp2 += quadWeights1d[j] * quadNodes1d[j] * lambda[Nq1d*l+j];
	    tmp3 += quadWeights1d[j] * quadNodes1d[Nq1d+j] * lambda[Nq1d*l+j];
	  }

	  widehatW[ N*l+v1Id ] += tmp1 * tmp2;
	  widehatW[ N*l+v2Id ] += tmp1 * tmp3;
	}
      }
    }
  }

  free( nodes );
  free( lambda );

  /*
   * Impose Dirichlet boundary conditions
   *
   * Notation: flag = 0, internal edge; flag = 1, Dirichlet edge; flag = 2, Neumann edge.
   */

  for (i = 0; i < pMesh->Ned; ++i) {
    if ( pMesh->eTopFlags[i] == 1 ) {
      v1Id = pMesh->edges[2*i];
      v2Id = pMesh->edges[2*i+1];

      for (j = 0; j < N; ++j) {
	idx = N*j + v1Id;
	gradxx[idx] = 0.0; gradxy[idx] = 0.0; gradyy[idx] = 0.0; S[idx] = 0.0;
	idx = N*j + v2Id;
	gradxx[idx] = 0.0; gradxy[idx] = 0.0; gradyy[idx] = 0.0; S[idx] = 0.0;
	idx = N*v1Id + j;
	gradxx[idx] = 0.0; gradxy[idx] = 0.0; gradyy[idx] = 0.0; S[idx] = 0.0;
	idx = N*v2Id + j;
	gradxx[idx] = 0.0; gradxy[idx] = 0.0; gradyy[idx] = 0.0; S[idx] = 0.0;
      }

      idx = N*v1Id + v1Id;
      gradxx[idx] = 1.0; gradxy[idx] = 1.0; gradyy[idx] = 1.0; S[idx] = 1.0;
      idx = N*v2Id + v2Id;
      gradxx[idx] = 1.0; gradxy[idx] = 1.0; gradyy[idx] = 1.0; S[idx] = 1.0;

      for (j = 0; j <= k; ++j) {
	widehatW[ N*j+v1Id ] = 0.0;
	widehatW[ N*j+v2Id ] = 0.0;
      }
    }
  }

#ifdef DEBUG
  Scheck = (double *) malloc (N * N * sizeof(double));
  memcpy(Scheck, gradxx, N * N * sizeof(double));
  cblas_daxpy(N*N, 1.0, gradyy, 1, Scheck, 1);
  cblas_daxpy(N*N, -1.0, S, 1, Scheck, 1);
  tmp1 = cblas_dnrm2(N*N, Scheck, 1);
  printf("\nCheck decomposition: %.16e\n", tmp1);
  free(Scheck);
#endif

  /*
   * Solve a single linear system with multiple right hand sides
   * (dsysv does overwrite the coefficient matrix S)
   */

  ipiv = (lapack_int *) malloc (N * sizeof(lapack_int));

  memcpy(GNeu, widehatW, N * (k+1) * sizeof(double));

  /* LAPACKE_dsysv( LAPACK_COL_MAJOR, 'L', (lapack_int)(N), */
  /* 		 (lapack_int)(k+1), S, (lapack_int)(N), */
  /* 		 ipiv, widehatW, (lapack_int)(N) ); */
  LAPACKE_dgesv( LAPACK_COL_MAJOR, (lapack_int)(N),
		 (lapack_int)(k+1), S, (lapack_int)(N),
		 ipiv, widehatW, (lapack_int)(N) );

  free(ipiv);
  free(S);
}


void stabilizationOnSubTriangle( const int k,
				 const int eIdx,
				 const int NvLoc,
				 const double * const v1,
				 const double * const v2,
				 const double * const centroid,
				 const double h,
				 const double eLength,
				 const double perimeter,
				 const primalMesh2d * const pMeshHat,
				 const double * const gradxx,
				 const double * const gradxy,
				 const double * const gradyy,
				 const double * const GNeu,
				 const double * const widehatW,
				 const double fIntegral,
				 const double * const gaussNodes1d,   /* must be col major */
				 const double * const gaussWeights1d,
				 const int Ngauss1d,
				 const double * const gaussNodes2d,   /* must be col major */
				 const double * const gaussWeights2d,
				 const int Ngauss2d,
				 const double t,
				 const double alpha,
#ifdef DEBUG
				 double * const tildeA_E,
				 double * const tildeBT_E,
				 double * const tildeF_E,
#endif
				 double * const Z_E,
				 double * const G_E
				 )
{
  int i, j, l, dPk, NvHat;
  int v1hatId, v2hatId, v3hatId;

  double *v1hatAddr = NULL, *v2hatAddr = NULL, *v3hatAddr = NULL;

  double Bi[4], verticesHat[6], vertices[6];
  double Nx, Ny;
  double detBi, measureHat, tmp, ratioSidePerimeter = eLength/perimeter;
  double a, b, c;
  double *Sscaled = NULL, *Si = NULL;
  double *tmp1 = NULL, *tmp2 = NULL, *tmp3 = NULL, *tmp4 = NULL;
  double *tmp5 = NULL, *tmp6 = NULL, *tmp7 = NULL, *tmp8 = NULL;
  double *tildeAT = NULL, *tildeB = NULL, *tildeF = NULL;

  lapack_int *ipiv = NULL;

  dPk   = ((k+1)*(k+2)) / 2;
  NvHat = pMeshHat->Nver;

  Sscaled = (double *) calloc (NvHat * NvHat, sizeof(double));
  tmp1    = (double *) calloc (NvHat * (k+1), sizeof(double));
  Si      = (double *) calloc ((k+1) * (k+1), sizeof(double));

  /*
   * Local stiffness matrix Si
   */

  detBi = orient2dfast( v1, v2, centroid );
  Bi[0] = v2[0] - v1[0];
  Bi[1] = v2[1] - v1[1];
  Bi[2] = centroid[0] - v1[0];
  Bi[3] = centroid[1] - v1[1];

  tmp    = 0.5 / detBi;
  a      = tmp * (Bi[3]*Bi[3] + Bi[2]*Bi[2]);
  b      = -tmp * (Bi[1]*Bi[3] + Bi[0]*Bi[2]);
  c      = tmp * (Bi[1]*Bi[1] + Bi[0]*Bi[0]);

  cblas_daxpy(NvHat * NvHat, a, gradxx, 1, Sscaled, 1);
  cblas_daxpy(NvHat * NvHat, b, gradxy, 1, Sscaled, 1);
  cblas_daxpy(NvHat * NvHat, c, gradyy, 1, Sscaled, 1);

  cblas_dgemm( CblasColMajor, CblasNoTrans, CblasNoTrans, NvHat, k+1, NvHat,
	       1.0, Sscaled, NvHat, widehatW, NvHat, 0.0, tmp1, NvHat);
  cblas_dgemm( CblasColMajor, CblasTrans, CblasNoTrans, k+1, k+1, NvHat,
	       1.0, widehatW, NvHat, tmp1, NvHat, 0.0, Si, k+1);

  free(tmp1);
  free(Sscaled);

  /*
   * \tilde A_i^T = \int_{T_i} \nabla v\cdot\nabla\phi_l, with \phi_l \in W_i
   *
   * \tilde B_i   = \int_{\partial T_i} (\lambda - \bar\lambda)\phi_l
   *
   * \tilde F_i   = <(1-\tilde\pi)f, \phi_l>_{T_i}
   */

  tildeAT = (double *) calloc (dPk * (k+1), sizeof(double));
  tildeB  = (double *) calloc ((k+1) * (k+1), sizeof(double));
  tildeF  = (double *) calloc (k+1, sizeof(double));

  /* Part 1 of \tilde A_i^T: -\int_{T_i} \Delta v \phi_l */

  /*
   * Compute the reference Lagrangian basis scaled by the gauss weights (row major)
   *
   * Note that the Lagrangian basis coincides with array gaussNodes2d.
   */

  tmp1 = (double *) malloc (Ngauss2d * 3 * sizeof(double));
  for (i = 0; i < Ngauss2d; ++i) {
    tmp1[3*i]   = gaussWeights2d[i] * gaussNodes2d[i];
    tmp1[3*i+1] = gaussWeights2d[i] * gaussNodes2d[i+Ngauss2d];
    tmp1[3*i+2] = gaussWeights2d[i] * gaussNodes2d[i+2*Ngauss2d];
  }

  tmp2 = (double *) calloc (dPk * NvHat, sizeof(double));
  tmp3 = (double *) calloc (Ngauss2d * 2, sizeof(double));
  tmp4 = (double *) malloc (Ngauss2d * dPk * sizeof(double));
  tmp5 = (double *) calloc (dPk * 3, sizeof(double));
  tmp6 = (double *) malloc (Ngauss2d * sizeof(double));
  tmp7 = (double *) calloc (NvHat, sizeof(double));
  tmp8 = (double *) calloc (3, sizeof(double));

  for (i = 0; i < pMeshHat->Nelt; ++i) {
    v1hatId = pMeshHat->elements[3*i];
    v2hatId = pMeshHat->elements[3*i+1];
    v3hatId = pMeshHat->elements[3*i+2];

    v1hatAddr = pMeshHat->vertices + 2*v1hatId;
    v2hatAddr = pMeshHat->vertices + 2*v2hatId;
    v3hatAddr = pMeshHat->vertices + 2*v3hatId;

    measureHat = 0.5 * fabs( orient2dfast( v1hatAddr, v2hatAddr, v3hatAddr ) );

    /* verticesHat is col major */
    verticesHat[0] = *v1hatAddr; verticesHat[1] = *(v1hatAddr+1);
    verticesHat[2] = *v2hatAddr; verticesHat[3] = *(v2hatAddr+1);
    verticesHat[4] = *v3hatAddr; verticesHat[5] = *(v3hatAddr+1);

    /* vertices is row major (or col major if one considers vertices^T ) */
    vertices[0] = Bi[0] * verticesHat[0] + Bi[2] * verticesHat[1] + v1[0];
    vertices[3] = Bi[1] * verticesHat[0] + Bi[3] * verticesHat[1] + v1[1];
    vertices[1] = Bi[0] * verticesHat[2] + Bi[2] * verticesHat[3] + v1[0];
    vertices[4] = Bi[1] * verticesHat[2] + Bi[3] * verticesHat[3] + v1[1];
    vertices[2] = Bi[0] * verticesHat[4] + Bi[2] * verticesHat[5] + v1[0];
    vertices[5] = Bi[1] * verticesHat[4] + Bi[3] * verticesHat[5] + v1[1];

    /* Quadrature nodes on true sub triangle */
    cblas_dgemm( CblasColMajor, CblasNoTrans, CblasNoTrans, Ngauss2d, 2, 3,
		 1.0, gaussNodes2d, Ngauss2d, vertices, 3, 0.0, tmp3, Ngauss2d);

    for (j = 0; j < Ngauss2d; ++j) {
      compute_lapPk_2d(k, tmp3[j], tmp3[j+Ngauss2d], centroid[0], centroid[1], h, tmp4+j*dPk);
      tmp6[j] = dgLoad(tmp3[j], tmp3[j+Ngauss2d]);
    }

    cblas_dgemm( CblasRowMajor, CblasTrans, CblasNoTrans, 3, dPk, Ngauss2d,
		 measureHat, tmp1, 3, tmp4, dPk, 0.0, tmp5, dPk );

    cblas_dgemv( CblasRowMajor, CblasTrans, Ngauss2d, 3, measureHat, tmp1, 3, tmp6, 1,
		 0.0, tmp8, 1 );

    /* Scatter - tmp2 is col major ordered, whereas tmp5 is row major */
    for (j = 0; j < dPk; ++j) {
      tmp2[NvHat*j + v1hatId] += tmp5[j];
      tmp2[NvHat*j + v2hatId] += tmp5[dPk+j];
      tmp2[NvHat*j + v3hatId] += tmp5[2*dPk+j];
    }

    tmp7[v1hatId] += tmp8[0];
    tmp7[v2hatId] += tmp8[1];
    tmp7[v3hatId] += tmp8[2];
  }

  cblas_dgemm( CblasColMajor, CblasTrans, CblasNoTrans, dPk, k+1, NvHat,
	       -detBi, tmp2, NvHat, widehatW, NvHat, 0.0, tildeAT, dPk );

  cblas_dgemm( CblasColMajor, CblasTrans, CblasNoTrans, 1, k+1, NvHat,
	       detBi, tmp7, NvHat, widehatW, NvHat, 0.0, tildeF, 1 );

  cblas_dgemm( CblasColMajor, CblasTrans, CblasNoTrans, 1, k+1, NvHat,
	       -fIntegral*ratioSidePerimeter, GNeu, NvHat, widehatW, NvHat, 1.0, tildeF, 1 );

  free(tmp1);
  free(tmp3);
  free(tmp4);
  free(tmp5);
  free(tmp6);
  free(tmp7);
  free(tmp8);

  /* Part 2 of \tilde A_i^T: \int_{\partial T_i} \nabla v\cdot\underline n \phi_l
   * and \tilde B_i */

  Nx = v2[1] - v1[1];
  Ny = v1[0] - v2[0];

  /*
   * Compute the 1d reference Lagrangian basis scaled by the 1d gauss weights (row major)
   *
   * Note that the Lagrangian basis coincides with array gaussNodes1d.
   */

  tmp1 = (double *) malloc (Ngauss1d * 2 * sizeof(double));
  for (i = 0; i < Ngauss1d; ++i) {
    tmp1[2*i]   = gaussWeights1d[i] * gaussNodes1d[i];
    tmp1[2*i+1] = gaussWeights1d[i] * gaussNodes1d[i+Ngauss1d];
  }

  memset(tmp2, 0, dPk * NvHat * sizeof(double));
  tmp3 = (double *) calloc (Ngauss1d * 2, sizeof(double));
  tmp4 = (double *) calloc (Ngauss1d * dPk, sizeof(double));
  tmp5 = (double *) calloc (dPk * 2, sizeof(double));
  tmp6 = (double *) calloc (dPk, sizeof(double)); /* will hold Gkx */
  tmp7 = (double *) calloc (dPk, sizeof(double)); /* will hold Gky */

  tmp8 = (double *) malloc ((k+1) * NvHat * sizeof(double));
  memcpy(tmp8, GNeu, (k+1) * NvHat * sizeof(double));

  for (i = 0; i < pMeshHat->Ned; ++i) {
    if ( pMeshHat->eTopFlags[i] == 2 ) {

      /* Map 1d quadrature nodes back to T_i */

      v1hatId = pMeshHat->edges[2*i];
      v2hatId = pMeshHat->edges[2*i+1];

      v1hatAddr = pMeshHat->vertices + 2*v1hatId;
      v2hatAddr = pMeshHat->vertices + 2*v2hatId;

      verticesHat[0] = *v1hatAddr; verticesHat[1] = *(v1hatAddr+1);
      verticesHat[2] = *v2hatAddr; verticesHat[3] = *(v2hatAddr+1);

      measureHat = fabs(verticesHat[0]-verticesHat[2]); /* we are on [0,1] x {0} */

      vertices[0] = Bi[0] * verticesHat[0] + Bi[2] * verticesHat[1] + v1[0];
      vertices[3] = Bi[1] * verticesHat[0] + Bi[3] * verticesHat[1] + v1[1];
      vertices[1] = Bi[0] * verticesHat[2] + Bi[2] * verticesHat[3] + v1[0];
      vertices[4] = Bi[1] * verticesHat[2] + Bi[3] * verticesHat[3] + v1[1];
      vertices[2] = 0.0; /* dummy value */
      vertices[5] = 0.0; /* dummy value */

      /* Note: K != LDB */
      cblas_dgemm( CblasColMajor, CblasNoTrans, CblasNoTrans, Ngauss1d, 2, 2,
		   1.0, gaussNodes1d, Ngauss1d, vertices, 3, 0.0, tmp3, Ngauss1d);

      /* Evaluate \nabla v on true boundary quadrature nodes */
      for (j = 0; j < Ngauss1d; ++j) {
	compute_Gk_2d( k-1, tmp3[j], tmp3[j+Ngauss1d], centroid[0], centroid[1], h,
		       tmp6+1, tmp7+1 );
	for (l = 0; l < dPk; ++l) {
	  tmp4[j*dPk+l] = Nx*tmp6[l] + Ny*tmp7[l];
	}
      }

      cblas_dgemm( CblasRowMajor, CblasTrans, CblasNoTrans, 2, dPk, Ngauss1d,
		   measureHat, tmp1, 2, tmp4, dPk, 0.0, tmp5, dPk );

      /* Scatter - tmp2 is col major ordered, whereas tmp5 is row major */
      for (j = 0; j < dPk; ++j) {
	tmp2[NvHat*j + v1hatId] += tmp5[j];
	tmp2[NvHat*j + v2hatId] += tmp5[dPk+j];
      }

      /* tmp8[v1hatId] -= measureHat * ratioSidePerimeter * 0.5; */
      /* tmp8[v2hatId] -= measureHat * ratioSidePerimeter * 0.5; */
    }
  }

  cblas_dgemm( CblasColMajor, CblasTrans, CblasNoTrans, dPk, k+1, NvHat,
	       1.0, tmp2, NvHat, widehatW, NvHat, 1.0, tildeAT, dPk );

  cblas_dgemm( CblasColMajor, CblasTrans, CblasNoTrans, k+1, k+1, NvHat,
	       eLength, tmp8, NvHat, widehatW, NvHat, 0.0, tildeB, k+1 );

  free(tmp1);
  free(tmp2);
  free(tmp3);
  free(tmp4);
  free(tmp5);
  free(tmp6);
  free(tmp7);
  free(tmp8);

#ifdef DEBUG
  memcpy( tildeA_E + dPk*(k+1)*eIdx, tildeAT, dPk*(k+1)*sizeof(double) ); /* row-major */

  for (i = 0; i <= k; ++i)
    for (j = 0; j <= k; ++j)
      tildeBT_E[NvLoc*(k+1)*(eIdx*(k+1)+i)+(eIdx*(k+1)+j)] = tildeB[(k+1)*i+j];

  memcpy( tildeF_E + (k+1)*eIdx, tildeF, (k+1)*sizeof(double) );
#endif

  /*
   * Solve linear system with matrix Si and right hand side [\tilde A_i \tilde B^T \tilde F]
   */

  tmp1 = (double *) malloc ( ( (k+1) * (dPk + k+1 + 1) ) * sizeof(double) );
  for (j = 0; j < dPk; ++j) {
    for (i = 0; i <= k; ++i)
      tmp1[(k+1)*j+i] = tildeAT[dPk*i+j];  /* tildeAT is col-major */
  }
  for (j = dPk; j < dPk+k+1; ++j) {
    for (i = 0; i <= k; ++i)
      tmp1[(k+1)*j+i] = tildeB[(k+1)*i+j-dPk]; /* tildeB is col-major as well */
  }
  for (i = 0; i <= k; ++i)
    tmp1[(k+1)*(dPk+k+1)+i] = tildeF[i];

  ipiv = (lapack_int *) malloc ((k+1) * sizeof(lapack_int));

  /* LAPACKE_dsysv( LAPACK_COL_MAJOR, 'L', (lapack_int)(k+1), */
  /* 		 (lapack_int)(dPk+k+2), Si, (lapack_int)(k+1), */
  /* 		 ipiv, tmp1, (lapack_int)(k+1) ); */
  LAPACKE_dgesv( LAPACK_COL_MAJOR, (lapack_int)(k+1),
  		 (lapack_int)(dPk+k+2), Si, (lapack_int)(k+1),
  		 ipiv, tmp1, (lapack_int)(k+1) );

  /*
   * Fill all the right spots of matrices Z_E and G_E
   */

  /* Z_E */

  cblas_dgemm( CblasColMajor, CblasNoTrans, CblasNoTrans, dPk, dPk, k+1,
	       t*alpha, tildeAT, dPk, tmp1, k+1, 1.0, Z_E, dPk+(k+1)*NvLoc );

  cblas_dgemm( CblasColMajor, CblasNoTrans, CblasNoTrans, k+1, dPk, k+1,
	       -alpha, tildeB, k+1, tmp1, k+1, 1.0,
	       Z_E+dPk+eIdx*(k+1), dPk+(k+1)*NvLoc );

  cblas_dgemm( CblasColMajor, CblasNoTrans, CblasNoTrans, dPk, k+1, k+1,
	       -t*alpha, tildeAT, dPk, tmp1+dPk*(k+1), k+1, 1.0,
	       Z_E+(dPk+(k+1)*NvLoc)*(dPk+eIdx*(k+1)), dPk+(k+1)*NvLoc );

  cblas_dgemm( CblasColMajor, CblasNoTrans, CblasNoTrans, k+1, k+1, k+1,
	       alpha, tildeB, k+1, tmp1+dPk*(k+1), k+1, 1.0,
	       Z_E+(dPk+(k+1)*NvLoc)*(dPk+eIdx*(k+1))+dPk+eIdx*(k+1), dPk+(k+1)*NvLoc );

  /* G_E */

  cblas_dgemv( CblasColMajor, CblasNoTrans, dPk, k+1,
	       t*alpha, tildeAT, dPk, tmp1+(k+1)*(dPk+k+1), 1, 1.0, G_E, 1 );

  cblas_dgemv( CblasColMajor, CblasNoTrans, k+1, k+1,
	       -alpha, tildeB, k+1, tmp1+(k+1)*(dPk+k+1), 1, 1.0, G_E+dPk+eIdx*(k+1), 1 );

  free(ipiv);
  free(tmp1);
  free(Si);
  free(tildeAT);
  free(tildeB);
  free(tildeF);
}
