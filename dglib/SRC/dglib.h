#ifndef __DGLIB_H
#define __DGLIB_H

#include "pmgo.h"
#include "polygauss.h"
#include "basisFunctions.h"

#define CMD_MAXLENGTH 512

/*
 * Functions to be defined case by case
 */

void   dgExact( const double, const double, double * const );
void   dgGradExact( const double, const double, double * const , double * const );
double dgLoad( const double, const double );

void labelEdges( dualMesh2d * const dMesh );
void dgDirichlet( const double, const double, double * const );
void dgNeumann( const int, const double, const double, double * const, double * const );

/*
 *
 */

void computeWidehatW( const primalMesh2d * const pMesh,
		      const int k,
		      const double * const quadNodes1d,
		      const double * const quadWeights1d,
		      const int Nq1d,
		      double * const gradxx,
		      double * const gradxy,
		      double * const gradyy,
		      double * const GNeu,
		      double * const widehatW );

void stabilizationOnSubTriangle( const int k,
				 const int eIdx,
				 const int NvLoc,
				 const double * const v1,
				 const double * const v2,
				 const double * const centroid,
				 const double h,
				 const double eLength,
				 const double perimeter,
				 const primalMesh2d * const pMeshHat,
				 const double * const gradxx,
				 const double * const gradxy,
				 const double * const gradyy,
				 const double * const GNeu,
				 const double * const widehatW,
				 const double fIntegral,
				 /* 1D Stroud quadrature rule with degree of exactness = k.
				    Ordering: col-major */
				 const double * const gaussNodes1d,
				 const double * const gaussWeights1d,
				 const int Ngauss1d,
				 const double * const gaussNodes2d,
				 /* 2D Stroud quadrature rule with degree of exactness = k-1.
				    Ordering: col-major */
				 const double * const gaussWeights2d,
				 const int Ngauss2d,
				 const double t,
				 const double alpha,
#ifdef DEBUG
				 double * const tildeA_E,
				 double * const tildeBT_E,
				 double * const tildeF_E,
#endif
				 double * const Z_E,
				 double * const G_E );

void assembleGlobalFluxOperators( const int k,
				  const dualMesh2d * const dMesh,
				  const primalMesh2d * const pMeshHat,
				  const double * const gradxx,
				  const double * const gradxy,
				  const double * const gradyy,
				  const double * const GNeu,
				  const double * const widehatW,
				  /* Legendre polynomials of degree <= k,
				     evaluated at gaussNodes1d, and scaled by gaussWeights1d.
				     Ordering: row-major */
				  const double * const scaledLegendre,
				  /* 1D Stroud quadrature rule with degree of exactness = 2*k.
				     Ordering: col-major */
				  const int Ngauss1d,
				  const double * const gaussNodes1d, 
				  const double * const gaussWeights1d,
				  /* 2D Stroud quadrature rule with degree of exactness = k-1.
				     Ordering: col-major */
				  const int Ngauss2d,
				  const double * const gaussNodes2d,
				  const double * const gaussWeights2d,
				  const double t,
				  const double alpha,
				  double * const diameters,
				  double * const centroids,
				  /* Polygonal quadrature rules with degree of exactness >= 2*k */
				  double ** const xquad2dbyEle,
				  double ** const yquad2dbyEle,
				  double ** const wquad2dbyEle,
				  int * const Nq2dbyEle,
				  double ** const localSolvers,
				  double * const H,
				  double * const RHS );

void assembleLocalFluxOperators( const int * const allEdges,
				 const int NvLoc,
				 const int * const p2vLoc,
				 const int * const p2eLoc,
				 const double * const polygon,
				 const double * const edgeLengths,
				 const double * const centroid,
				 const double diameter,
				 const int k,
				 const primalMesh2d * const pMeshHat,
				 const double * const gradxx,
				 const double * const gradxy,
				 const double * const gradyy,
				 const double * const GNeu,
				 const double * const widehatW,
				 /* Legendre polynomials of degree <= k,
				    evaluated at gaussNodes1d, and scaled by gaussWeights1d.
				    Ordering: row-major */
				 const double * const scaledLegendre,
				 /* 1D Stroud quadrature rule with degree of exactness = 2*k.
				    Ordering: col-major */
				 const int Ngauss1d,
				 const double * const gaussNodes1d, 
				 const double * const gaussWeights1d,
				 /* 2D Stroud quadrature rule with degree of exactness = k-1.
				    Ordering: col-major */
				 const int Ngauss2d,
				 const double * const gaussNodes2d,
				 const double * const gaussWeights2d,
				  /* Polygonal quadrature rule with degree of exactness >= 2*k */
				 const int Nq2d_E,
				 const double * const xquad2d_E,
				 const double * const yquad2d_E,
				 const double * const wquad2d_E,
				 const double t,
				 const double alpha,
				 double * const localSolver,
				 int * const dofs,
				 double * const H_E,
				 double * const RHS_E );

void applyNeumannBC( const int k,
		     const dualMesh2d * const dMesh,
		     /* Legendre polynomials of degree <= k,
			evaluated at gaussNodes1d, and scaled by gaussWeights1d.
			Ordering: row-major */
		     const double * const scaledLegendre,
		     /* 1D Stroud quadrature rule with degree of exactness = 2*k.
			Ordering: col-major */
		     const int Ngauss1d,
		     const double * const gaussNodes1d, 
		     double * const RHS );

void applyDirichletBC( const int k,
		       const dualMesh2d * const dMesh,
		       /* Legendre polynomials of degree <= k,
			  evaluated at gaussNodes1d, and scaled by gaussWeights1d.
			  Ordering: row-major */
		       const double * const scaledLegendre,
		       /* 1D Stroud quadrature rule with degree of exactness = 2*k.
			  Ordering: col-major */
		       const int Ngauss1d,
		       const double * const gaussNodes1d, 
		       double * const H,
		       double * const RHS );

void reconstructVariables( const int k,
			   const dualMesh2d * const dMesh,
			   double ** const localSolvers,
			   const double * const X,
			   double ** const u,
			   double ** const lambda );

void computeErrors( const int k,
		    const dualMesh2d * const dMesh,
		    const double * const diameters,
		    const double * const centroids,
		    /* Polygonal quadrature rules with degree of exactness >= 2*k */
		    double ** const xquad2dbyEle,
		    double ** const yquad2dbyEle,
		    double ** const wquad2dbyEle,
		    int * const Nq2dbyEle,
		    double ** const u,
		    double * L2err,
		    double * H1err );

void dgPoisson( const int k,
		const double delta,
		const dualMesh2d * const dMesh,
		const double t,
		const double alpha,
		double * const diameters,
		double * const centroids,
		double ** const u,
		double ** const lambda,
		double * const uL2err,
		double * const uH1err );

#endif

