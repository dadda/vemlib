function y = actionFIrc2d( x, Nx, Ny, K, dimGlobalLambda, ...
                           primalDofs, dualDofs, internalDofs, ...
                           Bprimal, Bdual, ...
                           runAsFor )

y = zeros(dimGlobalLambda, 1);

numSubdomains = Nx * Ny;
parfor ( s = 1:numSubdomains, getParforArg(runAsFor) )

    tmp = K{s}( [ internalDofs{s}; dualDofs{s} ], [ internalDofs{s}; dualDofs{s} ] ) \ ...
        ( K{s}( [ internalDofs{s}; dualDofs{s} ], primalDofs{s} ) * (Bprimal{s} * x) );

    y = y + Bdual{s} * tmp;

end

end
