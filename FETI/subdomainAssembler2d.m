function [ K, F, hmax ] = ...
            subdomainAssembler2d( meshArray, Nx, Ny, rho, i_F, ...
                                  k, eoa, compressquad, formula1d, basis_type, use_mp, ...
                                  runAsFor )

numSubdomains = Nx*Ny;

K = cell( Nx, Ny );
F = cell( Nx, Ny );
hmax = zeros( Nx, Ny );

parfor ( s = 1:numSubdomains, getParforArg(runAsFor) )
% for s = 1:numSubdomains
    [ K{s}, row_dofs_for_K, col_dofs_for_K, F{s}, dofs_for_F, ~, ...
      ~, ~, ~, ~, ~, ...
      h ] = ...
        assemble_local_matrices_and_rhs_parallel( k, eoa, compressquad, meshArray{s}, ...
                                                  formula1d, basis_type, use_mp, i_F, [], 1 );
    hmax(s) = max(h);

    if i_F == 1
        F{s} = rho(s) * full( sparse(dofs_for_F, ones([length(dofs_for_F) 1]), F{s}) );
    elseif i_F == 2
        F{s} = full( sparse(dofs_for_F, ones([length(dofs_for_F) 1]), F{s}) );
    end

    K{s} = rho(s) * K{s};

    if ~isempty( meshArray{s}.diridofs )
        dofs = setdiff(1:meshArray{s}.dim, meshArray{s}.diridofs);

        % Extend local coordinate arrays with those of dofs on edges.
        
        x = meshArray{s}.coordinates(:, 1);
        x = x(meshArray{s}.edges(:,1:2)');
        x = formula1d(2:end-1,1:2)*x;
        x = [ meshArray{s}.coordinates(:, 1); x(:) ];

        y = meshArray{s}.coordinates(:, 2);
        y = y(meshArray{s}.edges(:,1:2)');
        y = formula1d(2:end-1,1:2)*y;
        y = [ meshArray{s}.coordinates(:, 2); y(:) ];

        F{s}(meshArray{s}.diridofs) = dirdata(x(meshArray{s}.diridofs), y(meshArray{s}.diridofs), use_mp);

        % Assemble K{s} temporarily to eliminate Dirichlet dofs
        dummy = sparse(row_dofs_for_K, col_dofs_for_K, K{s});
        dummy = dummy(dofs, meshArray{s}.diridofs);
        
        F{s}(dofs) = F{s}(dofs) - dummy * F{s}(meshArray{s}.diridofs);

        dummy = false(meshArray{s}.dim, 1);
        dummy(meshArray{s}.diridofs) = true;

        % Find mapping from vector of domain row degrees of freedom to
        % Dirichlet rows
        [~, ~, rjx] = unique(row_dofs_for_K);
        [~, ~, cjx] = unique(col_dofs_for_K);

        % Entries of K to be set = 1
        diri_ii = dummy(rjx) & (row_dofs_for_K == col_dofs_for_K);

        % Entries of K to be zeroed
        diri_ij = (dummy(rjx) | dummy(cjx)) & (~diri_ii);

        % Count the number of occurences of Dirichlet (i,i) entries
        counts = histc(row_dofs_for_K(diri_ii), 1:meshArray{s}.dim);

        K{s}(diri_ii) = 1.0 ./ counts(rjx(diri_ii));
        K{s}(diri_ij) = 0.0;
        
        % Final assemblage
        K{s} = sparse( row_dofs_for_K, col_dofs_for_K, K{s} );
    else
        K{s} = sparse( row_dofs_for_K, col_dofs_for_K, K{s} );
    end
end

