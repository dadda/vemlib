function z = solveDirichletPreconditioner2d( r, Nx, Ny, K, ...
                                             dimGlobalLambda, dualDofs, internalDofs, BdualScaled, ...
                                             runAsFor )

z = zeros(dimGlobalLambda, 1);

numSubdomains = Nx * Ny;
parfor ( s = 1:numSubdomains, getParforArg(runAsFor) )

    tmp1 = BdualScaled{s}' * r;
    dimInternalDofs = length( internalDofs{s} );

	tmp2 = K{s}(internalDofs{s}, internalDofs{s}) \ ...
          ( K{s}(internalDofs{s}, dualDofs{s}) * tmp1(dimInternalDofs+1:end) );
    
    tmp1 = K{s}(dualDofs{s}, dualDofs{s}) * tmp1(dimInternalDofs+1:end) ...
           - K{s}(dualDofs{s}, internalDofs{s}) * tmp2;

    z = z + BdualScaled{s} * [ zeros( dimInternalDofs, 1); tmp1 ];
end

end
