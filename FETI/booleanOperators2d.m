function [ dimGlobalPrimal, dimGlobalLambda, ...
           localPrimalDofs, localDualDofs, localInternalDofs, ...
           Bprimal, Bdual, BdualScaled ] = ...
                booleanOperators2d( meshArray, Nx, Ny, iwest, ieast, isouth, inorth, rho, gamma )

% IMPORTANT: only pure Dirichlet boundary conditions are supported at
% present.

localPrimalDofs   = cell( Nx, Ny );
localDualDofs     = cell( Nx, Ny );
localInternalDofs = cell( Nx, Ny );

Bprimal      = cell( Nx, Ny );
Bdual        = cell( Nx, Ny );
BdualScaled  = cell( Nx, Ny );

% Dimension primal space
dimGlobalPrimal = (Nx-1) * (Ny-1);

% Dimension of the space of the Lagrange multiplier lambda
dimGlobalLambda = 0;
for j = 1:Ny
    for i = 1:Nx
        if j < Ny
            if i < Nx
                dimGlobalLambda = dimGlobalLambda + length( ieast{i,j} ) + length( inorth{i,j} ) - 4;
            else
                dimGlobalLambda = dimGlobalLambda + length( inorth{i,j} ) - 2;
            end
        elseif i < Nx
            dimGlobalLambda = dimGlobalLambda + length( ieast{i,j} ) - 2;
        end
    end
end

globalLambdaDofsStart = zeros( Nx, Ny, 2 );
globalLambdaDofsEnd   = zeros( Nx, Ny );

primalPatch = [ -Nx; -(Nx-1); -1; 0 ];
dualPatch   = [ -1  0;
                 1  0;
                 0 -1;
                 0  1 ];

subdomainIdx = 0;

for j = 1:Ny
    for i = 1:Nx
        subdomainIdx = subdomainIdx + 1;

        cornerPoints       = [ isouth{i,j}(1); isouth{i,j}(end); inorth{i,j}(1); inorth{i,j}(end) ];
        edgeInternalPoints = { iwest{i,j}(2:end-1);
                               ieast{i,j}(2:end-1);
                               isouth{i,j}(2:end-1);
                               inorth{i,j}(2:end-1) };
        numEdgeInternalPoints = [ length(edgeInternalPoints{1});
                                  length(edgeInternalPoints{2});
                                  length(edgeInternalPoints{3});
                                  length(edgeInternalPoints{4}) ];

        if j == 1
            if i == 1
                interfaceVertices = 4;
                interfaceEdges    = [2 4];
            elseif i < Nx
                interfaceVertices = [3 4];
                interfaceEdges    = [1 2 4];
            else
                interfaceVertices = 3;
                interfaceEdges    = [1 4];
            end
        elseif j < Ny
            if i == 1
                interfaceVertices = [2 4];
                interfaceEdges    = 2:4;
            elseif i < Nx
                interfaceVertices = 1:4;
                interfaceEdges    = 1:4;
            else
                interfaceVertices = [1 3];
                interfaceEdges    = [1 3 4];
            end
        else
            if i == 1
                interfaceVertices = 2;
                interfaceEdges    = [2 3];
            elseif i < Ny
                interfaceVertices = [1 2];
                interfaceEdges    = 1:3;
            else
                interfaceVertices = 1;
                interfaceEdges    = [1 3];
            end
        end

        % Primal dofs
        globalPrimalDofs     = (j-1)*(Nx-1) + i + primalPatch( interfaceVertices );
        localPrimalDofs{i,j} = cornerPoints( interfaceVertices );

        % Dual dofs
        localDualDofs{i,j} = vertcat( edgeInternalPoints{ interfaceEdges } );

        % Internal dofs
        localInternalDofs{i,j} = ...
            setdiff( (1:meshArray{i,j}.dim)', ...
                     [ localPrimalDofs{i,j}; localDualDofs{i,j}; meshArray{i,j}.diridofs ] );
         
        dimLocalPrimal   = length( localPrimalDofs{i,j} );
        dimLocalInternal = length( localInternalDofs{i,j} );

         % Dofs for boolean operators
        globalLambdaDofs = cell(4, 1);
        globalLambdaDofsEnd( subdomainIdx ) = globalLambdaDofsEnd( max( subdomainIdx-1, 1 ) );
        
        localInternalDualDofs = cell(4, 1);
        localInternalDualDofsEnd = dimLocalInternal;

        constraintsSign       = cell(4, 1);
        constraintsSignScaled = cell(4, 1);

        for e = interfaceEdges
            if mod(e,2)
                globalLambdaDofs{e} = ...
                    globalLambdaDofsStart( i+dualPatch(e,1), j+dualPatch(e,2), ceil(e/2) ) ...
                    - 1 + (1:numEdgeInternalPoints(e))';

                constraintsSign{e}       = -ones(numEdgeInternalPoints(e),1);
                constraintsSignScaled{e} = -ones(numEdgeInternalPoints(e),1) ...
                    * ( rho( i+dualPatch(e,1), j+dualPatch(e,2) )^gamma ...
                        / ( rho(i,j)^gamma + rho( i+dualPatch(e,1), j+dualPatch(e,2) )^gamma ) );
            else
                globalLambdaDofs{e} = globalLambdaDofsEnd(i,j) + (1:numEdgeInternalPoints(e))';

                globalLambdaDofsStart(i,j,e/2) = globalLambdaDofsEnd(i,j) + 1;
                globalLambdaDofsEnd(i,j)       = globalLambdaDofsEnd(i,j) + numEdgeInternalPoints(e);

                constraintsSign{e}       = ones(numEdgeInternalPoints(e),1);
                constraintsSignScaled{e} = ones(numEdgeInternalPoints(e),1) ...
                    * ( rho( i+dualPatch(e,1), j+dualPatch(e,2) )^gamma ...
                        / ( rho(i,j)^gamma + rho( i+dualPatch(e,1), j+dualPatch(e,2) )^gamma ) );
            end

            localInternalDualDofs{e} = localInternalDualDofsEnd + (1:numEdgeInternalPoints(e))';
            localInternalDualDofsEnd = localInternalDualDofsEnd + numEdgeInternalPoints(e);
        end
        globalLambdaDofs      = vertcat( globalLambdaDofs{:} );
        localInternalDualDofs = vertcat( localInternalDualDofs{:} );
        constraintsSign       = vertcat( constraintsSign{:} );
        constraintsSignScaled = vertcat( constraintsSignScaled{:} );
        
        Bprimal{i,j}     = sparse( 1:dimLocalPrimal, globalPrimalDofs, true, ...
                                   dimLocalPrimal, dimGlobalPrimal );
        Bdual{i,j}       = sparse( globalLambdaDofs, localInternalDualDofs, constraintsSign, ...
                                   dimGlobalLambda, localInternalDualDofsEnd );
        BdualScaled{i,j} = sparse( globalLambdaDofs, localInternalDualDofs, constraintsSignScaled, ...
                                   dimGlobalLambda, localInternalDualDofsEnd );
    end
end
