function [ lambda, relRes, iter, relResVec, eigEst ] = ...
    pcgFETIDP2d( meshBasename, k, eoa, compressquad, basis_type, use_mp, ...
                 Nx, Ny, i_rho, K, rhoPower, ...
                 dimGlobalPrimal, dimGlobalLambda, ...
                 primalDofs, dualDofs, internalDofs, ...
                 Bprimal, Bdual, BdualScaled, ...
                 Kstar, d, tol, maxit, use_pc, fid, savePCGiter, runAsFor )

% Solve the dual interface problem by the preconditioned conjugate gradient
% algorithm.

iter = 0;

try
    filename = sprintf(['../FETI/workspaces/%s__k_%i__eoa_%i__compressquad_%i__' ...
                        'basis_type_%i__use_mp_%i__Nx_%i__Ny_%i__i_rho_%i__gamma_%g__' ...
                        'tol_%g__maxit_%i__use_pc_%i__lastIt.mat'], ...
                        meshBasename, k, eoa, compressquad, basis_type, use_mp, Nx, Ny, ...
                        i_rho, rhoPower, tol, maxit, use_pc );
    load(filename);

    if ~isempty(fid)
        fprintf(fid, 'Resuming PCG from iteration %i\n', iter);
    else
        fprintf('Resuming PCG from iteration %i\n', iter);
    end
catch
    dim    = length(d);
    lambda = zeros(dim, 1);
    r      = d;
    
    relRes    = 1;
    relResVec = ones(maxit, 1);

    alpha = 1;
    T = zeros(maxit);
end

% Initial residual norm
rNorm0 = norm(d, 2);
if rNorm0 < 1e-12
    if ~isempty(fid)
        % fid provided
        fprintf(fid, ['WARNING: the initial guess lambda = 0 is close to the ' ...
                      'exact solution. Tolerance changed to 0.01\n']);
    else
        warning( ['The initial guess lambda = 0 is close to the exact solution.' ...
                  ' Tolerance changed to 0.01']);
    end
    tol = 0.01;
end

while ( (relRes > tol) && (iter < maxit) )
    iter = iter + 1;
    
    if i_rho ~= 1
        if ~isempty(fid)
            fprintf(fid, 'Starting PCG iteration %i ... ', iter);
        else
            fprintf('Starting PCG iteration %i ... ', iter);
        end
    end
    
    if use_pc
        z = solveDirichletPreconditioner2d( r, Nx, Ny, K, ...
                                            dimGlobalLambda, dualDofs, internalDofs, BdualScaled, ...
                                            runAsFor );
    else
        z = r;
    end
    
    gamma = z' * r;
    
    if iter == 1
        p = z;
    else
        beta = gamma / gammaOld;
        p    = z + beta*p;
    end
    
    FIp = actionFI2d( p, Nx, Ny, K, dimGlobalPrimal, dimGlobalLambda, ...
                      primalDofs, dualDofs, internalDofs, Bprimal, Bdual, Kstar, runAsFor );

    alphaOld = alpha;
    alpha    = gamma / (p' * FIp);
    lambda   = lambda + alpha*p;
    r        = r - alpha*FIp;
        
    % Compute approximate condition number of M^-1 * F_I
    % from the tridiagonal Lanczos matrix generated during the PCG
    % iteration as the ratio between the maximum and the minimum
    % eigenvalues
    if iter > 1
        T(iter-1:iter, iter-1:iter) = T(iter-1:iter, iter-1:iter) + ...
            [1 sqrt(beta); sqrt(beta) beta] / alphaOld;
    end
    
    gammaOld = gamma;
    relRes   = norm(r, 2) / rNorm0;
    relResVec(iter) = relRes;

    if abs( alpha ) < 1e-15
        if ~isempty(fid)
        % fid provided
            fprintf(fid, ['WARNING: PCG stagnated (two consecutive iterates were the same). ' ...
                          'Stopping PCG ...\n']);
        else
            warning('PCG stagnated (two consecutive iterates were the same). Stopping PCG ...');
        end
        break
    end

    if alpha < 0
        if ~isempty(fid)
            fprintf(fid, ['WARNING: the coefficient matrix of the dual interface system is negative. ' ...
                          'Stopping PCG ...\n']);
        else
            warning('The coefficient matrix of the dual interface system is negative. Stopping PCG ...');
        end
        break
    end

    if savePCGiter ~= 0
        filename = sprintf(['../FETI/workspaces/%s__k_%i__eoa_%i__compressquad_%i__' ...
                            'basis_type_%i__use_mp_%i__Nx_%i__Ny_%i__i_rho_%i__gamma_%g__' ...
                            'tol_%g__maxit_%i__use_pc_%i__lastIt.mat'], ...
                            meshBasename, k, eoa, compressquad, basis_type, use_mp, Nx, Ny, ...
                            i_rho, rhoPower, tol, maxit, use_pc );
        save(filename, 'iter', 'lambda', 'r', 'relRes', 'relResVec', ...
                       'alpha', 'T', 'gammaOld', 'p', '-v7.3');
    end
    
    if i_rho ~= 1
        if ~isempty(fid)
            fprintf(fid, 'done\n');
        else
            fprintf('done\n');
        end
    end
end

relResVec = relResVec(1:iter);

if iter > 3
    T = T(2:iter-2,2:iter-2);
    l = eig(T);
    eigEst = [min(l), max(l)];
else
    eigEst = [1 1];
    if ~isempty(fid)
        fprintf(fid, 'WARNING: eigenvalue estimate failed because iterates converged too fast.\n');
    else
        warning('Eigenvalue estimate failed: iterates converged too fast.');
    end
end

if relRes <= tol
    if ~isempty(fid)
        fprintf(fid, 'PCG converged to the desired tolerance %g within %i iterations.\n', tol, iter);
    else
        fprintf('PCG converged to the desired tolerance %g within %i iterations.\n', tol, iter);
    end
else
    if ~isempty(fid)
        fprintf(fid, 'PCG iterated %i times but did not converge.\n', iter);
    else
        fprintf('PCG iterated %i times but did not converge.\n', iter);
    end
end

end
