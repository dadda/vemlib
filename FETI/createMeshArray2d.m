function [ meshArray, iwest, ieast, isouth, inorth ] = ...
                createMeshArray2d( meshName, subdomainMeshApproach, Nx, Ny, ...
                                   k, formula1d, use_mp )

numSubdomains = Nx*Ny;
meshArray     = cell( Nx, Ny );

Hx = 1/Nx;
Hy = 1/Ny;

switch subdomainMeshApproach
    case 0
        % Create subdomain meshes from a reference mesh 
        refMesh = loadmesh( meshName, use_mp );

        x0 = refMesh.coordinates(:,1);
        y0 = refMesh.coordinates(:,2);
        
        Hx = 1/Nx;
        Hy = 1/Ny;

        for j = 1:Ny
            for i = 1:Nx
                
                meshArray{i,j} = refMesh;
                
                x = x0 * Hx * (2*mod(i,2)-1) + Hx * (i-mod(i,2));
                y = y0 * Hy * (2*mod(j,2)-1) + Hy * (j-mod(j,2));

                meshArray{i,j}.coordinates(:,1) = x;
                meshArray{i,j}.coordinates(:,2) = y;

                if mod( i+j, 2 )
                    for K = 1:meshArray{i,j}.Nelt
                        NverLoc = refMesh.elements(K,1);

                        meshArray{i,j}.elements(K, 2:NverLoc+1) = ...
                            refMesh.elements(K, NverLoc+1:-1:2);

                        meshArray{i,j}.edgebyele(K, 1:NverLoc) = ...
                            refMesh.edgebyele(K, [NverLoc-1:-1:1 NverLoc]);

                        meshArray{i,j}.perm(K, 1:NverLoc) = ...
                            1 + mod( refMesh.perm(K, [NverLoc-1:-1:1 NverLoc]), 2 );
                    end
                end
            end
        end

    case 1
        NeltArray = zeros( Nx, Ny );
        
        % Read cut mesh
        tmpMesh  = readOFFmesh( meshName );
        NeltGlob = size( tmpMesh.Element, 1 );
        
        % Read subdomain flags
        fid = fopen([meshName '.domains.txt']);
        for i = 1:NeltGlob
            subdomainIdx = fscanf(fid, '%d', 1) + 1; % Indices in Livesu's files start from 0
            NeltArray(subdomainIdx) = NeltArray(subdomainIdx) + 1;
        end
        fclose(fid);
        
        % Allocate space
        elementArray = cell( Nx, Ny );
        for i = 1:numSubdomains
            elementArray{i} = cell( NeltArray(i), 1 );
        end
        
        % Read subdomain flags again
        NeltArray(:) = 0;
        
        fid = fopen([meshName '.domains.txt']);
        for i = 1:NeltGlob
            subdomainIdx = fscanf(fid, '%d', 1) + 1; % Indices in Livesu's files start from 0
            NeltArray(subdomainIdx) = NeltArray(subdomainIdx) + 1;

            elementArray{subdomainIdx}{NeltArray(subdomainIdx)} = tmpMesh.Element{i};
        end
        fclose(fid);

        % Create subdomain mesh data structures with local (subdomain) numbering
        for i = 1:numSubdomains
            [C, ~, IC]  = unique( horzcat( elementArray{i}{:} ) );
            Node = tmpMesh.Node( C, : );
            
            % Update numbering
            counter = 0;
            for K = 1:NeltArray(i)
                NverLoc = length( elementArray{i}{K} );
                for n = 1:NverLoc
                    counter = counter + 1;
                    elementArray{i}{K}(n) = IC(counter);
                end
            end
            
            auxMesh.Node    = Node;
            auxMesh.Element = elementArray{i};

            % Set the correct subdomain index
            center = sum( Node, 1 ) / size(Node, 1);
            iUB = []; jUB = [];
            for is = 1:Nx
                if is*Hx > center(1)
                    iUB = is;
                    break;
                end
            end
            for js = 1:Ny
                if js*Hy > center(2)
                    jUB = js;
                    break;
                end
            end
            subdomainIdx            = ( jUB - 1 )*Nx + iUB;
            meshArray{subdomainIdx} = loadmesh( auxMesh, use_mp );
        end
end

%
%
%

d2m2 = (k*(k-1))/2;

iwest  = cell( Nx, Ny );
ieast  = cell( Nx, Ny );
isouth = cell( Nx, Ny );
inorth = cell( Nx, Ny );

for j = 1:Ny
    for i = 1:Nx

        meshArray{i,j}.dim = meshArray{i,j}.Nver ...
                             + (k-1)*meshArray{i,j}.Nfc ...
                             + d2m2*meshArray{i,j}.Nelt;

        x = meshArray{i,j}.coordinates(:, 1);
        x = x(meshArray{i,j}.edges( meshArray{i,j}.dirfaces ,1:2)');

        y = meshArray{i,j}.coordinates(:, 2);
        y = y(meshArray{i,j}.edges( meshArray{i,j}.dirfaces ,1:2)');

        xN = y(2,:) - y(1,:); 
        yN = x(1,:) - x(2,:);
        norms = sqrt( xN.^2 + yN.^2 );
        xN = xN ./ norms;
        yN = yN ./ norms;

        dirfacesWest  = ( abs(xN) >= 0.5 ) & ( x(1,:) < (i-0.5)*Hx );
        dirfacesEast  = ( abs(xN) >= 0.5 ) & ( x(1,:) > (i-0.5)*Hx );
        dirfacesSouth = ( abs(yN) > 0.5 ) & ( y(1,:) < (j-0.5)*Hy );
        dirfacesNorth = ( abs(yN) > 0.5 ) & ( y(1,:) > (j-0.5)*Hy );
        
        x = meshArray{i,j}.coordinates(:, 1);
        x = x(meshArray{i,j}.edges(:,1:2)');
        x = formula1d(2:end-1,1:2)*x;
        x = [ meshArray{i,j}.coordinates(:, 1); x(:) ];

        y = meshArray{i,j}.coordinates(:, 2);
        y = y(meshArray{i,j}.edges(:,1:2)');
        y = formula1d(2:end-1,1:2)*y;
        y = [ meshArray{i,j}.coordinates(:, 2); y(:) ];

        % Remember that we do not know how each edges is
        % oriented

        % West

        tmpA = meshArray{i,j}.edges( meshArray{i,j}.dirfaces( dirfacesWest ), 1:2 );
        tmpA = unique( tmpA(:) );
        tmpB = bsxfun(@plus, (k-1)*(meshArray{i,j}.dirfaces( dirfacesWest )-1), 1:k-1 ) ...
               + meshArray{i,j}.Nver;
        tmp = [ tmpA; tmpB(:) ];
        [~, ix] = sort( y(tmp) );
        iwest{i,j} = tmp(ix);
        iwest{i,j} = ...
            iwest{i,j}( abs( x( iwest{i,j} ) - (i-1)*Hx ) < 1e-8 );

        % East
        tmpA = meshArray{i,j}.edges( meshArray{i,j}.dirfaces( dirfacesEast ), 1:2 );
        tmpA = unique( tmpA(:) );
        tmpB = bsxfun(@plus, (k-1)*(meshArray{i,j}.dirfaces( dirfacesEast )-1), 1:k-1 ) ...
               + meshArray{i,j}.Nver;
        tmp = [ tmpA; tmpB(:) ];
        [~, ix] = sort( y(tmp) );
        ieast{i,j} = tmp(ix);
        ieast{i,j} = ...
            ieast{i,j}( abs( x( ieast{i,j} ) - i*Hx ) < 1e-8 );

        % South
        tmpA = meshArray{i,j}.edges( meshArray{i,j}.dirfaces( dirfacesSouth ), 1:2 );
        tmpA = unique( tmpA(:) );
        tmpB = bsxfun(@plus, (k-1)*(meshArray{i,j}.dirfaces( dirfacesSouth )-1), 1:k-1 ) ...
               + meshArray{i,j}.Nver;
        tmp = [ tmpA; tmpB(:) ];
        [~, ix] = sort( x(tmp) );
        isouth{i,j} = tmp(ix);
        isouth{i,j} = ...
            isouth{i,j}( abs( y( isouth{i,j} ) - (j-1)*Hy ) < 1e-8 );

        % North
        tmpA = meshArray{i,j}.edges( meshArray{i,j}.dirfaces( dirfacesNorth ), 1:2 );
        tmpA = unique( tmpA(:) );
        tmpB = bsxfun(@plus, (k-1)*(meshArray{i,j}.dirfaces( dirfacesNorth )-1), 1:k-1 ) ...
               + meshArray{i,j}.Nver;
        tmp = [ tmpA; tmpB(:) ];
        [~, ix] = sort( x(tmp) );
        inorth{i,j} = tmp(ix);
        inorth{i,j} = ...
            inorth{i,j}( abs( y( inorth{i,j} ) - j*Hy ) < 1e-8 );

        meshArray{i,j} = rmfield( meshArray{i,j}, 'dirfaces' );

        if iwest{i,j}(1) ~= isouth{i,j}(1)
            warning('Bottom left corner is not consistent for subdomain (%i, %i)', i, j);
            tmp = abs( [ x( iwest{i,j}(1) ) - x( isouth{i,j}(1) );
                         y( iwest{i,j}(1) ) - y( isouth{i,j}(1) ) ] );
            [~, ix] = max(tmp);
            if ix == 1
                isouth{i,j}(1) = iwest{i,j}(1);
            else
                iwest{i,j}(1)  = isouth{i,j}(1);
            end
        end
        if  isouth{i,j}(end) ~= ieast{i,j}(1)
            warning('Bottom right corner is not consistent for subdomain (%i, %i)', i, j);
            tmp = abs( [ x( isouth{i,j}(end) ) - x( ieast{i,j}(1) );
                         y( isouth{i,j}(end) ) - y( ieast{i,j}(1) ) ] );
            [~, ix] = max(tmp);
            if ix == 1
                isouth{i,j}(end) = ieast{i,j}(1);
            else
                ieast{i,j}(1)    = isouth{i,j}(end);
            end
        end
        if  iwest{i,j}(end) ~= inorth{i,j}(1)
            warning('Top left corner is not consistent for subdomain (%i, %i)', i, j);
            tmp = abs( [ x( iwest{i,j}(end) ) - x( inorth{i,j}(1) );
                         y( iwest{i,j}(end) ) - y( inorth{i,j}(1) ) ] );
            [~, ix] = max(tmp);
            if ix == 1
                inorth{i,j}(1)  = iwest{i,j}(end);
            else
                iwest{i,j}(end) = inorth{i,j}(1);
            end
        end
        if  ieast{i,j}(end) ~= inorth{i,j}(end)
            warning('Top right corner is not consistent for subdomain (%i, %i)', i, j);
            tmp = abs( [ x( ieast{i,j}(end) ) - x( inorth{i,j}(end) );
                         y( ieast{i,j}(end) ) - y( inorth{i,j}(end) ) ] );
            [~, ix] = max(tmp);
            if ix == 1
                inorth{i,j}(end) = ieast{i,j}(end);
            else
                ieast{i,j}(end)  = inorth{i,j}(end);
            end
        end

        if i == 1
            if j == 1
                meshArray{i,j}.diridofs = [ iwest{i,j}; isouth{i,j}(2:end) ];
            elseif j < Ny
                meshArray{i,j}.diridofs = iwest{i,j};
            else
                meshArray{i,j}.diridofs = [ iwest{i,j}; inorth{i,j}(2:end) ];
            end
        elseif i < Nx
            if j == 1
                meshArray{i,j}.diridofs = isouth{i,j};
            elseif j < Ny
                meshArray{i,j}.diridofs = [];
            else
                meshArray{i,j}.diridofs = inorth{i,j};
            end
        else
            if j == 1
                meshArray{i,j}.diridofs = [ ieast{i,j}; isouth{i,j}(1:end-1) ];
            elseif j < Ny
                meshArray{i,j}.diridofs = ieast{i,j};
            else
                meshArray{i,j}.diridofs = [ ieast{i,j}; inorth{i,j}(1:end-1) ];
            end
        end

    end
end

end
