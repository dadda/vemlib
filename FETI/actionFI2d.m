function delta = actionFI2d( lambda, Nx, Ny, K, ...
                             dimGlobalPrimal, dimGlobalLambda, ...
                             primalDofs, dualDofs, internalDofs, ...
                             Bprimal, Bdual, Kstar, ...
                             runAsFor )

y     = zeros(dimGlobalPrimal, 1);
delta = zeros(dimGlobalLambda, 1);

numSubdomains = Nx * Ny;
parfor ( s = 1:numSubdomains, getParforArg(runAsFor) )
    % Step 1
    tmp = K{s}( [ internalDofs{s}; dualDofs{s} ], [ internalDofs{s}; dualDofs{s} ] ) ...
          \ ( Bdual{s}' * lambda );

    delta = delta + Bdual{s} * tmp;
    
    % Step 2-1
    y = y + Bprimal{s}' * ( K{s}( primalDofs{s}, [ internalDofs{s}; dualDofs{s} ] ) * tmp );
end

% Step 2-2
x = Kstar \ y;

z = actionFIrc2d( x, Nx, Ny, K, dimGlobalLambda, primalDofs, dualDofs, internalDofs, ...
                  Bprimal, Bdual, runAsFor );

% Combine Step 1 and Step 2
delta = delta + z;
