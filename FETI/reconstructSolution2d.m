function u = reconstructSolution2d( lambda, uPrimal, meshArray, Nx, Ny, K, F, ...
                                    primalDofs, dualDofs, internalDofs, ...
                                    Bprimal, Bdual, runAsFor )

u = cell(Nx, Ny);

numSubdomains = Nx * Ny;
parfor ( s = 1:numSubdomains, getParforArg(runAsFor) )

    u{s} = zeros( meshArray{s}.dim, 1 );
    
    % Set u at primal dofs
    u{s}( primalDofs{s} ) = Bprimal{s} * uPrimal;
    
    % Set u at internal and dual dofs
    u{s}( [ internalDofs{s}; dualDofs{s} ] ) = ...
        K{s}( [ internalDofs{s}; dualDofs{s} ], [ internalDofs{s}; dualDofs{s} ] ) \ ...
        ( F{s}( [ internalDofs{s}; dualDofs{s} ] )  ...
          - ( Bdual{s}' * lambda ) ...
          - K{s}( [ internalDofs{s}; dualDofs{s} ], primalDofs{s} ) * u{s}( primalDofs{s} ) );

    % Set u at Dirichlet dofs
    u{s}( meshArray{s}.diridofs ) = F{s}( meshArray{s}.diridofs );

end

end
