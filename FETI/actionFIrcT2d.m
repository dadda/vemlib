function y = actionFIrcT2d( x, Nx, Ny, K, ...
                            dimGlobalPrimal, primalDofs, dualDofs, internalDofs, ...
                            Bprimal, Bdual, ...
                            runAsFor )

y = zeros(dimGlobalPrimal, 1);

numSubdomains = Nx * Ny;
parfor ( s = 1:numSubdomains, getParforArg(runAsFor) )

    tmp = K{s}( [ internalDofs{s}; dualDofs{s} ], [ internalDofs{s}; dualDofs{s} ] ) ...
          \ ( Bdual{s}' * x );

    y = y + Bprimal{s}' * ( K{s}( primalDofs{s}, [ internalDofs{s}; dualDofs{s} ] ) * tmp );

end

end
