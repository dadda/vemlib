function sol = exactSolution2d( meshArray, Nx, Ny, ...
                                k, eoa, compressquad, basis_type, formula1d, use_mp, ...
                                runAsFor )

d2  = ( (k+1)*(k+2) ) / 2;

sol = cell(Nx, Ny);

numSubdomains = Nx * Ny;

dofsLayout = 1;

if basis_type == 1
    parfor ( s = 1:numSubdomains, getParforArg(runAsFor) )
%     for s = 1:numSubdomains
        areas       = zeros(1,meshArray{s}.Nelt);
        xqd         = cell(meshArray{s}.Nelt,1);
        yqd         = cell(meshArray{s}.Nelt,1);
        wqd         = cell(meshArray{s}.Nelt,1);
        basis_on_qd = cell(meshArray{s}.Nelt,1);

        for E = 1:meshArray{s}.Nelt
            [ ~, ~, xqd{E}, yqd{E}, wqd{E}, ~, ~, ...
              hE, areas(E), xb, yb ] = build_coordinate_matrices_parallel( k, eoa, compressquad, ...
                    meshArray{s}.elements(E,:), meshArray{s}.coordinates, formula1d, use_mp );

            basis_on_qd{E} = monomial_basis( xqd{E}, yqd{E}, k, ...
                                             xb, yb, hE, ones(size(xqd{E})));
            basis_on_qd{E} = permute(basis_on_qd{E}, [1,3,2]);

        end

        sol{s} = compute_VEM_sol_parallel( k, meshArray{s}, ...
            [], [], [], [], dofsLayout, ...
            areas, xqd, yqd, wqd, basis_on_qd, formula1d, use_mp );
    end
else
    parfor ( s = 1:numSubdomains, getParforArg(runAsFor) )
        areas       = zeros(1,meshArray{s}.Nelt);
        xqd         = cell(meshArray{s}.Nelt,1);
        yqd         = cell(meshArray{s}.Nelt,1);
        wqd         = cell(meshArray{s}.Nelt,1);
        basis_on_qd = cell(meshArray{s}.Nelt,1);
        basisL2norm = zeros(meshArray{s}.Nelt,d2);

        for E = 1:meshArray{s}.Nelt
            [ ~, ~, xqd{E}, yqd{E}, wqd{E}, ~, ~, ...
              hE, areas(E), xb, yb ] = build_coordinate_matrices_parallel( k, eoa, compressquad, ...
                    meshArray{s}.elements(E,:), meshArray{s}.coordinates, formula1d, use_mp );

            basis_on_qd{E} = monomial_basis( xqd{E}, yqd{E}, k, ...
                                             xb, yb, hE, ones(size(xqd{E})));
            basis_on_qd{E} = permute(basis_on_qd{E}, [1,3,2]);

            basisL2norm(E,:) = sqrt( wqd{E}' * basis_on_qd{E}.^2 );

            % Normalize basis functions
            basis_on_qd{E} = bsxfun(@times, 1./basisL2norm(E,:), basis_on_qd{E});
        end

        sol{s} = compute_VEM_sol_parallel( k, meshArray{s}, ...
            [], [], [], [], dofsLayout, ...
            areas, xqd, yqd, wqd, basis_on_qd, formula1d, use_mp, basisL2norm );
    end
end


end
