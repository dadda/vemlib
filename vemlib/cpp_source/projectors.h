#ifndef _PROJECTORS_H
#define _PROJECTORS_H

#include <cblas.h>
#include "basisFunctions.h"
#include "geometry.h"
#include "quadrature.h"
#include <lapacke.h>

void computeScalarBf( double * const Bf,
		      const int scaleDof,
		      const int k,
		      const int Nver,
		      const double faceCentroidX,
		      const double faceCentroidY,
		      const double faceDiam,
		      const double faceArea,
		      const double * const xbnd,
		      const double * const ybnd,
		      const double * const edgeNormalsX,
		      const double * const edgeNormalsY,
		      const double * const allQuadWeights1d );

void computeScalarHf_chinAlg( double * const Hf,
			      const int k,
			      const double * const vertices,
			      const int * const face,
			      const int NvLoc,
			      const double faceDiam,
			      const double faceCentroidX,
			      const double faceCentroidY );

void computeScalarH_quadrature( double * const H,
				const int dPk,
				const double * const basisOnQd,
				const double * const wquad,
				const int Nq );

void computeScalarDf( double * const Df,
		      const int scaleDof,
		      const int k,
		      const double faceArea,
		      const int dPk,
		      const int dPkm2,
		      const double * const basisOnBndNodes,
		      const int NbndNodes,
		      const int Ndof,
		      const double * const Hf );

#endif
