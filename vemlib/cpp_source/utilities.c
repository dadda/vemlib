#include "utilities.h"


int compareInt( const void * a,
		const void * b )
{
  return ( *(int *)a - *(int *)b );
}


int compareSize1( const void * a, const void * b )
{
  int *rowA, *rowB;
  rowA = *(int **)a;
  rowB = *(int **)b;
  return rowA[0] - rowB[0];
}


int compareSize2( const void * a, const void * b )
{
  int *rowA, *rowB;
  rowA = *(int **)a;
  rowB = *(int **)b;
  if ( rowA[0] < rowB[0] ) return -1;
  if ( rowA[0] > rowB[0] ) return 1;
  if ( rowA[1] < rowB[1] ) return -1;
  if ( rowA[1] > rowB[1] ) return 1;
  return 0;
}


int compareSize3( const void * a, const void * b )
{
  int *rowA, *rowB;
  rowA = *(int **)a;
  rowB = *(int **)b;
  if ( rowA[0] < rowB[0] ) return -1;
  if ( rowA[0] > rowB[0] ) return 1;
  if ( rowA[1] < rowB[1] ) return -1;
  if ( rowA[1] > rowB[1] ) return 1;
  if ( rowA[2] < rowB[2] ) return -1;
  if ( rowA[2] > rowB[2] ) return 1;
  return 0;
}


void intersectSorted( const int * const a,
		      const int m,
		      const int * const b,
		      const int n,
		      int **intersection,
		      int * k )
{
  int i = 0, j = 0, itmp;

  itmp = m < n ? m : n;
  *intersection = (int *) malloc (itmp * sizeof(int));
  *k   = 0;

  while ( (i < m) && (j < n) ) {
    if      ( a[i] < b[j] ) ++i;
    else if ( a[i] > b[j] ) ++j;
    else {
      (*intersection)[*k] = a[i];
      ++i; ++j; ++(*k);
    }
  }
}


void unique( const int * const a,
	     const int n,
	     int **c,
	     int * cLength,
	     int * const ic )
{
  int i, itmp, counter;
  int **buffer = NULL;

  buffer = (int **) malloc (n * sizeof(int *));
  for (i = 0; i < n; ++i) {
    buffer[i]    = (int *) calloc (3, sizeof(int));
    buffer[i][0] = a[i];
    buffer[i][1] = i;
  }

  qsort( buffer, n, sizeof(int *), compareSize1 );

  counter = 1;
  for (i = 1; i < n; ++i) {
    if ( buffer[i][0] != buffer[i-1][0] )
      buffer[i][2] = counter++;
    else
      buffer[i][2] = counter;
    itmp         = buffer[i][0];
    buffer[i][0] = buffer[i][1];
    buffer[i][1] = itmp;
  }

  *c      = (int *) malloc (counter * sizeof(int));
  (*c)[0] = buffer[0][1];
  counter = 1;
  for (i = 1; i < n; ++i)
    if ( buffer[i][1] != buffer[i-1][1] ) (*c)[counter++] = buffer[i][1];
  *cLength = counter;

  if ( ic != NULL ) {
    qsort( buffer, n, sizeof(int *), compareSize1 );
    for (i = 0; i < n; ++i) {
      ic[i] = buffer[i][2];
    }
  }

  for (i = 0; i < n; ++i)
    free(buffer[i]);
  free(buffer);
}

