#include "pmgo.h"
#include "vespace.h"

int main()
{
  const char * filename =
    "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles3d/ThirtyCells.ovm";

  int k = 4;
  int *vDofsMap = NULL, *eStartDofsMap = NULL, *fStartDofsMap = NULL, *pStartDofsMap = NULL;
  dualMesh mesh;

  dualMeshInitialize( &mesh );
  dualMeshReadOVM( filename, &mesh );
  dualMeshConstructConnectivity( &mesh );
  /* dualMeshView( &mesh ); */

  vDofsMap      = (int *) malloc (mesh.Nver * sizeof(int));
  eStartDofsMap = (int *) malloc (mesh.Ned * sizeof(int));
  fStartDofsMap = (int *) malloc (mesh.Nfc * sizeof(int));
  pStartDofsMap = (int *) malloc (mesh.Npol * sizeof(int));

  getScalarDofsGlobalMapping( &mesh, k, vDofsMap, eStartDofsMap, fStartDofsMap, pStartDofsMap );

  free(vDofsMap);
  free(eStartDofsMap);
  free(fStartDofsMap);
  free(pStartDofsMap);
  dualMeshDestroy( &mesh );

  return 0;
}
