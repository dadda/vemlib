#include <math.h>
#include "quadrature.h"
#include "projectors.h"
#include "basisFunctions.h"
#include <lapacke.h>

int main()
{
  int i, j, k, dPk, dPkm2, Ndof, counter, counter2, Nq2d, scaleDof;

  int    *ipiv = NULL;
  double *polygon_extended = NULL;

  double *PNs = NULL, *D = NULL, *G = NULL, *PN = NULL;
  double *H = NULL, *H2 = NULL, *buffer = NULL;

  double *tmpN = NULL, *tmpW = NULL;
  double *quadNodes1d = NULL, *quadWeights1d = NULL;
  double **tmpX2d = NULL, **tmpY2d = NULL, **tmpW2d = NULL;
  double *xQuad2d = NULL, *yQuad2d = NULL, *wQuad2d = NULL;
  double *xbnd = NULL, *ybnd = NULL;
  double *Nx = NULL, *Ny = NULL;
  double x[2], y[2];
  double area, diam, xC, yC;

  double *basisOnBndNodes = NULL, *basisOnQuad = NULL;

  double *w = NULL;

  /***********************/

  // /* Unit square example */
  // int Nver = 4;
  // double vertices[] = { 0.0, 0.0,
  //                       1.0, 0.0,
  //                       1.0, 1.0,
  //                       0.0, 1.0 };
  // int face[] = {0, 1, 2, 3};

  // xC   = 0.5;
  // yC   = 0.5;
  // diam = sqrt(2.0);
  // area = 1.0;

  /***********************/

  /* Pentagon */
  int Nver = 5;
  double vertices[] = { 0.0, 0.0,
                        3.0, 0.0,
                        3.0, 2.0,
                        1.5, 4.0,
                        0.0, 4.0 };
  int face[] = {0, 1, 2, 3, 4};

  xC   = 19.0 / 14.0;
  yC   = 38.0 / 21.0;
  diam = 5.0;
  area = 10.5;

  /***********************/

  printf("Enter k: ");
  i = scanf("%i", &k);

  dPk   = ((k+2)*(k+1)) / 2;
  dPkm2 = (k*(k-1)) / 2;

  printf("Scale dof by polygon area (0/1): ");
  i = scanf("%i", &scaleDof);

  /***********************/

  Ndof = k * Nver + dPkm2;

  tmpN          = (double *) malloc ((k+1) * sizeof(double));
  tmpW          = (double *) malloc ((k+1) * sizeof(double));
  quadNodes1d   = (double *) malloc (2 * (k+1) * sizeof(double));
  quadWeights1d = (double *) malloc ((k+1) * sizeof(double));
  lobatto_compute(k+1, tmpN, tmpW);
  for (i = 0; i <= k; ++i) {
    quadNodes1d[2*i]   = (1.0 - tmpN[i]) * 0.5;
    quadNodes1d[2*i+1] = (1.0 + tmpN[i]) * 0.5;
    quadWeights1d[i]   = tmpW[i] * 0.5;
  }
  free(tmpN);
  free(tmpW);

  xbnd = (double *) malloc (k * Nver * sizeof(double));
  ybnd = (double *) malloc (k * Nver * sizeof(double));
  Nx   = (double *) malloc (Nver * sizeof(double));
  Ny   = (double *) malloc (Nver * sizeof(double));

  polygon_extended = (double *) malloc (2 * (Nver+1) * sizeof(double));

  for (i = 0; i < Nver; ++i) {
    x[0]    = vertices[2*i];            y[0] = vertices[2*i+1];
    x[1]    = vertices[2*((i+1)%Nver)]; y[1] = vertices[2*((i+1)%Nver)+1];
    xbnd[i] = x[0];
    ybnd[i] = y[0];
    for (j = 0; j < k-1; ++j) {
      xbnd[Nver + (k-1)*i + j] = quadNodes1d[2*(j+1)]*x[0] + quadNodes1d[2*(j+1)+1]*x[1];
      ybnd[Nver + (k-1)*i + j] = quadNodes1d[2*(j+1)]*y[0] + quadNodes1d[2*(j+1)+1]*y[1];
    }
    Nx[i] = y[1] - y[0];
    Ny[i] = x[0] - x[1];

    polygon_extended[i]        = x[0];
    polygon_extended[i+Nver+1] = y[0];
  }
  polygon_extended[Nver]     = polygon_extended[0];
  polygon_extended[2*Nver+1] = polygon_extended[Nver+1];

  /*
   * Matrix B
   */

  PNs = (double *) calloc (dPk * Ndof, sizeof(double));

  computeScalarBf( PNs, scaleDof, k, Nver, xC, yC, diam, area, xbnd, ybnd, Nx, Ny, quadWeights1d );

  printf("Matrix B:\n");
  for (i = 0; i < dPk; ++i) {
    for (j = 0; j < Ndof; ++j)
      printf("\t%f", PNs[dPk*j + i]);
    printf("\n");
  }
  printf("\n");

  /*
   * Matrix H
   */

  /* Chin algorithm */

  H  = (double *) malloc (dPk * dPk * sizeof(double));
  H2 = (double *) malloc (dPk * dPk * sizeof(double));

  computeScalarHf_chinAlg( H, k, vertices, face, Nver, diam, xC, yC );

  /* Numerical quadrature */

  tmpX2d  = (double **) malloc (Nver * (2*k+1) * sizeof(double *));
  tmpY2d  = (double **) malloc (Nver * (2*k+1) * sizeof(double *));
  tmpW2d  = (double **) malloc (Nver * (2*k+1) * sizeof(double *));
  xQuad2d = (double *) malloc (Nver * (2*k+1) * (2*k) * sizeof(double));
  yQuad2d = (double *) malloc (Nver * (2*k+1) * (2*k) * sizeof(double));
  wQuad2d = (double *) malloc (Nver * (2*k+1) * (2*k) * sizeof(double));

  for (i = 0; i < Nver*(2*k+1); ++i) {
    tmpX2d[i] = (double *) malloc (2*k * sizeof(double));
    tmpY2d[i] = (double *) malloc (2*k * sizeof(double));
    tmpW2d[i] = (double *) malloc (2*k * sizeof(double));
  }

  polygauss_2013(polygon_extended, Nver+1, 2*k, tmpX2d, tmpY2d, tmpW2d, &counter, &diam);
  Nq2d = counter * 2 * k;

  for (i = 0, counter2 = 0; i < counter; ++i, counter2 += 2*k) {
    memcpy(xQuad2d + counter2, tmpX2d[i], 2 * k * sizeof(double));
    memcpy(yQuad2d + counter2, tmpY2d[i], 2 * k * sizeof(double));
    memcpy(wQuad2d + counter2, tmpW2d[i], 2 * k * sizeof(double));
  }

  basisOnQuad = (double *) malloc (Nq2d * dPk * sizeof(double));
  buffer      = (double *) malloc (Nq2d * dPk * sizeof(double));
  counter     = 0;
  for (i = 0; i < Nq2d; ++i) {
    compute_Pk_2d(k, xQuad2d[i], yQuad2d[i], xC, yC, diam, buffer+counter);
    counter += dPk;
  }
  counter = 0;
  for (i = 0; i < dPk; ++i) {
    for (j = 0; j < Nq2d; ++j, ++counter) {
      basisOnQuad[counter] = buffer[dPk*j+i];
    }
  }
  free(buffer);

  computeScalarH_quadrature( H2, dPk, basisOnQuad, wQuad2d, Nq2d );

  // printf("Matrix H:\n");
  // for (i = 0; i < dPk; ++i) {
  //   for (j = 0; j < dPk; ++j)
  //     printf("   %f", H[dPk*j + i]);
  //   printf("\n");
  // }
  // printf("\n");

  // printf("Matrix H2:\n");
  // for (i = 0; i < dPk; ++i) {
  //   for (j = 0; j < dPk; ++j)
  //     printf("   %f", H2[dPk*j + i]);
  //   printf("\n");
  // }
  // printf("\n");

  // buffer = (double *) malloc (dPk * dPk * sizeof(double));

  // printf("Computing matrix difference\n");
  // for (i = 0; i < dPk; ++i)
  //   for (j = 0; j < dPk; ++j)
  //     buffer[dPk*j+i] = H[dPk*j+i] - H2[dPk*j+i];

  // printf("1-norm of the difference: %e\n",
  // 	 LAPACKE_dlange( LAPACK_COL_MAJOR, '1', dPk, dPk, buffer, dPk) );

  // w = (double *) malloc (dPk * sizeof(double));

  // /* Careful: H and H2 are get modified by *_dsyev() */

  // LAPACKE_dsyev( LAPACK_COL_MAJOR, 'N', 'L', dPk, H, dPk, w);
  // printf("Condition number of matrix H: %.16e\n", w[dPk-1] / w[0]);

  // LAPACKE_dsyev( LAPACK_COL_MAJOR, 'N', 'L', dPk, H2, dPk, w);
  // printf("Condition number of matrix H2: %.16e\n", w[dPk-1] / w[0]);

  // free(buffer);

  /*
   * \Pi^\nabla
   */

  basisOnBndNodes = (double *) malloc (k * Nver * dPk * sizeof(double));
  buffer          = (double *) malloc (k * Nver * dPk * sizeof(double));
  D               = (double *) malloc (Ndof * dPk * sizeof(double));
  G               = (double *) malloc (dPk * dPk * sizeof(double));

  counter = 0;
  for (i = 0; i < k*Nver; ++i) {
    compute_Pk_2d(k, xbnd[i], ybnd[i], xC, yC, diam, buffer+counter);
    counter += dPk;
  }
  counter = 0;
  for (i = 0; i < dPk; ++i) {
    for (j = 0; j < k*Nver; ++j, ++counter) {
      basisOnBndNodes[counter] = buffer[dPk*j+i];
    }
  }
  free(buffer);

  computeScalarDf(D, scaleDof, k, area, dPk, dPkm2, basisOnBndNodes, k*Nver, Ndof, H2 );

  /* Matrix G */
  cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, dPk, dPk, Ndof, 1.0,
	      PNs, dPk, D, Ndof, 0.0, G, dPk);

  printf("Matrix G:\n");
  for (i = 0; i < dPk; ++i) {
    for (j = 0; j < dPk; ++j)
      printf("\t%f", G[dPk*j + i]);
    printf("\n");
  }
  printf("\n");

  /* PNs */
  ipiv = (int *) malloc (dPk * sizeof(int));
  LAPACKE_dgesv(LAPACK_COL_MAJOR, dPk, Ndof, G, dPk, ipiv, PNs, dPk);
  free(ipiv);

  // printf("Matrix PNs:\n");
  // for (i = 0; i < dPk; ++i) {
  //   for (j = 0; j < Ndof; ++j)
  //     printf("   %f", PNs[dPk*j + i]);
  //   printf("\n");
  // }
  // printf("\n");

  /* PN */

  PN = (double *) calloc (Ndof * Ndof, sizeof(double));
  cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, Ndof, Ndof, dPk, 1.0,
	      D, Ndof, PNs, dPk, 0.0, PN, Ndof);

  printf("Matrix PN:\n");
  for (i = 0; i < Ndof; ++i) {
    for (j = 0; j < Ndof; ++j)
      printf("\t%f", PN[Ndof*j + i]);
    printf("\n");
  }
  printf("\n");

  /*
   * Release memory
   */

  for (i = 0; i < Nver*(2*k+1); ++i) {
    free(tmpX2d[i]);
    free(tmpY2d[i]);
    free(tmpW2d[i]);
  }
  free(polygon_extended);
  free(tmpX2d);
  free(tmpY2d);
  free(tmpW2d);
  free(xQuad2d);
  free(yQuad2d);
  free(wQuad2d);
  free(PNs);
  free(PN);
  free(D);
  free(G);
  free(H);
  free(H2);
  free(w);
  free(quadNodes1d);
  free(quadWeights1d);
  free(xbnd);
  free(ybnd);
  free(Nx);
  free(Ny);
  free(basisOnQuad);
  free(basisOnBndNodes);

  return 0;
}
