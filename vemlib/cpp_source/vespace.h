#ifndef _VESPACE_H
#define _VESPACE_H

#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include "pmgo.h"
#include "utilities.h"

void getScalarDofsGlobalMapping( const dualMesh * const mesh,
				 const int k,
				 int * const vDofsMap,
				 int * const eStartDofsMap,
				 int * const fStartDofsMap,
				 int * const pStartDofsMap );

#endif
