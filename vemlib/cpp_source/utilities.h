#ifndef _UTILITIES_H
#define _UTILITIES_H

#include <stdlib.h>


int compareInt  ( const void * a, const void * b );

int compareSize1( const void * a, const void * b );
int compareSize2( const void * a, const void * b );
int compareSize3( const void * a, const void * b );

void intersectSorted( const int * const a,
		      const int m,
		      const int * const b,
		      const int n,
		      int ** intersection,
		      int * k );

void unique( const int * const a,
	     const int n,
	     int **c,
	     int * cLength,
	     int * const ic );

#endif
