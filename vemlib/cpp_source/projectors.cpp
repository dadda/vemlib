#include "projectors.h"

using namespace GiNaC;

/*
 * Compute matrix Bf for a scalar VEM space
 */
void computeScalarBf( double * const Bf,
		      const int scaleDof,
		      const int k,
		      const int Nver,
		      const double faceCentroidX,
		      const double faceCentroidY,
		      const double faceDiam,
		      const double faceArea,
		      const double * const xbnd,
		      const double * const ybnd,
		      const double * const edgeNormalsX,
		      const double * const edgeNormalsY,
		      const double * const allQuadWeights1d )
{
  int i, j, q, dPk, dPkm2, Ndof;
  double tmp1, tmp2;
  double *dab = NULL;
  double *Gkm1x = NULL, *Gkm1y = NULL;

  dPk   = ((k+2)*(k+1)) / 2;
  dPkm2 = (k*(k-1))/2;
  Ndof  = k*Nver + dPkm2;

  memset( Bf, 0.0, Ndof * dPk * sizeof(double) );

  if ( k == 1 ) {
    /* Note: Nver = Ndof for k == 1 */

    tmp1 = 1.0 / (double)(Nver);
    tmp2 = 2.0 * faceDiam;
    for (j = 0; j < Nver; ++j) {
      Bf[dPk*j]   = tmp1;
      Bf[dPk*j+1] = ( edgeNormalsX[(Nver-1+j)%Nver] + edgeNormalsX[j] ) / tmp2;
      Bf[dPk*j+2] = ( edgeNormalsY[(Nver-1+j)%Nver] + edgeNormalsY[j] ) / tmp2;
    }

  } else {

    dab = (double *) calloc (dPk * dPkm2, sizeof(double));
    llcmb( k, faceDiam, dab );

    if ( scaleDof == 1 ) {
      for (j = 0; j < dPkm2; ++j)
	for (i = 0; i < dPk; ++i)
	  Bf[dPk*(k*Nver+j)+i] = - faceArea * dab[dPk*j+i];
    } else {
      for (j = 0; j < dPkm2; ++j)
	for (i = 0; i < dPk; ++i)
	  Bf[dPk*(k*Nver+j)+i] = - dab[dPk*j+i];
    }

    Bf[dPk*k*Nver] = 1.0;
    free( dab );

    Gkm1x = (double *) malloc ((dPk-1) * sizeof(double));
    Gkm1y = (double *) malloc ((dPk-1) * sizeof(double));

    for (j = 0; j < Nver; ++j) {
      compute_Gk_2d(k-1, xbnd[j], ybnd[j], faceCentroidX, faceCentroidY, faceDiam, Gkm1x, Gkm1y);
      for (i = 1; i < dPk; ++i) {
	Bf[dPk*j+i] =
	  ( (edgeNormalsX[(Nver-1+j)%Nver] + edgeNormalsX[j])*Gkm1x[i-1]
	    + (edgeNormalsY[(Nver-1+j)%Nver] + edgeNormalsY[j])*Gkm1y[i-1] ) * allQuadWeights1d[0];
      }
      for (q = 0; q < k-1; ++q) {
	compute_Gk_2d(k-1, xbnd[Nver+(k-1)*j+q], ybnd[Nver+(k-1)*j+q],
		      faceCentroidX, faceCentroidY, faceDiam, Gkm1x, Gkm1y);
	for (i = 1; i < dPk; ++i)
	  Bf[dPk*(Nver+(k-1)*j+q)+i] =
	    (edgeNormalsX[j] * Gkm1x[i-1] + edgeNormalsY[j] * Gkm1y[i-1]) * allQuadWeights1d[1+q];
      }
    }

    free(Gkm1x);
    free(Gkm1y);
  }
}


void computeScalarHf_chinAlg( double * const Hf,
			      const int k,
			      const double * const vertices,
			      const int * const face,
			      const int NvLoc,
			      const double faceDiam,
			      const double faceCentroidX,
			      const double faceCentroidY )
{
  int i, dPk;
  double edgeLength;

  double *vertices_copy = NULL, *a = NULL, *b = NULL;
  size_t *face_copy = NULL, *edgesCumsizes = NULL;

  int sR, sC, degxR, degxC, degyR, degyC, counterR, counterC;
  char str[256];

  ex g, Int;
  symbol x("x"), y("y");
  symtab table;

  table["x"] = x; table["y"] = y;
  parser reader(table);

  lst l;
  l = {x, y};

  dPk = ((k+2)*(k+1)) / 2;

  /* First reshape data in the format required by chinAlgorithm2D() */
  vertices_copy = (double *) malloc (2 * NvLoc * sizeof(double));
  a             = (double *) malloc (2 * NvLoc * sizeof(double));
  b             = (double *) malloc (NvLoc * sizeof(double));
  face_copy     = (size_t *) malloc (2 * NvLoc * sizeof(size_t));
  edgesCumsizes = (size_t *) malloc ((NvLoc+1) * sizeof(size_t));

  edgesCumsizes[0] = 0;
  for (i = 0; i < NvLoc; ++i) {
    vertices_copy[i]       = vertices[2*face[i]];
    vertices_copy[i+NvLoc] = vertices[2*face[i]+1];
    face_copy[2*i]           = (size_t)(i+1);
    face_copy[2*i+1]         = (size_t)((i+1)%NvLoc + 1);
    edgesCumsizes[i+1]       = edgesCumsizes[i] + 2;

    a[i]       = vertices[ 2*(face_copy[2*i+1]-1) + 1 ] -
                 vertices[ 2*(face_copy[2*i]-1) + 1 ];
    a[NvLoc+i] = vertices[ 2*(face_copy[2*i]-1) ] -
                 vertices[ 2*(face_copy[2*i+1]-1) ];

    edgeLength = cblas_dnrm2(2, a, NvLoc);

    a[i]       /= edgeLength;
    a[NvLoc+i] /= edgeLength;
    b[i]       = a[i] * vertices[ 2*(face_copy[2*i]-1) ] +
                 a[NvLoc+i] * vertices[ 2*(face_copy[2*i]-1) + 1 ];
  }

  memset(Hf, 0.0, dPk*dPk*sizeof(double));

  counterC = 0;
  for (sC = 0; sC <= k; ++sC) {
    for (degxC = sC; degxC >= 0; --degxC, ++counterC) {
      degyC    = sC-degxC;

      sprintf(str, "pow((x-(%.16f))/%.16f,%d) * pow((y-(%.16f))/%.16f,%d)",
	      faceCentroidX, faceDiam, 2*degxC,
	      faceCentroidY, faceDiam, 2*degyC);

      g   = reader(str);
      Int = chinAlgorithm2D(g, vertices_copy, (size_t)NvLoc, face_copy,
      			    (size_t)NvLoc, edgesCumsizes, a, b, l);
      if (is_a<numeric>(Int)) {
      	Hf[dPk*counterC+counterC] = ex_to<numeric>(Int).to_double();
      } else {
      	printf("File %s line %i: encountered error while computing Hf, aborting ...\n",
      	       __FILE__, __LINE__);
      	exit(1);
      }

      counterR = 0;
      for (sR = 0; sR <= k; ++sR) {
	for (degxR = sR; degxR >= 0; --degxR, ++counterR) {
	  degyR = sR-degxR;
	  if (counterR > counterC) {
	    sprintf(str, "pow((x-(%.16f))/%.16f,%d) * pow((y-(%.16f))/%.16f,%d)",
		    faceCentroidX, faceDiam, degxR+degxC,
		    faceCentroidY, faceDiam, degyR+degyC);
	    g   = reader(str);
	    Int = chinAlgorithm2D(g, vertices_copy, (size_t)NvLoc, face_copy,
	    			  (size_t)NvLoc, edgesCumsizes, a, b, l);
	    if (is_a<numeric>(Int)) {
	      Hf[dPk*counterC+counterR] = ex_to<numeric>(Int).to_double();
	      Hf[dPk*counterR+counterC] = Hf[dPk*counterC+counterR];
	    } else {
	      printf("File %s line %i: encountered error while computing Hf, aborting ...\n",
	    	     __FILE__, __LINE__);
	      exit(1);
	    }
	  }
	}
      }

    }
  }

  free(vertices_copy);
  free(a);
  free(b);
  free(face_copy);
  free(edgesCumsizes);
}


void computeScalarH_quadrature( double * const H,
				const int dPk,
				const double * const basisOnQd,
				const double * const wquad,
				const int Nq )
{
  int i, j, q;
  double tmp;

  memset(H, 0.0, dPk*dPk*sizeof(double));

  for (j = 0; j < dPk; ++j) {
    for (i = 0; i < j; ++i) {
      tmp  = 0.0;
      for (q = 0; q < Nq; ++q) tmp += wquad[q] * ( basisOnQd[Nq*i+q] * basisOnQd[Nq*j+q] );
      H[dPk*j+i] = tmp;
      H[dPk*i+j] = tmp;
    }
    tmp  = 0.0;
    for (q = 0; q < Nq; ++q) tmp += wquad[q] * ( basisOnQd[Nq*i+q] * basisOnQd[Nq*i+q] );
    H[dPk*i+i] = tmp;
  }
}


void computeScalarDf( double * const Df,
		      const int scaleDof,
		      const int k,
		      const double faceArea,
		      const int dPk,
		      const int dPkm2,
		      const double * const basisOnBndNodes,
		      const int NbndNodes,
		      const int Ndof,
		      const double * const Hf )
{
  int i, j;

  memset(Df, 0.0, Ndof * dPk * sizeof(double));

  if ( k == 1 ) {
    memcpy( Df, basisOnBndNodes, Ndof * dPk * sizeof(double) );
  } else {
    if ( scaleDof == 0 ) {
      for (i = 0; i < dPk; ++i) {
	memcpy( Df+Ndof*i, basisOnBndNodes+NbndNodes*i, NbndNodes*sizeof(double) );
	memcpy( Df+Ndof*i+NbndNodes, Hf+dPk*i, dPkm2*sizeof(double) );
      }
    } else {
      for (i = 0; i < dPk; ++i) {
	memcpy( Df+Ndof*i, basisOnBndNodes+NbndNodes*i, NbndNodes*sizeof(double) );
	for (j = 0; j < dPkm2; ++j) {
	  Df[Ndof*i+NbndNodes+j] = Hf[dPk*i+j] / faceArea;
	}
      }
    }
  }
}


void computeScalarBp( double * const Bp,
		      const int k,
		      const int NuV,
		      const int NuE,
		      const int Nfc,
		      const int * const p2iuniqueV,
		      const int * const p2iuniqueE,
		      const polyGeoQuad pGQ,
		      const double * const allQuadWeights1d )
{
  int i, j, f, NvLoc, counter;
  int dPkm2_2d, dPkm2_3d, dPk_2d, dPk_3d, Ndof;
  double tmp;

  int    *ipiv   = NULL;
  double *buffer = NULL;
  double *basisOnBndNodes_f = NULL, *D_f = NULL, *G_f = NULL, *PNs_f = NULL;

  dPkm2_2d = (k*(k-1)) / 2;
  dPk_2d   = ((k+2)*(k+1)) / 2;

  dPkm2_3d = ((k+1)*k*(k-1)) / 6;
  dPk_3d   = ((k+3)*(k+2)*(k+1)) / 6;

  Ndof     = NuV + (k-1)*NuE + dPkm2_2d*Nfc + dPkm2_3d;

  memset( Bp, 0.0, dPk_3d * Ndof * sizeof(double) );

  if (k == 1) {

    tmp = 1.0 / (double)NuV;
    for (j = 0; j < NuV; ++j) Bp[dPk_3d*j] = tmp;

    for (f = 0; f < Nfc; ++f) {
      NvLoc  = pGQ.facesCumsizes[f+1] - pGQ.facesCumsizes[f];

      /* Matrix B_f */
      PNs_f = (double *) calloc (dPk_2d * NvLoc * sizeof(double));
      computeScalarBf( PNs_f, 0, k, NvLoc,
		       pGQ.facesCentroidX[f], pGQ.facesCentroidY[f],
		       pGQ.facesDiameters[f], pGQ.facesAreas[f],
		       pGQ.facesBndNodesX[f], pGQ.facesBndNodesY[f],
		       pGQ.facesEdgeNormalsX[f], pGQ.facesEdgeNormalsY[f],
		       allQuadWeights1d );

      /* Matrix D_f */
      buffer = (double *) malloc (NvLoc * dPk_2d * sizeof(double));
      D_f     = (double *) malloc (NvLoc * dPk_2d * sizeof(double));

      counter = 0;
      for (i = 0; i < NvLoc; ++i) {
	compute_Pk_2d(k, pGQ.facesBndNodesX[f][i], pGQ.facesBndNodesY[f][i],
		      pGQ.facesCentroidX[f], pGQ.facesCentroidY[f],
		      pGQ.facesDiameters[f], buffer+counter);
	counter += dPk_2d;
      }
      counter = 0;
      for (i = 0; i < dPk_2d; ++i) {
	for (j = 0; j < NvLoc; ++j, ++counter) {
	  D_f[counter] = buffer[dPk_2d*j+i];
	}
      }

      /* Matrix G_f */
      G_f = (double *) malloc (dPk_2d * dPk_2d * sizeof(double));
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, dPk_2d, dPk_2d, NvLoc, 1.0,
		  PNs_f, dPk_2d, D_f, NvLoc, 0.0, G_f, dPk_2d);

      /* Matrix PNs_f */
      ipiv = (int *) malloc (dPk_2d * sizeof(int));
      LAPACKE_dgesv(LAPACK_COL_MAJOR, dPk_2d, NvLoc, G_f, dPk_2d, ipiv, PNs_f, dPk_2d);
      free(ipiv);

      free(PNs_f);
      free(buffer);
      free(D_f);
      free(G_f);
    }

  } else {

  }
}
