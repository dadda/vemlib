#include "vespace.h"

void getScalarDofsGlobalMapping( const dualMesh * const mesh,
				 const int k,
				 int * const vDofsMap,
				 int * const eStartDofsMap,
				 int * const fStartDofsMap,
				 int * const pStartDofsMap )
{
  int i, f, v, Nfc, counter, dofCounter, hFace, tFace, hEdge, tEdge, NvLoc;
  int metisNver, metisNed, status, NuV, NuE, pIdx;
  int dPkm2_2d, dPkm2_3d;
  int *p2v = NULL, *p2e = NULL;
  int *p2uniqueV = NULL, *p2uniqueE = NULL;
  int **buffer = NULL;
  char cmd[1024];
  FILE *fp = NULL;

  /*
   * Call Metis
   */

  metisNver = mesh->Npol;

  /* The number of edges in the metis graph file correspond to the number of internal faces */
  metisNed  = 0;
  for (i = 0; i < mesh->Nfc; ++i) if ( mesh->f2p[2*i+1] != -1 ) ++metisNed;

  fp = fopen("./__tmp__.metis", "w");
  fprintf(fp, "%i %i\n", metisNver, metisNed);
  for (i = 0; i < mesh->Npol; ++i) {
    Nfc = mesh->p2hfDim[i+1] - mesh->p2hfDim[i];
    for (f = 0; f < Nfc; ++f) {
      hFace = mesh->p2hf[ mesh->p2hfDim[i] + f ];
      tFace = hFace / 2;
      if ( mesh->f2p[2*tFace+1] != -1 ) {
	if ( mesh->f2p[2*tFace] == i )
	  fprintf(fp, "   %i", mesh->f2p[2*tFace+1]+1);
	else
	  fprintf(fp, "   %i", mesh->f2p[2*tFace]+1);
      }
    }
    fprintf(fp, "\n");
  }
  fclose(fp);

  sprintf(cmd, "/home/daniele/Documents/codes/metis-5.1.0/build/Linux-x86_64/programs/gpmetis "
	  "-ptype=rb ./__tmp__.metis %i", metisNver);
  status = system(cmd);
  if ( !WIFEXITED(status) ) {
    printf("File %s line %i: error running gpmetis, aborting ...\n", __FILE__, __LINE__);
    exit(1);
  }

  /* Remove auxiliary file */
  status = system("rm ./__tmp__.metis");
  if ( !WIFEXITED(status) ) {
    printf("File %s line %i: error removing auxiliary file, aborting ...\n", __FILE__, __LINE__);
    exit(1);
  }

  /* Read Metis output */

  buffer = (int **) malloc ((mesh->Npol) * sizeof(int *));

  cmd[0] = '\0';
  sprintf(cmd, "./__tmp__.metis.part.%i", metisNver);

  fp = fopen(cmd, "r");
  for (i = 0; i < mesh->Npol; ++i) {
    buffer[i] = (int *) malloc (2 * sizeof(int));
    if ( fscanf(fp, "%i", buffer[i]) != 1 ) {
      printf("File %s line %i: encountered error while reading Metis output file, aborting ...\n",
	     __FILE__, __LINE__);
      exit(1);
    }
    buffer[i][1] = i;
  }
  fclose(fp);

  /* Remove auxiliary file */
  cmd[0] = '\0';
  sprintf(cmd, "rm ./__tmp__.metis.part.%i", metisNver);
  status = system(cmd);
  if ( !WIFEXITED(status) ) {
    printf("File %s line %i: error removing auxiliary file, aborting ...\n", __FILE__, __LINE__);
    exit(1);
  }

  qsort( buffer, mesh->Npol, sizeof(int *), compareSize1 );

  /*
   * Compute dofs mappings by visiting polyhedra in the order specified by buffer[:][1]
   */

  for (i = 0; i < mesh->Nver; ++i) vDofsMap[i]      = -1;
  for (i = 0; i < mesh->Ned; ++i)  eStartDofsMap[i] = -1;
  for (i = 0; i < mesh->Nfc; ++i)  fStartDofsMap[i] = -1;
  for (i = 0; i < mesh->Npol; ++i) pStartDofsMap[i] = -1;

  dPkm2_2d = (k*(k-1)) / 2;
  dPkm2_3d = ((k+1)*k*(k-1)) / 6;

  dofCounter = 0;

  for (i = 0; i < mesh->Npol; ++i) {
    pIdx = buffer[i][1];
    Nfc  = mesh->p2hfDim[pIdx+1] - mesh->p2hfDim[pIdx];

    counter = 0;
    for (f = 0; f < Nfc; ++f) {
      hFace   = mesh->p2hf[ mesh->p2hfDim[pIdx] + f ];
      tFace   = hFace / 2;
      NvLoc   = mesh->f2heDim[ tFace + 1 ] - mesh->f2heDim[ tFace ];
      counter += NvLoc;
    }

    p2v     = (int *) malloc (counter * sizeof(int));
    p2e     = (int *) malloc (counter * sizeof(int));
    counter = 0;
    for (f = 0; f < Nfc; ++f) {
      hFace   = mesh->p2hf[ mesh->p2hfDim[pIdx] + f ];
      tFace   = hFace / 2;
      NvLoc   = mesh->f2heDim[ tFace + 1 ] - mesh->f2heDim[ tFace ];
      if ( hFace%2 ) {
	for (v = 0; v < NvLoc; ++v, ++counter) {
	  hEdge        = mesh->f2he[ mesh->f2heDim[tFace] + NvLoc - 1 - v ];
	  tEdge        = hEdge/2;
	  p2v[counter] = mesh->edges[ 2 * tEdge + 1 - hEdge%2 ];
	  p2e[counter] = tEdge;
	}
      } else {
	for (v = 0; v < NvLoc; ++v, ++counter) {
	  hEdge        = mesh->f2he[ mesh->f2heDim[tFace] + v ];
	  tEdge        = hEdge/2;
	  p2v[counter] = mesh->edges[ 2 * tEdge + hEdge%2 ];
	  p2e[counter] = tEdge;
	}
      }
    }

    unique(p2v, counter, &p2uniqueV, &NuV, NULL);
    unique(p2e, counter, &p2uniqueE, &NuE, NULL);

    for (v = 0; v < NuV; ++v) {
      if ( vDofsMap[p2uniqueV[v]] == -1 )
	vDofsMap[p2uniqueV[v]] = dofCounter++;
    }

    for (v = 0; v < NuE; ++v) {
      if ( eStartDofsMap[p2uniqueE[v]] == -1 ) {
	eStartDofsMap[p2uniqueE[v]] = dofCounter;
	dofCounter += k-1;
      }
    }

    for (f = 0; f < Nfc; ++f) {
      hFace   = mesh->p2hf[ mesh->p2hfDim[pIdx] + f ];
      tFace   = hFace / 2;
      if ( fStartDofsMap[tFace] == -1 ) {
	fStartDofsMap[tFace] = dofCounter;
	dofCounter += dPkm2_2d;
      }
    }

    pStartDofsMap[pIdx] = dofCounter;
    dofCounter += dPkm2_3d;

    free(p2v);
    free(p2e);
    free(p2uniqueV);
    free(p2uniqueE);
    free(buffer[i]);
  }
  free(buffer);

  if ( dofCounter != (mesh->Nver
		      + (k-1)*(mesh->Ned)
		      + dPkm2_2d*(mesh->Nfc)
		      + dPkm2_3d*(mesh->Npol)) ) {
    printf("File %s line %i: the final number of degrees of freedom is not correct, "
	   "aborting ...\n", __FILE__, __LINE__);
    exit(1);
  }

}
