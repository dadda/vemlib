
patternA  = '[*]* Start of Test ([0-9]), iM = ([0-9]), iN = ([0-9]), k = ([0-9])';
patternB  = 'D.o.f.: (.*)';
patternC  = 'Minimum eigenvalue: (.*)';
patternD  = 'Maximum eigenvalue: (.*)';
patternIt = 'Number of PCG iterations: (.*)';
patternE  = '[*]* End of Test ([0-9]), iM = ([0-9]), iN = ([0-9]), k = ([0-9])';

filename = '/home/daniele/Documents/codes/vem_matlab/Runs/testsFETI/2d/test_randVoro_smooth_data_high_order_part05.txt';
% filename = '/home/daniele/Documents/codes/vem_matlab/Runs/testsFETI/2d/test_randVoro_rand_data_high_order_part01.txt';

fid = fopen(filename, 'r');
while 1
   tline  = fgetl(fid);
   if ~ischar(tline), break, end
   tokens1 = regexp(tline, patternA, 'tokens');
   if ~isempty(tokens1)
       while 1
           tline   = fgetl(fid);
           if ~ischar(tline), break, end
           tokens2 = regexp(tline, patternB, 'tokens');
           if isempty(tokens2)
               tokensE = regexp(tline, patternE, 'tokens');
               if ~isempty(tokensE), break, end
           else
               tline   = fgetl(fid);               
               if ~ischar(tline), break, end
               tline   = fgetl(fid);               
               if ~ischar(tline), break, end               
               tokens3 = regexp(tline, patternC, 'tokens');
               if ~isempty(tokens3)
                   tline   = fgetl(fid);
                   if ~ischar(tline), break, end
                   tokens4 = regexp(tline, patternD, 'tokens');
                   if ~isempty(tokens4)
                       tline   = fgetl(fid);
                       if ~ischar(tline), break, end
                       tokens5 = regexp(tline, patternIt, 'tokens');
                       if ~isempty(tokens5)
                           numSubdomains = 16 * ( str2double(tokens1{1}{3}) )^2;
                           sqrtKappa = sqrt( str2double(tokens4{1}{1}) / str2double(tokens3{1}{1}) );
                           fprintf('\nTest %s, iM = %s, k = %s\n', tokens1{1}{1}, tokens1{1}{2}, tokens1{1}{4});
                           fprintf('Number of subdomains: %i\n', numSubdomains);
                           fprintf('D.o.f.: %s\n', tokens2{1}{1});
                           fprintf('Minimum eigenvalue: %s\n', tokens3{1}{1});
                           fprintf('Maximum eigenvalue: %s\n', tokens4{1}{1});
                           fprintf('Number of PCG iterations: %s\n', tokens5{1}{1});
                           fprintf('(%s, %.16e)\n', tokens2{1}{1}, sqrtKappa);
                           fprintf('(%s, %.16e)\n', tokens1{1}{4}, sqrtKappa);
                           fprintf('& %1.2f & %s\n', sqrtKappa, tokens5{1}{1});
                           fprintf('& $%1.2f (%s)$\n', sqrtKappa, tokens5{1}{1});
                           break;
                       end
                   end
               end
           end
       end
   end
end
fclose(fid);
