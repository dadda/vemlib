
patternStart         = '[*]* Start of Test [^.]*, iM = ([0-9]*), iR = ([0-9]*), iN = ([0-9]*), iA = ([0-9])';
patternNumSubdomains = 'Number of subdomains: (.*)';
patternOverh         = '1/h: (.*)';
patternHOverh        = 'H/h: (.*)';
patternDof           = 'D.o.f.: (.*)';
patternCoarse        = 'Dimension of coarse space: (.*)';
patternMinLambda     = 'Minimum eigenvalue: (.*)';
patternMaxLambda     = 'Maximum eigenvalue: (.*)';
patternIt            = 'Number of PCG iterations: (.*)';
patternErrorA        = 'Relative error energy norm: (.*)';
patternErrorInf      = 'Relative error inf norm: (.*)';
patternEnd           = '[*]* End of Test [^.]*, iM = [0-9]*, iR = [0-9]*, iN = [0-9]*, iA = ([0-9])';

filenames = {
%     '/home/daniele/Documents/codes/vem_matlab/Runs/testsFETI/3d/test1/test1_voro_part01.txt';
%     '/home/daniele/Documents/codes/vem_matlab/Runs/testsFETI/3d/test1/test1_voro_part02.txt';
%     '/home/daniele/Documents/codes/vem_matlab/Runs/testsFETI/3d/test1/test1_voro_checker_part01.txt';
%     '/home/daniele/Documents/codes/vem_matlab/Runs/testsFETI/3d/test1/test1_voro_checker_part02.txt';
%     '/home/daniele/Documents/codes/vem_matlab/Runs/testsFETI/3d/test1/test1_voro_checker_part03.txt';
%     '/home/daniele/Documents/codes/vem_matlab/Runs/testsFETI/3d/test2/test2_voro_part01.txt';
%     '/home/daniele/Documents/codes/vem_matlab/Runs/testsFETI/3d/test2/test2_voro_part02.txt';
%     '/home/daniele/Documents/codes/vem_matlab/Runs/testsFETI/3d/test2/test2_voro_part03.txt';
%     '/home/daniele/Documents/codes/vem_matlab/Runs/testsFETI/3d/test2/test2_voro_part04.txt';
%     '/home/daniele/Documents/codes/vem_matlab/Runs/testsFETI/3d/test2/test2_voro_checker_part01.txt';
%     '/home/daniele/Documents/codes/vem_matlab/Runs/testsFETI/3d/test2/test2_voro_checker_part02.txt';
%     '/home/daniele/Documents/codes/vem_matlab/Runs/testsFETI/3d/test1/test1_bcc_part01.txt';
%     '/home/daniele/Documents/codes/vem_matlab/Runs/testsFETI/3d/test1/test1_bcc_part02.txt';
%     '/home/daniele/Documents/codes/vem_matlab/Runs/testsFETI/3d/test1/test1_bcc_checker_part01.txt';
%     '/home/daniele/Documents/codes/vem_matlab/Runs/testsFETI/3d/test1/test1_bcc_checker_part02.txt';
%     '/home/daniele/Documents/codes/vem_matlab/Runs/testsFETI/3d/test2/test2_bcc_part01.txt';
%     '/home/daniele/Documents/codes/vem_matlab/Runs/testsFETI/3d/test2/test2_bcc_part02.txt';
    '/home/daniele/Documents/codes/vem_matlab/Runs/testsFETI/3d/test2/test2_bcc_checker_part01.txt';
    };

% Inspect the output in the files listed in 'filenames' to determine the
% correct values for the following quantities
numMeshes      = 8;
iRhoVectLength = 1;
NxVectLength   = 1;
numAlgorithms  = 7;

%
% Output options
%

journal    = 'SIAM';      % Format tables as prescribed in the paper to be submitted to SIAM
testType   = 2;           % 1, 2
dataType   = 'smooth';    % 'smooth', 'notsmooth'
outputType = 'figure';     % 'table', 'figure';
xCoord     = 'Dof';    % 'Dof', 'Hoh': horizontal coordinate axis for figures

%
%
%

L         = zeros(numMeshes, iRhoVectLength, NxVectLength, numAlgorithms); % Number of subdomains
hInv      = L;    % 1/h
Hoh       = L;    % H/h
dof       = L;    % Degrees of freedom
coarse    = L;    % Dimension of the coarse space
lambdaMin = L;    % Minimum eigenvalue
lambdaMax = L;    % Maximum eigenvalue
it        = L;    % Number of PCG iterations
errA      = L;    % Relative error in the energy norm
errInf    = L;    % Relative error in the inf norm

numFiles = numel(filenames);
for i = 1:numFiles
    fid = fopen(filenames{i}, 'r');

%     % This flag is checked to verify whether the current file has a broken
%     % output due to an error issued by MATLAB.
%     lastAlg = -1;

    while 1
        tline  = fgetl(fid);
        if ~ischar(tline), break, end
        tokens1 = regexp(tline, patternStart, 'tokens');
        if ~isempty(tokens1)
            iM = str2double(tokens1{1}{1});
            iR = str2double(tokens1{1}{2});
            iN = str2double(tokens1{1}{3});
            iA = str2double(tokens1{1}{4});
%             if tokens1{1}{1} ~= lastAlg
% %                 fprintf('\n\n');
%                 lastAlg = tokens1{1}{1};
%             end
            while 1
                tline   = fgetl(fid);
                if ~ischar(tline), break, end
                tokens2 = regexp(tline, patternEnd, 'tokens');
                if ~isempty(tokens2), break, end
                tokens3 = regexp(tline, patternNumSubdomains, 'tokens');
                if ~isempty(tokens3)
                    tline    = fgetl(fid);
                    tokens4  = regexp(tline, patternOverh, 'tokens');
                    tline    = fgetl(fid);
                    tokens5 = regexp(tline, patternHOverh, 'tokens');
                    tline    = fgetl(fid);
                    tokens6  = regexp(tline, patternDof, 'tokens');
                    tline    = fgetl(fid);
                    tokens7  = regexp(tline, patternCoarse, 'tokens');
                    tline    = fgetl(fid);
                    tokens8  = regexp(tline, patternMinLambda, 'tokens');
                    tline    = fgetl(fid);
                    tokens9  = regexp(tline, patternMaxLambda, 'tokens');
                    tline    = fgetl(fid);
                    tokens10 = regexp(tline, patternIt, 'tokens');
                    tline    = fgetl(fid);
                    tokens11 = regexp(tline, patternErrorA, 'tokens');
                    tline    = fgetl(fid);
                    tokens12 = regexp(tline, patternErrorInf, 'tokens');

                    L(iM, iR, iN, iA)         = str2double(tokens3{1}{1});
                    hInv(iM, iR, iN, iA)      = str2double(tokens4{1}{1});
                    Hoh(iM, iR, iN, iA)       = str2double(tokens5{1}{1});
                    dof(iM, iR, iN, iA)       = str2double(tokens6{1}{1});
                    coarse(iM, iR, iN, iA)    = str2double(tokens7{1}{1});
                    lambdaMin(iM, iR, iN, iA) = str2double(tokens8{1}{1});
                    lambdaMax(iM, iR, iN, iA) = str2double(tokens9{1}{1});
                    it(iM, iR, iN, iA)        = str2double(tokens10{1}{1});
                    if ~isempty(tokens11)
                        errA(iM, iR, iN, iA)   = str2double(tokens11{1}{1});
                    end
                    if ~isempty(tokens12)
                        errInf(iM, iR, iN, iA) = str2double(tokens12{1}{1});
                    end

%                     lambdam  = str2double(tokens8{1}{1});
%                     lambdaM  = str2double(tokens9{1}{1});
%                     if strcmp( outputType, 'table')
%                         if testType == 1
%                             fprintf('%i & %.2f & %i & %i & %.2f & %.2f & %i & %1.2e & %1.2e\\\\\n', ...
%                                 str2double(tokens3{1}{1}), str2double(tokens4{1}{1}), ...
%                                 str2double(tokens6{1}{1}), str2double(tokens7{1}{1}), ...
%                                 lambdam, lambdaM, str2double(tokens10{1}{1}), ...
%                                 str2double(tokens11{1}{1}), str2double(tokens12{1}{1}) );
%                         elseif testType == 2
%                             fprintf('%i & %.2f & %i & %i & %.2f & %.2f & %i & %1.2e\\\\\n', ...
%                                 str2double(tokens3{1}{1}), str2double(tokens4{1}{1}), ...
%                                 str2double(tokens6{1}{1}), str2double(tokens7{1}{1}), ...
%                                 lambdam, lambdaM, str2double(tokens10{1}{1}), ...
%                                 str2double(tokens11{1}{1}) );
%                         else
%                             fprintf('%i & %.2f & %i & %i & %.2f & %.2f & %i\\\\\n', ...
%                                 str2double(tokens3{1}{1}), str2double(tokens4{1}{1}), ...
%                                 str2double(tokens6{1}{1}), str2double(tokens7{1}{1}), ...
%                                 lambdam, lambdaM, str2double(tokens10{1}{1}) );
%                         end
%                     else
%                         fprintf('(%g, %g)\n', str2double(tokens5{1}{1}), sqrt(lambdaM/lambdam));
%                     end
                end
            end
        end
    end
    fclose(fid);
end

% testType   = 1;
% journal    = 'SIAM';    % Format tables as prescribed in the paper to be submitted to SIAM
% outputType = 'table';  % 'table'; 'figure';
% 
% numMeshes      = 1;
% iRhoVectLength = 1;
% NxVectLength   = 6;
% numAlgorithms  = 7;
% 
% L(iM, iR, iN, iA)         = str2double(tokens3{1}{1});
% hInv(iM, iR, iN, iA)      = str2double(tokens4{1}{1});
% Hoh(iM, iR, iN, iA)       = str2double(tokens5{1}{1});
% dof(iM, iR, iN, iA)       = str2double(tokens6{1}{1});
% coarse(iM, iR, iN, iA)    = str2double(tokens7{1}{1});
% lambdaMin(iM, iR, iN, iA) = str2double(tokens8{1}{1});
% lambdaMax(iM, iR, iN, iA) = str2double(tokens9{1}{1});
% it(iM, iR, iN, iA)        = str2double(tokens10{1}{1});
% errA(iM, iR, iN, iA)      = str2double(tokens11{1}{1});
% errInf(iM, iR, iN, iA)    = str2double(tokens12{1}{1});

if strcmp(journal, 'SIAM')
    if testType == 1
        iM = 1;
        iR = 1;
        if strcmp(outputType, 'table')
            fprintf('\n');
            for iN = 1:NxVectLength
                fprintf('%i', L(iM, iR, iN, 1))
                for iA = 1:5
                    kappa = lambdaMax(iM, iR, iN, iA) / lambdaMin(iM, iR, iN, iA);
                    if strcmp(dataType, 'notsmooth') && iA == 3 % Only primal faces
                        fprintf('   &   %e   &   %i', kappa, it(iM, iR, iN, iA));
                    else
                        fprintf('   &   %f   &   %i', kappa, it(iM, iR, iN, iA));
                    end
                end
                fprintf('\\\\\n');
            end
            fprintf('\n')
        end
    else
        iR = 1;
        iN = 1;
        if strcmp(outputType, 'table')
            
        elseif strcmp(outputType, 'figure')
            fprintf('\n');
            for iA = 1:numAlgorithms
                fprintf('iA = %i\n', iA);
                fprintf('-------\n');
                for iM = 1:numMeshes
                    kappa = lambdaMax(iM, iR, iN, iA) / lambdaMin(iM, iR, iN, iA);
                    if strcmp(xCoord, 'Dof')
                        fprintf('(%i, %g)\n', dof(iM, iR, iN, iA), sqrt(kappa));
                    elseif strcmp(xCoord, 'Hoh')
                        fprintf('(%g, %g)\n', Hoh(iM, iR, iN, iA), sqrt(kappa));
                    end
                end
                fprintf('\n')
            end
        end        
    end
end














