function [ Dof, TimeSetupSolve, Residual2norm ] = read_direct_solver_output(filename)

patternTime          = ' 2: KSPSetUpAndSolve: ([^ ]*)';
patternDof           = 'Dof: (\d+)';
patternResidual2norm = '(Absolute|Relative)? residual 2-norm: (.*)';
patternReason        = 'Reason: used by the preonly preconditioner that always uses ONE iteration';
fid = fopen(filename, 'r');

Dof = nan;
TimeSetupSolve = nan;
flagReason = 0;

while 1
    tline = fgetl(fid);
    if ~ischar(tline), break, end
    tokensTime  = regexp(tline, patternTime, 'tokens');
    tokensDof   = regexp(tline, patternDof, 'tokens');
    tokensRes   = regexp(tline, patternResidual2norm, 'tokens');
    if ~isempty(tokensTime)
        TimeSetupSolve = str2double(tokensTime{1}{1});
    elseif ~isempty(tokensDof)
        Dof = str2double(tokensDof{1}{1});
    elseif ~isempty(tokensRes)
        Residual2norm = str2double(tokensRes{1}{2});
    elseif strcmp(tline, patternReason)
        flagReason = flagReason + 1;
    end
end

fclose(fid);

if flagReason ~= 2
    Dof            = nan;
    TimeSetupSolve = nan;
    Residual2norm  = nan;
end

end
