function [ Dof, Kappa, TimeSetupSolve, Iterations, Reference, ErrA ] = ...
    read_iterative_solver_output(filename)

patternKappa  = '\s*\d+ KSP Residual norm [^%]*% max [^m]*min [^m]*max/min (.*)';
patternTime   = ' 2: KSPSetUpAndSolve: ([^ ]*)';
patternDof    = 'Dof: (\d+)';
patternIt     = 'Iterations: (\d+)';
patternReason = 'Reason:( )?(relative|\(preconditioned\))? residual (l)?2-norm [^(=]*(\(|=) ([^ ,]*).*';
patternErrA   = 'Relative error energy norm: (.*)';
fid = fopen(filename, 'r');

Kappa          = nan;
TimeSetupSolve = nan;
Dof            = nan;
Iterations     = nan;
Reference      = nan;
ErrA           = nan;
flagReason     = 0;

while 1
    tline = fgetl(fid);
    if ~ischar(tline), break, end
    tokensKappa  = regexp(tline, patternKappa, 'tokens');
    tokensTime   = regexp(tline, patternTime, 'tokens');
    tokensDof    = regexp(tline, patternDof, 'tokens');
    tokensIt     = regexp(tline, patternIt, 'tokens');
    tokensReason = regexp(tline, patternReason, 'tokens');
    tokensErrA   = regexp(tline, patternErrA, 'tokens');
    if ~isempty(tokensKappa)
        Kappa = str2double(tokensKappa{1}{1});
    elseif ~isempty(tokensTime)
        TimeSetupSolve = str2double(tokensTime{1}{1});
    elseif ~isempty(tokensDof)
        Dof = str2double(tokensDof{1}{1});
    elseif ~isempty(tokensIt)
        Iterations = str2double(tokensIt{1}{1});
    elseif ~isempty(tokensReason)
        flagReason = flagReason + 1;
        Reference = str2double(tokensReason{1}{5});
    elseif ~isempty(tokensErrA)
        ErrA = str2double(tokensErrA{1}{1});
    end
end

fclose(fid);

if flagReason ~= 2
    Dof            = nan;
    Kappa          = nan;
    TimeSetupSolve = nan;
    Iterations     = nan;
    Reference      = nan;
    ErrA           = nan;
end

end
