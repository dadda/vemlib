\documentclass[11pt]{article}
\usepackage{amsmath,amssymb,amsthm}
\usepackage{graphicx}
\usepackage{geometry}
\usepackage{caption}
\usepackage{subfig}
\usepackage{booktabs}
\usepackage{siunitx}
\usepackage{cite}
\usepackage{tikz}
\usepackage{pgfplots}
\pgfplotsset{compat=1.13}
\usepackage{multirow}

% \geometry{a4paper, top=3cm, bottom=3cm, left=1cm, right=1cm}
% \geometry{margin=1in}

\begin{document}

\title{Numerical Tests for FETI-DP in Three Dimensions for the Virtual Element Method Applied to the Poisson Equation}
\author{Daniele Prada}

\maketitle

%
%
%

\section{Introduction}

Consider the model problem
\begin{subequations}\label{eq:poisson}
\begin{align}
  -\nabla\cdot(\rho \nabla u) &= f\qquad \text{in }\Omega = (0,1)^3,\\
  u &= g\qquad \text{on }\partial\Omega,
\end{align}
\end{subequations}
where $\rho$ is the diffusion coefficient.

In the framework of the Virtual Element Method, the most common assumptions on a tessellation $\Omega_h$ of the computation domain $\Omega$ can be formulated in terms of the following parameters~\cite{bertoluzza2017}
%
\begin{itemize}
  \item $\displaystyle h = \max_{K\in\Omega_h} h_K$, where $h_K$ is the diameter of element $K\in\Omega_h$.
  \item $\displaystyle h_\textup{min} = \min_{K\in\Omega_h} h_{\textup{min},K}$, where $h_{\textup{min},K}$ is the minimum distance between any two vertices of $K$.
  \item $\displaystyle \gamma_0 = \min_{K\in\Omega_h}\frac{\rho_K}{h_K}$, where $\rho_K$ is the radius of the largest ball that is contained inside $K$. Note that this ball might not be unique~\cite{murty2010}.
  \item $\displaystyle \gamma_1 = \min_{K\in\Omega_h}\frac{h_{\textup{min},K}}{h_K}$.
  \item $\displaystyle \alpha_0 = \frac{h_\textup{min}}{h}$.
\end{itemize}

Tables~\ref{tab:cubicalmeshes}, \ref{tab:bccmeshes} and~\ref{tab:randVoro} list the values of these geometrical parameters for the reference meshes used in the experiments.
\begin{table}
  \centering
  \caption{Data for the cubical meshes used in the experiments.}
  \label{tab:cubicalmeshes}
  \begin{tabular}{c
      S[table-format=1.2e-2]
      S[table-format=1.2e-2]
      S[table-format=1.2e-2]
      S[table-format=1.2e-2]
      S[table-format=1.2]
    }
    \toprule
    Mesh & $h$ & {$h_\textup{min}$} & {$\gamma_0$} & {$\gamma_1$} & {$\alpha_0$}\\
    \midrule
    cube00064 & 4.33e-01 & 2.50e-01 & 2.89e-01 & 5.77e-01 & 1.00e+00\\
    cube00216 & 2.89e-01 & 1.67e-01 & 2.89e-01 & 5.77e-01 & 1.00e+00\\
    cube00512 & 2.17e-01 & 1.25e-01 & 2.89e-01 & 5.77e-01 & 1.00e+00\\
    cube01728 & 1.44e-01 & 8.33e-02 & 2.89e-01 & 5.77e-01 & 1.00e+00\\
    cube04096 & 1.08e-01 & 6.25e-02 & 2.89e-01 & 5.77e-01 & 1.00e+00\\
    cube08000 & 8.66e-02 & 5.00e-02 & 2.89e-01 & 5.77e-01 & 1.00e+00\\
    cube13824 & 7.22e-02 & 4.17e-02 & 2.89e-01 & 5.77e-01 & 1.00e+00\\
    cube21952 & 6.19e-02 & 3.57e-02 & 2.89e-01 & 5.77e-01 & 1.00e+00\\
    cube27000 & 5.77e-02 & 3.33e-02 & 2.89e-01 & 5.77e-01 & 1.00e+00\\
    cube32768 & 5.41e-02 & 3.12e-02 & 2.89e-01 & 5.77e-01 & 1.00e+00\\
    \bottomrule
  \end{tabular}
\end{table}

\begin{table}
  \centering
  \caption{Data for the meshes of truncated octahedra used in the experiments.}
  \label{tab:bccmeshes}
  \begin{tabular}{c
      S[table-format=1.2e-02]
      S[table-format=1.2e-02]
      S[table-format=1.2e-02]
      S[table-format=1.2e-02]
      S[table-format=1.2e-02]
    }
    \toprule
    Mesh & $h$ & {$h_\textup{min}$} & {$\gamma_0$} & {$\gamma_1$} & {$\alpha_0$}\\
    \midrule
    oct04 & 4.33e-01 & 6.25e-02 & 2.71e-01 & 1.44e-01 & 6.45e-01\\
    oct06 & 2.89e-01 & 4.17e-02 & 2.71e-01 & 1.44e-01 & 6.45e-01\\
    oct08 & 2.17e-01 & 3.12e-02 & 2.71e-01 & 1.44e-01 & 6.45e-01\\
    oct10 & 1.73e-01 & 2.50e-02 & 2.71e-01 & 1.44e-01 & 6.45e-01\\
    oct12 & 1.44e-01 & 2.08e-02 & 2.71e-01 & 1.44e-01 & 6.45e-01\\
    oct14 & 1.24e-01 & 1.79e-02 & 2.71e-01 & 1.44e-01 & 6.45e-01\\
    oct16 & 1.08e-01 & 1.56e-02 & 2.71e-01 & 1.44e-01 & 6.45e-01\\
    oct18 & 9.62e-02 & 1.39e-02 & 2.71e-01 & 1.44e-01 & 6.45e-01\\
    \bottomrule
  \end{tabular}
\end{table}

\begin{table}
  \centering
  \caption{Data for the random Voronoi meshes used in the experiments.}
  \label{tab:randVoro}
  \begin{tabular}{c
      S[table-format=1.2e-02]
      S[table-format=1.2e-02]
      S[table-format=1.2e-02]
      S[table-format=1.2e-02]
      S[table-format=1.2e-02]
    }
    \toprule
    Mesh & $h$ & {$h_\textup{min}$} & {$\gamma_0$} & {$\gamma_1$} & {$\alpha_0$}\\
    \midrule
    voro0032 & 8.76e-01 & 6.49e-04 & 8.79e-02 & 8.98e-04 & 6.05e-01\\
    voro0064 & 7.16e-01 & 2.40e-04 & 8.64e-02 & 4.57e-04 & 4.88e-01\\
    voro0128 & 5.66e-01 & 2.48e-04 & 1.02e-01 & 7.15e-04 & 3.99e-01\\
    voro0256 & 5.37e-01 & 1.26e-04 & 8.61e-02 & 2.34e-04 & 3.13e-01\\
    voro0512 & 3.62e-01 & 2.60e-05 & 4.24e-02 & 8.87e-05 & 3.32e-01\\
    voro1024 & 2.81e-01 & 5.69e-06 & 7.85e-02 & 2.26e-05 & 3.34e-01\\
    voro2048 & 2.46e-01 & 3.12e-06 & 7.88e-02 & 2.50e-05 & 3.28e-01\\
    voro4096 & 1.90e-01 & 1.31e-06 & 6.52e-02 & 8.47e-06 & 3.04e-01\\
    voro8192 & 1.48e-01 & 5.93e-08 & 5.03e-02 & 5.56e-07 & 2.89e-01\\
    \bottomrule
  \end{tabular}
\end{table}

\begin{figure}
  \centering
  \includegraphics[width=0.75\textwidth]{Figures3D/bcc_lattice_08}
  \caption{View of a mesh made of truncated octahedra, obtained by computing the Voronoi diagram corresponding
    to a body centered cubic lattice.}
\end{figure}

%
%
%

\section{Numerical Results}

Problem~\eqref{eq:poisson} is discretized with virtual elements of degree $1$. In all experiments $\Omega$ is divided into $N\times N\times N$ cubic subdomains with side length $H = 1/N$. 
For simplicity, subdomain meshes are obtained by rescaling and reflecting one of the meshes of the unit cube shown in Tables~\ref{tab:cubicalmeshes}, \ref{tab:bccmeshes} and~\ref{tab:randVoro}.

Let $\Gamma$ be the interface between subdomains. The following algorithms are tested:
\begin{itemize}
\item Algorithm A: the solution is forced to be continuous at interface vertices. In other words, only values at interface vertices are classified as \emph{primal} degrees of freedom.
\item Algorithm B: the solution is forced to have continuous average on interface edges. In other words, only averages on interface edges are classified as \emph{primal} degrees of freedom.
\item Algorithm C: continuity of interface face averages is enforced.
\item Algorithm D: primal vertex and edge constraints are enforced.
\item Algorithm E: primal vertex and face constraints are enforced.
\item Algorithm F: primal edge and face constraints are enforced.
\item Algorithm G: primal vertex, edge, and face constraints are enforced.
\end{itemize}
%
We use the symbols $\Pi_V, \Pi_E$, or $\Pi_F$ to indicate that primal vertex, edge, or face constraints are enforced.
%
Edge and face constraints are imposed using an explicit change of basis. To simplify the implementation, we work with a fully redundant set of Lagrange multipliers for the dual part of the solution $u_\Delta$. In order to analyze the numerical scalability of these algorithms, we perform two types of experiments:

\begin{enumerate}
\item We fix the subdomain problem size by choosing a reference mesh (either \emph{cube04096} in Table~\ref{tab:cubicalmeshes}, or \emph{oct08} in Table~\ref{tab:bccmeshes}, or \emph{voro1024} in Table~\ref{tab:randVoro}) and increase the number of subdomains and thus the overall problem size. Thus, $H/h$ is fixed.\label{test1}
\item We fix the number of subdomains (i.e. $H$ is kept constant) and increase the size of the local problems by choosing finer and finer reference meshes, thereby incrementing the overall problem size. This results in a smaller $h$ and a bigger $H/h$.\label{test2}
\end{enumerate}
%
For both types of experiments, the diffusion coefficient $\rho$ is $1$ in each subdomain. Load term and Dirichlet boundary conditions are taken such that
\[
u = \frac{1}{12\pi^2}\sin(2\pi x)\sin(2\pi y)\sin(2\pi z)
\]
is the exact solution. We use the conjugate gradient with the FETI-DP Dirichlet preconditioner, with zero initial guess and, as a stopping criterion, the relative reduction of the dual residual by $10^{-6}$. MATLAB$^\copyright$ is used as the subdomain and coarse sparse direct solver. Results for the first experiment are shown in the following tables:
\begin{itemize}
\item meshes of cubes: Tables~\ref{tab:test1-algA}--\ref{tab:test1-algG};
\item meshes of truncated octahedra: Tables~\ref{tab:test1-algA-truncoct}--\ref{tab:test1-algG-truncoct};
\item meshes of arbitrary Voronoi cells: Tables~\ref{tab:test1-algA-randVoro}--\ref{tab:test1-algG-randVoro}.
\end{itemize}
%
Results for the second experiment are shown in the following tables and figures:
\begin{itemize}
\item meshes of cubes: Tables~\ref{tab:test2-algA}--\ref{tab:test2-algG} and Figures~\ref{fig:cubes-test2-test3B-Alg-ABC}, \ref{fig:cubes-test2-test3B-Alg-DEFG} (see solid lines);
\item meshes of truncated octahedra: Tables~\ref{tab:test2-algA-truncoct}--\ref{tab:test2-algG-truncoct} and Figure~\ref{fig:test2-truncoct};
\item meshes of arbitrary Voronoi cells: Tables~\ref{tab:test2-algA-randVoro}--\ref{tab:test2-algG-randVoro} and Figure~\ref{fig:test2-voro}.
\end{itemize}

In order to test the robustness of FETI-DP, the two previous experiments are repeated with a load term $f$ varying randomly and uniformly in $[-1, 1]$ and, for each subdomain, a diffusion coefficient $\rho = 10^\alpha$, where $\alpha$ is a random integer in $[-10,10]$. Repeating the first experiment with this new choice of data gives the following results:
\begin{itemize}
\item meshes of cubes: Tables~\ref{tab:test3A-algA}--\ref{tab:test3A-algG};
\item meshes of truncated octahedra: Tables~\ref{tab:test3A-algA-truncoct}--\ref{tab:test3A-algG-truncoct};
\item meshes of arbitrary Voronoi cells: Tables~\ref{tab:test3A-algA-randVoro}--\ref{tab:test3A-algG-randVoro}.
\end{itemize}
%
Results for the second experiment with the new data are shown in the following tables and Figures:
\begin{itemize}
\item meshes of cubes: Tables~\ref{tab:test3B-algA}--\ref{tab:test3B-algG} and Figures~\ref{fig:cubes-test2-test3B-Alg-ABC}, \ref{fig:cubes-test2-test3B-Alg-DEFG} (see dashed lines);
\end{itemize}

\clearpage

\input{tables_test1}

\clearpage

\input{tables_test2}

\clearpage

\input{tables_test3}


\bibliography{VEM3D_numerical_tests}{}
\bibliographystyle{plain}

\end{document}
