
pattern = '[.]*\$';

% filename = '/home/daniele/Documents/codes/vem_matlab/Runs/testsCurved/ucirc__m_1_2_3_4_5_6__gamma_100__sol_cos4.txt';
% filename = '/home/daniele/Documents/codes/vem_matlab/Runs/testsCurved/flower__m_1_2_3_4_5_6__gamma_100__sol_shannon45.txt';
filename = '/home/daniele/Documents/codes/vem_matlab/Runs/testsCurved/spiral_fat__m_1_2_3_4_5_6__gamma_100__sol_squeezebox_fast.txt';

numMeshes = 7;
mVect     = 1:6;

numM  = length(mVect);
names = cell(numMeshes, 1);
h     = zeros(numMeshes, 1);
errS  = zeros(numMeshes * numM, 1);
errL2 = zeros(numMeshes * numM, 1);

fid = fopen(filename, 'r');

linelist = cell(numMeshes * numM, 1);
counter  = 0;

while 1
    tline = fgetl(fid);
    if ~ischar(tline), break, end
    match = regexp(tline, pattern);
    if ~isempty(match)
        counter           = counter + 1;        
        linelist{counter} = tline;
    end
end
fclose(fid);

if size(linelist,1) ~= numMeshes * numM
    error('Something is wrong in the output')
end

%
% First set of rows - read h, and errors for m = 1
%

totSize = numMeshes * numM;
for i = 1:totSize
    line = strsplit(linelist{i}, '&');
    if i <= numMeshes
        names{i} = line{1};
        h(i)     = str2double(line{2});
    end
    errS(i)  = str2double(line{4});
    errL2(i) = str2double(line{6});
end

errS  = reshape(errS, numMeshes, numM);
errL2 = reshape(errL2, numMeshes, numM);

%
% Print table for SIMAI conference
%

fprintf('\n\n');

for i = 1:numMeshes
    fprintf('%s   &   %e', names{i}, h(i));
%     % Print both errS and errL2
%     for j = 1:numM
%         fprintf('   &   %e   &   %e', errS(i,j), errL2(i,j));
%     end
    % Print only errS
    for j = 1:numM
        fprintf('   &   %e', errS(i,j));
    end
    fprintf('\\\\\n');
end

