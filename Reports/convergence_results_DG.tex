\documentclass[11pt]{article}
\usepackage{titlesec} %to change format of sections
\usepackage{amsmath,amssymb,amsthm}
\usepackage{graphicx}
\usepackage[margin=1in]{geometry}
\usepackage{fancyhdr}
\usepackage{enumerate}
\usepackage{tikz}
\usepackage{wrapfig}
\usepackage{siunitx}
\usepackage{subfig}
\usepackage{caption}
\usepackage{booktabs}
\usepackage{braket}
\usepackage{multirow}
\usepackage{url}

\begin{document}

\title{Convergence tests for DG-VEM}
\author{Daniele Prada}

\maketitle

\section*{Introduction}

In the MATLAB code, the user is allowed to choose:
\begin{itemize}
\item the degree $k$ of the DG-VEM method;
\item the type of basis functions for $P_k(K)$; two types are currently available:
\begin{enumerate}
\item $m_\alpha = [(x-x_K)/h_K]^{\alpha_1}[(y-y_K)/h_K]^{\alpha_2}$, where $(x_K,y_K)$ and $h_K$ are the centroid and the diameter, respectively, of element $K$;
\item $\bar m_\alpha = m_\alpha / ||m_\alpha||_{L^2(K)}$;
\end{enumerate}
\item additional order of exactness for 2D quadrature formulas;
\item compressed 2D quadrature formulas, i.e., with a number of points and weights equal to the dimension of $P_k(K)$ (option not working with Advanpix);
\item the stabilization coefficient $t$, with $t = -1, 0$, or $1$;
\item the scaling factor $\alpha = [\alpha_K]_{K\in\Omega_h}$ multiplying the VEM stabilization forms $[s_K]_{K\in\Omega_h}$; currently only the option $\alpha = C\cdot\verb|ones(1, nbElements)|$, with $C$ constant, is allowed;
\item the stabilization term, which can be computed by following either a standard VEM approach~\cite{beirao2014}, or a mortar approach (need a reference here). Stabilization can be either computed on few reference elements (a triangle, a square, ...) and then scaled by the area of the current element $K$ of the mesh, or computed from scratch on each mesh element.
\item how to compute the dual product between the load term $f$ and the VEM functions $\phi \in W(K)$ to compute action of the stabilization $s_K$ on $f$:
\begin{enumerate}
\item $\langle f, \phi\rangle \approx \langle f, \Pi^0_{k+2}\phi \rangle$;
\item $\langle f, \phi\rangle \approx \langle f, \Pi^0_{k+1}\phi \rangle$;
\item $\langle f, \phi\rangle \approx \langle f, \Pi^0_{k}\phi \rangle$;
\item $\langle f, \phi\rangle \approx \langle f, \Pi^\nabla_{k+2}\phi \rangle$;
\end{enumerate}
where $\Pi^\nabla_*$ is the VEM $\Pi^\nabla$-projection, and $\Pi^0_*$ is the VEM $L^2$-projection. For convenience, we recall that a function $\phi$ in the VEM space $W(K)$ is defined by the following properties:
\begin{itemize}
\item $\phi$ is polynomial of degree $k+2$ on each edge $e$ of $K$;
\item $\phi$ on $\partial K$ is globally continuous;
\item $\Delta\phi$ is a polynomial of degree $k$ in $K$;
\item all the moments of $\phi$ up to degree $k$ are zero, i.e., $(m_\alpha, \phi) = 0$, for all $m_\alpha$ up to degree $k$.
\end{itemize}
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section*{Test case 1}

\begin{table}
  \centering
  \begin{tabular}{lccc}
    \toprule
    Name & Nb. of elements & Nb. of faces & $h$\\
    \midrule
    esagoni8x10d & $94$ & $283$ & $0.2190$ \\
    esagoni18x20d & $389$ & $1168$ & $0.1033$ \\
    esagoni26x30d & $822$ & $2467$ & $0.0711$ \\
    esagoni34x40d & $1415$ & $4246$ & $0.0542$ \\
    esagoni44x50d & $2270$ & $6811$ & $0.0423$ \\
    esagoni52x60d & $3203$ & $9610$ & $0.0357$ \\
    esagoni60x70d & $4296$ & $12889$ & $0.0308$ \\
    esagoni70x80d & $5711$ & $17134$ & $0.0266$ \\
    \bottomrule
  \end{tabular}
  \caption{Data for the eight meshes used in the experiments.}
  \label{tab:meshdata}
\end{table}

We take the domain $\Omega$ to be the unit square $[0,1]\times[0,1]$. 
We take Dirichlet boundary conditions and load term so that
\[
u = \frac{1}{2\pi^2}\sin(\pi x)\sin(\pi y)
\]
is the exact solution. We made the following choices concerning the stabilization:
\begin{itemize}
\item $t = 1$;
\item $\alpha_K = 1$, $\forall\,K\in\Omega_h$;
\item stabilization matrices $\mathcal S$ assembled from scratch for every single element by following a standard VEM approach~\cite{beirao2014};
\end{itemize}
Tables \ref{tab:test1-k12}--\ref{tab:test1-k56} show the relative errors $e^u_0 = ||u-u_h ||_{L^2} / || u ||_{L^2}$, $e^u_1 = ||u-u_h ||_{H^1} / || u ||_{H^1}$
and the estimated convergence rates (ecr) for several values of the polynomial degree $k$ on the eight meshes in Table~\ref{tab:meshdata}.
We use $\bar m_\alpha$ as basis functions for $P_k(K)$. Similar errors and convergence rates
can be obtained using basis functions $m_\alpha$; however, in this case, for $k \geq 4$,
matrices $H$ that are used to calculate the VEM projector $\Pi^0$ are close to singular.

%% Similar convergence tests have been performed by choosing $m_\alpha$ as a basis for $P_k(K)$ and computing the VEM matrix $\hat S_{\hat K}$ representing the stabilization
%% term $s_K$ on a reference hexahedron. With this approach, for $k > 2$,
%% there is a stagnation in both the approximated errors, which remain bigger than $O(10^{-6})$, and the convergence rates,
%% which do not improve beyond $2$, approximately. The right scaling between $\hat S_{\hat K}$ and $S_K$ should be further investigated.

\begin{table}
\centering
\begin{tabular}{
S[table-format=1.2e-2]
S[table-format=1.2]
S[table-format=1.2e-2]
S[table-format=1.2]|
S[table-format=1.2e-2]
S[table-format=1.2]
S[table-format=1.2e-2]
S[table-format=1.2]
}
\toprule
\multicolumn{4}{c}{$k = 1$} & \multicolumn{4}{c}{$k = 2$} \\
\midrule
{$e^u_0$} & {ecr} & {$e^u_1$} & {ecr} & {$e^u_0$} & {ecr} & {$e^u_1$} & {ecr}\\
\midrule
   2.07e-02 &  {-}  &  1.47e-01 &  {-}  & 5.74e-03 & {-} & 1.24e-02 & {-}\\
   4.80e-03 &  1.95 &  7.08e-02 &  0.97 & 1.28e-03 & 2.00 & 2.94e-03 & 1.92\\
   2.24e-03 &  2.04 &  4.85e-02 &  1.01 & 6.10e-04 & 1.98 & 1.37e-03 & 2.05\\
   1.29e-03 &  2.03 &  3.68e-02 &  1.01 & 3.56e-04 & 1.99 & 7.88e-04 & 2.04\\
   8.01e-04 &  1.93 &  2.97e-02 &  0.97 & 2.18e-04 & 1.98 & 4.91e-04 & 1.91\\
   5.66e-04 &  2.02 &  2.44e-02 &  1.01 & 1.55e-04 & 1.98 & 3.46e-04 & 2.04\\
   4.22e-04 &  2.02 &  2.10e-02 &  1.01 & 1.16e-04 & 1.99 & 2.57e-04 & 2.04\\
   3.16e-04 &  1.95 &  1.82e-02 &  0.97 & 8.64e-05 & 2.00 & 1.94e-04 & 1.93\\
\bottomrule
\end{tabular}
\caption{Errors and estimated convergence rates (ecr) for the first test case, $k = 1,2$.}
\label{tab:test1-k12}
\end{table}

\begin{table}
\centering
\begin{tabular}{
S[table-format=1.2e-2]
S[table-format=1.2]
S[table-format=1.2e-2]
S[table-format=1.2]|
S[table-format=1.2e-2]
S[table-format=1.2]
S[table-format=1.2e-2]
S[table-format=1.2]
}
\toprule
\multicolumn{4}{c}{$k = 3$} & \multicolumn{4}{c}{$k = 4$}\\
\midrule
{$e^u_0$} & {ecr} & {$e^u_1$} & {ecr} & {$e^u_0$} & {ecr} & {$e^u_1$} & {ecr}\\
\midrule
6.98e-05 & {-}  & 7.37e-04 & {-}  & 4.35e-06 & {-}  & 3.58e-05 & {-}\\
3.83e-06 & 3.87 & 8.46e-05 & 2.88 & 2.23e-07 & 3.96 & 2.00e-06 & 3.84\\
8.35e-07 & 4.08 & 2.66e-05 & 3.09 & 4.78e-08 & 4.12 & 4.29e-07 & 4.12\\
2.78e-07 & 4.06 & 1.16e-05 & 3.07 & 1.58e-08 & 4.08 & 1.42e-07 & 4.09\\
1.07e-07 & 3.85 & 5.70e-06 & 2.86 & 6.11e-09 & 3.83 & 5.51e-08 & 3.81\\
5.34e-08 & 4.05 & 3.37e-06 & 3.07 & 3.04e-09 & 4.07 & 2.73e-08 & 4.09\\
2.96e-08 & 4.05 & 2.16e-06 & 3.06 & 1.68e-09 & 4.07 & 1.51e-08 & 4.08\\
1.67e-08 & 3.88 & 1.41e-06 & 2.88 & 9.49e-10 & 3.86 & 8.54e-09 & 3.84\\
\bottomrule
\end{tabular}
\caption{Errors and estimated convergence rates (ecr) for the first test case, $k = 3,4$.}
\label{tab:test1-k34}
\end{table}


\begin{table}
\centering
\begin{tabular}{
S[table-format=1.2e-2]
S[table-format=1.2]
S[table-format=1.2e-2]
S[table-format=1.2]|
S[table-format=1.2e-2]
S[table-format=1.2]
S[table-format=1.2e-2]
S[table-format=1.2]
}
\toprule
\multicolumn{4}{c}{$k = 5$} & \multicolumn{4}{c}{$k = 6$}\\
\midrule
{$e^u_0$} & {ecr} & {$e^u_1$} & {ecr} & {$e^u_0$} & {ecr} & {$e^u_1$} & {ecr}\\
\midrule
8.45e-08 & {-}  & 1.40e-06 & {-}  & 2.69e-09 & {-}  & 4.83e-08 & {-}\\
1.13e-09 & 5.74 & 3.82e-08 & 4.80 & 2.27e-11 & 6.36 & 6.19e-10 & 5.80\\
1.14e-10 & 6.16 & 5.57e-09 & 5.16 & 2.12e-12 & 6.35 & 6.12e-11 & 6.20\\
2.16e-11 & 6.12 & 1.39e-09 & 5.12 & 3.96e-13 & 6.20 & 1.16e-11 & 6.14\\
5.22e-12 & 5.73 & 4.26e-10 & 4.77 & 9.14e-14 & 5.91 & 2.79e-12 & 5.74\\
1.84e-12 & 6.09 & 1.77e-10 & 5.11 & 3.21e-14 & 6.10 & 9.75e-13 & 6.13\\
7.62e-13 & 6.03 & 8.42e-11 & 5.10 & 1.32e-14 & 6.09 & 3.99e-13 & 6.13\\
3.25e-13 & 5.78 & 4.14e-11 & 4.81 & 5.52e-15 & 5.91 & 1.70e-13 & 5.78\\
\bottomrule
\end{tabular}
\caption{Errors and estimated convergence rates (ecr) for the first test case, $k = 5,6$.
Results for $k = 6$ have been obtained using the ADVANPIX Multiprecision Computing Toolbox (\protect\url{https://www.advanpix.com})
with $20$ decimal digits of precision.}
\label{tab:test1-k56}
\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section*{Test case 2}

\begin{table}
  \centering
  \begin{tabular}{lcccc}
    \toprule
    Name & Nb. of elements & Nb. of faces & $h$ & $h_\textup{min}$\\
    \midrule
    \bottomrule
  \end{tabular}
  \caption{Data for the eight meshes used in the experiments.}
  \label{tab:meshdata-regularized-nested-voro}
\end{table}

We solved the same problem presented in the previous section on the six nested Voronoi meshes depicted in Figures~\ref{fig:test2-regularized-nested-Voro}. In Tables~\ref{tab:test2-part1}, \ref{tab:test2-part2} we compare the results obtained by computing the stabilization term in two different ways:
\begin{enumerate}
\item we computed the whole stiffness matrix of VEM;
\item we replaced the stabilization term of the VEM stiffness matrix with a mortar-like stabilization term.
\end{enumerate}

\begin{figure}
\centering
\subfloat{\includegraphics[width=0.5\columnwidth]{Figures/nested_regularized_01}}
\subfloat{\includegraphics[width=0.5\columnwidth]{Figures/nested_regularized_02}}\\
\subfloat{\includegraphics[width=0.5\columnwidth]{Figures/nested_regularized_03}}
\subfloat{\includegraphics[width=0.5\columnwidth]{Figures/nested_regularized_04}}\\
\subfloat{\includegraphics[width=0.5\columnwidth]{Figures/nested_regularized_05}}
\caption{Second test case: (left to right, top to bottom) in each plot, the coarser mesh is plotted in green, whereas the finer one is plotted in blue.}
\label{fig:test2-regularized-nested-Voro}
\end{figure}

\begin{table}
\centering
\begin{tabular}{
S[table-format=1.2e-2]
S[table-format=1.2]
S[table-format=1.2e-2]
S[table-format=1.2]
S[table-format=1.2e-2]
S[table-format=1.2]
S[table-format=1.2e-2]
S[table-format=1.2]
}
\toprule
\multicolumn{4}{c}{$k = 1$, whole VEM} & \multicolumn{4}{c}{$k = 1$, VEM + mortar}\\
\midrule
{$e^u_0$} & {ecr} & {$e^u_1$} & {ecr} & {$e^u_0$} & {ecr} & {$e^u_1$} & {ecr} \\
\midrule
8.58e-2 & {-}  & 3.11e-1 & {-}  & 1.28e-1 & {-}  & 3.19e-1 & {-}\\
2.40e-2 & 2.47 & 1.63e-1 & 1.25 & 3.58e-2 & 2.48 & 1.65e-1 & 1.27\\
6.45e-3 & 2.19 & 8.23e-2 & 1.14 & 9.42e-3 & 2.22 & 8.30e-2 & 1.15\\
1.58e-3 & 2.16 & 4.13e-2 & 1.06 & 2.34e-3 & 2.14 & 4.16e-2 & 1.06\\
3.72e-4 & 2.27 & 2.08e-2 & 1.08 & 5.37e-4 & 2.31 & 2.09e-2 & 1.08\\
1.04e-4 & 2.29 & 1.06e-2 & 1.21 & 1.38e-4 & 2.44 & 1.06e-2 & 1.21\\
\midrule
\multicolumn{4}{c}{k = 2, whole VEM} & \multicolumn{4}{c}{k = 2, VEM + mortar}\\
\midrule
{$e^u_0$} & {ecr} & {$e^u_1$} & {ecr} & {$e^u_0$} & {ecr} & {$e^u_1$} & {ecr} \\
\midrule
2.19e-2 & {-} & 5.72e-2 & {-}   & 2.09e-2 & {-}  & 5.75e-2 & {-}\\
6.32e-3 & 2.41 & 1.54e-2 & 2.55 & 5.42e-3 & 2.62 & 1.54e-2 & 2.56\\
1.62e-3 & 2.26 & 3.91e-3 & 2.27 & 1.35e-3 & 2.31 & 3.91e-3 & 2.27\\
4.07e-4 & 2.12 & 9.88e-4 & 2.12 & 3.38e-4 & 2.13 & 9.88e-4 & 2.11\\
8.55e-5 & 2.45 & 2.53e-4 & 2.14 & 7.06e-5 & 2.46 & 2.52e-4 & 2.14\\
1.82e-5 & 2.78 & 6.53e-5 & 2.43 & 1.50e-5 & 2.78 & 6.53e-5 & 2.43\\
\midrule
\multicolumn{4}{c}{k = 3, whole VEM} & \multicolumn{4}{c}{k = 3, VEM + mortar}\\
\midrule
{$e^u_0$} & {ecr} & {$e^u_1$} & {ecr} & {$e^u_0$} & {ecr} & {$e^u_1$} & {ecr} \\
\midrule
1.17e-3 & {-} & 7.05e-3 & {-}   & 1.13e-3 & {-}  & 7.08e-3 & {-}\\
9.62e-5 & 4.85 & 9.89e-4 & 3.81 & 8.96e-5 & 4.92 & 9.89e-4 & 3.82\\
6.71e-6 & 4.42 & 1.28e-4 & 3.40 & 6.32e-6 & 4.41 & 1.28e-4 & 3.40\\
4.12e-7 & 4.29 & 1.63e-5 & 3.16 & 3.84e-7 & 4.30 & 1.63e-5 & 3.16\\
2.85e-8 & 4.19 & 2.11e-6 & 3.21 & 2.58e-8 & 4.24 & 2.11e-6 & 3.21\\
1.92e-9 & 4.84 & 2.86e-7 & 3.59 & 1.80e-9 & 4.78 & 2.86e-7 & 3.59\\
\bottomrule
\end{tabular}
\caption{Errors and estimated convergence rates (ecr) for the second test case, $k = 1, \dots, 3$, obtained with two different stabilization strategies: (left) whole VEM stiffness matrix; (right) VEM consistency term and mortar-like stabilization term.}
\label{tab:test2-part1}
\end{table}

\begin{table}
\centering
\begin{tabular}{
S[table-format=1.2e-2]
S[table-format=1.2]
S[table-format=1.2e-2]
S[table-format=1.2]
S[table-format=1.2e-2]
S[table-format=1.2]
S[table-format=1.2e-2]
S[table-format=1.2]
}
\toprule
\multicolumn{4}{c}{k = 4, whole VEM} & \multicolumn{4}{c}{k = 4, VEM + mortar}\\
\midrule
{$e^u_0$} & {ecr} & {$e^u_1$} & {ecr} & {$e^u_0$} & {ecr} & {$e^u_1$} & {ecr} \\
\midrule
9.08e-5 & {-} & 6.60e-4 & {-}    & 8.99e-5 & {-}  & 6.58e-4 & {-}\\
5.85e-6 & 5.33 & 4.83e-5 & 5.08  & 5.25e-6 & 5.51 & 4.85e-5 & 5.06\\
3.76e-7 & 4.56 & 3.20e-6 & 4.51  & 3.26e-7 & 4.62 & 3.21e-6 & 4.51\\
2.38e-8 & 4.25 & 2.09e-7 & 4.20  & 2.04e-8 & 4.26 & 2.09e-7 & 4.20\\
1.26e-9 & 4.60 & 1.43e-8 & 4.21  & 1.09e-9 & 4.59 & 1.43e-8 & 4.21\\
6.92e-11 & 5.21 & 1.00e-9 & 4.77 & 5.95e-11 & 5.22 & 1.01e-9 & 4.76\\
\midrule
\multicolumn{4}{c}{k = 5, whole VEM} & \multicolumn{4}{c}{k = 5, VEM + mortar}\\
\midrule
{$e^u_0$} & {ecr} & {$e^u_1$} & {ecr} & {$e^u_0$} & {ecr} & {$e^u_1$} & {ecr} \\
\midrule
5.57e-6  & {-}  & 5.19e-5  & {-}  & 5.56e-6  & {-}  & 5.21e-5  & {-}\\
1.11e-7  & 7.60 & 1.96e-6  & 6.36 & 1.11e-7  & 7.60 & 1.97e-6  & 6.36\\
2.04e-9  & 6.65 & 6.72e-8  & 5.61 & 2.01e-9  & 6.67 & 6.75e-8  & 5.61\\
3.58e-11 & 6.22 & 2.28e-9  & 5.20 & 3.51e-11 & 6.22 & 2.29e-9  & 5.20\\
6.77e-13 & 6.22 & 7.79e-11 & 5.29 & 6.21e-13 & 6.33 & 7.82e-11 & 5.30\\
4.81e-14 & 4.74 & 3.00e-12 & 5.84 & 3.67e-13 & 0.95 & 3.05e-12 & 5.82\\
\bottomrule
\end{tabular}
\caption{Errors and estimated convergence rates (ecr) for the second test case, $k = 4, 5$, obtained with two different stabilization strategies: (left) whole VEM stiffness matrix; (right) VEM consistency term and mortar-like stabilization term.}
\label{tab:test2-part2}
\end{table}

%
%
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% \section*{Test case 3}

%% \begin{figure}
%% \centering
%% \subfloat{\includegraphics[width=0.5\columnwidth]{Figures/nested_regularized_01}}
%% \subfloat{\includegraphics[width=0.5\columnwidth]{Figures/nested_regularized_02}}\\
%% \subfloat{\includegraphics[width=0.5\columnwidth]{Figures/nested_regularized_03}}
%% \subfloat{\includegraphics[width=0.5\columnwidth]{Figures/nested_regularized_04}}\\
%% \caption{Third test case: (left to right, top to bottom): in each plot, the coarser mesh is plotted in green, whereas the finer one is plotted in blue.}
%% \label{fig:test3-meshes1to5}
%% \end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{thebibliography}{2}

\bibitem{beirao2014}
  L. Beir\~ao da Veiga, F. Brezzi, L.D. Marini, A. Russo,
  \emph{The hitchhiker's guide to the Virtual Element Method}.
  Math. Models Methods Appl. Sci., Volume 24, 2014.

\bibitem{bertoluzza}
  S. Bertoluzza,
  \emph{On the algebraic representation of dual scalar products}.
  in progress.

\end{thebibliography}

\end{document}
