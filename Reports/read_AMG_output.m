addpath /home/daniele/Documents/codes/petsc/share/petsc/matlab

% root = '/home/daniele/Documents/codes/vem_matlab/Runs/testsAMG/2d/lowest_order/voronoi';
% 
% reportFiles = {
%     'randVoro__Nelt_002500__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08__i_F_1__k_01__basis_1';
%     'randVoro__Nelt_005000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08__i_F_1__k_01__basis_1';
%     'randVoro__Nelt_010000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08__i_F_1__k_01__basis_1';
%     'randVoro__Nelt_020000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08__i_F_1__k_01__basis_1';
%     'randVoro__Nelt_040000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08__i_F_1__k_01__basis_1';
%     'randVoro__Nelt_080000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08__i_F_1__k_01__basis_1';
%     'randVoro__Nelt_160000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08__i_F_1__k_01__basis_1';
%     'randVoro__Nelt_320000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08__i_F_1__k_01__basis_1';
%     'randVoro__Nelt_640000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08__i_F_1__k_01__basis_1';
%     'randVoro__Nelt_01280000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08__i_F_1__k_01__basis_1';
%     };
% 
% meshnameShort = 'voro';

% root = '/home/daniele/Documents/codes/vem_matlab/Runs/testsAMG/2d/lowest_order/voronoi_aggregate_02';
% 
% reportFiles = {
%     'randVoro__Nelt_005000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off.metis_nparts_00000512__i_F_1__k_01__basis_1';
%     'randVoro__Nelt_010000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off.metis_nparts_00001024__i_F_1__k_01__basis_1';
%     'randVoro__Nelt_020000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off.metis_nparts_00002048__i_F_1__k_01__basis_1';
% 	'randVoro__Nelt_040000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off.metis_nparts_00004096__i_F_1__k_01__basis_1';
% 	'randVoro__Nelt_080000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off.metis_nparts_00008192__i_F_1__k_01__basis_1';
% 	'randVoro__Nelt_160000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off.metis_nparts_00016384__i_F_1__k_01__basis_1';
% 	'randVoro__Nelt_320000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off.metis_nparts_00032768__i_F_1__k_01__basis_1';
% 	'randVoro__Nelt_640000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off.metis_nparts_00065536__i_F_1__k_01__basis_1';
% 	'randVoro__Nelt_01280000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off.metis_nparts_00131072__i_F_1__k_01__basis_1';
%     };
% 
% meshnameShort = 'a-voro';

% root = '/home/daniele/Documents/codes/vem_matlab/Runs/testsAMG/2d/lowest_order/horse';
% 
% reportFiles = {
%     'horse_002500__i_F_1__k_01__basis_1';
%     'horse_003600__i_F_1__k_01__basis_1';
%     'horse_004900__i_F_1__k_01__basis_1';
% 	'horse_006400__i_F_1__k_01__basis_1';
% 	'horse_008100__i_F_1__k_01__basis_1';
% 	'horse_010000__i_F_1__k_01__basis_1';
% 	'horse_012100__i_F_1__k_01__basis_1';
% 	'horse_014400__i_F_1__k_01__basis_1';
% 	'horse_016900__i_F_1__k_01__basis_1';
%     'horse_019600__i_F_1__k_01__basis_1';
%     };
% 
% meshnameShort = 'horse';

% root = '/home/daniele/Documents/codes/vem_matlab/Runs/testsAMG/2d/lowest_order/horse_aggregate';
% 
% reportFiles = {
%     'horse_002500.mat.metis_nparts_00000250__i_F_1__k_01__basis_1';
%     'horse_003600.mat.metis_nparts_00000360__i_F_1__k_01__basis_1';
%     'horse_004900.mat.metis_nparts_00000490__i_F_1__k_01__basis_1';
% 	'horse_006400.mat.metis_nparts_00000640__i_F_1__k_01__basis_1';
% 	'horse_008100.mat.metis_nparts_00000810__i_F_1__k_01__basis_1';
% 	'horse_010000.mat.metis_nparts_00001000__i_F_1__k_01__basis_1';
%     };
% 
% meshnameShort = 'a-horse';

% root = '/home/daniele/Documents/codes/vem_matlab/Runs/testsAMG/2d/lowest_order/koch_01';
% 
% reportFiles = {
% 	'koch_iter_3_12x12_bndRefined_0__i_F_1__k_01__basis_1';
% 	'koch_iter_3_16x16_bndRefined_0__i_F_1__k_01__basis_1';
% 	'koch_iter_3_20x20_bndRefined_0__i_F_1__k_01__basis_1';
% 	'koch_iter_3_24x24_bndRefined_0__i_F_1__k_01__basis_1';
% 	'koch_iter_3_28x28_bndRefined_0__i_F_1__k_01__basis_1';
% 	'koch_iter_3_32x32_bndRefined_0__i_F_1__k_01__basis_1';
% 	'koch_iter_3_36x36_bndRefined_0__i_F_1__k_01__basis_1';
% 	'koch_iter_3_40x40_bndRefined_0__i_F_1__k_01__basis_1';
% 	'koch_iter_3_44x44_bndRefined_0__i_F_1__k_01__basis_1';
% 	'koch_iter_3_48x48_bndRefined_0__i_F_1__k_01__basis_1';
% 	'koch_iter_3_52x52_bndRefined_0__i_F_1__k_01__basis_1';
% 	'koch_iter_3_56x56_bndRefined_0__i_F_1__k_01__basis_1';
% 	'koch_iter_3_60x60_bndRefined_0__i_F_1__k_01__basis_1';
%     };
% 
% meshnameShort = 'koch';

% root = '/home/daniele/Documents/codes/vem_matlab/Runs/testsAMG/2d/high_order/voronoi_01';
% 
% reportFiles = {
% 	'notnested_MaxIter100_000016__i_F_1__k_%02i__basis_2';
%     };

% root = '/home/daniele/Documents/codes/vem_matlab/Runs/testsAMG/2d/high_order/horse';
% 
% reportFiles = {
% 	'horse_000016__i_F_1__k_%02i__basis_2';
%     };

% root = '/home/daniele/Documents/codes/vem_matlab/Runs/testsAMG/2d/high_order/koch';
% 
% reportFiles = {
% 	'koch_iter_3_04x04_bndRefined_0__i_F_1__k_%02i__basis_2';
%     };

% root = '/home/daniele/Documents/codes/vem_matlab/Runs/testsAMG/2d/FEMvsVEM';
% stiffnessMatrixRoot = '/home/daniele/Documents/codes/triangle/meshes/conforming_delaunay';
% 
% reportFiles = {
%     'unitsquare_a1em7__i_F_1__k_01__basis_1';
%     'unitsquare_a1em7.off.metis_nparts_00000064__i_F_1__k_01__basis_1';
%     'unitsquare_a1em7.off.metis_nparts_00000128__i_F_1__k_01__basis_1';
%     'unitsquare_a1em7.off.metis_nparts_00000256__i_F_1__k_01__basis_1';
%     'unitsquare_a1em7.off.metis_nparts_00000512__i_F_1__k_01__basis_1';
%     'unitsquare_a1em7.off.metis_nparts_00001024__i_F_1__k_01__basis_1';
%     'unitsquare_a1em7.off.metis_nparts_00002048__i_F_1__k_01__basis_1';
%     'unitsquare_a1em7.off.metis_nparts_00004096__i_F_1__k_01__basis_1';
%     'unitsquare_a1em7.off.metis_nparts_00008192__i_F_1__k_01__basis_1';
%     'unitsquare_a1em7.off.metis_nparts_00016384__i_F_1__k_01__basis_1';
%     'unitsquare_a1em7.off.metis_nparts_00032768__i_F_1__k_01__basis_1';
%     'unitsquare_a1em7.off.metis_nparts_00065536__i_F_1__k_01__basis_1';
%     'unitsquare_a1em7.off.metis_nparts_00131072__i_F_1__k_01__basis_1';
%     'unitsquare_a1em7.off.metis_nparts_00262144__i_F_1__k_01__basis_1';
%     'unitsquare_a1em7.off.metis_nparts_00524288__i_F_1__k_01__basis_1';
%     'unitsquare_a1em7.off.metis_nparts_01048576__i_F_1__k_01__basis_1';
%     'unitsquare_a1em7.off.metis_nparts_02097152__i_F_1__k_01__basis_1';
%     };
% 
% meshnameShort = 'a-tri';

% root = '/home/daniele/Documents/codes/vem_matlab/Runs/testsAMG/2d/lowest_order_random_data';
% 
% reportFiles = {
%     'randVoro__Nelt_010000__hmin_3e-05__minArea_1e-15__tolTotArea_1e-8__cut%02ix%02i__i_rho_%i__i_F_2__k_01__basis_1';
%     'randVoro__Nelt_020000__hmin_2e-05__minArea_1e-15__tolTotArea_1e-8__cut%02ix%02i__i_rho_%i__i_F_2__k_01__basis_1';
%     'randVoro__Nelt_040000__hmin_2e-05__minArea_1e-15__tolTotArea_1e-6__cut%02ix%02i__i_rho_%i__i_F_2__k_01__basis_1';
%     'randVoro__Nelt_080000__hmin_2e-05__minArea_1e-15__tolTotArea_1e-6__cut%02ix%02i__i_rho_%i__i_F_2__k_01__basis_1';
%     'randVoro__Nelt_160000__hmin_2e-05__minArea_1e-07__tolTotArea_1e-7__cut%02ix%02i__i_rho_%i__i_F_2__k_01__basis_1';
%     };

% root = '/home/daniele/Documents/codes/vem_matlab/Runs/testsAMG/2d/lowest_order/hexagons';
% 
% reportFiles = {
%     'esagoni0100x0100__i_rho_1__i_F_1__k_01__basis_1';
%     'esagoni0200x0200__i_rho_1__i_F_1__k_01__basis_1';
%     'esagoni0300x0300__i_rho_1__i_F_1__k_01__basis_1';
%     'esagoni0400x0400__i_rho_1__i_F_1__k_01__basis_1';
%     'esagoni0500x0500__i_rho_1__i_F_1__k_01__basis_1';
%     'esagoni0600x0600__i_rho_1__i_F_1__k_01__basis_1';
%     'esagoni0700x0700__i_rho_1__i_F_1__k_01__basis_1';
%     'esagoni0800x0800__i_rho_1__i_F_1__k_01__basis_1';
%     'esagoni0900x0900__i_rho_1__i_F_1__k_01__basis_1';
%     'esagoni1000x1000__i_rho_1__i_F_1__k_01__basis_1';
%     };
% 
% meshnameShort = 'hexa';

% root = '/home/daniele/Documents/codes/vem_matlab/Runs/testsAMG/2d/lowest_order_random_data/hexagons_metis';
% 
% reportFiles = {
%     'esagoni0200x0200__i_rho_3__i_F_2__k_01__basis_1__L_%i';
%     'esagoni0400x0400__i_rho_3__i_F_2__k_01__basis_1__L_%i';
%     'esagoni0600x0600__i_rho_3__i_F_2__k_01__basis_1__L_%i';
%     'esagoni0800x0800__i_rho_3__i_F_2__k_01__basis_1__L_%i';
%     'esagoni1000x1000__i_rho_3__i_F_2__k_01__basis_1__L_%i';
%     };
% 
% meshnameShort = 'hexa-metis';

root = '/home/daniele/Documents/codes/vem_matlab/Runs/testsAMG/2d/lowest_order_random_data/voronoi_metis';

reportFiles = {
    'randVoro__Nelt_010000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08__i_rho_3__i_F_2__k_01__basis_1__L_%i';
    'randVoro__Nelt_020000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08__i_rho_3__i_F_2__k_01__basis_1__L_%i';
    'randVoro__Nelt_040000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08__i_rho_3__i_F_2__k_01__basis_1__L_%i';
    'randVoro__Nelt_080000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08__i_rho_3__i_F_2__k_01__basis_1__L_%i';
    'randVoro__Nelt_160000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08__i_rho_3__i_F_2__k_01__basis_1__L_%i';
    'randVoro__Nelt_320000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08__i_rho_3__i_F_2__k_01__basis_1__L_%i';
    'randVoro__Nelt_640000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08__i_rho_3__i_F_2__k_01__basis_1__L_%i';
    'randVoro__Nelt_01280000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08__i_rho_3__i_F_2__k_01__basis_1__L_%i';
    };

meshnameShort = 'voro-metis';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

kvalues = 1;

test_type = 'jumps'; % options: 'lowest', 'high', 'FEMvsVEM', 'jumps'

NxVect   = [64 128 256 512 1024];
NyVect   = [1 1 1 1 1];
irhoVect = 3;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

direct_solvers = {
    'superlu';
    'umfpack';
    'mumps_np_2';
    'superlu_dist_np_2';
    'pastix_np_2';
    };

iterative_solvers = {
%     'cg_custom_0_np_2';
    'gamg_classical_custom_0_np_2';
    'gamg_agg_custom_0_np_2';
    'boomeramg_custom_0_np_2';
    'ml_custom_0_np_2';
    };

numFilesPerSolver   = length(reportFiles);
numIterativeSolvers = length(iterative_solvers);
numDirectSolvers    = length(direct_solvers);

fprintf('\n');

if strcmp(test_type, 'lowest')
    dofs           = zeros(numFilesPerSolver,1);
    reference      = zeros(numFilesPerSolver,1);
    kappa          = zeros(numFilesPerSolver, numIterativeSolvers);
    timeSetupSolve = zeros(numFilesPerSolver, numIterativeSolvers);
    iterations     = zeros(numFilesPerSolver, numIterativeSolvers);

    % Condition number estimates for iterative methods
    fprintf('\n\nCondition number estimates for iterative methods\n\n');
    for i = 1:numFilesPerSolver
        fprintf('%s$_{%i}$', meshnameShort, i);
        for j = 1:numIterativeSolvers
            filename = [reportFiles{i} '_' iterative_solvers{j} '.out'];
            if j == 4 % BoomerAMG
                [ dofs(i), kappa(i,j), timeSetupSolve(i,j), iterations(i,j), reference(i) ] = ...
                    read_iterative_solver_output([root '/' filename]);
            end
            [ ~, kappa(i,j), timeSetupSolve(i,j), iterations(i,j) ] = ...
                read_iterative_solver_output([root '/' filename]);
        end
        for j = 1:numIterativeSolvers
            if j == 1
                fprintf('   &   %e', reference(i));
                if isnan(kappa(i,j))
                    fprintf('   &   {-}');
                else
                    fprintf('   &   %e', kappa(i,j));
                end
            else
                if isnan(kappa(i,j))
                    fprintf('   &   {-}');
                else
                    fprintf('   &   %f', kappa(i,j));
                end
            end
        end
        fprintf('\\\\\n');
    end

    % Print setup+solving time for plotting
    fprintf('\n\nPrint setup + solving time of iterative solvers\n');
    for j = 1:numIterativeSolvers
        fprintf('\n%s\n', iterative_solvers{j});
        for i = 1:numFilesPerSolver
            fprintf('(%i, %g)\n', dofs(i), timeSetupSolve(i,j));
        end
    end

    % Print iterations for plotting
    fprintf('\n\nPrint iterations count for iterative solvers\n');
    for j = 1:numIterativeSolvers
        fprintf('\n%s\n', iterative_solvers{j});
        for i = 1:numFilesPerSolver
            fprintf('(%i, %i)\n', dofs(i), iterations(i,j));
        end
    end

    fprintf('\nReading direct solvers output ...\n');

    timeSetupSolve = zeros(numFilesPerSolver, numDirectSolvers);

    for i = 1:numFilesPerSolver
        for j = 1:numDirectSolvers
            filename = [reportFiles{i} '_' direct_solvers{j} '.out'];
            [ ~, timeSetupSolve(i,j) ] = read_direct_solver_output([root '/' filename]);
        end
    end

    fprintf('\n\nPrint setup + solving time of direct solvers\n');
    for j = 1:numDirectSolvers
        fprintf('\n%s\n', direct_solvers{j});
        for i = 1:numFilesPerSolver
            fprintf('(%i, %g)\n', dofs(i), timeSetupSolve(i,j));
        end
    end
    
elseif strcmp(test_type, 'high')
    
    %
    % High order
    %

    dofs           = zeros(length(kvalues),1);
    reference      = zeros(length(kvalues),1);
    kappa          = zeros(length(kvalues), numIterativeSolvers);
    timeSetupSolve = zeros(length(kvalues), numIterativeSolvers);
    iterations     = zeros(length(kvalues), numIterativeSolvers);

    % Condition number estimates for iterative methods
    fprintf('\n\nCondition number estimates for iterative methods\n\n');
    for k = kvalues
        fprintf('%i', k);
        for j = 1:numIterativeSolvers
            filename = [sprintf(reportFiles{1}, k) '_' iterative_solvers{j} '.out'];
            if j == 3 % GAMG agg
                [ dofs(k), kappa(k,j), timeSetupSolve(k,j), iterations(k,j), reference(k) ] = ...
                    read_iterative_solver_output([root '/' filename]);
            end
            [ ~, kappa(k,j), timeSetupSolve(k,j), iterations(k,j) ] = ...
                read_iterative_solver_output([root '/' filename]);
        end
        for j = 1:numIterativeSolvers
            if j == 1
                fprintf('   &   %i   &   %e', dofs(k), reference(k));
                if isnan(kappa(k,j))
                    fprintf('   &   {-}');
                else
                    fprintf('   &   %e', kappa(k,j));
                end
            else
                if isnan(kappa(k,j))
                    fprintf('   &   {-}');
                else
                    fprintf('   &   %e', kappa(k,j));
                end
            end
        end
        fprintf('\\\\\n');
    end

    % Print setup+solving time for plotting
    fprintf('\n\nPrint setup + solving time of iterative solvers\n');
    for j = 1:numIterativeSolvers
        fprintf('\n%s\n', iterative_solvers{j});
        for k = kvalues
            fprintf('(%i, %g)\n', k, timeSetupSolve(k,j));
        end
    end

    % Print iterations for plotting
    fprintf('\n\nPrint iterations count for iterative solvers\n');
    for j = 1:numIterativeSolvers
        fprintf('\n%s\n', iterative_solvers{j});
        for k = kvalues
            fprintf('(%i, %i)\n', k, iterations(k,j));
        end
    end

    fprintf('\nReading direct solvers output ...\n');

    timeSetupSolve = zeros(length(kvalues), numDirectSolvers);

    for k = kvalues
        for j = 1:numDirectSolvers
            filename = [sprintf(reportFiles{1}, k) '_' direct_solvers{j} '.out'];
            [ ~, timeSetupSolve(k,j) ] = read_direct_solver_output([root '/' filename]);
        end
    end

    fprintf('\n\nPrint setup + solving time of direct solvers\n');
    for j = 1:numDirectSolvers
        fprintf('\n%s\n', direct_solvers{j});
        for k = kvalues
            fprintf('(%i, %g)\n', k, timeSetupSolve(k,j));
        end
    end

elseif strcmp(test_type, 'FEMvsVEM')

    for i = 1:numFilesPerSolver
        if i == 1
            fprintf('original');
        else
            fprintf('%s$_{%i}$', meshnameShort, i-1);
        end
        S = PetscBinaryRead([stiffnessMatrixRoot '/' reportFiles{i} '__S_rhs_sol.bin']);
        fprintf('   &   %e', nnz(S)/(size(S,1)^2));
        for j = 1:numIterativeSolvers
            filename = [reportFiles{i} '_' iterative_solvers{j} '.out'];
            [ ~, kappa, timeSetupSolve, iterations, ~, errA ] = ...
                    read_iterative_solver_output([root '/' filename]);
            if isnan(kappa)
                fprintf('   &   {-}   &   {-}   &   {-}   &   {-}');
            else
                fprintf('   &   %f   &   %f   &   %i   &   %e', kappa, timeSetupSolve, iterations, errA);                
            end
        end
        fprintf('\\\\\n');
    end

elseif strcmp(test_type, 'jumps')

    % Condition number estimates for iterative methods
    fprintf('\n\nCondition number estimates for iterative methods\n\n');

    numSubdomainsX = length(NxVect);
    numRhoTypes    = length(irhoVect);

    kappa          = zeros(numFilesPerSolver, numIterativeSolvers, numSubdomainsX, numRhoTypes);
    timeSetupSolve = zeros(numFilesPerSolver, numIterativeSolvers, numSubdomainsX, numRhoTypes);
    iterations     = zeros(numFilesPerSolver, numIterativeSolvers, numSubdomainsX, numRhoTypes);

    fprintf('\nReading iterative solvers output ...\n');

    for i = 1:numFilesPerSolver
        for j = 1:numIterativeSolvers
            for iN = 1:numSubdomainsX
                for iR = 1:numRhoTypes
%                     filename = [ sprintf(reportFiles{i}, NxVect(iN), NyVect(iN), irhoVect(iR)) ...
%                                  '_' iterative_solvers{j} '.out' ];
                    filename = [ sprintf(reportFiles{i}, NxVect(iN)*NyVect(iN)) ...
                                 '_' iterative_solvers{j} '.out' ];
                    [ ~, kappa(i,j,iN,iR), timeSetupSolve(i,j,iN,iR), iterations(i,j,iN,iR) ] = ...
                        read_iterative_solver_output([root '/' filename]);
                end
            end
        end
    end

    dofs                 = zeros(numFilesPerSolver, numSubdomainsX);
    reference            = zeros(numFilesPerSolver, numSubdomainsX, numRhoTypes);
    timeSetupSolveDirect = zeros(numFilesPerSolver, numDirectSolvers, numSubdomainsX, numRhoTypes);

    for i = 1:numFilesPerSolver
        for j = 1:numDirectSolvers
            for iN = 1:numSubdomainsX
                for iR = 1:numRhoTypes
%                     filename = [ sprintf(reportFiles{i}, NxVect(iN), NyVect(iN), irhoVect(iR)) ...
%                                  '_' direct_solvers{j} '.out' ];
                    filename = [ sprintf(reportFiles{i}, NxVect(iN)*NyVect(iN)) ...
                                 '_' direct_solvers{j} '.out' ];
                    if strcmp(direct_solvers{j}, 'superlu_dist_np_2')
                        [ dofs(i,iN), timeSetupSolveDirect(i,j,iN,iR), reference(i,iN,iR) ] = read_direct_solver_output([root '/' filename]);
                    else
                        [ ~, timeSetupSolveDirect(i,j,iN,iR) ] = read_direct_solver_output([root '/' filename]);
                    end
                end
            end
        end
    end

    %
    % Residual 2-norms for stopping criteria
    %

    fprintf('\n\nResidual 2-norms for stopping criteria\n');

    for iR = 1:numRhoTypes
        fprintf('\n');
        for iN = 1:numSubdomainsX
            fprintf('%i', NxVect(iN)*NyVect(iN));
            for i = 1:numFilesPerSolver
                fprintf('   &   %e', reference(i,iN,iR));
            end
            fprintf('\\\\\n');
        end
    end

    for iR = 1:numRhoTypes

        %
        % Condition number estimates for iterative methods
        %

        fprintf('\n\nCondition number estimates for iterative methods, i_rho = %i\n', irhoVect(iR));
        for j = 1:numIterativeSolvers
            fprintf('\n%s\n', iterative_solvers{j});
            for iN = 1:numSubdomainsX
                fprintf('%i', NxVect(iN)*NyVect(iN));
                for i = 1:numFilesPerSolver
                    if isnan(kappa(i,j,iN,iR))
                        fprintf('   &   {-}');
                    else
                        fprintf('   &   %e', kappa(i,j,iN,iR));
                    end
                end
                fprintf('\\\\\n');
            end
        end

        %
        % Print setup+solving time and iteration counts for plotting
        %

        fprintf('\n\nPrint setup + solving time and iteration counts, i_rho = %i\n', irhoVect(iR));

        for iN = 1:numSubdomainsX
            for j = 1:numIterativeSolvers
                fprintf('\n%s, time, iN = %i\n', iterative_solvers{j}, iN);
                for i = 1:numFilesPerSolver
                    fprintf('(%i, %g)\n', dofs(i,iN), timeSetupSolve(i,j,iN,iR));
                end
            end
            for j = 1:numDirectSolvers
                fprintf('\n%s, time, iN = %i\n', direct_solvers{j}, iN);
                for i = 1:numFilesPerSolver
                    fprintf('(%i, %g)\n', dofs(i,iN), timeSetupSolveDirect(i,j,iN,iR));
                end
            end
            for j = 1:numIterativeSolvers
                fprintf('\n%s, iteration count, iN = %i\n', iterative_solvers{j}, iN);
                for i = 1:numFilesPerSolver
                    fprintf('(%i, %i)\n', dofs(i,iN), iterations(i,j,iN,iR));
                end
            end
        end
    end
end
