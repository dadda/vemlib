addpath /home/daniele/Downloads/altmany-export_fig-5be2ca4

meshroot = '../Mesh_Handlers/meshfiles2d/voronoi-nested/series03/';

mesh1 = load([meshroot 'regularized_MaxIter10_001024']);
mesh2 = load([meshroot 'regularized_MaxIter10_004096']);

figure()
axis equal; hold on;

% Plot initial mesh
for K = 1:size(mesh1.mesh.Element,1)
    plot(mesh1.mesh.Node([mesh1.mesh.Element{K} mesh1.mesh.Element{K}(1)],1), ...
         mesh1.mesh.Node([mesh1.mesh.Element{K} mesh1.mesh.Element{K}(1)],2), 'g', 'Linewidth', 2)    
end

% Overlay finer mesh
for K = 1:size(mesh2.mesh.Element,1)
    plot(mesh2.mesh.Node([mesh2.mesh.Element{K} mesh2.mesh.Element{K}(1)],1), ...
         mesh2.mesh.Node([mesh2.mesh.Element{K} mesh2.mesh.Element{K}(1)],2), 'b');
end

set(gca, 'Color', 'None')
export_fig nested_regularized_04.pdf
