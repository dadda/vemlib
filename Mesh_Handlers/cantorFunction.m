function [ y, xepts, yepts ] = cantorFunction(x, a, b, fa, fb, currentLevel, maxLevel)

  if maxLevel == 0
    y     = x;
    xepts = [a; b];
    yepts = [0; 1];
  else
    currentLevel = currentLevel + 1;
    hx = (b - a)/3.;
    hy = (fb + fa)/2.;
    xC = x( ( x >= (a+hx) ) & ( x < a+2*hx) );
    yC = hy * ones(size(xC));

    xeptsC = [a+hx; a+2*hx];
    yeptsC = [hy; hy];

    xL = x( x < (a+hx) );
    xR = x( x >= (a+2*hx) );

    if currentLevel == maxLevel
      mL = (hy - fa)/hx; qL = fa - mL*a;
      yL = mL * xL + qL;

      mR = (fb - hy)/hx; qR = fb - mR*b;
      yR = mR * xR + qR;

      xeptsL = []; yeptsL = [];
      xeptsR = []; yeptsR = [];
    else
      [ yL, xeptsL, yeptsL ] = cantorFunction(xL, a, a+hx, fa, hy, currentLevel, maxLevel);
      [ yR, xeptsR, yeptsR ] = cantorFunction(xR, a+2*hx, b, hy, fb, currentLevel, maxLevel);
    end
    y     = [yL; yC; yR];
    xepts = [xeptsL; xeptsC; xeptsR];
    yepts = [yeptsL; yeptsC; yeptsR];
  end
end
