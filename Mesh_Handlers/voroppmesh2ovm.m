function mesh = voroppmesh2ovm( basename, tolerance )
% Read file generated with Voro++ and return a mesh in OVM format

% Read number of elements
fid = fopen([basename '.txt']);

tmpMesh.Nelt = str2double(fgetl(fid));

% Buffers
lines   = cell(tmpMesh.Nelt,1);
cellnum = zeros(tmpMesh.Nelt,1);

% (Over)estimate the number of vertices, faces, and edges
Nver     = 0;
Nfc      = 0;
Ned      = 0;
max_Ned  = 0;
for E = 1:tmpMesh.Nelt
    lines{E} = strsplit( fgetl(fid) );
    cellnum(E) = str2double(lines{E}{1});
    
    Nver_loc = str2double(lines{E}{2});
    Nfc_loc  = str2double(lines{E}{2 + Nver_loc + 1});
    
    Nver = Nver + Nver_loc;
    Nfc  = Nfc + Nfc_loc;

    % What follows is a trick to sum the order of each face.
    Ned_loc = ...
        length(strsplit(cell2mat(lines{E}(2 + Nver_loc + 1 + Nfc_loc +1:end)),',')) + Nfc_loc - 1;
    Ned     = Ned + Ned_loc;
    max_Ned = max(max_Ned, Ned_loc);
end

fclose(fid);

[~, sortedcell] = sort(cellnum);

% (Over)allocate data structures
vertices      = zeros(Nver, 3);
edge2vertices = zeros(Ned, 2);
faces         = cell(Nfc,1);
face2elements = -ones(Nfc,2);

tmpMesh.element2hfaces = cell(tmpMesh.Nelt,1);
neighbors = cell(tmpMesh.Nelt,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Step 1: make a first scan of the voro++ file to construct global data
%         structures.

% Count the number of edges
iNed = 0;

%% Read first cell

% Vertices
Nver_loc = str2double(lines{sortedcell(1)}{2});
for v = 1:Nver_loc
    vertices(v, :) = str2num( lines{sortedcell(1)}{2+v}(2:end-1) ); %#ok<*ST2NM>
end

% Faces, edges, and element2faces, face2elements
Nfc_loc  = str2double(lines{sortedcell(1)}{2 + Nver_loc + 1});
tmpMesh.element2hfaces{1} = zeros(1, Nfc_loc);

% Remark: +1 removed from neighbors when generating meshes with tetgen
neighbors{1} = str2num( strjoin( lines{sortedcell(1)}(2 + Nver_loc + 1 + 2*Nfc_loc + 1:end),',' ) ) + 1; %+1

% Compute barycenter
barycenter_el = sum(vertices,1)/Nver_loc;

for f = 1:Nfc_loc
    Ned_f = str2double(lines{sortedcell(1)}{2 + Nver_loc + 1 + f});
    face  = str2num( lines{sortedcell(1)}{2 + Nver_loc + 1 + Nfc_loc + f}(2:end-1) ) + 1;
    
    % Faces
    faces{f} = [ Ned_f face ];
    
    % Edges
    edge2vertices(iNed+1:iNed+Ned_f, :) = [face' [face(2:end) face(1)]'];
    iNed = iNed + Ned_f;
    
    % element2faces
    %
    % Remark: voronoi cells are obtained by intersecting half-spaces, thus
    % they will always be convex.
        
	v1 = [(vertices(face(2:end-1),1)-vertices(face(1),1)) ...
          (vertices(face(2:end-1),2)-vertices(face(1),2)) ...
          (vertices(face(2:end-1),3)-vertices(face(1),3))];
	v2 = [(vertices(face(3:end),1)-vertices(face(1),1)) ...
          (vertices(face(3:end),2)-vertices(face(1),2)) ...
          (vertices(face(3:end),3)-vertices(face(1),3))];
      
    normal = sum([v1(:,2).*v2(:,3) - v1(:,3).*v2(:,2), ...
                  v1(:,3).*v2(:,1) - v1(:,1).*v2(:,3), ...
                  v1(:,1).*v2(:,2) - v1(:,2).*v2(:,1)],1);
    normal = normal / norm(normal);
    
    % Check on which size the barycenter is to find the correct half-face
    check_test = dot(normal, barycenter_el - vertices(face(1),:));
    if check_test > 0
        % The face is clockwise-oriented.
        tmpMesh.element2hfaces{1}(f) = 2*f;
    else        
        % The face is counter-clockwise oriented.
        tmpMesh.element2hfaces{1}(f) = 2*f-1;
    end
    
    % Mind the C-language numbering
    if neighbors{1}(f) <= 0
        face2elements(f,1) = 1;
    else
        face2elements(f,:) = [1 neighbors{1}(f)];
    end
end

iNver = Nver_loc;
iNfc  = Nfc_loc;

%% Iterate over the remaining cells

for E = 2:tmpMesh.Nelt
    
    old_Nver_loc = str2double(lines{sortedcell(E)}{2});
    old_Nfc_loc  = str2double(lines{sortedcell(E)}{2 + old_Nver_loc + 1});

    % Allocate a cell for loading the local (pre-processed) faces
    initial_faces = cell(old_Nfc_loc, 1);
    
    % Read local faces
    for f = 1:old_Nfc_loc
        % Read face vertices (wrt their local numbering)
        initial_faces{f} = str2num( lines{sortedcell(E)}{2 + old_Nver_loc + 1 + old_Nfc_loc + f}(2:end-1) ) + 1;
    end
        
    % Read neighboring cells - mind the C-numbering
    
    % Remark: +1 removed from neighbors when generating meshes with tetgen
    neighbors{E} = str2num( strjoin( lines{sortedcell(E)}(2 + old_Nver_loc + 1 + 2*old_Nfc_loc + 1:end),',' ) ) + 1; %+1;
    
    % Find faces that are shared with other elements
    shared_faces = find(neighbors{E} < E & neighbors{E} > 0);
    new_faces    = setdiff(1:old_Nfc_loc, shared_faces);
    
    % Update Nfc_loc
    nb_new_faces = length(new_faces);

    vertices_to_remove = horzcat(initial_faces{shared_faces});
    vertices_to_keep   = setdiff(1:old_Nver_loc, vertices_to_remove);
    nb_new_vertices    = length(vertices_to_keep);

    % Create vertices and faces maps
    vertex_map = zeros(1, old_Nver_loc);
    vertex_map(vertices_to_keep) = iNver + (1:nb_new_vertices);
    
    tmpMesh.element2hfaces{E} = zeros(1, old_Nfc_loc);

    for sf = shared_faces
        flocal = find( neighbors{ neighbors{E}(sf) } == E );
        if isempty(flocal)
            error('Neighboring information not symmetric! Aborting ...')
        else
            % Read the half face associated to the shared face in the
            % neighboring element
            half_face = tmpMesh.element2hfaces{neighbors{E}(sf)}(flocal);

            if mod(half_face,2) == 0
                % The face is clockwise ordered for the neighboring element
                % so we have to assign the previous odd half_face to the same face
                % for the current element
                tmpMesh.element2hfaces{E}(sf)    = half_face-1;
                
                first_vertex = str2num( lines{sortedcell(E)}{2 + initial_faces{sf}(1)}(2:end-1) ); %#ok<*ST2NM>
                [~, map_first] = min( pdist2(first_vertex, vertices(faces{half_face/2}(2:end),:)) );             
                reversed_iterator = [map_first+1:-1:2 length(initial_faces{sf})+1:-1:map_first+2];
                vertex_map(initial_faces{sf}) = faces{half_face/2}(reversed_iterator);
            else
                % The face is counter clockwise ordered for the neighboring
                % element so we have to assign the next even half_face to
                % the same face for the current element
                tmpMesh.element2hfaces{E}(sf)    = half_face+1;

                first_vertex = str2num( lines{sortedcell(E)}{2 + initial_faces{sf}(1)}(2:end-1) );
                [~, map_first] = min( pdist2(first_vertex, vertices(faces{(half_face+1)/2}(2:end),:)) );
                reversed_iterator = [map_first+1:-1:2 length(initial_faces{sf})+1:-1:map_first+2];
                vertex_map(initial_faces{sf}) = faces{(half_face+1)/2}(reversed_iterator);
            end
        end
    end

    % Load only new vertices
    for v = 1:nb_new_vertices
        vertices(iNver + v, :) = str2num( lines{sortedcell(E)}{2+vertices_to_keep(v)}(2:end-1) );
    end

    % Compute barycenter
    barycenter_el = sum(vertices(vertex_map,:),1)/old_Nver_loc;
    
    for f = 1:nb_new_faces
        % Load only new faces and corresponding udapted node numberings
        Ned_f = length(initial_faces{new_faces(f)});
        face = vertex_map( initial_faces{new_faces(f)} );
        
        faces{iNfc + f} = [Ned_f face];
        
        % Edges
        edge2vertices(iNed+1:iNed+Ned_f, :) = [face; face([2:Ned_f 1])]';
        iNed = iNed + Ned_f;

        % element2faces
        v1 = [(vertices(face(2:end-1),1)-vertices(face(1),1)) ...
              (vertices(face(2:end-1),2)-vertices(face(1),2)) ...
              (vertices(face(2:end-1),3)-vertices(face(1),3))];
        v2 = [(vertices(face(3:end),1)-vertices(face(1),1)) ...
              (vertices(face(3:end),2)-vertices(face(1),2)) ...
              (vertices(face(3:end),3)-vertices(face(1),3))];
        normal = sum([v1(:,2).*v2(:,3) - v1(:,3).*v2(:,2), ...
                      v1(:,3).*v2(:,1) - v1(:,1).*v2(:,3), ...
                      v1(:,1).*v2(:,2) - v1(:,2).*v2(:,1)],1);
        normal = normal / norm(normal);
        
        % Check on which size the barycenter is to find the correct half-face
        check_test = dot(normal, barycenter_el - vertices(face(1),:));
        if check_test > 0
            % The face is clockwise-oriented.
            tmpMesh.element2hfaces{E}(new_faces(f)) = 2*(iNfc + f);
        else        
            % The face is counter-clockwise oriented.
            tmpMesh.element2hfaces{E}(new_faces(f)) = 2*(iNfc + f)-1;
        end
        
        if neighbors{E}(new_faces(f)) <= 0
            face2elements(iNfc + f,1) = E;
        else
            face2elements(iNfc + f,:) = [E neighbors{E}(new_faces(f))];
        end
    end

    iNver = iNver + nb_new_vertices;
    iNfc  = iNfc + nb_new_faces;
end

% Select only nonzero entries

tmpMesh.Nver = iNver;
tmpMesh.Nfc  = iNfc;

tmpMesh.coordinates = vertices(1:tmpMesh.Nver,:);
tmpMesh.face2elements = face2elements(1:tmpMesh.Nfc,:);

tmpMesh.edge2vertices = unique( sort( edge2vertices(1:iNed,:), 2 ), 'rows' );
tmpMesh.Ned = size(tmpMesh.edge2vertices, 1);

% Create face2edges
face2edges = zeros(tmpMesh.Nfc*max_Ned,2);
faces      = faces(1:tmpMesh.Nfc);
counter = 0;
for fc = 1:tmpMesh.Nfc
    iterfc = [2:faces{fc}(1) 1];
    %tmpMesh.face2hedges{fc}(1) = faces{fc}(1);
    for i = 1:faces{fc}(1)
        counter = counter + 1;
        face2edges(counter, :) = faces{fc}([1+i 1+iterfc(i)]);
    end
end
face2edges = face2edges(1:counter,:);

% Step 1: sort edges to compare them with edge2vertices
[face2edges, I] = sort(face2edges, 2);
I = mod(I(:,1),2);   % Now I can be used to choose between odd (I(i) = 1)
                     % and even (I(i) = 0) hald edge.

% Step 2: take unique edges not to have troubles with intersect
[tmp, ~, jx1] = unique(face2edges, 'rows');

% Step 3: intersect with edge2vertices
[~, ~, jx2] = intersect(tmp, tmpMesh.edge2vertices, 'rows');

counter = 0;
tmpMesh.face2hedges = cell(tmpMesh.Nfc, 1);
for fc = 1:tmpMesh.Nfc
    tmpMesh.face2hedges{fc} = zeros(1, faces{fc}(1));
    for i = 1:faces{fc}(1)
        counter = counter + 1;
        tmpMesh.face2hedges{fc}(i) = 2*jx2(jx1(counter)) - I(counter);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Step 2: remove coincident vertices and construct OVM mesh data structure
%         using global data structures previously constructed

mesh.coordinates = tmpMesh.coordinates;

% Check for coincident vertices

disp('Checking for vertices closer than given tolerance...')

% Remark: preallocation would cause "Out of memory error" on big meshes
to_be_eliminated = [];
to_be_kept       = [];

for i = 1:tmpMesh.Nver
    distances = pdist2(mesh.coordinates(i,:), mesh.coordinates(i+1:end,:));
    tmp = find(distances < tolerance) + i;
    to_be_eliminated = [to_be_eliminated tmp]; %#ok<AGROW>
    to_be_kept       = [to_be_kept zeros(size(tmp))+i]; %#ok<AGROW>
end

if ~isempty(to_be_eliminated)
    warning('Removing vertices closer than given tolerance...')
end

[to_be_eliminated, ix] = unique(to_be_eliminated);
to_be_kept  = to_be_kept(ix);
% [~, IA, IB] = intersect(to_be_kept, to_be_eliminated);
% to_be_kept(IA) = to_be_kept(IB);
niterations = 1;
while intersect(to_be_eliminated, to_be_kept)
    fprintf('Number of recursions (we expect to perform one iteration here): %d\n', niterations)
    for i = 1:length(to_be_eliminated)
        to_be_kept(to_be_eliminated(i) == to_be_kept) = to_be_kept(i);
    end
    niterations = niterations + 1;
end

% % Construct clusters

% % Use this call for coarser mesh - as the mesh gets really fine, use the
% % 'savememory' version

% T = clusterdata(mesh.coordinates, 'distance', 'euclidean', ...
%                                   'linkage', 'single', ...
%                                   'cutoff', tolerance, ...
%                                   'criterion', 'distance' );
% % T = clusterdata(mesh.coordinates, 'distance', 'euclidean', ...
% %                                   'linkage', 'median', ...
% %                                   'savememory', 'on', ...
% %                                   'cutoff', tolerance, ...
% %                                   'criterion', 'distance' );
%
% numClusters = max(T);
% if numClusters ~= tmpMesh.Nver
%     warning('Removing %d vertices...', tmpMesh.Nver-numClusters)
% end
%
% to_be_kept       = zeros(tmpMesh.Nver,1);
% to_be_eliminated = zeros(tmpMesh.Nver,1);
% is_cluster_busy  = zeros(numClusters,1);
%
% counterE = 0;
% counterK = 0;
% for i = 1:tmpMesh.Nver
%     myCluster = T(i);
%     if is_cluster_busy(myCluster) == 0
%         counterK = counterK + 1;
%         to_be_kept(counterK) = i;
%         is_cluster_busy(myCluster) = 1;
%     else
%         counterE = counterE + 1;
%         to_be_eliminated(counterE) = i;
%     end
% end
%
% to_be_kept = to_be_kept(1:counterK);
% to_be_eliminated = to_be_eliminated(1:counterE);

mesh.coordinates(to_be_eliminated, :) = [];
mesh.Nver = size(mesh.coordinates, 1);

faces = cell(tmpMesh.Nfc, 1);
mesh.face2elements = -ones(tmpMesh.Nfc, 2);

mesh.Nelt = tmpMesh.Nelt; % We are assuming no element collapses
mesh.element2hfaces = cell(mesh.Nelt,1);

%% Load the first element
Ned = 0;
fc  = 1;
Nfc_loc = length(tmpMesh.element2hfaces{1});
element = construct_element_connectivity(tmpMesh, 1);
mesh.element2hfaces{1} = zeros(1, Nfc_loc);
for f = 1:Nfc_loc
    face_order   = length(element{f});
    faces{fc}    = zeros(1, face_order+1);
    faces{fc}(1) = face_order;
    previousnode = [face_order 1:face_order-1];
    nextnode     = [2:face_order 1];
    for i = 1:face_order
        node  = element{f}(i);
        inode = find(node == to_be_eliminated);
        if inode
            if ( element{f}(previousnode(i)) == to_be_kept(inode) ) || ...
               ( element{f}(nextnode(i)) == to_be_kept(inode) )
                faces{fc}(1)   = faces{fc}(1)-1;
            else
                faces{fc}(i+1) = to_be_kept(inode) - sum(to_be_kept(inode) > to_be_eliminated);
            end
        else
            faces{fc}(i+1) = node - sum(node > to_be_eliminated);            
        end
    end
    
    faces{fc}(faces{fc} == 0) = [];
    if length(faces{fc}) > 3
        % Since we are in the first element, we assign odd half face
        % without checking the orientation of the normal
        mesh.element2hfaces{1}(fc) = 2*fc-1;
        mesh.face2elements(fc,1) = 1;
        fc = fc + 1;
    else
        neighbors{1}(fc) = [];
        if length(neighbors{1}) < 4
            error('First element degenerates')
        end
    end

    Ned = Ned + face_order;
end

mesh.element2hfaces{1}(fc:end) = [];

%% Iterate over the remaining cells

for E = 2:mesh.Nelt
    Nfc_loc = length(tmpMesh.element2hfaces{E});
    element = construct_element_connectivity(tmpMesh, E);
    mesh.element2hfaces{E} = zeros(1, Nfc_loc);
    fc2 = 1;
    for f = 1:Nfc_loc
        face_order = length(element{f});

        % Topological face index in the old mesh
        tface = floor( ( tmpMesh.element2hfaces{E}(f)+1 )/2 );
        Elow  = tmpMesh.face2elements(tface, tmpMesh.face2elements(tface,:) < E & ...
                                          tmpMesh.face2elements(tface,:) > 0);
        if Elow
            % element{f} has already been processed. We need to find the
            % proper EVEN half face to assign to it. faces should not be
            % updated (neither should fc).
            
            % Let us look for the half faces of Elow in the new mesh
            tfaces1 = floor( ( mesh.element2hfaces{Elow}+1 )/2 );
            hface = 2*tfaces1(neighbors{Elow} == E);
            
            % Check whether this face has been removed from Elow
            if ~isempty(hface)
                % If this face has not been removed from Elow
                mesh.element2hfaces{E}(fc2) = hface;
                mesh.face2elements(hface/2,2) = E;
                fc2 = fc2 + 1;
                
                % Extra consistency check
                if (mod(mesh.element2hfaces{Elow}(neighbors{Elow} == E),2) == 0)
                    error('Something is wrong with half faces...')
                end
            else
                neighbors{E}(fc2) = [];
            end
        else
            % element{f} has not been processed yet. Check whether it
            % should be added as done in vorocell2ovm. If yes increment
            % both fc and fc2
            faces{fc} = zeros(1, face_order+1);
            faces{fc}(1) = face_order;
            previousnode = [face_order 1:face_order-1];
            nextnode     = [2:face_order 1];
            for i = 1:face_order
                node = element{f}(i);
                inode = find(node == to_be_eliminated);
                if inode
                    if (E == 1355)
                       1; 
                    end
                    if ( element{f}(previousnode(i)) == to_be_kept(inode) ) || ...
                       ( element{f}(nextnode(i)) == to_be_kept(inode) )
                        faces{fc}(1)   = faces{fc}(1)-1;
                    else
                        faces{fc}(i+1) = to_be_kept(inode) - sum(to_be_kept(inode) > to_be_eliminated);
                    end
                else
                    faces{fc}(i+1) = node - sum(node > to_be_eliminated);            
                end
            end
            
            faces{fc}(faces{fc} == 0) = [];
            if length(faces{fc}) > 3
                % Since the current face was not processed yet, we assign
                % an odd half face to it (note: fc2 refers to local numbering,
                % whereas fc to global).
                mesh.element2hfaces{E}(fc2) = 2*fc-1;
                mesh.face2elements(fc,1) = E;
                fc2 = fc2 + 1;
                fc = fc + 1;
            else
                neighbors{E}(fc2) = [];
                if length(neighbors{E}) < 4
                    error('Element degenerates...')
                end
            end
        end
        Ned = Ned + face_order;
    end
    mesh.element2hfaces{E}(fc2:end) = [];
end

mesh.Nfc = fc - 1;
mesh.face2elements = mesh.face2elements(1:mesh.Nfc,:);
faces     = faces(1:mesh.Nfc);

% Overallocate edges
edge2vertices = zeros(Ned,2);
counter = 0;
for f = 1:mesh.Nfc
    iterfc = [2:faces{f}(1) 1];
    for i = 1:faces{f}(1)
        counter = counter + 1;
        edge2vertices(counter, :) = faces{f}([1+i 1+iterfc(i)]);
    end
end
mesh.edge2vertices = unique(sort(edge2vertices(1:counter,:), 2), 'rows');
mesh.Ned = size(mesh.edge2vertices, 1);

% Create face2edges
face2edges = zeros(mesh.Nfc*max_Ned,2);
counter = 0;
for f = 1:mesh.Nfc
    iterfc = [2:faces{f}(1) 1];
    for i = 1:faces{f}(1)
        counter = counter + 1;
        face2edges(counter, :) = faces{f}([1+i 1+iterfc(i)]);
    end
end
face2edges = face2edges(1:counter,:);

% Step 1: sort edges to compare them with edge2vertices
[face2edges, I] = sort(face2edges, 2);
I = mod(I(:,1),2);   % Now I can be used to choose between odd (I(i) = 1)
                     % and even (I(i) = 0) hald edge.

% Step 2: take unique edges not to have troubles with intersect
[tmp, ~, jx1] = unique(face2edges, 'rows');

% Step 3: intersect with edge2vertices
[~, ~, jx2] = intersect(tmp, mesh.edge2vertices, 'rows');

counter = 0;
mesh.face2hedges = cell(mesh.Nfc, 1);
for f = 1:mesh.Nfc
    mesh.face2hedges{f} = zeros(1, faces{f}(1));
    for i = 1:faces{f}(1)
        counter = counter + 1;
        mesh.face2hedges{f}(i) = 2*jx2(jx1(counter)) - I(counter);
    end
end

save([basename '.mat'], 'mesh')

end
