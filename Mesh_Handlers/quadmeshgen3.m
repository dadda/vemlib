function mesh = quadmeshgen3(ax, bx, Nx, ay, by, Ny)

hx = (bx-ax) / Nx;
hy = (by-ay) / Ny;

counter = 0;
for j = 1:Ny+1
    for i = 1:Nx+1
        counter = counter + 1;
        P(counter,1) = ax + (i-1)*hx;
        P(counter,2) = ay + (j-1)*hy;
    end
end

NN      = Nx * Ny;
T       = zeros(NN,4);
counter = 0;
for j = 1:Ny
    for i = 1:Nx
        counter = counter + 1;
        T(counter, 1) = (j-1)*(Nx+1) + i;
        T(counter, 2) = (j-1)*(Nx+1) + i + 1;
        T(counter, 3) = j*(Nx+1) + i + 1;
        T(counter, 4) = j*(Nx+1) + i;
    end
end    
        
mesh.Node = P;
mesh.Element = mat2cell(T, ones(NN,1), 4);