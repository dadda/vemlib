function element = construct_element_connectivity(mesh, E)
% CONSTRUCT_ELEMENT_CONNECTIVITY    Construction of element connectivity information.
%     ELEMENT = CONSTRUCT_ELEMENT_CONNECTIVITY(MESH, E) reads a MESH in OVM
%     format and returns connecitivty information for element E in ELEMENT.
%     ELEMENT is a col cell array of size equal to the number of faces
%     of E. The f-th entry ELEMENT{F} is a row array with the vertices
%     bounding face F listed in counterclockwise order, i.e. according to
%     the right hand rule applied to the outer normal.

% Topological face index and face orientation
tfaces = floor( ( mesh.element2hfaces{E}+1 )/2 );
faces_orientation = mod(mesh.element2hfaces{E}, 2);

Nfc = length(mesh.element2hfaces{E});
element = cell(Nfc, 1);
for f = 1:Nfc
    % Topological edge index and edge orientation
    tedges = floor( ( mesh.face2hedges{tfaces(f)}+1 )/2 );
    edges_orientation = mod(mesh.face2hedges{tfaces(f)}, 2);
    edges = mesh.edge2vertices(tedges,:)';

    % If the current half face is odd, then its half
    % edges are given by mesh.face2hedges{tface}. Otherwise, half
    % face = opp(mesh.face2hedges{tface}) = opp of each single half
    % edge mesh.face2hedges{tface}. To find the opp half edges, we
    % have to check whether mesh.face2hedges{tface} are odd or even.
    % In the first case, opp(half edge) = half edge + 1, whereas in the
    % second case, opp(half edge) = half edge - 1.
    element{f} = faces_orientation(f) * (edges_orientation .* edges(1,:) + (1-edges_orientation).*edges(2,:)) + ...
           (1-faces_orientation(f)) * ((1-edges_orientation(end:-1:1)) .* edges(1,end:-1:1) + edges_orientation(end:-1:1).*edges(2,end:-1:1));
end

end
