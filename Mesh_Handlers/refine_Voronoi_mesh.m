%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Refine a Voronoi mesh
%
% Given a starting Voronoi mesh, this script generate a finer nested mesh.
%

addpath ../../PolyMesher/
addpath ../Mesh_Handlers
addpath ../Mesh_Handlers/PolyMesher_Addons/
addpath ../Quadrature

rng('shuffle')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Random distribution for seed points:
randType = 'uniform';

% Initial number of iteration for PolyMesher
MaxIterDefault      = 100;

% Relative tolerance for area test (see comments in create_Voronoi_mesh_unitsquare.m
% about potential problems caused by roundoff errors).
reltolerance4Areas  = 1e-8;

% Maximum number of restarts allowed for PolyMesher
maxNumRestarts      = 100;

% Tolerance for checking point proximity
abstolPointTooClose = 5e-4;

% Tolerance for checking whether a point is on a line or not
abstolPointOnLine   = abstolPointTooClose;

% Tolerance for checking whether a point is on the border of a polygon or not
abstolOnPolygon     = abstolPointTooClose/10;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Override error messages issued by MATLAB to allow PolyMesher restarting
% in particular degenerate situations
warning('error', 'MATLAB:voronoin:circumcenter:ColinearCoplanarPoints');
warning('error', 'MATLAB:qhullmx:InternalWarning');

% Load the mesh to be refined
filename = input('Name of the mesh to be refined: ', 's');
data = load(filename);

Element        = data.mesh.Element;
Node           = data.mesh.Node;
numOldElements = length(Element);

hmax = zeros(numOldElements, 1);
area = zeros(numOldElements, 1);

% Compute diameter for each element in the mesh
for K = 1:numOldElements
    hmax(K) = max( pdist( Node(Element{K},:) ) );
    
    NverLoc = length(Element{K});
    Nx      = Node(Element{K}([2:NverLoc 1]),2) - Node(Element{K},2);
    Ny      = Node(Element{K},1) - Node(Element{K}([2:NverLoc 1]),1);
    area(K) = (Nx' * Node(Element{K},1) + Ny' * Node(Element{K},2))/2.;
end

[hmaxSorted, ix] = sort(hmax);
hmax             = hmaxSorted(end);

% Index of the last element in the sorted list that does not have to be
% refined
keepUntilMe      = find(hmaxSorted <= hmax/2);

% [sortedArea, ix] = sort(area);
% maxArea          = sortedArea(end);
% % Index of the last element in the sorted list that does not have to be
% % refined
% keepUntilMe      = find(sortedArea < maxArea/4);

if isempty(keepUntilMe)
    keepUntilMe = 0;
else
    keepUntilMe = keepUntilMe(end);
end

NElem          = input('Enter the number of subdivisions for each element to be refined: ');
numNewElements = keepUntilMe + (numOldElements-keepUntilMe) * NElem;

% Array for storing old and new Node/Element data
tmpNodeList     = cell(numOldElements, 1);
tmpElementList  = cell(numOldElements, 1);

% Cell array of ndoes that will be added to newNodes
addedNodes = cell(numOldElements, 1);

% Put all the original nodes into the list of nodes of the finer mesh
newNodes = Node;

% Construct edge2elements map
tmp = loadmesh(filename, 0);

edge2elements = -ones(tmp.Nfc, 2);
for K = 1:tmp.Nelt
    Nedge_loc = tmp.elements(K,1);
    for e = 1:Nedge_loc
        edge = tmp.edgebyele(K,e);
        if edge2elements(edge,1) == -1
            edge2elements(edge,1) = K;
        else
            edge2elements(edge,2) = K;
        end
    end
end

for K = 1:keepUntilMe
    tmpNodeList{K}    = Node( Element{ix(K)}, :);
    tmpElementList{K} = cell(1,1); tmpElementList{K}{1} = 1:length(Element{ix(K)});
end

parfor K = keepUntilMe+1:numOldElements
    fprintf('Running the mesher on element %i...\n', K)
    myElementNodes = Node(Element{ix(K)},:);
    BdBox   = [ min(myElementNodes(:,1)) max(myElementNodes(:,1)) ...
                min(myElementNodes(:,2)) max(myElementNodes(:,2)) ];
    NverLoc = size(myElementNodes,1);
    Nx = myElementNodes([2:NverLoc 1],2) - myElementNodes(:,2);
    Ny = myElementNodes(:,1) - myElementNodes([2:NverLoc 1],1);
    refArea = (Nx' * myElementNodes(:,1) + Ny' * myElementNodes(:,2))/2.;
    
    MaxIter = MaxIterDefault;
    numRestarts = 0;
    while true
        try
            [localNodes, localElement] = PolyMesher_modified(@voroDomain,NElem,MaxIter,randType,...
                myElementNodes,BdBox);
        catch error
            if strcmp(error.identifier, 'MATLAB:voronoin:circumcenter:ColinearCoplanarPoints') || ...
                strcmp(error.identifier, 'MATLAB:qhullmx:InternalWarning')
                warning( 'Warning issued by qhull or voronoin. Number of restarts: %i. Restarting PolyMesher ...', ...
                         numRestarts )
                numRestarts = numRestarts + 1;
                continue;
            end
        end
        
        checkArea = 0;
        for s = 1:NElem
            myLocalElement = [ localNodes(localElement{s},1), ...
                               localNodes(localElement{s},2) ];
            NverLoc = size(myLocalElement,1);
            Nx = myLocalElement([2:NverLoc 1],2) - myLocalElement(:,2);
            Ny = myLocalElement(:,1) - myLocalElement([2:NverLoc 1],1);
            checkArea = checkArea + (Nx' * myLocalElement(:,1) + Ny' * myLocalElement(:,2))/2.;
        end
        relError = abs(refArea - checkArea)/refArea;
        minDist  = min( pdist( localNodes ) );
        if (relError < reltolerance4Areas*10^(floor(numRestarts/maxNumRestarts))) && ...
                (minDist > abstolPointTooClose)
            break
        else
            warning( 'Relative error = %e, minimum distance = %e. Number of restarts: %i. Restarting PolyMesher ...', ...
                     relError, minDist, numRestarts )
        end
        if relError > reltolerance4Areas*10^(floor(numRestarts/maxNumRestarts))
            numRestarts = numRestarts + 1;
        end
    end

    tmpNodeList{K} = localNodes;
    tmpElementList{K} = localElement;
    
    idx = knnsearch(localNodes, myElementNodes);
    localNodes(idx,:) = [];
    addedNodes{K} = localNodes;
end

nodesFromEdges = cell(tmp.Nfc,1);

for e = 1:tmp.Nfc
    % edge is in the format [x1 y1 x2 y2]
    edge = [tmp.coordinates(tmp.edges(e,1),1) tmp.coordinates(tmp.edges(e,1),2) ...
            tmp.coordinates(tmp.edges(e,2),1) tmp.coordinates(tmp.edges(e,2),2)];
        
    edgeLength = sqrt((edge(1)-edge(3))^2 + (edge(2) - edge(4))^2);

    nodesOnEdge = [];
    
    % Process one element
    K = edge2elements(e,1);
    
    nodesFromK = addedNodes{K};
    if ~isempty(nodesFromK)    
        normCrossProd = (edge(1) - nodesFromK(:,1)) .* (edge(4) - nodesFromK(:,2)) ...
                        - (edge(2) - nodesFromK(:,2)) .* (edge(3) - nodesFromK(:,1));

        distance = abs(normCrossProd) / edgeLength;
        test     = distance < abstolPointOnLine;

        % We cannot have more than NElem-1 nodes on an edge
        if sum(test) > (NElem-1)
            [~, ix] = sort(distance);
            test(ix(NElem:end)) = false;    
        end
        
        nodesOnEdge = [ nodesOnEdge; nodesFromK(test,:) ]; %#ok<AGROW>
        addedNodes{K}(test,:) = [];
    end
    
    % Process the other one
    K = edge2elements(e,2);
    if K ~= -1
        nodesFromK = addedNodes{K};
        if ~isempty(nodesFromK)
            normCrossProd = (edge(1) - nodesFromK(:,1)) .* (edge(4) - nodesFromK(:,2)) ...
                            - (edge(2) - nodesFromK(:,2)) .* (edge(3) - nodesFromK(:,1));

            distance = abs(normCrossProd) / edgeLength;
            test     = distance < abstolPointOnLine;

            % We cannot have more than NElem-1 nodes on an edge
            if sum(test) > (NElem-1)
                [~, ix] = sort(distance);
                test(ix(NElem:end)) = false;
            end

            nodesOnEdge = [ nodesOnEdge; nodesFromK(test,:) ]; %#ok<AGROW>
            addedNodes{K}(test,:) = [];
        end
    end

    if ~isempty(nodesOnEdge)
        minDist = Inf;
        for q = 2:size(nodesOnEdge,1)
            for p = 1:q-1
                dist = pdist2(nodesOnEdge(q,:), nodesOnEdge(p,:));
                if dist < minDist
                    minDist = dist;
                    removeMe = p;
                end
            end
        end

        while minDist < abstolPointTooClose        
            nodesOnEdge(removeMe, :) = [];  %#ok<SAGROW>
            minDist = Inf;
            for q = 2:size(nodesOnEdge,1)
                for p = 1:q-1
                    dist = pdist2(nodesOnEdge(q,:), nodesOnEdge(p,:));
                    if dist < minDist
                        minDist = dist;
                        removeMe = p;
                    end
                end
            end
        end

        dist = pdist2( nodesOnEdge, [ edge(1) edge(2) ; edge(3) edge(4) ] );
        nodesOnEdge( any( dist < abstolPointTooClose, 2 ), : ) = []; %#ok<SAGROW>
    end
    nodesFromEdges{e} = nodesOnEdge;
end

% At this point nodesFromEdges should be added to the list of nodes
for e = 1:tmp.Nfc
    for v = 1:size(nodesFromEdges{e},1)
        dist = min( pdist2(nodesFromEdges{e}(v,:), newNodes) );
        if dist > abstolPointTooClose
            newNodes = [ newNodes; nodesFromEdges{e}(v,:) ];
        end
    end
end
% newNodes = [ newNodes; vertcat(nodesFromEdges{:}) ];

for K = keepUntilMe+1:numOldElements
    for q = 1:size(addedNodes{K},1)
        dist = min( pdist2( addedNodes{K}(q,:), newNodes ) );
        if dist > abstolPointTooClose
            newNodes = [ newNodes; addedNodes{K}(q,:) ];
        end
    end
end

%
% Construct connectivity information for new conforming mesh
%

% Connectivity array of new conforming mesh
newElements = cell(numNewElements, 1);
counter = 0;

for K = 1:numOldElements
    % Find nodes associated with element K
    for k = 1:length(tmpElementList{K})
        counter = counter + 1;
        myOldBoundary = [ tmpNodeList{K}(tmpElementList{K}{k},1) ...
                          tmpNodeList{K}(tmpElementList{K}{k},2) ];
                      
        numNodes = length(tmpElementList{K}{k});
        iterator = [2:numNodes 1];
        Dist = -Inf;

        for v = 1:numNodes
            newDist = dLine(newNodes, myOldBoundary(v,1), myOldBoundary(v,2), ...
                                      myOldBoundary(iterator(v),1), myOldBoundary(iterator(v),2));
            newDist = newDist(:,2);
            Dist = max( Dist, newDist );
        end
        
        % Preliminar connectivity data for current element can be obtained
        % by finding the points lying on the polygon's sides
        preliminarElement = find( abs(Dist) < abstolOnPolygon );
        
%         [~, onpoly] = inpolygon( NewNodes(:,1), NewNodes(:,2), ...
%                                  [myOldBoundary(:,1); myOldBoundary(1,1)], ...
%                                  [myOldBoundary(:,2); myOldBoundary(1,2)] );
%         preliminarElement = find( onpoly );
                
        % To select polygon vertices in counterclockwise fashion, we
        % compute the phase angles of the segments connecting the polygon
        % barycenter with the vertices and sort them in ascending order
        myNewBoundary = newNodes(preliminarElement, :);
        barycenter = sum(myNewBoundary,1)/length(preliminarElement);
        myNewBoundary(:,1) = myNewBoundary(:,1) - barycenter(1);
        myNewBoundary(:,2) = myNewBoundary(:,2) - barycenter(2);
        myTheta = atan2(myNewBoundary(:,2), myNewBoundary(:,1));
        [~, I] = sort(myTheta);
        newElements{counter} = preliminarElement(I)';
        myNewBoundary = myNewBoundary(I,:);

        % Check whether there is an old boundary point that should have been included
        Dist = -Inf;
        numNodes = length(newElements{counter});
        iterator = [2:numNodes 1];
        for v = 1:numNodes
            newDist = dLine(myOldBoundary, myNewBoundary(v,1), myNewBoundary(v,2), ...
                                           myNewBoundary(iterator(v),1), myNewBoundary(iterator(v),2));
            newDist = newDist(:,2);
            Dist = max( Dist, newDist );
        end
        ix = find(Dist > 0);
        
        preliminarElement = newElements{counter};
        for node = 1:length(ix)
            Dist = pdist2(newNodes, myOldBoundary(ix(node),:));
            [~, jx] = min(Dist);
            preliminarElement = [preliminarElement jx]; %#ok<AGROW>
        end
        preliminarElement = unique(preliminarElement);

        myNewBoundary = newNodes(preliminarElement, :);
        barycenter = sum(myNewBoundary,1)/length(preliminarElement);
        myNewBoundary(:,1) = myNewBoundary(:,1) - barycenter(1);
        myNewBoundary(:,2) = myNewBoundary(:,2) - barycenter(2);
        myTheta = atan2(myNewBoundary(:,2), myNewBoundary(:,1));
        [~, I] = sort(myTheta);
        newElements{counter} = preliminarElement(I);        
    end
end

% % Check the newly generated mesh
% figure()
% axis equal; hold on;
% 
% % Plot initial mesh
% for K = 1:size(Element,1)
%     plot(Node([Element{K} Element{K}(1)],1), ...
%          Node([Element{K} Element{K}(1)],2), 'g', 'Linewidth', 2)    
% end
% 
% pause
% 
% % Overlay finer mesh
% maxSideNum = 0;
% for K = 1:size(newElements,1)
%     plot(newNodes([newElements{K} newElements{K}(1)],1), ...
%          newNodes([newElements{K} newElements{K}(1)],2), 'b')
%     maxSideNum = max( maxSideNum, length(newElements{K}) );
% end
% 
% pause
% 
% for K = 1:size(newElements,1)
%     plot(newNodes(newElements{K},1), ...
%          newNodes(newElements{K},2), 'r*')
%     if length(newElements{K}) == maxSideNum
%         patch(newNodes(newElements{K},1), ...
%               newNodes(newElements{K},2), 'y')
%     else
%         patch(newNodes(newElements{K},1), ...
%               newNodes(newElements{K},2), 'b')
%     end
%     myNewBoundary = newNodes(newElements{K}, :);
%     barycenter = sum(myNewBoundary,1)/length(newElements{K});
%     text(barycenter(1), barycenter(2), [num2str(K) ',' num2str(length(newElements{K}))], 'HorizontalAlignment', 'center')
% end
% 
% for K = 1:size(Element,1)
%     plot(Node([Element{K} Element{K}(1)],1), ...
%          Node([Element{K} Element{K}(1)],2), 'g')    
% end

mesh.Node = max(min(newNodes, 1), 0);
mesh.Element = newElements;

fprintf('Number of nodes: %i\n', size(mesh.Node,1));
fprintf('Number of elements: %i\n', size(mesh.Element,1));

filename = input('Name of the refined mesh: ', 's');
save(filename, 'mesh')
