function mesh = readGMSHmesh( filename )

fid = fopen(filename);

fgets(fid);
fgets(fid);
fgets(fid);
fgets(fid);

Nver = fscanf(fid, '%d', 1);
Node = zeros(Nver, 2);
for i = 1:Nver
    tmp = fscanf(fid, '%f', 4);
    Node(i,:) = tmp(2:3);
end

% Fixing bug for unit circle and sector meshes
%
% Due to how the .geo file for the sector is written, node 3 turns out
% being a ghost node, whereas for the unit circle, node 5 is ghost. For all
% the other cases, use ghostNode = [].
ghostNode         = [];
Node(ghostNode,:) = [];

fgets(fid);
fgets(fid);
fgets(fid);

N = fscanf(fid, '%d', 1);
Element = cell(N, 1);
counter = 0;
for i = 1:N
    tmp = fscanf(fid, '%d', 2);
    if tmp(2) == 15
        fscanf(fid, '%d', 4);
    elseif tmp(2) == 1
        fscanf(fid, '%d', 5);
    else
        tmp = fscanf(fid, '%d', 6);
        counter = counter + 1;
        tmp = tmp(4:6); tmp = reshape(tmp, 1, 3);
        if ~isempty(ghostNode)
            if any(tmp == ghostNode)
                error('%i is not ghost', ghostNode)
            end
            tmp( tmp > ghostNode ) = tmp( tmp > ghostNode ) - 1;
        end
        Element{counter} = tmp;
    end
end
fclose(fid);

Element(counter+1:end) = [];

mesh = [];
mesh.Node = Node;
mesh.Element = Element;

end
