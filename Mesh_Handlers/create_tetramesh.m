% Generate a 3D Delaunay Triangulation and save it in the format required
% by the VEM code.

%% Mesh the unit cube [0,1]^3 or similar cubes and visualize it
n = 33;
d = linspace(0,1,n);
[x,y,z] = meshgrid(d,d,d);
x = x(:); y = y(:); z = z(:);
T = delaunayTriangulation(x,y,z);

% Visualize mesh
%figure()
%tetramesh(T)

%% Create data structure required by the VEM code

mesh.coordinates = T.Points;
mesh.Nver        = size(mesh.coordinates,1);

elements         = T.ConnectivityList;
mesh.Nelt        = size(elements, 1);

% Form all edges
edges = [elements(:,[1 2]);
         elements(:,[1 3]);
         elements(:,[1 4]);
         elements(:,[2 3]);
         elements(:,[2 4]);
         elements(:,[3 4])];
edges = sort(edges, 2);

mesh.edge2vertices = unique(edges, 'rows');
mesh.Ned = size(mesh.edge2vertices, 1);

% Form all faces
faces = [elements(:, [1 2 3]);
         elements(:, [1 2 4]);
         elements(:, [1 3 4]);
         elements(:, [4 2 3])];
faces = sort(faces, 2);
% Remark: faces can be overwritten. Since we are working with triangular
% faces, sorting cannot mess face orientation up, as in the hexahedral
% case. It can change it, at most.
[faces, ~, jx] = unique(faces, 'rows');

mesh.Nfc = size(faces, 1);

element2faces = zeros(mesh.Nelt, 4);
element2faces(:, 1) = jx(1:mesh.Nelt);
element2faces(:, 2) = jx(mesh.Nelt+1:2*mesh.Nelt);
element2faces(:, 3) = jx(2*mesh.Nelt+1:3*mesh.Nelt);
element2faces(:, 4) = jx(3*mesh.Nelt+1:end);

mesh.face2elements = -ones(mesh.Nfc, 2);
for K = 1:mesh.Nelt
    for fc = 1:4
        if mesh.face2elements(element2faces(K,fc),1) == -1
            mesh.face2elements(element2faces(K,fc),1) = K;
        else
            mesh.face2elements(element2faces(K,fc),2) = K;
        end
    end
end

mesh.element2hfaces = zeros(mesh.Nelt,4);
%mesh.element2hfaces(:,1) = 4;
for fc = 1:mesh.Nfc
    Ktmp = mesh.face2elements(fc,1);
    % Local face numbering
    localfc = find(element2faces(Ktmp,:) == fc);
    
    % Compute barycenter
    barycenter_el = sum(mesh.coordinates(elements(Ktmp,:),:),1)/4.;
    % Compute normal
    v1 = mesh.coordinates(faces(fc,2),:) - mesh.coordinates(faces(fc,1),:);
    v2 = mesh.coordinates(faces(fc,3),:) - mesh.coordinates(faces(fc,1),:);
    normal = [v1(2)*v2(3) - v1(3)*v2(2), ...
              v1(3)*v2(1) - v1(1)*v2(3), ...
              v1(1)*v2(2) - v1(2)*v2(1)];
    
    % Check on which size the barycenter is to find the correct half-face
    check_test = dot(normal, barycenter_el - mesh.coordinates(faces(fc,1),:));
    if check_test > 0
        % The face is clockwise-oriented. Assign
        % even half-face (slightly different from OpenVolumeMesh)
        mesh.element2hfaces(Ktmp,localfc) = 2*fc;
        Ktmp = mesh.face2elements(fc,2);
        if Ktmp ~= -1
            % For the adjacent element, fc is counter-clockwise oriented
            localfc = find(element2faces(Ktmp,:) == fc);
            mesh.element2hfaces(Ktmp,localfc) = 2*fc-1;
        end
    else        
        % The face is counter-clockwise oriented. Assign
        % odd half-face (slightly different from OpenVolumeMesh)
        mesh.element2hfaces(Ktmp,localfc) = 2*fc-1;
        Ktmp = mesh.face2elements(fc,2);
        if Ktmp ~= -1
            % For the adjacent element, fc is counter-clockwise oriented
            localfc = find(element2faces(Ktmp,:) == fc);
            mesh.element2hfaces(Ktmp,localfc) = 2*fc;
        end        
    end
end
mesh.element2hfaces = mat2cell(mesh.element2hfaces, ones(mesh.Nelt,1), 4);

% In the previous loop, we assumed that each face fc is oriented as
% follows: faces(fc,1) -> faces(fc,2) -> faces(fc,3) -> faces(fc,1).
% Since each row of faces has been sorted in ascending order above,
% the orientation of edges 1->2 and 2->3 will match that of the
% corresponding edges in mesh.edge2vertices. Thus, an odd half edge is
% assigned. Similarly, the orientation of edge 3->1 is switched with
% respect to the corresponding edge in mesh.edge2vertices. Hence, an
% even half edge is assigned.

mesh.face2hedges = zeros(mesh.Nfc,3);
[tmp, ~, jx1] = unique([faces(:,[1 2]); faces(:,[2 3]); faces(:,[1 3])], 'rows');
[~, ~, jx2] = intersect(tmp, mesh.edge2vertices, 'rows');
mesh.face2hedges(:,1) = 2*jx2(jx1(1:mesh.Nfc))-1;
mesh.face2hedges(:,2) = 2*jx2(jx1(mesh.Nfc+1:2*mesh.Nfc))-1;
mesh.face2hedges(:,3) = 2*jx2(jx1(2*mesh.Nfc+1:end));
mesh.face2hedges = mat2cell(mesh.face2hedges, ones(mesh.Nfc,1), 3);

%% Save the mesh
name = sprintf('meshfiles3d/tetrahedra/tetra_Nelt%06d.mat', mesh.Nelt);
save(name, 'mesh')

