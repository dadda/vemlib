function [E]=edgefinder(T)
% ---------
% given a mesh defined by input matrix T it
% finds the edges and generates an edge matrix E organized as follows.
% On each row there appear the vertex number 1 number, vertex number 2 number, 
% ... and then left and right element numbers. 
% NOTE: matrix E has a zero in the last column instead of 
% ... an element number if this is a boundary edge.
% ---------
% Input matrix T has on each row
% the data for an element, i.e. the number of vertexes, then the vertex
% numbers ordered in anti-clockwise sense.
% ---------
m=size(T,1);
k=1;
% ----------------------------------------------------------------- 
% generate matrix A with repeated edges, generated element by element 
% -----------------------------------------------------------------
for i=1:m
    for j=1:(T(i,1)-1)
        A(k,1:2)=T(i,j+1:j+2);
        A(k,3)=i;
        k=k+1;
    end
    A(k,1)=T(i,T(i,1)+1);
    A(k,2)=T(i,2);
    A(k,3)=i;
    k=k+1; 
end       
% ----------------------------------------------------------------- 
%    generate matrix E working on matrix A
% -----------------------------------------------------------------
flag=0;
% N=size(A,1);
for k=1:size(A,1);
    flag=0;
    for kk=k+1:size(A,1)
        if (( A(k,1)==A(kk,2) ) && ( A(k,2)==A(kk,1) ))   
            E(k,:)=[A(k,1:3),A(kk,3)];
            A=[A(1:kk-1,:) ; A(kk+1:end,:)];    
            % N=N-1;    
            flag = 1;
            break
        end
    end
    if k > size(A,1)  
       break
    end
    if flag < 0.5
        E(k,:)=[A(k,1:3),0];
    end
end    
    

    
    
    
    
    
    
    
    
    
    
    