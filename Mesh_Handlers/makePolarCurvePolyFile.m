function points = makePolarCurvePolyFile(filename, Vmax)
% FILENAME name of the output .poly file
% VMAX     approximate maximum volume of a mesh triangle

Lmax = sqrt( 4 * Vmax / sqrt(3) );

L      = Lmax; %Lmax/2;
dtheta = asin(L/2);

Npt   = ceil(2*pi/dtheta);
theta = (0:Npt-1) * dtheta;

% % For the astroid
% theta = [ theta( theta < pi/2 ), ...
%           pi/2, ...
%           theta( theta > pi/2 & theta < pi ), ...
%           pi, ...
%           theta( theta > pi & theta < 3*pi/2 ), ...
%           3*pi/2, ...
%           theta( theta > 3*pi/2 ) ];

% % For the spiral
% theta = [ theta, 2*pi ];
% theta = 4*theta + (1 - theta/(2*pi)) * pi/2;
% theta = [ theta, theta(end:-1:1) ];

theta = theta(:);
Npt   = length(theta);

% Create file
fid = fopen(filename, 'w');

%
% Node part
%

% % Unit circle
% points = [ cos(theta) sin(theta) ];

% % Astroid
% points = [ cos(theta).^3 sin(theta).^3 ];

% % Flower
% n      = 9;
% radius = 2 + sin(n*theta);
% points = [ radius.*cos(theta) radius.*sin(theta) ];

% % Spiral
% Ri     = 0.9;
% radius = [ sqrt(theta(1:Npt/2));
%            Ri*sqrt(theta(Npt/2+1:end)) ];
% points = [ radius.*cos(theta) radius.*sin(theta) ];

% Egg
a = 1;
b = 0.7*a; bp = b/4; ap = a/2 - bp;
xF = bp*cos(theta); rp = ap + bp*cos(theta);
points = [xF+rp.*cos(theta) rp.*sin(theta)];

fprintf(fid, '%i 2 0 0\n', Npt);
for i = 1:Npt
    fprintf(fid, '%i %.16f %.16f\n', i, points(i,1), points(i,2));
end

% Boundary segments
fprintf(fid, '%i 0\n', Npt);
for i = 1:Npt
    fprintf(fid, '%i %i %i\n', i, i, mod(i,Npt)+1);
end

% No holes
fprintf(fid, '0\n');

fclose(fid);

end
