function [ elements_diameters, elements_volumes, elements_centroids, elements_qformula3D ] = ...
    elements_features( vertices, elements, faces_areas_by_ele, faces_normals_by_ele, faces_algdist_by_ele, ...
                       faces_orders_by_ele, k )
                   
% Number of volumes
Nelt = size(elements, 1);

elements_diameters  = zeros(Nelt,1);
elements_volumes    = zeros(Nelt,1);
elements_centroids  = zeros(Nelt,3);
elements_qformula3D = cell(Nelt,1);

% Set k < 0 if no quadrature over the polyhedron is required.
% if k >= 0
%     % Create 3d quadrature formula (just the nodes) of order k
%     formula = StroudQuadrature(k,3);
%     formula = formula(:,1:4);
% 
%     % Template string for monomial basis functions
%     basis_str = 'pow((x-(%.16f))/%.16f,%d) * pow((y-(%.16f))/%.16f,%d) * pow((z-(%.16f))/%.16f,%d)';
% 
%     % Dimension of the 3d polynomial space
%     d3 = ((k+3)*(k+2)*(k+1))/6;
% end

for E = 1:Nelt
    % Element diameter
    elements_diameters(E) = max( pdist( vertices(horzcat(elements{E}{:}),:) ) );
    
    % Element volume
    %
    % We assume that faces_areas_by_ele{E} and faces_algdist_by_ele{E} are
    % both column vectors. Formula (12) in Chin paper is used.
    elements_volumes(E)   = (faces_areas_by_ele{E}' * faces_algdist_by_ele{E})/3.;
    
    % Number of faces
    Nfc = uint64(size(elements{E},1));
    
    facets_cumsizes = uint64([0; cumsum( faces_orders_by_ele{E} )]);
    
    % Centroid
    elements_centroids(E,1) = ...
        chinIntegrate4Matlab('x', vertices, uint64(horzcat(elements{E}{:})), ...
                             Nfc, facets_cumsizes, ...
                             faces_normals_by_ele{E}, faces_algdist_by_ele{E}) / elements_volumes(E);
    elements_centroids(E,2) = ...
        chinIntegrate4Matlab('y', vertices, uint64(horzcat(elements{E}{:})), ...
                             Nfc, facets_cumsizes, ...
                             faces_normals_by_ele{E}, faces_algdist_by_ele{E}) / elements_volumes(E);
    elements_centroids(E,3) = ...
        chinIntegrate4Matlab('z', vertices, uint64(horzcat(elements{E}{:})), ...
                             Nfc, facets_cumsizes, ...
                             faces_normals_by_ele{E}, faces_algdist_by_ele{E}) / elements_volumes(E);

    % Construct quadrature scheme only if k >= 0
    %
    % In our context, quadrature schemes are only needed if we have a right
    % hand side which is not a polynomial.
    if k >= 0
%         %%%%%% Step 1: tetrahedralize the element using tetgen
%         
%         %%% Create temporary .poly file
%         fid = fopen('tmp.poly', 'w');
%         [unique_vertices, ~, ic] = unique(horzcat(elements{E}{:}));
%         local_vertices  = vertices(unique_vertices, :);
%         local_Nver      = size(local_vertices,1);
%         
%         % Part 1 - node list
%         fprintf(fid, '%d 3 0 0\n', local_Nver);
%         for i = 1:local_Nver
%             fprintf(fid, '%d %.16f %.16f %.16f\n', i, local_vertices(i, :));
%         end
%         
%         % Part 2 - facet list
%         fprintf(fid, '%d 0\n', Nfc);
%         for i = 1:Nfc
%             fprintf(fid, '1\n');
%             fprintf(fid, '%d %s\n', length(elements{E}{i}), ...
%                                     num2str(ic(facets_cumsizes(i)+1:facets_cumsizes(i+1))') );
%         end
%         fclose(fid);
%         
%         %%% Call tetgen
% %        !/home/daniele/Documents/codes/tetgen1.5.1-beta1/tetgen -pQF tmp.poly
%         eval(sprintf('!/home/daniele/Documents/codes/tetgen1.5.1-beta1/tetgen -pQFYa%g tmp.poly', ...
%             elements_volumes(E)))
% %        !rm tmp.poly
%         
%         %%%%%% Step 2: load tetgen output and generate quadrature nodes
%         
%         fid = fopen('tmp.1.node');
%         line = str2num( fgetl(fid) );
%         local_Nver = line(1);
%         local_vertices = zeros(local_Nver,3);
%         for i = 1:local_Nver
%             line = str2num( fgetl(fid) );
%             local_vertices(i,:) = line(2:4);
%         end
%         fclose(fid);
% %        !rm tmp.1.node
%         
%         fid = fopen('tmp.1.ele');
%         line = str2num( fgetl(fid) ); %#ok<*ST2NM>
%         local_Nelt = line(1);
%         local_elements = zeros(4,local_Nelt);
%         for i = 1:local_Nelt
%             line = str2num( fgetl(fid) );
%             local_elements(:,i) = line(2:end);
%         end
%         fclose(fid);
% %        !rm tmp.1.ele
%         
%         x_local = local_vertices(:,1); x_local = x_local(local_elements);
%         y_local = local_vertices(:,2); y_local = y_local(local_elements);
%         z_local = local_vertices(:,3); z_local = z_local(local_elements);
%         x_local = formula*x_local; x_local = x_local(:);
%         y_local = formula*y_local; y_local = y_local(:);
%         z_local = formula*z_local; z_local = z_local(:);
%         
%         %%%%%% Step 3: assemble moment fitting system and solve it
%         
%         %%% RHS for the moment fitting equation
%         %
%         % Each entry of rhs_mf is of the form
%         % (\int_E phi_k dE)/|E|, for k = 1, ..., d3.
%         rhs_mf = zeros(d3,1);
% 
%         % The first monomial basis function is the indicator function
%         rhs_mf(1) = elements_volumes(E);
%         
%         i = 2;
%         for q = 1:k
%             for r = q:-1:0
%                 for s = q-r:-1:0
%                     t = q-r-s;
%                     basis_fun = sprintf(basis_str, ...
%                         elements_centroids(E,1), elements_diameters(E), r, ...
%                         elements_centroids(E,2), elements_diameters(E), s, ...
%                         elements_centroids(E,3), elements_diameters(E), t);
%     
% %                     rhs_mf(i) = ...
% %                         chinIntegrate4Matlab(basis_fun, vertices, ...
% %                         uint64(horzcat(elements{E}{:})), Nfc, facets_cumsizes, ...
% %                         faces_normals_by_ele{E}, faces_algdist_by_ele{E}) / elements_volumes(E);
%                     rhs_mf(i) = ...
%                         chinIntegrate4Matlab(basis_fun, vertices, ...
%                         uint64(horzcat(elements{E}{:})), Nfc, facets_cumsizes, ...
%                         faces_normals_by_ele{E}, faces_algdist_by_ele{E});
%                     i = i + 1;
%                 end
%             end
%         end
%         
%         %%% Coefficient matrix for the moment fitting equation
%         A_mf = monomial_basis_3d(x_local, y_local, z_local, k, ...
%             elements_centroids(E,1), elements_centroids(E,2), elements_centroids(E,3), ...
%             elements_diameters(E), ones(size(x_local)));
%         A_mf = permute(A_mf, [3 1 2]);
%         
%         %%% Solve the moment fitting equation using linear least squares
%         %   with nonnegativity constraints
%         %
%         wstar = lsqnonneg(A_mf, rhs_mf);
% 
%         %   The solution of the moment fitting equation should be a vector
%         %   with the same dimension as the polynomial space of degree at
%         %   most k. Thus we select the highest d3 elements of the weights
%         %   vector and restrict x_local, y_local, and z_local accordingly.
% 
%         [~, iwstar] = sort(wstar, 'descend');
%         elements_qformula3D{E} = [x_local(iwstar(1:d3)) ...
%                                   y_local(iwstar(1:d3)) ...
%                                   z_local(iwstar(1:d3)) ...
%                                   wstar(iwstar(1:d3))];

        % constructQuadrature requires vertices of the current element
        % only.
        [unique_vertices, ~, iuv] = unique(horzcat(elements{E}{:}));
        [nodes, weights] = constructQuadrature4Matlab(vertices(unique_vertices, :), uint64(iuv), ...
            Nfc, facets_cumsizes, int32(k), elements_diameters(E), elements_volumes(E), ...
            elements_centroids(E,:), faces_normals_by_ele{E}, ...
            faces_algdist_by_ele{E});
        elements_qformula3D{E} = [nodes weights];
    end
end


end









