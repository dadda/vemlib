function [] = writeOVM( mesh, filename )

fid = fopen(filename, 'w');

fprintf(fid, 'OVM ASCII\n');

fprintf(fid, 'Vertices\n');
fprintf(fid, '%i\n', mesh.Nver);
for i = 1:mesh.Nver
    fprintf(fid, '%.16f %.16f %.16f\n', mesh.coordinates(i,:));    
end

fprintf(fid, 'Edges\n');
fprintf(fid, '%i\n', mesh.Ned);
for i = 1:mesh.Ned
    fprintf(fid, '%i %i\n', mesh.edge2vertices(i,:)-1);
end

fprintf(fid, 'Faces\n');
fprintf(fid, '%i\n', mesh.Nfc);
for i = 1:mesh.Nfc
    fprintf(fid, '%i %s\n', length(mesh.face2hedges{i}), num2str(mesh.face2hedges{i}-1));
end

fprintf(fid, 'Polyhedra\n');
fprintf(fid, '%i\n', mesh.Nelt);
for i = 1:mesh.Nelt
    fprintf(fid, '%i %s\n', length(mesh.element2hfaces{i}), num2str(mesh.element2hfaces{i}-1));
end

fclose(fid);

end
