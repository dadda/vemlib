function mesh = readDualMesh( basename )

%
% Read 'Vertices' file
%

fid = fopen(sprintf('%s%s', basename, '.node'));

tmp = fscanf(fid, '%i', 2);
mesh.Nver = tmp(1);
mesh.coordinates = zeros(mesh.Nver, 3);
if tmp(2) == 0
    mesh.vBndMarkers = [];
    for i = 1:mesh.Nver
        mesh.coordinates(i,:) = fscanf(fid, '%f', 3);
    end
else
    mesh.vBndMarkers = zeros(mesh.Nver, 1);
    for i = 1:mesh.Nver
        tmp = fscanf(fid, '%f', 4);
        mesh.coordinates(i,:) = tmp(1:3);
	mesh.vBndMarkers(i) = tmp(4);
    end    
end

fclose(fid);

%
% Read 'v2p' file
%

fid = fopen(sprintf('%s%s', basename, '.v2p'));
if fid ~= -1
    tmp = fscanf(fid, '%i', 1);
    mesh.v2p = cell(mesh.Nver, 1);
    for i = 1:mesh.Nver
        NpLoc = fscanf(fid, '%i', 1);
        mesh.v2p{i} = fscanf(fid, '%i', [1, NpLoc]) + 1;
    end
    fclose(fid);
end

%
% Read 'Edges' file
%

fid = fopen(sprintf('%s%s', basename, '.edge'));

tmp = fscanf(fid, '%i', 2);
mesh.Ned = tmp(1);
mesh.edge2vertices = zeros(mesh.Ned, 2);
if tmp(2) == 0
    mesh.eBndMarkers = [];
    for i = 1:mesh.Ned
        mesh.edge2vertices(i,:) = fscanf(fid, '%i', 2) + 1;
    end
else
    mesh.eBndMarkers = zeros(mesh.Ned, 1);
    for i = 1:mesh.Ned
        tmp = fscanf(fid, '%i', 3);
        mesh.edge2vertices(i,:) = tmp(1:2) + 1;
	mesh.eBndMarkers(i) = tmp(3);
    end
end

fclose(fid);

%
% Read 'Faces' file
%

fid = fopen(sprintf('%s%s', basename, '.face'));

tmp = fscanf(fid, '%i', 2);
mesh.Nfc = tmp(1);
mesh.face2hedges = cell(mesh.Nfc, 1);
mesh.face2elements = zeros(mesh.Nfc, 2);
if tmp(2) == 0
    mesh.fBndMarkers = [];
else
    mesh.fBndMarkers = zeros(mesh.Nfc, 1);
end

for i = 1:mesh.Nfc
    NedLoc = fscanf(fid, '%i', 1);
    mesh.face2hedges{i} = fscanf(fid, '%i', [1, NedLoc]) + 1; % Mind the different convention used in the c code
    mesh.face2elements(i,:) = fscanf(fid, '%i', 2) + 1;
    if mesh.face2elements(i,2) == 0
        mesh.face2elements(i,2) = -1;
    end
    if tmp(2) == 1
        mesh.fBndMarkers(i) = fscanf(fid, '%i', 1);
    end
end

fclose(fid);

%
% Read 'Polyhedra' file
%

fid = fopen(sprintf('%s%s', basename, '.polyhedron'));

mesh.Nelt = fscanf(fid, '%i', 1);
mesh.element2hfaces = cell(mesh.Nelt, 1);
for i = 1:mesh.Nelt
    NfcLoc = fscanf(fid, '%i', 1);
    mesh.element2hfaces{i} = fscanf(fid, '%i', [1, NfcLoc]) + 1;
end

fclose(fid);

end
