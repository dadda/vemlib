#include "pmgo.h"

/*
 * Compute the dimension of the parametric space
 */
int dimParametricSpace( const int Nver,
			const int * const vFlags )
{
  int i, dim = 0;

  for (i = 0; i < Nver; ++i) {
    if ( vFlags[i] == -1 )
      continue;
    else if ( vFlags[i] == 0 )
      dim += 3;
    else if ( ( vFlags[i] >= 1 ) && ( vFlags[i] <= 12 ) )
      ++dim;
    else
      dim += 2;
  }

  return dim;
}


/*
 * Compute lower and upper boundaries for coordinates in the parametric space
 */
void getParametricBoundaries( const int Nver,
			      const int * const vFlags,
			      double * lbounds,
			      double * ubounds )
{
  int i, counter = 0;

  for (i = 0; i < Nver; ++i) {
    if ( vFlags[i] == -1 )
      continue;
    else if ( vFlags[i] == 0 ) {
      lbounds[counter]   = 0.0; ubounds[counter]   = 1.0;
      lbounds[counter+1] = 0.0; ubounds[counter+1] = 1.0;
      lbounds[counter+2] = 0.0; ubounds[counter+2] = 1.0;
      counter += 3;
    }
    else if ( ( vFlags[i] >= 1 ) && ( vFlags[i] <= 12 ) ) {
      lbounds[counter] = 0.0; ubounds[counter] = 1.0;
      ++counter;
    }
    else {
      lbounds[counter]   = 0.0; ubounds[counter]   = 1.0;
      lbounds[counter+1] = 0.0; ubounds[counter+1] = 1.0;
      counter += 2;
    }
  }
}


/*
 * Cartesian to parametric conversion
 */
void car2par( const int Nver,
	      const int * const vFlags,
	      const double * const cartesian,
	      double * parametric )
{
  int i, counter = 0;

  for (i = 0; i < Nver; ++i) {
    if ( vFlags[i] == -1 )
      continue;
    else if ( vFlags[i] == 0 ) {
      parametric[counter]   = cartesian[3*i];
      parametric[counter+1] = cartesian[3*i+1];
      parametric[counter+2] = cartesian[3*i+2];
      counter += 3;
    }
    else if ( ( vFlags[i] == 1 ) || ( vFlags[i] == 2 )
	      || ( vFlags[i] == 9 ) || ( vFlags[i] == 10 ) ) {
      parametric[counter] = cartesian[3*i+1];
      ++counter;
    }
    else if ( ( vFlags[i] == 3 ) || ( vFlags[i] == 4 )
	      || ( vFlags[i] == 11 ) || ( vFlags[i] == 12 ) ) {
      parametric[counter] = cartesian[3*i];
      ++counter;
    }
    else if ( ( vFlags[i] == 5 ) || ( vFlags[i] == 6 )
	      || ( vFlags[i] == 7 ) || ( vFlags[i] == 8 ) ) {
      parametric[counter] = cartesian[3*i+2];
      ++counter;
    }
    else if ( ( vFlags[i] == 13 ) || ( vFlags[i] == 14 ) ) {
      parametric[counter]   = cartesian[3*i+1];
      parametric[counter+1] = cartesian[3*i+2];
      counter += 2;
    }
    else if ( ( vFlags[i] == 15 ) || ( vFlags[i] == 16 ) ) {
      parametric[counter]   = cartesian[3*i];
      parametric[counter+1] = cartesian[3*i+2];
      counter += 2;
    }
    else {
      parametric[counter]   = cartesian[3*i];
      parametric[counter+1] = cartesian[3*i+1];
      counter += 2;
    }
  }
}


/*
 * Indices mapping each cartesian vertex to its first corresponding parameter
 */
void car2firstParIdx( const int Nver,
		      const int * const vFlags,
		      int * const c2firstpIdx )
{
  int i, counter = 0;

  for (i = 0; i < Nver; ++i) {
    if ( vFlags[i] == -1 ) {
      c2firstpIdx[i] = -1;
    } else if ( vFlags[i] == 0 ) {
      c2firstpIdx[i] = counter;
      counter += 3;
    } else if ( ( vFlags[i] >= 1 ) && ( vFlags[i] <= 12 ) ) {
      c2firstpIdx[i] = counter;
      ++counter;
    } else {
      c2firstpIdx[i] = counter;
      counter += 2;
    }
  }  
}


/*
 * Parametric to cartesian conversion
 */
void par2car( const double * const parametric,
	      double * const cartesian,
	      const int Nver,
	      const int * const cartesianIdx,
	      const int cartesianIdxDim,
	      const int * const c2firstpIdx,
	      const int * const vFlags )
{
  int i, j, N;

  N = cartesianIdx == NULL ? Nver : cartesianIdxDim;
  for (i = 0; i < N; ++i) {
    j = cartesianIdx == NULL ? i : cartesianIdx[i];
    if ( vFlags[j] == -1 )
      continue;
    else if ( vFlags[j] == 0 ) {
      cartesian[3*j]   = parametric[c2firstpIdx[j]];
      cartesian[3*j+1] = parametric[c2firstpIdx[j]+1];
      cartesian[3*j+2] = parametric[c2firstpIdx[j]+2];
    }
    else if ( ( vFlags[j] == 1 ) || ( vFlags[j] == 2 )
  	      || ( vFlags[j] == 9 ) || ( vFlags[j] == 10 ) ) {
      cartesian[3*j+1] = parametric[c2firstpIdx[j]];
    }
    else if ( ( vFlags[j] == 3 ) || ( vFlags[j] == 4 )
  	      || ( vFlags[j] == 11 ) || ( vFlags[j] == 12 ) ) {
      cartesian[3*j] = parametric[c2firstpIdx[j]];
    }
    else if ( ( vFlags[j] == 5 ) || ( vFlags[j] == 6 )
  	      || ( vFlags[j] == 7 ) || ( vFlags[j] == 8 ) ) {
      cartesian[3*j+2] = parametric[c2firstpIdx[j]];
    }
    else if ( ( vFlags[j] == 13 ) || ( vFlags[j] == 14 ) ) {
      cartesian[3*j+1] = parametric[c2firstpIdx[j]];
      cartesian[3*j+2] = parametric[c2firstpIdx[j]+1];
    }
    else if ( ( vFlags[j] == 15 ) || ( vFlags[j] == 16 ) ) {
      cartesian[3*j]   = parametric[c2firstpIdx[j]];
      cartesian[3*j+2] = parametric[c2firstpIdx[j]+1];
    }
    else {
      cartesian[3*j]   = parametric[c2firstpIdx[j]];
      cartesian[3*j+1] = parametric[c2firstpIdx[j]+1];
    }
  }
}


/*
 * Indices mapping each parameter to the corresponding cartesian vertex
 */
void par2carIdx( const int Nver,
		 const int * const vFlags,
		 int * const p2cIdx )
{
  int i, counter = 0;

  for (i = 0; i < Nver; ++i) {
    if ( vFlags[i] == -1 )
      continue;
    else if ( vFlags[i] == 0 ) {
      p2cIdx[counter]   = i;
      p2cIdx[counter+1] = i;
      p2cIdx[counter+2] = i;
      counter += 3;
    }
    else if ( ( vFlags[i] >= 1 ) && ( vFlags[i] <= 12 ) ) {
      p2cIdx[counter] = i;
      ++counter;
    }
    else {
      p2cIdx[counter]   = i;
      p2cIdx[counter+1] = i;
      counter += 2;
    }
  }

}
