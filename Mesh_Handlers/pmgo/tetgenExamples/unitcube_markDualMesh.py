import numpy as np
import shutil

node  = np.loadtxt('unitcube.1_marked.dual.node', skiprows=1)
flags = -np.ones([node.shape[0],1])

# # Mark only internal nodes
# for i in range(node.shape[0]):
#     if ( ( node[i,0] > 0.0 ) & ( node[i,0] < 1.0 ) \
#          & ( node[i,1] > 0.0 ) & ( node[i,1] < 1.0 ) \
#          & ( node[i,2] > 0.0 ) & ( node[i,2] < 1.0 ) ):
#         flags[i] = 0

# Mark all nodes
for i in range(node.shape[0]):
    if abs(node[i,0]) < 1e-12:
        if abs(node[i,1]) < 1e-12:
            if abs(node[i,2]) < 1e-12:
                flags[i] = -1
            elif abs(1-node[i,2]) < 1e-12:
                flags[i] = -1
            else:
                flags[i] = 5
        elif abs(1-node[i,1]) < 1e-12:
            if abs(node[i,2]) < 1e-12:
                flags[i] = -1
            elif abs(1-node[i,2]) < 1e-12:
                flags[i] = -1
            else:
                flags[i] = 7
        else:
            if abs(node[i,2]) < 1e-12:
                flags[i] = 1
            elif abs(1-node[i,2]) < 1e-12:
                flags[i] = 9
            else:
                flags[i] = 13
    elif abs(1-node[i,0]) < 1e-12:
        if abs(node[i,1]) < 1e-12:
            if abs(node[i,2]) < 1e-12:
                flags[i] = -1
            elif abs(1-node[i,2]) < 1e-12:
                flags[i] = -1
            else:
                flags[i] = 6
        elif abs(1-node[i,1]) < 1e-12:
            if abs(node[i,2]) < 1e-12:
                flags[i] = -1
            elif abs(1-node[i,2]) < 1e-12:
                flags[i] = -1
            else:
                flags[i] = 8
        else:
            if abs(node[i,2]) < 1e-12:
                flags[i] = 2
            elif abs(1-node[i,2]) < 1e-12:
                flags[i] = 10
            else:
                flags[i] = 14
    else:
        if abs(node[i,1]) < 1e-12:
            if abs(node[i,2]) < 1e-12:
                flags[i] = 3
            elif abs(1-node[i,2]) < 1e-12:
                flags[i] = 11
            else:
                flags[i] = 15
        elif abs(1-node[i,1]) < 1e-12:
            if abs(node[i,2]) < 1e-12:
                flags[i] = 4
            elif abs(1-node[i,2]) < 1e-12:
                flags[i] = 12
            else:
                flags[i] = 16
        else:
            if abs(node[i,2]) < 1e-12:
                flags[i] = 17
            elif abs(1-node[i,2]) < 1e-12:
                flags[i] = 18
            else:
                flags[i] = 0

newnodes = np.zeros([node.shape[0],4])
newnodes[:,:3] = node
newnodes[:,-1] = flags[:].T

np.savetxt('unitcube.1_marked.dual_marked.node', newnodes, fmt='%.18e %.18e %.18e %d', header=str(node.shape[0]) + ' 1')

shutil.copy('unitcube.1_marked.dual.polyhedron', 'unitcube.1_marked.dual_marked.polyhedron')
shutil.copy('unitcube.1_marked.dual.face', 'unitcube.1_marked.dual_marked.face')
shutil.copy('unitcube.1_marked.dual.edge', 'unitcube.1_marked.dual_marked.edge')
shutil.copy('unitcube.1_marked.dual.v2p', 'unitcube.1_marked.dual_marked.v2p')
