function v2p = v2pConnectivity( mesh )

Nver   = size(mesh.Node, 1);
Nelt   = size(mesh.Element, 1);
v2p    = cell(Nver, 1);
v2pDim = zeros(Nver, 1);

for i = 1:Nelt
    v2pDim( mesh.Element{i} ) = v2pDim( mesh.Element{i} ) + 1;
end
v2pDimCopy = v2pDim;

% Second loop to allocate memory
for i = 1:Nver
    v2p{i} = zeros( 1, v2pDim(i) );
end

for i = 1:Nelt
    for j = mesh.Element{i}
        v2p{j}(v2pDim(j) - v2pDimCopy(j) + 1) = i;
    end
    v2pDimCopy(mesh.Element{i}) = v2pDimCopy(mesh.Element{i}) - 1;
end

% Check
if any(v2pDimCopy)
    error('Something is wrong')
end

end
