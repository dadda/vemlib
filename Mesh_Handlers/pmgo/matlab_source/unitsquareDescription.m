function [ outCoords, lb, ub ] = unitsquareDescription( inCoords, mesh, method )

if strcmp(method, 'par2car')

    outCoords = zeros(mesh.Nver, 2);
    counter   = 0;
    for i = 1:mesh.Nver
        switch mesh.vMarkers(i)
            case 0
                outCoords(i,:) = inCoords(counter+1:counter+2);
                counter        = counter + 2;
            case 1
                outCoords(i,:) = [ inCoords(counter+1) 0 ];
                counter        = counter + 1;
            case 2
                outCoords(i,:) = [ 1 inCoords(counter+1) ];
                counter        = counter + 1;           
            case 3
                outCoords(i,:) = [ inCoords(counter+1) 1 ];
                counter        = counter + 1;
            case 4
                outCoords(i,:) = [ 0 inCoords(counter+1) ];
                counter        = counter + 1;
            case 5
                outCoords(i,:) = mesh.coordinates(i,:);
        end
    end

    lb = zeros(mesh.Nver, 1);
    ub = ones(mesh.Nver, 1);

elseif strcmp(method, 'car2par')

    outCoords = zeros(mesh.Nver, 1);
    counter   = 0;
    for i = 1:mesh.Nver
        switch mesh.vMarkers(i)
            case 0
                outCoords(counter+1:counter+2) = inCoords(i,:)';
                counter                        = counter + 2;
            case 1
                outCoords(counter+1) = inCoords(i,1);
                counter              = counter + 1;
            case 2
                outCoords(counter+1) = inCoords(i,2);
                counter              = counter + 1;
            case 3
                outCoords(counter+1) = inCoords(i,1);
                counter              = counter + 1;
            case 4
                outCoords(counter+1) = inCoords(i,2);
                counter              = counter + 1;
        end
    end
    outCoords(counter+1:end) = [];

    numParams = length(outCoords);
    lb = zeros(numParams, 1);
    ub = ones(numParams, 1);

end

end
