%
% REMARK: This script only works for polyhedral domain. The primal mesh is
% assumed to be stored in TetGen format.
%

% basename = 'tetgenExamples/tet.1';
basename = 'tetgenExamples/unitcube.3';
% basename = 'tetgenExamples/octahedron.2';

% % Regular tetrahedron
% refVertices = [-0.5, 0, 0;
%                0.5, 0, 0;
%                0, 0.866, 0;
%                0, 0.2887, 0.8165];
% refFaces = [1, 3, 2;
%             1, 2, 4;
%             2, 3, 4;
%             4, 3, 1];
% refFaces = mat2cell( refFaces, ones(1,4), 3 );

% Unit cube
refVertices = [0 0 0;
               1 0 0;
               1 1 0;
               0 1 0;
               0 0 1;
               1 0 1;
               1 1 1;
               0 1 1];
refFaces = [ 1 4 3 2;
             5 6 7 8;
             1 2 6 5;
             4 8 7 3;
             1 5 8 4;
             2 3 7 6] ;
refFaces = mat2cell( refFaces, ones(1,6), 4 );
         
% % Octahedron
% refVertices = [1, 0, 0;
%                0, 1, 0;
%                -1, 0, 0;
%                0, -1, 0;
%                0, 0, 1;
%                0, 0, -1];
% refFaces = [ ...
%      1     2     5;
%      2     3     5;
%      3     4     5;
%      4     1     5;
%      1     6     2;
%      2     6     3;
%      3     6     4;
%      1     4     6 ];
% refFaces = mat2cell( refFaces, ones(1,8), 3 );

numRefFaces = size(refFaces,1);

% Vertices

fid  = fopen([basename '.node']);
fid2 = fopen([basename '_marked.node'], 'w');

tmp  = fscanf(fid, '%d', 4);
Nver = tmp(1);
vertices = zeros(Nver, 4);
hasMarker = tmp(4);

fprintf(fid2, '%d 3 0 1\n', Nver);
           
for i = 1:Nver
    tmp = fscanf(fid, '%f', [1,4+hasMarker]);

    % Assign correct flag:
    % - 0: internal node
    %
    % - 100-199: on the internal of a model face. Tens and ones
    % represent how many model faces emanate from the vertex (in
    % absence of internal boundary faces, we expect tens = 0, ones = 1.
    % It is clear that we allow 99 model faces at most.
    %
    % - 200-299: on the internal of a model edge. Meaning of tens
    % and ones is the same as above. In absence of internal boundary faces,
    % we expect tens = 0 and ones = 2.
    %
    % - 300-399: a model vertex. Meaning of tens and ones is
    % the same as above.

    % Assign flag
    flag = 0;
    test1 = zeros(numRefFaces,1);
    
    for f = 1:numRefFaces
        test1(f) = abs( orient3D( refVertices(refFaces{f}(1), :), ...
                                  refVertices(refFaces{f}(2), :), ...
                                  refVertices(refFaces{f}(3), :), tmp(2:4) ) );
        if test1(f) < 1e-8
            flag = flag + 1;
        end
    end
    
    flag = min(flag,3)*100 + flag;
    fprintf(fid2, '%d %g %g %g %d\n', tmp(1:4), flag);
    vertices(i,1:3) = tmp(2:4);
    vertices(i,4) = flag;
end

fclose(fid);
fclose(fid2);

% Check
test = sum( vertices(:,4) >= 300 );
if test ~= size(refVertices,1)
    error('Number of model vertices: %d. Found: %d', size(refVertices,1), test);
end

% Edges to vertices, edge markers, edge to element

fid  = fopen([basename '.edge']);
fid2 = fopen([basename '_marked.edge'], 'w');

tmp = fscanf(fid, '%d', 2);
Ned = tmp(1);

fprintf(fid2, '%d 1\n', Ned);

for i = 1:Ned
    tmp = fscanf(fid, '%d', [1,5]);
    edge = tmp(2:3);
    edgeAvg = sum( vertices(edge, 1:3), 1 ) / 2;

    % Assign correct flag:
    % - 0: internal edge
    %
    % - 1: on the internal of a model face.
    %
    % - 2: on the internal of a model edge.

    flag = 0;
    test1 = zeros(numRefFaces,1);

    for f = 1:numRefFaces
        test1(f) = abs( orient3D( refVertices(refFaces{f}(1), :), ...
                                  refVertices(refFaces{f}(2), :), ...
                                  refVertices(refFaces{f}(3), :), edgeAvg ) );
        if test1(f) < 1e-10
            flag = flag + 1;
        end
    end

    if flag > 2
        error('Something is wrong')
    end

    fprintf(fid2, '%d %d %d %d %d\n', tmp(1:3), flag, tmp(5));
end

fclose(fid);
fclose(fid2);

% Faces to vertices, face markers, face to element

fid  = fopen([basename '.face']);
fid2 = fopen([basename '_marked.face'], 'w');

tmp = fscanf(fid, '%d', 2);
Nfc = tmp(1);

fprintf(fid2, '%d 1\n', Nfc);

for i = 1:Nfc
    tmp = fscanf(fid, '%d', [1,7]);

    % Assign marker
    % - 0: internal face
    %
    % - 1: boundary face. Tens and ones represent the model face we
    % are on.

    flag = 0;
    if tmp(7) == -1
        flag = 1;
    end
    
    fprintf(fid2, '%d %d %d %d %d %d %d\n', tmp(1:4), flag, tmp([6,7]));
end

fclose(fid);
fclose(fid2);

eval(sprintf('!cp %s.ele %s_marked.ele', basename, basename));

figure()
hold on
test = vertices(:,4) >= 300;
plot3(vertices(test, 1), vertices(test, 2), vertices(test, 3), 'r*')
pause
test = vertices(:,4) < 300 & vertices(:,4) >= 200;
plot3(vertices(test, 1), vertices(test, 2), vertices(test, 3), 'g*')
pause
test = vertices(:,4) < 200 & vertices(:,4) >= 100;
plot3(vertices(test, 1), vertices(test, 2), vertices(test, 3), 'b*')
pause
test = vertices(:,4) == 0;
plot3(vertices(test, 1), vertices(test, 2), vertices(test, 3), 'c*')
pause
