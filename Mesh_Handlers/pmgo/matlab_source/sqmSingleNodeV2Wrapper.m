function out = sqmSingleNodeV2Wrapper( x, rescale, lBound, uBound, ...
                                       mesh, modelDescription, topFlag, geoFlag, ...
                                       nodeGlobIdx, v2p, coordinates )

if rescale
    x = x .* (uBound - lBound) + lBound;
end

% Convert from parametric to Cartesian coordinates
outCoords = modelDescription(topFlag, geoFlag, x, 2);

coordinates(nodeGlobIdx, :) = outCoords;

% Compute shape quality measure for a single node
out = sqmSingleNodeV2(coordinates, mesh, nodeGlobIdx, v2p);

end
