function circumcenter = tricircumcenter3d(a, b, c)

% /*****************************************************************************/
% /*                                                                           */
% /*  tricircumcenter3d()   Find the circumcenter of a triangle in 3D.         */
% /*                                                                           */
% /*  The result is returned both in terms of xyz coordinates and xi-eta       */
% /*  coordinates, relative to the triangle's point `a' (that is, `a' is       */
% /*  the origin of both coordinate systems).  Hence, the xyz coordinates      */
% /*  returned are NOT absolute; one must add the coordinates of `a' to        */
% /*  find the absolute coordinates of the circumcircle.  However, this means  */
% /*  that the result is frequently more accurate than would be possible if    */
% /*  absolute coordinates were returned, due to limited floating-point        */
% /*  precision.  In general, the circumradius can be computed much more       */
% /*  accurately.                                                              */
% /*                                                                           */
% /*  The xi-eta coordinate system is defined in terms of the triangle.        */
% /*  Point `a' is the origin of the coordinate system.  The edge `ab' extends */
% /*  one unit along the xi axis.  The edge `ac' extends one unit along the    */
% /*  eta axis.  These coordinate values are useful for linear interpolation.  */
% /*                                                                           */
% /*  If `xi' is NULL on input, the xi-eta coordinates will not be computed.   */
% /*                                                                           */
% /*****************************************************************************/


% /* Use coordinates relative to point `a' of the triangle. */
  xba = b(1) - a(1);
  yba = b(2) - a(2);
  zba = b(3) - a(3);
  xca = c(1) - a(1);
  yca = c(2) - a(2);
  zca = c(3) - a(3);
%  /* Squares of lengths of the edges incident to `a'. */
  balength = xba * xba + yba * yba + zba * zba;
  calength = xca * xca + yca * yca + zca * zca;
  
%  /* Cross product of these edges. */

%  /* Take your chances with floating-point roundoff. */
  xcrossbc = yba * zca - yca * zba;
  ycrossbc = zba * xca - zca * xba;
  zcrossbc = xba * yca - xca * yba;

%  /* Calculate the denominator of the formulae. */
  denominator = 0.5 / (xcrossbc * xcrossbc + ycrossbc * ycrossbc + ...
                       zcrossbc * zcrossbc);

%  /* Calculate offset (from `a') of circumcenter. */
  xcirca = ((balength * yca - calength * yba) * zcrossbc - ...
            (balength * zca - calength * zba) * ycrossbc) * denominator;
  ycirca = ((balength * zca - calength * zba) * xcrossbc - ...
            (balength * xca - calength * xba) * zcrossbc) * denominator;
  zcirca = ((balength * xca - calength * xba) * ycrossbc - ...
            (balength * yca - calength * yba) * xcrossbc) * denominator;
  circumcenter(1) = xcirca;
  circumcenter(2) = ycirca;
  circumcenter(3) = zcirca;

end
