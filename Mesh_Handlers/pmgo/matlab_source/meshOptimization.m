function newMeshNodes = meshOptimization( mesh, modelDescription, ...
                                          optType, ...
                                          globalObjFun, globalOptAlg, ...
                                          localObjFun )

switch(optType)
    
    case 'global'

        newMeshNodes = globalMeshOptimization( mesh, modelDescription, ...
                                               globalObjFun, [], globalOptAlg, [] );

    case 'local'

        newMeshNodes = localMeshOptimization( mesh, modelDescription, ...
                                              localObjFun );

end

end
