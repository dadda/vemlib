
% nameOrig = '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/voronoi-notnested/cantor/cantor_level_04.poly';
% basename = '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/voronoi-notnested/cantor/cantor_level_04_a1em3';
% basename = '/home/daniele/Documents/codes/ff++/myexamples/concus_gmsh_step05_refined_05.1';
% basename = '/home/daniele/Documents/codes/ff++/myexamples/catenoid_gmsh_step05_refined_05.1';
% basename = '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/voronoi-dual/unitsquare_07.1';

rootdir  = '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/unitcircle/dual';
basename = 'unitcircle';

% xmin = -0.8; xmax = 0.8;
% ymin = -0.8; ymax = 0.8;

constraintStr = {'0.01', '0.0025', '0.000625', '0.00015625', '0.0000390625', '0.000009765625', '0.00000244140625'};
% constraintStr = {'0.02', '0.005', '0.00125', '0.0003125', '0.000078125', '0.00001953125'};
% constraintStr = {'0.05', '0.0125', '0.003125', '0.00078125', '0.0001953125', '0.000048828125', '0.00001220703125'};
numMeshes     = numel(constraintStr);
% numMeshes = 7;

% % Required to mark meshes with Cantor nodes at the boundary
% % 
% % Number of boundary nodes manually generated. By default, triangle writes
% % these manually generated nodes before all the others
% numOrigNodes = 124;

tol = 1e-12;

for l = 1:numMeshes
    tmp      = replace(constraintStr{l}, '.', 'p');
    filename = sprintf('%s/%s_a%s.1', rootdir, basename, tmp);
%     filename = sprintf('%s/%s_%02i.1', rootdir, basename, l);
    fid      = fopen([filename '.node']);
    fid2     = fopen([filename '_marked.node'], 'w');

    tmp       = fscanf(fid, '%d', 4);
    Nver      = tmp(1);
    vertices  = zeros(Nver, 2);
    hasMarker = tmp(4);

    fprintf(fid2, '%d 2 0 1\n', Nver);

    for i = 1:Nver
        tmp           = fscanf(fid, '%f', [1,3+hasMarker]);
        vertices(i,:) = tmp(2:3);

        % Mark curved meshes
        if tmp(4) == 1
            flag = 2;
        else
            flag = 0;
        end

%         % Mark generic unit square mesh
%         if ( abs ( vertices(i,1) - xmin ) < tol || abs ( xmax - vertices(i,1) ) < tol )
%             if ( abs ( vertices(i,2) - ymin ) < tol || abs ( ymax - vertices(i,2) ) < tol )
%                 flag = 2;
%             else
%                 flag = 1;
%             end
%         else
%             if ( abs ( vertices(i,2) - ymin ) < tol || abs ( ymax - vertices(i,2) ) < tol )
%                 flag = 1;
%             else
%                 flag = 0;
%             end
%         end

%         % Mark Cantor meshes
%         if i <= numOrigNodes
%             flag = 2;
%         elseif ( abs ( vertices(i,1) ) < tol || abs ( 1-vertices(i,1) ) < tol ...
%                  || abs ( vertices(i,2) ) < tol || abs ( 1-vertices(i,2) ) < tol )
%             flag = 1;
%         else
%             flag = 0;
%         end
        fprintf(fid2, '%d %.16f %.16f %d\n', tmp(1:3), flag);
    end

    fclose(fid);
    fclose(fid2);

%     % Check for Cantor meshes
%     
%     % Load the original .poly file to check that the first nodes actually
%     % coincide with those manually created
%     verticesCheck = zeros(numOrigNodes,2);
%     
%     fid = fopen(nameOrig);
%     tmp = fscanf(fid, '%d', 4);
%     hasMarker = tmp(4);
%     for i = 1:numOrigNodes
%         tmp = fscanf(fid, '%f', [1,3+hasMarker]);
%         verticesCheck(i,:) = tmp(2:3);
%     end
%     fclose(fid);
%     
%     check = norm(vertices(1:numOrigNodes,:) - verticesCheck, 'fro');
%     if check > tol
%         error('Manually created nodes have not been stored as expected')
%     end

    eval(sprintf('!cp %s.edge %s_marked.edge', filename, filename));
    eval(sprintf('!cp %s.ele %s_marked.ele', filename, filename));
end



