function newNodes = localMeshOptimization2d( mesh )

v2p = v2pConnectivity(mesh);

lb = min(mesh.Node, [], 1); lb = lb(:);
ub = max(mesh.Node, [], 1); ub = ub(:);

opts         = cmaesDefaults();
opts.LBounds = 0;
opts.UBounds = 1;
insigma      = 1e-3;

tmpMesh  = loadmesh(mesh, 0);
vMarkers = zeros(tmpMesh.Nver, 1);
vMarkers(tmpMesh.B) = 1;

newNodes = tmpMesh.coordinates;
for i = 1:tmpMesh.Nver
    if vMarkers(i) == 0  % Optimize only internal nodes
        xstart = tmpMesh.coordinates(i,:)';
        xstart = (xstart - lb) ./ (ub - lb);

        [~, ~, ~, ~, ~, bestever] = ...
            cmaes( 'sqm2dlocal', xstart, insigma, opts, ...
                   lb, ub, i, v2p{i}, tmpMesh );

        newNodes(i,:) = bestever.x .* (ub - lb) + lb;
    end
end

end
