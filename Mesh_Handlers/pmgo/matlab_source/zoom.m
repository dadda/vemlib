function [ alphaStar, phiNew, counter ] = zoom( alphaLow, alphaHigh, ...
                                                phiAlphaLow, phiAlphaHigh, ...
                                                dphiAlphaLow, dphiAlphaHigh, ...
                                                tolAlpha, phi0, dphi0, ...
                                                objFun, x, p, opts, rescale, origLBounds, origUBounds, ...
                                                mesh, modelDescription, funOpt )

counter = 0;

while true
    if abs(alphaHigh - alphaLow) < tolAlpha
        alphaStar = alphaLow;
        phiNew    = phiAlphaLow;
        break;
    end
    
    counter = counter + 1;
    alpha = interpolate( alphaLow, alphaHigh, phiAlphaLow, phiAlphaHigh, ...
                         dphiAlphaLow, dphiAlphaHigh, opts.tolAlphaLow );
                     
    % Evaluate phi(alpha)
    xNew = x + alpha * p;
    if any( xNew < opts.LBounds ) || any( xNew > opts.UBounds )
        warning('This is unexpected')
    end
    phiNew = feval( objFun, xNew, rescale, origLBounds, origUBounds, mesh, modelDescription, funOpt );
    
    % Evaluate phi'(alpha) = \nabla f(x + alpha*p) \cdot p.
    dphiNew = directionalDerivative( objFun, xNew, p, rescale, origLBounds, origUBounds, ...
                                     mesh, modelDescription, funOpt, phiNew, opts );
    
    if ( phiNew > phi0 + opts.c1 * alpha * dphi0 ) || ( phiNew >= phiAlphaLow )
        alphaHigh     = alpha;
        phiAlphaHigh  = phiNew;
        dphiAlphaHigh = dphiNew;
    else
        if abs( dphiNew ) <= - opts.c2 * dphi0
            alphaStar = alpha;
            break;
        end
        
        if dphiNew * (alphaHigh - alphaLow) >= 0
            alphaHigh     = alphaLow;
            phiAlphaHigh  = phiAlphaLow;
            dphiAlphaHigh = dphiAlphaLow;
        end
        
        alphaLow     = alpha;
        phiAlphaLow  = phiNew;
        dphiAlphaLow = dphiNew;
    end
end

end
