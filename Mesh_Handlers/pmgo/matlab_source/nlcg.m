function x = nlcg( objFun, xstart, opts, ...
                   rescale, origLBounds, origUBounds, mesh, modelDescription, funOpt )

% Evaluate the gradient at the initial point
f0 = feval( objFun, xstart, rescale, origLBounds, origUBounds, mesh, modelDescription, funOpt );
g  = approxGradient( objFun, xstart, ...
                     rescale, origLBounds, origUBounds, mesh, modelDescription, funOpt, ...
                     f0, opts );
gNorm = norm(g, 2);

p = -g;

% Try full step length at the very beginning
alphaOld = 1;

x     = xstart;
phi0  = f0;
dphi0 = -gNorm^2;
gOld  = g;

iter  = 0;
while gNorm > opts.tolG && iter < opts.maxit
    fprintf('Iteration %i\n', iter);
    
    deltaAlpha = alphaOld;
    
    pNorm    = norm(p, 2);
    tolAlpha = opts.tolX / pNorm;
 
    % Compute step length
    [ alphaNew, phiNew ] = lineSearch( objFun, x, p, opts, rescale, origLBounds, origUBounds, ...
                                       mesh, modelDescription, funOpt, deltaAlpha, tolAlpha, ...
                                       phi0, dphi0 );
                       
    if alphaNew * pNorm <= opts.tolX
        break;
    end
    
    switch opts.bndMethod
        case 'bndTransform'
            x = boundary_transformation( x + alphaNew * p, ...
                                         opts.LBounds, opts.UBounds, opts.al, opts.au );
        case 'shrink'
            x = x + alphaNew * p;
            if any( x < opts.LBounds ) || any( x > opts.UBounds )
                warning('Unexpected x in main routine')
            end
    end
    
    % Compute the gradient
    g  = approxGradient( objFun, x, ...
                         rescale, origLBounds, origUBounds, mesh, modelDescription, funOpt, ...
                         phiNew, opts );

    beta = max( g' * (g - gOld) / gNorm^2, 0 );
    p    = -g + beta*p;
    
    phi0  = phiNew;
    dphi0 = dot(g, p);
    
    alphaOld = alphaNew;
    gOld     = g;
    gNorm    = norm(g, 2);
    iter     = iter + 1;
end

end
