function [ al, au ] = boundary_transformation_init( lb, ub )
% Transformation taken from CMA-ES code

al = min( (ub - lb)/2, (1 + abs(lb))/20 );
au = min( (ub - lb)/2, (1 + abs(ub))/20 );

end
