function newMeshNodes = globalMeshOptimization( mesh, modelDescription, ...
                                                globalObjFun, funOpt, ...
                                                globalOptAlg, opts )

%
% Construct lower and upper bounds, as well as initial solution.
%

topFlag = floor( mesh.verticesMarkers/100 );
geoFlag = mesh.verticesMarkers - topFlag*100;

xstart = cell(mesh.Nver,1);
lb     = cell(mesh.Nver,1);
ub     = cell(mesh.Nver,1);

for i = 1:mesh.Nver
    if topFlag(i) ~= 3
        [xstart{i}, lb{i}, ub{i}] = modelDescription(topFlag(i), geoFlag(i), mesh.coordinates(i,:), 1);
    end
end

xstart = vertcat( xstart{:} );
lb     = vertcat( lb{:} );
ub     = vertcat( ub{:} );

%
% Perform preliminary work
%

switch globalObjFun

    case 'refJac'

    case 'planarMetric'

        funOpt.v2p = vertex2polyhedra(mesh);
        
    case 'sqmGlobalWrapper'

end

%
% Run optimization algorithm
%

switch globalOptAlg

    case 'cmaes'

        % Covariance Matrix Adaptation Evolution Strategy
        
        xstart  = (xstart - lb) ./ (ub - lb);
        insigma = 1/3;

        if isempty(opts)
            opts = cmaesDefaults();
        end

        opts.LBounds = zeros( size(xstart) );
        opts.UBounds = ones( size(xstart) );

        rescale = true;
        
        % Run CMA-ES

        [~, ~, ~, ~, ~, bestever] = ...
            cmaes( globalObjFun, xstart, insigma, opts, ...
                   rescale, lb, ub, mesh, modelDescription, funOpt );

        newMeshNodes = parametric2cartesian( bestever.x, rescale, lb, ub, mesh, modelDescription );

    case 'nlcg'

        % Nonlinear conjugate gradient method

        if isempty(opts)
            opts = nlcgDefaults();
        end

        opts.LBounds = lb;
        opts.UBounds = ub;
        
        rescale = false;
        
        if strcmp(opts.bndMethod, 'bndTransform')
            [opts.al, opts.au] = boundary_transformation_init( opts.LBounds, opts.UBounds );
        end
        
        % Run the NLCG method

        best = nlcg( globalObjFun, xstart, opts, rescale, lb, ub, mesh, modelDescription, funOpt );
        
        newMeshNodes = parametric2cartesian( best, rescale, lb, ub, mesh, modelDescription );

end


end




