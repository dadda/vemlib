function circumcenter = tetcircumcenter(a, b, c, d)

% /*****************************************************************************/
% /*                                                                           */
% /*  tetcircumcenter()   Find the circumcenter of a tetrahedron.              */
% /*                                                                           */
% /*  The result is returned both in terms of xyz coordinates and xi-eta-zeta  */
% /*  coordinates, relative to the tetrahedron's point `a' (that is, `a' is    */
% /*  the origin of both coordinate systems).  Hence, the xyz coordinates      */
% /*  returned are NOT absolute; one must add the coordinates of `a' to        */
% /*  find the absolute coordinates of the circumcircle.  However, this means  */
% /*  that the result is frequently more accurate than would be possible if    */
% /*  absolute coordinates were returned, due to limited floating-point        */
% /*  precision.  In general, the circumradius can be computed much more       */
% /*  accurately.                                                              */
% /*                                                                           */
% /*  The xi-eta-zeta coordinate system is defined in terms of the             */
% /*  tetrahedron.  Point `a' is the origin of the coordinate system.          */
% /*  The edge `ab' extends one unit along the xi axis.  The edge `ac'         */
% /*  extends one unit along the eta axis.  The edge `ad' extends one unit     */
% /*  along the zeta axis.  These coordinate values are useful for linear      */
% /*  interpolation.                                                           */
% /*                                                                           */
% /*  If `xi' is NULL on input, the xi-eta-zeta coordinates will not be        */
% /*  computed.                                                                */
% /*                                                                           */
% /*****************************************************************************/

%  /* Use coordinates relative to point `a' of the tetrahedron. */
  xba = b(1) - a(1);
  yba = b(2) - a(2);
  zba = b(3) - a(3);
  xca = c(1) - a(1);
  yca = c(2) - a(2);
  zca = c(3) - a(3);
  xda = d(1) - a(1);
  yda = d(2) - a(2);
  zda = d(3) - a(3);
%  /* Squares of lengths of the edges incident to `a'. */
  balength = xba * xba + yba * yba + zba * zba;
  calength = xca * xca + yca * yca + zca * zca;
  dalength = xda * xda + yda * yda + zda * zda;
%  /* Cross products of these edges. */
  xcrosscd = yca * zda - yda * zca;
  ycrosscd = zca * xda - zda * xca;
  zcrosscd = xca * yda - xda * yca;
  xcrossdb = yda * zba - yba * zda;
  ycrossdb = zda * xba - zba * xda;
  zcrossdb = xda * yba - xba * yda;
  xcrossbc = yba * zca - yca * zba;
  ycrossbc = zba * xca - zca * xba;
  zcrossbc = xba * yca - xca * yba;

%  /* Calculate the denominator of the formulae. */

% /* Take your chances with floating-point roundoff. */
  denominator = 0.5 / (xba * xcrosscd + yba * ycrosscd + zba * zcrosscd);

%  /* Calculate offset (from `a') of circumcenter. */
  xcirca = (balength * xcrosscd + calength * xcrossdb + dalength * xcrossbc) * ...
           denominator;
  ycirca = (balength * ycrosscd + calength * ycrossdb + dalength * ycrossbc) * ...
           denominator;
  zcirca = (balength * zcrosscd + calength * zcrossdb + dalength * zcrossbc) * ...
           denominator;
  circumcenter(1) = xcirca;
  circumcenter(2) = ycirca;
  circumcenter(3) = zcirca;

end
