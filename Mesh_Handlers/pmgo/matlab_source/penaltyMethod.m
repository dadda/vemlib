function newMeshNodes = penaltyMethod( mesh, modelDescription, globalObjFun )

mu0     = 1;
tolGrad = 1e-8;
rho     = 2;
iter    = 0;
maxit   = 5;

funOpt.planarPenalty = true;
funOpt.penaltyParam  = mu0;

opts      = nlcgDefaults();
opts.tolG = tolGrad;
opts.tolX = tolGrad;

while iter < maxit

    newMeshNodes = globalMeshOptimization( mesh, modelDescription, ...
                                           globalObjFun, funOpt, ...
                                           'nlcg', opts );

    drawOVMmesh

    funOpt.penaltyParam = funOpt.penaltyParam * rho;
    opts.tolG = opts.tolG / rho;
    opts.tolX = opts.tolX / rho;

    mesh.coordinates = newMeshNodes;
	iter = iter + 1;
end

end
