function condF = condEscobarMod( e1, e2, e3, vol0 )

L = sqrt( sum( e1.^2 + e2.^2 + e3.^2 ) );
A = sqrt( sum( cross(e1, e2).^2 + cross(e2, e3).^2 + cross(e3, e1).^2 ) );
V = sum( cross( e1, e2 ) .* e3 );

% Parameter c particularly influences the final outcome. A higher value of
% c, usually leads to more planar faces.
c = 10;

delta = 1 / (1 + c*exp(V/vol0));
condF = (2 * A * L) / (V + sqrt(V^2 + delta^2));

% If the original metric proposed by Dyadecho et al. is used, then, as a
% further constraint, we have to guarantee that V > 0.
if isinf(c)
    if V < 1e-15
        condF = nan;
    end
end

end
