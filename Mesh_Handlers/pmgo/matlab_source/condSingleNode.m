function [ out, numTetCorners ] = condSingleNode( coordinates, nodeGlobIdx, element, elementCenter, vol0 )

out           = 0;
numTetCorners = 0;

Nfc = size( element, 1 );
for f = 1:Nfc
    tmp = find( element{f} == nodeGlobIdx );
    if ~isempty(tmp)
        numTetCorners = numTetCorners + 2;

        Ned = length( element{f} );
        faceCenter = sum( coordinates( element{f}, : ), 1 ) / Ned;

        iterator = [Ned 1:Ned 1];
        prec = iterator(tmp);
        next = iterator(tmp+2);
        
        V = coordinates( element{f}(tmp), : );

        % Contribution from tetrahedron prec-V-faceCenter-elementCenter
        e1  = coordinates( element{f}(prec), : ) - V;
        e2  = faceCenter - V;
        e3  = elementCenter - V;
        
%         e1 = e1 / norm(e1);
%         e2 = e2 / norm(e2);
%         e3 = e3 / norm(e3);
        out = out + condEscobarMod( e1, e2, e3, vol0 );

        % Contribution from tetrahedron V-next-faceCenter-elementCenter
        e1  = e2;
        e2  = coordinates( element{f}(next), : ) - V;
        
%         e1 = e1 / norm(e1);
%         e2 = e2 / norm(e2);
%         e3 = e3 / norm(e3);
        out = out + condEscobarMod( e1, e2, e3, vol0 );

    end
end

end
