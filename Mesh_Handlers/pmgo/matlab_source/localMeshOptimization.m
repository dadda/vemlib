function newMeshNodes = localMeshOptimization( mesh, modelDescription, localObjFun )


topFlag = floor( mesh.verticesMarkers/100 );
geoFlag = mesh.verticesMarkers - topFlag*100;

v2p = vertex2polyhedra( mesh );

%
% Use CMA-ES as local optimization algorithm
%

rescale = true;

insigma = 1/3;

opts = cmaesDefaults();
opts.LBounds = 0;
opts.UBounds = 1;

newMeshNodes = mesh.coordinates;
for i = [9 8 11]
    fprintf('Optimizing vertex %i ... ', i);
    
    % Optimize all but the vertices that coincide with a model vertex
    
    if topFlag(i) ~= 3
        
        [xstart, lb, ub] = modelDescription(topFlag(i), geoFlag(i), newMeshNodes(i,:), 1);
        xstart  = (xstart - lb) ./ (ub - lb);
        
        % Run CMA-ES

        [~, ~, ~, ~, ~, bestever] = ...
            cmaes( localObjFun, xstart, insigma, opts, ...
                   rescale, lb, ub, mesh, modelDescription, topFlag(i), geoFlag(i), ...
                   i, v2p{i}, newMeshNodes );

        newMeshNodes(i,:) = ...
            modelDescription(topFlag(i), geoFlag(i), bestever.x .* (ub - lb) + lb, 2);
    end

    fprintf('done\n');

end

end
