function [ alphaNew, deltaAlpha, alphaMax, lastFunEval, xTmp ] = ...
             stepLength( alphaOld, deltaAlpha, alphaMax, tolAlpha, ...
                         objFun, x, p, opts, ...
                         rescale, origLBounds, origUBounds, ...
                         mesh, modelDescription, funOpt )

alphaNew = alphaOld + deltaAlpha;

while deltaAlpha > tolAlpha && alphaNew >= alphaMax
    deltaAlpha = deltaAlpha * opts.rho;
    alphaNew   = alphaOld + deltaAlpha;
end

% We assume that x + alphaNew*p is not feasible if the objective functions
% evaluates to nan

switch opts.bndMethod
    case 'bndTransform'
        xTmp = boundary_transformation( x + alphaNew*p, opts.LBounds, opts.UBounds, ...
                                        opts.al, opts.au );
        lastFunEval = feval( objFun, xTmp, rescale, origLBounds, origUBounds, ...
                             mesh, modelDescription, funOpt );
    case 'shrink'
        xTmp = x + alphaNew*p;
        if any( xTmp < opts.LBounds ) || any( xTmp > opts.UBounds )
            lastFunEval = nan;
        else
            lastFunEval = feval( objFun, xTmp, rescale, origLBounds, origUBounds, ...
                                 mesh, modelDescription, funOpt );
        end
end

while deltaAlpha > tolAlpha && isnan(lastFunEval)
    if alphaNew < alphaMax
        alphaMax = alphaNew;
    end
    deltaAlpha = deltaAlpha * opts.rho;
    alphaNew   = alphaOld + deltaAlpha;

    switch opts.bndMethod
        case 'bndTransform'
            xTmp = boundary_transformation( x + alphaNew*p, opts.LBounds, opts.UBounds, ...
                                            opts.al, opts.au );
            lastFunEval = feval( objFun, xTmp, rescale, origLBounds, origUBounds, ...
                                 mesh, modelDescription, funOpt );
        case 'shrink'
            xTmp = x + alphaNew*p;
            if any( xTmp < opts.LBounds ) || any( xTmp > opts.UBounds )
                lastFunEval = nan;
            else
                lastFunEval = feval( objFun, xTmp, rescale, origLBounds, origUBounds, ...
                                     mesh, modelDescription, funOpt );
            end
    end

end

end
