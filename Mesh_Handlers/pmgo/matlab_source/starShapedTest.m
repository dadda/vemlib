


for K = 1:mesh.Nelt

    % Compute faces and element centers
    
    Nfc_K = length( mesh.element2hfaces{K} );
    
    element = construct_element_connectivity(mesh, K);
    uniqueVertices = unique( horzcat(element{:}) );
    Nver_K = length(uniqueVertices);
    
    elementCenter = sum( mesh.coordinates( uniqueVertices, : ), 1 ) / Nver_K;
    
    failedTest = false;
    
    for f = 1:Nfc_K
        Ned_f = length( element{f} );
        faceCenter = sum( mesh.coordinates( element{f}, : ), 1 ) / Ned_f;
        
        iterator = [2:Ned_f 1];
        negativeTetra = false;
        for e = 1:Ned_f
            e1 = faceCenter - mesh.coordinates( element{f}(e), : );
            e2 = mesh.coordinates( element{f}(iterator(e)), : ) - ...
                 mesh.coordinates( element{f}(e), : );
            e3 = elementCenter - mesh.coordinates( element{f}(e), : );
            
            test = ( e1(2)*e2(3) - e2(2)*e1(3) ) * e3(1) ...
                 + ( e1(3)*e2(1) - e2(3)*e1(1) ) * e3(2) ...
                 + ( e1(1)*e2(2) - e2(1)*e1(2) ) * e3(3);
             if test < 0
                 negativeTetra = true;
                 break;
             end
        end
        if negativeTetra
            failedTest = true;
            break;
        end
    end
    
    if failedTest
        warning('Element %i is not star-shaped\n', K);
    end
end