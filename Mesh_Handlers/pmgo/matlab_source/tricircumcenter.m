function [circumcenter, xi, eta] = tricircumcenter(a, b, c)

xba = b(1) - a(1);
yba = b(2) - a(2);
xca = c(1) - a(1);
yca = c(2) - a(2);

balength = xba * xba + yba * yba;
calength = xca * xca + yca * yca;

denominator = 0.5 / (xba * yca - yba * xca);

xcirca = (yca * balength - yba * calength) * denominator;  
ycirca = (xba * calength - xca * balength) * denominator;  
circumcenter(1) = xcirca;
circumcenter(2) = ycirca;

xi = (xcirca * yca - ycirca * xca) * (2.0 * denominator);
eta = (ycirca * xba - xcirca * yba) * (2.0 * denominator);

end
