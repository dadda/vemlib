function grad = approxGradient( objFun, x, ...
                                rescale, origLBounds, origUBounds, mesh, modelDescription, funOpt, ...
                                objFunAtX, opts )

dim   = length(x);
delta = sqrt( eps );

grad = zeros( dim, 1 );

switch opts.gradMethod
    
    case 'backward'

        switch opts.bndMethod

            case 'bndTransform'
                parfor i = 1:dim
                    xTmp    = x;
                    xTmp(i) = x(i) - delta;
                    xTmp    = boundary_transformation( xTmp, opts.LBounds, opts.UBounds, opts.al, opts.au );
                    fMinus  = feval( objFun, xTmp, ...
                                     rescale, origLBounds, origUBounds, mesh, modelDescription, funOpt );
                    grad(i) = ( objFunAtX - fMinus ) / delta;
                end

            case 'shrink'
                % Find delta that makes x - delta feasible in each
                % component
                xTmp       = x - delta;
                outsideBnd = any( xTmp < opts.LBounds ) || any( xTmp > opts.UBounds );
                iter       = 1;

                while outsideBnd && ( iter < opts.maxit4grad )
                    delta      = delta * opts.rho;
                    xTmp       = x - delta;
                    outsideBnd = any( xTmp < opts.LBounds ) || any( xTmp > opts.UBounds );
                    iter       = iter + 1;
                end
                if outsideBnd
                    warning('Cannot find delta for backward formula');
                end
                
                % Compute the gradient with the obtained delta
                parfor i = 1:dim
                    xTmp    = x;
                    xTmp(i) = x(i) - delta;
                    fMinus  = feval( objFun, xTmp, rescale, origLBounds, origUBounds, ...
                                     mesh, modelDescription, funOpt );
                    grad(i) = ( objFunAtX - fMinus ) / delta;
                end
        end

    case 'central'

        switch opts.bndMethod

            case 'bndTransform'
                parfor i = 1:dim
                    xTmp    = x;

                    xTmp(i) = x(i) - delta;
                    xTmp    = boundary_transformation( xTmp, opts.LBounds, opts.UBounds, opts.al, opts.au );
                    fMinus  = feval( objFun, xTmp, ...
                                     rescale, origLBounds, origUBounds, mesh, modelDescription, funOpt );

                    xTmp(i) = x(i) + delta;
                    xTmp    = boundary_transformation( xTmp, opts.LBounds, opts.UBounds, opts.al, opts.au );
                    fPlus   = feval( objFun, xTmp, ...
                                     rescale, origLBounds, origUBounds, mesh, modelDescription, funOpt );

                    grad(i) = (fPlus - fMinus) / (2 * delta);
                end

            case 'shrink'
                % Find delta that makes both x - delta and x + delta feasible in each
                % component
                xTmp1 = x - delta; xTmp2 = x + delta;
                outsideBnd = any( xTmp1 < opts.LBounds ) || any( xTmp1 > opts.UBounds ) || ...
                             any( xTmp2 < opts.LBounds ) || any( xTmp2 > opts.UBounds );
                iter = 1;

                while outsideBnd && ( iter < opts.maxit4grad )
                    delta = delta * opts.rho;
                    xTmp1 = x - delta; xTmp2 = x + delta;
                    outsideBnd = any( xTmp1 < opts.LBounds ) || any( xTmp1 > opts.UBounds ) || ...
                                 any( xTmp2 < opts.LBounds ) || any( xTmp2 > opts.UBounds );
                    iter = iter + 1;
                end
                if outsideBnd
                    warning('Cannot find delta for central formula');
                end

                parfor i = 1:dim
                    xTmp    = x;

                    xTmp(i) = x(i) - delta;
                    fMinus  = feval( objFun, xTmp, rescale, origLBounds, origUBounds, ...
                                     mesh, modelDescription, funOpt );

                    xTmp(i) = x(i) + delta;
                    fPlus   = feval( objFun, xTmp, rescale, origLBounds, origUBounds, ...
                                     mesh, modelDescription, funOpt );

                    grad(i) = (fPlus - fMinus) / (2 * delta);
                end
        end
        
end
