function out = sqm2dGlobalWrapper(x, rescale, lb, ub, mesh, modelDescription, varargin )

if rescale
    x = x .* (ub - lb) + lb;
end

vertices = modelDescription( x, mesh, 'par2car');
out      = sqm2dGlobal( vertices, mesh );

end
