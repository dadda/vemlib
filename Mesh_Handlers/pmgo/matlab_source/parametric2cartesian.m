function vertices = parametric2cartesian(x, rescale, lBound, uBound, mesh, modelDescription)

if rescale
    x = x .* (uBound - lBound) + lBound;
end

% Build coordinates array
vertices = zeros(mesh.Nver, 3);

% Keep track of the cumulative number of parametric coordinates to
% correctly read x
counter = 0;

topFlag = floor( mesh.verticesMarkers/100 );
geoFlag = mesh.verticesMarkers - topFlag*100;

for i = 1:mesh.Nver
    if topFlag(i) == 3
        % This node coincides with a model vertex and must not be moved
        vertices(i,:) = mesh.coordinates(i,:);
    else
        % Find number of parametric coordinates for the current node
        tmp = modelDescription(topFlag(i), geoFlag(i), mesh.coordinates(i,:), 1);
        numParams = length(tmp);
        
        % Convert parametric coordinates to Cartesian
        inCoords = x(counter+1:counter+numParams);
        tmp = modelDescription(topFlag(i), geoFlag(i), inCoords, 2);
        vertices(i,:) = tmp;
        
        counter = counter + numParams;
    end
end


end
