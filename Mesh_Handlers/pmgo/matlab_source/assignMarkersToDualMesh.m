%
% REMARK: This script only works for polyhedral domain. Currently we assume
% the correct OVM mesh has been created.
%

% % Regular tetrahedron
% refVertices = [-0.5, 0, 0;
%                0.5, 0, 0;
%                0, 0.866, 0;
%                0, 0.2887, 0.8165];
% refFaces = [1, 3, 2;
%             1, 2, 4;
%             2, 3, 4;
%             4, 3, 1];

% Unit cube
refVertices = [0 0 0;
               1 0 0;
               1 1 0;
               0 1 0;
               0 0 1;
               1 0 1;
               1 1 1;
               0 1 1];
refEdges = [1 2;
            2 3;
            4 3;
            1 4;
            1 5;
            2 6;
            3 7;
            4 8;
            5 6;
            6 7;
            8 7;
            5 8];
refFaces = [ 1 4 3 2;
             5 6 7 8;
             1 2 6 5;
             4 8 7 3;
             1 5 8 4;
             2 3 7 6] ;
         
% % Octahedron
% refVertices = [1, 0, 0;
%                0, 1, 0;
%                -1, 0, 0;
%                0, -1, 0;
%                0, 0, 1;
%                0, 0, -1];
% refFaces = [ ...
%      1     2     5;
%      2     3     5;
%      3     4     5;
%      4     1     5;
%      1     6     2;
%      2     6     3;
%      3     6     4;
%      1     4     6 ];

numRefEdges = size(refEdges,1);
numRefFaces = size(refFaces,1);

% Vertices
mesh.verticesMarkers = nan(mesh.Nver,1);

for i = 1:mesh.Nver

    % Assign correct flag:
    % - 0: internal node
    %
    % - 100-199: on the internal of a model face.
    % Tens and ones represent the model face the mesh node is on. It is
    % clear that we allow 99 model faces at most.
    %
    % - 200-299: on the internal of a model edge. Tens and ones represent the
    % model edge the node is on.
    %
    % - 300-399: a model vertex. Tens and ones represent the index of the model
    % vertex.

    % Assign topology flag
    topFlag = 0;
    test1 = zeros(numRefFaces,1);
    
    for f = 1:numRefFaces
        test1(f) = abs( orient3D( refVertices(refFaces(f,1), :), ...
                                  refVertices(refFaces(f,2), :), ...
                                  refVertices(refFaces(f,3), :), mesh.coordinates(i,:) ) );
        if test1(f) < 1e-8
            topFlag = topFlag + 1;
        end
    end

    % Assign geometry flag
    geoFlag = 0;
    if topFlag == 1
        % Find the model face the point is on
        [~, geoFlag] = min(test1);
    elseif topFlag == 2
        % Find the model edge the point is on
        
        test2 = zeros(numRefEdges,1);
        for e = 1:numRefEdges
            a0p  = mesh.coordinates(i,:) - refVertices( refEdges(e,1), : );
            a0a1 = refVertices( refEdges(e,2), : ) - refVertices( refEdges(e,1), : );
            t = (a0p * a0a1') / norm(a0a1);
            test2(e) = norm(a0p - t*a0a1);
        end

        [~, geoFlag] = min(test2);
    elseif topFlag == 3
        [~, geoFlag] = min( pdist2( mesh.coordinates(i,:), refVertices ) );
    end
    
    mesh.verticesMarkers(i) = min(topFlag,3)*100 + geoFlag;
end

% Check
test = sum( mesh.verticesMarkers >= 300 );
if test ~= size(refVertices,1)
    error('Number of model vertices: %d. Found: %d', size(refVertices,1), test);
end

% Edges
mesh.edgesMarkers = nan(mesh.Ned,1);

for i = 1:mesh.Ned
    edge = mesh.edge2vertices(i,:);
    edgeAvg = sum( mesh.coordinates(edge, 1:3), 1 ) / 2;

    % Assign correct flag:
    % - 0: internal edge
    %
    % - 100-199: on the internal of a model face.
    % Tens and ones represent the model face the edge is on.
    %
    % - 200-299: on the internal of a model edge. Tens and ones represent the
    % model edge the edge is on.

    topFlag = 0;
    test1 = zeros(numRefFaces,1);

    for f = 1:numRefFaces
        test1(f) = abs( orient3D( refVertices(refFaces(f,1), :), ...
                                  refVertices(refFaces(f,2), :), ...
                                  refVertices(refFaces(f,3), :), edgeAvg ) );
        if test1(f) < 1e-10
            topFlag = topFlag + 1;
        end
    end

    if topFlag > 2
        error('Something is wrong')
    end

    % Assign geometry flag
    geoFlag = 0;
    if topFlag == 1
        % Find the model face the edge is on
        [~, geoFlag] = min(test1);
    elseif topFlag == 2
        % Find the model edge the edge is on
        
        test2 = zeros(numRefEdges,1);
        for e = 1:numRefEdges
            a0p  = edgeAvg - refVertices( refEdges(e,1), : );
            a0a1 = refVertices( refEdges(e,2), : ) - refVertices( refEdges(e,1), : );
            t = (a0p * a0a1') / norm(a0a1);
            test2(e) = norm(a0p - t*a0a1);
        end

        [~, geoFlag] = min(test2);
    end

    mesh.edgesMarkers(i) = topFlag*100 + geoFlag;
end

% Faces
mesh.facesMarkers = nan(mesh.Nfc,1);

for i = 1:mesh.Nfc

    % Assign marker
    % - 0: internal face
    %
    % - 100-199: boundary face. Tens and ones represent the model face we
    % are on.
    
    if mesh.face2elements(i,2) == -1
        test1 = zeros(numRefFaces,1);

        Ned = length( mesh.face2hedges{i} );
        face = zeros(1,Ned);
        for l = 1:Ned
            hedge = mesh.face2hedges{i}(l);
            tedge = floor( (hedge+1)/2 );
            if mod(hedge,2)
                face(l) = mesh.edge2vertices(tedge,1);
            else
                face(l) = mesh.edge2vertices(tedge,2);
            end
        end

        faceCenter = sum( mesh.coordinates(face, :), 1 ) / Ned;
        for f = 1:numRefFaces
            test1(f) = abs( orient3D( refVertices(refFaces(f,1), :), ...
                                      refVertices(refFaces(f,2), :), ...
                                      refVertices(refFaces(f,3), :), faceCenter ) );
        end
        [~, flag] = min(test1);
        flag = 100 + flag;
    else
        flag = 0;
    end

    mesh.facesMarkers(i) = flag;
end

figure()
hold on
test = mesh.verticesMarkers >= 300;
plot3(mesh.coordinates(test, 1), mesh.coordinates(test, 2), mesh.coordinates(test, 3), 'r*')
pause
test = mesh.verticesMarkers < 300 & mesh.verticesMarkers >= 200;
plot3(mesh.coordinates(test, 1), mesh.coordinates(test, 2), mesh.coordinates(test, 3), 'g*')
pause
test = mesh.verticesMarkers < 200 & mesh.verticesMarkers >= 100;
plot3(mesh.coordinates(test, 1), mesh.coordinates(test, 2), mesh.coordinates(test, 3), 'b*')
pause
test = mesh.verticesMarkers == 0;
plot3(mesh.coordinates(test, 1), mesh.coordinates(test, 2), mesh.coordinates(test, 3), 'c*')
pause
