function opts = nlcgDefaults()

opts.tolG        = 1e-12;
opts.tolX        = 1e-12;
opts.tolAlphaLow = 0.1;
opts.rho         = 0.5;
opts.c1          = 1e-4;
opts.c2          = 0.1;
opts.maxit       = 1000;
opts.gradMethod  = 'backward'; % 'backward', 'central'
opts.maxit4grad  = 100;
opts.bndMethod   = 'shrink'; % 'bndTransform', 'shrink'

end
