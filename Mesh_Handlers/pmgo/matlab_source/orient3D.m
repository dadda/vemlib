function test = orient3D( a, b, c, d )

test = (a(1)-d(1)) * ( (b(2)-d(2))*(c(3)-d(3)) - (b(3)-d(3))*(c(2)-d(2)) ) ...
     - (a(2)-d(2)) * ( (b(1)-d(1))*(c(3)-d(3)) - (b(3)-d(3))*(c(1)-d(1)) )  ...
     + (a(3)-d(3)) * ( (b(1)-d(1))*(c(2)-d(2)) - (b(2)-d(2))*(c(1)-d(1)) );

end

