function coordinates = triBarycentricCoordinates3d(a, b, c, p)

  xba = b(1) - a(1);
  yba = b(2) - a(2);
  zba = b(3) - a(3);
  xca = c(1) - a(1);
  yca = c(2) - a(2);
  zca = c(3) - a(3);
  xpa = p(1) - a(1);
  ypa = p(2) - a(2);
  zpa = p(3) - a(3);
  
%  /* Cross product of these edges. */

%  /* Take your chances with floating-point roundoff. */
  xcrossbc = yba * zca - yca * zba;
  ycrossbc = zba * xca - zca * xba;
  zcrossbc = xba * yca - xca * yba;

  xcrossbp = yba * zpa - ypa * zba;
  ycrossbp = zba * xpa - zpa * xba;
  zcrossbp = xba * ypa - xpa * yba;

  xcrosspc = ypa * zca - yca * zpa;
  ycrosspc = zpa * xca - zca * xpa;
  zcrosspc = xpa * yca - xca * ypa;

  normCrossABC = sqrt(xcrossbc^2 + ycrossbc^2 + zcrossbc^2);
  normCrossABP = sqrt(xcrossbp^2 + ycrossbp^2 + zcrossbp^2);
  normCrossAPC = sqrt(xcrosspc^2 + ycrosspc^2 + zcrosspc^2);
  
  u = normCrossAPC / normCrossABC;
  v = normCrossABP / normCrossABC;
  w = 1-u-v;
  
  coordinates = [u,v,w];
  
end