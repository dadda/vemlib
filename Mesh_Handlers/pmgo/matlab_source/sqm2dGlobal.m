function out = sqm2dGlobal( vertices, mesh )

out = 0;

c = 10;

for i = 1:mesh.Nelt
    NverLoc     = mesh.elements(i,1);
    polySQM     = 0;
    iterator1   = [2:NverLoc 1];
%     iterator2   = [3:NverLoc 1 2];
    localCoords = vertices( mesh.elements(i,2:NverLoc+1), : );
    center      = sum( localCoords, 1 ) / NverLoc;
    xymin       = min(localCoords, [], 1);
    xymax       = max(localCoords, [], 1);
    A0          = ( xymax(2)-xymin(2) ) * ( xymax(1)-xymin(1) );
    for j = 1:NverLoc
        a = center - localCoords(j,:);
        b = localCoords(iterator1(j),:) - localCoords(j,:);

        A       = a(1)*b(2) - b(1)*a(2);
        L       = sqrt( a(1)^2 + a(2)^2 + b(1)^2 + b(2)^2 );
        delta   = 1 / (1 + c*exp(A/A0));
        polySQM = polySQM + L^2 / (A + sqrt(A^2 + delta));
    end
    out = out + polySQM / NverLoc;
end

out = out / mesh.Nelt;

end
