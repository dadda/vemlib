function [outCoords, lb, ub] = cubeDescription( topFlag, geoFlag, inCoords, convType )
% Description of the unit cube

% CONVTYPE = 1: from Cartesian to free parametric coordinates.
% CONVTYPE = 2: from parametric coordinates to Cartesian.

inCoords = inCoords(:);

switch topFlag
    case 0
        % Internal node

        outCoords = inCoords;
        if convType == 1
            lb = [0; 0; 0];
            ub = [1; 1; 1];
        end

    case 1
        % Node on model face
        
        switch geoFlag
            case 1
                % Face 1
                if convType == 1
                    outCoords = inCoords([1 2]);
                else
                    outCoords = [ inCoords; 0 ];
                end
            case 2
                % Face 2
                if convType == 1
                    outCoords = inCoords([1 2]);
                else
                    outCoords = [ inCoords; 1 ];
                end
            case 3
                % Face 3
                if convType == 1
                    outCoords = inCoords([1 3]);
                else
                    outCoords = [ inCoords(1); 0; inCoords(2) ];
                end
            case 4
                % Face 4
                if convType == 1
                    outCoords = inCoords([1 3]);
                else
                    outCoords = [ inCoords(1); 1; inCoords(2) ];
                end
            case 5
                % Face 5
                if convType == 1
                    outCoords = inCoords([2 3]);
                else
                    outCoords = [ 0; inCoords ];
                end
            case 6
                % Face 6
                if convType == 1
                    outCoords = inCoords([2 3]);
                else
                    outCoords = [ 1; inCoords ];
                end
        end
        
        if convType == 1
             lb = [0; 0];
             ub = [1; 1];
        end
        
    case 2
        % Node on model edge

        switch geoFlag
            case 1
                % Edge 1
                if convType == 1
                    outCoords = inCoords(1);
                else
                    outCoords = [ inCoords; 0; 0 ];
                end
            case 2
                % Edge 2
                if convType == 1
                    outCoords = inCoords(2);
                else
                    outCoords = [ 1; inCoords; 0 ];
                end
            case 3
                % Edge 3
                if convType == 1
                    outCoords = inCoords(1);
                else
                    outCoords = [ inCoords; 1; 0 ];
                end
            case 4
                % Edge 4
                if convType == 1
                    outCoords = inCoords(2);
                else
                    outCoords = [ 0; inCoords; 0 ];
                end
            case 5
                % Edge 5
                if convType == 1
                    outCoords = inCoords(3);
                else
                    outCoords = [ 0; 0; inCoords ];
                end
            case 6
                % Edge 6
                if convType == 1
                    outCoords = inCoords(3);
                else
                    outCoords = [ 1; 0; inCoords ];
                end
            case 7
                % Edge 7
                if convType == 1
                    outCoords = inCoords(3);
                else
                    outCoords = [ 1; 1; inCoords ];
                end
            case 8
                % Edge 8
                if convType == 1
                    outCoords = inCoords(3);
                else
                    outCoords = [ 0; 1; inCoords ];
                end
            case 9
                % Edge 9
                if convType == 1
                    outCoords = inCoords(1);
                else
                    outCoords = [ inCoords; 0; 1 ];
                end
            case 10
                % Edge 10
                if convType == 1
                    outCoords = inCoords(2);
                else
                    outCoords = [ 1; inCoords; 1 ];
                end
            case 11
                % Edge 11
                if convType == 1
                    outCoords = inCoords(1);
                else
                    outCoords = [ inCoords; 1; 1 ];
                end
            case 12
                % Edge 12
                if convType == 1
                    outCoords = inCoords(2);
                else
                    outCoords = [ 0; inCoords; 1 ];
                end
        end

        if convType == 1
             lb = 0;
             ub = 1;
        end
end

if convType == 2
    lb = [0; 0; 0];
    ub = [1; 1; 1];
end

end
