function y = boundary_transformation( x, lb, ub, al, au )

%
% Shift into feasible preimage
%

xlow = lb - 2*al - (ub-lb)/2;
xup  = ub + 2*au + (ub-lb)/2;
r    = xup - xlow;

y = x;

% Shift up
test    = y < xlow;
y(test) = y(test) + r(test) .* ( 1 + floor( (xlow(test) - y(test)) ./ r(test) ) );

% Shift down
test    = y > xup;
y(test) = y(test) - r(test) .* ( 1 + floor( (y(test) - xup(test)) ./ r(test) ) );

% Mirror
test    = y < (lb - al);
y(test) = y(test) + 2 * (lb(test) - al(test) - y(test));

test    = y > (ub + au);
y(test) = y(test) - 2 * (y(test) - ub(test) - au(test));

% Quadratic mapping
test    = y < (lb + al);
y(test) = lb(test) + ( y(test) - (lb(test) - al(test)) ).^2 ./ (4 * al(test));

test    = y > (ub - au);
y(test) = ub(test) - ( y(test) - (ub(test) + au(test)) ).^2 ./ (4 * au(test));

end





