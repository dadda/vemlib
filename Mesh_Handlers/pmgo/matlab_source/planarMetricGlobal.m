function out = planarMetricGlobal( vertices, mesh )

out = 0;

for K = 1:mesh.Nelt
    element = construct_element_connectivity( mesh, K );
    Nfc = size(element, 1);

    metric = 0;
    for f = 1:Nfc
        metric = metric + leastSquaredDistances( vertices(element{f}, :) );
    end
    
    out = out + metric / Nfc;
end

end
