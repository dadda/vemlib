function v2p = vertex2polyhedra( mesh )
% Construct vertices to polyhedra connectivity information

v2p    = cell(mesh.Nver, 1);
v2pDim = zeros(mesh.Nver, 1);

% Count how much memory we need
for K = 1:mesh.Nelt
    element = construct_element_connectivity(mesh, K);
    uniqueVertices = unique( horzcat(element{:}) );
    v2pDim( uniqueVertices ) = v2pDim( uniqueVertices ) + 1;
end
v2pDimCopy = v2pDim;

% Allocate memory
for i = 1:mesh.Nver
    v2p{i} = zeros(1, v2pDim(i));
end

for K = 1:mesh.Nelt
    element = construct_element_connectivity(mesh, K);
    uniqueVertices = unique( horzcat(element{:}) );
    for i = 1:length(uniqueVertices)
        v2p{ uniqueVertices(i) }( v2pDim(uniqueVertices(i)) - v2pDimCopy(uniqueVertices(i)) + 1 ) = K;
    end
    
    v2pDimCopy( uniqueVertices ) = v2pDimCopy( uniqueVertices ) - 1;
end

if any(v2pDimCopy)
    error('Something is wrong')
end

end
