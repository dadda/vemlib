function [t, s] = minDistBetweenLines(a0, a1, b0, b1)

a1a0 = a1 - a0;
b1b0 = b1 - b0;
a0b0 = a0 - b0;

A = sum( a1a0.^2 );
B = sum( a1a0 .* a0b0 );
C = sum( a1a0 .* b1b0 );
D = sum( a0b0 .* b1b0 );
E = sum( b1b0.^2 );

denom = A*E - C^2;

t = (D*C - B*E) / denom;
s = (D*A - B*C) / denom;

end