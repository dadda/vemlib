function out = sqmGlobal( vertices, mesh )

out = 0;

for i = 1:mesh.Nelt

    % The quality measure of a polyhedral element is the sum of the
    % condition numbers of the tetrahedral corners in the symmetric
    % decomposition of the polyhedron

    element = construct_element_connectivity( mesh, i );

    uniqueVertices = unique( horzcat( element{:} ) );
    Nver = length(uniqueVertices);

    elementCenter = sum( vertices( uniqueVertices, : ), 1 ) / Nver;

    eleSQM = 0;
    eleTetCorners = 0;
    coordMin = min( vertices( uniqueVertices, : ), [], 1 );
    coordMax = max( vertices( uniqueVertices, : ), [], 1 );
    vol0 = prod( coordMax - coordMin );
    for l = 1:Nver
        [ tmp, nodeTetCorners ] = condSingleNode( vertices, uniqueVertices(l), ...
                                                  element, elementCenter, vol0 );
        eleSQM = eleSQM + tmp;
        eleTetCorners = eleTetCorners + nodeTetCorners;
    end
    eleSQM = eleSQM / eleTetCorners;

    out = out + eleSQM;
end
out = out / mesh.Nelt;

end