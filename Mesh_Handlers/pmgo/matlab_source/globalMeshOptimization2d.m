function newNodes = globalMeshOptimization2d( mesh, modelDescription, ...
                                              globalObjFun, ...
                                              globalOptAlg, opts )


[xstart, lb, ub] = modelDescription( mesh.coordinates, mesh, 'car2par');

switch globalOptAlg

    case 'cmaes'

        xstart  = (xstart - lb) ./ (ub - lb);
        insigma = 0.001;

        if isempty(opts)
            opts = cmaesDefaults();
        end

        opts.LBounds = zeros( size(xstart) );
        opts.UBounds = ones( size(xstart) );

        rescale = true;

        % Run CMA-ES

        [~, ~, ~, ~, ~, bestever] = ...
            cmaes( globalObjFun, xstart, insigma, opts, ...
                   rescale, lb, ub, mesh, modelDescription );

        if rescale
            xopt = bestever.x .* (ub - lb) + lb;
        end
        newNodes = modelDescription( xopt, mesh, 'par2car');

    case 'nlcg'

        % Nonlinear conjugate gradient method

        if isempty(opts)
            opts = nlcgDefaults();
        end

        opts.LBounds = lb;
        opts.UBounds = ub;
        
        rescale = true;
        
        if strcmp(opts.bndMethod, 'bndTransform')
            [opts.al, opts.au] = boundary_transformation_init( opts.LBounds, opts.UBounds );
        end
        
        % Run the NLCG method

        best = nlcg( globalObjFun, xstart, opts, rescale, lb, ub, mesh, modelDescription, [] );

        if rescale
            xopt = best .* (ub - lb) + lb;
        end
        newNodes = modelDescription( xopt, mesh, 'par2car');
end

end
