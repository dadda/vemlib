function dphi = directionalDerivative( objFun, x, p, rescale, origLBounds, origUBounds, ...
                                       mesh, modelDescription, funOpt, phiAtX, opts )

% Compute the directional derivative by moving along a unit vector in the
% direction of p, and then multiply by the norm of p itself.

pNorm = norm(p, 2);
pUnit = p / pNorm;
delta = sqrt( eps );

switch opts.gradMethod

    case 'backward'
        
        switch opts.bndMethod
            case 'bndTransform'
                xMinus = boundary_transformation( x - delta * pUnit, ...
                                                  opts.LBounds, opts.UBounds, opts.al, opts.au );
                phiMinus = feval( objFun, xMinus, rescale, origLBounds, origUBounds, ...
                                  mesh, modelDescription, funOpt);
            case 'shrink'
                xMinus = x - delta * pUnit;
                outsideBnd = any( xMinus < opts.LBounds ) || any( xMinus > opts.UBounds );
                gradIter = 1;

                while outsideBnd && ( gradIter < opts.maxit4grad )
                    delta = delta * opts.rho;
                    xMinus = x - delta * pUnit;
                    outsideBnd = any( xMinus < opts.LBounds ) || any( xMinus > opts.UBounds );
                    gradIter = gradIter + 1;
                end
                if outsideBnd
                    warning('Cannot find delta for backward formula');
                end

                phiMinus = feval( objFun, xMinus, rescale, origLBounds, origUBounds, ...
                                  mesh, modelDescription, funOpt);
        end
        dphi  = ( (phiAtX - phiMinus) / delta ) * pNorm;

    case 'central'

        switch opts.bndMethod
            case 'bndTransform'
                xMinus   = boundary_transformation( x - delta * pUnit, ...
                                                    opts.LBounds, opts.UBounds, opts.al, opts.au );
                phiMinus = feval( objFun, xMinus, rescale, origLBounds, origUBounds, ...
                                  mesh, modelDescription, funOpt);
                xPlus    = boundary_transformation( x + delta * pUnit, ...
                                                    opts.LBounds, opts.UBounds, opts.al, opts.au );
                phiPlus  = feval( objFun, xPlus, rescale, origLBounds, origUBounds, ...
                                  mesh, modelDescription, funOpt);
            case 'shrink'
                xMinus = x - delta * pUnit; xPlus  = x + delta * pUnit;
                outsideBnd = any( xMinus < opts.LBounds ) || any( xMinus > opts.UBounds ) || ...
                             any( xPlus < opts.LBounds ) || any( xPlus > opts.UBounds );
                gradIter = 1;

                while outsideBnd && ( gradIter < opts.maxit4grad )
                    delta = delta * opts.rho;
                    xMinus = x - delta * pUnit; xPlus  = x + delta * pUnit;
                    outsideBnd = any( xMinus < opts.LBounds ) || any( xMinus > opts.UBounds ) || ...
                                 any( xPlus < opts.LBounds ) || any( xPlus > opts.UBounds );
                    gradIter = gradIter + 1;
                end
                if outsideBnd
                    warning('Cannot find delta for central formula');
                end

                phiMinus = feval( objFun, xMinus, rescale, origLBounds, origUBounds, ...
                                  mesh, modelDescription, funOpt);
                phiPlus  = feval( objFun, xPlus, rescale, origLBounds, origUBounds, ...
                                  mesh, modelDescription, funOpt);
        end
        dphi = ( ( phiPlus - phiMinus ) / (2 * delta) ) * pNorm;

end
    
end
