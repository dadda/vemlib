
basename = 'tetgenExamples/unitcube.1_marked';

%
% It is assumed that the primal mesh has already been generated and that
% boundary markers have ALREADY been set to classify mesh entities.
%
% BEWARE: internal boundaries are not supported currently.
%

% Vertices
fid  = fopen([basename '.node']);
tmp  = fscanf(fid, '%d', 4);
Nver = tmp(1);
vertices = zeros(Nver, 4);
for i = 1:Nver
    tmp = fscanf(fid, '%f', [1,5]);
    vertices(i,:) = tmp(2:5);
end
fclose(fid);

tolInTetraTest = 1e-10;
tolCollapsePt  = 1e-8; %min( pdist( vertices(:,1:3) ) ) / 100; % 2e-5;

% Edges to vertices, edge markers, edge to element

fid = fopen([basename '.edge']);
tmp = fscanf(fid, '%d', 2);
Ned = tmp(1);

edges = zeros(Ned, 4);
for i = 1:Ned
    tmp = fscanf(fid, '%d', [1,5]);
    edges(i,:) = tmp(2:5);
end

fclose(fid);

% Faces to vertices, face markers, face to element

fid = fopen([basename '.face']);
tmp = fscanf(fid, '%d', 2);
Nfc = tmp(1);

faces = zeros(Nfc, 6);
for i = 1:Nfc
    tmp = fscanf(fid, '%d', [1,7]);
    faces(i,:) = tmp(2:7);
end

fclose(fid);

% Element to vertices connectivity

fid  = fopen([basename '.ele']);
tmp  = fscanf(fid, '%d', 3);
Nelt = tmp(1);
if tmp(3) == 0
    eleAttr = 0;
else
    eleAttr = 1;
end

elements = zeros(Nelt, 4);
for i = 1:Nelt
    tmp = fscanf(fid, '%d', [1,5+eleAttr]);
    elements(i,:) = tmp(2:5);
end

fclose(fid);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create auxiliary connectivity information

sortedEdges = sort(edges(:,1:2), 2);
sortedEdges = unique(sortedEdges,'rows');

edges(:,1:2) = sort( edges(:,1:2), 2 );
edges = unique(edges, 'rows');

faces(:,1:3) = sort( faces(:,1:3), 2 );
faces = unique(faces, 'rows');

%
% Create vertices to edges connectivity information
%

v2e = cell(Nver,1);
v2eDim = zeros(Nver,1);

for i = 1:Ned
    v2eDim( edges(i,1) ) = v2eDim( edges(i,1) ) + 1;
    v2eDim( edges(i,2) ) = v2eDim( edges(i,2) ) + 1;
end
v2eDimCopy = v2eDim;

% Second loop to allocate memory
for i = 1:Nver
    v2e{i} = zeros( 1, v2eDim(i) );
end

for i = 1:Ned
    v2e{ edges(i,1) }(v2eDim(edges(i,1)) - v2eDimCopy(edges(i,1)) + 1) = i;
    v2e{ edges(i,2) }(v2eDim(edges(i,2)) - v2eDimCopy(edges(i,2)) + 1) = i;
    
    v2eDimCopy(edges(i,1:2)) = v2eDimCopy(edges(i,1:2)) - 1;
end

% Check
if any(v2eDimCopy)
    error('Something is wrong')
end

%
% Create vertices to face connectivity information
%

v2f = cell(Nver,1);
v2fDim = zeros(Nver,1);

for i = 1:Nfc
    v2fDim( faces(i,1) ) = v2fDim( faces(i,1) ) + 1;
    v2fDim( faces(i,2) ) = v2fDim( faces(i,2) ) + 1;
    v2fDim( faces(i,3) ) = v2fDim( faces(i,3) ) + 1;
end
v2fDimCopy = v2fDim;

% Second loop to allocate memory
for i = 1:Nver
    v2f{i} = zeros( 1, v2fDim(i) );
end

for i = 1:Nfc
    v2f{ faces(i,1) }(v2fDim(faces(i,1)) - v2fDimCopy(faces(i,1)) + 1) = i;
    v2f{ faces(i,2) }(v2fDim(faces(i,2)) - v2fDimCopy(faces(i,2)) + 1) = i;
    v2f{ faces(i,3) }(v2fDim(faces(i,3)) - v2fDimCopy(faces(i,3)) + 1) = i;
    
    v2fDimCopy(faces(i,1:3)) = v2fDimCopy(faces(i,1:3)) - 1;
end

% Check
if any(v2fDimCopy)
    error('Something is wrong')
end

%
% Create face to edge connectivity information
%

f2e = zeros(Nfc, 3);
fid = fopen([basename '.f2e']);
if fid == -1
    for i = 1:Nfc
        localEdges = [ faces(i,1) faces(i,2);
                       faces(i,2) faces(i,3);
                       faces(i,3) faces(i,1) ];
        sortedLocalEdges = sort( localEdges, 2 );
        for l = 1:3
            test = ( sortedLocalEdges(l,1) == sortedEdges(:,1) ) & ...
                   ( sortedLocalEdges(l,2) == sortedEdges(:,2) );
            f2e(i,l) = find(test);
        end
    end
else
    for i = 1:Nfc
        tmp = fscanf(fid, '%d', 4);
        f2e(i,:) = tmp(2:4);
    end
    fclose(fid);
end

%
% Create edge to face connectivity information
%

e2f    = cell(Ned,1);
e2fDim = zeros(Ned,1);

for i = 1:Nfc
    e2fDim( f2e(i,:) ) = e2fDim( f2e(i,:) ) + 1;
end
e2fDimCopy = e2fDim;

% Allocate memory
for i = 1:Ned
    e2f{i} = zeros( 1, e2fDim(i) );
end

for i = 1:Nfc
    e2f{ f2e(i,1) }( e2fDim(f2e(i,1)) - e2fDimCopy(f2e(i,1)) + 1 ) = i;
    e2f{ f2e(i,2) }( e2fDim(f2e(i,2)) - e2fDimCopy(f2e(i,2)) + 1 ) = i;
    e2f{ f2e(i,3) }( e2fDim(f2e(i,3)) - e2fDimCopy(f2e(i,3)) + 1 ) = i;
    
    e2fDimCopy( f2e(i,:) ) = e2fDimCopy( f2e(i,:) ) - 1;
end

% Check
if any(e2fDimCopy)
    error('Something is wrong')
end

%
% Create edge to tetrahedra connectivity information
%

e2t    = cell(Ned,1);
e2tDim = zeros(Ned,1);

fid = fopen([basename '.t2e']);
if fid == -1
    % First loop to estimate memory requirements
    for i = 1:Nelt
        localEdges = [ elements(i,1) elements(i,2);
                       elements(i,1) elements(i,3);
                       elements(i,1) elements(i,4);
                       elements(i,2) elements(i,3);
                       elements(i,2) elements(i,4);
                       elements(i,3) elements(i,4) ];
        sortedLocalEdges = sort( localEdges, 2 );
        for l = 1:6
            test = ( sortedLocalEdges(l,1) == sortedEdges(:,1) ) & ...
                  ( sortedLocalEdges(l,2) == sortedEdges(:,2) );
            e2tDim( test ) = e2tDim( test ) + 1;
        end
    end
    e2tDimCopy = e2tDim;
    
    % Second loop to allocate memory
    for i = 1:Ned
        e2t{i} = zeros( 1, e2tDim(i) );
    end
    
    % Third loop to construct connectivity information
    for i = 1:Nelt
        localEdges = [ elements(i,1) elements(i,2);
                       elements(i,1) elements(i,3);
                       elements(i,1) elements(i,4);
                       elements(i,2) elements(i,3);
                       elements(i,2) elements(i,4);
                       elements(i,3) elements(i,4) ];
        sortedLocalEdges = sort( localEdges, 2 );
        for l = 1:6
            test = ( sortedLocalEdges(l,1) == sortedEdges(:,1) ) & ...
                  ( sortedLocalEdges(l,2) == sortedEdges(:,2) );
            e2t{ test }(e2tDim(test) - e2tDimCopy(test) + 1) = i;
            e2tDimCopy(test) = e2tDimCopy(test) - 1;
        end
    end
    
    % Check
    if any(e2tDimCopy)
        error('Something is wrong')
    end
else
    for i = 1:Nelt
        tmp = fscanf(fid, '%d', 7);
        for l = 1:6
            e2tDim( tmp(1+l) ) = e2tDim( tmp(1+l) ) + 1;
        end
    end
    e2tDimCopy = e2tDim;
    fseek(fid,0,-1);
    
    % Second loop to allocate memory
    for i = 1:Ned
        e2t{i} = zeros( 1, e2tDim(i) );
    end

    for i = 1:Nelt
        tmp = fscanf(fid, '%d', 7);
        for l = 1:6
            e2t{ tmp(1+l) }(e2tDim(tmp(1+l)) - e2tDimCopy(tmp(1+l)) + 1) = i;
            e2tDimCopy(tmp(1+l)) = e2tDimCopy(tmp(1+l)) - 1;
        end
    end
    fclose(fid);
    
    % Check
    if any(e2tDimCopy)
        error('Something is wrong')
    end
end

%
% Construct tetrahedron to face connectivity information
%

t2f = zeros(Nelt, 4);

fid = fopen([basename '.t2f']);
if fid == -1
    sortedFaces = sort( faces(:,1:3), 2 );
    localFaces = [1 2 3;
                  1 2 4;
                  1 3 4;
                  2 3 4];
    for i = 1:Nelt
        for l = 1:4
            face = sort( elements(i, localFaces(l,:)) );
            test = ( sortedFaces(:,1) == face(1) ) & ...
                   ( sortedFaces(:,2) == face(2) ) & ...
                   ( sortedFaces(:,3) == face(3) );
            t2f(i,l) = find( test );
        end
    end
else
    for i = 1:Nelt
        tmp = fscanf(fid, '%d', 5);
        t2f(i,:) = tmp(2:5);
    end
    fclose(fid);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Dual mesh generation

numDualVertices = Nelt ...                   % Number of primal mesh elements (regions)
                  + sum(faces(:,4) == 1) ... % Number of primal mesh faces classified on the boundary
                  + sum(edges(:,3) == 2) ... % Number of primal mesh edges classified on a model edge
                  + sum(vertices(:,4) >= 300); % Number of primal mesh vertices classified on a model vertex

dualVertices = zeros(numDualVertices,3);
dualVerticesPointers = -ones(numDualVertices,1);
dualVerticesCounter  = 0;

%
% Step 1
%
% Create a dual vertex at a central point in each primal mesh region.
%

fid = fopen([basename '.v.node']);
if fid == -1
    warning('Voronoi node file does not exist. Computing circumcenters...')
    tr = triangulation(elements, vertices(:,1:3));
    cc = circumcenter(tr);
else
    cc = zeros(Nelt, 3);
    fscanf(fid, '%d', 4);
    for i = 1:Nelt
        tmp = fscanf(fid, '%f', 4);
        cc(i,:) = tmp(2:4);
    end
    fclose(fid);
end

% Check whether the circumcenter is inside the tetrahedron. If yes, add
% it to the list of dual vertices, otherwise compute a point within the
% tretrahedron as close to the circumcenter as possible on the line
% connecting the circumcenter and the centroid.

numWellCentered = 0;

for i = 1:Nelt
    
    % Check orientation of the vertices of the tetrahedron first
    centroid = sum( vertices( elements(i,:), 1:3 ), 1 ) / 4;
    test = orient3D( vertices(elements(i,1), 1:3), ...
                     vertices(elements(i,2), 1:3), ...
                     vertices(elements(i,3), 1:3), ...
                     centroid );

    if test > 0
        aIdx = [1,1,1,2];
        bIdx = [2,4,3,4];
        cIdx = [3,2,4,3];
    else
        aIdx = [1,1,1,2];
        bIdx = [2,3,4,3];
        cIdx = [4,2,3,4];
    end

    for l = 1:4
        test = orient3D( vertices(elements(i,aIdx(l)), 1:3), ...
                         vertices(elements(i,bIdx(l)), 1:3), ...
                         vertices(elements(i,cIdx(l)), 1:3), ...
                         cc(i,:) );
        if test < -tolInTetraTest
%            warning('Tetrahedron %i does not contain its own circumcenter (test %e); there will be polyhedra with curved faces', i, test)
            break
        end
    end

    if test >= -tolInTetraTest
        % Circumcenter is inside the tetrahedron or on the border
        
        if dualVerticesCounter == 0
            dualVerticesCounter = dualVerticesCounter + 1;
            
            dualVertices(dualVerticesCounter,:) = cc(i,:);
            dualVerticesPointers(i) = dualVerticesCounter;
        else
            % Check whether the point should be added or not
            [tmp, itmp] = min( pdist2( cc(i,:), dualVertices(1:dualVerticesCounter,:) ) );
            if tmp > tolCollapsePt
                dualVerticesCounter = dualVerticesCounter + 1;
                
                dualVertices(dualVerticesCounter,:) = cc(i,:);
                dualVerticesPointers(i) = dualVerticesCounter;
            else
                dualVerticesPointers(i) = itmp;
            end
        end
        
        numWellCentered = numWellCentered + 1;
    else
        if dualVerticesCounter == 0
            dualVerticesCounter = dualVerticesCounter + 1;
            
            dualVertices(dualVerticesCounter,:) = cc(i,:);
            dualVerticesPointers(i) = dualVerticesCounter;
        else
            % Check whether the point should be added or not
            [tmp, itmp] = min( pdist2( cc(i,:), dualVertices(1:dualVerticesCounter,:) ) );
            if tmp > tolCollapsePt
                dualVerticesCounter = dualVerticesCounter + 1;
                
                dualVertices(dualVerticesCounter,:) = cc(i,:);
                dualVerticesPointers(i) = dualVerticesCounter;
            else
                dualVerticesPointers(i) = itmp;
            end
        end
%         % Find the face intersecting the line connecting the centroid with
%         % the circumcenter
%         foundPlane = false;
%         for l = 1:4
%             A = [ centroid(1)-cc(i,1) ...
%                   vertices(elements(i,bIdx(l)),1)-vertices(elements(i,aIdx(l)),1) ...
%                   vertices(elements(i,cIdx(l)),1)-vertices(elements(i,aIdx(l)),1);
%                   centroid(2)-cc(i,2) ...
%                   vertices(elements(i,bIdx(l)),2)-vertices(elements(i,aIdx(l)),2) ...
%                   vertices(elements(i,cIdx(l)),2)-vertices(elements(i,aIdx(l)),2);
%                   centroid(3)-cc(i,3) ...
%                   vertices(elements(i,bIdx(l)),3)-vertices(elements(i,aIdx(l)),3) ...
%                   vertices(elements(i,cIdx(l)),3)-vertices(elements(i,aIdx(l)),3) ];
%             b = [ centroid(1)-vertices(elements(i,aIdx(l)),1);
%                   centroid(2)-vertices(elements(i,aIdx(l)),2);
%                   centroid(3)-vertices(elements(i,aIdx(l)),3) ];
%             x = A\b;
%             if all((x >= 0) & (x <= 1)) && (x(2)+x(3) <= 1)
%                 foundPlane = true;
%                 break
%             end
%         end
%         if foundPlane
%             % Make sure new point is inside tetrahedron
%             newPt = centroid + 0.99*x(1)*(cc(i,:)-centroid);
%             for m = 1:4
%                 test = orient3D( vertices(elements(i,aIdx(m)), 1:3), ...
%                                  vertices(elements(i,bIdx(m)), 1:3), ...
%                                  vertices(elements(i,cIdx(m)), 1:3), ...
%                                  newPt );
%                 if test < 0
%                     error('New point is outside the tetrahedron');
%                 end
%             end
%             
%             
%             if dualVerticesCounter == 0
%                 dualVerticesCounter = dualVerticesCounter + 1;
%                 
%                 dualVertices(dualVerticesCounter,:) = newPt;
%                 dualVerticesPointers(i) = dualVerticesCounter;
%             else
%                 % Check whether the point should be added or not
%                 [tmp, itmp] = min( pdist2( newPt, dualVertices(1:dualVerticesCounter,:) ) );
%                 if tmp > tolCollapsePt
%                     dualVerticesCounter = dualVerticesCounter + 1;
%                     
%                     dualVertices(dualVerticesCounter,:) = newPt;
%                     dualVerticesPointers(i) = dualVerticesCounter;
%                 else
%                     dualVerticesPointers(i) = itmp;
%                 end
%             end
%         else
%             error('Something is wrong');
%         end
    end
end

fprintf('%d tetrahedra out of %d are well-centered\n', numWellCentered, Nelt);

%
% Step 2
%
% Create a dual vertex at a central point in each primal mesh face
% classified on the boundary. The circumcenter of the face is used if
% possible.
%

faces2dualVertices = -ones(Nfc,1);

counter = Nelt;
numWellCentered = 0;
for i = 1:Nfc
    if faces(i,4) == 1
        counter = counter + 1;
        faces2dualVertices(i) = counter;
        
        tricircumcenter = tricircumcenter3d( vertices(faces(i,1),1:3), ...
                                          vertices(faces(i,2),1:3), ...
                                          vertices(faces(i,3),1:3) );
        % Circumcenter
        tricircumcenter = tricircumcenter + vertices(faces(i,1),1:3);
        
        % Check whether the circumcenter is inside the face
        barCoords = triBarycentricCoordinates3d( vertices(faces(i,1),1:3), ...
                                                 vertices(faces(i,2),1:3), ...
                                                 vertices(faces(i,3),1:3), ...
                                                 tricircumcenter );
        if all( barCoords >= 0 & barCoords <= 1 )
            numWellCentered = numWellCentered + 1;
            
            % Check whether the point should be added or not
            [tmp, itmp] = min( pdist2( tricircumcenter, dualVertices(1:dualVerticesCounter,:) ) );
            if tmp > tolCollapsePt
                dualVerticesCounter = dualVerticesCounter + 1;
                
                dualVertices(dualVerticesCounter,:) = tricircumcenter;
                dualVerticesPointers(counter) = dualVerticesCounter;
            else
                dualVerticesPointers(counter) = itmp;
            end

        else
            % Check whether the point should be added or not
            [tmp, itmp] = min( pdist2( tricircumcenter, dualVertices(1:dualVerticesCounter,:) ) );
            if tmp > tolCollapsePt
                dualVerticesCounter = dualVerticesCounter + 1;
                
                dualVertices(dualVerticesCounter,:) = tricircumcenter;
                dualVerticesPointers(counter) = dualVerticesCounter;
            else
                dualVerticesPointers(counter) = itmp;
            end
            
%             % Find a point within the face as close to the face
%             % circumcenter as possible on the line connecting the
%             % circumcenter and the centroid.
% 
%             % Due to roundoff errors, it is unlikely that the line
%             % connecting the centroid and the circumcenter intersects any
%             % of the face edges. Therefore, given the parametric equations
%             % of these segments, for each pair (edge)-(line cetroid to
%             % circumcenter), we compute the points on these lines that are
%             % closest to each other.
%             
%             centroid = sum( vertices(faces(i,1:3),1:3), 1 ) / 3;
%             
%             iterator = [2 3 1];
%             foundSomething = false;
%             for e = 1:3
%                 [t, s] = minDistBetweenLines( vertices(faces(i,e),1:3), ...
%                                               vertices(faces(i,iterator(e)),1:3), ...
%                                               centroid, tricircumcenter);
%                 if t >= 0 && s >= 0 && t <= 1 && s <= 1
%                     foundSomething = true;
%                     break;
%                 end
%             end
%             if foundSomething
%                 newPt = centroid + 0.99*s*(tricircumcenter-centroid);
%                 
%                 % Check whether the point should be added or not
%                 [tmp, itmp] = min( pdist2( newPt, dualVertices(1:dualVerticesCounter,:) ) );
%                 if tmp > tolCollapsePt
%                     dualVerticesCounter = dualVerticesCounter + 1;
%                     
%                     dualVertices(dualVerticesCounter,:) = newPt;
%                     dualVerticesPointers(counter) = dualVerticesCounter;
%                 else
%                     dualVerticesPointers(counter) = itmp;
%                 end
%             else
%                 warning('Using centroid on face (probably due to roundoff errors)')
%                 
%                 % Check whether the point should be added or not
%                 [tmp, itmp] = min( pdist2( centroid, dualVertices(1:dualVerticesCounter,:) ) );
%                 if tmp > tolCollapsePt
%                     dualVerticesCounter = dualVerticesCounter + 1;
%                     
%                     dualVertices(dualVerticesCounter,:) = centroid;
%                     dualVerticesPointers(counter) = dualVerticesCounter;
%                 else
%                     dualVerticesPointers(counter) = itmp;
%                 end
%             end
        end
    end
end

fprintf('%d boundary faces out of %d are well-centered\n', numWellCentered, counter-Nelt);

%
% Step 3
%
% Create a dual vertex at mid-point of each primal mesh edge classified on
% a model edge.
%

edges2dualVertices = -ones(Ned,1);

for i = 1:Ned
    if edges(i,3) == 2
        counter = counter + 1;
        edges2dualVertices(i) = counter;

        newPt = sum( vertices(edges(i,1:2),1:3), 1 ) / 2.;
        
        [tmp, itmp] = min( pdist2( newPt, dualVertices(1:dualVerticesCounter,:) ) );
        if tmp > tolCollapsePt
            dualVerticesCounter = dualVerticesCounter + 1;
            
            dualVertices(dualVerticesCounter,:) = newPt;
            dualVerticesPointers(counter) = dualVerticesCounter;
        else
            dualVerticesPointers(counter) = itmp;
        end
    end
end

%
% Step 4
%
% Create a dual vertex at each primal mesh vertex classified on a model
% vertex.
%

vertex2dualVertices = -ones(Nver,1);

for i = 1:Nver
    if vertices(i,4) >= 300
        counter = counter + 1;
        vertex2dualVertices(i) = counter;
        
        [tmp, itmp] = min( pdist2( vertices(i,1:3), dualVertices(1:dualVerticesCounter,:) ) );
        if tmp > tolCollapsePt
            dualVerticesCounter = dualVerticesCounter + 1;
            
            dualVertices(dualVerticesCounter,:) = vertices(i,1:3);
            dualVerticesPointers(counter) = dualVerticesCounter;
        else
            dualVerticesPointers(counter) = itmp;
        end
    end
end

% Trim dualVertices
dualVertices(dualVerticesCounter+1:end,:) = [];
numDualVertices = dualVerticesCounter;

numDualFaces = sum( edges(:,3) == 0 ) ...      % Number of edges classified in the domain interior
               + sum( edges(:,3) == 1 ) ...    % Number of edges classified in the interior of a model face
               + sum( edges(:,3) == 2 ) ...    % Number of edges classified on a model edge
               + sum( vertices(:,4) >= 100 & vertices(:,4) < 200 ) ... % Number of vertices classified on the internal of a model face
               + sum( ( vertices(:,4) >= 200 & vertices(:,4) < 300 ) .* (vertices(:,4)-200) ) ...
               + sum( ( vertices(:,4) >= 300 ) .* (vertices(:,4)-300) );
               

% figure()
% hold on
% for K = 1:Nelt
%     plot3(vertices(elements(K,[1,2]),1), vertices(elements(K,[1,2]),2), vertices(elements(K,[1,2]),3), 'g')
%     plot3(vertices(elements(K,[1,3]),1), vertices(elements(K,[1,3]),2), vertices(elements(K,[1,3]),3), 'g')
%     plot3(vertices(elements(K,[1,4]),1), vertices(elements(K,[1,4]),2), vertices(elements(K,[1,4]),3), 'g')
%     plot3(vertices(elements(K,[2,4]),1), vertices(elements(K,[2,4]),2), vertices(elements(K,[2,4]),3), 'g')
%     plot3(vertices(elements(K,[3,4]),1), vertices(elements(K,[3,4]),2), vertices(elements(K,[3,4]),3), 'g')
%     plot3(vertices(elements(K,[3,2]),1), vertices(elements(K,[3,2]),2), vertices(elements(K,[3,2]),3), 'g')
% end
% plot3(dualVertices(:,1), dualVertices(:,2), dualVertices(:,3), 'r*')

%
% Steps 5 and 6
%
% Create dual faces corresponding to primal edges.
%

edges2dualFace = -ones(Ned,1);

dualEdge2dualVertices = [];
dualEdgeCounter = 0;

dualFace2hDualEdges = cell(numDualFaces,1);
dualFaceCounter = 0;

for i = 1:Ned
    if edges(i,3) == 0
%         fprintf('Creating a face from an interior primal edge...');
%         plot3(vertices(edges(i,[1,2]),1), vertices(edges(i,[1,2]),2), vertices(edges(i,[1,2]),3), 'r')
        
        % Create a single dual face. Note that indices of dual vertices
        % correspond to the indices of the containing tetrahedra
        
        hDualFace = zeros(1, e2tDim(i));
        
        prevFace = nan;
        elt2 = nan;
        
        for m = 1:e2tDim(i)
            
            if m == 1
                elt1 = e2t{i}(1);
            else
                elt1 = elt2;
            end
            
            tmp = intersect( t2f(elt1,:), e2f{i} );
            if length(tmp) ~= 2
                error('Something is wrong')
            else
                if m == 1
                    f1 = tmp(1); % Arbitrary choice
                else
                    % Ensure connected tetrahedra are traversed
                    % sequentially in a single direction
                    
                    if prevFace == tmp(1)
                        f1 = tmp(2);
                    else
                        f1 = tmp(1);
                    end
                end
            end

            % Move to the neighboring tetrahedron
            if faces(f1, 5) == elt1
                elt2 = faces(f1, 6);
            else
                elt2 = faces(f1, 5);
            end

            mappedVertex1 = dualVerticesPointers(elt1);
            mappedVertex2 = dualVerticesPointers(elt2);
            
            if mappedVertex1 ~= mappedVertex2
                % The edge is not collapsing
            
                % Sort edge vertices
                if mappedVertex1 < mappedVertex2
                    sEdge = [mappedVertex1 mappedVertex2];
                else
                    sEdge = [mappedVertex2 mappedVertex1];
                end

%                 plot3(dualVertices(sEdge,1), dualVertices(sEdge,2), dualVertices(sEdge,3), 'b-')

                % Check whether elt1-elt2 has already been inserted into the list
                % of dual edges
                test = false;
                if ~isempty( dualEdge2dualVertices )
                    test = ( sEdge(1) == dualEdge2dualVertices(:,1) ) & ...
                           ( sEdge(2) == dualEdge2dualVertices(:,2) );
                end
                if ~any( test )
                    % Create a new dual edge
                    dualEdgeCounter = dualEdgeCounter + 1;

                    dualEdge2dualVertices = [ dualEdge2dualVertices;
                                              sEdge ]; %#ok<AGROW>

                    if mappedVertex1 < mappedVertex2
                        % Assign odd half edge
                        hDualFace(m) = 2*dualEdgeCounter-1;
                    else
                        % Assign even half edge
                        hDualFace(m) = 2*dualEdgeCounter;
                    end
                else
                    oldDualEdge = find( test );
                    if mappedVertex1 < mappedVertex2
                        % Assign odd half edge
                        hDualFace(m) = 2*oldDualEdge-1;
                    else
                        % Assign even half edge
                        hDualFace(m) = 2*oldDualEdge;
                    end
                end
            end
            
            prevFace = f1;
        end
        
        % Check that we returned back to the starting tetrahedron
        if e2t{i}(1) ~= elt2
            error('We did not come back to the starting tetrahedron')
        end

        hDualFace(hDualFace == 0) = [];
        if numel(hDualFace) >= 3
            dualFaceCounter = dualFaceCounter + 1;
            edges2dualFace(i) = dualFaceCounter;
            
            dualFace2hDualEdges{dualFaceCounter} = hDualFace;
%             fprintf('face created\n');
        else
%             fprintf('degenerate face. Nothing was created\n');
        end
        
    elseif edges(i,3) == 1
%         fprintf('Creating a face from a primal edge on the interior of a model face...');
%         plot3(vertices(edges(i,[1,2]),1), vertices(edges(i,[1,2]),2), vertices(edges(i,[1,2]),3), 'r')

        % Create a single dual face
        hDualFace = zeros(1, e2tDim(i)+2);

        % Find boundary faces in the primal mesh connected to the edge
        test = faces( e2f{i}, 6 ) == -1; 
        tmp  = e2f{i}(test);
        if length(tmp) ~= 2
            error('Something is wrong')
        end

        firstFace = tmp(1); % Arbitrary choice
        lastFace  = tmp(2);
        
        dualVertex2 = nan;
        prevFace = firstFace;
        
        for m = 1:e2tDim(i)+2

            if m == 1
                dualVertex1 = faces2dualVertices(firstFace);
                dualVertex2 = faces(firstFace, 5);
            elseif m < e2tDim(i)+1
                dualVertex1 = dualVertex2;
                
                tmp = intersect( t2f(dualVertex1,:), e2f{i} );
                if length(tmp) ~= 2
                    error('Something is wrong')
                else
                    if prevFace == tmp(1)
                        f1 = tmp(2);
                    else
                        f1 = tmp(1);
                    end
                    if faces(f1,5) == dualVertex1
                        dualVertex2 = faces(f1, 6);
                    else
                        dualVertex2 = faces(f1, 5);
                    end
                    prevFace = f1;
                end
            elseif m == e2tDim(i)+1
                dualVertex1 = dualVertex2;
                dualVertex2 = faces2dualVertices(lastFace);
            else
                dualVertex1 = dualVertex2;
                dualVertex2 = faces2dualVertices(firstFace);
            end

            mappedVertex1 = dualVerticesPointers(dualVertex1);
            mappedVertex2 = dualVerticesPointers(dualVertex2);
            
            if mappedVertex1 ~= mappedVertex2
                % The edge is not collapsing

                % Sort edge vertices
                if mappedVertex1 < mappedVertex2
                    sEdge = [mappedVertex1 mappedVertex2];
                else
                    sEdge = [mappedVertex2 mappedVertex1];
                end

%                 plot3(dualVertices(sEdge,1), dualVertices(sEdge,2), dualVertices(sEdge,3), 'b-')

                test = false;
                if ~isempty( dualEdge2dualVertices )
                    test = ( sEdge(1) == dualEdge2dualVertices(:,1) ) & ...
                           ( sEdge(2) == dualEdge2dualVertices(:,2) );
                end
                if ~any( test )
                    % Create a new dual edge
                    dualEdgeCounter = dualEdgeCounter + 1;

                    dualEdge2dualVertices = [ dualEdge2dualVertices;
                                              sEdge ]; %#ok<AGROW>

                    if mappedVertex1 < mappedVertex2
                        % Assign odd half edge
                        hDualFace(m) = 2*dualEdgeCounter-1;
                    else
                        % Assign even half edge
                        hDualFace(m) = 2*dualEdgeCounter;
                    end
                else
                    oldDualEdge = find( test );
                    if mappedVertex1 < mappedVertex2
                        % Assign odd half edge
                        hDualFace(m) = 2*oldDualEdge-1;
                    else
                        % Assign even half edge
                        hDualFace(m) = 2*oldDualEdge;
                    end
                end
            end
        end

        hDualFace(hDualFace == 0) = [];
        if numel(hDualFace) >= 3
            dualFaceCounter = dualFaceCounter + 1;
            edges2dualFace(i) = dualFaceCounter;
            
            dualFace2hDualEdges{dualFaceCounter} = hDualFace;
%             fprintf('face created\n');
        else
%             fprintf('degenerate face. Nothing was created\n');
        end
        
    elseif edges(i,3) == 2
%         fprintf('Creating a face from a primal edge on a model edge...');
%         plot3(vertices(edges(i,[1,2]),1), vertices(edges(i,[1,2]),2), vertices(edges(i,[1,2]),3), 'r')

        % Create a single dual face
        hDualFace = zeros(1, e2tDim(i)+3);
        
        % Find boundary faces in the primal mesh connected to the edge
        test = faces( e2f{i}, 6 ) == -1; 
        tmp  = e2f{i}(test);
        if length(tmp) ~= 2
            error('Something is wrong')
        end

        firstFace  = tmp(1); % Arbitrary choice
        secondFace = tmp(2);

        dualVertex2 = nan;
        prevFace = firstFace;
        
        for m = 1:e2tDim(i)+3
            
            if m == 1
                dualVertex1 = edges2dualVertices(i);
                dualVertex2 = faces2dualVertices(firstFace);
            elseif m == 2
                dualVertex1 = dualVertex2;
                dualVertex2 = faces(firstFace, 5);
            elseif m < e2tDim(i)+2
                dualVertex1 = dualVertex2;

                tmp = intersect( t2f(dualVertex1,:), e2f{i} );
                if length(tmp) ~= 2
                    error('Something is wrong')
                else
                    if prevFace == tmp(1)
                        f1 = tmp(2);
                    else
                        f1 = tmp(1);
                    end
                    if faces(f1,5) == dualVertex1
                        dualVertex2 = faces(f1, 6);
                    else
                        dualVertex2 = faces(f1, 5);
                    end
                    prevFace = f1;
                end
            elseif m == e2tDim(i)+2
                dualVertex1 = dualVertex2;
                dualVertex2 = faces2dualVertices(secondFace);
            else
                dualVertex1 = dualVertex2;
                dualVertex2 = edges2dualVertices(i);
            end

            mappedVertex1 = dualVerticesPointers(dualVertex1);
            mappedVertex2 = dualVerticesPointers(dualVertex2);
            
            if mappedVertex1 ~= mappedVertex2
                % The edge is not collapsing

                % Sort edge vertices
                if mappedVertex1 < mappedVertex2
                    sEdge = [mappedVertex1 mappedVertex2];
                else
                    sEdge = [mappedVertex2 mappedVertex1];
                end

%                 plot3(dualVertices(sEdge,1), dualVertices(sEdge,2), dualVertices(sEdge,3), 'b-')

                test = false;
                if ~isempty( dualEdge2dualVertices )
                    test = ( sEdge(1) == dualEdge2dualVertices(:,1) ) & ...
                           ( sEdge(2) == dualEdge2dualVertices(:,2) );
                end
                if ~any( test )
                    % Create a new dual edge
                    dualEdgeCounter = dualEdgeCounter + 1;

                    dualEdge2dualVertices = [ dualEdge2dualVertices;
                                              sEdge ]; %#ok<AGROW>

                    if mappedVertex1 < mappedVertex2
                        % Assign odd half edge
                        hDualFace(m) = 2*dualEdgeCounter-1;
                    else
                        % Assign even half edge
                        hDualFace(m) = 2*dualEdgeCounter;
                    end
                else
                    oldDualEdge = find( test );
                    if mappedVertex1 < mappedVertex2
                        % Assign odd half edge
                        hDualFace(m) = 2*oldDualEdge-1;
                    else
                        % Assign even half edge
                        hDualFace(m) = 2*oldDualEdge;
                    end
                end    
            end
        end
        
        hDualFace(hDualFace == 0) = [];
        if numel(hDualFace) >= 3
            dualFaceCounter = dualFaceCounter + 1;
            edges2dualFace(i) = dualFaceCounter;
            
            dualFace2hDualEdges{dualFaceCounter} = hDualFace;
%             fprintf('face created\n');
        else
%             fprintf('degenerate face. Nothing was created\n');
        end

    end
end

%
% Step 7
%
% Create boundary dual faces corresponding to primal boundary vertices.
%

vertex2dualBndFaces = cell(Nver,1);

for i = 1:Nver
    if (vertices(i,4) >= 100) && (vertices(i,4) < 200)
%         fprintf('Creating a face from a primal vertex on a model face...\n');
%         plot3(vertices(i,1), vertices(i,2), vertices(i,3), 'r*')
        
        % Create a single dual face        
        test = faces( v2f{i}, 6 ) == -1;
        dualFaceSize = sum( test );
        bndPrimalFaces = v2f{i}( test );
        
        hDualFace = zeros(1, dualFaceSize);
        prevEdge = nan;
        dualVertex2 = nan;
        
        for m = 1:dualFaceSize
            if m == 1
                dualVertex1 = faces2dualVertices( bndPrimalFaces(1) );
                currentFace = bndPrimalFaces(1);
            else
                dualVertex1 = dualVertex2;
            end
            
            tmp = intersect( f2e(currentFace,1:3), v2e{i} );
            if length(tmp) ~= 2
                error('Something is wrong')
            else
                if prevEdge == tmp(1)
                    currentEdge = tmp(2);
                else
                    currentEdge = tmp(1);
                end
                test = ( faces(e2f{currentEdge}, 6)' == -1 ) & ...
                       ( e2f{currentEdge} ~= currentFace );
                currentFace = e2f{currentEdge}( test );
                
                dualVertex2 = faces2dualVertices( currentFace );
                
                if m == dualFaceSize && dualVertex2 ~= faces2dualVertices( bndPrimalFaces(1) )
                    error('Something is wrong')
                end
                prevEdge = currentEdge;
            end
            
            mappedVertex1 = dualVerticesPointers(dualVertex1);
            mappedVertex2 = dualVerticesPointers(dualVertex2);
            
            if mappedVertex1 ~= mappedVertex2
                % The edge is not collapsing

                % Sort edge vertices
                if mappedVertex1 < mappedVertex2
                    sEdge = [mappedVertex1 mappedVertex2];
                else
                    sEdge = [mappedVertex2 mappedVertex1];
                end
                
%                 plot3(dualVertices(sEdge,1), dualVertices(sEdge,2), dualVertices(sEdge,3), 'b-')
                
                test = false;
                if ~isempty( dualEdge2dualVertices )
                    test = ( sEdge(1) == dualEdge2dualVertices(:,1) ) & ...
                           ( sEdge(2) == dualEdge2dualVertices(:,2) );
                end
                if ~any( test )
                    % Create a new dual edge
                    dualEdgeCounter = dualEdgeCounter + 1;

                    dualEdge2dualVertices = [ dualEdge2dualVertices;
                                              sEdge ]; %#ok<AGROW>

                    if mappedVertex1 < mappedVertex2
                        % Assign odd half edge
                        hDualFace(m) = 2*dualEdgeCounter-1;
                    else
                        % Assign even half edge
                        hDualFace(m) = 2*dualEdgeCounter;
                    end
                else
                    oldDualEdge = find( test );
                    if mappedVertex1 < mappedVertex2
                        % Assign odd half edge
                        hDualFace(m) = 2*oldDualEdge-1;
                    else
                        % Assign even half edge
                        hDualFace(m) = 2*oldDualEdge;
                    end
                end
            end
        end

        hDualFace(hDualFace == 0) = [];
        if numel(hDualFace) >= 3
            dualFaceCounter = dualFaceCounter + 1;
            vertex2dualBndFaces{i} = dualFaceCounter;
            
            dualFace2hDualEdges{dualFaceCounter} = hDualFace;
%             fprintf('face created\n');
        else
%             fprintf('degenerate face. Nothing was created\n');
        end
        
    elseif (vertices(i,4) >= 200) && (vertices(i,4) < 300)
%         fprintf('Creating (possibly) 2 faces from a primal vertex on a model edge...\n');
%         plot3(vertices(i,1), vertices(i,2), vertices(i,3), 'r*')
        
        numDualFaces2Add = vertices(i,4) - 200;
        if numDualFaces2Add ~= 2
            error(['Found more than 2 model faces emanating from a vertex' ...
                   ' classified on a model edge.\nAre there internal boundary faces?' ...
                   ' The current implementation does not support them.'])
        end

        vertex2dualBndFaces{i} = zeros(1,numDualFaces2Add);

        % Find edges on model edge connected to the current primal vertex
        test = edges( v2e{i}, 3 ) == 2;
        connectedEdges = v2e{i}(test);
        if length(connectedEdges) ~= 2
            error('Something is wrong')
        end
        
        % Find primal boundary faces connected to the starting primal edge
        test = faces( e2f{connectedEdges(1)}, 6 ) == -1;
        startingFaces = e2f{connectedEdges(1)}(test);
        if length(startingFaces) ~= 2
            error('Something is wrong')
        end
        
        % Overestimate dual face size
        test = faces( v2f{i}, 6 ) == -1;
        dualFaceMaxSize = sum(test) + 2;
        
        for f = 1:numDualFaces2Add
            hDualFace = zeros(1, dualFaceMaxSize);

            dualVertex2  = nan;
            prevEdge     = connectedEdges(1);
            exitInnerFor = false;
            doTest = true;
            m = nan;
            
            for m = 1:dualFaceMaxSize
                if m == 1
                    dualVertex1 = edges2dualVertices( connectedEdges(1) );
                    
                    currentFace = startingFaces(f);
                    dualVertex2 = faces2dualVertices( currentFace );
                else
                    dualVertex1 = dualVertex2;
                    if doTest
                        % Check whether we reached the other side of the face
                        test = f2e( currentFace, 1:3 ) == connectedEdges(2);
                        if any(test)
                            % We reached the other side
                            dualVertex2 = edges2dualVertices( connectedEdges(2) );
                            doTest = false;
                        else
                            tmp = intersect( f2e(currentFace,1:3), v2e{i} );
                            if length(tmp) ~= 2
                                error('Something is wrong')
                            else
                                if prevEdge == tmp(1)
                                    currentEdge = tmp(2);
                                else
                                    currentEdge = tmp(1);
                                end
                                test = ( faces(e2f{currentEdge}, 6)' == -1 ) & ...
                                       ( e2f{currentEdge} ~= currentFace );
                                currentFace = e2f{currentEdge}( test );

                                dualVertex2 = faces2dualVertices( currentFace );
                                
                                prevEdge = currentEdge;
                            end
                        end
                    else
                        dualVertex2 = edges2dualVertices( connectedEdges(1) );
                        exitInnerFor = true;
                    end
                end
                
                mappedVertex1 = dualVerticesPointers(dualVertex1);
                mappedVertex2 = dualVerticesPointers(dualVertex2);

                if mappedVertex1 ~= mappedVertex2
                    % The edge is not collapsing

                    % Sort edge vertices
                    if mappedVertex1 < mappedVertex2
                        sEdge = [mappedVertex1 mappedVertex2];
                    else
                        sEdge = [mappedVertex2 mappedVertex1];
                    end

%                     plot3(dualVertices(sEdge,1), dualVertices(sEdge,2), dualVertices(sEdge,3), 'b-')

                    test = false;
                    if ~isempty( dualEdge2dualVertices )
                        test = ( sEdge(1) == dualEdge2dualVertices(:,1) ) & ...
                               ( sEdge(2) == dualEdge2dualVertices(:,2) );
                    end
                    if ~any( test )
                        % Create a new dual edge
                        dualEdgeCounter = dualEdgeCounter + 1;

                        dualEdge2dualVertices = [ dualEdge2dualVertices;
                                                  sEdge ]; %#ok<AGROW>

                        if mappedVertex1 < mappedVertex2
                            % Assign odd half edge
                            hDualFace(m) = 2*dualEdgeCounter-1;
                        else
                            % Assign even half edge
                            hDualFace(m) = 2*dualEdgeCounter;
                        end
                    else
                        oldDualEdge = find( test );
                        if mappedVertex1 < mappedVertex2
                            % Assign odd half edge
                            hDualFace(m) = 2*oldDualEdge-1;
                        else
                            % Assign even half edge
                            hDualFace(m) = 2*oldDualEdge;
                        end
                    end    

                    if exitInnerFor
                        break;
                    end
                end
            end
            
            hDualFace(hDualFace == 0) = [];
            if numel(hDualFace) >= 3
                dualFaceCounter = dualFaceCounter + 1;
                vertex2dualBndFaces{i}(f) = dualFaceCounter;

                dualFace2hDualEdges{dualFaceCounter} = hDualFace;
%                 fprintf('face created\n');
            else
%                 fprintf('degenerate face. Nothing was created\n');
            end

        end
        vertex2dualBndFaces{i}(vertex2dualBndFaces{i} == 0) = [];
        
    elseif vertices(i,4) >= 300
        numDualFaces2Add = vertices(i,4) - 300;

%         fprintf('Creating (possibly) %d faces from a primal vertex on a model vertex...\n', numDualFaces2Add);
%         plot3(vertices(i,1), vertices(i,2), vertices(i,3), 'r*')

        vertex2dualBndFaces{i} = zeros(1,numDualFaces2Add);
        
        % Find primal edges on model edges connected to current primal
        % vertex
        
        test = edges( v2e{i}, 3 ) == 2;
        connectedEdges = v2e{i}(test);
        
        % Matrix to keep track of the dual faces that have been generated
        % and of those that still have to be.
        
        edge2bndFace = -ones(length(connectedEdges), 2);
        
        % Overestimate dual face size
        test = faces( v2f{i}, 6 ) == -1;
        dualFaceMaxSize = sum(test) + numDualFaces2Add;
        
        addedDualFaces = 0;
        while (addedDualFaces < numDualFaces2Add)
            addedDualFaces = addedDualFaces + 1;
            hDualFace = zeros(1, dualFaceMaxSize);
            
            dualVertex2 = nan;
            m = nan;
            exitInnerFor = false;
            doTest = true;
            
            % Find an edge to start with
            edgeRow = 0;
            startingEdge = nan;
            
            while isnan(startingEdge)
                edgeRow = edgeRow + 1;
                if any(edge2bndFace(edgeRow,:) == -1)
                    startingEdge = connectedEdges(edgeRow);
                end
            end
            
            currentFace = nan;
            prevEdge = startingEdge;
            addFirstEdge = false;
            
            for m = 1:dualFaceMaxSize
                if m == 1
                    dualVertex1 = edges2dualVertices( startingEdge );
                    
                    % Go into the right direction, i.e. follow a primal
                    % boundary face that have not been processed yet
                    
                    test = faces( e2f{startingEdge}, 6 ) == -1;
                    trialFaces = e2f{startingEdge}(test);
                    
                    remainedFaces = setdiff( trialFaces, edge2bndFace(edgeRow,:) );
                    if isempty(remainedFaces)
                        error('startingEdge has not been chosen properly')
                    else
                        currentFace = remainedFaces(1);
                    end
                    
                    % Update the matrix of processed primal boundary faces
                    if edge2bndFace(edgeRow,1) == -1
                        edge2bndFace(edgeRow,1) = currentFace;
                    else
                        edge2bndFace(edgeRow,2) = currentFace;
                    end
                    
                    dualVertex2 = faces2dualVertices( currentFace );
                else
                    dualVertex1 = dualVertex2;
                    if doTest
                        tmp = intersect( f2e(currentFace,1:3), connectedEdges );
                        test = tmp ~= prevEdge;
                        % Trivial check - it fails only if we have a
                        % triangle degenerating into a line
                        if sum(test) > 1
                            error('Something is wrong')
                        end
                        if any(test)
                            dualVertex2 = edges2dualVertices(tmp(test));
                            addFirstEdge = true;
                            doTest = false;
                            
                            % Check that we got back to one of the
                            % connected edges
                            edge2update = tmp(test) == connectedEdges;
                            if ~any(edge2update)
                                error('Something is wrong')
                            end
                            
                            if edge2bndFace(edge2update,1) == -1
                                edge2bndFace(edge2update,1) = currentFace;
                            else
                                edge2bndFace(edge2update,2) = currentFace;
                            end
                        else
                            tmp = intersect( f2e(currentFace,1:3), v2e{i} );
                            if length(tmp) ~= 2
                                error('Something is wrong')
                            else
                                if prevEdge == tmp(1)
                                    currentEdge = tmp(2);
                                else
                                    currentEdge = tmp(1);
                                end
                                
                                test = ( faces(e2f{currentEdge}, 6)' == -1 ) & ...
                                       ( e2f{currentEdge} ~= currentFace );
                                currentFace = e2f{currentEdge}( test );
                                
                                dualVertex2 = faces2dualVertices( currentFace );
                                
                                prevEdge = currentEdge;
                            end
                        end
                    else
                        if addFirstEdge
                            dualVertex2 = vertex2dualVertices(i);
                        else
                            dualVertex2 = edges2dualVertices( startingEdge );
                            exitInnerFor = true;
                        end
                        addFirstEdge = false;
                    end
                end
                
                mappedVertex1 = dualVerticesPointers(dualVertex1);
                mappedVertex2 = dualVerticesPointers(dualVertex2);

                if mappedVertex1 ~= mappedVertex2
                    % The edge is not collapsing

                    % Sort edge vertices
                    if mappedVertex1 < mappedVertex2
                        sEdge = [mappedVertex1 mappedVertex2];
                    else
                        sEdge = [mappedVertex2 mappedVertex1];
                    end

%                     plot3(dualVertices(sEdge,1), dualVertices(sEdge,2), dualVertices(sEdge,3), 'b-')

                    test = false;
                    if ~isempty( dualEdge2dualVertices )
                        test = ( sEdge(1) == dualEdge2dualVertices(:,1) ) & ...
                               ( sEdge(2) == dualEdge2dualVertices(:,2) );
                    end
                    if ~any( test )
                        % Create a new dual edge
                        dualEdgeCounter = dualEdgeCounter + 1;

                        dualEdge2dualVertices = [ dualEdge2dualVertices;
                                                  sEdge ]; %#ok<AGROW>

                        if mappedVertex1 < mappedVertex2
                            % Assign odd half edge
                            hDualFace(m) = 2*dualEdgeCounter-1;
                        else
                            % Assign even half edge
                            hDualFace(m) = 2*dualEdgeCounter;
                        end
                    else
                        oldDualEdge = find( test );
                        if mappedVertex1 < mappedVertex2
                            % Assign odd half edge
                            hDualFace(m) = 2*oldDualEdge-1;
                        else
                            % Assign even half edge
                            hDualFace(m) = 2*oldDualEdge;
                        end
                    end    

                    if exitInnerFor
                        break;
                    end
                end
            end

            hDualFace(hDualFace == 0) = [];
            if numel(hDualFace) >= 3
                dualFaceCounter = dualFaceCounter + 1;
                vertex2dualBndFaces{i}(addedDualFaces) = dualFaceCounter;

                dualFace2hDualEdges{dualFaceCounter} = hDualFace;
%                 fprintf('face created\n');
            else
%                 fprintf('degenerate face. Nothing was created\n');
            end

        end

        vertex2dualBndFaces{i}(vertex2dualBndFaces{i} == 0) = [];

        if any(edge2bndFace(:) == -1)
            error('Primal boundary faces have not been processed properly')
        end
    end
end

dualFace2hDualEdges(dualFaceCounter+1:end) = [];
numDualFaces = dualFaceCounter;

%
% Steps 8 and 9
%
% Create polyhedral regions
%

dualElements2hDualFaces = cell(Nver,1);
dualFaces2dualElements = -ones(numDualFaces,2);

for i = 1:Nver
    if vertices(i,4) == 0
        if any( edges(v2e{i}, 3) )
            error('All the connected primal edges should be internal')
        end
        dualFaceIdx = edges2dualFace(v2e{i});
        dualFaceIdx(edges2dualFace(v2e{i}) == -1) = [];
    else
        tmp = edges2dualFace(v2e{i});
        tmp(edges2dualFace(v2e{i}) == -1) = [];
        dualFaceIdx = [ tmp; vertex2dualBndFaces{i}' ];
    end
    
    dualElementSize = length(dualFaceIdx);
    dualElements2hDualFaces{i} = zeros(1, dualElementSize);
    dualElement = cell(dualElementSize, 1);
    
    facesCentroids = zeros(dualElementSize, 3);
    
    for m = 1:dualElementSize
        
        dualFaceSize = length( dualFace2hDualEdges{ dualFaceIdx(m) } );
        dualElement{m} = zeros(1, dualFaceSize);
        
        for p = 1:dualFaceSize
            hDualEdge = dualFace2hDualEdges{ dualFaceIdx(m) }(p);
            tDualEdge = floor( (hDualEdge+1)/2 );
            if mod(hDualEdge,2)
                dualElement{m}(p) = dualEdge2dualVertices(tDualEdge,1);
            else
                dualElement{m}(p) = dualEdge2dualVertices(tDualEdge,2);
            end
        end
        
        facesCentroids(m,:) = sum( dualVertices( dualElement{m}, : ), 1 ) / dualFaceSize;
    end
    
    centroid = sum( facesCentroids, 1 ) / dualElementSize;
    
    for m = 1:dualElementSize
        
        dualFaceSize = length( dualElement{m} );
        
        v1 = [(dualVertices(dualElement{m},1)-facesCentroids(m,1)) ...
              (dualVertices(dualElement{m},2)-facesCentroids(m,2)) ...
              (dualVertices(dualElement{m},3)-facesCentroids(m,3))];
        v2 = [(dualVertices(dualElement{m}([2:dualFaceSize 1]),1)-facesCentroids(m,1)) ...
              (dualVertices(dualElement{m}([2:dualFaceSize 1]),2)-facesCentroids(m,2)) ...
              (dualVertices(dualElement{m}([2:dualFaceSize 1]),3)-facesCentroids(m,3))];

        normal = sum([(v1(:,2).*v2(:,3) - v1(:,3).*v2(:,2)) ...
                      (v1(:,3).*v2(:,1) - v1(:,1).*v2(:,3)) ...
                      (v1(:,1).*v2(:,2) - v1(:,2).*v2(:,1))],1);
        normal = normal / norm(normal);
        
        v = facesCentroids(m,:) - centroid;
        
        tmp = v*normal';
        
        if tmp > 0
            dualElements2hDualFaces{i}(m) = 2*dualFaceIdx(m)-1;
        else
            dualElements2hDualFaces{i}(m) = 2*dualFaceIdx(m);
        end
        
        if dualFaces2dualElements( dualFaceIdx(m), 1 ) == -1
            dualFaces2dualElements( dualFaceIdx(m), 1 ) = i;
        else
            dualFaces2dualElements( dualFaceIdx(m), 2 ) = i;
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mesh.Nver = numDualVertices;
mesh.coordinates = dualVertices;

mesh.Nelt = Nver;

% Mind that this field MUST be updated after mesh optimization. For the
% moment we keep it only for drawing and debugging purposes
mesh.element2hfaces = dualElements2hDualFaces;

mesh.Nfc = numDualFaces;
mesh.face2hedges = dualFace2hDualEdges;
mesh.face2elements = dualFaces2dualElements;

mesh.Ned = size(dualEdge2dualVertices, 1);
mesh.edge2vertices = dualEdge2dualVertices;


