function lsqDistances = leastSquaredDistances(X)

numPts = size(X,1);
centroid = sum(X,1) / numPts;

X = bsxfun(@minus, X, centroid);

[~,S,V] = svd(X);

minIdx = 1;
minVal = S(1,1);
lastDim = min(size(S));

for l = 2:lastDim
    if minVal > S(l,l)
        minIdx = l;
        minVal = S(l,l);
    end
end

n = V(:,minIdx);
n = n / norm(n);

tmp = X * n;
lsqDistances = sum( tmp.^2 );

end




