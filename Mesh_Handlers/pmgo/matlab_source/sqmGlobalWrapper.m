function [out, vertices ] = sqmGlobalWrapper(x, rescale, lBound, uBound, mesh, modelDescription, funOpt)

% Convert x to the corresponding array of Cartesian coordinates
vertices = parametric2cartesian(x, rescale, lBound, uBound, mesh, modelDescription);

% Compute global shape quality measure
out = sqmGlobal( vertices, mesh );

% Check whether the a penalty should be added for not planar faces
if isfield(funOpt, 'planarPenalty') && isfield(funOpt, 'penaltyParam')
    if funOpt.planarPenalty
        out = out + funOpt.penaltyParam * planarMetricGlobal( vertices, mesh );
    end
end

end
