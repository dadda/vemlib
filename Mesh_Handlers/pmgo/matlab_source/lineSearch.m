function [ alpha, phiNew ]= lineSearch( objFun, x, p, opts, ...
                                        rescale, origLBounds, origUBounds, ...
                                        mesh, modelDescription, funOpt, ...
                                        deltaAlpha, tolAlpha, phi0, dphi0 )

alphaOld = 0;
alphaMax = Inf;

phiOld   = phi0;
dphiOld  = dphi0;
iter     = 1;

while true
    fprintf('Line search iteration %i\n', iter);

    [ alpha, deltaAlpha, alphaMax, phiNew, xNew ] = ...
             stepLength( alphaOld, deltaAlpha, alphaMax, tolAlpha, ...
                         objFun, x, p, opts, rescale, origLBounds, origUBounds, ...
                         mesh, modelDescription, funOpt );
    if deltaAlpha <= tolAlpha
        % Return alpha
        break;
    end
    
    % Evaluate phi'(alpha) = \nabla f(x + alpha*p) \cdot p
    dphiNew = directionalDerivative( objFun, xNew, p, rescale, origLBounds, origUBounds, ...
                                     mesh, modelDescription, funOpt, phiNew, opts );
    
    if ( phiNew > ( phi0 + opts.c1 * alpha * dphi0 ) ) || (phiNew >= phiOld && iter > 1)
        [ alpha, phiNew ] = zoom( alphaOld, alpha, phiOld, phiNew, dphiOld, dphiNew, tolAlpha, ...
                                  phi0, dphi0, objFun, x, p, opts, rescale, ...
                                  origLBounds, origUBounds, mesh, modelDescription, funOpt );
        break;
    end
    
    if abs( dphiNew ) <= - opts.c2 * dphi0
        % Return alpha
        break;
    end
    
    if dphiNew >= 0
        [ alpha, phiNew ] = zoom( alpha, alphaOld, phiNew, phiOld, dphiNew, dphiOld, tolAlpha, ...
                                  phi0, dphi0, objFun, x, p, opts, rescale, ...
                                  origLBounds, origUBounds, mesh, modelDescription, funOpt );
        break;
    end
    
    phiOld   = phiNew;
    dphiOld  = dphiNew;
    alphaOld = alpha;
    iter     = iter + 1;
end

end
