function opts = cmaesDefaults()
% Personalized default options for the Covariance Matrix Adaptation
% Evolution Strategy

opts.StopOnWarnings = 0;
opts.DispFinal = 0;
opts.DispModulo = 5;
opts.SaveVariables = 0;
opts.LogModulo = 0;
opts.LogTime = 0;
opts.LogPlot = 0;
opts.CMA.active = 0;
%opts.PopSize = 100;

end
