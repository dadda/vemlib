function out = sqmSingleNodeV2( coordinates, mesh, nodeGlobIdx, v2p )

Nelt = length(v2p);

out = 0;

for i = 1:Nelt

    element = construct_element_connectivity( mesh, i );

    uniqueVertices = unique( horzcat( element{:} ) );
    Nver = length(uniqueVertices);

    elementCenter = sum( coordinates( uniqueVertices, : ), 1 ) / Nver;

    %
    % Build adjacency list for vertex 'vertexIdx'
    %
    
    Nfc = size( element, 1 );
    neighbors = cell(Nfc, 1);
    for f = 1:Nfc
        tmp = find( element{f} == nodeGlobIdx );
        if ~isempty(tmp)
            Ned = length( element{f} );
            iterator = [Ned 1:Ned 1];
            neighbors{f} = [ element{f}(iterator(tmp)) nodeGlobIdx element{f}(iterator(tmp+2)) ];
        end
    end
    
    neighbors = unique( horzcat(neighbors{:}) );
    
    sumNeighborsSQM = 0;
    numNeighbors = length(neighbors);
    for l = 1:numNeighbors
        [ neighborSQM, nodeTetCorners ] = condSingleNode( coordinates, neighbors(l), ...
                                                  element, elementCenter );
        sumNeighborsSQM = sumNeighborsSQM + neighborSQM / nodeTetCorners;
    end
    
    out = out + sumNeighborsSQM;
end

out = out / Nelt;

end
