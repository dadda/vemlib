function out = sqm2dlocal(x, lb, ub, vIdx, v2pLocal, mesh)

vertex = x .* (ub - lb) + lb;

out = 0;
c   = 10;
Np  = length(v2pLocal);

vertices         = mesh.coordinates;
vertices(vIdx,:) = vertex;

for i = 1:Np
    NverLoc     = mesh.elements(v2pLocal(i),1);
    polySQM     = 0;
    iterator1   = [2:NverLoc 1];
    localCoords = vertices( mesh.elements(v2pLocal(i),2:NverLoc+1), : );
    center      = sum( localCoords, 1 ) / NverLoc;
    xymin       = min(localCoords, [], 1);
    xymax       = max(localCoords, [], 1);
    A0          = ( xymax(2)-xymin(2) ) * ( xymax(1)-xymin(1) );
    for j = 1:NverLoc
        a = center - localCoords(j,:);
        b = localCoords(iterator1(j),:) - localCoords(j,:);

        A       = a(1)*b(2) - b(1)*a(2);
        L       = sqrt( a(1)^2 + a(2)^2 + b(1)^2 + b(2)^2 );
        delta   = 1 / (1 + c*exp(A/A0));
        polySQM = polySQM + L^2 / (A + sqrt(A^2 + delta));
    end
    out = out + polySQM / NverLoc;
end

out = out / Np;

end
