function mesh = assignMarkersToDualMesh2d( mesh )

Nver = mesh.Nver;

mesh.vMarkers = zeros(Nver, 1);

tol = 1e-12;
for i = 1:Nver
    pt = mesh.coordinates(i,:);
    if abs( pt(1) ) < tol
        if abs( pt(2) ) < tol || abs( 1 - pt(2) ) < tol
            mesh.vMarkers(i) = 5;
        else
            mesh.vMarkers(i) = 4;
        end
    elseif abs( 1 - pt(1) ) < tol
        if abs( pt(2) ) < tol || abs( 1 - pt(2) ) < tol
            mesh.vMarkers(i) = 5;
        else
            mesh.vMarkers(i) = 2;
        end
    else
        if abs( pt(2) ) < tol
            mesh.vMarkers(i) = 1;
        elseif abs( 1 - pt(2) ) < tol
            mesh.vMarkers(i) = 3;
        end
    end
end

end
