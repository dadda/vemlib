function alpha = interpolate( alphaLow, alphaHigh, ...
                              phiAlphaLow, phiAlphaHigh, ...
                              dphiAlphaLow, dphiAlphaHigh, ...
                              tolAlphaLow )

d1 = dphiAlphaLow + dphiAlphaHigh - 3 * (phiAlphaHigh - phiAlphaLow) / (alphaHigh - alphaLow);
d2 = sqrt(d1^2 - dphiAlphaLow*dphiAlphaHigh);

if alphaHigh < alphaLow
    d2 = -d2;
end

alpha = alphaHigh - (alphaHigh - alphaLow) ...
                    * (dphiAlphaHigh + d2 - d1) / (dphiAlphaHigh - dphiAlphaLow + 2*d2);

if abs(alpha - alphaLow) < tolAlphaLow * abs(alphaHigh - alphaLow)
    alpha = alphaLow + tolAlphaLow * (alphaHigh - alphaLow);
end

end
