#include "pmgo.h"
#define NUMMESHES 9


int main()
{
  int i, metricsArgc;
  double * newVertices;
  const char *basenames[NUMMESHES];
  char filename[256];
  char **metricsArgv;
  dualMesh2d dMesh2d;

  /*
   * Input/output files for the CMA-ES algorithm
   */
  const char * cmaesInitialsFilename =
    "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/"
    "cpp_source/cmaes_initials.par";

  const char * cmaesSignalsFilename =
    "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/"
    "cpp_source/cmaes_signals.par";

  /*
   * Astroid
   */

  /* basenames[0] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/astroid/dual/astroid_a0p01.1_marked_dual_corrected"; */
  /* basenames[1] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/astroid/dual/astroid_a0p0025.1_marked_dual_corrected"; */
  /* basenames[2] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/astroid/dual/astroid_a0p000625.1_marked_dual_corrected"; */
  /* basenames[3] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/astroid/dual/astroid_a0p00015625.1_marked_dual_corrected"; */
  /* basenames[4] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/astroid/dual/astroid_a0p0000390625.1_marked_dual_corrected"; */
  /* basenames[5] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/astroid/dual/astroid_a0p000009765625.1_marked_dual_corrected"; */
  /* basenames[6] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/astroid/dual/astroid_a0p00000244140625.1_marked_dual_corrected"; */

  /*
   * Flower
   */

  basenames[0] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/flower/dual/flower_a0p02.1_marked_dual_corrected";
  basenames[1] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/flower/dual/flower_a0p01.1_marked_dual_corrected";
  basenames[2] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/flower/dual/flower_a0p005.1_marked_dual_corrected";
  basenames[3] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/flower/dual/flower_a0p0025.1_marked_dual_corrected";
  basenames[4] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/flower/dual/flower_a0p00125.1_marked_dual_corrected";
  basenames[5] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/flower/dual/flower_a0p000625.1_marked_dual_corrected";
  basenames[6] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/flower/dual/flower_a0p0003125.1_marked_dual_corrected";
  basenames[7] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/flower/dual/flower_a0p00015625.1_marked_dual_corrected";
  basenames[8] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/flower/dual/flower_a0p000078125.1_marked_dual_corrected";

  /* basenames[5] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/flower/dual/flower_a0p00001953125.1_marked_dual_corrected"; */

  /*
   * Quarter
   */

  /* basenames[0] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/dual/quarter_a0p01.1_marked_dual_corrected"; */
  /* basenames[1] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/dual/quarter_a0p0025.1_marked_dual_corrected"; */
  /* basenames[2] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/dual/quarter_a0p000625.1_marked_dual_corrected"; */
  /* basenames[3] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/dual/quarter_a0p00015625.1_marked_dual_corrected"; */
  /* basenames[4] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/dual/quarter_a0p0000390625.1_marked_dual_corrected"; */
  /* basenames[5] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/dual/quarter_a0p000009765625.1_marked_dual_corrected"; */
  /* basenames[6] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/dual/quarter_a0p00000244140625.1_marked_dual_corrected"; */

  /*
   * Spiral
   */

  /* basenames[0] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/spiral/dual/spiral_fat_a0p01.1_marked_dual_corrected"; */
  /* basenames[1] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/spiral/dual/spiral_fat_a0p005.1_marked_dual_corrected"; */
  /* basenames[2] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/spiral/dual/spiral_fat_a0p0025.1_marked_dual_corrected"; */
  /* basenames[3] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/spiral/dual/spiral_fat_a0p00125.1_marked_dual_corrected"; */
  /* basenames[4] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/spiral/dual/spiral_fat_a0p000625.1_marked_dual_corrected"; */
  /* basenames[5] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/spiral/dual/spiral_fat_a0p0003125.1_marked_dual_corrected"; */
  /* basenames[6] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/spiral/dual/spiral_fat_a0p00015625.1_marked_dual_corrected"; */
  /* basenames[7] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/spiral/dual/spiral_fat_a0p000078125.1_marked_dual_corrected"; */

  /* basenames[4] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/spiral/dual/spiral_a0p0000390625.1_marked_dual_corrected"; */
  /* basenames[5] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/spiral/dual/spiral_a0p000009765625.1_marked_dual_corrected"; */

  /*
   *
   */

  dualMesh2dInitialize( &dMesh2d );

  /* Choose a function to optimize */
  chooseObjFun( &metricsArgc, &metricsArgv );

  printf("\n");
  for (i = 0; i < NUMMESHES; ++i) {
    printf("Processing mesh %i ... ", i);

    sprintf( filename, "%s%s", basenames[i], ".off" );
    readOFFmesh( filename, &dMesh2d );

    vertex2PolygonConnectivity( &dMesh2d );
    edgeConnectivity( &dMesh2d );
    findBndVertices( &dMesh2d );

    newVertices = (double *) malloc (2 * dMesh2d.Nver * sizeof(double));
    localMesh2dOptimCMAES( &dMesh2d, newVertices,
			   cmaesInitialsFilename,
			   cmaesSignalsFilename,
			   metricsArgv );
    memcpy( dMesh2d.vertices, newVertices, 2 * dMesh2d.Nver * sizeof(double) );
    free( newVertices );

    sprintf( filename, "%s_optimized%s", basenames[i], ".off" );
    saveOFFmesh( filename, &dMesh2d );

    dualMesh2dDestroy( &dMesh2d );
    printf("done\n");
  }

  for (i = 0; i < metricsArgc; ++i) free( metricsArgv[i] );
  free( metricsArgv );

  return 0;
}
