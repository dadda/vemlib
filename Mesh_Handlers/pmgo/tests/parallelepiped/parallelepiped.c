#include "pmgo.h"

int dimParametricSpace( const int Nver,
			const int * const vFlags )
{
  int i, dim = 0;

  for (i = 0; i < Nver; ++i) if ( vFlags[i] != -1 ) ++dim;

  return dim;
}


/*
 * Compute lower and upper boundaries for coordinates in the parametric space
 */
void getParametricBoundaries( const int Nver,
			      const int * const vFlags,
			      double * lbounds,
			      double * ubounds )
{
  int i, counter = 0;

  for (i = 0; i < Nver; ++i) {
    if ( vFlags[i] != -1 ) {
      lbounds[counter] = 0.0; ubounds[counter] = 2.0;
      ++counter;
    }
  }
}


/*
 * Cartesian to parametric conversion
 */
void car2par( const int Nver,
	      const int * const vFlags,
	      const double * const cartesian,
	      double * parametric )
{
  int i, counter = 0;

  for (i = 0; i < Nver; ++i) {
    if ( vFlags[i] != -1 ) {
      parametric[counter] = cartesian[3*i];
      ++counter;
    }
  }
}


/*
 * Indices mapping each cartesian vertex to its first corresponding parameter
 */
void car2firstParIdx( const int Nver,
		      const int * const vFlags,
		      int * const c2firstpIdx )
{
  int i, counter = 0;

  for (i = 0; i < Nver; ++i) {
    if ( vFlags[i] == -1 ) {
      c2firstpIdx[i] = -1;
    } else {
      c2firstpIdx[i] = counter;
      ++counter;
    }
  }
}


/*
 * Parametric to cartesian conversion
 */
void par2car( const double * const parametric,
	      double * const cartesian,
	      const int Nver,
	      const int * const cartesianIdx,
	      const int cartesianIdxDim,
	      const int * const c2firstpIdx,
	      const int * const vFlags )
{
  int i, j, N;

  N = cartesianIdx == NULL ? Nver : cartesianIdxDim;
  for (i = 0; i < N; ++i) {
    j = cartesianIdx == NULL ? i : cartesianIdx[i];
    if ( vFlags[j] != -1 )
      cartesian[3*j] = parametric[ c2firstpIdx[j] ];
  }
}


/*
 * Indices mapping each parameter to the corresponding cartesian vertex
 */
void par2carIdx( const int Nver,
		 const int * const vFlags,
		 int * const p2cIdx )
{
  int i, counter = 0;

  for (i = 0; i < Nver; ++i) {
    if ( vFlags[i] != -1 ) {
      p2cIdx[counter]   = i;
      ++counter;
    }
  }
}
