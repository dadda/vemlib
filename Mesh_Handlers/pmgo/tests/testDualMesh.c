#include "pmgo.h"
#include <time.h>

int main()
{
  const char * basename = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/"
    "tetgenExamples/unitcube.1_marked";
  const char * outfilename = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/"
    "tetgenExamples/unitcube.1.dual";
  clock_t ck0, ck1;
  primalMesh pMesh;
  dualMesh dMesh;
  double elapsedTime = 0.0;
  double tolCollapse = 1e-8;

  ck0 = clock();
  primalMeshInitialize( &pMesh );
  readTetgen( basename, false, &pMesh );
  primalMeshConstructConnectivity( &pMesh );
  ck1 = clock();
  elapsedTime = (double)(ck1-ck0)/CLOCKS_PER_SEC;
  printf("\nTime required for reading and constructing primal mesh: %e seconds\n", elapsedTime );

  ck0 = clock();
  dualMeshInitialize( &dMesh );
  dualMeshCreate( &pMesh, tolCollapse, &dMesh );
  dualMeshConstructConnectivity( &dMesh );
  ck1 = clock();
  elapsedTime = (double)(ck1-ck0)/CLOCKS_PER_SEC;
  printf("\nTime required for constructing dual mesh: %e seconds\n", elapsedTime );

  /* dualMeshView( &dMesh ); */
  dualMeshSave( outfilename, &dMesh );

  primalMeshDestroy( &pMesh );
  dualMeshDestroy( &dMesh );

  return 0;
}
