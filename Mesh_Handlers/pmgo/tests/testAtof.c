#include <stdio.h>      /* printf, fgets */
#include <stdlib.h>     /* atof */
#include <math.h>       /* sin */

int main ()
{
  double m, n;
  char **args = NULL;

  args = (char **) malloc (2 * sizeof(char *));
  args[0] = (char *) malloc (256 * sizeof(char));
  args[1] = (char *) malloc (256 * sizeof(char));

  sprintf(args[0], "%s", "10.0");
  sprintf(args[1], "%s", "20.0");

  m = atof( args[0] );
  n = atof( args[1] );

  printf ("m: %f, n: %f\n", m, n);

  free( args[0] );
  free( args[1] );
  free( args );
  return 0;
}

