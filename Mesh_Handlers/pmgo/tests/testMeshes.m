%
% Pool of simple meshes to test mesh optimization algorithms
% 

% Two twisted parallelepipeds
mesh.coordinates = [ 0 0 0;
                     1 0 0;
                     2 0 0;
                     0 1 0;
                     1 1 0;
                     2 1 0;
                     0 0 1;
                     0.7 0 1;
                     1.7 0 1;
                     0 1 1;
                     1.3 1 1;
                     2 1 1];
                 
mesh.edge2vertices = [ 1 2;
                       2 3;
                       1 4;
                       2 5;
                       3 6;
                       4 5;
                       5 6;
                       1 7;
                       2 8;
                       3 9;
                       4 10;
                       5 11;
                       6 12;
                       7 8;
                       8 9;
                       7 10;
                       8 11;
                       9 12;
                       10 11;
                       11 12 ];

mesh.face2hedges = [ 1 7 12 6;
                     3 9 14 8;
                     1 17 28 16;
                     3 19 30 18;
                     15 31 22 6;
                     33 24 8 17;
                     35 26 10 19;
                     37 24 12 21;
                     39 26 14 23;
                     27 33 38 32;
                     35 40 34 29 ];

mesh.face2hedges = mat2cell( mesh.face2hedges, ones( size(mesh.face2hedges,1), 1 ), 4 );

mesh.face2elements = [ 1 -1;
                       2 -1;
                       1 -1;
                       2 -1;
                       1 -1;
                       1 2;
                       2 -1;
                       1 -1;
                       2 -1;
                       1 -1;
                       2 -1 ];

mesh.element2hfaces = [ 2 19 5 15 9 12;
                        4 21 7 17 11 14 ];
mesh.element2hfaces = mat2cell( mesh.element2hfaces, [1,1], 6 );

mesh.Nver = 12;
mesh.Nfc  = 11;
mesh.Nelt = 2;
mesh.Ned  = 20;

mesh.verticesMarkers = 300 * ones(mesh.Nver, 1);

% A small trick to optimize only the chosen nodes
mesh.verticesMarkers(8) = 209;
mesh.verticesMarkers(9) = 209;
mesh.verticesMarkers(11) = 211;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% % A cube inside the unit cube - make sure any optimization routine leave it untouched
%
% mesh.coordinates = [ 0.25     0.25     0.25;
%                      0.75     0.25     0.25;
%                      0.25     0.75     0.25;
%                      0.75     0.75     0.25;
%                      0.25     0.25     0.75;
%                      0.75     0.25     0.75;
%                      0.25     0.75     0.75;
%                      0.75     0.75     0.75 ] ;
%
% mesh.edge2vertices = [ 1     2;
%                        1     3;
%                        1     5;
%                        2     4;
%                        2     6;
%                        3     4;
%                        3     7;
%                        4     8;
%                        5     6;
%                        5     7;
%                        6     8;
%                        7     8 ];
%
% mesh.face2hedges = [ 3    11     8     2;
%                      17    21    24    20;
%                      7    15    22    10;
%                      5    19    14     4;
%                      1     9    18     6;
%                      13    23    16    12 ];
%
% mesh.face2hedges = mat2cell( mesh.face2hedges, ones( size(mesh.face2hedges,1), 1 ), 4 );
%
% mesh.face2elements = [ 1 -1;
%                        1 -1;
%                        1 -1;
%                        1 -1;
%                        1 -1;
%                        1 -1 ];
%
% mesh.element2hfaces = { [1 3 5 7 9 11] };
%
% mesh.Nver = 8;
% mesh.Nfc  = 6;
% mesh.Nelt = 1;
% mesh.Ned  = 12;
%
% mesh.verticesMarkers = zeros(mesh.Nver, 1);
%
