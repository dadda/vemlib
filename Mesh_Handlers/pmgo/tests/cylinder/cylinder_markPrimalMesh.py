
### This file might be required if Steiner points are added by Tetgen,
### in which case a proper preliminary mark must be assigned to them.

import sys
import salome
import shutil

salome.salome_init()
theStudy = salome.myStudy

import salome_notebook
notebook = salome_notebook.NoteBook(theStudy)
sys.path.insert( 0, r'/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/tests/cylinder')

###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS
import numpy as np

geompy = geomBuilder.New(theStudy)

O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)
geomObj_1 = geompy.MakeMarker(0, 0, 0, 1, 0, 0, 0, 1, 0)
Vertex_1 = geompy.MakeVertex(0, 0, 0)
Vertex_2 = geompy.MakeVertex(0, 0, 10)
Line_1 = geompy.MakeLineTwoPnt(Vertex_1, Vertex_2)
Circle_1 = geompy.MakeCircle(Vertex_1, Line_1, 2)
Face_1 = geompy.MakeFaceWires([Circle_1], 1)
Pipe_1 = geompy.MakePipe(Face_1, Line_1)
[Face_2,Face_3,Face_4] = geompy.ExtractShapes(Pipe_1, geompy.ShapeType["FACE"], True)
[Edge_1,Edge_2,Edge_3] = geompy.ExtractShapes(Pipe_1, geompy.ShapeType["EDGE"], True)
Planar_faces = geompy.MakeCompound([Face_2, Face_4])
circles = geompy.MakeCompound([Edge_1, Edge_2])
Face_compound = geompy.MakeCompound([Face_2, Face_3, Face_4])
geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( Vertex_1, 'Vertex_1' )
geompy.addToStudy( Vertex_2, 'Vertex_2' )
geompy.addToStudy( Line_1, 'Line_1' )
geompy.addToStudy( Circle_1, 'Circle_1' )
geompy.addToStudy( Face_1, 'Face_1' )
geompy.addToStudy( Pipe_1, 'Pipe_1' )
geompy.addToStudyInFather( Pipe_1, Face_2, 'Face_2' )
geompy.addToStudyInFather( Pipe_1, Face_3, 'Face_3' )
geompy.addToStudyInFather( Pipe_1, Face_4, 'Face_4' )
geompy.addToStudyInFather( Pipe_1, Edge_1, 'Edge_1' )
geompy.addToStudyInFather( Pipe_1, Edge_2, 'Edge_2' )
geompy.addToStudyInFather( Pipe_1, Edge_3, 'Edge_3' )
geompy.addToStudy( Planar_faces, 'Planar_faces' )
geompy.addToStudy( circles, 'circles' )
geompy.addToStudy( Face_compound, 'Face_compound' )


###
### SMESH component - Load it from Tetgen output
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

tetgenBasename = r'/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/tests/' \
                 r'cylinder/cylinder.1'
newTetgenBasename = r'/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/tests/' \
                    r'cylinder/cylinder.1_corrected'

smesh = smeshBuilder.New(theStudy)
Mesh_1 = smesh.Mesh()

#
# Read mesh nodes
#

fid  = open(tetgenBasename + '.node', 'r')
line = fid.readline()
numNodes = np.int64( line.split()[0] )
nodes = np.zeros( (numNodes, 3), dtype='float64' )

for i in range(numNodes):
    line = fid.readline()
    nodes[i,:] = [ np.float64(j) for j in line.split()[1:4] ]
fid.close()

#
# Read mesh tetrahedra
#

fid  = open(tetgenBasename + '.ele', 'r')
line = fid.readline()
numTetra = np.int64( line.split()[0] )
ele  = np.zeros( (numTetra, 4), dtype='int64' )
for i in range(numTetra):
    line = fid.readline()
    ele[i,:] = [ np.int64(j) for j in line.split()[1:] ]
fid.close()

for i in range(numNodes):
    Mesh_1.AddNode( nodes[i,0], nodes[i,1], nodes[i,2] )

for i in range(numTetra):
    Mesh_1.AddVolume( [ ele[i,0], ele[i,1], ele[i,2], ele[i,3] ] )

aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_LyingOnGeom,SMESH.FT_Undefined,Planar_faces)
aCriteria.append(aCriterion)
aFilter_1 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_1.SetMesh(Mesh_1.GetMesh())
Group_1 = Mesh_1.GroupOnFilter( SMESH.NODE, 'Group_1', aFilter_1 )
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,circles)
aCriteria.append(aCriterion)
aFilter_2 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_2.SetMesh(Mesh_1.GetMesh())
nodes_on_circles = Mesh_1.GroupOnFilter( SMESH.NODE, 'nodes_on_circles', aFilter_2 )
nodes_type_101 = Mesh_1.GetMesh().CutListOfGroups( [ Group_1 ], [ nodes_on_circles ], 'nodes_type_101' )
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_NodeConnectivityNumber,'=',6)
aCriteria.append(aCriterion)
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,Pipe_1)
aCriteria.append(aCriterion)
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_LyingOnGeom,SMESH.FT_Undefined,Pipe_1)
aCriteria.append(aCriterion)
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,Face_compound)
aCriteria.append(aCriterion)
aFilter_6 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_6.SetMesh(Mesh_1.GetMesh())
boundary_nodes = Mesh_1.GroupOnFilter( SMESH.NODE, 'boundary_nodes', aFilter_6 )
nodes_type_300 = Mesh_1.GetMesh().CutListOfGroups( [ boundary_nodes ], [ nodes_type_101 ], 'nodes_type_300' )

all_nodes_flags = [0] * Mesh_1.NbNodes()
for i in range(nodes_type_101.GetNumberOfNodes()):
  nodeID = nodes_type_101.GetID(i+1)
  all_nodes_flags[nodeID-1] = 101

for i in range(nodes_type_300.GetNumberOfNodes()):
  nodeID = nodes_type_300.GetID(i+1)
  all_nodes_flags[nodeID-1] = 300

# Save both marked and unmarked files for double-checking with Tetgen

fid = open(newTetgenBasename + '.node', 'w')
fid.write(str(Mesh_1.NbNodes()) + ' 3 0 1\n')
for i in range( Mesh_1.NbNodes() ):
  fid.write(str(i+1) + ' ' + ' '.join('%.16f' % coord for coord in Mesh_1.GetNodeXYZ(i+1)) + ' ' + str(all_nodes_flags[i]) + '\n')
fid.close()

shutil.copy(tetgenBasename + '.ele', newTetgenBasename + '.ele')
shutil.copy(tetgenBasename + '.face', newTetgenBasename + '.face')
shutil.copy(tetgenBasename + '.edge', newTetgenBasename + '.edge')

## Set names of Mesh objects
smesh.SetName(Mesh_1.GetMesh(), 'Mesh_1')
smesh.SetName(boundary_nodes, 'boundary_nodes')
smesh.SetName(nodes_type_300, 'nodes_type_300')
smesh.SetName(nodes_on_circles, 'nodes_on_circles')
smesh.SetName(nodes_type_101, 'nodes_type_101')
smesh.SetName(Group_1, 'Group_1')


if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser(True)
