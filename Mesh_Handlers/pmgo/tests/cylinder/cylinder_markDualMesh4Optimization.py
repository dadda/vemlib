
import sys
import salome
import shutil

salome.salome_init()
theStudy = salome.myStudy

import salome_notebook
notebook = salome_notebook.NoteBook(theStudy)
sys.path.insert( 0, r'/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/tests/cylinder')

###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS
import numpy as np

geompy = geomBuilder.New(theStudy)

O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)
geomObj_1 = geompy.MakeMarker(0, 0, 0, 1, 0, 0, 0, 1, 0)
Vertex_1 = geompy.MakeVertex(0, 0, 0)
Vertex_2 = geompy.MakeVertex(0, 0, 10)
Line_1 = geompy.MakeLineTwoPnt(Vertex_1, Vertex_2)
Circle_1 = geompy.MakeCircle(Vertex_1, Line_1, 2)
Face_1 = geompy.MakeFaceWires([Circle_1], 1)
Pipe_1 = geompy.MakePipe(Face_1, Line_1)
[Face_2,Face_3,Face_4] = geompy.ExtractShapes(Pipe_1, geompy.ShapeType["FACE"], True)
[Edge_1,Edge_2,Edge_3] = geompy.ExtractShapes(Pipe_1, geompy.ShapeType["EDGE"], True)
Planar_faces = geompy.MakeCompound([Face_2, Face_4])
circles = geompy.MakeCompound([Edge_1, Edge_2])
Face_compound = geompy.MakeCompound([Face_2, Face_3, Face_4])
geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( Vertex_1, 'Vertex_1' )
geompy.addToStudy( Vertex_2, 'Vertex_2' )
geompy.addToStudy( Line_1, 'Line_1' )
geompy.addToStudy( Circle_1, 'Circle_1' )
geompy.addToStudy( Face_1, 'Face_1' )
geompy.addToStudy( Pipe_1, 'Pipe_1' )
geompy.addToStudyInFather( Pipe_1, Face_2, 'Face_2' )
geompy.addToStudyInFather( Pipe_1, Face_3, 'Face_3' )
geompy.addToStudyInFather( Pipe_1, Face_4, 'Face_4' )
geompy.addToStudyInFather( Pipe_1, Edge_1, 'Edge_1' )
geompy.addToStudyInFather( Pipe_1, Edge_2, 'Edge_2' )
geompy.addToStudyInFather( Pipe_1, Edge_3, 'Edge_3' )
geompy.addToStudy( Planar_faces, 'Planar_faces' )
geompy.addToStudy( circles, 'circles' )
geompy.addToStudy( Face_compound, 'Face_compound' )

###
### SMESH component
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

#
# Read dual mesh
#

dualBasename = r'/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/tests/' \
               r'cylinder/cylinder.1_corrected.dual'

vertices = np.loadtxt( dualBasename + '.node', dtype='float64', skiprows=1, usecols=(0,1,2) )
Nver     = vertices.shape[0]

edges = np.loadtxt( dualBasename + '.edge', dtype='int64', skiprows=1, usecols=(0,1) )
Ned   = edges.shape[0]

fid = open( dualBasename + '.face', 'r' )
lines = fid.readlines()[1:]
fid.close()

Nfc  = len(lines)
f2he = [0] * Nfc
f2p  = np.zeros([Nfc,2])
for i,line in enumerate(lines):
    tmp = line.split()
    NedLoc   = np.int64( tmp[0] )
    f2he[i]  = np.int64( [ j for j in tmp[1:(NedLoc+1)] ] )
    f2p[i,:] = [ tmp[NedLoc+1], tmp[NedLoc+2] ]

fid = open( dualBasename + '.polyhedron', 'r' )
lines = fid.readlines()[1:]
fid.close()

p2hf = [ np.int64( [ i for i in line.split()[1:] ] ) for line in lines ]
Npol = len(p2hf)

#
# Create polyhedral mesh
#

smesh = smeshBuilder.New(theStudy)
Mesh_1 = smesh.Mesh()

for i in range(Nver):
    Mesh_1.AddNode( vertices[i,0], vertices[i,1], vertices[i,2] )

for i in range(Npol):
    hFaces = p2hf[i]
    tFaces = hFaces/2
    NfcLoc = len(hFaces)
    polyhedron = [0] * NfcLoc
    NedByFace  = [0] * NfcLoc
    for j in range(NfcLoc):
        hEdges = f2he[tFaces[j]]
        tEdges = hEdges/2
        edgesOrientation = hEdges%2
        edgesLoc = edges[tEdges,:].T
        polyhedron[j] = (1 - hFaces[j]%2) * ( (1-edgesOrientation) * edgesLoc[0,:] \
                                              + edgesOrientation * edgesLoc[1,:] ) \
                        + (hFaces[j]%2) * ( (1-edgesOrientation[-1::-1]) * edgesLoc[1,-1::-1] \
                                            + edgesOrientation[-1::-1] * edgesLoc[0,-1::-1] )
        polyhedron[j] = polyhedron[j] + 1
        NedByFace[j] = len(polyhedron[j])
    Mesh_1.AddPolyhedralVolume( np.hstack(polyhedron).tolist(), NedByFace )

#
# Add all faces
#

for i in range(Nfc):
    hEdges = f2he[i]
    tEdges = hEdges/2
    edgesOrientation = hEdges%2
    edgesLoc = edges[tEdges,:].T
    face = (1-edgesOrientation) * edgesLoc[0,:] + edgesOrientation * edgesLoc[1,:]
    face = face + 1
    Mesh_1.AddPolygonalFace( face.tolist() )

aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.FACE,SMESH.FT_FreeFaces,SMESH.FT_Undefined,0)
aCriteria.append(aCriterion)
aFilter_1 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_1.SetMesh(Mesh_1.GetMesh())
boundary_faces = Mesh_1.GroupOnFilter( SMESH.FACE, 'boundary_faces', aFilter_1 )

aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.FACE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,Face_2)
aCriteria.append(aCriterion)
aFilter_2 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_2.SetMesh(Mesh_1.GetMesh())
faces_flag_2 = Mesh_1.GroupOnFilter( SMESH.FACE, 'faces_flag_2', aFilter_2 )
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.FACE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,Face_4)
aCriteria.append(aCriterion)
aFilter_3 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_3.SetMesh(Mesh_1.GetMesh())
faces_flag_5 = Mesh_1.GroupOnFilter( SMESH.FACE, 'faces_flag_5', aFilter_3 )
faces_flag_3 = Mesh_1.GetMesh().CutListOfGroups( [ boundary_faces ], [ faces_flag_2, faces_flag_5 ], 'faces_flag_3' )
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,Edge_1)
aCriteria.append(aCriterion)
aFilter_4 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_4.SetMesh(Mesh_1.GetMesh())
nodes_on_Edge_1 = Mesh_1.GroupOnFilter( SMESH.NODE, 'nodes_on_Edge_1', aFilter_4 )
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGenSurface,SMESH.FT_Undefined,Face_2)
aCriteria.append(aCriterion)
aFilter_5 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_5.SetMesh(Mesh_1.GetMesh())
nodes_on_Face_2 = Mesh_1.GroupOnFilter( SMESH.NODE, 'nodes_on_Face_2', aFilter_5 )
internal_nodes_on_Face_2 = Mesh_1.GetMesh().CutListOfGroups( [ nodes_on_Face_2 ], [ nodes_on_Edge_1 ], 'internal_nodes_on_Face_2' )
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,Edge_2)
aCriteria.append(aCriterion)
aFilter_6 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_6.SetMesh(Mesh_1.GetMesh())
nodes_on_Edge_2 = Mesh_1.GroupOnFilter( SMESH.NODE, 'nodes_on_Edge_2', aFilter_6 )
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToPlane,SMESH.FT_Undefined,Face_4)
aCriteria.append(aCriterion)
aFilter_7 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_7.SetMesh(Mesh_1.GetMesh())
nodes_on_Face_4 = Mesh_1.GroupOnFilter( SMESH.NODE, 'nodes_on_Face_4', aFilter_7 )
internal_nodes_on_Face_4 = Mesh_1.GetMesh().CutListOfGroups( [ nodes_on_Face_4 ], [ nodes_on_Edge_2 ], 'internal_nodes_on_Face_4' )
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,Face_3)
aCriteria.append(aCriterion)
aFilter_8 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_8.SetMesh(Mesh_1.GetMesh())
nodes_on_Face_3 = Mesh_1.GroupOnFilter( SMESH.NODE, 'nodes_on_Face_3', aFilter_8 )
internal_nodes_on_Face_3 = Mesh_1.GetMesh().CutListOfGroups( [ nodes_on_Face_3 ], [ nodes_on_Edge_1, nodes_on_Edge_2 ], 'internal_nodes_on_Face_3' )
all_nodes = Mesh_1.CreateEmptyGroup( SMESH.NODE, 'all_nodes' )
nbAdd = all_nodes.AddFrom( Mesh_1.GetMesh() )
internal_nodes = Mesh_1.GetMesh().CutListOfGroups( [ all_nodes ], [ nodes_on_Edge_1, nodes_on_Face_2, internal_nodes_on_Face_2, nodes_on_Edge_2, nodes_on_Face_4, internal_nodes_on_Face_4, nodes_on_Face_3, internal_nodes_on_Face_3 ], 'internal_nodes' )


#
# Assign markers for mesh optimization
#
# 1: on Edge 1
# 2: interior of Face 2
# 3: interior of Face 3
# 4: on Edge 2
# 5: interior of Face 4
#

all_nodes_flags = [0] * Nver

for i in range( nodes_on_Edge_1.GetNumberOfNodes() ):
    nodeID = nodes_on_Edge_1.GetID(i+1)
    all_nodes_flags[nodeID-1] = 1

for i in range( internal_nodes_on_Face_2.GetNumberOfNodes() ):
    nodeID = internal_nodes_on_Face_2.GetID(i+1)
    all_nodes_flags[nodeID-1] = 2

for i in range( internal_nodes_on_Face_3.GetNumberOfNodes() ):
    nodeID = internal_nodes_on_Face_3.GetID(i+1)
    all_nodes_flags[nodeID-1] = 3

for i in range( nodes_on_Edge_2.GetNumberOfNodes() ):
    nodeID = nodes_on_Edge_2.GetID(i+1)
    all_nodes_flags[nodeID-1] = 4

for i in range( internal_nodes_on_Face_4.GetNumberOfNodes() ):
    nodeID = internal_nodes_on_Face_4.GetID(i+1)
    all_nodes_flags[nodeID-1] = 5


fid = open(dualBasename + '_marked.node', 'w')
fid.write(str(Nver) + ' 1\n')
for i in range( Nver ):
    fid.write(' '.join('%.16f' % coord for coord in Mesh_1.GetNodeXYZ(i+1)) + ' ' + str(all_nodes_flags[i]) + '\n')
fid.close()

shutil.copy(dualBasename + '.edge', dualBasename + '_marked.edge')
shutil.copy(dualBasename + '.face', dualBasename + '_marked.face')
shutil.copy(dualBasename + '.polyhedron', dualBasename + '_marked.polyhedron')
shutil.copy(dualBasename + '.v2p', dualBasename + '_marked.v2p')

## Set names of Mesh objects
smesh.SetName(internal_nodes, 'internal_nodes')
smesh.SetName(all_nodes, 'all_nodes')
smesh.SetName(internal_nodes_on_Face_3, 'internal_nodes_on_Face_3')
smesh.SetName(nodes_on_Face_3, 'nodes_on_Face_3')
smesh.SetName(boundary_faces, 'boundary_faces')
smesh.SetName(faces_flag_2, 'faces_flag_2')
smesh.SetName(faces_flag_5, 'faces_flag_5')
smesh.SetName(faces_flag_3, 'faces_flag_3')
smesh.SetName(Mesh_1.GetMesh(), 'Mesh_1')
smesh.SetName(nodes_on_Face_4, 'nodes_on_Face_4')
smesh.SetName(internal_nodes_on_Face_4, 'internal_nodes_on_Face_4')
smesh.SetName(internal_nodes_on_Face_2, 'internal_nodes_on_Face_2')
smesh.SetName(nodes_on_Edge_2, 'nodes_on_Edge_2')
smesh.SetName(nodes_on_Edge_1, 'nodes_on_Edge_1')
smesh.SetName(nodes_on_Face_2, 'nodes_on_Face_2')


if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser(True)
