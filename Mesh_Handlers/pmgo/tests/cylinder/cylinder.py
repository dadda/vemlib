# -*- coding: utf-8 -*-

###
### This file is generated automatically by SALOME v8.3.0 with dump python functionality
###

import sys
import salome
import shutil

salome.salome_init()
theStudy = salome.myStudy

import salome_notebook
notebook = salome_notebook.NoteBook(theStudy)
sys.path.insert( 0, r'/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/tests/cylinder')

###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS
import numpy as np

geompy = geomBuilder.New(theStudy)

O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)
geomObj_1 = geompy.MakeMarker(0, 0, 0, 1, 0, 0, 0, 1, 0)
Vertex_1 = geompy.MakeVertex(0, 0, 0)
Vertex_2 = geompy.MakeVertex(0, 0, 10)
Line_1 = geompy.MakeLineTwoPnt(Vertex_1, Vertex_2)
Circle_1 = geompy.MakeCircle(Vertex_1, Line_1, 2)
Face_1 = geompy.MakeFaceWires([Circle_1], 1)
Pipe_1 = geompy.MakePipe(Face_1, Line_1)
[Face_2,Face_3,Face_4] = geompy.ExtractShapes(Pipe_1, geompy.ShapeType["FACE"], True)
[Edge_1,Edge_2,Edge_3] = geompy.ExtractShapes(Pipe_1, geompy.ShapeType["EDGE"], True)
Planar_faces = geompy.MakeCompound([Face_2, Face_4])
circles = geompy.MakeCompound([Edge_1, Edge_2])
Face_compound = geompy.MakeCompound([Face_2, Face_3, Face_4])
geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( Vertex_1, 'Vertex_1' )
geompy.addToStudy( Vertex_2, 'Vertex_2' )
geompy.addToStudy( Line_1, 'Line_1' )
geompy.addToStudy( Circle_1, 'Circle_1' )
geompy.addToStudy( Face_1, 'Face_1' )
geompy.addToStudy( Pipe_1, 'Pipe_1' )
geompy.addToStudyInFather( Pipe_1, Face_2, 'Face_2' )
geompy.addToStudyInFather( Pipe_1, Face_3, 'Face_3' )
geompy.addToStudyInFather( Pipe_1, Face_4, 'Face_4' )
geompy.addToStudyInFather( Pipe_1, Edge_1, 'Edge_1' )
geompy.addToStudyInFather( Pipe_1, Edge_2, 'Edge_2' )
geompy.addToStudyInFather( Pipe_1, Edge_3, 'Edge_3' )
geompy.addToStudy( Planar_faces, 'Planar_faces' )
geompy.addToStudy( circles, 'circles' )
geompy.addToStudy( Face_compound, 'Face_compound' )

###
### SMESH component
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

tetgenBasename = r'/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/tests/' \
                 r'cylinder/cylinder'

smesh = smeshBuilder.New(theStudy)
Mesh_1 = smesh.Mesh(Pipe_1)
Regular_1D = Mesh_1.Segment()
Automatic_Length_1 = Regular_1D.AutomaticLength(0.5)
MEFISTO_2D = Mesh_1.Triangle(algo=smeshBuilder.MEFISTO)
NETGEN_3D = Mesh_1.Tetrahedron()
isDone = Mesh_1.Compute()
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_LyingOnGeom,SMESH.FT_Undefined,Planar_faces)
aCriteria.append(aCriterion)
aFilter_1 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_1.SetMesh(Mesh_1.GetMesh())
Group_1 = Mesh_1.GroupOnFilter( SMESH.NODE, 'Group_1', aFilter_1 )
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,circles)
aCriteria.append(aCriterion)
aFilter_2 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_2.SetMesh(Mesh_1.GetMesh())
nodes_on_circles = Mesh_1.GroupOnFilter( SMESH.NODE, 'nodes_on_circles', aFilter_2 )
nodes_type_101 = Mesh_1.GetMesh().CutListOfGroups( [ Group_1 ], [ nodes_on_circles ], 'nodes_type_101' )
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_NodeConnectivityNumber,'=',6)
aCriteria.append(aCriterion)
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,Pipe_1)
aCriteria.append(aCriterion)
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_LyingOnGeom,SMESH.FT_Undefined,Pipe_1)
aCriteria.append(aCriterion)
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,Face_compound)
aCriteria.append(aCriterion)
aFilter_6 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_6.SetMesh(Mesh_1.GetMesh())
boundary_nodes = Mesh_1.GroupOnFilter( SMESH.NODE, 'boundary_nodes', aFilter_6 )
nodes_type_300 = Mesh_1.GetMesh().CutListOfGroups( [ boundary_nodes ], [ nodes_type_101 ], 'nodes_type_300' )

all_nodes_flags = [0] * Mesh_1.NbNodes()
for i in range(nodes_type_101.GetNumberOfNodes()):
  nodeID = nodes_type_101.GetID(i+1)
  all_nodes_flags[nodeID-1] = 101

for i in range(nodes_type_300.GetNumberOfNodes()):
  nodeID = nodes_type_300.GetID(i+1)
  all_nodes_flags[nodeID-1] = 300

# Save both marked and unmarked files for double-checking with Tetgen

fid  = open(tetgenBasename + '.node', 'w')
fidU = open(tetgenBasename + '_unmarked.node', 'w')
fid.write(str(Mesh_1.NbNodes()) + ' 3 0 1\n')
fidU.write(str(Mesh_1.NbNodes()) + ' 3 0 0\n')
for i in range( Mesh_1.NbNodes() ):
  fid.write(str(i+1) + ' ' + ' '.join('%.16f' % coord for coord in Mesh_1.GetNodeXYZ(i+1)) + ' ' + str(all_nodes_flags[i]) + '\n')
  fidU.write(str(i+1) + ' ' + ' '.join('%.16f' % coord for coord in Mesh_1.GetNodeXYZ(i+1)) + '\n')
fid.close()
fidU.close()

counter = 0
fid     = open(tetgenBasename + '.ele', 'w')
fid.write(str(Mesh_1.NbTetras()) + ' 4 0\n')
for i in range(Mesh_1.NbElements()):
  numLocalNodes = Mesh_1.GetElemNbNodes(i+1)
  if numLocalNodes == 4:
    counter = counter+1
    fid.write(str(counter) + ' ' + ' '.join(str(id) for id in Mesh_1.GetElemNodes(i+1)) + '\n')
fid.close()

shutil.copy(tetgenBasename + '.ele', tetgenBasename + '_unmarked.ele')

## Set names of Mesh objects
smesh.SetName(Regular_1D.GetAlgorithm(), 'Regular_1D')
smesh.SetName(NETGEN_3D.GetAlgorithm(), 'NETGEN 3D')
smesh.SetName(MEFISTO_2D.GetAlgorithm(), 'MEFISTO_2D')
smesh.SetName(Automatic_Length_1, 'Automatic Length_1')
smesh.SetName(Mesh_1.GetMesh(), 'Mesh_1')
smesh.SetName(boundary_nodes, 'boundary_nodes')
smesh.SetName(nodes_type_300, 'nodes_type_300')
smesh.SetName(nodes_on_circles, 'nodes_on_circles')
smesh.SetName(nodes_type_101, 'nodes_type_101')
smesh.SetName(Group_1, 'Group_1')


if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser(True)
