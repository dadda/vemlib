#include "pmgo.h"

#define _CYLINDER_RADIUS 2.0
#define _CYLINDER_HEIGHT 10.0
#define _CIRCLE_SEGMENTS 20

/*
 * Compute the dimension of the parametric space
 */
int dimParametricSpace( const int Nver,
			const int * const vFlags )
{
  int i, dim = 0;

  for (i = 0; i < Nver; ++i) {
    if ( vFlags[i] == -1 )
      continue;
    else if ( vFlags[i] == 0 )
      dim += 3;
    else if ( ( vFlags[i] == 1 ) || ( vFlags[i] == 2 ) || ( vFlags[i]%4 == 2 ) )
      dim += 2;
    else
      ++dim;
  }

  return dim;
}


/*
 * Compute lower and upper boundaries for coordinates in the parametric space
 */
void getParametricBoundaries( const int Nver,
			      const int * const vFlags,
			      double * lbounds,
			      double * ubounds )
{
  int i, counter = 0;

  for (i = 0; i < Nver; ++i) {
    if ( vFlags[i] == -1 )
      continue;
    else if ( vFlags[i] == 0 ) {
      lbounds[counter]   = 0.0;  ubounds[counter]   = _CYLINDER_RADIUS;
      lbounds[counter+1] = -1.0; ubounds[counter+1] = 1.0;
      lbounds[counter+2] = 0.0;  ubounds[counter+2] = _CYLINDER_HEIGHT;
      counter += 3;
    }
    else if ( ( vFlags[i] == 1 ) || ( vFlags[i] == 2 ) ) {
      lbounds[counter]   = 0.0;  ubounds[counter]   = _CYLINDER_RADIUS;
      lbounds[counter+1] = -1.0; ubounds[counter+1] = 1.0;
      counter += 2;
    }
    else if ( ( vFlags[i]%4 == 3 ) || ( vFlags[i]%4 == 0 ) || ( vFlags[i]%4 == 1 ) ) {
      lbounds[counter] = 0.0; ubounds[counter] = 1.0;
      ++counter;
    }
    else {
      lbounds[counter]   = 0.0; ubounds[counter]   = 1.0;
      lbounds[counter+1] = 0.0; ubounds[counter+1] = 1.0;
      counter += 2;
    }
  }
}


/*
 * Cartesian to parametric conversion
 */
void car2par( const int Nver,
	      const int * const vFlags,
	      const double * const cartesian,
	      double * parametric )
{
  int i, counter = 0, segmentID;
  double theta, x, y, typicalLengthSq;
  double dTheta = 1.0 / (double)(_CIRCLE_SEGMENTS);

  x = _CYLINDER_RADIUS - _CYLINDER_RADIUS * cos(2.0 * M_PI * dTheta);
  y = _CYLINDER_RADIUS * sin(2.0 * M_PI * dTheta);
  typicalLengthSq = x*x + y*y;

  for (i = 0; i < Nver; ++i) {
    if ( vFlags[i] == -1 )
      continue;
    if ( vFlags[i] == 0 ) {
      parametric[counter]   = sqrt( cartesian[3*i]*cartesian[3*i]
				    + cartesian[3*i+1]*cartesian[3*i+1] );
      parametric[counter+1] = atan2( cartesian[3*i+1], cartesian[3*i] ) / (2.0 * M_PI);
      parametric[counter+2] = cartesian[3*i+2];
      counter += 3;
    }
    else if ( ( vFlags[i] == 1 ) || ( vFlags[i] == 2 ) ) {
      parametric[counter]   = sqrt( cartesian[3*i]*cartesian[3*i]
				    + cartesian[3*i+1]*cartesian[3*i+1] );
      parametric[counter+1] = atan2( cartesian[3*i+1], cartesian[3*i] ) / (2.0 * M_PI);
      counter += 2;
    }
    else if ( ( vFlags[i]%4 == 3 ) || ( vFlags[i]%4 == 0 ) ) {
      segmentID = ( vFlags[i] + (vFlags[i]%4)/3 ) / 4;
      theta     = ( (double)(segmentID-1) ) * dTheta;
      x = _CYLINDER_RADIUS * cos(2.0 * M_PI * theta) - cartesian[3*i];
      y = _CYLINDER_RADIUS * sin(2.0 * M_PI * theta) - cartesian[3*i+1];
      parametric[counter] = sqrt( (x*x + y*y) / typicalLengthSq );
      ++counter;
    }
    else if ( vFlags[i]%4 == 1 ) {
      parametric[counter] = cartesian[3*i+2] / _CYLINDER_HEIGHT;
      ++counter;
    }
    else {
      segmentID = ( vFlags[i] - 2 ) / 4;
      theta     = ( (double)(segmentID-1) ) * dTheta;
      x = _CYLINDER_RADIUS * cos(2.0 * M_PI * theta) - cartesian[3*i];
      y = _CYLINDER_RADIUS * sin(2.0 * M_PI * theta) - cartesian[3*i+1];
      parametric[counter]   = sqrt( (x*x + y*y) / typicalLengthSq );
      parametric[counter+1] = cartesian[3*i+2] / _CYLINDER_HEIGHT;
      counter += 2;
    }
  }
}


/*
 * Indices mapping each cartesian vertex to its first corresponding parameter
 */
void car2firstParIdx( const int Nver,
		      const int * const vFlags,
		      int * const c2firstpIdx )
{
  int i, counter = 0;

  for (i = 0; i < Nver; ++i) {
    if ( vFlags[i] == -1 )
      c2firstpIdx[i] = -1;
    else if ( vFlags[i] == 0 ) {
      c2firstpIdx[i] = counter;
      counter += 3;
    }
    else if ( ( vFlags[i] == 1 ) || ( vFlags[i] == 2 ) || ( vFlags[i]%4 == 2 ) ) {
      c2firstpIdx[i] = counter;
      counter += 2;
    }
    else {
      c2firstpIdx[i] = counter;
      ++counter;
    }
  }
}


/*
 * Parametric to cartesian conversion
 */
void par2car( const double * const parametric,
	      double * const cartesian,
	      const int Nver,
	      const int * const cartesianIdx,
	      const int cartesianIdxDim,
	      const int * const c2firstpIdx,
	      const int * const vFlags )
{
  int i, j, segmentID, N;
  double theta, ax, ay, bx, by, dTheta = 1.0 / (double)(_CIRCLE_SEGMENTS);

  N = cartesianIdx == NULL ? Nver : cartesianIdxDim;
  for (i = 0; i < N; ++i) {
    j = cartesianIdx == NULL ? i : cartesianIdx[i];
    if ( vFlags[j] == -1 )
      continue;
    else if ( vFlags[j] == 0 ) {
      cartesian[3*j]   =
	parametric[c2firstpIdx[j]] * cos(2.0 * M_PI * parametric[c2firstpIdx[j]+1]);
      cartesian[3*j+1] =
	parametric[c2firstpIdx[j]] * sin(2.0 * M_PI * parametric[c2firstpIdx[j]+1]);
      cartesian[3*j+2] = parametric[c2firstpIdx[j]+2];
    }
    else if ( ( vFlags[j] == 1 ) || ( vFlags[j] == 2 ) ) {
      cartesian[3*j]   =
	parametric[c2firstpIdx[j]] * cos(2.0 * M_PI * parametric[c2firstpIdx[j]+1]);
      cartesian[3*j+1] =
	parametric[c2firstpIdx[j]] * sin(2.0 * M_PI * parametric[c2firstpIdx[j]+1]);
    }
    else if ( ( vFlags[j]%4 == 3 ) || ( vFlags[j]%4 == 0 ) ) {
      segmentID = ( vFlags[j] + (vFlags[j]%4)/3 ) / 4;
      theta     = ( (double)(segmentID-1) ) * dTheta;
      ax = _CYLINDER_RADIUS * cos(2.0 * M_PI * theta);
      ay = _CYLINDER_RADIUS * sin(2.0 * M_PI * theta);
      bx = _CYLINDER_RADIUS * cos(2.0 * M_PI * (theta+dTheta));
      by = _CYLINDER_RADIUS * sin(2.0 * M_PI * (theta+dTheta));
      cartesian[3*j]   = ax + parametric[c2firstpIdx[j]] * (bx - ax);
      cartesian[3*j+1] = ay + parametric[c2firstpIdx[j]] * (by - ay);
    }
    else if ( vFlags[j]%4 == 1 ) {
      cartesian[3*j+2] = _CYLINDER_HEIGHT * parametric[c2firstpIdx[j]];
    }
    else {
      segmentID = ( vFlags[j] - 2 ) / 4;
      theta     = ( (double)(segmentID-1) ) * dTheta;
      ax = _CYLINDER_RADIUS * cos(2.0 * M_PI * theta);
      ay = _CYLINDER_RADIUS * sin(2.0 * M_PI * theta);
      bx = _CYLINDER_RADIUS * cos(2.0 * M_PI * (theta+dTheta));
      by = _CYLINDER_RADIUS * sin(2.0 * M_PI * (theta+dTheta));
      cartesian[3*j]   = ax + parametric[c2firstpIdx[j]] * (bx - ax);
      cartesian[3*j+1] = ay + parametric[c2firstpIdx[j]] * (by - ay);
      cartesian[3*j+2] = _CYLINDER_HEIGHT * parametric[c2firstpIdx[j]+1];
    }
  }
}


/*
 * Indices mapping each parameter to the corresponding cartesian vertex
 */
void par2carIdx( const int Nver,
		 const int * const vFlags,
		 int * const p2cIdx )
{
  int i, counter = 0;

  for (i = 0; i < Nver; ++i) {
    if ( vFlags[i] == -1 )
      continue;
    else if ( vFlags[i] == 0 ) {
      p2cIdx[counter]   = i;
      p2cIdx[counter+1] = i;
      p2cIdx[counter+2] = i;
      counter += 3;
    }
    else if ( ( vFlags[i] == 1 ) || ( vFlags[i] == 2 ) || ( vFlags[i]%4 == 2 ) ) {
      p2cIdx[counter]   = i;
      p2cIdx[counter+1] = i;
      counter += 2;
    }
    else {
      p2cIdx[counter] = i;
      ++counter;
    }
  }
}
