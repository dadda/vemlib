
### This file might be required if Steiner points are added by Tetgen,
### in which case a proper preliminary mark must be assigned to them.

import sys
import salome
import shutil

salome.salome_init()
theStudy = salome.myStudy

import salome_notebook
notebook = salome_notebook.NoteBook(theStudy)
sys.path.insert( 0, r'/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/tests/cylinder')

###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS
import numpy as np

geompy = geomBuilder.New(theStudy)

O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)
geomObj_1 = geompy.MakeMarker(0, 0, 0, 1, 0, 0, 0, 1, 0)

radius      = 2.0
numSegments = 20
theta       = np.linspace(0.0, 2.0*math.pi, numSegments+1)
theta       = theta[:-1]

BottomVertexList  = [ geompy.MakeVertex(radius*math.cos(t), radius*math.sin(t), 0.0) \
                      for t in theta ]
UpperVertexList   = [ geompy.MakeVertex(radius*math.cos(t), radius*math.sin(t), 10.0) \
                      for t in theta ]
VertexCompound    = geompy.MakeCompound( BottomVertexList + UpperVertexList )
numBottomVertices = len(BottomVertexList)

BottomEdgeList   = [ geompy.MakeLineTwoPnt( BottomVertexList[i], \
                                            BottomVertexList[(i+1)%numBottomVertices] ) \
                     for i in range(numBottomVertices) ]
VerticalEdgeList = [ geompy.MakeLineTwoPnt( BottomVertexList[i], \
                                            UpperVertexList[i] ) \
                     for i in range(numBottomVertices) ]
UpperEdgeList    = [ geompy.MakeLineTwoPnt( UpperVertexList[i], \
                                            UpperVertexList[(i+1)%numBottomVertices] ) \
                     for i in range(numBottomVertices) ]
EdgeCompound       = geompy.MakeCompound( BottomEdgeList + VerticalEdgeList + UpperEdgeList )
BottomEdgeCompound = geompy.MakeCompound( BottomEdgeList )
UpperEdgeCompound  = geompy.MakeCompound( UpperEdgeList )

Curve_1 = geompy.MakePolyline( BottomVertexList, True )
Floor   = geompy.MakeFaceWires([Curve_1], 1)
VerticalFaceList = \
    [ geompy.MakeFaceWires( [ \
                              geompy.MakePolyline( [ BottomVertexList[i], \
                                                     BottomVertexList[(i+1)%numBottomVertices], \
                                                     UpperVertexList[(i+1)%numBottomVertices], \
                                                     UpperVertexList[i] ], True ) ], 1 ) \
      for i in range(numBottomVertices) ]
Curve_2 = geompy.MakePolyline( UpperVertexList, True )
Roof    = geompy.MakeFaceWires([Curve_2], 1)
FaceCompound = geompy.MakeCompound( [Floor] + VerticalFaceList + [Roof] )

geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( Floor, 'Floor' )
geompy.addToStudy( Roof, 'Roof' )
geompy.addToStudy( FaceCompound, 'FaceCompound' )
geompy.addToStudy( EdgeCompound, 'EdgeCompound' )
geompy.addToStudy( VertexCompound, 'VertexCompound' )
geompy.addToStudy( BottomEdgeCompound, 'BottomEdgeCompound' )
geompy.addToStudy( UpperEdgeCompound, 'UpperEdgeCompound' )

###
### SMESH component
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

#
# Read dual mesh
#

dualBasename = r'/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/tests/' \
               r'cylinder/cylinder2.1_marked.dual'

whichFlag = 'all'    # Other choices: 'boundary', 'internal'

node = np.loadtxt( dualBasename + '.node', dtype='float64', skiprows=1, usecols=(0,1,2) )
Nver = node.shape[0]

edges = np.loadtxt( dualBasename + '.edge', dtype='int64', skiprows=1, usecols=(0,1) )
Ned   = edges.shape[0]

fid = open( dualBasename + '.face', 'r' )
lines = fid.readlines()[1:]
fid.close()

Nfc  = len(lines)
f2he = [0] * Nfc
f2p  = np.zeros([Nfc,2])
for i,line in enumerate(lines):
    tmp = line.split()
    NedLoc   = np.int64( tmp[0] )
    f2he[i]  = np.int64( [ j for j in tmp[1:(NedLoc+1)] ] )
    f2p[i,:] = [ tmp[NedLoc+1], tmp[NedLoc+2] ]

fid = open( dualBasename + '.polyhedron', 'r' )
lines = fid.readlines()[1:]
fid.close()

p2hf = [ np.int64( [ i for i in line.split()[1:] ] ) for line in lines ]
Npol = len(p2hf)

#
# Create polyhedral mesh
#

smesh = smeshBuilder.New(theStudy)
Mesh_1 = smesh.Mesh()

for i in range(Nver):
    Mesh_1.AddNode( node[i,0], node[i,1], node[i,2] )

for i in range(Npol):
    hFaces = p2hf[i]
    tFaces = hFaces/2
    NfcLoc = len(hFaces)
    polyhedron = [0] * NfcLoc
    NedByFace  = [0] * NfcLoc
    for j in range(NfcLoc):
        hEdges = f2he[tFaces[j]]
        tEdges = hEdges/2
        edgesOrientation = hEdges%2
        edgesLoc = edges[tEdges,:].T
        polyhedron[j] = (1 - hFaces[j]%2) * ( (1-edgesOrientation) * edgesLoc[0,:] \
                                              + edgesOrientation * edgesLoc[1,:] ) \
                        + (hFaces[j]%2) * ( (1-edgesOrientation[-1::-1]) * edgesLoc[1,-1::-1] \
                                            + edgesOrientation[-1::-1] * edgesLoc[0,-1::-1] )
        polyhedron[j] = polyhedron[j] + 1
        NedByFace[j] = len(polyhedron[j])
    Mesh_1.AddPolyhedralVolume( np.hstack(polyhedron).tolist(), NedByFace )

#
# Add all faces
#

for i in range(Nfc):
    hEdges = f2he[i]
    tEdges = hEdges/2
    edgesOrientation = hEdges%2
    edgesLoc = edges[tEdges,:].T
    face = (1-edgesOrientation) * edgesLoc[0,:] + edgesOrientation * edgesLoc[1,:]
    face = face + 1
    Mesh_1.AddPolygonalFace( face.tolist() )

all_nodes = Mesh_1.CreateEmptyGroup( SMESH.NODE, 'all_nodes' )
nbAdd = all_nodes.AddFrom( Mesh_1.GetMesh() )
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,FaceCompound)
aCriteria.append(aCriterion)
aFilter_1 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_1.SetMesh(Mesh_1.GetMesh())
boundary_nodes = Mesh_1.GroupOnFilter( SMESH.NODE, 'boundary_nodes', aFilter_1 )
internal_nodes = Mesh_1.GetMesh().CutListOfGroups( [ all_nodes ], [ boundary_nodes ], 'internal_nodes' )
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,VertexCompound)
aCriteria.append(aCriterion)
aFilter_2 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_2.SetMesh(Mesh_1.GetMesh())
fixed_nodes = Mesh_1.GroupOnFilter( SMESH.NODE, 'fixed_nodes', aFilter_2 )
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,BottomEdgeCompound)
aCriteria.append(aCriterion)
aFilter_3 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_3.SetMesh(Mesh_1.GetMesh())
nodes_on_bottom_edge_compound = Mesh_1.GroupOnFilter( SMESH.NODE, 'nodes_on_bottom_edge_compound', aFilter_3 )
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,UpperEdgeCompound)
aCriteria.append(aCriterion)
aFilter_4 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_4.SetMesh(Mesh_1.GetMesh())
nodes_on_upper_edge_compound = Mesh_1.GroupOnFilter( SMESH.NODE, 'nodes_on_upper_edge_compound', aFilter_4 )
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,Floor)
aCriteria.append(aCriterion)
aFilter_5 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_5.SetMesh(Mesh_1.GetMesh())
floor_nodes = Mesh_1.GroupOnFilter( SMESH.NODE, 'floor_nodes', aFilter_5 )
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,Roof)
aCriteria.append(aCriterion)
aFilter_6 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_6.SetMesh(Mesh_1.GetMesh())
roof_nodes = Mesh_1.GroupOnFilter( SMESH.NODE, 'roof_nodes', aFilter_6 )
internal_floor_nodes = Mesh_1.GetMesh().CutListOfGroups( [ floor_nodes ], [ nodes_on_bottom_edge_compound ], 'internal_floor_nodes' )
internal_roof_nodes = Mesh_1.GetMesh().CutListOfGroups( [ roof_nodes ], [ nodes_on_upper_edge_compound ], 'internal_roof_nodes' )

nodes_on_bottom_edges = [0] * numBottomVertices
internal_nodes_on_bottom_edges = [0] * numBottomVertices

nodes_on_upper_edges = [0] * numBottomVertices
internal_nodes_on_upper_edges = [0] * numBottomVertices

nodes_on_vertical_edges = [0] * numBottomVertices
internal_nodes_on_vertical_edges = [0] * numBottomVertices
for i in range(numBottomVertices):
    aCriteria = []
    aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,\
                                    SMESH.FT_Undefined,BottomEdgeList[i])
    aCriteria.append(aCriterion)
    aFilter = smesh.GetFilterFromCriteria(aCriteria)
    aFilter.SetMesh(Mesh_1.GetMesh())
    nodes_on_bottom_edges[i] = Mesh_1.GroupOnFilter( \
                                            SMESH.NODE, \
                                            'nodes_on_bottom_edges_' + str(i+1), \
                                            aFilter )
    internal_nodes_on_bottom_edges[i] = \
        Mesh_1.GetMesh().CutListOfGroups( [ nodes_on_bottom_edges[i] ], [ fixed_nodes ], \
                                          'internal_nodes_on_bottom_edges_' + str(i+1) )
    aCriteria = []
    aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,\
                                    SMESH.FT_Undefined,UpperEdgeList[i])
    aCriteria.append(aCriterion)
    aFilter = smesh.GetFilterFromCriteria(aCriteria)
    aFilter.SetMesh(Mesh_1.GetMesh())
    nodes_on_upper_edges[i] = Mesh_1.GroupOnFilter( \
                                            SMESH.NODE, \
                                            'nodes_on_upper_edges_' + str(i+1), \
                                            aFilter )
    internal_nodes_on_upper_edges[i] = \
        Mesh_1.GetMesh().CutListOfGroups( [ nodes_on_upper_edges[i] ], [ fixed_nodes ], \
                                          'internal_nodes_on_upper_edges_' + str(i+1) )
    aCriteria = []
    aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,\
                                    SMESH.FT_Undefined,VerticalEdgeList[i])
    aCriteria.append(aCriterion)
    aFilter = smesh.GetFilterFromCriteria(aCriteria)
    aFilter.SetMesh(Mesh_1.GetMesh())
    nodes_on_vertical_edges[i] = Mesh_1.GroupOnFilter( \
                                            SMESH.NODE, \
                                            'nodes_on_vertical_edges_' + str(i+1), \
                                            aFilter )
    internal_nodes_on_vertical_edges[i] = \
        Mesh_1.GetMesh().CutListOfGroups( [ nodes_on_vertical_edges[i] ], \
                                          [ fixed_nodes ], \
                                          'internal_nodes_on_vertical_edges_' + str(i+1) )

nodes_on_vertical_faces = [0] * numBottomVertices
internal_nodes_on_vertical_faces = [0] * numBottomVertices
for i in range(numBottomVertices):
    aCriteria = []
    aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,\
                                    SMESH.FT_Undefined,VerticalFaceList[i])
    aCriteria.append(aCriterion)
    aFilter = smesh.GetFilterFromCriteria(aCriteria)
    aFilter.SetMesh(Mesh_1.GetMesh())
    nodes_on_vertical_faces[i] = Mesh_1.GroupOnFilter( \
                                            SMESH.NODE, \
                                            'nodes_on_vertical_faces_' + str(i+1), \
                                            aFilter )
    internal_nodes_on_vertical_faces[i] = \
        Mesh_1.GetMesh().CutListOfGroups( [ nodes_on_vertical_faces[i] ], \
                                          [ fixed_nodes, \
                                            internal_nodes_on_bottom_edges[i], \
                                            internal_nodes_on_upper_edges[i], \
                                            internal_nodes_on_vertical_edges[i], \
                                            internal_nodes_on_vertical_edges[(i+1)%numBottomVertices] ], \
                                          'internal_nodes_on_vertical_faces_' + str(i+1) )


#
# Assign markers for mesh optimization
#

# Start by fixing all the nodes
all_nodes_flags      = [-1] * Mesh_1.NbNodes()
boundary_nodes_flags = [-1] * Mesh_1.NbNodes()
internal_nodes_flags = [-1] * Mesh_1.NbNodes()

for i in range(internal_nodes.GetNumberOfNodes()):
    nodeID = internal_nodes.GetID(i+1)
    all_nodes_flags[nodeID-1]      = 0
    internal_nodes_flags[nodeID-1] = 0
    
for i in range(internal_floor_nodes.GetNumberOfNodes()):
    nodeID = internal_floor_nodes.GetID(i+1)
    all_nodes_flags[nodeID-1]      = 1
    boundary_nodes_flags[nodeID-1] = 1

for i in range(internal_roof_nodes.GetNumberOfNodes()):
    nodeID = internal_roof_nodes.GetID(i+1)
    all_nodes_flags[nodeID-1]      = 2
    boundary_nodes_flags[nodeID-1] = 2

current_flag = 2
for i in range(numBottomVertices):
    current_flag = current_flag + 1
    for j in range(internal_nodes_on_bottom_edges[i].GetNumberOfNodes()):
        nodeID = internal_nodes_on_bottom_edges[i].GetID(j+1)
        all_nodes_flags[nodeID-1]      = current_flag
        boundary_nodes_flags[nodeID-1] = current_flag
    #
    current_flag = current_flag + 1
    for j in range(internal_nodes_on_upper_edges[i].GetNumberOfNodes()):
        nodeID = internal_nodes_on_upper_edges[i].GetID(j+1)
        all_nodes_flags[nodeID-1]      = current_flag
        boundary_nodes_flags[nodeID-1] = current_flag
    #
    current_flag = current_flag + 1
    for j in range(internal_nodes_on_vertical_edges[i].GetNumberOfNodes()):
        nodeID = internal_nodes_on_vertical_edges[i].GetID(j+1)
        all_nodes_flags[nodeID-1]      = current_flag
        boundary_nodes_flags[nodeID-1] = current_flag
    #
    current_flag = current_flag + 1
    for j in range(internal_nodes_on_vertical_faces[i].GetNumberOfNodes()):
        nodeID = internal_nodes_on_vertical_faces[i].GetID(j+1)
        all_nodes_flags[nodeID-1]      = current_flag
        boundary_nodes_flags[nodeID-1] = current_flag


fid = open(dualBasename + '_marked.node', 'w')
fid.write(str(Nver) + ' 1\n')
for i in range( Nver ):
    if whichFlag == 'all':
        fid.write(' '.join('%.16f' % coord for coord in Mesh_1.GetNodeXYZ(i+1)) + ' ' + str(all_nodes_flags[i]) + '\n')
    elif whichFlag == 'boundary':
        fid.write(' '.join('%.16f' % coord for coord in Mesh_1.GetNodeXYZ(i+1)) + ' ' + str(boundary_nodes_flags[i]) + '\n')
    elif whichFlag == 'internal':
        fid.write(' '.join('%.16f' % coord for coord in Mesh_1.GetNodeXYZ(i+1)) + ' ' + str(internal_nodes_flags[i]) + '\n')
fid.close()

shutil.copy(dualBasename + '.edge', dualBasename + '_marked.edge')
shutil.copy(dualBasename + '.face', dualBasename + '_marked.face')
shutil.copy(dualBasename + '.polyhedron', dualBasename + '_marked.polyhedron')
shutil.copy(dualBasename + '.v2p', dualBasename + '_marked.v2p')

np.savetxt(dualBasename + '_marked.all_nodes_flags', all_nodes_flags, fmt='%i', header=str(Nver), comments='')
np.savetxt(dualBasename + '_marked.boundary_nodes_flags', boundary_nodes_flags, fmt='%i', header=str(Nver), comments='')
np.savetxt(dualBasename + '_marked.internal_nodes_flags', internal_nodes_flags, fmt='%i', header=str(Nver), comments='')

## Set names of Mesh objects
smesh.SetName(internal_roof_nodes, 'internal_roof_nodes')
smesh.SetName(Mesh_1.GetMesh(), 'Mesh_1')
smesh.SetName(roof_nodes, 'roof_nodes')
smesh.SetName(internal_floor_nodes, 'internal_floor_nodes')
smesh.SetName(boundary_nodes, 'boundary_nodes')
smesh.SetName(internal_nodes, 'internal_nodes')
smesh.SetName(all_nodes, 'all_nodes')
smesh.SetName(nodes_on_upper_edge_compound, 'nodes_on_upper_edge_compound')
smesh.SetName(floor_nodes, 'floor_nodes')
smesh.SetName(fixed_nodes, 'fixed_nodes')
smesh.SetName(nodes_on_bottom_edge_compound, 'nodes_on_bottom_edge_compound')
for i in range(numBottomVertices):
    smesh.SetName(internal_nodes_on_bottom_edges[i], 'internal_nodes_on_bottom_edges_' + str(i+1))
    smesh.SetName(internal_nodes_on_upper_edges[i], 'internal_nodes_on_upper_edges_' + str(i+1))
    smesh.SetName(internal_nodes_on_vertical_edges[i], 'internal_nodes_on_vertical_edges_' + str(i+1))
    smesh.SetName(internal_nodes_on_vertical_faces[i], 'internal_nodes_on_vertical_faces_' + str(i+1))


if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser(True)
