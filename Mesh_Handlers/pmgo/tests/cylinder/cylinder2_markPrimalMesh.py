
### This file might be required if Steiner points are added by Tetgen,
### in which case a proper preliminary mark must be assigned to them.

import sys
import salome
import shutil

salome.salome_init()
theStudy = salome.myStudy

import salome_notebook
notebook = salome_notebook.NoteBook(theStudy)
sys.path.insert( 0, r'/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/tests/cylinder')

###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS
import numpy as np

geompy = geomBuilder.New(theStudy)

O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)
geomObj_1 = geompy.MakeMarker(0, 0, 0, 1, 0, 0, 0, 1, 0)

radius      = 2.0
numSegments = 20
theta       = np.linspace(0.0, 2.0*math.pi, numSegments+1)
theta       = theta[:-1]

BottomVertexList = [ geompy.MakeVertex(radius*math.cos(t), radius*math.sin(t), 0.0) for t in theta ]
typicalLength    = math.sqrt( (radius-radius*math.cos(theta[1]))**2 + \
                              (radius*math.sin(theta[1]))**2 )
Curve_1 = geompy.MakePolyline( BottomVertexList, True )
Face_1  = geompy.MakeFaceWires([Curve_1], 1)
Extrusion_1 = geompy.MakePrismVecH(Face_1, OZ, 10)

FaceList     = geompy.ExtractShapes(Extrusion_1, geompy.ShapeType["FACE"], True)
FaceCompound = geompy.MakeCompound(FaceList)

EdgeList     = geompy.ExtractShapes(Extrusion_1, geompy.ShapeType["EDGE"], True)
EdgeCompound = geompy.MakeCompound(EdgeList)

VertexList     = geompy.ExtractShapes(Extrusion_1, geompy.ShapeType["VERTEX"], True)
VertexCompound = geompy.MakeCompound(VertexList)

geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( Curve_1, 'Curve_1' )
geompy.addToStudy( Face_1, 'Face_1' )
geompy.addToStudy( Extrusion_1, 'Extrusion_1' )
geompy.addToStudy( FaceCompound, 'FaceCompound' )
geompy.addToStudy( EdgeCompound, 'EdgeCompound' )
geompy.addToStudy( VertexCompound, 'VertexCompound' )


###
### SMESH component - Load it from Tetgen output
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

tetgenBasename = r'/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/tests/' \
                 r'cylinder/cylinder2.1'
newTetgenBasename = r'/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/tests/' \
                    r'cylinder/cylinder2.1_marked'

smesh = smeshBuilder.New(theStudy)
Mesh_1 = smesh.Mesh()

#
# Read tetgen mesh
#

node = np.loadtxt( tetgenBasename + '.node', dtype='float64', skiprows=1, usecols=(1,2,3) )
Nver = node.shape[0]

ele  = np.loadtxt( tetgenBasename + '.ele', dtype='int64', skiprows=1, usecols=(1,2,3,4) )
Nelt = ele.shape[0]

face = np.loadtxt( tetgenBasename + '.face', dtype='int64', skiprows=1, usecols=(1,2,3,4,5,6) )
f2e = np.loadtxt( tetgenBasename + '.f2e', dtype='int64', usecols=(1,2,3) )
Nfc  = face.shape[0]

edge = np.loadtxt( tetgenBasename + '.edge', dtype='int64', skiprows=1, usecols=(1,2,3,4) )
Ned  = edge.shape[0]

for i in range(Nver):
    Mesh_1.AddNode( node[i,0], node[i,1], node[i,2] )

for i in range(Nelt):
    Mesh_1.AddVolume( [ ele[i,0], ele[i,1], ele[i,2], ele[i,3] ] )

for i in range(Nfc):
    Mesh_1.AddFace( [ face[i,0], face[i,1], face[i,2] ] )

for i in range(Ned):
    Mesh_1.AddEdge( [ edge[i,0], edge[i,1] ] )

all_nodes = Mesh_1.CreateEmptyGroup( SMESH.NODE, 'all_nodes' )
nbAdd = all_nodes.AddFrom( Mesh_1.GetMesh() )
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,FaceCompound)
aCriteria.append(aCriterion)
aFilter_1 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_1.SetMesh(Mesh_1.GetMesh())
boundary_nodes = Mesh_1.GroupOnFilter( SMESH.NODE, 'boundary_nodes', aFilter_1 )
internal_nodes = Mesh_1.GetMesh().CutListOfGroups( [ all_nodes ], [ boundary_nodes ], 'internal_nodes' )
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,VertexCompound)
aCriteria.append(aCriterion)
aFilter_2 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_2.SetMesh(Mesh_1.GetMesh())
nodes_type_303 = Mesh_1.GroupOnFilter( SMESH.NODE, 'nodes_type_303', aFilter_2 )
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,EdgeCompound)
aCriteria.append(aCriterion)
aFilter_3 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_3.SetMesh(Mesh_1.GetMesh())
nodes_on_edges = Mesh_1.GroupOnFilter( SMESH.NODE, 'nodes_on_edges', aFilter_3 )
nodes_type_202 = Mesh_1.GetMesh().CutListOfGroups( [ nodes_on_edges ], [ nodes_type_303 ], 'nodes_type_202' )
nodes_type_101 = Mesh_1.GetMesh().CutListOfGroups( [ boundary_nodes ], [ nodes_on_edges ], 'nodes_type_101' )
all_edges = Mesh_1.CreateEmptyGroup( SMESH.EDGE, 'all_edges' )
nbAdd = all_edges.AddFrom( Mesh_1.GetMesh() )
all_faces = Mesh_1.CreateEmptyGroup( SMESH.FACE, 'all_faces' )
nbAdd = all_faces.AddFrom( Mesh_1.GetMesh() )

tmp,     = np.where( face[:,3] == -1 )
bndEdges = f2e[ tmp, : ].ravel()
bndEdges = set( bndEdges ) # Repetitions are removed here, indices start from 1
bndEdges = np.array( [ i for i in bndEdges ] ) + Nelt + Nfc
boundary_edges = Mesh_1.CreateEmptyGroup( SMESH.EDGE, 'boundary_edges' )
nbAdd = boundary_edges.Add( bndEdges.tolist() )
internal_edges = Mesh_1.GetMesh().CutListOfGroups( [ all_edges ], [ boundary_edges ], 'internal_edges' )

tmp = tmp + 1 + Nelt
boundary_faces = Mesh_1.CreateEmptyGroup( SMESH.FACE, 'boundary_faces' )
nbAdd = boundary_faces.Add( tmp.tolist() )
internal_faces = Mesh_1.GetMesh().CutListOfGroups( [ all_faces ], [ boundary_faces ], 'internal_faces' )

tmp, = np.where( edge[:,2] == -1 )
tmp = np.array( tmp ) + 1 + Nelt + Nfc
edges_type_2 = Mesh_1.CreateEmptyGroup( SMESH.EDGE, 'edges_type_2' )
nbAdd = edges_type_2.Add( tmp.tolist() )
edges_type_1 = Mesh_1.GetMesh().CutListOfGroups( [ boundary_edges ], [ edges_type_2 ], 'edges_type_1' )


all_nodes_flags = [0] * Mesh_1.NbNodes()
for i in range(nodes_type_101.GetNumberOfNodes()):
  nodeID = nodes_type_101.GetID(i+1)
  all_nodes_flags[nodeID-1] = 101

for i in range(nodes_type_202.GetNumberOfNodes()):
  nodeID = nodes_type_202.GetID(i+1)
  all_nodes_flags[nodeID-1] = 202

for i in range(nodes_type_303.GetNumberOfNodes()):
  nodeID = nodes_type_303.GetID(i+1)
  all_nodes_flags[nodeID-1] = 303

fid = open(newTetgenBasename + '.node', 'w')
fid.write(str(Mesh_1.NbNodes()) + ' 3 0 1\n')
for i in range( Mesh_1.NbNodes() ):
  fid.write(str(i+1) + ' ' + ' '.join('%.16f' % coord for coord in Mesh_1.GetNodeXYZ(i+1)) + ' ' + str(all_nodes_flags[i]) + '\n')
fid.close()

shutil.copy(tetgenBasename + '.ele', newTetgenBasename + '.ele')

all_faces_flags = [0] * Mesh_1.NbFaces()
for i in range(len(boundary_faces.GetIDs())):
  faceID = boundary_faces.GetID(i+1) - Nelt
  all_faces_flags[faceID-1] = 1

fid = open(newTetgenBasename + '.face', 'w')
fid.write(str(Mesh_1.NbFaces()) + ' 1\n')
for i in range( Mesh_1.NbFaces() ):
  fid.write(str(i+1) + ' ' + str(face[i,0]) + ' ' + str(face[i,1]) + ' ' + str(face[i,2]) + ' ' + \
            str(all_faces_flags[i]) + ' ' + str(face[i,4]) + ' ' + str(face[i,5]) + '\n')
fid.close()

all_edges_flags = [0] * Mesh_1.NbEdges()
for i in range(len(edges_type_1.GetIDs())):
  edgeID = edges_type_1.GetID(i+1) - Nelt - Nfc
  all_edges_flags[edgeID-1] = 1
for i in range(len(edges_type_2.GetIDs())):
  edgeID = edges_type_2.GetID(i+1) - Nelt - Nfc
  all_edges_flags[edgeID-1] = 2

fid = open(newTetgenBasename + '.edge', 'w')
fid.write(str(Mesh_1.NbEdges()) + ' 1\n')
for i in range( Mesh_1.NbEdges() ):
  fid.write(str(i+1) + ' ' + str(edge[i,0]) + ' ' + str(edge[i,1]) + ' ' + str(all_edges_flags[i]) \
            + ' ' + str(edge[i,3]) + '\n')
fid.close()

## Set names of Mesh objects
smesh.SetName(Mesh_1.GetMesh(), 'Mesh_1')

smesh.SetName(all_faces, 'all_faces')
smesh.SetName(boundary_faces, 'boundary_faces')
smesh.SetName(internal_faces, 'internal_faces')

smesh.SetName(all_edges, 'all_edges')
smesh.SetName(boundary_edges, 'boundary_edges')
smesh.SetName(internal_edges, 'internal_edges')
smesh.SetName(edges_type_2, 'edges_type_2')
smesh.SetName(edges_type_1, 'edges_type_1')

smesh.SetName(all_nodes, 'all_nodes')
smesh.SetName(boundary_nodes, 'boundary_nodes')
smesh.SetName(internal_nodes, 'internal_nodes')
smesh.SetName(nodes_type_202, 'nodes_type_202')
smesh.SetName(nodes_type_101, 'nodes_type_101')
smesh.SetName(nodes_type_303, 'nodes_type_303')
smesh.SetName(nodes_on_edges, 'nodes_on_edges')


if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser(True)
