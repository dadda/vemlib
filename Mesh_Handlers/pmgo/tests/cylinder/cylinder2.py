import sys
import salome

salome.salome_init()
theStudy = salome.myStudy

import salome_notebook
notebook = salome_notebook.NoteBook(theStudy)
sys.path.insert( 0, r'/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/tests/cylinder')

###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS
import numpy as np

geompy = geomBuilder.New(theStudy)

O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)
geomObj_1 = geompy.MakeMarker(0, 0, 0, 1, 0, 0, 0, 1, 0)

radius      = 2.0
numSegments = 20
theta       = np.linspace(0.0, 2.0*math.pi, numSegments+1)
theta       = theta[:-1]

BottomVertexList = [ geompy.MakeVertex(radius*math.cos(t), radius*math.sin(t), 0.0) for t in theta ]
typicalLength    = math.sqrt( (radius-radius*math.cos(theta[1]))**2 + \
                              (radius*math.sin(theta[1]))**2 )
Curve_1 = geompy.MakePolyline( BottomVertexList, True )
Face_1  = geompy.MakeFaceWires([Curve_1], 1)
Extrusion_1 = geompy.MakePrismVecH(Face_1, OZ, 10)

FaceList     = geompy.ExtractShapes(Extrusion_1, geompy.ShapeType["FACE"], True)
FaceCompound = geompy.MakeCompound(FaceList)

EdgeList     = geompy.ExtractShapes(Extrusion_1, geompy.ShapeType["EDGE"], True)
EdgeCompound = geompy.MakeCompound(EdgeList)

VertexList     = geompy.ExtractShapes(Extrusion_1, geompy.ShapeType["VERTEX"], True)
VertexCompound = geompy.MakeCompound(VertexList)

geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( Curve_1, 'Curve_1' )
geompy.addToStudy( Face_1, 'Face_1' )
geompy.addToStudy( Extrusion_1, 'Extrusion_1' )
geompy.addToStudy( FaceCompound, 'FaceCompound' )
geompy.addToStudy( EdgeCompound, 'EdgeCompound' )
geompy.addToStudy( VertexCompound, 'VertexCompound' )

###
### SMESH component
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

tetgenBasename = r'/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/tests/' \
                 r'cylinder/cylinder2'

smesh = smeshBuilder.New(theStudy)
Mesh_1 = smesh.Mesh(Extrusion_1)
Regular_1D = Mesh_1.Segment()
Local_Length_1 = Regular_1D.LocalLength(0.625738,None,0.5)
MEFISTO_2D = Mesh_1.Triangle(algo=smeshBuilder.MEFISTO)
NETGEN_3D = Mesh_1.Tetrahedron()
isDone = Mesh_1.Compute()
all_nodes = Mesh_1.CreateEmptyGroup( SMESH.NODE, 'all_nodes' )
nbAdd = all_nodes.AddFrom( Mesh_1.GetMesh() )
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,FaceCompound)
aCriteria.append(aCriterion)
aFilter_1 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_1.SetMesh(Mesh_1.GetMesh())
boundary_nodes = Mesh_1.GroupOnFilter( SMESH.NODE, 'boundary_nodes', aFilter_1 )
internal_nodes = Mesh_1.GetMesh().CutListOfGroups( [ all_nodes ], [ boundary_nodes ], 'internal_nodes' )
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,VertexCompound)
aCriteria.append(aCriterion)
aFilter_2 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_2.SetMesh(Mesh_1.GetMesh())
nodes_type_303 = Mesh_1.GroupOnFilter( SMESH.NODE, 'nodes_type_303', aFilter_2 )
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,EdgeCompound)
aCriteria.append(aCriterion)
aFilter_3 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_3.SetMesh(Mesh_1.GetMesh())
nodes_on_edges = Mesh_1.GroupOnFilter( SMESH.NODE, 'nodes_on_edges', aFilter_3 )
nodes_type_202 = Mesh_1.GetMesh().CutListOfGroups( [ nodes_on_edges ], [ nodes_type_303 ], 'nodes_type_202' )
nodes_type_101 = Mesh_1.GetMesh().CutListOfGroups( [ boundary_nodes ], [ nodes_on_edges ], 'nodes_type_101' )


all_nodes_flags = [0] * Mesh_1.NbNodes()
for i in range(nodes_type_101.GetNumberOfNodes()):
  nodeID = nodes_type_101.GetID(i+1)
  all_nodes_flags[nodeID-1] = 101

for i in range(nodes_type_202.GetNumberOfNodes()):
  nodeID = nodes_type_202.GetID(i+1)
  all_nodes_flags[nodeID-1] = 202

for i in range(nodes_type_303.GetNumberOfNodes()):
  nodeID = nodes_type_303.GetID(i+1)
  all_nodes_flags[nodeID-1] = 303


# Save both marked and unmarked files for double-checking with Tetgen

fid = open(tetgenBasename + '.node', 'w')
fid.write(str(Mesh_1.NbNodes()) + ' 3 0 1\n')
for i in range( Mesh_1.NbNodes() ):
  fid.write(str(i+1) + ' ' + ' '.join('%.16f' % coord for coord in Mesh_1.GetNodeXYZ(i+1)) + ' ' + str(all_nodes_flags[i]) + '\n')
fid.close()

counter = 0
fid     = open(tetgenBasename + '.ele', 'w')
fid.write(str(Mesh_1.NbTetras()) + ' 4 0\n')
for i in range(Mesh_1.NbElements()):
  numLocalNodes = Mesh_1.GetElemNbNodes(i+1)
  if numLocalNodes == 4:
    counter = counter+1
    fid.write(str(counter) + ' ' + ' '.join(str(id) for id in Mesh_1.GetElemNodes(i+1)) + '\n')
fid.close()

## Set names of Mesh objects
smesh.SetName(Regular_1D.GetAlgorithm(), 'Regular_1D')
smesh.SetName(NETGEN_3D.GetAlgorithm(), 'NETGEN 3D')
smesh.SetName(MEFISTO_2D.GetAlgorithm(), 'MEFISTO_2D')
smesh.SetName(Local_Length_1, 'Local Length_1')
smesh.SetName(Mesh_1.GetMesh(), 'Mesh_1')
smesh.SetName(boundary_nodes, 'boundary_nodes')
smesh.SetName(internal_nodes, 'internal_nodes')
smesh.SetName(all_nodes, 'all_nodes')
smesh.SetName(nodes_type_202, 'nodes_type_202')
smesh.SetName(nodes_type_101, 'nodes_type_101')
smesh.SetName(nodes_type_303, 'nodes_type_303')
smesh.SetName(nodes_on_edges, 'nodes_on_edges')


if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser(True)
