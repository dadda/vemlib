#include "pmgo.h"

int main()
{
  const char * dualfilename = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/tests/"
    "cylinder/cylinder2.1_marked.dual_marked";
  dualMesh dMesh;
  int dimPar;
  int *c2firstpIdx = NULL;
  double *parametricCoords = NULL, *verticesCopy = NULL;
  double startTime, elapsedTime;

  dualMeshInitialize( &dMesh );
  dualMeshRead( dualfilename, &dMesh );

  dimPar = dimParametricSpace( dMesh.Nver, dMesh.vBndMarkers );
  parametricCoords = (double *) malloc (dimPar * sizeof(double));

  verticesCopy = (double *) malloc (3 * dMesh.Nver * sizeof(double));
  memcpy( verticesCopy, dMesh.vertices, 3 * dMesh.Nver * sizeof(double) );

  c2firstpIdx = (int *) malloc (dMesh.Nver * sizeof(int));
  car2firstParIdx(dMesh.Nver, dMesh.vBndMarkers, c2firstpIdx);

  startTime = omp_get_wtime();
  car2par( dMesh.Nver, dMesh.vBndMarkers, dMesh.vertices, parametricCoords );
  elapsedTime = omp_get_wtime() - startTime;
  printf("\nCartesian to parametric conversion in %.16f seconds\n", elapsedTime);

  startTime = omp_get_wtime();
  par2car( parametricCoords, verticesCopy, dMesh.Nver, NULL, 0, c2firstpIdx, dMesh.vBndMarkers );
  elapsedTime = omp_get_wtime() - startTime;
  printf("\nParametric to cartesian conversion in %.16f seconds\n", elapsedTime);

  dualMeshDestroy( &dMesh );
  free( parametricCoords );
  free( verticesCopy );
  free( c2firstpIdx );

  return 0;
}
