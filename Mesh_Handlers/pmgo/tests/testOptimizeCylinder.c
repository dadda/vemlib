#include "pmgo.h"

int main()
{
  /*
   * Input/output files for the dual mesh
   *
   * Flag files are provided in order to allow optimizing
   * a group of dual nodes at a time:
   * - all_nodes_flags: optimize all nodes;
   * - boundary_nodes_flags: optimize boundary nodes only;
   * - internal_nodes_flags: optimize internal nodes only.
   */
  const char * dualmeshFilename =
    "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/tests/"
    "cylinder/cylinder2.1_marked.dual_marked";

  const char * dualmeshAllNodesFlagsFilename =
    "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/tests/"
    "cylinder/cylinder2.1_marked.dual_marked.all_nodes_flags";

  const char * dualmeshBndNodesFlagsFilename =
    "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/tests/"
    "cylinder/cylinder2.1_marked.dual_marked.boundary_nodes_flags";

  const char * dualmeshIntNodesFlagsFilename =
    "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/tests/"
    "cylinder/cylinder2.1_marked.dual_marked.internal_nodes_flags";

  const char * dualmeshOutputFilebasename =
    "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/tests/"
    "cylinder/cylinder2.1_marked.dual_marked_optimized";

  /*
   * Input/output files for the CMA-ES algorithm
   */
  const char * cmaesInitialsFilename =
    "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/"
    "cpp_source/cmaes_initials.par";

  const char * cmaesSignalsFilename =
    "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/"
    "cpp_source/cmaes_signals.par";

  const char * cmaesOutputFilename =
    "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/"
    "tests/cylinder/allcmaes.dat";

  /*
   * Input files for the NLCG algorithm
   */
  const char * nlcgInitialsFilename =
    "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/"
    "cpp_source/nlcg_initials.par";
    
  dualMesh dMesh;
  double * newVertices;
  char **metricsArgv;
  int metricsArgc, maxit;

  double LSsum, tmp, buffer[3], tolerance;

  int i, flag;
  nlcg_t nlcgDefaults;

  /* Load dual mesh */
  dualMeshInitialize( &dMesh );
  dualMeshRead( dualmeshFilename, &dMesh );
  dualMeshConstructConnectivity( &dMesh );

  /* Compute the least squares metric to check whether the mesh must be optimized or not */
  /* Check whether all the faces are planar */
  LSsum    = 0.0;

#pragma omp parallel for default(none) shared(dMesh)			\
  private(i, buffer, tmp)						\
  schedule(dynamic) reduction(+:LSsum)
  for (i = 0; i < dMesh.Nfc; ++i) {
    leastSquaresFace( &dMesh, dMesh.vertices, i, buffer, &tmp );
    LSsum += tmp;
  }

  printf("Before mesh optimization:\n");
  printf("Sum of least squares over faces is %e\n", LSsum );

  /*
   * Optimize mesh
   */

  newVertices = (double *) malloc (3 * (dMesh.Nver) * sizeof(double));
  memcpy( newVertices, dMesh.vertices, 3 * (dMesh.Nver) * sizeof(double) );

  while( true ) {

    /* Choose which group of nodes should be optimized */
    chooseGroupNodes( dualmeshAllNodesFlagsFilename,
		      dualmeshBndNodesFlagsFilename,
		      dualmeshIntNodesFlagsFilename,
		      dMesh.vBndMarkers,
		      dMesh.Nver );

    /* Choose a function to optimize */
    chooseObjFun( &metricsArgc, &metricsArgv );

    /* Choose an optimization algorithm */
    printf("\nChoose an optimization algorithm:\n"
	   "0. global mesh optimization with CMA-ES;\n"
	   "1. global mesh optimization with NLCG;\n"
	   "2. local mesh optimization with least squares.\n\n"
	   "Choice: ");
    if ( scanf("%i", &flag) != 1 ) {
      printf("File %s, line %i: error, user choice could not be read from terminal, "
	     "aborting ...\n", __FILE__, __LINE__);
      exit(1);
    }

    /* Run optimization */
    printf("\nStarting mesh optimization ...\n");
    if ( flag == 0 ) {

      globalMeshOptimCMAES( &dMesh, newVertices,
			    cmaesInitialsFilename, cmaesSignalsFilename, cmaesOutputFilename,
			    metricsArgv );

    } else if ( flag == 1 ) {

      nlcgInitialize( &nlcgDefaults, nlcgInitialsFilename );
      globalMeshOptimNLCG( &dMesh, newVertices, metricsArgv, &nlcgDefaults);
      nlcgDestroy( &nlcgDefaults );

    } else if ( flag == 2 ) {

      printf("Enter tolerance value: ");
      if ( scanf("%lf", &tolerance) != 1 ) {
	printf("File %s, line %i: error, user choice could not be read from terminal, "
	       "aborting ...\n", __FILE__, __LINE__);
	exit(1);
      }
      printf("Enter maximum number of iterations: ");
      if ( scanf("%i", &maxit) != 1 ) {
	printf("File %s, line %i: error, user choice could not be read from terminal, "
	       "aborting ...\n", __FILE__, __LINE__);
	exit(1);
      }
      localMeshOptim( &dMesh, newVertices,
		      metricsArgv, tolerance, maxit );

    }

    /* Copy optimized vertices back into dual vertices coordinate array */
    memcpy( dMesh.vertices, newVertices, 3 * dMesh.Nver * sizeof(double) );

    /* Check the least squares metric after the optimization */
    LSsum    = 0.0;

#pragma omp parallel for default(none) shared(dMesh)			\
  private(i, buffer, tmp)						\
  schedule(dynamic) reduction(+:LSsum)
    for (i = 0; i < dMesh.Nfc; ++i) {
      leastSquaresFace( &dMesh, dMesh.vertices, i, buffer, &tmp );
      LSsum += tmp;
    }

    printf("\nAfter mesh optimization:\n");
    printf("Sum of least squares over faces is %e\n", LSsum );

    dualMeshSave( dualmeshOutputFilebasename, &dMesh );

    printf("\nGo and check the result with SALOME. Continue mesh optimization (0/1)? ");
    if ( scanf("%i", &flag) != 1 ) {
      printf("File %s, line %i: user choice could not be read from terminal, "
	     "aborting ...\n", __FILE__, __LINE__);
      exit(1);
    }
    if ( flag == 0 ) break;
  }

  /* Release memory */
  dualMeshDestroy( &dMesh );
  free( newVertices );

  for (i = 0; i < metricsArgc; ++i) free( metricsArgv[i] );
  free( metricsArgv );

  return 0;
}
