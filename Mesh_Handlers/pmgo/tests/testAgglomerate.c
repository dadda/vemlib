#include "pmgo.h"

/*
 * Experimental script for agglomerating tetrahedral meshes
 */

int main()
{
  const char * basename = "/home/daniele/Documents/codes/vem_matlab/Applications/lamina/"
    "salome_old/lamina_cell.1";
  const char * metisPartition = "/home/daniele/Documents/codes/vem_matlab/Applications/lamina/"
    "salome_old/lamina_cell.metis.epart.8";

  int        i, j, dummy, Nbnd, Nsurvivors;
  int        *partition = NULL;
  FILE       *fp = NULL;

  primalMesh pMesh;

  primalMeshInitialize( &pMesh );
  readTetgen( basename, false, &pMesh );
  primalMeshConstructConnectivity( &pMesh );

  /* Read metis partition */
  partition = (int *) malloc (pMesh.Nelt * sizeof(int));
  fp = fopen(metisPartition, "r");
  for (i = 0; i < pMesh.Nelt; ++i)
    if ( fscanf(fp, "%i", partition+i) != 1 )
      exit(1);
  fclose(fp);
  printf("\nPartition read.\n");

  Nbnd       = 0;
  Nsurvivors = 0;

  /* Boundary nodes are never collapsed */
  for (i = 0; i < pMesh.Nver; ++i)
    if (pMesh.vTopFlags[i] == 1) {
      ++Nsurvivors;
      ++Nbnd;
    } else {
      dummy = partition[pMesh.v2t[pMesh.v2tDim[i]]];
      for (j = 1; j < pMesh.v2tDim[i+1] - pMesh.v2tDim[i]; ++j) {
	if ( dummy != partition[pMesh.v2t[pMesh.v2tDim[i]+j]] ) {
	  ++Nsurvivors;
	  break;
	}
      }
    }

  printf( "Original nodes: %i\nBoundary nodes: %i\nSurviving nodes: %i\n",
	  pMesh.Nver, Nbnd, Nsurvivors );

  free(partition);
  primalMeshDestroy( &pMesh );

  return 0;
}
