#include "pmgo.h"
#define NUMMESHES 1

int main()
{
  int i;

  const char *basenames[NUMMESHES];
  char outfilename[1024];

  double tolCollapsePt = 1e-12;
  primalMesh2d pMesh2d;
  dualMesh2d dMesh2d;

  /*
   * Astroid
   */

  /* basenames[0] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/astroid/dual/astroid_a0p01.1_marked"; */
  /* basenames[1] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/astroid/dual/astroid_a0p0025.1_marked"; */
  /* basenames[2] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/astroid/dual/astroid_a0p000625.1_marked"; */
  /* basenames[3] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/astroid/dual/astroid_a0p00015625.1_marked"; */
  /* basenames[4] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/astroid/dual/astroid_a0p0000390625.1_marked"; */
  /* basenames[5] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/astroid/dual/astroid_a0p000009765625.1_marked"; */
  /* basenames[6] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/astroid/dual/astroid_a0p00000244140625.1_marked"; */

  /*
   * Flower
   */

  /* basenames[0] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/flower/dual/flower_a0p02.1_marked"; */
  /* basenames[1] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/flower/dual/flower_a0p01.1_marked"; */
  /* basenames[2] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/flower/dual/flower_a0p005.1_marked"; */
  /* basenames[3] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/flower/dual/flower_a0p0025.1_marked"; */
  /* basenames[4] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/flower/dual/flower_a0p00125.1_marked"; */
  /* basenames[5] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/flower/dual/flower_a0p000625.1_marked"; */
  /* basenames[6] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/flower/dual/flower_a0p0003125.1_marked"; */
  /* basenames[7] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/flower/dual/flower_a0p00015625.1_marked"; */
  /* basenames[8] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/flower/dual/flower_a0p000078125.1_marked"; */

  /*
   * Egg
   */

  basenames[0] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/egg/dual/egg_a0p04.1_marked";

  /*
   * Quarter
   */

  /* basenames[0] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/dual/quarter_a0p01.1_marked"; */
  /* basenames[1] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/dual/quarter_a0p0025.1_marked"; */
  /* basenames[2] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/dual/quarter_a0p000625.1_marked"; */
  /* basenames[3] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/dual/quarter_a0p00015625.1_marked"; */
  /* basenames[4] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/dual/quarter_a0p0000390625.1_marked"; */
  /* basenames[5] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/dual/quarter_a0p000009765625.1_marked"; */
  /* basenames[6] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/dual/quarter_a0p00000244140625.1_marked"; */

  /*
   * Sector
   */

  /* basenames[0] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/sector/dual/sector_a0p05.1_marked"; */
  /* basenames[1] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/sector/dual/sector_a0p0125.1_marked"; */
  /* basenames[2] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/sector/dual/sector_a0p003125.1_marked"; */
  /* basenames[3] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/sector/dual/sector_a0p00078125.1_marked"; */
  /* basenames[4] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/sector/dual/sector_a0p0001953125.1_marked"; */
  /* basenames[5] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/sector/dual/sector_a0p000048828125.1_marked"; */
  /* basenames[6] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/sector/dual/sector_a0p00001220703125.1_marked"; */

  /*
   * Spiral
   */

  /* basenames[0] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/spiral/dual/spiral_fat_a0p01.1_marked"; */
  /* basenames[1] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/spiral/dual/spiral_fat_a0p005.1_marked"; */
  /* basenames[2] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/spiral/dual/spiral_fat_a0p0025.1_marked"; */
  /* basenames[3] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/spiral/dual/spiral_fat_a0p00125.1_marked"; */
  /* basenames[4] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/spiral/dual/spiral_fat_a0p000625.1_marked"; */
  /* basenames[5] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/spiral/dual/spiral_fat_a0p0003125.1_marked"; */
  /* basenames[6] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/spiral/dual/spiral_fat_a0p00015625.1_marked"; */
  /* basenames[7] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/spiral/dual/spiral_fat_a0p000078125.1_marked"; */

  /* basenames[4] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/spiral/dual/spiral_a0p0000390625.1_marked"; */
  /* basenames[5] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/spiral/dual/spiral_a0p000009765625.1_marked"; */

  /*
   * Unitcircle
   */

  /* basenames[0] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/unitcircle/dual/unitcircle_a0p01.1_marked"; */
  /* basenames[1] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/unitcircle/dual/unitcircle_a0p0025.1_marked"; */
  /* basenames[2] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/unitcircle/dual/unitcircle_a0p000625.1_marked"; */
  /* basenames[3] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/unitcircle/dual/unitcircle_a0p00015625.1_marked"; */
  /* basenames[4] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/unitcircle/dual/unitcircle_a0p0000390625.1_marked"; */
  /* basenames[5] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/unitcircle/dual/unitcircle_a0p000009765625.1_marked"; */
  /* basenames[6] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/unitcircle/dual/unitcircle_a0p00000244140625.1_marked"; */

  primalMesh2dInitialize( &pMesh2d );
  dualMesh2dInitialize( &dMesh2d );

  printf("\n");
  for (i = 0; i < NUMMESHES; ++i) {
    printf("Processing mesh %i ... ", i);

    readTriangle( basenames[i], &pMesh2d );
    primalMesh2dConstructConnectivity( &pMesh2d );

    dualMesh2dCreate( &pMesh2d, tolCollapsePt, &dMesh2d );

    outfilename[0] = '\0';
    sprintf(outfilename, "%s_dual.off", basenames[i]);
    saveOFFmesh( outfilename, &dMesh2d );

    primalMesh2dDestroy( &pMesh2d );
    dualMesh2dDestroy( &dMesh2d );
    printf("done\n");
  }

  return 0;
}
