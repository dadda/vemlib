#include "pmgo.h"

int main()
{
  const char * dualmeshFilename =
    "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/tetgenExamples/"
    "unitcube.1_marked.dual_marked";

  const char * cmaesInitialsFilename =
    "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/"
    "cpp_source/cmaes_initials.par";
  const char * cmaesSignalsFilename =
    "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/"
    "cpp_source/cmaes_signals.par";
  const char * cmaesOutputFilename =
    "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/tetgenExamples/"
    "allcmaes.dat";

  const char * nlcgInitialsFilename =
    "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/"
    "cpp_source/nlcg_initials.par";

  const char * dualmeshOutputFilebasename =
    "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/tetgenExamples/"
    "unitcube.1_marked.dual_marked_optimized";
    
  dualMesh dMesh;
  double * newVertices;
  char **metricsArgv;
  int metricsArgc, maxit;

  double LSsum, tmp, buffer[3], tolerance;

  int i, flagAlg;
  nlcg_t nlcgDefaults;

  /* Load dual mesh */
  dualMeshInitialize( &dMesh );
  dualMeshRead( dualmeshFilename, &dMesh );
  dualMeshConstructConnectivity( &dMesh );

  /* Compute the least squares metric to check whether the mesh must be optimized or not */
  LSsum    = 0.0;

#pragma omp parallel for default(none) shared(dMesh)			\
  private(i, buffer, tmp)						\
  schedule(dynamic) reduction(+:LSsum)
  for (i = 0; i < dMesh.Nfc; ++i) {
    leastSquaresFace( &dMesh, dMesh.vertices, i, buffer, &tmp );
    LSsum += tmp;
  }

  printf("Before mesh optimization:\n");
  printf("Sum of least squares over faces is %e\n", LSsum );

  /*
   * Optimize mesh
   */

  newVertices = (double *) malloc (3 * (dMesh.Nver) * sizeof(double));
  memcpy( newVertices, dMesh.vertices, 3 * (dMesh.Nver) * sizeof(double) );

  /* Choose a function to optimize */
  chooseObjFun( &metricsArgc, &metricsArgv );

  /* Choose an optimization algorithm */
  printf("\nChoose an optimization algorithm:\n"
	 "0. global mesh optimization with CMA-ES;\n"
	 "1. global mesh optimization with NLCG;\n"
	 "2. local mesh optimization with least squares.\n\n"
	 "Choice: ");
  if ( scanf("%i", &flagAlg) != 1 ) {
    printf("File %s, line %i: error, user choice could not be read from terminal, "
	   "aborting ...\n", __FILE__, __LINE__);
    exit(1);
  }

  /* Run optimization */
  printf("\nStarting mesh optimization ...\n");
  if ( flagAlg == 0 ) {

    globalMeshOptimCMAES( &dMesh, newVertices,
			  cmaesInitialsFilename, cmaesSignalsFilename, cmaesOutputFilename,
			  metricsArgv );

  } else if ( flagAlg == 1 ) {

    nlcgInitialize( &nlcgDefaults, nlcgInitialsFilename );
    globalMeshOptimNLCG( &dMesh, newVertices, metricsArgv, &nlcgDefaults);
    nlcgDestroy( &nlcgDefaults );

  } else if ( flagAlg == 2 ) {

    printf("Enter tolerance value: ");
    if ( scanf("%lf", &tolerance) != 1 ) {
      printf("File %s, line %i: error, user choice could not be read from terminal, "
	     "aborting ...\n", __FILE__, __LINE__);
      exit(1);
    }
    printf("Enter maximum number of iterations: ");
    if ( scanf("%i", &maxit) != 1 ) {
      printf("File %s, line %i: error, user choice could not be read from terminal, "
	     "aborting ...\n", __FILE__, __LINE__);
      exit(1);
    }
    localMeshOptim( &dMesh, newVertices,
		    metricsArgv, tolerance, maxit );

  }

  memcpy( dMesh.vertices, newVertices, 3 * (dMesh.Nver) * sizeof(double) );

  /* Check the least squares metric after the optimization */
  LSsum    = 0.0;

#pragma omp parallel for default(none) shared(dMesh)			\
  private(i, buffer, tmp)						\
  schedule(dynamic) reduction(+:LSsum)
  for (i = 0; i < dMesh.Nfc; ++i) {
    leastSquaresFace( &dMesh, dMesh.vertices, i, buffer, &tmp );
    LSsum += tmp;
  }
  printf("\nAfter mesh optimization:\n");
  printf("Sum of least squares over faces is %e\n", LSsum );

  dualMeshSave( dualmeshOutputFilebasename, &dMesh );

  /* Release memory */
  dualMeshDestroy( &dMesh );
  free( newVertices );

  for (i = 0; i < metricsArgc; ++i) free( metricsArgv[i] );
  free( metricsArgv );

  return 0;
}
