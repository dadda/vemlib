#include "polymesher.hpp"

int main()
{
  double xmin, xmax, ymin, ymax;
  double hminTarget;
  double minAreaTarget;
  double tolArea, residual;
  int NElem, maxit, i;

  double totAreaFinal = DBL_MAX, minAreaFinal = DBL_MIN, hminFinal = DBL_MIN;
  
  char filename[512];

  double * Nodes = NULL;
  int numNodes   = 0;
  
  polygon * Elements = NULL;
  int numElements    = 0;

  const char *domain = "sector";
  double param[5]    = { 2.0, 1.0, 0.0, 0.0, 4.0 };
  double area        = 16.0 * asin(1.0) - sqrt(15.0) - 16.0*asin(0.25);

  // const char *domain = "circle";
  // double param[4]    = { 1.0, 0.0, 0.0, 1.0 };
  // double area        = 4.0 * atan(1.0);

  // const char *domain = "quarter";
  // double param[9]    = { 4.0, 0.0, 0.0, 0.0, 2.0, 0.0, 0.0, 0.0, 1.0 };
  // double area        = 3.0 * atan(1.0);

  /*
   *
   */

  xmin = 1.0;
  xmax = 4.0;
  ymin = -sqrt(15.0);
  ymax = sqrt(15.0);

  // cout << "\nxmin: ";
  // cin  >> xmin;

  // cout << "xmax: ";
  // cin  >> xmax;

  // cout << "ymin: ";
  // cin  >> ymin;

  // cout << "ymax: ";
  // cin  >> ymax;

  cout << "Enter number of elements: ";
  cin  >> NElem;

  hminTarget    = 1e-10;
  minAreaTarget = 1e-20;
  tolArea       = 1.0;
  maxit         = 10;

  // cout << "Enter target hmin: ";
  // cin  >> hminTarget;

  // cout << "Enter target minimum area: ";
  // cin  >> minAreaTarget;

  // cout << "Enter tolerance for total area: ";
  // cin  >> tolArea;
  
  // cout << "Enter maximum number of iterations: ";
  // cin  >> maxit;

  sprintf(filename, "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/%s/%s__Nelt_%06i__hmin_%.e__minArea_%.e__tolTotArea_%.e.off", domain, domain, NElem, hminTarget, minAreaTarget, tolArea);

  // sprintf(filename, "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/unitcircle/ucirc__Nelt_%06i__hmin_%.e__minArea_%.e__tolTotArea_%.e.off", NElem, hminTarget, minAreaTarget, tolArea);

  i = 0;
  do {
    printf("Starting iteration %i ... ", i+1);
    PolyMesher( domain, param, area,
		xmin, xmax, ymin, ymax, NElem, hminTarget,
  		&Nodes, &numNodes,
  		&Elements, &numElements );
    computeStatistics( Nodes, Elements, &totAreaFinal, &minAreaFinal, &hminFinal );
    residual = fabs(totAreaFinal/area - 1.0);
    printf("done. Area residual: %e; min area: %e; hmin: %e\n",
	   residual, minAreaFinal, hminFinal);
    ++i;
    
    if ( ( ( hminFinal > hminTarget )
  	   && ( minAreaFinal > minAreaTarget )
	   && ( residual < tolArea ) )
  	 || (i == maxit) )
      break;
    else
      meshDestroy( Nodes, Elements );
  } while ( true );

  if (i == maxit) {
    printf("Reached maximum number of iterations, nothing has been saved\n");
  } else {
    printf("Saving OFF mesh ... ");
    writeOFFmesh( Nodes, numNodes, Elements, numElements, filename );
    printf("done\n");
  }

  meshDestroy( Nodes, Elements );
  
  return 0;
}
