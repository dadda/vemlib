#include "pmgo.h"
#include <time.h>

#define NTESTS 1

int main()
{
  const char * basename = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/tetgenExamples/unitcube.1_marked";
  clock_t ck0, ck1;
  primalMesh mesh;
  int i = 0;
  double elapsedTimeRead = 0.0, elapsedTimeConnectivity = 0.0;

  primalMeshInitialize( &mesh );

  for (i = 0; i < NTESTS; ++i) {
    ck0 = clock();
    readTetgen( basename, false, &mesh );
    ck1 = clock();
    elapsedTimeRead += (double)(ck1-ck0)/CLOCKS_PER_SEC;

    ck0 = clock();
    primalMeshConstructConnectivity( &mesh );
    ck1 = clock();
    elapsedTimeConnectivity += (double)(ck1-ck0)/CLOCKS_PER_SEC;

    primalMeshView( &mesh );
    primalMeshDestroy( &mesh );
  }
  printf("\nPrimal mesh loaded in %e seconds\n\n", elapsedTimeRead/NTESTS);
  printf("\nPrimal mesh connectivity built in %e seconds\n\n", elapsedTimeConnectivity/NTESTS );

  return 0;
}
