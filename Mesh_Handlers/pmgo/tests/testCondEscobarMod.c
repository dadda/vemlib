#include "pmgo.h"

int main()
{
  double V1[] = {0.3, 0.7, 0.5};
  double V2[] = {0.3, 1.7, 0.5};
  double F[]  = {1.3, 0.7, 0.5};
  double R[]  = {0.3, 0.7, 1.5};
  double c    = 0.0;
  double vol0 = 1.0;

  printf("Estimated condition number: %.16f\n", condEscobarMod( V1, V2, F, R, c, vol0 ) );
  printf("Expected value of the condition number: %.16f\n", 6.0 / (1.0 + sqrt(2.0) ) );
  
  return 0;
}
