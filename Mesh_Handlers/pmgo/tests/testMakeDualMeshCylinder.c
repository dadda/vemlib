#include "pmgo.h"

int main()
{
  const char * basename = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/tests/"
    "cylinder/cylinder2.1_marked";
  const char * outfilename = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/tests/"
    "cylinder/cylinder2.1_marked.dual";
  const char * outfilenameOVM = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/tests/"
    "cylinder/cylinder2.1_marked.dual.ovm";

  bool updateMarks = false;
  primalMesh pMesh;
  dualMesh dMesh;
  double tolCollapse = 1e-8;
  double LSsum, tmp, buffer[3];
  int j;

  primalMeshInitialize( &pMesh );
  readTetgen( basename, updateMarks, &pMesh );
  primalMeshConstructConnectivity( &pMesh );
  /* primalMeshView( &pMesh ); */

  dualMeshInitialize( &dMesh );
  dualMeshCreate( &pMesh, tolCollapse, &dMesh );
  dualMeshConstructConnectivity( &dMesh );
  /* dualMeshView( &dMesh ); */
  
  /* Check whether all the faces are planar */
  LSsum = 0.0;
#pragma omp parallel for default(none) shared(dMesh)			\
  private(j, buffer, tmp)						\
  schedule(dynamic) reduction(+:LSsum)
  for (j = 0; j < dMesh.Nfc; ++j) {
    leastSquaresFace( &dMesh, dMesh.vertices, j, buffer, &tmp );
    LSsum += tmp;
  }
  printf( "\nLeast squares of entire mesh is %e\n", LSsum);

  dualMeshSave( outfilename, &dMesh );
  dualMeshSaveOVM( outfilenameOVM, &dMesh );

  primalMeshDestroy( &pMesh );
  dualMeshDestroy( &dMesh );

  /* dualMeshRead( outfilename, &dMesh ); */
  /* dualMeshConstructConnectivity( &dMesh ); */
  /* dualMeshView( &dMesh ); */
  /* dualMeshSave( outfilename, &dMesh ); */

  /* dualMeshDestroy( &dMesh ); */

  return 0;
}
