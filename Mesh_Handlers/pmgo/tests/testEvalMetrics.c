#include "pmgo.h"

#define N 1

int main()
{
  /* const char * basename = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/" */
  /*   "tetgenExamples/unitcube.1_marked"; */
  const char * filename    = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/"
    "tetgenExamples/unitcube.3.voro.ovm";
  const char * outfilename = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/pmgo/"
    "tetgenExamples/unitcube.3.voro";

  /* primalMesh pMesh; */
  dualMesh dMesh;
  /* double tolCollapse = 1e-8; */
  double c = 10.0, tmp1;
  double sqm, LSsum, startTime, elapsedTime, buffer[3];
  int i, j;

  /* primalMeshInitialize( &pMesh ); */
  /* readTetgen( basename, false, &pMesh ); */
  /* primalMeshConstructConnectivity( &pMesh ); */

  dualMeshInitialize( &dMesh );
  /* dualMeshCreate( &pMesh, tolCollapse, &dMesh ); */
  dualMeshReadOVM( filename, &dMesh );
  dualMeshConstructConnectivity( &dMesh );

  elapsedTime = 0.0;
  for (i = 0; i < N; ++i) {
    startTime = omp_get_wtime();
    sqm = 0.0;

#pragma omp parallel for default(none) shared(dMesh) private(j) firstprivate(c) \
  schedule(dynamic) reduction(+:sqm)
    for (j = 0; j < dMesh.Npol; ++j)
      sqm += sqmPolyhedron( &dMesh, dMesh.vertices, c, j );
      
    sqm /= dMesh.Npol;
    elapsedTime += omp_get_wtime() - startTime;
  }
  elapsedTime /= (double)N;
  printf("\nSQM of entire mesh is %.16f in %.16f seconds\n", sqm, elapsedTime);

  elapsedTime = 0.0;
  for (i = 0; i < N; ++i) {
    startTime = omp_get_wtime();
    
    LSsum = 0.0;
#pragma omp parallel for default(none) shared(dMesh)			\
  private(j, tmp1, buffer)						\
  schedule(dynamic) reduction(+:LSsum)
    for (j = 0; j < dMesh.Nfc; ++j) {
      leastSquaresFace( &dMesh, dMesh.vertices, j, buffer, &tmp1 );
      LSsum += tmp1;
    }

    elapsedTime += omp_get_wtime() - startTime;
  }
  elapsedTime /= (double)N;
  printf( "\nLeast squares of entire mesh is %.16f in %.16f seconds\n", LSsum, elapsedTime );

  dualMeshSave( outfilename, &dMesh );

  /* primalMeshDestroy( &pMesh ); */
  dualMeshDestroy( &dMesh );

  return 0;
}
