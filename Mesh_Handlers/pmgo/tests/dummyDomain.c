#include "pmgo.h"


int dimParametricSpace( const int Nver, const int * const vFlags )
{
  int i;
  i = Nver;
  i += vFlags[0];
  return 0;
}


void getParametricBoundaries( const int Nver, const int * const vFlags,
			      double * lbounds, double * ubounds )
{
  int i;
  double v;
  i = Nver;
  i += vFlags[0];
  v = lbounds[0];
  v += ubounds[0];
}


void car2par( const int Nver,
	      const int * const vFlags,
	      const double * const cartesian,
	      double * parametric )
{
  int i;
  double v;
  i = Nver;
  i += vFlags[0];
  v = cartesian[0];
  v += parametric[0];
}


void car2firstParIdx( const int Nver,
		      const int * const vFlags,
		      int * const c2firstpIdx )
{
  int i;
  i = Nver;
  i += vFlags[0];
  c2firstpIdx[0] = i;
}


void par2car( const double * const parametric,
	      double * const cartesian,
	      const int Nver,
	      const int * const cartesianIdx,
	      const int cartesianIdxDim,
	      const int * const c2firstpIdx,
	      const int * const vFlags )
{
  int i;
  double v;
  i = Nver;
  i = cartesianIdx[0];
  i = cartesianIdxDim;
  i = c2firstpIdx[0];
  i += vFlags[0];
  v = cartesian[0];
  v += parametric[0];
}


void par2carIdx( const int Nver,
		 const int * const vFlags,
		 int * const p2cIdx )
{
  int i;
  i = Nver;
  i = vFlags[0];
  i += p2cIdx[0];
}
