#ifndef __PMGO_H
#define __PMGO_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>
#include <stdbool.h>
#include <math.h>
#include <omp.h>
#include <lapacke.h>
#include <cblas.h>
#include "cmaes_interface.h"
#include "boundary_transformation.h"
#include "utilities.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#ifdef INF
#undef INF
#define INF (DBL_MAX/1e2)
#else
#define INF (DBL_MAX/1e2)
#endif

#if defined (__cplusplus)
extern "C" {
#endif


typedef struct primalMesh2d {
  int Nver;
  double * vertices;
  int * vTopFlags;
  int * v2e;
  int * v2eDim;      /* Cumulative sum of vertex edge patches */
  int * v2t;
  int * v2tDim;

  int Ned;
  int * edges;       /* Edge vertices sorted in ascending order */
  int * eTopFlags;
  int * e2t;

  int Nelt;
  int * elements;
  int * t2e;
} primalMesh2d;


typedef struct primalMesh {
  int Nver;
  double * vertices;
  int * vTopFlags;
  int * v2e;
  int * v2eDim;      /* Cumulative sum of vertex edge patches */
  int * v2f;
  int * v2fDim;
  int * v2t;
  int * v2tDim;

  int Ned;
  int * edges;       /* Edge vertices sorted in ascending order */
  int * eTopFlags;
  int * e2f;
  int * e2fDim;
  int * e2t;
  int * e2tDim;

  int Nfc;
  int * faces;
  int * fTopFlags;
  int * f2t;         /* Face to tetrahedra */
  int * f2e;

  int Nelt;
  int * elements;
  int * t2e;
  int * t2f;
} primalMesh;


typedef struct dualMesh2d {
  int Nver;
  double * vertices;
  int * vBndMarkers;
  int * v2p;
  int * v2pDim;

  int Ned;
  int NedDir;
  int NedNeu;
  int * edges;
  int * eBndMarkers;
  int * dirEdges;
  int * neuEdges;
  int * e2p;

  int Npol;
  int * p2v;
  int * p2vDim;
  int * p2e;
  int * p2eDim;
} dualMesh2d;


typedef struct dualMesh {
  int Nver;
  double * vertices;
  int * vBndMarkers;
  int * v2f;
  int * v2fDim;
  int * v2p;
  int * v2pDim;

  int Ned;
  int * edges;
  int * eBndMarkers;
  
  int Nfc;
  int * f2he;
  int * f2heDim;
  int * f2p;
  int * fBndMarkers;

  int Npol;
  int * p2hf;
  int * p2hfDim;
} dualMesh;


/*
 * Functions for handling primal meshes
 */

void primalMesh2dInitialize( primalMesh2d * );
void primalMeshInitialize( primalMesh * );

void primalMesh2dDestroy( primalMesh2d * );
void primalMeshDestroy( primalMesh * );

void readTriangle( const char * const basename,
		   primalMesh2d * pMesh2d );
void readTetgen( const char * const basename,
		 const bool updateMarks,
		 primalMesh * pMesh );

void bottomUpConnectivity( const int Nb,
			   const int Nu,
			   const int d,
			   const int * const u2b,
			   int ** b2uDimAddr,
			   int ** b2uAddr );
void bottomUpLastLevel( const int Nb,
			const int Nu,
			const int * const u2b,
			const int NdLoc,
			int ** b2uAddr );

void topDownConnectivity( const int * const top2vertex,
			  const int Nt,
			  const int Nvt,
			  const int Nvd,
			  const int * const localConnectivity,
			  int **t2dAddr,
			  const int NdLoc );

void primalMesh2dConstructConnectivity( primalMesh2d * );
void primalMeshConstructConnectivity( primalMesh * );

void primalMeshView( const primalMesh * const );

/**************************************************/

/* Dual mesh data structures and routines */

typedef struct v2v {
  int neighbor;
  int dualEdge;
  struct v2v * next;
} v2v;

typedef struct dualEdge {
  int vertices[2];
  struct dualEdge * next;
} dualEdge;

typedef struct dualPolygon {
  int Nver;
  int * verticesIdx;
  struct dualPolygon * next;
} dualPolygon;

typedef struct dualFace {
  int Ned;
  int * hDualEdges;
  struct dualFace * next;
} dualFace;

typedef struct dualPolyhedron {
  int Nfc;
  int * hDualFaces;
  struct dualPolyhedron * next;
} dualPolyhedron;

typedef struct bndV2dualFaces {
  int Nfc;
  int * dualFacesIdx;
  struct bndV2dualFaces * next;
} bndV2dualFaces;

typedef struct intNode {
  int value;
  struct intNode * next;
} intNode;

void tetcircumcenter( const double * const a,
		      const double * const b,
		      const double * const c,
		      const double * const d,
		      double * relative_circumcenter,
		      double * xi,
		      double * eta,
		      double * zeta);

void tricircumcenter3d( const double * const a,
			const double * const b,
			const double * const c,
			double * relative_circumcenter,
			double * xi,
			double * eta );

void tricircumcenter( const double * const a,
		      const double * const b,
		      const double * const c,
		      double * relative_circumcenter,
		      double * xi,
		      double * eta );

double orient3d( const double * const pa,
		 const double * const pb,
		 const double * const pc,
		 const double * const pd );

double orient3dfast( const double * const pa,
		     const double * const pb,
		     const double * const pc,
		     const double * const pd );

double orient2d( const double * const pa,
		 const double * const pb,
		 const double * const pc );

double orient2dfast( const double * const pa,
		     const double * const pb,
		     const double * const pc );

void replaceTetCircumcenter( const double * const a,
			     const double * const b,
			     const double * const c,
			     const double * const d,
			     const double xi,
			     const double eta,
			     const double zeta,
			     const double limInfTol,
			     double * centralPt );

void replaceTriCircumcenter( const double * const a,
			     const double * const b,
			     const double * const c,
			     const double xi,
			     const double eta,
			     double * centralPt );

double pdistOldDualVertices( const int dim,
			     const int pCurrCounter,
			     const int pCumCounter,
			     const int * const current2v,
			     const int NverLoc,
			     const int * const v2up,
			     const int * const v2upDim,
			     const int * const primal2dualPointers,
			     const double tolerance,
			     const double * const dualVertex,
			     double * dualVertices,
			     int * dualVerticesPointers );

void pdistNewDualVertices( const int dim,
			   const int pCurrCounter,
			   const int pCumCounter,
			   const int * const up2v,
			   const int NverLoc,
			   const int * const v2up,
			   const int * const v2upDim,
			   const int * const primal2dualPointers,
			   const double tolerance,
			   const double * const dualVertex,
			   double * dualVertices,
			   int * dualVerticesPointers,
			   int * dualVerticesCounter );

bndV2dualFaces * bndV2dualFacesCreate( const int Nfc,
				       const int * const dualFacesIdx,
				       bndV2dualFaces * const next );

v2v * v2vCreate( const int neighbor,
		 const int dualEdge,
		 v2v * const next );

dualEdge * dualEdgeCreate( const int vertexIdx1,
			   const int vertexIdx2,
			   dualEdge * const next );

dualPolygon * dualPolygonCreate( const int Nver,
				 const int * const verticesIdx,
				 dualPolygon * const next );

dualFace * dualFaceCreate( const int Ned,
			   const int * const hDualEdges,
			   dualFace * const next );

dualPolyhedron * dualPolyhedronCreate( const int Nfc,
				       const int * const hDualFaces,
				       dualPolyhedron * const next );

intNode * intNodeCreate( const int value,
			 intNode * const next );

void dualMesh2dInitialize( dualMesh2d * );
void dualMeshInitialize( dualMesh * );

void dualMesh2dCreate( const primalMesh2d * const pMesh2d,
		       const double tolCollapsePt ,
		       dualMesh2d * dMesh2d);
void dualMeshCreate( const primalMesh * const pMesh,
		     const double tolCollapsePt ,
		     dualMesh * dMesh);

void dualMeshView( const dualMesh * const );

void dualMesh2dDestroy( dualMesh2d * );
void dualMeshDestroy( dualMesh * );


void saveOFFmesh( const char * const filename,
		  const dualMesh2d * const dMesh2d );
void readOFFmesh( const char * const filename,
		  dualMesh2d * dMesh2d );

void dualMeshSave( const char * const basename,
		   const dualMesh * const dMesh );
void dualMeshRead( const char * const basename,
		   dualMesh * dMesh );

void dualMeshReadOVM( const char * const filename,
		      dualMesh * dMesh );
void dualMeshSaveOVM( const char * const filename,
		      const dualMesh * const dMesh );

void vertex2PolygonConnectivity( dualMesh2d * dMesh2d );
void edgeConnectivity( dualMesh2d * dMesh2d );
void findBndVertices( dualMesh2d * dMesh2d );

void dualMeshConstructConnectivity( dualMesh * dMesh );
void vertex2FaceConnectivity( dualMesh * dMesh );
void vertex2PolyhedronConnectivity( dualMesh * dMesh );
void face2PolyhedronConnectivity( dualMesh * dMesh );

/**************************************************/

/* Mesh metrics */

double condEscobarMod( const double * const V1,
		       const double * const V2,
		       const double * const F,
		       const double * const R,
		       const double c,
		       const double volume0 );

double condEscobarModFast( const double * const V1,
			   const double * const V2,
			   const double * const F,
			   const double * const R,
			   const double c,
			   const double volume0 );

double sqm2dVertex( const double * const x,
		    const double * const lb,
		    const double * const ub,
		    const int vIdx,
		    const dualMesh2d * const dMesh2d,
		    double * const vertices,
		    const double c );

double sqmPolyhedron( const dualMesh * const dMesh,
		      const double * const vertices,
		      const double c,
		      const int pIdx );

void leastSquaresFace( const dualMesh * const dMesh,
		       const double * const vertices,
		       const int fIdx,
		       double * normal,
		       double * LSsum );

/**************************************************/

/* Mesh optimization */

typedef struct nlcg_t {
  short rescaleUnknowns;
  short method4derivatives;
  double tolG;
  double tolX;
  double tolAlphaLow;
  double c1;
  double c2;
  double rho;
  int maxit;

  char **formats;
  void **addresses;
  int numScalarParameters;
} nlcg_t;

int dimParametricSpace( const int Nver,
			const int * const vFlags );

void getParametricBoundaries( const int Nver,
			      const int * const vFlags,
			      double * lbounds,
			      double * ubounds );

void car2par( const int Nver,
	      const int * const vFlags,
	      const double * const cartesian,
	      double * parametric );

void car2firstParIdx( const int Nver,
		      const int * const vFlags,
		      int * const c2firstpIdx );

void par2car( const double * const parametric,
	      double * const cartesian,
	      const int Nver,
	      const int * const cartesianIdx,
	      const int cartesianIdxDim,
	      const int * const c2firstpIdx,
	      const int * const vFlags );

void par2carIdx( const int Nver,
		 const int * const vFlags,
		 int * const p2cIdx );

double evalMeshMetrics( const double * const x,
			const int xDim,
			const int * const xIdx,
			const int xIdxDim,
			const short rescale,
			const double * const lbounds,
			const double * const ubounds,
			double * const xRescaled,
			double * const vertices,
			const int * const verticesIdx,
			const int verticesIdxDim,
			const int * const c2firstpIdx,
			const dualMesh * const dMesh,
			char** metricsArgv,
			double * const metricsValues );
  
void approxGradient( const short method,
		     const double * const x,
		     const int N,
		     const double * const vertices_at_x,
		     const short rescale,
		     const double * const lbounds,
		     const double * const ubounds,
		     const int * const p2cIdx,
		     const int * const c2firstpIdx,
		     const dualMesh * const dMesh,
		     char ** metricsArgv,
		     const double * const metricsValues,
		     double * const gradient );

double directionalDerivative( const short method,
			      const double pNorm,
			      const double * const pUnit,
			      const double phiJ,
			      const double * const x,
			      double * const xShifted,
			      const int N,
			      const short rescale,
			      const double * const lbounds,
			      const double * const ubounds,
			      double * const xRescaled,
			      double * const vertices,
			      const int * const c2firstpIdx,
			      const dualMesh * const dMesh,
			      char ** metricsArgv );

double cubicInterpolate( const double alphaLow,
			 const double alphaHigh,
			 const double phiAlphaLow,
			 const double phiAlphaHigh,
			 const double dphiAlphaLow,
			 const double dphiAlphaHigh,
			 const double tolAlphaLow );

void zoom( const double alphaLowIN,
	   const double alphaHighIN,
	   const double phiAlphaLowIN,
	   const double phiAlphaHighIN,
	   const double dphiAlphaLowIN,
	   const double dphiAlphaHighIN,
	   const double tolAlpha,
	   const double tolAlphaLow,
	   const double phi0,
	   const double dphi0,
	   const double c1,
	   const double c2,
	   const short method,
	   const double * const x,
	   double * const xTmp1,
	   double * const xTmp2,
	   const double * const p,
	   const double pNorm,
	   const double * const pUnit,
	   const int N,
	   const short rescale,
	   const double * const lbounds,
	   const double * const ubounds,
	   double * const xRescaled,
	   double * const vertices,
	   const int * const c2firstpIdx,
	   const dualMesh * const dMesh,
	   char ** metricsArgv,
	   double * alphaStar,
	   double * phiNew,
	   double * const meshMetricByPolyhedron );

void stepLength( const double alphaOld,
		 const double phiOld,
		 const double tolAlpha,
		 const double rho,
		 const double * const x,
		 const double * const p,
		 const int N,
		 const short rescale,
		 const double * const lbounds,
		 const double * const ubounds,
		 double * const xRescaled,
		 double * const vertices,
		 const int * const c2firstpIdx,
		 const dualMesh * const dMesh,
		 char ** metricsArgv,
		 double * alphaNew,
		 double * deltaAlpha,
		 double * alphaMax,
		 double * phiNew,
		 double * const xNew,
		 double * const meshMetricByPolyhedron );

void lineSearch( const double phi0,
		 const double dphi0,
		 const double c1,
		 const double c2,
		 const double tolAlpha,
		 const double tolAlphaLow,
		 const double rho,
		 const short method,
		 const double * const x,
		 double * const xTmp1,
		 double * const xTmp2,
		 const double * const p,
		 const double pNorm,
		 const double * const pUnit,
		 const int N,
		 const short rescale,
		 const double * const lbounds,
		 const double * const ubounds,
		 double * const xRescaled,
		 double * const vertices,
		 const int * const c2firstpIdx,
		 const dualMesh * const dMesh,
		 char ** metricsArgv,
		 double * alphaNew,
		 double * deltaAlpha,
		 double * phiNew,
		 double * const meshMetricByPolyhedron );

void nlcgInitialize( nlcg_t * nlcgPtr,
		     char const *filename );

void nlcgDestroy( nlcg_t * nlcgPtr );

void globalMeshOptimNLCG( const dualMesh * const dMesh,
			  double * const newVertices,
			  char **metricsArgv,
			  const nlcg_t * const nlcgPtr );

void globalMeshOptimCMAES( const dualMesh * const dMesh,
			   double * const newVertices,
			   const char * const cmaesInitialsFilename,
			   const char * const cmaesSignalsFilename,
			   const char * const cmaesOutputFilename,
			   char **metricsArgv );

void localMeshOptim( const dualMesh * const dMesh,
		     double * const newVertices,
		     char** metricsArgv,
		     const double tolerance,
		     const int maxit );

void chooseObjFun( int * objfunArgc,
		   char *** objfunArgv );

void chooseGroupNodes( const char * allflagsFilename,
		       const char * bndflagsFilename,
		       const char * intflagsFilename,
		       int * const vBndMarkers,
		       const int Nver );

void localMesh2dOptimCMAES( const dualMesh2d * const dMesh2d,
			    double * const newVertices,
			    const char * const cmaesInitialsFilename,
			    const char * const cmaesSignalsFilename,
			    char **metricsArgv );

#if defined (__cplusplus)
}
#endif

#endif
