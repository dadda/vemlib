#include "pmgo.h"


void primalMesh2dInitialize( primalMesh2d * pMesh2d )
{
  pMesh2d->Nver      = 0;
  pMesh2d->vertices  = NULL;
  pMesh2d->vTopFlags = NULL;
  pMesh2d->v2e       = NULL;
  pMesh2d->v2eDim    = NULL;
  pMesh2d->v2t       = NULL;
  pMesh2d->v2tDim    = NULL;

  pMesh2d->Ned       = 0;
  pMesh2d->edges     = NULL;
  pMesh2d->eTopFlags = NULL;
  pMesh2d->e2t       = NULL;

  pMesh2d->Nelt      = 0;
  pMesh2d->elements  = NULL;
  pMesh2d->t2e       = NULL;
}


void primalMeshInitialize( primalMesh * pMesh )
{
  pMesh->Nver      = 0;
  pMesh->vertices  = NULL;
  pMesh->vTopFlags = NULL;
  pMesh->v2e       = NULL;
  pMesh->v2eDim    = NULL;
  pMesh->v2f       = NULL;
  pMesh->v2fDim    = NULL;
  pMesh->v2t       = NULL;
  pMesh->v2tDim    = NULL;

  pMesh->Ned       = 0;
  pMesh->edges     = NULL;
  pMesh->eTopFlags = NULL;
  pMesh->e2f       = NULL;
  pMesh->e2fDim    = NULL;
  pMesh->e2t       = NULL;
  pMesh->e2tDim    = NULL;

  pMesh->Nfc       = 0;
  pMesh->faces     = NULL;
  pMesh->fTopFlags = NULL;
  pMesh->f2t       = NULL;
  pMesh->f2e       = NULL;

  pMesh->Nelt      = 0;
  pMesh->elements  = NULL;
  pMesh->t2e       = NULL;
  pMesh->t2f       = NULL;
}


void readTriangle( const char * const basename,
		   primalMesh2d * pMesh2d )
{
  char filename[256];
  FILE *fp = NULL;
  int i, itmp1, itmp2, itmp3, itmp4, itmp5;

  int **itmp = NULL;

  /*
   * Read mesh vertices
   */

  sprintf(filename, "%s%s", basename, ".node");
  fp = fopen(filename, "r");
  if ( fscanf(fp, "%i %i %i %i", &(pMesh2d->Nver), &itmp1, &itmp2, &itmp3) != 4 ) {
    printf("File %s line %i: encountered error while reading node file, aborting ...\n",
	   __FILE__, __LINE__);
    exit(1);
  }

  pMesh2d->vTopFlags = (int *) malloc ( (pMesh2d->Nver) * sizeof(int) );
  pMesh2d->vertices  = (double *) malloc (2 * (pMesh2d->Nver) * sizeof(double));

  for(i = 0; i < pMesh2d->Nver; ++i) {
    if ( fscanf(fp, "%i %lf %lf %i", &itmp1,
		pMesh2d->vertices + 2*i, pMesh2d->vertices + 2*i + 1,
		pMesh2d->vTopFlags + i) != 4 ) {
      printf("File %s line %i: encountered error while reading node file, aborting ...\n",
	     __FILE__, __LINE__);
      exit(1);
    }
  }
  fclose(fp);

  /*
   * Read mesh elements
   */

  filename[0] = '\0';
  sprintf(filename, "%s%s", basename, ".ele");
  fp = fopen(filename, "r");
  if ( fscanf(fp, "%i %i %i", &(pMesh2d->Nelt), &itmp1, &itmp2) != 3 ) {
    printf("File %s line %i: encountered error while reading element file, aborting ...\n",
	   __FILE__, __LINE__);
    exit(1);
  }

  pMesh2d->elements = (int *) malloc (3 * (pMesh2d->Nelt) * sizeof(int));

  for(i = 0; i < pMesh2d->Nelt; ++i) {
    if ( fscanf(fp, "%i %i %i %i",
		&itmp1, &itmp3, &itmp4, &itmp5) != 4 ) {
      printf("File %s line %i: encountered error while reading element file, aborting ...\n",
	     __FILE__, __LINE__);
      exit(1);
    }

    if ( itmp2 )
      if ( fscanf(fp, "%i", &itmp1) != 1 ) {
	printf("File %s line %i: encountered error while reading element file, aborting ...\n",
	       __FILE__, __LINE__);
	exit(1);
      }

    /* Use C-numbering */
    pMesh2d->elements[3*i]   = itmp3 - 1;
    pMesh2d->elements[3*i+1] = itmp4 - 1;
    pMesh2d->elements[3*i+2] = itmp5 - 1;
  }
  fclose(fp);

  /*
   * Read and sort mesh edges
   */

  filename[0] = '\0';
  sprintf(filename, "%s%s", basename, ".edge");
  fp = fopen(filename, "r");
  if ( fscanf(fp, "%i %i", &(pMesh2d->Ned), &itmp1) != 2 ) {
    printf("File %s line %i: encountered error while reading edge file, aborting ...\n",
	   __FILE__, __LINE__);
    exit(1);
  }

  pMesh2d->edges     = (int *) malloc (2 * (pMesh2d->Ned) * sizeof(int));
  pMesh2d->eTopFlags = (int *) calloc (pMesh2d->Ned, sizeof(int));
  itmp             = (int **) malloc ((pMesh2d->Ned) * sizeof(int *));

  for(i = 0; i < pMesh2d->Ned; ++i) {
    /* Container for edge vertex indices and edge flag */
    itmp[i] = (int *) malloc (3 * sizeof(int));
    if ( fscanf(fp, "%i %i %i %i",
		&itmp1, &itmp2, &itmp3, &itmp[i][2]) != 4 ) {
      printf("File %s line %i: encountered error while reading edge file, aborting ...\n",
	     __FILE__, __LINE__);
      exit(1);
    }

    /* Sort edge vertices and use C-numbering */
    if (itmp2 > itmp3) {
      itmp[i][0] = itmp3 - 1;
      itmp[i][1] = itmp2 - 1;
    } else {
      itmp[i][0] = itmp2 - 1;
      itmp[i][1] = itmp3 - 1;
    }
  }
  fclose(fp);

  /* Sort edges row-wise */
  qsort( itmp, pMesh2d->Ned, sizeof(int *), compareSize2 );

  /* Load edges and flags in sorted order */
  for (i = 0; i < pMesh2d->Ned; ++i) {
    pMesh2d->edges[2*i]   = itmp[i][0];
    pMesh2d->edges[2*i+1] = itmp[i][1];
    pMesh2d->eTopFlags[i] = itmp[i][2];
    free(itmp[i]);
  }
  free(itmp);
}


void readTetgen( const char * const basename,
		 const bool updateMarks,
		 primalMesh * pMesh )
{
  char filename[256];
  FILE *fp = NULL;
  int i, itmp1, itmp2, itmp3, itmp4, itmp5, itmp6;
  /* int j; */

  int **itmp = NULL;
  /* int faceEdgeConnectivity[6] = { 0, 1, 1, 2, 2, 0 }; */

  /*
   * Read mesh vertices
   */

  sprintf(filename, "%s%s", basename, ".node");
  fp = fopen(filename, "r");
  if ( fscanf(fp, "%i %i %i %i", &(pMesh->Nver), &itmp1, &itmp2, &itmp3) != 4 ) {
    printf("File %s line %i: encountered error while reading node file, aborting ...\n",
	   __FILE__, __LINE__);
    exit(1);
  }

  pMesh->vTopFlags = (int *) malloc ( (pMesh->Nver) * sizeof(int) );
  pMesh->vertices  = (double *) malloc (3 * (pMesh->Nver) * sizeof(double));

  for(i = 0; i < pMesh->Nver; ++i) {
    if ( fscanf(fp, "%i %lf %lf %lf %i", &itmp1,
		pMesh->vertices + 3*i, pMesh->vertices + 3*i + 1, pMesh->vertices + 3*i + 2,
		pMesh->vTopFlags + i) != 5 ) {
      printf("File %s line %i: encountered error while reading node file, aborting ...\n",
	     __FILE__, __LINE__);
      exit(1);
    }
  }
  fclose(fp);

  /*
   * Read mesh elements
   */

  filename[0] = '\0';
  sprintf(filename, "%s%s", basename, ".ele");
  fp = fopen(filename, "r");
  if ( fscanf(fp, "%i %i %i", &(pMesh->Nelt), &itmp1, &itmp2) != 3 ) {
    printf("File %s line %i: encountered error while reading element file, aborting ...\n",
	   __FILE__, __LINE__);
    exit(1);
  }

  pMesh->elements = (int *) malloc (4 * (pMesh->Nelt) * sizeof(int));

  for(i = 0; i < pMesh->Nelt; ++i) {
    if ( fscanf(fp, "%i %i %i %i %i",
		&itmp1, &itmp3, &itmp4, &itmp5, &itmp6) != 5 ) {
      printf("File %s line %i: encountered error while reading element file, aborting ...\n",
	     __FILE__, __LINE__);
      exit(1);
    }

    if ( itmp2 )
      if ( fscanf(fp, "%i", &itmp1) != 1 ) {
	printf("File %s line %i: encountered error while reading element file, aborting ...\n",
	       __FILE__, __LINE__);
	exit(1);
      }

    /* Use C-numbering */
    pMesh->elements[4*i]   = itmp3 - 1; pMesh->elements[4*i+1] = itmp4 - 1;
    pMesh->elements[4*i+2] = itmp5 - 1; pMesh->elements[4*i+3] = itmp6 - 1;
  }
  fclose(fp);

  /*
   * Read mesh faces
   */

  filename[0] = '\0';
  sprintf(filename, "%s%s", basename, ".face");
  fp = fopen(filename, "r");
  if ( fscanf(fp, "%i %i", &(pMesh->Nfc), &itmp1) != 2 ) {
    printf("File %s line %i: encountered error while reading face file, aborting ...\n",
  	   __FILE__, __LINE__);
    exit(1);
  }

  pMesh->faces     = (int *) malloc (3 * (pMesh->Nfc) * sizeof(int));
  pMesh->fTopFlags = (int *) calloc (pMesh->Nfc, sizeof(int));
  pMesh->f2t       = (int *) malloc (2 * (pMesh->Nfc) * sizeof(int));
  itmp             = (int **) malloc ((pMesh->Nfc) * sizeof(int *));

  for(i = 0; i < pMesh->Nfc; ++i) {
    itmp[i] = (int *) malloc (6 * sizeof(int));
    if ( fscanf(fp, "%i %i %i %i %i %i %i", &itmp1,
  		&itmp2, &itmp3, &itmp4,
  		&itmp[i][3],
  		&itmp5, &itmp6) != 7 ) {
      printf("File %s line %i: encountered error while reading face file, aborting ...\n",
  	     __FILE__, __LINE__);
      exit(1);
    }

    /* Sort face vertex indices */
    if (itmp2 > itmp3) { itmp1 = itmp2; itmp2 = itmp3; itmp3 = itmp1; }
    if (itmp2 > itmp4) { itmp1 = itmp2; itmp2 = itmp4; itmp4 = itmp1; }
    if (itmp3 > itmp4) { itmp1 = itmp3; itmp3 = itmp4; itmp4 = itmp1; }

    /* Use C-numbering */
    itmp[i][0] = itmp2 - 1; itmp[i][1] = itmp3 - 1; itmp[i][2] = itmp4 - 1;
    itmp[i][4] = itmp5 - 1; itmp[i][5] = itmp6 == -1 ? -1 : itmp6 - 1;
  }
  fclose(fp);

  /* Sort faces row-wise */
  qsort( itmp, pMesh->Nfc, sizeof(int *), compareSize3 );

  /* Load face and flags in sorted order */
  for (i = 0; i < pMesh->Nfc; ++i) {
    pMesh->faces[3*i]   = itmp[i][0];
    pMesh->faces[3*i+1] = itmp[i][1];
    pMesh->faces[3*i+2] = itmp[i][2];
    if ( itmp[i][3] != 0 ) {
      pMesh->fTopFlags[i] = 1;
      if ( ( pMesh->vTopFlags[ pMesh->faces[3*i] ] == 0 )
	   || ( pMesh->vTopFlags[ pMesh->faces[3*i+1] ] == 0 )
	   || ( pMesh->vTopFlags[ pMesh->faces[3*i+2] ] == 0 ) ) {
	printf("File %s line %i: error while reading face file, "
	       "face %i and/or nodes %i,%i,%i not marked correctly, aborting ...\n",
	       __FILE__, __LINE__,
	       i, pMesh->faces[3*i], pMesh->faces[3*i+1], pMesh->faces[3*i+2]);
	exit(1);
      }
    }
    pMesh->f2t[2*i]   = itmp[i][4];
    pMesh->f2t[2*i+1] = itmp[i][5];
    free( itmp[i] );
  }
  free( itmp );

  /*
   * Read and sort mesh edges
   */

  filename[0] = '\0';
  sprintf(filename, "%s%s", basename, ".edge");
  fp = fopen(filename, "r");
  if ( fscanf(fp, "%i %i", &(pMesh->Ned), &itmp1) != 2 ) {
    printf("File %s line %i: encountered error while reading edge file, aborting ...\n",
	   __FILE__, __LINE__);
    exit(1);
  }

  pMesh->edges     = (int *) malloc (2 * (pMesh->Ned) * sizeof(int));
  pMesh->eTopFlags = (int *) calloc (pMesh->Ned, sizeof(int));
  itmp             = (int **) malloc ((pMesh->Ned) * sizeof(int *));

  for(i = 0; i < pMesh->Ned; ++i) {
    /* Container for edge vertex indices and edge flag */
    itmp[i] = (int *) malloc (3 * sizeof(int));
    if ( fscanf(fp, "%i %i %i %i %i",
		&itmp1, &itmp2, &itmp3, &itmp[i][2], &itmp4) != 5 ) {
      printf("File %s line %i: encountered error while reading edge file, aborting ...\n",
	     __FILE__, __LINE__);
      exit(1);
    }

    /* Sort edge vertices and use C-numbering */
    if (itmp2 > itmp3) {
      itmp[i][0] = itmp3 - 1;
      itmp[i][1] = itmp2 - 1;
    } else {
      itmp[i][0] = itmp2 - 1;
      itmp[i][1] = itmp3 - 1;
    }
  }
  fclose(fp);

  /* Sort edges row-wise */
  qsort( itmp, pMesh->Ned, sizeof(int *), compareSize2 );

  /* Load edges and flags in sorted order */
  for (i = 0; i < pMesh->Ned; ++i) {
    pMesh->edges[2*i]   = itmp[i][0];
    pMesh->edges[2*i+1] = itmp[i][1];
    if ( !updateMarks ) pMesh->eTopFlags[i] = itmp[i][2];
    free(itmp[i]);
  }
  free(itmp);

  /* if ( updateMarks ) { */

  /*   topDownConnectivity( pMesh->faces, pMesh->Nfc, 3, 2, */
  /* 			 faceEdgeConnectivity, &pMesh->f2e, 3 ); */

  /*   for (i = 0; i < pMesh->Nfc; ++i) { */
  /*     if ( pMesh->fTopFlags[i] == 1 ) { */
  /* 	pMesh->eTopFlags[ pMesh->f2e[3*i] ]   = 1; */
  /* 	pMesh->eTopFlags[ pMesh->f2e[3*i+1] ] = 1; */
  /* 	pMesh->eTopFlags[ pMesh->f2e[3*i+2] ] = 1; */
  /*     } */
  /*   } */

  /*   for (i = 0; i < pMesh->Ned; ++i) { */
  /*     if ( pMesh->eTopFlags[i] == 1 ) { */
  /* 	if ( ( pMesh->vTopFlags[ pMesh->edges[2*i] ] >= 300 ) */
  /* 	     && ( pMesh->vTopFlags[ pMesh->edges[2*i+1] ] >= 300 ) ) */
  /* 	  pMesh->eTopFlags[i] = 2; */
  /*     } */
  /*   } */

  /*   /\* Build vertex to edges connectivity information. This is done in order to update */
  /*    * vertex topological flags of type '300', with the number of incident edges of type '2' *\/ */

  /*   bottomUpConnectivity( pMesh->Nver, pMesh->Ned, 2, pMesh->edges, */
  /* 			  &pMesh->v2eDim, &pMesh->v2e ); */

  /*   /\* Update vertex topological flags *\/ */
  /*   for (i = 0; i < pMesh->Nver; ++i) { */
  /*     if ( pMesh->vTopFlags[i] == 300 ) { */
  /* 	/\* count number of boundary edges of type 2 *\/ */
  /* 	itmp1 = 0; */
  /* 	for (j = 0; j < ( pMesh->v2eDim[i+1] - pMesh->v2eDim[i] ); ++j) */
  /* 	  if ( pMesh->eTopFlags[ pMesh->v2e[ pMesh->v2eDim[i] + j ] ] == 2 ) ++itmp1; */
  /* 	pMesh->vTopFlags[i] += itmp1; */
  /*     } */
  /*   } */
  /* } */
}


void primalMeshView( const primalMesh * const pMesh )
{
  int i, j, patchSize;

  printf("\n\nPrinting primal mesh data ... (note: tetgen numbering is used)\n\n");

  /* View nodes */
  if ( pMesh->Nver ) {
    printf("\n\nVertices: vertex index");
    if ( pMesh->vertices )  printf(", x, y, z");
    if ( pMesh->vTopFlags ) printf(", topological flag");
    printf("\n");
    for(i = 0; i < pMesh->Nver; ++i) {
      printf("%i", i+1);
      if ( pMesh->vertices )  printf(" %f %f %f", pMesh->vertices[3*i],
				     pMesh->vertices[3*i+1], pMesh->vertices[3*i+2]);
      if ( pMesh->vTopFlags ) printf(" %i", pMesh->vTopFlags[i]);
      printf("\n");
    }
  }

  /* View edges */
  if ( pMesh->Ned ) {
    printf("\n\nEdges: edge index");
    if ( pMesh->edges )     printf(", vertex1, vertex2");
    if ( pMesh->eTopFlags ) printf(", topological flag");
    printf("\n");
    for(i = 0; i < pMesh->Ned; ++i) {
      printf("%i", i+1);
      if ( pMesh->edges )     printf(" %i %i", pMesh->edges[2*i]+1, pMesh->edges[2*i+1]+1);
      if ( pMesh->eTopFlags ) printf(" %i", pMesh->eTopFlags[i]);
      printf("\n");
    }
  }

  /* View faces */
  if ( pMesh->Nfc ) {
    printf("\n\nFaces: face index");
    if ( pMesh->faces )     printf(", vertex1, vertex2, vertex3");
    if ( pMesh->fTopFlags ) printf(", topological flag");
    if ( pMesh->f2t )       printf(", ele 1, ele 2");
    printf("\n");
    for(i = 0; i < pMesh->Nfc; ++i) {
      printf("%i", i+1);
      if ( pMesh->faces )     printf(" %i %i %i", pMesh->faces[3*i]+1,
				     pMesh->faces[3*i+1]+1, pMesh->faces[3*i+2]+1);
      if ( pMesh->fTopFlags ) printf(" %i", pMesh->fTopFlags[i]);
      if ( pMesh->f2t )       printf(" %i %i", pMesh->f2t[2*i]+1, pMesh->f2t[2*i+1]+1);
      printf("\n");
    }
  }

  /* View elements */
  if ( pMesh->elements ) {
    printf("\n\nTetrahedra: vertex index, vertex1, vertex2, vertex3, vertex 4\n");
    for(i = 0; i < pMesh->Nelt; ++i) {
      printf( "%i %i %i %i %i\n", i+1, pMesh->elements[4*i]+1, pMesh->elements[4*i+1]+1,
	      pMesh->elements[4*i+2]+1, pMesh->elements[4*i+3]+1 );
    }
  }

  /* View v2e */
  if ( pMesh->v2e ) {
    printf("\n\nVertex to edge connectivity:\n");
    for (i = 0; i < pMesh->Nver; ++i) {
      patchSize = pMesh->v2eDim[i+1] - pMesh->v2eDim[i];
      for (j = 0; j < patchSize; ++j)
	printf("   %i", pMesh->v2e[ pMesh->v2eDim[i] + j ] + 1 );
      printf("\n");
    }
  }

  /* View v2f */
  if ( pMesh->v2f ) {
    printf("\n\nVertex to face connectivity:\n");
    for (i = 0; i < pMesh->Nver; ++i) {
      patchSize = pMesh->v2fDim[i+1] - pMesh->v2fDim[i];
      for (j = 0; j < patchSize; ++j)
	printf("   %i", pMesh->v2f[ pMesh->v2fDim[i] + j ] + 1 );
      printf("\n");
    }
  }

  /* View v2t */
  if ( pMesh->v2t ) {
    printf("\n\nVertex to tetrahedron connectivity:\n");
    for (i = 0; i < pMesh->Nver; ++i) {
      patchSize = pMesh->v2tDim[i+1] - pMesh->v2tDim[i];
      for (j = 0; j < patchSize; ++j)
	printf("   %i", pMesh->v2t[ pMesh->v2tDim[i] + j ] + 1 );
      printf("\n");
    }
  }

  /* View f2e */
  if ( pMesh->f2e ) {
    printf("\nFace to edge connectivity: face index, edge1, edge2, edge3\n");
    for(i = 0; i < pMesh->Nfc; ++i) {
      printf( "%i %i %i %i\n", i+1, pMesh->f2e[3*i]+1, pMesh->f2e[3*i+1]+1, pMesh->f2e[3*i+2]+1 );
    }
  }

  /* View e2f */
  if ( pMesh->e2f ) {
    printf("\n\nEdge to face connectivity:\n");
    for (i = 0; i < pMesh->Ned; ++i) {
      patchSize = pMesh->e2fDim[i+1] - pMesh->e2fDim[i];
      for (j = 0; j < patchSize; ++j)
	printf("   %i", pMesh->e2f[ pMesh->e2fDim[i] + j ] + 1 );
      printf("\n");
    }
  }

  /* View t2e */
  if ( pMesh->t2e ) {
    printf("\n\nTetrahedron to edge connectivity: tetrahedron index, edge1, ..., edge6\n");
    for(i = 0; i < pMesh->Nelt; ++i) {
      printf( "%i %i %i %i %i %i %i\n", i+1,
	      pMesh->t2e[6*i]+1, pMesh->t2e[6*i+1]+1, pMesh->t2e[6*i+2]+1,
	      pMesh->t2e[6*i+3]+1, pMesh->t2e[6*i+4]+1, pMesh->t2e[6*i+5]+1 );
    }
  }

  /* View e2t */
  if ( pMesh->e2t ) {
    printf("\n\nEdge to tetrahedron connectivity:\n");
    for (i = 0; i < pMesh->Ned; ++i) {
      patchSize = pMesh->e2tDim[i+1] - pMesh->e2tDim[i];
      for (j = 0; j < patchSize; ++j)
	printf("   %i", pMesh->e2t[ pMesh->e2tDim[i] + j ] + 1 );
      printf("\n");
    }
  }

  /* View t2f */
  if ( pMesh->t2f ) {
    printf("\n\nTetrahedron to face connectivity: tetrahedron index, face1, ..., face4\n");
    for(i = 0; i < pMesh->Nelt; ++i) {
      printf( "%i %i %i %i %i\n", i+1,
	      pMesh->t2f[4*i]+1, pMesh->t2f[4*i+1]+1, pMesh->t2f[4*i+2]+1, pMesh->t2f[4*i+3]+1 );
    }
  }

  /* View f2t */
  if ( pMesh->f2t ) {
    printf("\n\nFace to tetrahedron connectivity: face index, tet 1, tet 2 (or -1)\n");
    for(i = 0; i < pMesh->Nfc; ++i) {
      printf( "%i %i %i\n", i+1,
	      pMesh->f2t[2*i]+1, pMesh->f2t[2*i+1] == -1 ? -1 : pMesh->f2t[2*i+1]+1 );
    }
  }
}


void bottomUpLastLevel( const int Nb,
			const int Nu,
			const int * const u2b,
			const int NdLoc,
			int ** b2uAddr )
{
  int i, j, itmp;

  *b2uAddr = (int *) malloc ( 2 * Nb * sizeof(int) );
  memset( *b2uAddr, -1,  2 * Nb * sizeof(int) );

  for (i = 0; i < Nu; ++i) {
    for (j = 0; j < NdLoc; ++j) {
      itmp = (*b2uAddr)[ 2*u2b[NdLoc*i+j] ];
      if ( itmp == -1 )
	(*b2uAddr)[ 2*u2b[NdLoc*i+j] ]     = i;
      else
	(*b2uAddr)[ 2*u2b[NdLoc*i+j] + 1 ] = i;
    }
  }
}


void bottomUpConnectivity( const int Nb,
			   const int Nu,
			   const int d,
			   const int * const u2b,
			   int ** b2uDimAddr,
			   int ** b2uAddr )
{
  int i, j;
  int *itmpv = NULL;

  *b2uDimAddr = (int *) malloc ( (Nb+1) * sizeof(int) );
  memset( *b2uDimAddr, 0,  (Nb+1) * sizeof(int) );

  for (i = 0; i < Nu; ++i)
    for (j = 0; j < d; ++j) ++(*b2uDimAddr)[ u2b[d*i+j] + 1 ];

  itmpv = (int *) malloc ( Nb * sizeof(int) );
  memcpy( itmpv, (*b2uDimAddr)+1, Nb * sizeof(int) );

  for (i = 1; i <= Nb; ++i) (*b2uDimAddr)[i] += (*b2uDimAddr)[i-1];

  *b2uAddr = (int *) malloc ( (*b2uDimAddr)[Nb] * sizeof(int) );

  for (i = 0; i < Nu; ++i) {
    for (j = 0; j < d; ++j) {
      (*b2uAddr)[ (*b2uDimAddr)[ u2b[d*i+j] + 1 ] - itmpv[ u2b[d*i+j] ] ] = i;
      --itmpv[ u2b[d*i+j] ];
    }
  }

  /* Check that we traversed the arrays properly */
  for (i = 0; i < Nb; ++i) {
    if ( itmpv[i] != 0 ) {
      printf("File %s line %i: encountered error while constructing bottom-up connectivity, "
	     "aborting ...\n", __FILE__, __LINE__);
      exit(1);
    }
  }

  free( itmpv );
}


void topDownConnectivity( const int * const top2Vertices,
			  const int Nt,
			  const int Nvt,
			  const int Nvd,
			  const int * const localConnectivity,
			  int **t2dAddr,
			  const int NdLoc )
{
  int i, j, counter = 0, lastflag;
  int itmp1, itmp2, itmp3, itmp4, itmp;
  int **buffer = NULL;

  buffer = (int **) malloc ( Nt * NdLoc * sizeof(int *) );

  *t2dAddr = (int *) malloc ( Nt * NdLoc * sizeof(int) );
  memset( *t2dAddr, 0,  Nt * NdLoc * sizeof(int) );

  if (Nvd == 2) {
    
    for (i = 0; i < Nt; ++i) {
      for (j = 0; j < NdLoc; ++j, ++counter) {
	buffer[counter] = (int *) malloc ((Nvd+1) * sizeof(int));
	itmp1 = top2Vertices[ Nvt * i + localConnectivity[ Nvd*j ] ];
	itmp2 = top2Vertices[ Nvt * i + localConnectivity[ Nvd*j + 1 ] ];
	if (itmp1 > itmp2) {
	  buffer[counter][0] = itmp2;
	  buffer[counter][1] = itmp1;	  
	} else {
	  buffer[counter][0] = itmp1;
	  buffer[counter][1] = itmp2;	  
	}
	buffer[counter][2] = counter;
      }
    }
    
    qsort( buffer, counter, sizeof(int *), compareSize2 );
    
  } else {
    
    for (i = 0; i < Nt; ++i) {
      for (j = 0; j < NdLoc; ++j, ++counter) {
	buffer[counter] = (int *) malloc ((Nvd+1) * sizeof(int));
	itmp1 = top2Vertices[ Nvt * i + localConnectivity[ Nvd*j ] ];
	itmp2 = top2Vertices[ Nvt * i + localConnectivity[ Nvd*j + 1 ] ];
	itmp3 = top2Vertices[ Nvt * i + localConnectivity[ Nvd*j + 2 ] ];

	if (itmp1 > itmp2) { itmp4 = itmp1; itmp1 = itmp2; itmp2 = itmp4; }
	if (itmp1 > itmp3) { itmp4 = itmp1; itmp1 = itmp3; itmp3 = itmp4; }
	if (itmp2 > itmp3) { itmp4 = itmp2; itmp2 = itmp3; itmp3 = itmp4; }

	buffer[counter][0] = itmp1;
	buffer[counter][1] = itmp2;
	buffer[counter][2] = itmp3;
	buffer[counter][3] = counter;
      }
    }
    
    qsort( buffer, counter, sizeof(int *), compareSize3 );
    
  }

  itmp  = buffer[0][Nvd];
  itmp1 = itmp/NdLoc;         /* Number of top element */
  itmp2 = itmp - itmp1*NdLoc; /* Number of local down element */
  (*t2dAddr)[NdLoc*itmp1 + itmp2] = 0;
  lastflag = 0;

  for (i = 1; i < counter; ++i) {
    itmp  = buffer[i][Nvd];
    itmp1 = itmp/NdLoc;         /* Number of top element */
    itmp2 = itmp - itmp1*NdLoc; /* Number of local down element */
    if ( ( buffer[i][0] == buffer[i-1][0] )
	 && ( buffer[i][1] == buffer[i-1][1] )
	 && ( buffer[i][Nvd-1] == buffer[i-1][Nvd-1] ) )
      (*t2dAddr)[NdLoc*itmp1 + itmp2] = lastflag;
    else {
      (*t2dAddr)[NdLoc*itmp1 + itmp2] = ++lastflag;
    }
  }

  for (i = 0; i < Nt; ++i) qsort( (*t2dAddr) + NdLoc*i, NdLoc, sizeof(int), compareInt );

  for (i = 0; i < counter; ++i) free( buffer[i] );
  free( buffer );
}


void primalMesh2dConstructConnectivity( primalMesh2d * pMesh2d )
{
  int triEdgeConnectivity[6] = { 0, 1, 0, 2, 1, 2 };

  /* Vertex to edges */
  if ( pMesh2d->v2e == NULL )
    bottomUpConnectivity( pMesh2d->Nver, pMesh2d->Ned, 2, pMesh2d->edges,
			  &pMesh2d->v2eDim, &pMesh2d->v2e );

  /* Vertex to triangles */
  if ( pMesh2d->v2t == NULL )
    bottomUpConnectivity( pMesh2d->Nver, pMesh2d->Nelt, 3, pMesh2d->elements,
			  &pMesh2d->v2tDim, &pMesh2d->v2t );

  /* Tetrahedron to edges */
  if ( pMesh2d->t2e == NULL )
    topDownConnectivity( pMesh2d->elements, pMesh2d->Nelt, 3, 2,
			 triEdgeConnectivity, &pMesh2d->t2e, 3 );

  /* Edge to triangles - special code provided */
  if ( pMesh2d->e2t == NULL )
    bottomUpLastLevel( pMesh2d->Ned, pMesh2d->Nelt, pMesh2d->t2e, 3, &pMesh2d->e2t );
}


void primalMeshConstructConnectivity( primalMesh * pMesh )
{
  int faceEdgeConnectivity[6] = { 0, 1, 1, 2, 2, 0 };
  int tetraEdgeConnectivity[12] = { 0, 1, 0, 2, 0, 3, 1, 2, 1, 3, 2, 3 };
  int tetraFaceConnectivity[12] = { 0, 1, 2, 0, 1, 3, 0, 2, 3, 1, 2, 3 };

  /* Vertex to edges */
  if ( pMesh->v2e == NULL )
    bottomUpConnectivity( pMesh->Nver, pMesh->Ned, 2, pMesh->edges,
			  &pMesh->v2eDim, &pMesh->v2e );
  
  /* Vertex to faces */
  if ( pMesh->v2f == NULL )
    bottomUpConnectivity( pMesh->Nver, pMesh->Nfc, 3, pMesh->faces,
  			  &pMesh->v2fDim, &pMesh->v2f );

  /* Vertex to tetrahedra */
  if ( pMesh->v2t == NULL )
    bottomUpConnectivity( pMesh->Nver, pMesh->Nelt, 4, pMesh->elements,
  			  &pMesh->v2tDim, &pMesh->v2t );
  
  /* Face to edges */
  if ( pMesh->f2e == NULL )
    topDownConnectivity( pMesh->faces, pMesh->Nfc, 3, 2,
  			 faceEdgeConnectivity, &pMesh->f2e, 3 );

  /* Edge to faces */
  if ( pMesh->e2f == NULL )
    bottomUpConnectivity( pMesh->Ned, pMesh->Nfc, 3, pMesh->f2e,
  			  &pMesh->e2fDim, &pMesh->e2f );

  /* Tetrahedron to edges */
  if ( pMesh->t2e == NULL )
    topDownConnectivity( pMesh->elements, pMesh->Nelt, 4, 2,
  			 tetraEdgeConnectivity, &pMesh->t2e, 6 );

  /* Edge to tetrahedra */
  if ( pMesh->e2t == NULL )
    bottomUpConnectivity( pMesh->Ned, pMesh->Nelt, 6, pMesh->t2e,
  			  &pMesh->e2tDim, &pMesh->e2t );

  /* Tetrahedron to faces */
  if ( pMesh->t2f == NULL )
    topDownConnectivity( pMesh->elements, pMesh->Nelt, 4, 3,
  			 tetraFaceConnectivity, &pMesh->t2f, 4 );

  /* Face to tetrahedra - special code provided */
  if ( pMesh->f2t == NULL )
    bottomUpLastLevel( pMesh->Nfc, pMesh->Nelt, pMesh->t2f, 4, &pMesh->f2t );
}


void primalMesh2dDestroy( primalMesh2d * pMesh2d )
{
  if ( pMesh2d->vertices )  { free( pMesh2d->vertices ); pMesh2d->vertices = NULL; }
  if ( pMesh2d->vTopFlags ) { free( pMesh2d->vTopFlags ); pMesh2d->vTopFlags = NULL; }
  if ( pMesh2d->v2e )       { free( pMesh2d->v2e ); pMesh2d->v2e = NULL; }
  if ( pMesh2d->v2eDim )    { free( pMesh2d->v2eDim ); pMesh2d->v2eDim = NULL; }
  if ( pMesh2d->v2t )       { free( pMesh2d->v2t ); pMesh2d->v2t = NULL; }
  if ( pMesh2d->v2tDim )    { free( pMesh2d->v2tDim ); pMesh2d->v2tDim = NULL; }
  pMesh2d->Nver = 0;

  if ( pMesh2d->edges )     { free( pMesh2d->edges ); pMesh2d->edges = NULL; }
  if ( pMesh2d->eTopFlags ) { free( pMesh2d->eTopFlags ); pMesh2d->eTopFlags = NULL; }
  if ( pMesh2d->e2t )       { free( pMesh2d->e2t ); pMesh2d->e2t = NULL; }
  pMesh2d->Ned = 0;

  if ( pMesh2d->elements )  { free( pMesh2d->elements ); pMesh2d->elements = NULL; }
  if ( pMesh2d->t2e )       { free( pMesh2d->t2e ); pMesh2d->t2e = NULL; }
  pMesh2d->Nelt = 0;
}


void primalMeshDestroy( primalMesh * pMesh )
{
  if ( pMesh->vertices )  { free( pMesh->vertices ); pMesh->vertices = NULL; }
  if ( pMesh->vTopFlags ) { free( pMesh->vTopFlags ); pMesh->vTopFlags = NULL; }
  if ( pMesh->v2e )       { free( pMesh->v2e ); pMesh->v2e = NULL; }
  if ( pMesh->v2eDim )    { free( pMesh->v2eDim ); pMesh->v2eDim = NULL; }
  if ( pMesh->v2f )       { free( pMesh->v2f ); pMesh->v2f = NULL; }
  if ( pMesh->v2fDim )    { free( pMesh->v2fDim ); pMesh->v2fDim = NULL; }
  if ( pMesh->v2t )       { free( pMesh->v2t ); pMesh->v2t = NULL; }
  if ( pMesh->v2tDim )    { free( pMesh->v2tDim ); pMesh->v2tDim = NULL; }
  pMesh->Nver = 0;

  if ( pMesh->edges )     { free( pMesh->edges ); pMesh->edges = NULL; }
  if ( pMesh->eTopFlags ) { free( pMesh->eTopFlags ); pMesh->eTopFlags = NULL; }
  if ( pMesh->e2f )       { free( pMesh->e2f ); pMesh->e2f = NULL; }
  if ( pMesh->e2fDim )    { free( pMesh->e2fDim ); pMesh->e2fDim = NULL; }
  if ( pMesh->e2t )       { free( pMesh->e2t ); pMesh->e2t = NULL; }
  if ( pMesh->e2tDim )    { free( pMesh->e2tDim ); pMesh->e2tDim = NULL; }
  pMesh->Ned = 0;

  if ( pMesh->faces )     { free( pMesh->faces ); pMesh->faces = NULL; }
  if ( pMesh->fTopFlags ) { free( pMesh->fTopFlags ); pMesh->fTopFlags = NULL; }
  if ( pMesh->f2t )       { free( pMesh->f2t ); pMesh->f2t = NULL; }
  if ( pMesh->f2e )       { free( pMesh->f2e ); pMesh->f2e = NULL; }
  pMesh->Nfc = 0;

  if ( pMesh->elements )  { free( pMesh->elements ); pMesh->elements = NULL; }
  if ( pMesh->t2e )       { free( pMesh->t2e ); pMesh->t2e = NULL; }
  if ( pMesh->t2f )       { free( pMesh->t2f ); pMesh->t2f = NULL; }
  pMesh->Nelt = 0;
}

