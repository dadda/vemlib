#include "pmgo.h"

double condEscobarModFast( const double * const V1,
			   const double * const V2,
			   const double * const F,
			   const double * const R,
			   const double c,
			   const double volume0 )
{
  double x1, y1, z1, x2, y2, z2, x3, y3, z3;
  double xcross, ycross, zcross;
  double volume, area, length, delta;

  x1 = F[0] - V1[0];
  y1 = F[1] - V1[1];
  z1 = F[2] - V1[2];
  x2 = V2[0] - V1[0];
  y2 = V2[1] - V1[1];
  z2 = V2[2] - V1[2];
  x3 = R[0] - V1[0];
  y3 = R[1] - V1[1];
  z3 = R[2] - V1[2];

  length =
    x1 * x1 + y1 * y1 + z1 * z1 +
    x2 * x2 + y2 * y2 + z2 * z2 +
    x3 * x3 + y3 * y3 + z3 * z3;

  xcross = y1*z2 - z1*y2;
  ycross = z1*x2 - x1*z2;
  zcross = x1*y2 - y1*x2;
  area   = xcross*xcross + ycross*ycross + zcross*zcross;

  xcross = y2*z3 - z2*y3;
  ycross = z2*x3 - x2*z3;
  zcross = x2*y3 - y2*x3;
  area   += xcross*xcross + ycross*ycross + zcross*zcross;

  xcross = y1*z3 - z1*y3;
  ycross = z1*x3 - x1*z3;
  zcross = x1*y3 - y1*x3;
  area   += xcross*xcross + ycross*ycross + zcross*zcross;

  volume = x3 * (y1 * z2 - z1 * y2)
    + x1 * (y2 * z3 - z2 * y3)
    + x2 * (y3 * z1 - z3 * y1);

  delta = 1.0 / ( 1.0 + c * exp( volume / volume0 ) );

  return 2.0 * sqrt( area * length ) / ( volume + sqrt( volume * volume + delta * delta ) );
}


double condEscobarMod( const double * const V1,
		       const double * const V2,
		       const double * const F,
		       const double * const R,
		       const double c,
		       const double volume0 )
{
  double x1, y1, z1, x2, y2, z2, x3, y3, z3;
  double xcross, ycross, zcross;
  double volume, area, length, delta;
  double buffer1[2], buffer2[2], buffer3[2];

  x1 = F[0] - V1[0];
  y1 = F[1] - V1[1];
  z1 = F[2] - V1[2];
  x2 = V2[0] - V1[0];
  y2 = V2[1] - V1[1];
  z2 = V2[2] - V1[2];
  x3 = R[0] - V1[0];
  y3 = R[1] - V1[1];
  z3 = R[2] - V1[2];

  length =
    x1 * x1 + y1 * y1 + z1 * z1 +
    x2 * x2 + y2 * y2 + z2 * z2 +
    x3 * x3 + y3 * y3 + z3 * z3;

  xcross = orient2d( F+1, V2+1, V1+1 );
  buffer1[0] = F[2]; buffer1[1] = F[0];
  buffer2[0] = V2[2]; buffer2[1] = V2[0];
  buffer3[0] = V1[2]; buffer3[1] = V1[0];
  ycross = orient2d( buffer1, buffer2, buffer3 );
  zcross = orient2d( F, V2, V1 );
  
  area = xcross*xcross + ycross*ycross + zcross*zcross;

  xcross = orient2d( V2+1, R+1, V1+1 );
  buffer1[0] = V2[2]; buffer1[1] = V2[0];
  buffer2[0] = R[2]; buffer2[1] = R[0];
  ycross = orient2d( buffer1, buffer2, buffer3 );
  zcross = orient2d( V2, R, V1 );

  area += xcross*xcross + ycross*ycross + zcross*zcross;

  xcross = orient2d( F+1, R+1, V1+1 );
  buffer1[0] = F[2]; buffer1[1] = F[0];
  ycross = orient2d( buffer1, buffer2, buffer3 );
  zcross = orient2d( F, R, V1 );

  area += xcross*xcross + ycross*ycross + zcross*zcross;

  volume = orient3d( V1, V2, F, R );

  delta = 1.0 / ( 1.0 + c * exp( volume / volume0 ) );

  return 2.0 * sqrt( area * length ) / ( volume + sqrt( volume * volume + delta * delta ) );
}


double sqm2dVertex( const double * const x,
		    const double * const lb,
		    const double * const ub,
		    const int vIdx,
		    const dualMesh2d * const dMesh2d,
		    double * const vertices,
		    const double c )
{
  int i, j, pIdx, NpolLoc, NverLoc;
  double sqm = 0.0, polySQM, xTmp, yTmp, A0;
  double Lsq, delta, A;
  double vertex[2], center[2], lbLoc[2], ubLoc[2], a[2], b[2];

  vertex[0]          = x[0] * ( ub[0] - lb[0] ) + lb[0];
  vertex[1]          = x[1] * ( ub[1] - lb[1] ) + lb[1];
  vertices[2*vIdx]   = vertex[0];
  vertices[2*vIdx+1] = vertex[1];

  NpolLoc = dMesh2d->v2pDim[vIdx+1] - dMesh2d->v2pDim[vIdx];
  for (i = 0; i < NpolLoc; ++i) {
    pIdx    = dMesh2d->v2p[ dMesh2d->v2pDim[vIdx] + i ];
    NverLoc = dMesh2d->p2vDim[pIdx+1] - dMesh2d->p2vDim[pIdx];

    center[0] = 0.0; center[1] = 0.0;
    lbLoc[0] = DBL_MAX;
    lbLoc[1] = DBL_MAX;
    ubLoc[0] = -DBL_MAX;
    ubLoc[1] = -DBL_MAX;

    for (j = 0; j < NverLoc; ++j) {
      xTmp = vertices[2*dMesh2d->p2v[dMesh2d->p2vDim[pIdx]+j]];
      yTmp = vertices[2*dMesh2d->p2v[dMesh2d->p2vDim[pIdx]+j]+1];
      center[0] += xTmp;
      center[1] += yTmp;
      if ( xTmp < lbLoc[0] ) lbLoc[0] = xTmp;
      if ( yTmp < lbLoc[1] ) lbLoc[1] = yTmp;
      if ( xTmp > ubLoc[0] ) ubLoc[0] = xTmp;
      if ( yTmp > ubLoc[1] ) ubLoc[1] = yTmp;
    }
    center[0] /= (double)(NverLoc);
    center[1] /= (double)(NverLoc);
    A0        = (ubLoc[0] - lbLoc[0]) * (ubLoc[1] - lbLoc[1]);

    polySQM = 0.0;
    for (j = 0; j < NverLoc; ++j) {
      a[0] = center[0] - vertices[2*dMesh2d->p2v[dMesh2d->p2vDim[pIdx]+j]];
      a[1] = center[1] - vertices[2*dMesh2d->p2v[dMesh2d->p2vDim[pIdx]+j]+1];
      b[0] = vertices[2*dMesh2d->p2v[dMesh2d->p2vDim[pIdx]+(j+1)%NverLoc]]
	- vertices[2*dMesh2d->p2v[dMesh2d->p2vDim[pIdx]+j]];
      b[1] = vertices[2*dMesh2d->p2v[dMesh2d->p2vDim[pIdx]+(j+1)%NverLoc]+1]
	- vertices[2*dMesh2d->p2v[dMesh2d->p2vDim[pIdx]+j]+1];

      A       = a[0] * b[1] - b[0] * a[1];
      Lsq     = a[0]*a[0] + a[1]*a[1] + b[0]*b[0] + b[1]*b[1];
      delta   = 1.0 / ( 1.0 + c*exp(A / A0) );
      polySQM += Lsq / (A + sqrt(A*A + delta));
    }

    sqm += polySQM;
  }

  return sqm;
}


double sqmPolyhedron( const dualMesh * const dMesh,
		      const double * const vertices,
		      const double c,
		      const int pIdx )
{
  int j, k, Nfc, Ned, counter, itmp;
  int hFace, tFace, hEdge, tEdge;
  int **polyhedron = NULL;
  int *itmpV       = NULL;

  double xmin, xmax, ymin, ymax, zmin, zmax;
  double volume0, sqm = 0.0;
  
  double polyhedronCentroid[3];
  double *facesCenters = NULL;


  Nfc = dMesh->p2hfDim[pIdx+1] - dMesh->p2hfDim[pIdx];
  facesCenters = (double *) calloc ( 3 * Nfc, sizeof(double) );

  polyhedron = (int **) malloc ( Nfc * sizeof(int *) );

  xmin = DBL_MAX; ymin = DBL_MAX; zmin = DBL_MAX;
  xmax = -DBL_MAX; ymax = -DBL_MAX; zmax = -DBL_MAX;
  counter = 0;
  for (j = 0; j < Nfc; ++j) {
    hFace         = dMesh->p2hf[ dMesh->p2hfDim[pIdx] + j ];
    tFace         = hFace/2;
    Ned           = dMesh->f2heDim[ tFace + 1 ] - dMesh->f2heDim[ tFace ];
    counter       += Ned;
    polyhedron[j] = (int *) malloc ( Ned * sizeof(int) );
    if ( hFace%2 ) {
      for (k = 0; k < Ned; ++k) {
	hEdge = dMesh->f2he[ dMesh->f2heDim[tFace] + Ned - 1 - k ];
	tEdge = hEdge/2;
	polyhedron[j][k] = dMesh->edges[ 2 * tEdge + 1 - hEdge%2 ];
	facesCenters[3*j]   += vertices[ 3 * polyhedron[j][k]     ];
	facesCenters[3*j+1] += vertices[ 3 * polyhedron[j][k] + 1 ];
	facesCenters[3*j+2] += vertices[ 3 * polyhedron[j][k] + 2 ];
	if ( xmin > vertices[3*polyhedron[j][k]] ) xmin = vertices[3*polyhedron[j][k]];
	if ( xmax < vertices[3*polyhedron[j][k]] ) xmax = vertices[3*polyhedron[j][k]];
	if ( ymin > vertices[3*polyhedron[j][k]+1] ) ymin = vertices[3*polyhedron[j][k]+1];
	if ( ymax < vertices[3*polyhedron[j][k]+1] ) ymax = vertices[3*polyhedron[j][k]+1];
	if ( zmin > vertices[3*polyhedron[j][k]+2] ) zmin = vertices[3*polyhedron[j][k]+2];
	if ( zmax < vertices[3*polyhedron[j][k]+2] ) zmax = vertices[3*polyhedron[j][k]+2];
      }
    } else {
      for (k = 0; k < Ned; ++k) {
	hEdge = dMesh->f2he[ dMesh->f2heDim[tFace] + k ];
	tEdge = hEdge/2;
	polyhedron[j][k] = dMesh->edges[ 2 * tEdge + hEdge%2 ];
	facesCenters[3*j]   += vertices[ 3 * polyhedron[j][k]     ];
	facesCenters[3*j+1] += vertices[ 3 * polyhedron[j][k] + 1 ];
	facesCenters[3*j+2] += vertices[ 3 * polyhedron[j][k] + 2 ];
	if ( xmin > vertices[3*polyhedron[j][k]] ) xmin = vertices[3*polyhedron[j][k]];
	if ( xmax < vertices[3*polyhedron[j][k]] ) xmax = vertices[3*polyhedron[j][k]];
	if ( ymin > vertices[3*polyhedron[j][k]+1] ) ymin = vertices[3*polyhedron[j][k]+1];
	if ( ymax < vertices[3*polyhedron[j][k]+1] ) ymax = vertices[3*polyhedron[j][k]+1];
	if ( zmin > vertices[3*polyhedron[j][k]+2] ) zmin = vertices[3*polyhedron[j][k]+2];
	if ( zmax < vertices[3*polyhedron[j][k]+2] ) zmax = vertices[3*polyhedron[j][k]+2];
      }
    }
    facesCenters[3*j]   /= (double)(Ned);
    facesCenters[3*j+1] /= (double)(Ned);
    facesCenters[3*j+2] /= (double)(Ned);
  }

  itmpV = (int *) malloc (counter * sizeof(int));
  itmp  = 0;
  for (j = 0; j < Nfc; ++j) {
    hFace = dMesh->p2hf[ dMesh->p2hfDim[pIdx] + j ];
    tFace = hFace/2;
    Ned   = dMesh->f2heDim[ tFace + 1 ] - dMesh->f2heDim[ tFace ];
    for (k = 0; k < Ned; ++k) itmpV[itmp++] = polyhedron[j][k];
  }
  qsort( itmpV, counter, sizeof(int), compareInt );
  itmp = 1;
  polyhedronCentroid[0] = vertices[3*itmpV[0]];
  polyhedronCentroid[1] = vertices[3*itmpV[0]+1];
  polyhedronCentroid[2] = vertices[3*itmpV[0]+2];
  for (j = 1; j < counter; ++j) {
    if ( itmpV[j] != itmpV[j-1] ) {
      polyhedronCentroid[0] += vertices[3*itmpV[j]];
      polyhedronCentroid[1] += vertices[3*itmpV[j]+1];
      polyhedronCentroid[2] += vertices[3*itmpV[j]+2];
      ++itmp;
    }
  }
  polyhedronCentroid[0] /= (double)(itmp);
  polyhedronCentroid[1] /= (double)(itmp);
  polyhedronCentroid[2] /= (double)(itmp);
  free( itmpV );

  volume0 = (xmax - xmin) * (ymax - ymin) * (zmax - zmin);
  sqm     = 0.0;
  for (j = 0; j < Nfc; ++j) {
    hFace = dMesh->p2hf[ dMesh->p2hfDim[pIdx] + j ];
    tFace = hFace/2;
    Ned   = dMesh->f2heDim[ tFace + 1 ] - dMesh->f2heDim[ tFace ];
    for (k = 0; k < Ned; ++k) {
      sqm += condEscobarModFast( vertices + 3*(polyhedron[j][k]),
				 vertices + 3*(polyhedron[j][(k+1)%Ned]),
				 facesCenters + 3*j,
				 polyhedronCentroid,
				 c,
				 volume0 );
      sqm += condEscobarModFast( vertices + 3*(polyhedron[j][(k+1)%Ned]),
				 facesCenters + 3*j,
				 vertices + 3*(polyhedron[j][k]),
				 polyhedronCentroid,
				 c,
				 volume0 );
    }
    free( polyhedron[j] );
  }

  sqm /= (double)(2*counter);

  free( facesCenters );
  free( polyhedron );

  return sqm;
}


void leastSquaresFace( const dualMesh * const dMesh,
		       const double * const vertices,
		       const int fIdx,
		       double * normal,
		       double * LSsum )
{
  int k, Ned, itmp;
  int hEdge, tEdge;

  lapack_int m, n;

  double distance, tmp;

  double faceCentroid[3] = {0.0, 0.0, 0.0};
  double singularValues[3], VT[9], work[3];
  double *faceVerticesShifted = NULL, *buffer = NULL;


  Ned = dMesh->f2heDim[ fIdx + 1 ] - dMesh->f2heDim[ fIdx ];

  faceVerticesShifted = (double *) malloc ( 3 * Ned * sizeof(double) );
  buffer              = (double *) malloc ( 3 * Ned * sizeof(double) );
  for (k = 0; k < Ned; ++k) {
    hEdge = dMesh->f2he[ dMesh->f2heDim[fIdx] + k ];
    tEdge = hEdge/2;
    itmp  = dMesh->edges[ 2 * tEdge + hEdge%2 ];
    faceVerticesShifted[3*k]   = vertices[3*itmp  ];
    faceVerticesShifted[3*k+1] = vertices[3*itmp+1];
    faceVerticesShifted[3*k+2] = vertices[3*itmp+2];
    faceCentroid[0] += faceVerticesShifted[3*k];
    faceCentroid[1] += faceVerticesShifted[3*k+1];
    faceCentroid[2] += faceVerticesShifted[3*k+2];
  }
  faceCentroid[0] /= (double)(Ned);
  faceCentroid[1] /= (double)(Ned);
  faceCentroid[2] /= (double)(Ned);

  for (k = 0; k < Ned; ++k) {
    faceVerticesShifted[3*k]   -= faceCentroid[0];
    faceVerticesShifted[3*k+1] -= faceCentroid[1];
    faceVerticesShifted[3*k+2] -= faceCentroid[2];
    buffer[k]       = faceVerticesShifted[3*k];
    buffer[k+Ned]   = faceVerticesShifted[3*k+1];
    buffer[k+2*Ned] = faceVerticesShifted[3*k+2];
  }

  m = Ned;
  n = 3;
  LAPACKE_dgesvd( LAPACK_COL_MAJOR, 'N', 'A', m, n, buffer, m,
		  singularValues, NULL, m, VT, n, work );

  /* Select singular vector of smallest singular value
     (last column of V, i.e., last row of VT) */
  normal[0] = VT[2];
  normal[1] = VT[5];
  normal[2] = VT[8];
  tmp       = cblas_dnrm2(3, normal, 1);
  normal[0] /= tmp;
  normal[1] /= tmp;
  normal[2] /= tmp;

  *LSsum = 0.0;
  for (k = 0; k < Ned; ++k) {
    distance = faceVerticesShifted[3*k]*normal[0]
      + faceVerticesShifted[3*k+1]*normal[1]
      + faceVerticesShifted[3*k+2]*normal[2];
    *LSsum += distance * distance;
  }

  free( faceVerticesShifted );
  free( buffer );
}
