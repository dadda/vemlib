#include "pmgo.h"


/*****************************************************************************/
/*                                                                           */
/*  tetcircumcenter()   Find the circumcenter of a tetrahedron.              */
/*                                                                           */
/*  The result is returned both in terms of xyz coordinates and xi-eta-zeta  */
/*  coordinates, relative to the tetrahedron's point `a' (that is, `a' is    */
/*  the origin of both coordinate systems).  Hence, the xyz coordinates      */
/*  returned are NOT absolute; one must add the coordinates of `a' to        */
/*  find the absolute coordinates of the circumcircle.  However, this means  */
/*  that the result is frequently more accurate than would be possible if    */
/*  absolute coordinates were returned, due to limited floating-point        */
/*  precision.  In general, the circumradius can be computed much more       */
/*  accurately.                                                              */
/*                                                                           */
/*  The xi-eta-zeta coordinate system is defined in terms of the             */
/*  tetrahedron.  Point `a' is the origin of the coordinate system.          */
/*  The edge `ab' extends one unit along the xi axis.  The edge `ac'         */
/*  extends one unit along the eta axis.  The edge `ad' extends one unit     */
/*  along the zeta axis.  These coordinate values are useful for linear      */
/*  interpolation.                                                           */
/*                                                                           */
/*  If `xi' is NULL on input, the xi-eta-zeta coordinates will not be        */
/*  computed.                                                                */
/*                                                                           */
/*****************************************************************************/

void tetcircumcenter( const double * const a,
		      const double * const b,
		      const double * const c,
		      const double * const d,
		      double * relative_circumcenter,
		      double * xi,
		      double * eta,
		      double * zeta )
{
  double xba, yba, zba, xca, yca, zca, xda, yda, zda;
  double balength, calength, dalength;
  double xcrosscd, ycrosscd, zcrosscd;
  double xcrossdb, ycrossdb, zcrossdb;
  double xcrossbc, ycrossbc, zcrossbc;
  double denominator;
  double xcirca, ycirca, zcirca;

  /* Use coordinates relative to point `a' of the tetrahedron. */
  xba = b[0] - a[0];
  yba = b[1] - a[1];
  zba = b[2] - a[2];
  xca = c[0] - a[0];
  yca = c[1] - a[1];
  zca = c[2] - a[2];
  xda = d[0] - a[0];
  yda = d[1] - a[1];
  zda = d[2] - a[2];
  /* Squares of lengths of the edges incident to `a'. */
  balength = xba * xba + yba * yba + zba * zba;
  calength = xca * xca + yca * yca + zca * zca;
  dalength = xda * xda + yda * yda + zda * zda;
  /* Cross products of these edges. */
  xcrosscd = yca * zda - yda * zca;
  ycrosscd = zca * xda - zda * xca;
  zcrosscd = xca * yda - xda * yca;
  xcrossdb = yda * zba - yba * zda;
  ycrossdb = zda * xba - zba * xda;
  zcrossdb = xda * yba - xba * yda;
  xcrossbc = yba * zca - yca * zba;
  ycrossbc = zba * xca - zca * xba;
  zcrossbc = xba * yca - xca * yba;

  /* Calculate the denominator of the formulae. */
#ifdef EXACT
  /* Use orient3d() from http://www.cs.cmu.edu/~quake/robust.html     */
  /*   to ensure a correctly signed (and reasonably accurate) result, */
  /*   avoiding any possibility of division by zero.                  */
  denominator = 0.5 / orient3d(b, c, d, a);
#else
  /* Take your chances with floating-point roundoff. */
  denominator = 0.5 / (xba * xcrosscd + yba * ycrosscd + zba * zcrosscd);
#endif

  /* Calculate offset (from `a') of relative_circumcenter. */
  xcirca = (balength * xcrosscd + calength * xcrossdb + dalength * xcrossbc) *
           denominator;
  ycirca = (balength * ycrosscd + calength * ycrossdb + dalength * ycrossbc) *
           denominator;
  zcirca = (balength * zcrosscd + calength * zcrossdb + dalength * zcrossbc) *
           denominator;
  relative_circumcenter[0] = xcirca;
  relative_circumcenter[1] = ycirca;
  relative_circumcenter[2] = zcirca;

  if (xi != (double *) NULL) {
    /* To interpolate a linear function at the circumcenter, define a    */
    /*   coordinate system with a xi-axis directed from `a' to `b',      */
    /*   an eta-axis directed from `a' to `c', and a zeta-axis directed  */
    /*   from `a' to `d'.  The values for xi, eta, and zeta are computed */
    /*   by Cramer's Rule for solving systems of linear equations.       */
    *xi = (xcirca * xcrosscd + ycirca * ycrosscd + zcirca * zcrosscd) *
          (2.0 * denominator);
    *eta = (xcirca * xcrossdb + ycirca * ycrossdb + zcirca * zcrossdb) *
           (2.0 * denominator);
    *zeta = (xcirca * xcrossbc + ycirca * ycrossbc + zcirca * zcrossbc) *
            (2.0 * denominator);
  }
}


/*****************************************************************************/
/*                                                                           */
/*  tricircumcenter3d()   Find the circumcenter of a triangle in 3D.         */
/*                                                                           */
/*  The result is returned both in terms of xyz coordinates and xi-eta       */
/*  coordinates, relative to the triangle's point `a' (that is, `a' is       */
/*  the origin of both coordinate systems).  Hence, the xyz coordinates      */
/*  returned are NOT absolute; one must add the coordinates of `a' to        */
/*  find the absolute coordinates of the circumcircle.  However, this means  */
/*  that the result is frequently more accurate than would be possible if    */
/*  absolute coordinates were returned, due to limited floating-point        */
/*  precision.  In general, the circumradius can be computed much more       */
/*  accurately.                                                              */
/*                                                                           */
/*  The xi-eta coordinate system is defined in terms of the triangle.        */
/*  Point `a' is the origin of the coordinate system.  The edge `ab' extends */
/*  one unit along the xi axis.  The edge `ac' extends one unit along the    */
/*  eta axis.  These coordinate values are useful for linear interpolation.  */
/*                                                                           */
/*  If `xi' is NULL on input, the xi-eta coordinates will not be computed.   */
/*                                                                           */
/*****************************************************************************/

void tricircumcenter3d( const double * const a,
			const double * const b,
			const double * const c,
			double * circumcenter,
			double * xi,
			double * eta )
{
  double xba, yba, zba, xca, yca, zca;
  double balength, calength;
  double xcrossbc, ycrossbc, zcrossbc;
  double denominator;
  double xcirca, ycirca, zcirca;

  double buffer1[2], buffer2[2], buffer3[2];

  /* Use coordinates relative to point `a' of the triangle. */
  xba = b[0] - a[0];
  yba = b[1] - a[1];
  zba = b[2] - a[2];
  xca = c[0] - a[0];
  yca = c[1] - a[1];
  zca = c[2] - a[2];
  /* Squares of lengths of the edges incident to `a'. */
  balength = xba * xba + yba * yba + zba * zba;
  calength = xca * xca + yca * yca + zca * zca;
  
  /* Cross product of these edges. */
#ifdef EXACT
  /* Use orient2d() from http://www.cs.cmu.edu/~quake/robust.html     */
  /*   to ensure a correctly signed (and reasonably accurate) result, */
  /*   avoiding any possibility of division by zero.                  */
  xcrossbc = orient2d(b+1, c+1, a+1);
  buffer1[0] = b[2]; buffer1[1] = b[0];
  buffer2[0] = c[2]; buffer2[1] = c[0];
  buffer3[0] = a[2]; buffer3[1] = a[0];
  ycrossbc = orient2d(buffer1, buffer2, buffer3);
  zcrossbc = orient2d(b, c, a);
#else
  /* Take your chances with floating-point roundoff. */
  xcrossbc = yba * zca - yca * zba;
  ycrossbc = zba * xca - zca * xba;
  zcrossbc = xba * yca - xca * yba;
#endif

  /* Calculate the denominator of the formulae. */
  denominator = 0.5 / (xcrossbc * xcrossbc + ycrossbc * ycrossbc +
                       zcrossbc * zcrossbc);

  /* Calculate offset (from `a') of circumcenter. */
  xcirca = ((balength * yca - calength * yba) * zcrossbc -
            (balength * zca - calength * zba) * ycrossbc) * denominator;
  ycirca = ((balength * zca - calength * zba) * xcrossbc -
            (balength * xca - calength * xba) * zcrossbc) * denominator;
  zcirca = ((balength * xca - calength * xba) * ycrossbc -
            (balength * yca - calength * yba) * xcrossbc) * denominator;
  circumcenter[0] = xcirca;
  circumcenter[1] = ycirca;
  circumcenter[2] = zcirca;

  if (xi != (double *) NULL) {
    /* To interpolate a linear function at the circumcenter, define a     */
    /*   coordinate system with a xi-axis directed from `a' to `b' and    */
    /*   an eta-axis directed from `a' to `c'.  The values for xi and eta */
    /*   are computed by Cramer's Rule for solving systems of linear      */
    /*   equations.                                                       */

    /* There are three ways to do this calculation - using xcrossbc, */
    /*   ycrossbc, or zcrossbc.  Choose whichever has the largest    */
    /*   magnitude, to improve stability and avoid division by zero. */
    if (((xcrossbc >= ycrossbc) ^ (-xcrossbc > ycrossbc)) &&
        ((xcrossbc >= zcrossbc) ^ (-xcrossbc > zcrossbc))) {
      *xi = (ycirca * zca - zcirca * yca) / xcrossbc;
      *eta = (zcirca * yba - ycirca * zba) / xcrossbc;
    } else if ((ycrossbc >= zcrossbc) ^ (-ycrossbc > zcrossbc)) {
      *xi = (zcirca * xca - xcirca * zca) / ycrossbc;
      *eta = (xcirca * zba - zcirca * xba) / ycrossbc;
    } else {
      *xi = (xcirca * yca - ycirca * xca) / zcrossbc;
      *eta = (ycirca * xba - xcirca * yba) / zcrossbc;
    }
  }
}


/*****************************************************************************/
/*                                                                           */
/*  tricircumcenter()   Find the circumcenter of a triangle.                 */
/*                                                                           */
/*  The result is returned both in terms of x-y coordinates and xi-eta       */
/*  coordinates, relative to the triangle's point `a' (that is, `a' is       */
/*  the origin of both coordinate systems).  Hence, the x-y coordinates      */
/*  returned are NOT absolute; one must add the coordinates of `a' to        */
/*  find the absolute coordinates of the circumcircle.  However, this means  */
/*  that the result is frequently more accurate than would be possible if    */
/*  absolute coordinates were returned, due to limited floating-point        */
/*  precision.  In general, the circumradius can be computed much more       */
/*  accurately.                                                              */
/*                                                                           */
/*  The xi-eta coordinate system is defined in terms of the triangle.        */
/*  Point `a' is the origin of the coordinate system.  The edge `ab' extends */
/*  one unit along the xi axis.  The edge `ac' extends one unit along the    */
/*  eta axis.  These coordinate values are useful for linear interpolation.  */
/*                                                                           */
/*  If `xi' is NULL on input, the xi-eta coordinates will not be computed.   */
/*                                                                           */
/*****************************************************************************/

void tricircumcenter( const double * const a,
		      const double * const b,
		      const double * const c,
		      double * circumcenter,
		      double * xi,
		      double * eta )
{
  double xba, yba, xca, yca;
  double balength, calength;
  double denominator;
  double xcirca, ycirca;

  /* Use coordinates relative to point `a' of the triangle. */
  xba = b[0] - a[0];
  yba = b[1] - a[1];
  xca = c[0] - a[0];
  yca = c[1] - a[1];
  /* Squares of lengths of the edges incident to `a'. */
  balength = xba * xba + yba * yba;
  calength = xca * xca + yca * yca;

  /* Calculate the denominator of the formulae. */
#ifdef EXACT
  /* Use orient2d() from http://www.cs.cmu.edu/~quake/robust.html     */
  /*   to ensure a correctly signed (and reasonably accurate) result, */
  /*   avoiding any possibility of division by zero.                  */
  denominator = 0.5 / orient2d(b, c, a);
#else
  /* Take your chances with floating-point roundoff. */
  denominator = 0.5 / (xba * yca - yba * xca);
#endif

  /* Calculate offset (from `a') of circumcenter. */
  xcirca = (yca * balength - yba * calength) * denominator;
  ycirca = (xba * calength - xca * balength) * denominator;
  circumcenter[0] = xcirca;
  circumcenter[1] = ycirca;

  if (xi != (double *) NULL) {
    /* To interpolate a linear function at the circumcenter, define a     */
    /*   coordinate system with a xi-axis directed from `a' to `b' and    */
    /*   an eta-axis directed from `a' to `c'.  The values for xi and eta */
    /*   are computed by Cramer's Rule for solving systems of linear      */
    /*   equations.                                                       */
    *xi = (xcirca * yca - ycirca * xca) * (2.0 * denominator);
    *eta = (ycirca * xba - xcirca * yba) * (2.0 * denominator);
  }
}


void replaceTetCircumcenter( const double * const a,
			     const double * const b,
			     const double * const c,
			     const double * const d,
			     const double xi,
			     const double eta,
			     const double zeta,
			     const double limInfTol,
			     double * centralPt )
{
  short testFlag = 0;
  bool info = false;
  double t = 0.0, newLimInfTol = limInfTol;
  double xiNew = 0.0, etaNew = 0.0, zetaNew = 0.0;
  double centroid[3], trialPt[3];
  double minDist, currDist, orientTest;

  testFlag = xi < 0.0 ? testFlag + 1 : testFlag;
  testFlag = eta < 0.0 ? testFlag + 1 : testFlag;
  testFlag = zeta < 0.0 ? testFlag + 1 : testFlag;

  if ( testFlag == 0 ) {

    /* Point will be on the plane xi + eta + zeta = 1 */
    t = 1.0 / ( 4.0 * ( xi + eta + zeta ) - 3.0 );
    xiNew   = 0.25 + t * ( xi - 0.25 );
    etaNew  = 0.25 + t * ( eta - 0.25 );
    zetaNew = 0.25 + t * ( zeta - 0.25 );

    centralPt[0] = a[0] + xiNew*(b[0] - a[0]) + etaNew*(c[0] - a[0]) + zetaNew*(d[0] - a[0]);
    centralPt[1] = a[1] + xiNew*(b[1] - a[1]) + etaNew*(c[1] - a[1]) + zetaNew*(d[1] - a[1]);
    centralPt[2] = a[2] + xiNew*(b[2] - a[2]) + etaNew*(c[2] - a[2]) + zetaNew*(d[2] - a[2]);

  } else if ( testFlag == 1 ) {

    /* Only one local coordinate is < 0 */

    if ( xi < 0.0 ) {

      t = 1.0 / ( 1.0 - 4.0 * xi );
      etaNew  = 0.25 + t * ( eta - 0.25 );
      zetaNew = 0.25 + t * ( zeta - 0.25 );
      centralPt[0] = a[0] + etaNew*(c[0] - a[0]) + zetaNew*(d[0] - a[0]);
      centralPt[1] = a[1] + etaNew*(c[1] - a[1]) + zetaNew*(d[1] - a[1]);
      centralPt[2] = a[2] + etaNew*(c[2] - a[2]) + zetaNew*(d[2] - a[2]);

    } else if ( eta < 0.0 ) {

      t = 1.0 / ( 1.0 - 4.0 * eta );
      xiNew   = 0.25 + t * ( xi - 0.25 );
      zetaNew = 0.25 + t * ( zeta - 0.25 );
      centralPt[0] = a[0] + xiNew*(b[0] - a[0]) + zetaNew*(d[0] - a[0]);
      centralPt[1] = a[1] + xiNew*(b[1] - a[1]) + zetaNew*(d[1] - a[1]);
      centralPt[2] = a[2] + xiNew*(b[2] - a[2]) + zetaNew*(d[2] - a[2]);

    } else {

      t = 1.0 / ( 1.0 - 4.0 * zeta );
      xiNew   = 0.25 + t * ( xi - 0.25 );
      etaNew  = 0.25 + t * ( eta - 0.25 );
      centralPt[0] = a[0] + xiNew*(b[0] - a[0]) + etaNew*(c[0] - a[0]);
      centralPt[1] = a[1] + xiNew*(b[1] - a[1]) + etaNew*(c[1] - a[1]);
      centralPt[2] = a[2] + xiNew*(b[2] - a[2]) + etaNew*(c[2] - a[2]);

    }

  } else if ( testFlag == 2 ) {

    centroid[0] = (a[0] + b[0] + c[0] + d[0]) * 0.25;
    centroid[1] = (a[1] + b[1] + c[1] + d[1]) * 0.25;
    centroid[2] = (a[2] + b[2] + c[2] + d[2]) * 0.25;

    orientTest = orient3d(a, b, c, centroid);

    if ( xi < 0.0 ) {
      t       = 1.0 / ( 1.0 - 4.0 * xi );
      etaNew  = 0.25 + t * ( eta - 0.25 );
      zetaNew = 0.25 + t * ( zeta - 0.25 );
      trialPt[0] = a[0] + etaNew*(c[0] - a[0]) + zetaNew*(d[0] - a[0]);
      trialPt[1] = a[1] + etaNew*(c[1] - a[1]) + zetaNew*(d[1] - a[1]);
      trialPt[2] = a[2] + etaNew*(c[2] - a[2]) + zetaNew*(d[2] - a[2]);
      if ( orientTest > 0.0 ) {
	minDist  = orient3d(a, b, c, trialPt);
	currDist = orient3d(a, d, b, trialPt); if ( currDist < minDist ) minDist = currDist;
	currDist = orient3d(b, d, c, trialPt); if ( currDist < minDist ) minDist = currDist;
      } else {
	minDist  = orient3d(a, c, b, trialPt);
	currDist = orient3d(a, b, d, trialPt); if ( currDist < minDist ) minDist = currDist;
	currDist = orient3d(b, c, d, trialPt); if ( currDist < minDist ) minDist = currDist;
      }
      if ( minDist > newLimInfTol ) {
	newLimInfTol = minDist;
	centralPt[0] = trialPt[0];
	centralPt[1] = trialPt[1];
	centralPt[2] = trialPt[2];
	info = true;
      }
    }

    if ( eta < 0.0 ) {
      t       = 1.0 / ( 1.0 - 4.0 * eta );
      xiNew   = 0.25 + t * ( xi - 0.25 );
      zetaNew = 0.25 + t * ( zeta - 0.25 );
      trialPt[0] = a[0] + xiNew*(b[0] - a[0]) + zetaNew*(d[0] - a[0]);
      trialPt[1] = a[1] + xiNew*(b[1] - a[1]) + zetaNew*(d[1] - a[1]);
      trialPt[2] = a[2] + xiNew*(b[2] - a[2]) + zetaNew*(d[2] - a[2]);
      if ( orientTest > 0.0 ) {
	minDist  = orient3d(a, b, c, trialPt);
	currDist = orient3d(a, c, d, trialPt); if ( currDist < minDist ) minDist = currDist;
	currDist = orient3d(b, d, c, trialPt); if ( currDist < minDist ) minDist = currDist;
      } else {
	minDist  = orient3d(a, c, b, trialPt);
	currDist = orient3d(a, d, c, trialPt); if ( currDist < minDist ) minDist = currDist;
	currDist = orient3d(b, c, d, trialPt); if ( currDist < minDist ) minDist = currDist;
      }
      if ( minDist > newLimInfTol ) {
	newLimInfTol = minDist;
	centralPt[0] = trialPt[0];
	centralPt[1] = trialPt[1];
	centralPt[2] = trialPt[2];
	info = true;
      }
    }

    if ( zeta < 0.0 ) {
      t      = 1.0 / ( 1.0 - 4.0 * zeta );
      xiNew  = 0.25 + t * ( xi - 0.25 );
      etaNew = 0.25 + t * ( eta - 0.25 );
      trialPt[0] = a[0] + xiNew*(b[0] - a[0]) + etaNew*(c[0] - a[0]);
      trialPt[1] = a[1] + xiNew*(b[1] - a[1]) + etaNew*(c[1] - a[1]);
      trialPt[2] = a[2] + xiNew*(b[2] - a[2]) + etaNew*(c[2] - a[2]);
      if ( orientTest > 0.0 ) {
	minDist  = orient3d(a, c, d, trialPt);
	currDist = orient3d(a, d, b, trialPt); if ( currDist < minDist ) minDist = currDist;
	currDist = orient3d(b, d, c, trialPt); if ( currDist < minDist ) minDist = currDist;
      } else {
	minDist  = orient3d(a, d, c, trialPt);
	currDist = orient3d(a, b, d, trialPt); if ( currDist < minDist ) minDist = currDist;
	currDist = orient3d(b, c, d, trialPt); if ( currDist < minDist ) minDist = currDist;
      }
      if ( minDist > newLimInfTol ) {
	newLimInfTol = minDist;
	centralPt[0] = trialPt[0];
	centralPt[1] = trialPt[1];
	centralPt[2] = trialPt[2];
	info = true;
      }
    }

    if ( !info ) {
      printf("File %s line %i: could not compute central point, "
	     "aborting ...\n", __FILE__, __LINE__);
      exit(1);
    }

  } else {
    printf("File %s line %i: at least one local coordinate should be positive, "
	   "aborting ...\n", __FILE__, __LINE__);
    exit(1);
  }
}


void replaceTriCircumcenter( const double * const a,
			     const double * const b,
			     const double * const c,
			     const double xi,
			     const double eta,
			     double * centralPt )
{
  double t, xiNew, etaNew;

  if ( ( xi < 0.0 ) && ( eta > 0.0 ) ) {

    t      = 1.0 / ( 1.0 - 3.0 * xi );
    etaNew = ( 1.0 + t * ( 3.0 * eta - 1.0 ) ) / 3.0;
    centralPt[0] = a[0] + etaNew * ( c[0] - a[0] );
    centralPt[1] = a[1] + etaNew * ( c[1] - a[1] );
    centralPt[2] = a[2] + etaNew * ( c[2] - a[2] );

  } else if ( ( xi > 0.0 ) && ( eta < 0.0 ) ) {

    t     = 1.0 / ( 1.0 - 3.0 * eta );
    xiNew = ( 1.0 + t * ( 3.0 * xi - 1.0 ) ) / 3.0;
    centralPt[0] = a[0] + xiNew * ( b[0] - a[0] );
    centralPt[1] = a[1] + xiNew * ( b[1] - a[1] );
    centralPt[2] = a[2] + xiNew * ( b[2] - a[2] );

  } else if ( ( xi > 0.0 ) && ( eta > 0.0 ) ) {

    t      = 1.0 / ( 3.0 * ( xi + eta ) - 2.0 );
    xiNew  = ( 1.0 + t * ( 3.0 * xi - 1.0 ) ) / 3.0;
    etaNew = ( 1.0 + t * ( 3.0 * eta - 1.0 ) ) / 3.0;

    centralPt[0] = a[0] + xiNew*(b[0] - a[0]) + etaNew*(c[0] - a[0]);
    centralPt[1] = a[1] + xiNew*(b[1] - a[1]) + etaNew*(c[1] - a[1]);
    centralPt[2] = a[2] + xiNew*(b[2] - a[2]) + etaNew*(c[2] - a[2]);

  } else {
    printf("File %s line %i: unexpected behavior, "
	   "aborting ...\n", __FILE__, __LINE__);
    exit(1);
  }
}


double pdistOldDualVertices( const int dim,
			     const int pCurrCounter,
			     const int pCumCounter,
			     const int * const current2v,
			     const int NverLoc,
			     const int * const v2up,
			     const int * const v2upDim,
			     const int * const primal2dualPointers,
			     const double tolerance,
			     const double * const dualVertex,
			     double * dualVertices,
			     int * dualVerticesPointers )
{
  int itmp1, itmp2, itmp3, j, k, l;
  double tmp1, tmp2;
  
  tmp1  = DBL_MAX;
  itmp1 = -1;

  for (j = 0; j < NverLoc; ++j) {
    itmp3 = 
      NverLoc == 1 ?
      v2upDim[pCurrCounter+1] - v2upDim[pCurrCounter] :
      v2upDim[current2v[NverLoc*pCurrCounter+j]+1] - v2upDim[current2v[NverLoc*pCurrCounter+j]];
    for (k = 0; k < itmp3; ++k) {
      itmp2 =
	NverLoc == 1 ?
	v2up[ v2upDim[ pCurrCounter ] + k ] :
	v2up[ v2upDim[ current2v[NverLoc*pCurrCounter+j] ] + k ];
      if ( ( itmp2 != pCurrCounter ) && ( primal2dualPointers[itmp2] != -1 ) ) {
	tmp2 = 0.0;
	for (l = 0; l < dim; ++l)
	  tmp2 +=
	    (dualVertices[dim*dualVerticesPointers[primal2dualPointers[itmp2]]+l] - dualVertex[l])
	    * (dualVertices[dim*dualVerticesPointers[primal2dualPointers[itmp2]]+l] - dualVertex[l]);
	if ( tmp2 < tmp1 ) { tmp1 = tmp2; itmp1 = itmp2; }
      }
    }
  }
  if (tmp1 < tolerance)
    dualVerticesPointers[pCumCounter] = dualVerticesPointers[ primal2dualPointers[itmp1] ];

  return tmp1;
}


void pdistNewDualVertices( const int dim,
			   const int pCurrCounter,
			   const int pCumCounter,
			   const int * const up2v,
			   const int NverLoc,
			   const int * const v2up,
			   const int * const v2upDim,
			   const int * const primal2dualPointers,
			   const double tolerance,
			   const double * const dualVertex,
			   double * dualVertices,
			   int * dualVerticesPointers,
			   int * dualVerticesCounter )
{
  int itmp1, itmp2, itmp3, j, k, l;
  double tmp1, tmp2;

  tmp1  = DBL_MAX;
  itmp1 = -1;
  for (j = 0; j < NverLoc; ++j) {
    itmp3 = v2upDim[ up2v[NverLoc*pCurrCounter+j] + 1 ] - v2upDim[ up2v[NverLoc*pCurrCounter+j] ];
    for (k = 0; k < itmp3; ++k) {
      itmp2 = v2up[ v2upDim[ up2v[NverLoc*pCurrCounter+j] ] + k ];
      if ( ( itmp2 != pCurrCounter ) && ( primal2dualPointers[itmp2] != -1 ) ) {
	tmp2 = 0.0;
	for (l = 0; l < dim; ++l)
	  tmp2 +=
	    (dualVertices[dim*dualVerticesPointers[primal2dualPointers[itmp2]]+l] - dualVertex[l])
	    * (dualVertices[dim*dualVerticesPointers[primal2dualPointers[itmp2]]+l] - dualVertex[l]);
	if ( tmp2 < tmp1 ) { tmp1 = tmp2; itmp1 = itmp2; }
      }
    }
  }
  if ( tmp1 > tolerance ){
    for (l = 0; l < dim; ++l) dualVertices[ dim * (*dualVerticesCounter) + l ] = dualVertex[l];
    dualVerticesPointers[pCumCounter] = *dualVerticesCounter;
    ++(*dualVerticesCounter);
  } else {
    dualVerticesPointers[pCumCounter] = dualVerticesPointers[ primal2dualPointers[itmp1] ];
  }
}


bndV2dualFaces * bndV2dualFacesCreate( const int Nfc,
				       const int * const dualFacesIdx,
				       bndV2dualFaces * const next )
{
  bndV2dualFaces * newEle = (bndV2dualFaces *) malloc (sizeof(bndV2dualFaces));
  if ( newEle == NULL ) {
    printf("File %s, line %i: error while creating list of dual boundary faces "
	   "associated with primal boundary vertices.\n", __FILE__, __LINE__);
    exit(1);
  }
  newEle->Nfc = Nfc;
  if ( ( Nfc != 0 ) && ( dualFacesIdx != NULL ) ) {
    newEle->dualFacesIdx = (int *) malloc ( Nfc * sizeof(int) );
    memcpy( newEle->dualFacesIdx, dualFacesIdx, Nfc * sizeof(int) );
  }
  else {
    newEle->dualFacesIdx = NULL;
  }
  newEle->next = next;

  return newEle;
}


v2v * v2vCreate( const int neighbor,
		 const int dualEdge,
		 v2v * const next )
{
  v2v * newV2V = (v2v *) malloc (sizeof(v2v));
  if ( newV2V == NULL ) {
    printf("File %s, line %i: error while creating a new v2v object.\n", __FILE__, __LINE__);
    exit(1);
  }
  newV2V->neighbor = neighbor;
  newV2V->dualEdge = dualEdge;
  newV2V->next     = next;

  return newV2V;
}


dualEdge * dualEdgeCreate( const int vertex1Idx,
			   const int vertex2Idx,
			   dualEdge * const next )
{
  dualEdge * newDualEdge = (dualEdge *) malloc (sizeof(dualEdge));
  if ( newDualEdge == NULL ) {
    printf("File %s, line %i: error while creating a new dual edge.\n", __FILE__, __LINE__);
    exit(1);
  }
  newDualEdge->vertices[0] = vertex1Idx;
  newDualEdge->vertices[1] = vertex2Idx;
  newDualEdge->next        = next;

  return newDualEdge;
}


dualPolygon * dualPolygonCreate( const int Nver,
				 const int * const verticesIdx,
				 dualPolygon * const next )
{
  dualPolygon * newDualPolygon = (dualPolygon *) malloc (sizeof(dualPolygon));
  if ( newDualPolygon == NULL ) {
    printf("File %s, line %i: error while creating a new dual polygon.\n", __FILE__, __LINE__);
    exit(1);
  }
  newDualPolygon->Nver = Nver;
  if ( ( Nver != 0 ) && ( verticesIdx != NULL ) ) {
    newDualPolygon->verticesIdx = (int *) malloc ( Nver * sizeof(int) );
    memcpy( newDualPolygon->verticesIdx, verticesIdx, Nver * sizeof(int) );
  }
  else {
    newDualPolygon->verticesIdx = NULL;
  }
  newDualPolygon->next = next;

  return newDualPolygon;
}


dualFace * dualFaceCreate( const int Ned,
			   const int * const hDualEdges,
			   dualFace * const next )
{
  dualFace * newDualFace = (dualFace *) malloc (sizeof(dualFace));
  if ( newDualFace == NULL ) {
    printf("File %s, line %i: error while creating a new dual face.\n", __FILE__, __LINE__);
    exit(1);
  }
  newDualFace->Ned = Ned;
  if ( ( Ned != 0 ) && ( hDualEdges != NULL ) ) {
    newDualFace->hDualEdges = (int *) malloc ( Ned * sizeof(int) );
    memcpy( newDualFace->hDualEdges, hDualEdges, Ned * sizeof(int) );
  }
  else {
    newDualFace->hDualEdges = NULL;
  }
  newDualFace->next = next;

  return newDualFace;
}


dualPolyhedron * dualPolyhedronCreate( const int Nfc,
				       const int * const hDualFaces,
				       dualPolyhedron * const next )
{
  dualPolyhedron * newDualPolyhedron = (dualPolyhedron *) malloc (sizeof(dualPolyhedron));
  if ( newDualPolyhedron == NULL ) {
    printf("File %s, line %i: error while creating a new dual element.\n", __FILE__, __LINE__);
    exit(1);
  }
  newDualPolyhedron->Nfc = Nfc;
  if ( ( Nfc != 0 ) && ( hDualFaces != NULL ) ) {
    newDualPolyhedron->hDualFaces = (int *) malloc ( Nfc * sizeof(int) );
    memcpy( newDualPolyhedron->hDualFaces, hDualFaces, Nfc * sizeof(int) );
  }
  else {
    newDualPolyhedron->hDualFaces = NULL;
  }
  newDualPolyhedron->next = next;

  return newDualPolyhedron;
}


intNode * intNodeCreate( const int value,
			 intNode * const next )
{
  intNode * newNode = (intNode *) malloc (sizeof(intNode));
  if ( newNode == NULL ) {
    printf("File %s, line %i: error while creating a new intNode object.\n", __FILE__, __LINE__);
    exit(1);
  }
  newNode->value = value;
  newNode->next  = next;

  return newNode;
}


void dualMesh2dInitialize( dualMesh2d * dMesh2d )
{
  dMesh2d->Nver        = 0;
  dMesh2d->vertices    = NULL;
  dMesh2d->vBndMarkers = NULL;
  dMesh2d->v2p         = NULL;
  dMesh2d->v2pDim      = NULL;

  dMesh2d->Ned         = 0;
  dMesh2d->NedDir      = 0;
  dMesh2d->NedNeu      = 0;
  dMesh2d->edges       = NULL;
  dMesh2d->eBndMarkers = NULL;
  dMesh2d->dirEdges    = NULL;
  dMesh2d->neuEdges    = NULL;
  dMesh2d->e2p         = NULL;

  dMesh2d->Npol        = 0;
  dMesh2d->p2v         = NULL;
  dMesh2d->p2vDim      = NULL;
  dMesh2d->p2e         = NULL;
  dMesh2d->p2eDim      = NULL;
}


void dualMeshInitialize( dualMesh * dMesh )
{
  dMesh->Nver        = 0;
  dMesh->vertices    = NULL;
  dMesh->vBndMarkers = NULL;
  dMesh->v2f         = NULL;
  dMesh->v2fDim      = NULL;
  dMesh->v2p         = NULL;
  dMesh->v2pDim      = NULL;

  dMesh->Ned         = 0;
  dMesh->edges       = NULL;
  dMesh->eBndMarkers = NULL;

  dMesh->Nfc         = 0;
  dMesh->f2he        = NULL;
  dMesh->f2heDim     = NULL;
  dMesh->f2p         = NULL;
  dMesh->fBndMarkers = NULL;

  dMesh->Npol        = 0;
  dMesh->p2hf        = NULL;
  dMesh->p2hfDim     = NULL;
}


void dualEdgeInsert( const int dualVertex1,
		     const int dualVertex2,
		     const int * const dualVerticesPointers,
		     v2v ** const v2vPointers,
		     int * hDualEdge,
		     dualEdge ** firstDualEdgeAddr,
		     dualEdge ** currentDualEdgeAddr,
		     int * dualEdgesCounterAddr,
		     int * f2heSizeAddr,
		     int * counterLocalEdgesAddr )
{
  int sortedDualEdge[2];
  int hDualEdgeFlag;
  int mappedVertex1 = dualVerticesPointers[ dualVertex1 ];
  int mappedVertex2 = dualVerticesPointers[ dualVertex2 ];
  v2v *v2vCursor    = NULL;

  /*
   * <<< EVEN >>> half edges (starting from 0) correspond to topological edges, so
   * they <<< DO NOT REQUIRE >>> inversion of topological edge vertices.
   *
   * Conversely, <<< ODD >>> half edges (starting from 1) <<< DO REQUIRE >>> inversion
   * of topological edge vertices.
   */

  if ( mappedVertex1 != mappedVertex2 ) {
    if ( mappedVertex1 < mappedVertex2 ) {
      sortedDualEdge[0] = mappedVertex1;
      sortedDualEdge[1] = mappedVertex2;
      hDualEdgeFlag     = 0;
    } else {
      sortedDualEdge[0] = mappedVertex2;
      sortedDualEdge[1] = mappedVertex1;
      hDualEdgeFlag     = 1;
    }

    v2vCursor = v2vPointers[ sortedDualEdge[0] ];
    if ( v2vCursor == NULL ) {
      /* Create a new dual edge */
      if ( *firstDualEdgeAddr == NULL ) {
	/* The list of dual edges is empty */
	*firstDualEdgeAddr = dualEdgeCreate( sortedDualEdge[0],
					     sortedDualEdge[1],
					     NULL );
	*currentDualEdgeAddr = *firstDualEdgeAddr;
      } else {
	(*currentDualEdgeAddr)->next = dualEdgeCreate( sortedDualEdge[0],
						       sortedDualEdge[1],
						       NULL );
	(*currentDualEdgeAddr) = (*currentDualEdgeAddr)->next;
      }

      *hDualEdge = 2 * (*dualEdgesCounterAddr) + hDualEdgeFlag;
      v2vPointers[sortedDualEdge[0]] = v2vCreate( sortedDualEdge[1], *dualEdgesCounterAddr, NULL );
      ++(*dualEdgesCounterAddr);
    } else {
      while ( ( v2vCursor->neighbor != sortedDualEdge[1] ) && ( v2vCursor->next != NULL ) ) {
	v2vCursor = v2vCursor->next;
      }
      if ( v2vCursor->neighbor == sortedDualEdge[1] ) {
	/* sortedDualEdge has already been inserted */
	*hDualEdge = 2*(v2vCursor->dualEdge) + hDualEdgeFlag;
      } else {
	/* Create a new dual edge. If we get here, it means that the dual edges list
	 * cannot be empty, so we do not need to check firstDualEdge == NULL */
	(*currentDualEdgeAddr)->next = dualEdgeCreate( sortedDualEdge[0],
						       sortedDualEdge[1],
						       NULL );
	*currentDualEdgeAddr = (*currentDualEdgeAddr)->next;
	*hDualEdge = 2 * (*dualEdgesCounterAddr) + hDualEdgeFlag;
	v2vCursor->next = v2vCreate( sortedDualEdge[1], *dualEdgesCounterAddr, NULL );
	++(*dualEdgesCounterAddr);
      }
    }

    ++(*f2heSizeAddr);
    ++(*counterLocalEdgesAddr);
  }

}


void dualMesh2dCreate( const primalMesh2d * const pMesh2d,
		       const double tolCollapsePt,
		       dualMesh2d * dMesh2d )
{
  int i, j, NverLoc, idx1, idx2, counter1, counter2, itmp1, currentEdge;
  int numDualVertices     = 0;   /* Preliminary number of dual vertices */
  int dualVerticesCounter = 0;
  int dualElementCounter  = 0;

#ifdef DEBUG
  int oldDualVerticesCounter = 0;
#endif

  int *dualElement          = NULL;
  int *itmpV                = NULL;
  int *dualVerticesPointers = NULL;
  int *tri2dualVertices     = NULL;
  int *edge2dualVertices    = NULL;
  int *vertex2dualVertices  = NULL;

  int bndEdges[2];

  double tmp1, tmp2, tolCollapsePtSq;
  double dualVertex[2], polygonCenter[2];

  double *dualVertices = NULL;

  dualPolygon *firstPolygon   = NULL;
  dualPolygon *currentPolygon = NULL;
  dualPolygon *tmpPolygon     = NULL;

  numDualVertices = pMesh2d->Nelt;
  for (i = 0; i < pMesh2d->Ned; ++i)  if ( pMesh2d->eTopFlags[i] == 1 ) ++numDualVertices;
  for (i = 0; i < pMesh2d->Nver; ++i) if ( pMesh2d->vTopFlags[i] == 2 ) ++numDualVertices;

  dualVertices = (double *) malloc ( 2 * numDualVertices * sizeof(double) );
  dualVerticesPointers = (int *) malloc ( numDualVertices * sizeof(int) );
  memset( dualVerticesPointers, -1, numDualVertices * sizeof(int) );

  tolCollapsePtSq = tolCollapsePt * tolCollapsePt;

  /*
   * Step 1
   *
   * Create a dual vertex at a central point in each primal mesh triangle.
   */

  tri2dualVertices = (int *) malloc ( (pMesh2d->Nelt) * sizeof(int) );
  memset( tri2dualVertices, -1, (pMesh2d->Nelt) * sizeof(int) );

  counter1 = 0;
  for (i = 0; i < pMesh2d->Nelt; ++i) {
    tri2dualVertices[i] = i;

    tricircumcenter( pMesh2d->vertices + 2*( pMesh2d->elements[3*i]   ),
		     pMesh2d->vertices + 2*( pMesh2d->elements[3*i+1] ),
		     pMesh2d->vertices + 2*( pMesh2d->elements[3*i+2] ),
		     dualVertex, &tmp1, &tmp2 );

    if ( (tmp1 >= 0.0) && (tmp2 >= 0.0)
	 && ( (tmp1 + tmp2) <= 1.0+tolCollapsePt ) ) {
      ++counter1;
      dualVertex[0] += pMesh2d->vertices[ 2*(pMesh2d->elements[3*i])   ];
      dualVertex[1] += pMesh2d->vertices[ 2*(pMesh2d->elements[3*i])+1 ];
    } else {
      dualVertex[0] =
	( pMesh2d->vertices[ 2*(pMesh2d->elements[3*i]) ]
	  + pMesh2d->vertices[ 2*(pMesh2d->elements[3*i+1]) ]
	  + pMesh2d->vertices[ 2*(pMesh2d->elements[3*i+2]) ] ) / 3.0;
      dualVertex[1] =
	( pMesh2d->vertices[ 2*(pMesh2d->elements[3*i])+1 ]
	  + pMesh2d->vertices[ 2*(pMesh2d->elements[3*i+1])+1 ]
	  + pMesh2d->vertices[ 2*(pMesh2d->elements[3*i+2])+1 ] ) / 3.0;
    }

    /* Insert circumcenters only if they are not closer than given tolerance.
     * Check only circumcenters of neighboring tetrahedra. */

    pdistNewDualVertices( 2, i, i, pMesh2d->elements, 3, pMesh2d->v2t, pMesh2d->v2tDim,
			  tri2dualVertices, tolCollapsePtSq, dualVertex,
			  dualVertices, dualVerticesPointers, &dualVerticesCounter );
  }
  printf("\n%i out of %i triangles are well-centered\n", counter1, pMesh2d->Nelt);

#ifdef DEBUG
  printf("\nDual vertices from Step 1: %i\n", dualVerticesCounter);
  for (i = 0; i < dualVerticesCounter; ++i)
    printf("   %.16f   %.16f\n", dualVertices[2*i], dualVertices[2*i+1]);
  oldDualVerticesCounter = dualVerticesCounter;
#endif

  /*
   * Step 2
   *
   * Create a dual vertex at mid-point of each primal mesh edge classified
   * on a model edge.
   */

  edge2dualVertices = (int *) malloc ( (pMesh2d->Ned) * sizeof(int) );
  memset( edge2dualVertices, -1, (pMesh2d->Ned) * sizeof(int) );

  counter1 = pMesh2d->Nelt;
  for (i = 0; i < pMesh2d->Ned; ++i) {
    if ( pMesh2d->eTopFlags[i] == 1 ) {
      edge2dualVertices[i] = counter1;

      dualVertex[0] = ( pMesh2d->vertices[ 2 * ( pMesh2d->edges[2*i] ) ]
			+ pMesh2d->vertices[ 2 * ( pMesh2d->edges[2*i+1] ) ] ) / 2.0;
      dualVertex[1] = ( pMesh2d->vertices[ 2 * ( pMesh2d->edges[2*i] ) + 1 ]
			+ pMesh2d->vertices[ 2 * ( pMesh2d->edges[2*i+1] ) + 1 ] ) / 2.0;

      /* Check neighboring triangles */
      tmp1 = pdistOldDualVertices( 2, i, counter1, pMesh2d->edges, 2,
				   pMesh2d->v2t, pMesh2d->v2tDim,
				   tri2dualVertices, tolCollapsePtSq, dualVertex,
				   dualVertices, dualVerticesPointers );

      /* Now check edges */
      pdistNewDualVertices( 2, i, counter1, pMesh2d->edges, 2, pMesh2d->v2e, pMesh2d->v2eDim,
			    edge2dualVertices, tolCollapsePtSq, dualVertex,
			    dualVertices, dualVerticesPointers, &dualVerticesCounter );

      ++counter1;
    }
  }

  /*
   * Step 3
   *
   * Create a dual vertex at each primal mesh vertex classified on a model
   * vertex.
   */

  vertex2dualVertices = (int *) malloc ( (pMesh2d->Nver) * sizeof(int) );
  memset( vertex2dualVertices, -1, (pMesh2d->Nver) * sizeof(int) );

  for (i = 0; i < pMesh2d->Nver; ++i) {
    if ( pMesh2d->vTopFlags[i] == 2 ) {
      vertex2dualVertices[i] = counter1;

      /* Check neighboring triangles */
      tmp1 = pdistOldDualVertices( 2, i, counter1, NULL, 1, pMesh2d->v2t, pMesh2d->v2tDim,
				   tri2dualVertices, tolCollapsePtSq, pMesh2d->vertices + 2*i,
				   dualVertices, dualVerticesPointers );

      /* Check neighboring edges */
      tmp2 = pdistOldDualVertices( 2, i, counter1, NULL, 1, pMesh2d->v2e, pMesh2d->v2eDim,
				   edge2dualVertices, tolCollapsePtSq, pMesh2d->vertices + 2*i,
				   dualVertices, dualVerticesPointers );
      tmp1 = tmp1 > tmp2 ? tmp2 : tmp1;

      if ( tmp1 > tolCollapsePtSq ) {
	dualVertices[ 2*dualVerticesCounter   ] = pMesh2d->vertices[ 2*i   ];
	dualVertices[ 2*dualVerticesCounter+1 ] = pMesh2d->vertices[ 2*i+1 ];
	dualVerticesPointers[counter1] = dualVerticesCounter;
	++dualVerticesCounter;
      }

      ++counter1;
    }
  }

  dMesh2d->Nver = dualVerticesCounter;
  dMesh2d->vertices = (double *) malloc (2 * (dMesh2d->Nver) * sizeof(double));
  memcpy( dMesh2d->vertices, dualVertices, 2 * (dMesh2d->Nver) * sizeof(double));
  free( dualVertices );

  /*
   * Create dual polygons
   */

  counter2 = 0;
  for (i = 0; i < pMesh2d->Nver; ++i) {

    counter1    = 0;
    NverLoc     = pMesh2d->v2eDim[i+1] - pMesh2d->v2eDim[i] + pMesh2d->vTopFlags[i];
    dualElement = (int *) malloc (NverLoc * sizeof(int));
    memset( dualElement, -1, NverLoc * sizeof(int) );

    if ( pMesh2d->vTopFlags[i] == 0 ) {

      currentEdge = pMesh2d->v2e[ pMesh2d->v2eDim[i] ];
      idx1        = pMesh2d->e2t[ 2*currentEdge ];
      idx2        = pMesh2d->e2t[ 2*currentEdge+1 ];

      /* Add edge if it does not collapse */
      if ( dualVerticesPointers[idx1] != dualVerticesPointers[idx2] ) {
	dualElement[0] = dualVerticesPointers[idx1];
	++counter1;
      }

      idx1 = idx2;
      for (j = 1; j < NverLoc; ++j) {
	intersectSorted( pMesh2d->t2e + 3*idx1, 3, pMesh2d->v2e + pMesh2d->v2eDim[i],
			 pMesh2d->v2eDim[i+1] - pMesh2d->v2eDim[i],
			 &itmpV, &itmp1 );
	if ( itmp1 != 2 ) {
	  printf("File %s line %i: error while creating a dual polygon "
		 "from primal vertex %i. idx1: %i, j: %i, NverLoc: %i, itmp1: %i, "
		 "aborting ...\n", __FILE__, __LINE__, i, idx1, j, NverLoc, itmp1);
	  exit(1);
	}
	else {
	  currentEdge = currentEdge == itmpV[0] ? itmpV[1] : itmpV[0];
	  free( itmpV );
	  if ( pMesh2d->e2t[ 2*currentEdge ] == idx1 )
	    idx2 = pMesh2d->e2t[ 2*currentEdge+1 ];
	  else
	    idx2 = pMesh2d->e2t[ 2*currentEdge ];
	}

	/* Add edge if it does not collapse */
	if ( dualVerticesPointers[idx1] != dualVerticesPointers[idx2] ) {
	  dualElement[j] = dualVerticesPointers[idx1];
	  ++counter1;
	}

	idx1 = idx2;
      }

    } else {

      bndEdges[0] = -1;
      bndEdges[1] = -1;
      itmp1       = 0;
      for (j = 0; j < (pMesh2d->v2eDim[i+1] - pMesh2d->v2eDim[i]); ++j)
	if ( pMesh2d->eTopFlags[ pMesh2d->v2e[ pMesh2d->v2eDim[i] + j ] ] == 1 ) {
	  if ( itmp1 == 2 ) { /* Remember itmp1 starts from 0 */
	    printf("File %s line %i: found 3 boundary edges attached to primal vertex %i "
		   "rather than 2, aborting ...\n", __FILE__, __LINE__, i);
	    exit(1);
	  }
	  bndEdges[itmp1++] = pMesh2d->v2e[ pMesh2d->v2eDim[i] + j ];
	}

      if ( ( bndEdges[0] == -1 ) || ( bndEdges[1] == -1 ) ) {
	printf("File %s line %i: found less than 2 boundary edges attached to primal vertex %i, "
	       "aborting ...\n", __FILE__, __LINE__, i);
	exit(1);
      }

      currentEdge = bndEdges[0];
      idx1        = edge2dualVertices[ currentEdge ];
      idx2        = pMesh2d->e2t[ 2*currentEdge ];

      /* Add edge if it does not collapse */
      if ( dualVerticesPointers[idx1] != dualVerticesPointers[idx2] ) {
	dualElement[0] = dualVerticesPointers[idx1];
	++counter1;
      }

      idx1 = idx2;
      for (j = 1; j < (pMesh2d->v2eDim[i+1] - pMesh2d->v2eDim[i] - 1); ++j) {
	intersectSorted( pMesh2d->t2e + 3*idx1, 3, pMesh2d->v2e + pMesh2d->v2eDim[i],
			 pMesh2d->v2eDim[i+1] - pMesh2d->v2eDim[i],
			 &itmpV, &itmp1 );
	if ( itmp1 != 2 ) {
	  printf("File %s line %i: error while creating a dual polygon "
		 "from primal vertex %i. idx1: %i, j: %i, NverLoc: %i, itmp1: %i, "
		 "aborting ...\n", __FILE__, __LINE__, i, idx1, j, NverLoc, itmp1);
	  exit(1);
	}
	else {
	  currentEdge = currentEdge == itmpV[0] ? itmpV[1] : itmpV[0];
	  free( itmpV );
	  if ( pMesh2d->e2t[ 2*currentEdge ] == idx1 )
	    idx2 = pMesh2d->e2t[ 2*currentEdge+1 ];
	  else
	    idx2 = pMesh2d->e2t[ 2*currentEdge ];
	}

	/* Add edge if it does not collapse */
	if ( dualVerticesPointers[idx1] != dualVerticesPointers[idx2] ) {
	  dualElement[j] = dualVerticesPointers[idx1];
	  ++counter1;
	}

	idx1 = idx2;
      }

      /* At this point we reached the other boundary edge */
      idx2 = edge2dualVertices[ bndEdges[1] ];

      itmp1 = pMesh2d->v2eDim[i+1] - pMesh2d->v2eDim[i] - 1;

      /* Add edge if it does not collapse */
      if ( dualVerticesPointers[idx1] != dualVerticesPointers[idx2] ) {
	dualElement[itmp1] = dualVerticesPointers[idx1];
	++counter1;
      }
      idx1 = idx2;

      if ( pMesh2d->vTopFlags[i] == 1 ) {
	idx2 = edge2dualVertices[ bndEdges[0] ];
	if ( dualVerticesPointers[idx1] != dualVerticesPointers[idx2] ) {
	  dualElement[itmp1+1] = dualVerticesPointers[idx1];
	  ++counter1;
	}
      } else {
	idx2 = vertex2dualVertices[i];
	if ( dualVerticesPointers[idx1] != dualVerticesPointers[idx2] ) {
	  dualElement[itmp1+1] = dualVerticesPointers[idx1];
	  ++counter1;
	}
	idx1 = idx2;
	idx2 = edge2dualVertices[ bndEdges[0] ];
	if ( dualVerticesPointers[idx1] != dualVerticesPointers[idx2] ) {
	  dualElement[itmp1+2] = dualVerticesPointers[idx1];
	  ++counter1;
	}
      }
    }

    if ( counter1 > 2 ) {
      /* Create dual polygon */
      itmp1 = 0;
      itmpV = (int *) malloc ( counter1 * sizeof(int) );
      for (j = 0; j < NverLoc; ++j)
	if ( dualElement[j] != -1 ) itmpV[itmp1++] = dualElement[j];

      polygonCenter[0] = 0.0;
      polygonCenter[1] = 0.0;
      for (j = 0; j < counter1; ++j) {
	polygonCenter[0] += dMesh2d->vertices[2*itmpV[j]];
	polygonCenter[1] += dMesh2d->vertices[2*itmpV[j]+1];
      }
      polygonCenter[0] /= (double)(counter1);
      polygonCenter[1] /= (double)(counter1);

      tmp1 = 0.0;
      for (j = 0; j < counter1; ++j) {
	tmp1 += orient2d( polygonCenter,
			  dMesh2d->vertices + 2*itmpV[j],
			  dMesh2d->vertices + 2*itmpV[(j+1)%counter1] );
      }

      free( dualElement );
      dualElement = (int *) malloc (counter1 * sizeof(int));
      if ( tmp1 > 0.0 ) {
	for (j = 0; j < counter1; ++j) dualElement[j] = itmpV[j];
      } else {
	for (j = 0; j < counter1; ++j) dualElement[j] = itmpV[counter1-1-j];
      }

      if ( firstPolygon == NULL ) {
	firstPolygon   = dualPolygonCreate( counter1, dualElement, NULL );
	currentPolygon = firstPolygon;
      } else {
	currentPolygon->next = dualPolygonCreate( counter1, dualElement, NULL );
	currentPolygon       = currentPolygon->next;
      }
      ++dualElementCounter;
      counter2 += counter1;
      free( itmpV );
      free( dualElement );
    } else {
      printf("Found a degenerate polygon\n");
      free( dualElement );
    }
  }

  dMesh2d->Npol      = dualElementCounter;
  dMesh2d->p2v       = (int *) malloc ( counter2 * sizeof(int) );
  dMesh2d->p2vDim    = (int *) malloc ( (dMesh2d->Npol+1) * sizeof(int) );
  dMesh2d->p2vDim[0] = 0;

  currentPolygon = firstPolygon;
  for (i = 0; i < dMesh2d->Npol; ++i) {
    tmpPolygon = currentPolygon->next;
    memcpy( dMesh2d->p2v + dMesh2d->p2vDim[i], currentPolygon->verticesIdx,
	    ( currentPolygon->Nver ) * sizeof(int) );
    dMesh2d->p2vDim[i+1] = dMesh2d->p2vDim[i] + currentPolygon->Nver;
    free( currentPolygon->verticesIdx );
    free( currentPolygon );
    currentPolygon = tmpPolygon;
  }

  free( tri2dualVertices );
  free( edge2dualVertices );
  free( vertex2dualVertices );
  free( dualVerticesPointers );
}


void dualMeshCreate( const primalMesh * const pMesh,
		     const double tolCollapsePt,
		     dualMesh * dMesh )
{
  int i, j, k, f, counter, counter2;
  int numDualVertices = 0;   /* Preliminary number of dual vertices */
  int numDualFaces    = 0;   /* Preliminary number of dual faces */
  int dualVerticesCounter = 0;
  int dualEdgesCounter    = 0;
  int dualFacesCounter    = 0;
  int f2heSize            = 0;

  int itmp1, itmp2, currFace, currEdge, dualFaceMaxSize, numDualFaces2Add;
  int dualVertex1Idx, dualVertex2Idx;
  int hEdge, tEdge;

#ifdef DEBUG
  int oldDualVerticesCounter = 0;
#endif

  int * dualVerticesPointers = NULL;
  int * tetra2dualVertices   = NULL;
  int * face2dualVertices    = NULL;
  int * edge2dualVertices    = NULL;
  int * edge2dualFaces       = NULL;
  int * vertex2dualVertices  = NULL;

  int *bndFaces = NULL, *hDualEdges = NULL, *hDualFaces = NULL, *itmpV = NULL, *itmpV2 = NULL;
  int *v2dualFace = NULL, *connectedEdges = NULL;
  int **polyhedron = NULL;

  double tmp1, tmp2, tmp3, tolCollapsePtSq;
  double dualVertex[3], polyhedronCentroid[3];
  double *dualVertices = NULL, *facesCentroids = NULL;

  dualEdge *firstDualEdge = NULL, *currentDualEdge = NULL, *tmpE = NULL;
  dualFace *firstDualFace = NULL, *currentDualFace = NULL, *tmpF = NULL;
  dualPolyhedron *firstDualPolyhedron = NULL, *currentDualPolyhedron = NULL, *tmpP = NULL;

  v2v ** v2vPointers = NULL;
  v2v *v2vCursor = NULL, *v2vTmp = NULL;

  bndV2dualFaces *firstBndV2dualFaces   = NULL;
  bndV2dualFaces *currentBndV2dualFaces = NULL;
  bndV2dualFaces *tmpBndV2dualFaces     = NULL;

  bool exitInnerFor;

  numDualVertices = pMesh->Nelt;
  for (i = 0; i < pMesh->Nfc; ++i)  if ( pMesh->fTopFlags[i] == 1 )   ++numDualVertices;
  for (i = 0; i < pMesh->Ned; ++i)  if ( pMesh->eTopFlags[i] == 2 )   ++numDualVertices;
  for (i = 0; i < pMesh->Nver; ++i) if ( pMesh->vTopFlags[i] >= 300 ) ++numDualVertices;

  dualVertices = (double *) malloc ( 3 * numDualVertices * sizeof(double) );
  dualVerticesPointers = (int *) malloc ( numDualVertices * sizeof(int) );
  memset( dualVerticesPointers, -1, numDualVertices * sizeof(int) );

  tolCollapsePtSq = tolCollapsePt * tolCollapsePt;

  /*
   * Step 1
   * 
   * Create a dual vertex a a central point in each primal mesh tetrahedron
   */

  tetra2dualVertices = (int *) malloc ( (pMesh->Nelt) * sizeof(int) );
  memset( tetra2dualVertices, -1, (pMesh->Nelt) * sizeof(int) );

  counter2 = 0;   /* Count well-centered tetrahedra */

  for (i = 0; i < pMesh->Nelt; ++i) {
    tetra2dualVertices[i] = i;

    tetcircumcenter( pMesh->vertices + 3*( pMesh->elements[4*i]   ),
		     pMesh->vertices + 3*( pMesh->elements[4*i+1] ),
		     pMesh->vertices + 3*( pMesh->elements[4*i+2] ),
		     pMesh->vertices + 3*( pMesh->elements[4*i+3] ),
		     dualVertex, &tmp1, &tmp2, &tmp3 );

    if ( (tmp1 >= 0.0) && (tmp2 >= 0.0) && (tmp3 >= 0.0)
	 && ( (tmp1 + tmp2 + tmp3) <= 1.0+tolCollapsePt ) ) {
      ++counter2;
      dualVertex[0] += pMesh->vertices[ 3*(pMesh->elements[4*i])   ];
      dualVertex[1] += pMesh->vertices[ 3*(pMesh->elements[4*i])+1 ];
      dualVertex[2] += pMesh->vertices[ 3*(pMesh->elements[4*i])+2 ];
    } else {
      /* replaceTetCircumcenter( pMesh->vertices + 3*( pMesh->elements[4*i]   ), */
      /* 			      pMesh->vertices + 3*( pMesh->elements[4*i+1] ), */
      /* 			      pMesh->vertices + 3*( pMesh->elements[4*i+2] ), */
      /* 			      pMesh->vertices + 3*( pMesh->elements[4*i+3] ), */
      /* 			      tmp1, tmp2, tmp3, -tolCollapsePt, dualVertex ); */
      dualVertex[0] += pMesh->vertices[ 3*(pMesh->elements[4*i])   ];
      dualVertex[1] += pMesh->vertices[ 3*(pMesh->elements[4*i])+1 ];
      dualVertex[2] += pMesh->vertices[ 3*(pMesh->elements[4*i])+2 ];
    }

    /* Insert circumcenters only if they are not closer than given tolerance.
     * Check only circumcenters of neighboring tetrahedra. */

    pdistNewDualVertices( 3, i, i, pMesh->elements, 4, pMesh->v2t, pMesh->v2tDim,
			  tetra2dualVertices, tolCollapsePtSq, dualVertex,
			  dualVertices, dualVerticesPointers, &dualVerticesCounter );
  }
  printf("\n%i out of %i tetrahedra are well-centered\n", counter2, pMesh->Nelt);

#ifdef DEBUG
  printf("\nDual vertices from Step 1: %i\n", dualVerticesCounter);
  for (i = 0; i < dualVerticesCounter; ++i)
    printf("   %.16f   %.16f   %.16f\n",
	   dualVertices[3*i], dualVertices[3*i+1], dualVertices[3*i+2]);
  oldDualVerticesCounter = dualVerticesCounter;
#endif

  /*
   * End of Step 1
   */

  /*
   * Step 2
   *
   * Create a dual vertex at a central point in each primal mesh face
   * classified on the boundary. We use the face circumcenter even if it is not
   * contained by the face.
   */

  face2dualVertices = (int *) malloc ( (pMesh->Nfc) * sizeof(int) );
  memset( face2dualVertices, -1, (pMesh->Nfc) * sizeof(int) );

  counter2 = 0; /* Count well-centered boundary faces */
  counter         = pMesh->Nelt;
  for (i = 0; i < pMesh->Nfc; ++i) {
    if ( pMesh->fTopFlags[i] == 1 ) {
      face2dualVertices[i] = counter;
      
      tricircumcenter3d( pMesh->vertices + 3*( pMesh->faces[3*i]   ),
			 pMesh->vertices + 3*( pMesh->faces[3*i+1] ),
			 pMesh->vertices + 3*( pMesh->faces[3*i+2] ),
			 dualVertex, &tmp1, &tmp2 );

      if ( (tmp1 >= 0.0) && (tmp2 >= 0.0) && ( (tmp1 + tmp2) <= 1.0+tolCollapsePt ) ) {
	++counter2;
	dualVertex[0] += pMesh->vertices[ 3*(pMesh->faces[3*i])   ];
	dualVertex[1] += pMesh->vertices[ 3*(pMesh->faces[3*i])+1 ];
	dualVertex[2] += pMesh->vertices[ 3*(pMesh->faces[3*i])+2 ];
      } else {
	/* replaceTriCircumcenter( pMesh->vertices + 3*( pMesh->faces[3*i]   ), */
	/* 			pMesh->vertices + 3*( pMesh->faces[3*i+1] ), */
	/* 			pMesh->vertices + 3*( pMesh->faces[3*i+2] ), */
	/* 			tmp1, tmp2, dualVertex ); */
	dualVertex[0] += pMesh->vertices[ 3*(pMesh->faces[3*i])   ];
	dualVertex[1] += pMesh->vertices[ 3*(pMesh->faces[3*i])+1 ];
	dualVertex[2] += pMesh->vertices[ 3*(pMesh->faces[3*i])+2 ];
      }

      /* First check neighboring tetrahedra */
      tmp1 = pdistOldDualVertices( 3, i, counter, pMesh->faces, 3, pMesh->v2t, pMesh->v2tDim,
				   tetra2dualVertices, tolCollapsePtSq, dualVertex,
				   dualVertices, dualVerticesPointers );

      /* Now check faces */
      pdistNewDualVertices( 3, i, counter, pMesh->faces, 3, pMesh->v2f, pMesh->v2fDim,
			    face2dualVertices, tolCollapsePtSq, dualVertex,
			    dualVertices, dualVerticesPointers, &dualVerticesCounter );

      ++counter;
    }
  }
  printf("\n%i out of %i boundary faces are well-centered\n\n", counter2, counter-pMesh->Nelt);
  
#ifdef DEBUG
  printf("\nDual Vertices from Step 2: %i\n", dualVerticesCounter-oldDualVerticesCounter);
  for (i = oldDualVerticesCounter; i < dualVerticesCounter; ++i)
    printf("   %.16f   %.16f   %.16f\n",
	   dualVertices[3*i], dualVertices[3*i+1], dualVertices[3*i+2]);
  oldDualVerticesCounter = dualVerticesCounter;
#endif

  /*
   * End of Step 2
   */

  /*
   * Step 3
   *
   * Create a dual vertex at the mid-point of each primal mesh edge
   * classified on a model edge.
   */

  edge2dualVertices = (int *) malloc ( (pMesh->Ned) * sizeof(int) );
  memset( edge2dualVertices, -1, (pMesh->Ned) * sizeof(int) );

  for (i = 0; i < pMesh->Ned; ++i) {
    if ( pMesh->eTopFlags[i] == 2 ) {
      edge2dualVertices[i] = counter;

      dualVertex[0] = ( pMesh->vertices[ 3 * ( pMesh->edges[2*i] ) ]
			+ pMesh->vertices[ 3 * ( pMesh->edges[2*i+1] ) ] ) / 2.0;
      dualVertex[1] = ( pMesh->vertices[ 3 * ( pMesh->edges[2*i] ) + 1 ]
			+ pMesh->vertices[ 3 * ( pMesh->edges[2*i+1] ) + 1 ] ) / 2.0;
      dualVertex[2] = ( pMesh->vertices[ 3 * ( pMesh->edges[2*i] ) + 2 ]
			+ pMesh->vertices[ 3 * ( pMesh->edges[2*i+1] ) + 2 ] ) / 2.0;

      /* Check neighboring tetrahedra */
      tmp1 = pdistOldDualVertices( 3, i, counter, pMesh->edges, 2, pMesh->v2t, pMesh->v2tDim,
				   tetra2dualVertices, tolCollapsePtSq, dualVertex,
				   dualVertices, dualVerticesPointers );

      /* Check neighboring faces */
      tmp1 = pdistOldDualVertices( 3, i, counter, pMesh->edges, 2, pMesh->v2f, pMesh->v2fDim,
				   face2dualVertices, tolCollapsePtSq, dualVertex,
				   dualVertices, dualVerticesPointers );

      /* Now check edges */
      pdistNewDualVertices( 3, i, counter, pMesh->edges, 2, pMesh->v2e, pMesh->v2eDim,
			    edge2dualVertices, tolCollapsePtSq, dualVertex,
			    dualVertices, dualVerticesPointers, &dualVerticesCounter );

      ++counter;
    }
  }

#ifdef DEBUG
  printf("\nDual Vertices from Step 3: %i\n", dualVerticesCounter-oldDualVerticesCounter);
  for (i = oldDualVerticesCounter; i < dualVerticesCounter; ++i)
    printf("   %.16f   %.16f   %.16f\n",
	   dualVertices[3*i], dualVertices[3*i+1], dualVertices[3*i+2]);
  oldDualVerticesCounter = dualVerticesCounter;
#endif

  /*
   * End of Step 3
   */

  /*
   * Step 4
   *
   * Create a dual vertex at each primal mesh vertex classified
   * on a model vertex. Model vertices require a simpler processing.
   */

  vertex2dualVertices = (int *) malloc ( (pMesh->Nver) * sizeof(int) );
  memset( vertex2dualVertices, -1, (pMesh->Nver) * sizeof(int) );

  for (i = 0; i < pMesh->Nver; ++i) {
    if ( pMesh->vTopFlags[i] >= 300 ) {
      vertex2dualVertices[i] = counter;

      /* Check neighboring tetrahedra */
      tmp1 = pdistOldDualVertices( 3, i, counter, NULL, 1, pMesh->v2t, pMesh->v2tDim,
				   tetra2dualVertices, tolCollapsePtSq, pMesh->vertices + 3*i,
				   dualVertices, dualVerticesPointers );

      /* Check neighboring faces */
      tmp2 = pdistOldDualVertices( 3, i, counter, NULL, 1, pMesh->v2f, pMesh->v2fDim,
				   face2dualVertices, tolCollapsePtSq, pMesh->vertices + 3*i,
				   dualVertices, dualVerticesPointers );
      tmp1 = tmp1 > tmp2 ? tmp2 : tmp1;

      /* Check neighboring edges */
      tmp2 = pdistOldDualVertices( 3, i, counter, NULL, 1, pMesh->v2e, pMesh->v2eDim,
				   edge2dualVertices, tolCollapsePtSq, pMesh->vertices + 3*i,
				   dualVertices, dualVerticesPointers );
      tmp1 = tmp1 > tmp2 ? tmp2 : tmp1;

      if ( tmp1 > tolCollapsePtSq ) {
	dualVertices[ 3*dualVerticesCounter   ] = pMesh->vertices[ 3*i   ];
	dualVertices[ 3*dualVerticesCounter+1 ] = pMesh->vertices[ 3*i+1 ];
	dualVertices[ 3*dualVerticesCounter+2 ] = pMesh->vertices[ 3*i+2 ];
	dualVerticesPointers[counter] = dualVerticesCounter;
	++dualVerticesCounter;
      }

      ++counter;
    }
  }

#ifdef DEBUG
  printf("\nDual Vertices from Step 4: %i\n", dualVerticesCounter-oldDualVerticesCounter);
  for (i = oldDualVerticesCounter; i < dualVerticesCounter; ++i)
    printf("   %.16f   %.16f   %.16f\n",
	   dualVertices[3*i], dualVertices[3*i+1], dualVertices[3*i+2]);
#endif

  free( tetra2dualVertices );

  /*
   * End of Step 4
   */

  dMesh->Nver = dualVerticesCounter;
  dMesh->vertices = (double *) malloc (3 * (dMesh->Nver) * sizeof(double));
  memcpy( dMesh->vertices, dualVertices, 3 * (dMesh->Nver) * sizeof(double));
  free( dualVertices );

  /*
   * Create dual faces
   */

  numDualFaces = pMesh->Ned; /* REMARK: interior boundaries are not supported */
  for (i = 0; i < pMesh->Nver; ++i)
    numDualFaces += pMesh->vTopFlags[i] - ( pMesh->vTopFlags[i] / 100 ) * 100;

  /*
   * Steps 5 and 6
   *
   * Create dual faces corresponding to primal edges
   */

  edge2dualFaces = (int *) malloc ( (pMesh->Ned) * sizeof(int) );
  memset( edge2dualFaces, -1, (pMesh->Ned) * sizeof(int) );

  /* currentDualEdge = firstDualEdge; */
  /* currentDualFace = firstDualFace; */

  dualEdgesCounter = 0;
  dualFacesCounter = 0;
  f2heSize         = 0;
  v2vPointers      = (v2v **) malloc ( (dMesh->Nver) * sizeof(v2v *) );
  for (i = 0; i < dMesh->Nver; ++i) v2vPointers[i] = NULL;

  for (i = 0; i < pMesh->Ned; ++i) {
    if ( pMesh->eTopFlags[i] == 0 ) {
      /* Create a dual face from an interior primal edge */

      itmp2      = pMesh->e2tDim[i+1] - pMesh->e2tDim[i]; /* Potential number of half edges */
      hDualEdges = (int *) malloc ( itmp2 * sizeof(int) );
      memset( hDualEdges, -1, itmp2 * sizeof(int) );
      
      currFace       = -1;
      dualVertex1Idx = pMesh->e2t[ pMesh->e2tDim[i] ];
      dualVertex2Idx = -1;

      counter = 0; /* Count the number of half edges actually inserted locally */
      
      for (j = 0; j < itmp2; ++j) {

	intersectSorted( pMesh->t2f + 4*dualVertex1Idx, 4,
			 pMesh->e2f + pMesh->e2fDim[i], pMesh->e2fDim[i+1]-pMesh->e2fDim[i],
			 &itmpV, &itmp1 );
	if ( itmp1 != 2 ) {
	  printf("File %s line %i: encountered error while creating a dual face "
		 "from primal edge %i. dualVertex1Idx: %i, j: %i, itmp2: %i, itmp1: %i, "
		 "aborting ...\n", __FILE__, __LINE__, i, dualVertex1Idx, j, itmp2, itmp1);
	  exit(1);
	}
	else {
	  currFace = currFace == itmpV[0] ? itmpV[1] : itmpV[0];
	  free( itmpV );
	}

	/* Move to the neighboring tetrahedron */
	dualVertex2Idx = pMesh->f2t[2*currFace] == dualVertex1Idx ?
	  pMesh->f2t[2*currFace+1] : pMesh->f2t[2*currFace];

	dualEdgeInsert( dualVertex1Idx, dualVertex2Idx, dualVerticesPointers,
			v2vPointers, hDualEdges + j, &firstDualEdge, &currentDualEdge,
			&dualEdgesCounter, &f2heSize, &counter );

	dualVertex1Idx = dualVertex2Idx;
      }
      
      /* Check that we returned back to the starting tetrahedron */
      if ( pMesh->e2t[ pMesh->e2tDim[i] ] != dualVertex2Idx ) {
	printf("File %s line %i: We did not come back to the starting tetrahedron, "
	       "aborting ...\n", __FILE__, __LINE__);
	exit(1);
      }

    } else if ( pMesh->eTopFlags[i] == 1 ) {
      /* Create a face from a primal edge on the interior of a model face */

      itmp2 = pMesh->e2tDim[i+1] - pMesh->e2tDim[i] + 2;
      hDualEdges = (int *) malloc ( itmp2 * sizeof(int) );
      memset( hDualEdges, -1, itmp2 * sizeof(int) );

      /* Find boundary faces in the primal mesh connected to the edge */

      bndFaces = (int *) malloc (2 * sizeof(int));
      bndFaces[0] = -1;
      bndFaces[1] = -1;
      itmp1    = 0;
      for (j = 0; j < (pMesh->e2fDim[i+1] - pMesh->e2fDim[i]); ++j)
	if ( pMesh->f2t[2 * ( pMesh->e2f[ pMesh->e2fDim[i] + j ] ) + 1] == -1 ) {
	  if ( itmp1 == 2 ) { /* Remember itmp1 starts from 0 */
	    printf("File %s line %i: found 3 boundary faces attached to primal edge %i "
		   "rather than 2, aborting ...\n", __FILE__, __LINE__, i);
	    exit(1);
	  }
	  bndFaces[itmp1++] = pMesh->e2f[ pMesh->e2fDim[i] + j ];
	}

      if ( ( bndFaces[0] == -1 ) || ( bndFaces[1] == -1 ) ) {
	printf("File %s line %i: found less than 2 boundary faces attached to primal edge %i, "
	       "aborting ...\n", __FILE__, __LINE__, i);
	exit(1);
      }
      currFace       = bndFaces[0];
      dualVertex1Idx = face2dualVertices[ bndFaces[0] ];
      counter        = 0; /* Count the number of half edges actually inserted locally */

      for (j = 0; j < itmp2; ++j) {

	/* Determine dualVertex2Idx */
	if (j == 0) {
	  dualVertex2Idx = pMesh->f2t[2 * bndFaces[0]];
	} else if ( j < (itmp2-2) ) {
	  intersectSorted( pMesh->t2f + 4*dualVertex1Idx, 4,
			   pMesh->e2f + pMesh->e2fDim[i], pMesh->e2fDim[i+1]-pMesh->e2fDim[i],
			   &itmpV, &itmp1 );
	  if ( itmp1 != 2 ) {
	    printf("File %s line %i: encountered error while creating a dual face "
		   "from primal edge %i, found %i faces, aborting ...\n",
		   __FILE__, __LINE__, i, itmp1);
	    exit(1);
	  }
	  else {
	    currFace = currFace == itmpV[0] ? itmpV[1] : itmpV[0];
	    free( itmpV );
	  }

	  /* Move to the neighboring tetrahedron */
	  dualVertex2Idx = pMesh->f2t[2*currFace] == dualVertex1Idx ?
	    pMesh->f2t[2*currFace+1] : pMesh->f2t[2*currFace];
	} else if ( j == (itmp2-2) ) {
	  dualVertex2Idx = face2dualVertices[ bndFaces[1] ];
	} else {
	  dualVertex2Idx = face2dualVertices[ bndFaces[0] ];
	}

	dualEdgeInsert( dualVertex1Idx, dualVertex2Idx, dualVerticesPointers,
			v2vPointers, hDualEdges + j, &firstDualEdge, &currentDualEdge,
			&dualEdgesCounter, &f2heSize, &counter );

	dualVertex1Idx = dualVertex2Idx;
      }

      free( bndFaces );
      
    } else {
      /* Create a face from a primal edge on a model edge */

      itmp2 = pMesh->e2tDim[i+1] - pMesh->e2tDim[i] + 3;
      hDualEdges = (int *) malloc ( itmp2 * sizeof(int) );
      memset( hDualEdges, -1, itmp2 * sizeof(int) );

      /* Find boundary faces in the primal mesh connected to the edge */
      bndFaces = (int *) malloc (2 * sizeof(int));
      itmp1    = 0;
      for (j = 0; j < (pMesh->e2fDim[i+1] - pMesh->e2fDim[i]); ++j)
	if ( pMesh->f2t[2 * ( pMesh->e2f[ pMesh->e2fDim[i] + j ] ) + 1] == -1 ) {
	  if ( itmp1 == 2 ) { /* Remember itmp1 starts from 0 */
	    printf("File %s line %i: primal edge %i should have 2 boundary faces attached to it, "
		   "found 3 instead, aborting ...\n", __FILE__, __LINE__, i);
	    exit(1);
	  }
	  bndFaces[itmp1++] = pMesh->e2f[ pMesh->e2fDim[i] + j ];
	}

      currFace       = bndFaces[0];
      dualVertex1Idx = edge2dualVertices[i];
      counter        = 0; /* Count the number of half edges actually inserted locally */

      for (j = 0; j < itmp2; ++j) {

	/* Determine dualVertex2Idx */
	if ( j == 0 ) {
	  dualVertex2Idx = face2dualVertices[ bndFaces[0] ];
	} else if ( j == 1 ) {
	  dualVertex2Idx = pMesh->f2t[2 * bndFaces[0]];
	} else if ( j < (itmp2-2) ) {
	  intersectSorted( pMesh->t2f + 4*dualVertex1Idx, 4,
			   pMesh->e2f + pMesh->e2fDim[i], pMesh->e2fDim[i+1]-pMesh->e2fDim[i],
			   &itmpV, &itmp1 );
	  if ( itmp1 != 2 ) {
	    printf("File %s line %i: encountered error while creating a dual face "
		   "from primal edge %i, found %i faces, aborting ...\n",
		   __FILE__, __LINE__, i, itmp1);
	    exit(1);
	  }
	  else {
	    currFace = currFace == itmpV[0] ? itmpV[1] : itmpV[0];
	    free( itmpV );
	  }

	  /* Move to the neighboring tetrahedron */
	  dualVertex2Idx = pMesh->f2t[2*currFace] == dualVertex1Idx ?
	    pMesh->f2t[2*currFace+1] : pMesh->f2t[2*currFace];
	} else if ( j == (itmp2-2) ) {
	  dualVertex2Idx = face2dualVertices[ bndFaces[1] ];
	} else {
	  dualVertex2Idx = edge2dualVertices[i];
	}

	dualEdgeInsert( dualVertex1Idx, dualVertex2Idx, dualVerticesPointers,
			v2vPointers, hDualEdges + j, &firstDualEdge, &currentDualEdge,
			&dualEdgesCounter, &f2heSize, &counter );

	dualVertex1Idx = dualVertex2Idx;
      }

      free( bndFaces );
    }

    if ( counter > 2 ) {
      /* Create dual face */
      itmp1  = 0;
      itmpV = (int *) malloc ( counter * sizeof(int) );
      for (j = 0; j < itmp2; ++j)
	if ( hDualEdges[j] != -1 ) itmpV[itmp1++] = hDualEdges[j];
      edge2dualFaces[i] = dualFacesCounter;
      if ( firstDualFace == NULL ) {
	/* The list of dual faces is empty */
	firstDualFace   = dualFaceCreate( counter, itmpV, NULL );
	currentDualFace = firstDualFace;
      } else {
	currentDualFace->next = dualFaceCreate( counter, itmpV, NULL );
	currentDualFace       = currentDualFace->next;
      }
      ++dualFacesCounter;
      free( itmpV );
    } else {
      printf("Found a degenerate face\n");
    }

    free( hDualEdges );
  }
  
  /*
   * End of Steps 5 and 6
   */

  /*
   * Step 7
   *
   * Create boundary dual faces corresponding to primal boundary vertices
   */
  
  for (i = 0; i < pMesh->Nver; ++i) {
    if ( ( pMesh->vTopFlags[i] >= 100 ) && ( pMesh->vTopFlags[i] < 200 ) ) {
      /* Creating a face from a primal vertex on a model face */
      
      /* First loop to count boundary faces */
      itmp2 = 0;
      for (j = 0; j < (pMesh->v2fDim[i+1] - pMesh->v2fDim[i]); ++j)
	if ( pMesh->f2t[2 * pMesh->v2f[ pMesh->v2fDim[i] + j ] + 1] == -1 ) ++itmp2;
      
      hDualEdges = (int *) malloc ( itmp2 * sizeof(int) );
      memset( hDualEdges, -1, itmp2 * sizeof(int) );
      
      bndFaces = (int *) malloc ( itmp2 * sizeof(int) );
      itmp1    = 0;
      for (j = 0; j < (pMesh->v2fDim[i+1] - pMesh->v2fDim[i]); ++j)
	if ( pMesh->f2t[2 * pMesh->v2f[ pMesh->v2fDim[i] + j ] + 1] == -1 )
	  bndFaces[itmp1++] = pMesh->v2f[ pMesh->v2fDim[i] + j ];

      currEdge       = -1;
      currFace       = bndFaces[0];
      dualVertex1Idx = face2dualVertices[ bndFaces[0] ];
      dualVertex2Idx = -1;
      counter        = 0; /* Count the number of half edges actually inserted locally */
      
      for (j = 0; j < itmp2; ++j) {

	intersectSorted( pMesh->f2e + 3*currFace, 3,
			 pMesh->v2e + pMesh->v2eDim[i], pMesh->v2eDim[i+1]-pMesh->v2eDim[i],
			 &itmpV, &itmp1 );
	if ( itmp1 != 2 ) {
	  printf("File %s line %i: encountered error while creating a dual face "
		 "from boundary primal vertex %i. "
		 "Expected 2 shared edges, found %i instead, "
		 "aborting ...\n", __FILE__, __LINE__, i, itmp1);
	  exit(1);
	}
	else {
	  currEdge = currEdge == itmpV[0] ? itmpV[1] : itmpV[0];
	  free( itmpV );
	}

	for (k = 0; k < (pMesh->e2fDim[currEdge+1] - pMesh->e2fDim[currEdge]); ++k)
	  if ( ( pMesh->f2t[ 2 * pMesh->e2f[ pMesh->e2fDim[currEdge] + k ] + 1 ] == -1 )
	       && ( pMesh->e2f[ pMesh->e2fDim[currEdge] + k ] != currFace ) ) {
	    currFace = pMesh->e2f[ pMesh->e2fDim[currEdge] + k ];
	    break;
	  }

	dualVertex2Idx = face2dualVertices[ currFace ];

	dualEdgeInsert( dualVertex1Idx, dualVertex2Idx, dualVerticesPointers,
			v2vPointers, hDualEdges + j, &firstDualEdge, &currentDualEdge,
			&dualEdgesCounter, &f2heSize, &counter );

	dualVertex1Idx = dualVertex2Idx;
      }

      /* Check that we returned back to the starting boundary face */
      if ( face2dualVertices[ bndFaces[0] ] != dualVertex2Idx ) {
	printf("File %s line %i: We did not come back to the starting boundary face, "
	       "aborting ...\n", __FILE__, __LINE__);
	exit(1);
      }

      if ( counter > 2 ) {
	/* Create dual face */
	itmp1 = 0;
	itmpV = (int *) malloc ( counter * sizeof(int) );
	for (j = 0; j < itmp2; ++j)
	  if ( hDualEdges[j] != -1 ) itmpV[itmp1++] = hDualEdges[j];
	if ( firstBndV2dualFaces == NULL ) {
	  firstBndV2dualFaces   = bndV2dualFacesCreate(1, &dualFacesCounter, NULL );
	  currentBndV2dualFaces = firstBndV2dualFaces;
	} else {
	  currentBndV2dualFaces->next = bndV2dualFacesCreate(1, &dualFacesCounter, NULL);
	  currentBndV2dualFaces       = currentBndV2dualFaces->next;
	}
	/* At this point, the list of dual faces is not empty for sure */
	currentDualFace->next = dualFaceCreate( counter, itmpV, NULL );
	currentDualFace       = currentDualFace->next;

	++dualFacesCounter;
	free( itmpV );
      } else {
	printf("Found a degenerate face in Step 7a\n");
	if ( firstBndV2dualFaces == NULL ) {
	  firstBndV2dualFaces   = bndV2dualFacesCreate(0, NULL, NULL );
	  currentBndV2dualFaces = firstBndV2dualFaces;
	} else {
	  currentBndV2dualFaces->next = bndV2dualFacesCreate(0, NULL, NULL);
	  currentBndV2dualFaces       = currentBndV2dualFaces->next;
	}
      }

      free( hDualEdges );
      free( bndFaces );
      
    } else if ( ( pMesh->vTopFlags[i] >= 200 ) && ( pMesh->vTopFlags[i] < 300 ) ) {
      /* Creating (possibly) two faces from a primal vertex on a model edge */

      if ( ( pMesh->vTopFlags[i] - 200 ) != 2 ) {
	printf("File %s line %i: found more than 2 model faces emanating from a vertex "
	       "classified on a model edge.\n"
	       "Make sure there are no internal boundary faces. "
	       "Aborting ...\n", __FILE__, __LINE__);
	exit(1);
      }

      v2dualFace = (int *) malloc ( 2 * sizeof(int) );
      memset( v2dualFace, -1, 2 * sizeof(int) );

      /* Find mesh edges classified on a model edge connected
	 to the current primal vertex */

      connectedEdges = (int *) malloc ( 2 * sizeof(int) );
      memset( connectedEdges, -1, 2 * sizeof(int) );

      itmp1 = 0;
      for (j = 0; j < (pMesh->v2eDim[i+1] - pMesh->v2eDim[i]); ++j)
	if ( pMesh->eTopFlags[ pMesh->v2e[ pMesh->v2eDim[i] + j ] ] == 2 ) {
	  if ( itmp1 == 2 ) {
	    printf("File %s line %i: found more than 2 mesh edges emanating from mesh vertex "
		   "%i, which is classified on a model edge, aborting ...\n",
		   __FILE__, __LINE__, i);
	    exit(1);
	  }
	  connectedEdges[itmp1++] = pMesh->v2e[ pMesh->v2eDim[i] + j ];
	}

      /* Find primal boundary faces connected to the starting primal edge */

      /* bndFaces is used differently from the first if case */
      bndFaces = (int *) malloc ( 2 * sizeof(int) );
      itmp1 = 0;
      for (j = 0; j < (pMesh->e2fDim[connectedEdges[0]+1] - pMesh->e2fDim[connectedEdges[0]]); ++j)
	if ( pMesh->f2t[ 2*(pMesh->e2f[ pMesh->e2fDim[connectedEdges[0]] + j ]) + 1 ] == -1 ) {
	  if ( itmp1 == 2 ) {
	    printf("File %s line %i: found more than 2 boundary mesh faces emanating "
		   "from a mesh edge classified on a model edge, aborting ...\n",
		   __FILE__, __LINE__);
	    exit(1);
	  }
	  bndFaces[itmp1++] = pMesh->e2f[ pMesh->e2fDim[connectedEdges[0]] + j ];
	}

      /* Overestimate dual face size */
      dualFaceMaxSize = 4;
      for (j = 0; j < (pMesh->v2fDim[i+1] - pMesh->v2fDim[i]); ++j)
	if ( pMesh->f2t[ 2*(pMesh->v2f[ pMesh->v2fDim[i] + j ]) + 1 ] == -1 ) ++dualFaceMaxSize;

      for (f = 0; f < 2; ++f) {
	hDualEdges = (int *) malloc ( dualFaceMaxSize * sizeof(int) );
	memset( hDualEdges, -1, dualFaceMaxSize * sizeof(int) );

	currEdge       = connectedEdges[0];
	currFace       = bndFaces[f];
	dualVertex1Idx = edge2dualVertices[ connectedEdges[1] ];
	counter        = 0;
	exitInnerFor   = false;

	for (j = 0; j < dualFaceMaxSize; ++j) {

	  if ( j == 0 ) {
	    dualVertex2Idx = edge2dualVertices[ currEdge ];
	  } else if ( j == 1 ) {
	    dualVertex2Idx = face2dualVertices[ currFace ];
	  } else {
	    /* Check if we reached the other side of the face */

	    if ( ( pMesh->f2e[ 3*currFace ] == connectedEdges[1] )
		 || ( pMesh->f2e[ 3*currFace+1 ] == connectedEdges[1] )
		 || ( pMesh->f2e[ 3*currFace+2 ] == connectedEdges[1] ) ) {
	      dualVertex2Idx = edge2dualVertices[ connectedEdges[1] ];
	      exitInnerFor   = true;
	    } else {
	      intersectSorted( pMesh->f2e + 3*currFace, 3,
			       pMesh->v2e + pMesh->v2eDim[i], pMesh->v2eDim[i+1]-pMesh->v2eDim[i],
			       &itmpV, &itmp1 );
	      if ( itmp1 != 2 ) {
		printf("File %s line %i: encountered error while creating a dual face "
		       "from boundary primal vertex %i. "
		       "Expected 2 shared edges, found %i instead, "
		       "aborting ...\n", __FILE__, __LINE__, i, itmp1);
		exit(1);
	      }
	      else {
		currEdge = currEdge == itmpV[0] ? itmpV[1] : itmpV[0];
		free( itmpV );
	      }

	      for (k = 0; k < (pMesh->e2fDim[currEdge+1] - pMesh->e2fDim[currEdge]); ++k)
		if ( ( pMesh->f2t[ 2 * pMesh->e2f[ pMesh->e2fDim[currEdge] + k ] + 1 ] == -1 )
		     && ( pMesh->e2f[ pMesh->e2fDim[currEdge] + k ] != currFace ) ) {
		  currFace = pMesh->e2f[ pMesh->e2fDim[currEdge] + k ];
		  break;
		}

	      dualVertex2Idx = face2dualVertices[ currFace ];
	    }
	  }

	  dualEdgeInsert( dualVertex1Idx, dualVertex2Idx, dualVerticesPointers,
			  v2vPointers, hDualEdges + j, &firstDualEdge, &currentDualEdge,
			  &dualEdgesCounter, &f2heSize, &counter );
	  
	  dualVertex1Idx = dualVertex2Idx;

	  if ( exitInnerFor ) break;
	}

	if ( counter > 2 ) {
	  /* Create dual face */
	  itmp1 = 0;
	  itmpV = (int *) malloc ( counter * sizeof(int) );
	  for (j = 0; j < dualFaceMaxSize; ++j)
	    if ( hDualEdges[j] != -1 ) itmpV[itmp1++] = hDualEdges[j];
	  v2dualFace[f] = dualFacesCounter;
	  currentDualFace->next = dualFaceCreate( counter, itmpV, NULL );
	  currentDualFace       = currentDualFace->next;

	  ++dualFacesCounter;
	  free( itmpV );
	} else {
	  printf("Found a degenerate face in Step 7b\n");
	}
	
	free( hDualEdges );
      }

      if ( v2dualFace[0] != -1 ) {
	if ( v2dualFace[1] != -1 ) {
	  if ( firstBndV2dualFaces == NULL ) {
	    firstBndV2dualFaces   = bndV2dualFacesCreate(2, v2dualFace, NULL );
	    currentBndV2dualFaces = firstBndV2dualFaces;
	  } else {
	    currentBndV2dualFaces->next = bndV2dualFacesCreate(2, v2dualFace, NULL);
	    currentBndV2dualFaces       = currentBndV2dualFaces->next;
	  }
	} else {
	  if ( firstBndV2dualFaces == NULL ) {
	    firstBndV2dualFaces   = bndV2dualFacesCreate(1, v2dualFace, NULL );
	    currentBndV2dualFaces = firstBndV2dualFaces;
	  } else {
	    currentBndV2dualFaces->next = bndV2dualFacesCreate(1, v2dualFace, NULL);
	    currentBndV2dualFaces       = currentBndV2dualFaces->next;
	  }
	}
      } else if ( v2dualFace[1] != -1 ) {
	if ( firstBndV2dualFaces == NULL ) {
	  firstBndV2dualFaces   = bndV2dualFacesCreate(1, v2dualFace+1, NULL );
	  currentBndV2dualFaces = firstBndV2dualFaces;
	} else {
	  currentBndV2dualFaces->next = bndV2dualFacesCreate(1, v2dualFace+1, NULL);
	  currentBndV2dualFaces       = currentBndV2dualFaces->next;
	}
      } else {
	if ( firstBndV2dualFaces == NULL ) {
	  firstBndV2dualFaces   = bndV2dualFacesCreate(0, NULL, NULL );
	  currentBndV2dualFaces = firstBndV2dualFaces;
	} else {
	  currentBndV2dualFaces->next = bndV2dualFacesCreate(0, NULL, NULL);
	  currentBndV2dualFaces       = currentBndV2dualFaces->next;
	}
      }
	
      free( v2dualFace );
      free( connectedEdges );
      free( bndFaces );
      
    } else if ( pMesh->vTopFlags[i] >= 300 ) {
      /* Creating faces from a primal vertex on a model vertex */
      numDualFaces2Add = pMesh->vTopFlags[i] - 300;
      
      v2dualFace = (int *) malloc ( numDualFaces2Add * sizeof(int) );
      memset( v2dualFace, -1, numDualFaces2Add * sizeof(int) );

      /* Find mesh edges classified on a model edge connected
    	 to the current primal vertex */

      connectedEdges = (int *) malloc ( numDualFaces2Add * sizeof(int) );
      memset( connectedEdges, -1, numDualFaces2Add * sizeof(int) );

      itmp1 = 0;
      for (j = 0; j < (pMesh->v2eDim[i+1] - pMesh->v2eDim[i]); ++j)
    	if ( pMesh->eTopFlags[ pMesh->v2e[ pMesh->v2eDim[i] + j ] ] == 2 ) {
    	  if ( itmp1 == numDualFaces2Add ) {
    	    printf("File %s line %i: found more than %i mesh edges emanating from a mesh vertex "
    		   "classified on a model edge, aborting ...\n",
    		   __FILE__, __LINE__, numDualFaces2Add);
    	    exit(1);
    	  }
    	  connectedEdges[itmp1++] = pMesh->v2e[ pMesh->v2eDim[i] + j ];
    	}

      qsort( connectedEdges, numDualFaces2Add, sizeof(int), compareInt );
      
      /* bndFaces is used differently from the first and the second if cases.
       * Here, it contains the indices of the boundary faces connected to mesh edges
       * classified on model edges. */

      bndFaces = (int *) malloc ( 2 * numDualFaces2Add * sizeof(int) );
      memset( bndFaces, -1, 2 * numDualFaces2Add * sizeof(int) );

      /* Overestimate dual face size */
      dualFaceMaxSize = 3*numDualFaces2Add;
      for (j = 0; j < (pMesh->v2fDim[i+1] - pMesh->v2fDim[i]); ++j)
    	if ( pMesh->f2t[ 2*(pMesh->v2f[ pMesh->v2fDim[i] + j ]) + 1 ] == -1 ) ++dualFaceMaxSize;

      for (f = 0; f < numDualFaces2Add; ++f) {
    	hDualEdges = (int *) malloc ( dualFaceMaxSize * sizeof(int) );
    	memset( hDualEdges, -1, dualFaceMaxSize * sizeof(int) );

    	/* Select starting edge */

	currEdge = -1;
    	for (itmp2 = 0; itmp2 < numDualFaces2Add; ++itmp2)
    	  if ( ( bndFaces[2*itmp2] == -1 ) || ( bndFaces[2*itmp2+1] == -1 ) ) {
    	    currEdge = connectedEdges[itmp2];
    	    break;
    	  }

    	/* Select starting face */
	
    	currFace = -1;
    	for (j = 0; j < (pMesh->e2fDim[currEdge+1] - pMesh->e2fDim[currEdge]); ++j)
    	  if ( ( pMesh->f2t[ 2*(pMesh->e2f[ pMesh->e2fDim[currEdge] + j ]) + 1 ] == -1 )
    	       && ( pMesh->e2f[ pMesh->e2fDim[currEdge] + j ] != bndFaces[2*itmp2] )
    	       && ( pMesh->e2f[ pMesh->e2fDim[currEdge] + j ] != bndFaces[2*itmp2+1] ) ) {
    	    currFace = pMesh->e2f[ pMesh->e2fDim[currEdge] + j ];
    	    break;
    	  }
    	if ( currFace == -1 ) {
    	  printf("File %s line %i: the starting edge has not been chosen properly, "
    		 "aborting ...\n", __FILE__, __LINE__);
    	  exit(1);
    	}
    	/* Update bndFaces */
    	if ( bndFaces[2*itmp2] == -1 )
    	  bndFaces[2*itmp2]   = currFace;
    	else
    	  bndFaces[2*itmp2+1] = currFace;
	
    	dualVertex1Idx = vertex2dualVertices[i];
    	counter        = 0;
    	exitInnerFor   = false;

    	for (j = 0; j < dualFaceMaxSize; ++j) {

    	  if ( j == 0 ) {
    	    dualVertex2Idx = edge2dualVertices[ currEdge ];
    	  } else if ( j == 1 ) {
    	    dualVertex2Idx = face2dualVertices[ currFace ];
    	  } else {
    	    intersectSorted( pMesh->f2e + 3*currFace, 3,
    			     connectedEdges, numDualFaces2Add, &itmpV, &itmp1);

    	    itmp2 = -1;
    	    for (k = 0; k < itmp1; ++k)
    	      if ( itmpV[k] != currEdge ) {
    		itmp2 = itmpV[k];
    		break;
    	      }

	    free( itmpV );
	    
    	    if ( itmp2 != -1 ) {
    	      dualVertex2Idx = edge2dualVertices[ itmp2 ];
    	      exitInnerFor   = true;

    	      /* Update bndFaces for the terminating edge */
    	      for (k = 0; k < numDualFaces2Add; ++k) if ( itmp2 == connectedEdges[k] ) break;
    	      if ( bndFaces[2*k] == -1 )
    		bndFaces[2*k]   = currFace;
    	      else
    		bndFaces[2*k+1] = currFace;
    	    } else {
    	      intersectSorted( pMesh->f2e + 3*currFace, 3,
    			       pMesh->v2e + pMesh->v2eDim[i], pMesh->v2eDim[i+1]-pMesh->v2eDim[i],
    			       &itmpV, &itmp1 );
    	      if ( itmp1 != 2 ) {
    		printf("File %s line %i: encountered error while creating a dual face "
    		       "from boundary primal vertex %i. "
    		       "Expected 2 shared edges, found %i instead, "
    		       "aborting ...\n", __FILE__, __LINE__, i, itmp1);
    		exit(1);
    	      }
    	      else {
    		currEdge = currEdge == itmpV[0] ? itmpV[1] : itmpV[0];
    		free( itmpV );
    	      }

    	      for (k = 0; k < (pMesh->e2fDim[currEdge+1] - pMesh->e2fDim[currEdge]); ++k)
    		if ( ( pMesh->f2t[ 2 * pMesh->e2f[ pMesh->e2fDim[currEdge] + k ] + 1 ] == -1 )
    		     && ( pMesh->e2f[ pMesh->e2fDim[currEdge] + k ] != currFace ) ) {
    		  currFace = pMesh->e2f[ pMesh->e2fDim[currEdge] + k ];
    		  break;
    		}

    	      dualVertex2Idx = face2dualVertices[ currFace ];
	      
    	    }
    	  }

    	  dualEdgeInsert( dualVertex1Idx, dualVertex2Idx, dualVerticesPointers,
    			  v2vPointers, hDualEdges + j, &firstDualEdge, &currentDualEdge,
    			  &dualEdgesCounter, &f2heSize, &counter );
	  
    	  dualVertex1Idx = dualVertex2Idx;

    	  if ( exitInnerFor ) break;
    	}

    	/* Insert last edge */
    	dualEdgeInsert( dualVertex1Idx, vertex2dualVertices[i], dualVerticesPointers,
    			v2vPointers, hDualEdges + j + 1, &firstDualEdge, &currentDualEdge,
    			&dualEdgesCounter, &f2heSize, &counter );

    	if ( counter > 2 ) {
    	  /* Create dual face */
    	  itmp1 = 0;
    	  itmpV = (int *) malloc ( counter * sizeof(int) );
    	  for (j = 0; j < dualFaceMaxSize; ++j)
    	    if ( hDualEdges[j] != -1 ) itmpV[itmp1++] = hDualEdges[j];
    	  v2dualFace[f] = dualFacesCounter;
    	  currentDualFace->next = dualFaceCreate( counter, itmpV, NULL );
    	  currentDualFace       = currentDualFace->next;
    	  ++dualFacesCounter;
    	  free( itmpV );
    	} else {
	  printf("Found a degenerate face in Step 7c\n");
    	}

    	free( hDualEdges );
      }

      /* Count the actual number of inserted faces */
      counter = 0;
      for (f = 0; f < numDualFaces2Add; ++f)
    	if ( v2dualFace[f] != -1 )
    	  ++counter;
      if ( counter > 0 ) {
	itmp1 = 0;
	itmpV = (int *) malloc ( counter * sizeof(int) );
	for (f = 0; f < numDualFaces2Add; ++f)
	  if ( v2dualFace[f] != -1 )
	    itmpV[itmp1++] = v2dualFace[f];
	if ( firstBndV2dualFaces == NULL ) {
	  firstBndV2dualFaces   = bndV2dualFacesCreate(counter, itmpV, NULL );
	  currentBndV2dualFaces = firstBndV2dualFaces;
	} else {
	  currentBndV2dualFaces->next = bndV2dualFacesCreate(counter, itmpV, NULL);
	  currentBndV2dualFaces       = currentBndV2dualFaces->next;
	}
	free( itmpV );
      } else {
	if ( firstBndV2dualFaces == NULL ) {
	  firstBndV2dualFaces   = bndV2dualFacesCreate(0, NULL, NULL );
	  currentBndV2dualFaces = firstBndV2dualFaces;
	} else {
	  currentBndV2dualFaces->next = bndV2dualFacesCreate(0, NULL, NULL);
	  currentBndV2dualFaces       = currentBndV2dualFaces->next;
	}
      }

      free( v2dualFace );
      free( connectedEdges );
      free( bndFaces );
    }
  }

#ifdef DEBUG
  printf("\nSummary of Steps 5, 6, and 7:\n");
  printf("Number of dual edges: %i\n", dualEdgesCounter);
  printf("Dual edges:\n");
  currentDualEdge = firstDualEdge;
  i = 0;
  while ( currentDualEdge != NULL ) {
    printf("%i %i %i\n", i++, currentDualEdge->vertices[0], currentDualEdge->vertices[1]);
    currentDualEdge = currentDualEdge->next;
  }
  printf("\nNumber of dual faces: %i\n", dualFacesCounter);
  printf("Dual faces:\n");
  currentDualFace = firstDualFace;
  i = 0;
  while ( currentDualFace != NULL ) {
    printf("%i", i++);
    for (j = 0; j < currentDualFace->Ned; ++j)
      printf(" %i", currentDualFace->hDualEdges[j]);
    printf("\n");
    currentDualFace = currentDualFace->next;
  }
#endif

  free( dualVerticesPointers );
  free( vertex2dualVertices );
  free( edge2dualVertices );
  free( face2dualVertices );

  for (i = 0; i < dMesh->Nver; ++i) {
    v2vCursor = v2vPointers[i];
    while ( v2vCursor != NULL ) {
      v2vTmp = v2vCursor->next;
      free( v2vCursor );
      v2vCursor = v2vTmp;
    }
  }
  free( v2vPointers );

  /*
   * End of Step 7
   */

  dMesh->Ned   = dualEdgesCounter;
  dMesh->edges = (int *) malloc ( 2 * (dMesh->Ned) * sizeof(int) );

  currentDualEdge = firstDualEdge;
  for (i = 0; i < dMesh->Ned; ++i) {
    tmpE = currentDualEdge->next;
    dMesh->edges[2*i]   = currentDualEdge->vertices[0];
    dMesh->edges[2*i+1] = currentDualEdge->vertices[1];
    free( currentDualEdge );
    currentDualEdge = tmpE;
  }

  dMesh->Nfc        = dualFacesCounter;
  dMesh->f2he       = (int *) malloc ( f2heSize * sizeof(int) );
  dMesh->f2heDim    = (int *) malloc ( (dMesh->Nfc + 1) * sizeof(int) );
  dMesh->f2heDim[0] = 0;

  currentDualFace = firstDualFace;
  for (i = 0; i < dMesh->Nfc; ++i) {
    tmpF = currentDualFace->next;
    memcpy( dMesh->f2he + dMesh->f2heDim[i], currentDualFace->hDualEdges,
	    ( currentDualFace->Ned ) * sizeof(int) );
    dMesh->f2heDim[i+1] = dMesh->f2heDim[i] + currentDualFace->Ned;
    free( currentDualFace->hDualEdges );
    free( currentDualFace );
    currentDualFace = tmpF;
  }

  /*
   * Steps 8 and 9
   *
   * Create dual regions corresponding to primal vertices. Observe that, by construction
   * of edge2dualVertices and the bndV2dualFaces list, for each polyhedron, the corresponding
   * half faces will be listed in ascending order.
   */

  dMesh->f2p = (int *) malloc ( 2 * (dMesh->Nfc) * sizeof(int) );
  memset( dMesh->f2p, -1, 2 * (dMesh->Nfc) * sizeof(int) );

  counter               = 0; /* Count the total number of half faces */
  currentBndV2dualFaces = firstBndV2dualFaces;

  for (i = 0; i < pMesh->Nver; ++i) {

    if ( pMesh->vTopFlags[i] == 0 ) {
      itmp1 = 0;
      for (j = 0; j < ( pMesh->v2eDim[i+1] - pMesh->v2eDim[i] ); ++j)
	if ( edge2dualFaces[ pMesh->v2e[ pMesh->v2eDim[i] + j ] ] != -1 ) ++itmp1;
      itmpV = (int *) malloc ( itmp1 * sizeof(int) );

      itmp1 = 0;
      for (j = 0; j < ( pMesh->v2eDim[i+1] - pMesh->v2eDim[i] ); ++j)
	if ( edge2dualFaces[ pMesh->v2e[ pMesh->v2eDim[i] + j ] ] != -1 )
	  itmpV[itmp1++] = edge2dualFaces[ pMesh->v2e[ pMesh->v2eDim[i] + j ] ];
    } else {
      itmp1 = 0;
      for (j = 0; j < ( pMesh->v2eDim[i+1] - pMesh->v2eDim[i] ); ++j)
	if ( edge2dualFaces[ pMesh->v2e[ pMesh->v2eDim[i] + j ] ] != -1 ) ++itmp1;
      itmp1 += currentBndV2dualFaces->Nfc;
      itmpV = (int *) malloc ( itmp1 * sizeof(int) );

      itmp1 = 0;
      for (j = 0; j < ( pMesh->v2eDim[i+1] - pMesh->v2eDim[i] ); ++j)
	if ( edge2dualFaces[ pMesh->v2e[ pMesh->v2eDim[i] + j ] ] != -1 )
	  itmpV[itmp1++] = edge2dualFaces[ pMesh->v2e[ pMesh->v2eDim[i] + j ] ];
      for (j = 0; j < currentBndV2dualFaces->Nfc; ++j)
	itmpV[itmp1++] = currentBndV2dualFaces->dualFacesIdx[j];

      currentBndV2dualFaces = currentBndV2dualFaces->next;
    }

    hDualFaces     = (int *) malloc ( itmp1 * sizeof(int) );
    facesCentroids = (double *) malloc ( 3 * itmp1 * sizeof(double) );
    memset( facesCentroids, 0.0, 3 * itmp1 * sizeof(double) );

    polyhedron = (int **) malloc ( itmp1 * sizeof(int *) );

    counter2 = 0;
    for (j = 0; j < itmp1; ++j) {
      itmp2         = dMesh->f2heDim[ itmpV[j] + 1 ] - dMesh->f2heDim[ itmpV[j] ];
      polyhedron[j] = (int *) malloc ( itmp2 * sizeof(int) );
      for (k = 0; k < itmp2; ++k) {
	hEdge            = dMesh->f2he[ dMesh->f2heDim[ itmpV[j] ] + k ];
	tEdge            = hEdge/2;
	polyhedron[j][k] = dMesh->edges[ 2 * tEdge + (hEdge%2) ];
	facesCentroids[3*j]   += dMesh->vertices[ 3 * polyhedron[j][k]     ];
	facesCentroids[3*j+1] += dMesh->vertices[ 3 * polyhedron[j][k] + 1 ];
	facesCentroids[3*j+2] += dMesh->vertices[ 3 * polyhedron[j][k] + 2 ];
      }
      facesCentroids[3*j]   /= (double)(itmp2);
      facesCentroids[3*j+1] /= (double)(itmp2);
      facesCentroids[3*j+2] /= (double)(itmp2);
      counter2 += itmp2;
    }

    /* Compute element centroid */
    itmpV2 = (int *) malloc (counter2 * sizeof(int));
    itmp2  = 0;
    for (j = 0; j < itmp1; ++j) {
      for (k = 0; k < (dMesh->f2heDim[ itmpV[j] + 1 ] - dMesh->f2heDim[ itmpV[j] ]); ++k)
	itmpV2[itmp2++] = polyhedron[j][k];
    }
    qsort( itmpV2, counter2, sizeof(int), compareInt );
    itmp2 = 1;
    polyhedronCentroid[0] = dMesh->vertices[3*itmpV2[0]];
    polyhedronCentroid[1] = dMesh->vertices[3*itmpV2[0]+1];
    polyhedronCentroid[2] = dMesh->vertices[3*itmpV2[0]+2];
    for (j = 1; j < counter2; ++j) {
      if ( itmpV2[j] != itmpV2[j-1] ) {
	polyhedronCentroid[0] += dMesh->vertices[3*itmpV2[j]];
	polyhedronCentroid[1] += dMesh->vertices[3*itmpV2[j]+1];
	polyhedronCentroid[2] += dMesh->vertices[3*itmpV2[j]+2];
	++itmp2;
      }
    }
    polyhedronCentroid[0] /= (double)(itmp2);
    polyhedronCentroid[1] /= (double)(itmp2);
    polyhedronCentroid[2] /= (double)(itmp2);
    free( itmpV2 );

    for (j = 0; j < itmp1; ++j) {
      tmp1  = 0.0;
      itmp2 = dMesh->f2heDim[ itmpV[j] + 1 ] - dMesh->f2heDim[ itmpV[j] ];
      for (k = 0; k < itmp2; ++k) {
	tmp1 += orient3d( facesCentroids  + 3*j,
			  dMesh->vertices + 3*(polyhedron[j][k]),
			  dMesh->vertices + 3*(polyhedron[j][(k+1)%itmp2]),
			  polyhedronCentroid );
      }
      hDualFaces[j] = tmp1 > 0.0 ? 2*itmpV[j] : 2*itmpV[j]+1;
      free( polyhedron[j] );

      if ( dMesh->f2p[ 2*itmpV[j] ] == -1 )
	dMesh->f2p[ 2*itmpV[j]   ] = i;
      else
	dMesh->f2p[ 2*itmpV[j]+1 ] = i;
    }
    free( polyhedron );
    free( facesCentroids );
    free( itmpV );

    if ( firstDualPolyhedron == NULL ) {
      firstDualPolyhedron   = dualPolyhedronCreate( itmp1, hDualFaces, NULL );
      currentDualPolyhedron = firstDualPolyhedron;
    } else {
      currentDualPolyhedron->next = dualPolyhedronCreate( itmp1, hDualFaces, NULL );
      currentDualPolyhedron       = currentDualPolyhedron->next;
    }
    free( hDualFaces );
    counter += itmp1;
  }

  currentBndV2dualFaces = firstBndV2dualFaces;
  while( currentBndV2dualFaces != NULL ) {
    tmpBndV2dualFaces = currentBndV2dualFaces->next;
    if ( currentBndV2dualFaces->dualFacesIdx != NULL )
      free( currentBndV2dualFaces->dualFacesIdx );
    free( currentBndV2dualFaces );
    currentBndV2dualFaces = tmpBndV2dualFaces;
  }

  free( edge2dualFaces );

  /*
   * End of Steps 8 and 9
   */

  dMesh->Npol       = pMesh->Nver;
  dMesh->p2hf       = (int *) malloc ( counter * sizeof(int) );
  dMesh->p2hfDim    = (int *) malloc ( (dMesh->Npol + 1) * sizeof(int) );
  dMesh->p2hfDim[0] = 0;

  currentDualPolyhedron = firstDualPolyhedron;
  for (i = 0; i < dMesh->Npol; ++i) {
    tmpP = currentDualPolyhedron->next;
    memcpy( dMesh->p2hf + dMesh->p2hfDim[i], currentDualPolyhedron->hDualFaces,
	    ( currentDualPolyhedron->Nfc ) * sizeof(int) );
    dMesh->p2hfDim[i+1] = dMesh->p2hfDim[i] + currentDualPolyhedron->Nfc;
    free( currentDualPolyhedron->hDualFaces );
    free( currentDualPolyhedron );
    currentDualPolyhedron = tmpP;
  }

#ifdef DEBUG
  printf("\nNumber of dual polyhedra: %i\n", dMesh->Npol);
  printf("Dual polyhedra to half faces:\n");
  for (i = 0; i < dMesh->Npol; ++i) {
    printf("%i", i);
    for (j = 0; j < ( dMesh->p2hfDim[i+1] - dMesh->p2hfDim[i] ); ++j )
      printf(" %i", dMesh->p2hf[ dMesh->p2hfDim[i] + j ] );
    printf("\n");
  }
#endif
}


void dualMeshView( const dualMesh * const dMesh )
{
  int i, j, patchSize;

  /* View vertices */
  if ( dMesh->vertices ) {
    printf("\n\nDual vertices:\n");
    if ( dMesh->vBndMarkers ) {
      printf("%i 1\n", dMesh->Nver);
      for (i = 0; i < dMesh->Nver; ++i)
	printf("%.16f %.16f %.16f %i\n",
	       dMesh->vertices[3*i], dMesh->vertices[3*i+1], dMesh->vertices[3*i+2],
	       dMesh->vBndMarkers[i]);
    } else {
      printf("%i 0\n", dMesh->Nver);
      for (i = 0; i < dMesh->Nver; ++i)
	printf("%.16f %.16f %.16f\n",
	       dMesh->vertices[3*i], dMesh->vertices[3*i+1], dMesh->vertices[3*i+2]);
    }
  }

  /* View v2f */
  if ( dMesh->v2f ) {
    printf("\n\nDual vertex to face connectivity:\n");
    for (i = 0; i < dMesh->Nver; ++i) {
      patchSize = dMesh->v2fDim[i+1] - dMesh->v2fDim[i];
      printf("%i", patchSize);
      for (j = 0; j < patchSize; ++j)
	printf(" %i", dMesh->v2f[ dMesh->v2fDim[i] + j ] );
      printf("\n");
    }
  }

  /* View v2p */
  if ( dMesh->v2p ) {
    printf("\n\nDual vertex to polyhedra connectivity:\n");
    for (i = 0; i < dMesh->Nver; ++i) {
      patchSize = dMesh->v2pDim[i+1] - dMesh->v2pDim[i];
      printf("%i", patchSize);
      for (j = 0; j < patchSize; ++j)
	printf(" %i", dMesh->v2p[ dMesh->v2pDim[i] + j ] );
      printf("\n");
    }
  }

  /* View edges */
  if ( dMesh->edges ) {
    printf("\n\nDual edges:\n");
    if ( dMesh->eBndMarkers ) {
      printf("%i 1\n", dMesh->Ned);
      for (i = 0; i < dMesh->Ned; ++i)
	printf("%i %i %i\n",
	       dMesh->edges[2*i], dMesh->edges[2*i+1], dMesh->eBndMarkers[i]);
    } else {
      printf("%i 0\n", dMesh->Ned);
      for (i = 0; i < dMesh->Ned; ++i)
	printf("%i %i\n",
	       dMesh->edges[2*i], dMesh->edges[2*i+1]);
    }
  }

  /* View face data */
  if ( dMesh->f2he ) {
    printf("\n\nDual faces:\n");
    if ( dMesh->fBndMarkers ) {
      printf("%i 1\n", dMesh->Nfc);
      for (i = 0; i < dMesh->Nfc; ++i) {
	patchSize = dMesh->f2heDim[i+1] - dMesh->f2heDim[i];
	printf("%i", patchSize);
	for (j = 0; j < patchSize; ++j)
	  printf(" %i", dMesh->f2he[ dMesh->f2heDim[i] + j ] );
	printf(" %i %i %i\n", dMesh->f2p[2*i], dMesh->f2p[2*i+1], dMesh->fBndMarkers[i]);
      }
    } else {
      printf("%i 0\n", dMesh->Nfc);
      for (i = 0; i < dMesh->Nfc; ++i) {
	patchSize = dMesh->f2heDim[i+1] - dMesh->f2heDim[i];
	printf("%i", patchSize);
	for (j = 0; j < patchSize; ++j)
	  printf(" %i", dMesh->f2he[ dMesh->f2heDim[i] + j ] );
	printf(" %i %i\n", dMesh->f2p[2*i], dMesh->f2p[2*i+1]);
      }
    }
  }

  /* View polyhedra to half faces */
  if ( dMesh->p2hf ) {
    printf("\n\nDual polyhedra to half dual faces:\n");
    printf("%i\n", dMesh->Npol);
    for (i = 0; i < dMesh->Npol; ++i) {
      patchSize = dMesh->p2hfDim[i+1] - dMesh->p2hfDim[i];
      printf("%i", patchSize);
      for (j = 0; j < patchSize; ++j )
	printf("   %i", dMesh->p2hf[ dMesh->p2hfDim[i] + j ] );
      printf("\n");
    }
  }
}


void dualMesh2dDestroy( dualMesh2d * dMesh2d ) {
  if ( dMesh2d->vertices )    { free( dMesh2d->vertices ); dMesh2d->vertices = NULL; }
  if ( dMesh2d->vBndMarkers ) { free( dMesh2d->vBndMarkers ); dMesh2d->vBndMarkers = NULL; }
  if ( dMesh2d->v2p )         { free( dMesh2d->v2p ); dMesh2d->v2p = NULL; }
  if ( dMesh2d->v2pDim )      { free( dMesh2d->v2pDim ); dMesh2d->v2pDim = NULL; }
  dMesh2d->Nver = 0;

  if ( dMesh2d->edges )       { free( dMesh2d->edges ); dMesh2d->edges = NULL; }
  if ( dMesh2d->eBndMarkers ) { free( dMesh2d->eBndMarkers ); dMesh2d->eBndMarkers = NULL; }
  if ( dMesh2d->dirEdges )    { free( dMesh2d->dirEdges ); dMesh2d->dirEdges = NULL; }
  if ( dMesh2d->neuEdges )    { free( dMesh2d->neuEdges ); dMesh2d->neuEdges = NULL; }
  if ( dMesh2d->e2p   )       { free( dMesh2d->e2p ); dMesh2d->e2p = NULL; }
  dMesh2d->Ned    = 0;
  dMesh2d->NedDir = 0;
  dMesh2d->NedNeu = 0;

  if ( dMesh2d->p2v )         { free( dMesh2d->p2v ); dMesh2d->p2v = NULL; }
  if ( dMesh2d->p2vDim )      { free( dMesh2d->p2vDim ); dMesh2d->p2vDim = NULL; }
  if ( dMesh2d->p2e )         { free( dMesh2d->p2e ); dMesh2d->p2e = NULL; }
  if ( dMesh2d->p2eDim )      { free( dMesh2d->p2eDim ); dMesh2d->p2eDim = NULL; }
  dMesh2d->Npol = 0;
}


void dualMeshDestroy( dualMesh * dMesh ) {
  if ( dMesh->vertices )    { free( dMesh->vertices ); dMesh->vertices = NULL; }
  if ( dMesh->vBndMarkers ) { free( dMesh->vBndMarkers ); dMesh->vBndMarkers = NULL; }
  if ( dMesh->v2f )         { free( dMesh->v2f ); dMesh->v2f = NULL; }
  if ( dMesh->v2fDim )      { free( dMesh->v2fDim ); dMesh->v2fDim = NULL; }
  if ( dMesh->v2p )         { free( dMesh->v2p ); dMesh->v2p = NULL; }
  if ( dMesh->v2pDim )      { free( dMesh->v2pDim ); dMesh->v2pDim = NULL; }
  dMesh->Nver = 0;

  if ( dMesh->edges )       { free( dMesh->edges ); dMesh->edges = NULL; }
  if ( dMesh->eBndMarkers ) { free( dMesh->eBndMarkers ); dMesh->eBndMarkers = NULL; }
  dMesh->Ned = 0;

  if ( dMesh->f2he )        { free( dMesh->f2he ); dMesh->f2he = NULL; }
  if ( dMesh->f2heDim )     { free( dMesh->f2heDim ); dMesh->f2heDim = NULL; }
  if ( dMesh->f2p )         { free( dMesh->f2p ); dMesh->f2p = NULL; }
  if ( dMesh->fBndMarkers ) { free( dMesh->fBndMarkers ); dMesh->fBndMarkers = NULL; }
  dMesh->Nfc = 0;

  if ( dMesh->p2hf )      { free( dMesh->p2hf ); dMesh->p2hf = NULL; }
  if ( dMesh->p2hfDim )   { free( dMesh->p2hfDim ); dMesh->p2hfDim = NULL; }
  dMesh->Npol = 0;
}


void dualMeshSave( const char * const basename,
		   const dualMesh * const dMesh )
{
  char filename[256];
  FILE *fp = NULL;
  int i, j, patchSize;

  /* Vertices */
  sprintf(filename, "%s%s", basename, ".node");
  fp = fopen(filename, "w");
  if ( dMesh->vBndMarkers ) {
    fprintf(fp, "%i 1\n", dMesh->Nver);
    for (i = 0; i < dMesh->Nver; ++i)
      fprintf(fp, "%.16f %.16f %.16f %i\n",
	      dMesh->vertices[3*i], dMesh->vertices[3*i+1], dMesh->vertices[3*i+2],
	      dMesh->vBndMarkers[i]);
  } else {
    fprintf(fp, "%i 0\n", dMesh->Nver);
    for (i = 0; i < dMesh->Nver; ++i)
      fprintf(fp, "%.16f %.16f %.16f\n",
	      dMesh->vertices[3*i], dMesh->vertices[3*i+1], dMesh->vertices[3*i+2]);
  }
  fclose(fp);

  if ( dMesh->v2f ) {
    filename[0] = '\0';
    sprintf(filename, "%s%s", basename, ".v2f");
    fp = fopen(filename, "w");
    fprintf(fp, "%i\n", dMesh->Nver);
    for (i = 0; i < dMesh->Nver; ++i) {
      patchSize = dMesh->v2fDim[i+1] - dMesh->v2fDim[i];
      fprintf(fp, "%i", patchSize);
      for (j = 0; j < patchSize; ++j)
	fprintf(fp, " %i", dMesh->v2f[ dMesh->v2fDim[i] + j ] );
      fprintf(fp, "\n");
    }
    fclose(fp);
  }

  if ( dMesh->v2p ) {
    filename[0] = '\0';
    sprintf(filename, "%s%s", basename, ".v2p");
    fp = fopen(filename, "w");
    fprintf(fp, "%i\n", dMesh->Nver);
    for (i = 0; i < dMesh->Nver; ++i) {
      patchSize = dMesh->v2pDim[i+1] - dMesh->v2pDim[i];
      fprintf(fp, "%i", patchSize);
      for (j = 0; j < patchSize; ++j)
	fprintf(fp, " %i", dMesh->v2p[ dMesh->v2pDim[i] + j ] );
      fprintf(fp, "\n");
    }
    fclose(fp);
  }

  /* Edges */
  filename[0] = '\0';
  sprintf(filename, "%s%s", basename, ".edge");
  fp = fopen(filename, "w");
  if ( dMesh->eBndMarkers ) {
    fprintf(fp, "%i 1\n", dMesh->Ned);
    for (i = 0; i < dMesh->Ned; ++i)
      fprintf(fp, "%i %i %i\n",
	      dMesh->edges[2*i], dMesh->edges[2*i+1], dMesh->eBndMarkers[i]);
  } else {
    fprintf(fp, "%i 0\n", dMesh->Ned);
    for (i = 0; i < dMesh->Ned; ++i)
      fprintf(fp, "%i %i\n",
	      dMesh->edges[2*i], dMesh->edges[2*i+1]);
  }
  fclose(fp);

  /* Faces */
  filename[0] = '\0';
  sprintf(filename, "%s%s", basename, ".face");
  fp = fopen(filename, "w");
  if ( dMesh->fBndMarkers ) {
    fprintf(fp, "%i 1\n", dMesh->Nfc);
    for (i = 0; i < dMesh->Nfc; ++i) {
      patchSize = dMesh->f2heDim[i+1] - dMesh->f2heDim[i];
      fprintf(fp, "%i", patchSize);
      for (j = 0; j < patchSize; ++j)
	fprintf(fp, " %i", dMesh->f2he[ dMesh->f2heDim[i] + j ] );
      fprintf(fp, " %i %i %i\n", dMesh->f2p[2*i], dMesh->f2p[2*i+1], dMesh->fBndMarkers[i]);
    }
  } else {
    fprintf(fp, "%i 0\n", dMesh->Nfc);
    for (i = 0; i < dMesh->Nfc; ++i) {
      patchSize = dMesh->f2heDim[i+1] - dMesh->f2heDim[i];
      fprintf(fp, "%i", patchSize);
      for (j = 0; j < patchSize; ++j)
	fprintf(fp, " %i", dMesh->f2he[ dMesh->f2heDim[i] + j ] );
      fprintf(fp, " %i %i\n", dMesh->f2p[2*i], dMesh->f2p[2*i+1]);
    }
  }
  fclose(fp);

  /* Polyhedra to half faces */
  filename[0] = '\0';
  sprintf(filename, "%s%s", basename, ".polyhedron");
  fp = fopen(filename, "w");  
  fprintf(fp, "%i\n", dMesh->Npol);
  for (i = 0; i < dMesh->Npol; ++i) {
    patchSize = dMesh->p2hfDim[i+1] - dMesh->p2hfDim[i];
    fprintf(fp, "%i", patchSize);
    for (j = 0; j < patchSize; ++j )
      fprintf(fp, " %i", dMesh->p2hf[ dMesh->p2hfDim[i] + j ] );
    fprintf(fp, "\n");
  }
  fclose(fp);
}


void saveOFFmesh( const char * const filename,
		  const dualMesh2d * const dMesh2d )
{
  FILE *fp = NULL;
  int i, j, patchSize;

  fp = fopen(filename, "w");
  fprintf(fp, "OFF\n");

  if ( dMesh2d->vBndMarkers != NULL ) {
    fprintf(fp, "%i %i 1\n", dMesh2d->Nver, dMesh2d->Npol);
    for (i = 0; i < dMesh2d->Nver; ++i)
      fprintf(fp, "%.16f %.16f %i\n",
	      dMesh2d->vertices[2*i], dMesh2d->vertices[2*i+1], dMesh2d->vBndMarkers[i]);
  } else {
    fprintf(fp, "%i %i 0\n", dMesh2d->Nver, dMesh2d->Npol);
    for (i = 0; i < dMesh2d->Nver; ++i)
      fprintf(fp, "%.16f %.16f 0\n",
	      dMesh2d->vertices[2*i], dMesh2d->vertices[2*i+1]);
  }

  for (i = 0; i < dMesh2d->Npol; ++i) {
    patchSize = dMesh2d->p2vDim[i+1] - dMesh2d->p2vDim[i];
    fprintf(fp, "%i", patchSize);
    for (j = 0; j < patchSize; ++j)
      fprintf(fp, " %i", dMesh2d->p2v[ dMesh2d->p2vDim[i] + j ] );
    fprintf(fp, "\n");
  }

  fclose(fp);
}


void readOFFmesh( const char * const filename,
		  dualMesh2d * dMesh2d )
{
  char line[256];
  FILE *fp = NULL;
  int i, j, itmp1, itmp2;
  long size;
  int * polygon = NULL;
  double polygonCenter[2];
  double tmp;

  fp = fopen(filename, "r");
  if ( fgets(line, 256, fp) == NULL ) {
    printf("File %s line %i: error while reading header in OFF file, aborting ...\n",
	   __FILE__, __LINE__);
    exit(1);
  }

  /*
   * Read vertices coordinates
   */

  if ( fscanf(fp, "%i %i %i", &(dMesh2d->Nver), &(dMesh2d->Npol), &itmp1) != 3 ) {
    printf("File %s line %i: error while reading number of vertices/polygons in OFF file, "
	   "aborting ...\n", __FILE__, __LINE__);
    exit(1);
  }

  dMesh2d->vertices    = (double *) malloc (2 * (dMesh2d->Nver) * sizeof(double));
  dMesh2d->vBndMarkers = (int *) malloc ((dMesh2d->Nver) * sizeof(int));
  for (i = 0; i < dMesh2d->Nver; ++i) {
    if ( fscanf(fp, "%lf %lf %i",
		dMesh2d->vertices+2*i, dMesh2d->vertices+2*i+1, dMesh2d->vBndMarkers+i) != 3 ) {
      printf("File %s line %i: encountered error while reading coordinates of vertex %i in "
	     "OFF file, aborting ...\n", __FILE__, __LINE__, i);
      exit(1);
    }
  }

  /* Store file position */
  size = ftell(fp);

  /*
   * Read polygons section
   */

  dMesh2d->p2vDim = (int *) malloc ((dMesh2d->Npol+1) * sizeof(int));
  dMesh2d->p2vDim[0] = 0;
  for (i = 0; i < dMesh2d->Npol; ++i) {
    if ( fscanf(fp, "%i", &itmp1) != 1 ) {
      printf("File %s line %i: error while reading polygons in OFF file, "
	     "aborting ...\n", __FILE__, __LINE__);
      exit(1);
    }
    dMesh2d->p2vDim[i+1] = dMesh2d->p2vDim[i] + itmp1;
    for (j = 0; j < itmp1 ; ++j) {
      if ( fscanf(fp, "%i", &itmp2) != 1 ) {
	printf("File %s line %i: error while reading polygons in OFF file, "
	       "aborting ...\n", __FILE__, __LINE__);
	exit(1);
      }
    }
  }

  /* Reposition file pointer */
  fseek( fp, size, SEEK_SET );

  dMesh2d->p2v = (int *) malloc ((dMesh2d->p2vDim[dMesh2d->Npol]) * sizeof(int));

  itmp2 = 0;
  for (i = 0; i < dMesh2d->Npol; ++i) {

    if ( fscanf(fp, "%i", &itmp1) != 1 ) {
      printf("File %s line %i: error while reading polygons in OFF file, "
	     "aborting ...\n", __FILE__, __LINE__);
      exit(1);
    }

    polygon = (int *) malloc (itmp1 * sizeof(int));
    polygonCenter[0] = 0.0;
    polygonCenter[1] = 0.0;
    for (j = 0; j < itmp1; ++j) {
      if ( fscanf(fp, "%i", polygon + j) != 1 ) {
	printf("File %s line %i: error while reading polygons in OFF file, "
	       "aborting ...\n", __FILE__, __LINE__);
	exit(1);
      }
      polygonCenter[0] += dMesh2d->vertices[2*polygon[j]];
      polygonCenter[1] += dMesh2d->vertices[2*polygon[j]+1];
    }
    polygonCenter[0] /= (double)(itmp1);
    polygonCenter[1] /= (double)(itmp1);

    tmp = 0.0;
    for (j = 0; j < itmp1; ++j) {
      tmp += orient2d( polygonCenter,
		       dMesh2d->vertices + 2*polygon[j],
		       dMesh2d->vertices + 2*polygon[(j+1)%itmp1] );
    }
    if ( tmp > 0.0 ) {
      for (j = 0; j < itmp1; ++j) dMesh2d->p2v[itmp2+j] = polygon[j];
    } else {
      for (j = 0; j < itmp1; ++j) dMesh2d->p2v[itmp2+j] = polygon[itmp1-1-j];
    }
    itmp2 += itmp1;

    free(polygon);
  }

  fclose(fp);
}


void dualMeshRead( const char * const basename,
		   dualMesh * dMesh )
{
  char filename[256];
  FILE *fp = NULL;
  int i, j, counter, itmp1, itmp2, itmp3;

  /*
   * Vertices
   */

  sprintf(filename, "%s%s", basename, ".node");
  fp = fopen(filename, "r");
  if ( fscanf(fp, "%i %i", &(dMesh->Nver), &itmp1) != 2 ) {
    printf("File %s line %i: encountered error while reading dual mesh node file, aborting ...\n",
	   __FILE__, __LINE__);
    exit(1);
  }

  dMesh->vertices = (double *) malloc (3 * (dMesh->Nver) * sizeof(double));
  if ( itmp1 == 1 ) {
    dMesh->vBndMarkers = (int *) malloc ((dMesh->Nver) * sizeof(int));
    for (i = 0; i < dMesh->Nver; ++i) {
      if ( fscanf(fp, "%lf %lf %lf %i",
		  dMesh->vertices + 3*i, dMesh->vertices + 3*i + 1, dMesh->vertices + 3*i + 2,
		  dMesh->vBndMarkers + i) != 4 ) {
	printf("File %s line %i: encountered error while reading dual mesh node file, "
	       "aborting ...\n", __FILE__, __LINE__);
	exit(1);
      }
    }
  } else {
    for (i = 0; i < dMesh->Nver; ++i) {
      if ( fscanf(fp, "%lf %lf %lf",
		  dMesh->vertices+3*i, dMesh->vertices+3*i+1, dMesh->vertices+3*i+2) != 3 ) {
	printf("File %s line %i: encountered error while reading dual mesh node file, "
	       "aborting ...\n", __FILE__, __LINE__);
	exit(1);
      }
    }
  }
  fclose(fp);

  filename[0] = '\0';
  sprintf(filename, "%s%s", basename, ".v2p");
  fp = fopen(filename, "r");
  if ( fp != NULL ) {
    if ( fscanf(fp, "%i", &itmp1) != 1 ) {
      printf("File %s line %i: encountered error while reading dual mesh "
	     "v2p file, aborting ...\n", __FILE__, __LINE__);
      exit(1);
    }

    dMesh->v2pDim = (int *) malloc ((dMesh->Nver+1) * sizeof(int));
    dMesh->v2pDim[0] = 0;
    for (i = 0; i < dMesh->Nver; ++i) {
      if ( fscanf(fp, "%i", &itmp1) != 1 ) {
	printf("File %s line %i: encountered error while reading dual mesh "
	       "v2p file, aborting ...\n", __FILE__, __LINE__);
	exit(1);
      }
      dMesh->v2pDim[i+1] = dMesh->v2pDim[i] + itmp1;
      for (j = 0; j < itmp1; ++j) {
	if ( fscanf(fp, "%i", &itmp2) != 1 ) {
	  printf("File %s line %i: encountered error while reading dual mesh "
		 "v2p file, aborting ...\n", __FILE__, __LINE__);
	  exit(1);
	}
      }
    }

    rewind(fp);

    dMesh->v2p = (int *) malloc ((dMesh->v2pDim[dMesh->Nver]) * sizeof(int));

    if ( fscanf(fp, "%i", &itmp1) != 1 ) {
      printf("File %s line %i: encountered error while reading dual mesh "
	     "v2p file, aborting ...\n", __FILE__, __LINE__);
      exit(1);
    }

    counter = 0;
    for (i = 0; i < dMesh->Nver; ++i) {
      if ( fscanf(fp, "%i", &itmp1) != 1 ) {
	printf("File %s line %i: encountered error while reading dual mesh "
	       "v2p file, aborting ...\n", __FILE__, __LINE__);
	exit(1);
      }
      for (j = 0; j < itmp1; ++j, ++counter) {
	if ( fscanf(fp, "%i", dMesh->v2p + counter) != 1 ) {
	  printf("File %s line %i: encountered error while reading dual mesh "
		 "v2p file, aborting ...\n", __FILE__, __LINE__);
	  exit(1);
	}
      }
    }
    fclose(fp);
  }

  filename[0] = '\0';
  sprintf(filename, "%s%s", basename, ".v2f");
  fp = fopen(filename, "r");
  if ( fp != NULL ) {
    if ( fscanf(fp, "%i", &itmp1) != 1 ) {
      printf("File %s line %i: encountered error while reading dual mesh "
	     "v2f file, aborting ...\n", __FILE__, __LINE__);
      exit(1);
    }

    dMesh->v2fDim = (int *) malloc ((dMesh->Nver+1) * sizeof(int));
    dMesh->v2fDim[0] = 0;
    for (i = 0; i < dMesh->Nver; ++i) {
      if ( fscanf(fp, "%i", &itmp1) != 1 ) {
	printf("File %s line %i: encountered error while reading dual mesh "
	       "v2f file, aborting ...\n", __FILE__, __LINE__);
	exit(1);
      }
      dMesh->v2fDim[i+1] = dMesh->v2fDim[i] + itmp1;
      for (j = 0; j < itmp1; ++j) {
	if ( fscanf(fp, "%i", &itmp2) != 1 ) {
	  printf("File %s line %i: encountered error while reading dual mesh "
		 "v2f file, aborting ...\n", __FILE__, __LINE__);
	  exit(1);
	}
      }
    }

    rewind(fp);

    dMesh->v2f = (int *) malloc ((dMesh->v2fDim[dMesh->Nver]) * sizeof(int));

    if ( fscanf(fp, "%i", &itmp1) != 1 ) {
      printf("File %s line %i: encountered error while reading dual mesh "
	     "v2f file, aborting ...\n", __FILE__, __LINE__);
      exit(1);
    }

    counter = 0;
    for (i = 0; i < dMesh->Nver; ++i) {
      if ( fscanf(fp, "%i", &itmp1) != 1 ) {
	printf("File %s line %i: encountered error while reading dual mesh "
	       "v2f file, aborting ...\n", __FILE__, __LINE__);
	exit(1);
      }
      for (j = 0; j < itmp1; ++j, ++counter) {
	if ( fscanf(fp, "%i", dMesh->v2f + counter) != 1 ) {
	  printf("File %s line %i: encountered error while reading dual mesh "
		 "v2f file, aborting ...\n", __FILE__, __LINE__);
	  exit(1);
	}
      }
    }
    fclose(fp);
  }

  /*
   * Edges
   */

  filename[0] = '\0';
  sprintf(filename, "%s%s", basename, ".edge");
  fp = fopen(filename, "r");
  if ( fscanf(fp, "%i %i", &(dMesh->Ned), &itmp1) != 2 ) {
    printf("File %s line %i: encountered error while reading dual mesh edge file, aborting ...\n",
	   __FILE__, __LINE__);
    exit(1);
  }

  dMesh->edges = (int *) malloc (2 * (dMesh->Ned) * sizeof(int));
  if ( itmp1 == 1 ) {
    dMesh->eBndMarkers = (int *) malloc ((dMesh->Ned) * sizeof(int));
    for (i = 0; i < dMesh->Ned; ++i) {
      if ( fscanf(fp, "%i %i %i", dMesh->edges + 2*i, dMesh->edges + 2*i + 1,
		  dMesh->eBndMarkers + i) != 3 ) {
	printf("File %s line %i: encountered error while reading dual mesh edge file, "
	       "aborting ...\n", __FILE__, __LINE__);
	exit(1);
      }
    }
  } else {
    for (i = 0; i < dMesh->Ned; ++i) {
      if ( fscanf(fp, "%i %i", dMesh->edges + 2*i, dMesh->edges + 2*i + 1) != 2 ) {
	printf("File %s line %i: encountered error while reading dual mesh edge file, "
	       "aborting ...\n", __FILE__, __LINE__);
	exit(1);
      }
    }
  }
  fclose(fp);

  /*
   * Faces
   */

  /* Read the number of faces and count the total number of half edges */

  filename[0] = '\0';
  sprintf(filename, "%s%s", basename, ".face");
  fp = fopen(filename, "r");
  if ( fscanf(fp, "%i %i", &(dMesh->Nfc), &itmp1) != 2 ) {
    printf("File %s line %i: encountered error while reading dual mesh face file, "
	   "aborting ...\n", __FILE__, __LINE__);
    exit(1);
  }

  dMesh->f2heDim = (int *) malloc ((dMesh->Nfc+1) * sizeof(int));
  dMesh->f2heDim[0] = 0;
  for (i = 0; i < dMesh->Nfc; ++i) {
    if ( fscanf(fp, "%i", &itmp2) != 1 ) {
      printf("File %s line %i: encountered error while reading dual mesh face file, "
	     "aborting ...\n", __FILE__, __LINE__);
      exit(1);
    }
    dMesh->f2heDim[i+1] = dMesh->f2heDim[i] + itmp2;
    for (j = 0; j < ( itmp2 + 2 + itmp1) ; ++j) {
      if ( fscanf(fp, "%i", &itmp3) != 1 ) {
	printf("File %s line %i: encountered error while reading dual mesh face file, "
	       "aborting ...\n", __FILE__, __LINE__);
	exit(1);
      }
    }
  }
  rewind(fp);

  dMesh->f2he = (int *) malloc ((dMesh->f2heDim[dMesh->Nfc]) * sizeof(int));
  dMesh->f2p  = (int *) malloc (2 * (dMesh->Nfc) * sizeof(int));
  if ( itmp1 == 1 ) dMesh->fBndMarkers = (int *) malloc ((dMesh->Nfc) * sizeof(int));

  if ( fscanf(fp, "%i %i", &itmp2, &itmp3) != 2 ) {
    printf("File %s line %i: encountered error while reading dual mesh face file, "
	   "aborting ...\n", __FILE__, __LINE__);
    exit(1);
  }

  counter = 0;
  for (i = 0; i < dMesh->Nfc; ++i) {
    if ( fscanf(fp, "%i", &itmp2) != 1 ) {
      printf("File %s line %i: encountered error while reading dual mesh face file, "
	     "aborting ...\n", __FILE__, __LINE__);
      exit(1);
    }
    for (j = 0; j < itmp2; ++j, ++counter) {
      if ( fscanf(fp, "%i", dMesh->f2he + counter) != 1 ) {
	printf("File %s line %i: encountered error while reading dual mesh face file, "
	       "aborting ...\n", __FILE__, __LINE__);
	exit(1);
      }
    }
    if ( fscanf(fp, "%i %i", dMesh->f2p + 2*i, dMesh->f2p + 2*i + 1) != 2 ) {
      printf("File %s line %i: encountered error while reading dual mesh face file, "
	     "aborting ...\n", __FILE__, __LINE__);
      exit(1);
    }
    if ( itmp1 == 1 ) {
      if ( fscanf(fp, "%i", dMesh->fBndMarkers + i) != 1 ) {
	printf("File %s line %i: encountered error while reading dual mesh face file, "
	       "aborting ...\n", __FILE__, __LINE__);
	exit(1);
      }
    }
  }
  fclose(fp);

  /*
   * Polyhedra
   */

  /* Read the number of polyhedra and count the total number of half faces */

  filename[0] = '\0';
  sprintf(filename, "%s%s", basename, ".polyhedron");
  fp = fopen(filename, "r");
  if ( fscanf(fp, "%i", &(dMesh->Npol)) != 1 ) {
    printf("File %s line %i: encountered error while reading dual mesh "
	   "polyhedra file, aborting ...\n", __FILE__, __LINE__);
    exit(1);
  }

  dMesh->p2hfDim = (int *) malloc ((dMesh->Npol+1) * sizeof(int));
  dMesh->p2hfDim[0] = 0;
  for (i = 0; i < dMesh->Npol; ++i) {
    if ( fscanf(fp, "%i", &itmp1) != 1 ) {
      printf("File %s line %i: encountered error while reading dual mesh "
	     "polyhedra file, aborting ...\n", __FILE__, __LINE__);
      exit(1);
    }
    dMesh->p2hfDim[i+1] = dMesh->p2hfDim[i] + itmp1;
    for (j = 0; j < itmp1; ++j) {
      if ( fscanf(fp, "%i", &itmp2) != 1 ) {
	printf("File %s line %i: encountered error while reading dual mesh "
	       "polyhedra file, aborting ...\n", __FILE__, __LINE__);
	exit(1);
      }
    }
  }
  rewind(fp);

  /* Read half faces */

  dMesh->p2hf = (int *) malloc ((dMesh->p2hfDim[dMesh->Npol]) * sizeof(int));

  if ( fscanf(fp, "%i", &itmp1) != 1 ) {
    printf("File %s line %i: encountered error while reading dual mesh "
	   "polyhedra file, aborting ...\n", __FILE__, __LINE__);
    exit(1);
  }

  counter = 0;
  for (i = 0; i < dMesh->Npol; ++i) {
    if ( fscanf(fp, "%i", &itmp1) != 1 ) {
      printf("File %s line %i: encountered error while reading dual mesh "
	     "polyhedra file, aborting ...\n", __FILE__, __LINE__);
      exit(1);
    }
    for (j = 0; j < itmp1; ++j, ++counter) {
      if ( fscanf(fp, "%i", dMesh->p2hf + counter) != 1 ) {
	printf("File %s line %i: encountered error while reading dual mesh "
	       "polyhedra file, aborting ...\n", __FILE__, __LINE__);
	exit(1);
      }
    }
  }
  fclose(fp);
}


void dualMeshReadOVM( const char * const filename,
		      dualMesh * dMesh )
{
  char line[256];
  FILE *fp = NULL;
  int i, j, itmp1, itmp2;
  long size;

  fp = fopen(filename, "r");
  if ( fgets(line, 256, fp) == NULL ) {
    printf("File %s line %i: encountered error while reading header of OVM file, aborting ...\n",
	   __FILE__, __LINE__);
    exit(1);
  }

  /*
   * Read Vertices section
   */

  if ( fgets(line, 256, fp) == NULL ) {
    printf("File %s line %i: encountered error while reading Vertices section in OVM file, "
	   "aborting ...\n", __FILE__, __LINE__);
    exit(1);
  }

  if ( fscanf(fp, "%i", &(dMesh->Nver)) != 1 ) {
    printf("File %s line %i: encountered error while reading number of vertices in OVM file, "
	   "aborting ...\n", __FILE__, __LINE__);
    exit(1);
  }

  dMesh->vertices = (double *) malloc (3 * (dMesh->Nver) * sizeof(double));
  for (i = 0; i < dMesh->Nver; ++i) {
    if ( fscanf(fp, "%lf %lf %lf",
		dMesh->vertices+3*i, dMesh->vertices+3*i+1, dMesh->vertices+3*i+2) != 3 ) {
      printf("File %s line %i: encountered error while reading coordinates of vertex %i in "
	     "OVM file, aborting ...\n", __FILE__, __LINE__, i);
      exit(1);
    }
  }

  /*
   * Read Edges section
   */

  if ( fgets(line, 256, fp) == NULL ) {
    printf("File %s line %i: encountered error while reading Edges section in OVM file, "
	   "aborting ...\n", __FILE__, __LINE__);
    exit(1);
  } else if ( fgets(line, 256, fp) == NULL ) {
    printf("File %s line %i: encountered error while reading Edges section in OVM file, "
	   "aborting ...\n", __FILE__, __LINE__);
    exit(1);
  }

  if ( fscanf(fp, "%i", &(dMesh->Ned)) != 1 ) {
    printf("File %s line %i: encountered error while reading number of edges in OVM file, "
	   "aborting ...\n", __FILE__, __LINE__);
    exit(1);
  }

  dMesh->edges = (int *) malloc (2 * (dMesh->Ned) * sizeof(int));
  for (i = 0; i < dMesh->Ned; ++i) {
    if ( fscanf(fp, "%i %i", dMesh->edges + 2*i, dMesh->edges + 2*i + 1) != 2 ) {
      printf("File %s line %i: encountered error while reading edges in OVM file, "
	     "aborting ...\n", __FILE__, __LINE__);
      exit(1);
    }
  }

  /*
   * Read Facets section
   */

  if ( fgets(line, 256, fp) == NULL ) {
    printf("File %s line %i: encountered error while reading facets section in OVM file, "
	   "aborting ...\n", __FILE__, __LINE__);
    exit(1);
  } else if ( fgets(line, 256, fp) == NULL ) {
    printf("File %s line %i: encountered error while reading facets section in OVM file, "
	   "aborting ...\n", __FILE__, __LINE__);
    exit(1);
  }

  if ( fscanf(fp, "%i", &(dMesh->Nfc)) != 1 ) {
    printf("File %s line %i: encountered error while reading number of faces in OVM file, "
	   "aborting ...\n", __FILE__, __LINE__);
    exit(1);
  }

  /* Store file position */
  size = ftell(fp);

  dMesh->f2heDim = (int *) malloc ((dMesh->Nfc+1) * sizeof(int));
  dMesh->f2heDim[0] = 0;
  for (i = 0; i < dMesh->Nfc; ++i) {
    if ( fscanf(fp, "%i", &itmp1) != 1 ) {
      printf("File %s line %i: encountered error while reading facets in OVM file, "
	     "aborting ...\n", __FILE__, __LINE__);
      exit(1);
    }
    dMesh->f2heDim[i+1] = dMesh->f2heDim[i] + itmp1;
    for (j = 0; j < itmp1 ; ++j) {
      if ( fscanf(fp, "%i", &itmp2) != 1 ) {
	printf("File %s line %i: encountered error while reading facets in OVM file, "
	       "aborting ...\n", __FILE__, __LINE__);
	exit(1);
      }
    }
  }

  /* Reposition file pointer */
  fseek( fp, size, SEEK_SET );

  dMesh->f2he = (int *) malloc ((dMesh->f2heDim[dMesh->Nfc]) * sizeof(int));

  itmp2 = 0;
  for (i = 0; i < dMesh->Nfc; ++i) {
    if ( fscanf(fp, "%i", &itmp1) != 1 ) {
      printf("File %s line %i: encountered error while reading facets in OVM file, "
	     "aborting ...\n", __FILE__, __LINE__);
      exit(1);
    }
    for (j = 0; j < itmp1; ++j, ++itmp2) {
      if ( fscanf(fp, "%i", dMesh->f2he + itmp2) != 1 ) {
	printf("File %s line %i: encountered error while reading facets in OVM file, "
	       "aborting ...\n", __FILE__, __LINE__);
	exit(1);
      }
    }
  }

  /*
   * Read Polyhedra section
   */

  if ( fgets(line, 256, fp) == NULL ) {
    printf("File %s line %i: encountered error while reading polyhedra section in OVM file, "
	   "aborting ...\n", __FILE__, __LINE__);
    exit(1);
  } else if ( fgets(line, 256, fp) == NULL ) {
    printf("File %s line %i: encountered error while reading polyhedra section in OVM file, "
	   "aborting ...\n", __FILE__, __LINE__);
    exit(1);
  }

  if ( fscanf(fp, "%i", &(dMesh->Npol)) != 1 ) {
    printf("File %s line %i: encountered error while reading number of polyhedra in OVM file, "
	   "aborting ...\n", __FILE__, __LINE__);
    exit(1);
  }

  /* Store file position */
  size = ftell(fp);

  dMesh->p2hfDim = (int *) malloc ((dMesh->Npol+1) * sizeof(int));
  dMesh->p2hfDim[0] = 0;
  for (i = 0; i < dMesh->Npol; ++i) {
    if ( fscanf(fp, "%i", &itmp1) != 1 ) {
      printf("File %s line %i: encountered error while reading polyhedra in OVM file, "
	     "aborting ...\n", __FILE__, __LINE__);
      exit(1);
    }
    dMesh->p2hfDim[i+1] = dMesh->p2hfDim[i] + itmp1;
    for (j = 0; j < itmp1 ; ++j) {
      if ( fscanf(fp, "%i", &itmp2) != 1 ) {
	printf("File %s line %i: encountered error while reading polyhedra in OVM file, "
	       "aborting ...\n", __FILE__, __LINE__);
	exit(1);
      }
    }
  }

  /* Reposition file pointer */
  fseek( fp, size, SEEK_SET );

  dMesh->p2hf = (int *) malloc ((dMesh->p2hfDim[dMesh->Npol]) * sizeof(int));

  itmp2 = 0;
  for (i = 0; i < dMesh->Npol; ++i) {
    if ( fscanf(fp, "%i", &itmp1) != 1 ) {
      printf("File %s line %i: encountered error while reading polyhedra in OVM file, "
	     "aborting ...\n", __FILE__, __LINE__);
      exit(1);
    }
    for (j = 0; j < itmp1; ++j, ++itmp2) {
      if ( fscanf(fp, "%i", dMesh->p2hf + itmp2) != 1 ) {
	printf("File %s line %i: encountered error while reading polyhedra in OVM file, "
	       "aborting ...\n", __FILE__, __LINE__);
	exit(1);
      }
    }
  }

  fclose(fp);
}


void dualMeshSaveOVM( const char * const filename,
		      const dualMesh * const dMesh )
{
  FILE *fp = NULL;
  int i, j, patchSize;

  fp = fopen(filename, "w");
  fprintf(fp, "OVM ASCII\n");

  /*
   * Vertices
   */

  fprintf(fp, "Vertices\n");
  fprintf(fp, "%i\n", dMesh->Nver);
  for (i = 0; i < dMesh->Nver; ++i)
    fprintf(fp, "%.16f %.16f %.16f\n",
	    dMesh->vertices[3*i], dMesh->vertices[3*i+1], dMesh->vertices[3*i+2]);

  /*
   * Edges
   */

  fprintf(fp, "Edges\n");
  fprintf(fp, "%i\n", dMesh->Ned);
  for (i = 0; i < dMesh->Ned; ++i)
    fprintf(fp, "%i %i\n",
	    dMesh->edges[2*i], dMesh->edges[2*i+1]);

  /*
   * Faces
   */

  fprintf(fp, "Faces\n");
  fprintf(fp, "%i\n", dMesh->Nfc);
  for (i = 0; i < dMesh->Nfc; ++i) {
    patchSize = dMesh->f2heDim[i+1] - dMesh->f2heDim[i];
    fprintf(fp, "%i", patchSize);
    for (j = 0; j < patchSize; ++j)
      fprintf(fp, " %i", dMesh->f2he[ dMesh->f2heDim[i] + j ] );
    fprintf(fp, "\n");
  }

  /*
   * Polyhedra
   */

  fprintf(fp, "Polyhedra\n");
  fprintf(fp, "%i\n", dMesh->Npol);
  for (i = 0; i < dMesh->Npol; ++i) {
    patchSize = dMesh->p2hfDim[i+1] - dMesh->p2hfDim[i];
    fprintf(fp, "%i", patchSize);
    for (j = 0; j < patchSize; ++j )
      fprintf(fp, " %i", dMesh->p2hf[ dMesh->p2hfDim[i] + j ] );
    fprintf(fp, "\n");
  }

  fclose(fp);
}


void vertex2PolygonConnectivity( dualMesh2d * dMesh2d )
{
  int i, j, NverLoc, vIdx;

  intNode ** v2pListPointers = NULL;
  intNode *v2pListCursor = NULL, *v2pListTmp = NULL;

  dMesh2d->v2pDim = (int *) calloc (dMesh2d->Nver+1, sizeof(int));

  v2pListPointers = (intNode **) malloc ((dMesh2d->Nver) * sizeof(intNode *));
  for (i = 0; i < dMesh2d->Nver; ++i) v2pListPointers[i] = NULL;

  for (i = 0; i < dMesh2d->Npol; ++i) {
    NverLoc = dMesh2d->p2vDim[i+1] - dMesh2d->p2vDim[i];
    for (j = 0; j < NverLoc; ++j) {
      vIdx          = dMesh2d->p2v[ dMesh2d->p2vDim[i] + j ];
      v2pListCursor = v2pListPointers[ vIdx ];
      if ( v2pListCursor == NULL ) {
	v2pListPointers[vIdx] = intNodeCreate(i, NULL);
	dMesh2d->v2pDim[vIdx+1] = 1;
      } else {
	while ( ( v2pListCursor->value != i ) && ( v2pListCursor->next != NULL ) ) {
	  v2pListCursor = v2pListCursor->next;
	}
	if ( v2pListCursor->value != i ) {
	  v2pListCursor->next = intNodeCreate(i, NULL);
	  ++(dMesh2d->v2pDim[vIdx+1]);
	}
      }
    }
  }

  for (i = 0; i < dMesh2d->Nver; ++i) dMesh2d->v2pDim[i+1] += dMesh2d->v2pDim[i];

  dMesh2d->v2p = (int *) malloc ((dMesh2d->v2pDim[dMesh2d->Nver]) * sizeof(int));

  vIdx = 0;
  for (i = 0; i < dMesh2d->Nver; ++i) {
    v2pListCursor = v2pListPointers[i];
    while ( v2pListCursor != NULL ) {
      dMesh2d->v2p[vIdx++] = v2pListCursor->value;
      v2pListTmp = v2pListCursor->next;
      free( v2pListCursor );
      v2pListCursor = v2pListTmp;
    }
  }
  free( v2pListPointers );
}


void findBndVertices( dualMesh2d * dMesh2d )
{
  int i;

  for (i = 0; i < dMesh2d->Ned; ++i) {
    if ( dMesh2d->e2p[2*i+1] == -1 ) {
      dMesh2d->vBndMarkers[ dMesh2d->edges[2*i] ]   = 1;
      dMesh2d->vBndMarkers[ dMesh2d->edges[2*i+1] ] = 1;
    }
  }
}


void edgeConnectivity( dualMesh2d * dMesh2d )
{
  int i, j, itmp1, itmp2, NedMax, NedLoc, counter;
  int **buffer = NULL;

  NedMax = dMesh2d->p2vDim[ dMesh2d->Npol ];
  buffer = (int **) malloc ( NedMax * sizeof(int *) );
  for (i = 0; i < NedMax; ++i) buffer[i] = (int *) malloc (3 * sizeof(int));

  counter = 0;
  for (i = 0; i < dMesh2d->Npol; ++i) {
    NedLoc = dMesh2d->p2vDim[i+1] - dMesh2d->p2vDim[i];
    for (j = 0; j < NedLoc; ++j, ++counter) {
      itmp1 = dMesh2d->p2v[ dMesh2d->p2vDim[i] + j ];
      itmp2 = dMesh2d->p2v[ dMesh2d->p2vDim[i] + (j+1)%NedLoc ];
      if ( itmp1 < itmp2 ) {
	buffer[counter][0] = itmp1;
	buffer[counter][1] = itmp2;
      } else {
	buffer[counter][0] = itmp2;
	buffer[counter][1] = itmp1;
      }
      buffer[counter][2] = counter;
    }
  }

  qsort( buffer, counter, sizeof(int *), compareSize2 );

  /* Count the number of distinct edges */
  counter = 1;
  for (i = 1; i < NedMax; ++i)
    if ( ( buffer[i][0] != buffer[i-1][0] ) || ( buffer[i][1] != buffer[i-1][1] ) ) ++counter;

  /* Fill Ned, edges, p2e, p2eDim */
  dMesh2d->Ned    = counter;
  dMesh2d->edges  = (int *) malloc (2 * dMesh2d->Ned * sizeof(int));
  dMesh2d->p2e    = (int *) malloc (NedMax * sizeof(int));
  dMesh2d->p2eDim = (int *) malloc ((dMesh2d->Npol + 1) * sizeof(int));
  memcpy( dMesh2d->p2eDim, dMesh2d->p2vDim, (dMesh2d->Npol + 1) * sizeof(int) );

  counter                    = 0;
  dMesh2d->edges[0]          = buffer[0][0];
  dMesh2d->edges[1]          = buffer[0][1];
  dMesh2d->p2e[buffer[0][2]] = counter;
  for (i = 1; i < NedMax; ++i) {
    if ( ( buffer[i][0] != buffer[i-1][0] ) || ( buffer[i][1] != buffer[i-1][1] ) ) {
      ++counter;
      dMesh2d->edges[2*counter]   = buffer[i][0];
      dMesh2d->edges[2*counter+1] = buffer[i][1];
    }
    dMesh2d->p2e[buffer[i][2]] = counter;
  }

  /* Fill e2p */
  dMesh2d->e2p = (int *) malloc (2 * (dMesh2d->Ned) * sizeof(int));
  memset( dMesh2d->e2p, -1, 2 * (dMesh2d->Ned) * sizeof(int) );

  for (i = 0; i < dMesh2d->Npol; ++i) {
    NedLoc = dMesh2d->p2eDim[i+1] - dMesh2d->p2eDim[i];
    for (j = 0; j < NedLoc; ++j) {
      itmp1 = dMesh2d->p2e[ dMesh2d->p2eDim[i] + j ];
      if ( dMesh2d->e2p[2*itmp1] == -1 )
	dMesh2d->e2p[2*itmp1]   = i;
      else
	dMesh2d->e2p[2*itmp1+1] = i;
    }
  }

  for (i = 0; i < NedMax; ++i) free( buffer[i] );
  free( buffer );
}


void vertex2PolyhedronConnectivity( dualMesh * dMesh )
{
  int i, j, k, Nfc, Ned, itmp;
  int hFace, tFace, hEdge, tEdge;

  intNode ** v2pListPointers = NULL;
  intNode *v2pListCursor = NULL, *v2pListTmp = NULL;

  dMesh->v2pDim = (int *) calloc (dMesh->Nver+1, sizeof(int));

  v2pListPointers = (intNode **) malloc ((dMesh->Nver) * sizeof(intNode *));
  for (i = 0; i < dMesh->Nver; ++i) v2pListPointers[i] = NULL;

  for (i = 0; i < dMesh->Npol; ++i) {
    Nfc = dMesh->p2hfDim[i+1] - dMesh->p2hfDim[i];
    for (j = 0; j < Nfc; ++j) {
      hFace = dMesh->p2hf[ dMesh->p2hfDim[i] + j ];
      tFace = hFace/2;
      Ned   = dMesh->f2heDim[ tFace + 1 ] - dMesh->f2heDim[ tFace ];

      for (k = 0; k < Ned; ++k) {
	hEdge = dMesh->f2he[ dMesh->f2heDim[tFace] + (Ned - 1 - k)*(hFace%2) + k*(1 - hFace%2) ];
	tEdge = hEdge/2;
	itmp  = dMesh->edges[ 2 * tEdge + (1 - hEdge%2)*(hFace%2) + (hEdge%2)*(1 - hFace%2) ];
	v2pListCursor = v2pListPointers[itmp];
	if ( v2pListCursor == NULL ) {
	  v2pListPointers[itmp] = intNodeCreate(i, NULL);
	  dMesh->v2pDim[itmp+1] = 1;
	} else {
	  while ( ( v2pListCursor->value != i ) && ( v2pListCursor->next != NULL ) ) {
	    v2pListCursor = v2pListCursor->next;
	  }
	  if ( v2pListCursor->value != i ) {
	    v2pListCursor->next = intNodeCreate(i, NULL);
	    ++(dMesh->v2pDim[itmp+1]);
	  }
	}
      }
    }
  }

  for (i = 0; i < dMesh->Nver; ++i) dMesh->v2pDim[i+1] += dMesh->v2pDim[i];

  dMesh->v2p = (int *) malloc ((dMesh->v2pDim[dMesh->Nver]) * sizeof(int));

  itmp = 0;
  for (i = 0; i < dMesh->Nver; ++i) {
    v2pListCursor = v2pListPointers[i];
    while ( v2pListCursor != NULL ) {
      dMesh->v2p[itmp++] = v2pListCursor->value;
      v2pListTmp = v2pListCursor->next;
      free( v2pListCursor );
      v2pListCursor = v2pListTmp;
    }
  }
  free( v2pListPointers );
}


void vertex2FaceConnectivity( dualMesh * dMesh )
{
  int i, j, Ned, itmp;
  int hEdge, tEdge;

  intNode ** v2fListPointers = NULL;
  intNode *v2fListCursor = NULL, *v2fListTmp = NULL;

  dMesh->v2fDim = (int *) calloc (dMesh->Nver+1, sizeof(int));

  v2fListPointers = (intNode **) malloc ((dMesh->Nver) * sizeof(intNode *));
  for (i = 0; i < dMesh->Nver; ++i) v2fListPointers[i] = NULL;

  for (i = 0; i < dMesh->Nfc; ++i) {
    Ned   = dMesh->f2heDim[i+1] - dMesh->f2heDim[i];
    for (j = 0; j < Ned; ++j) {
      hEdge = dMesh->f2he[ dMesh->f2heDim[i] + j ];
      tEdge = hEdge/2;
      itmp  = dMesh->edges[ 2 * tEdge + hEdge%2 ];
      v2fListCursor = v2fListPointers[itmp];
      if ( v2fListCursor == NULL ) {
	v2fListPointers[itmp] = intNodeCreate(i, NULL);
	dMesh->v2fDim[itmp+1] = 1;
      } else {
	while ( ( v2fListCursor->value != i ) && ( v2fListCursor->next != NULL ) ) {
	  v2fListCursor = v2fListCursor->next;
	}
	if ( v2fListCursor->value != i ) {
	  v2fListCursor->next = intNodeCreate(i, NULL);
	  ++(dMesh->v2fDim[itmp+1]);
	}
      }
    }
  }

  for (i = 0; i < dMesh->Nver; ++i) dMesh->v2fDim[i+1] += dMesh->v2fDim[i];

  dMesh->v2f = (int *) malloc ((dMesh->v2fDim[dMesh->Nver]) * sizeof(int));

  itmp = 0;
  for (i = 0; i < dMesh->Nver; ++i) {
    v2fListCursor = v2fListPointers[i];
    while ( v2fListCursor != NULL ) {
      dMesh->v2f[itmp++] = v2fListCursor->value;
      v2fListTmp = v2fListCursor->next;
      free( v2fListCursor );
      v2fListCursor = v2fListTmp;
    }
  }
  free( v2fListPointers );
}


void face2PolyhedronConnectivity( dualMesh * dMesh )
{
  int i, j, Nfc, hFace, tFace, itmp;

  dMesh->f2p = (int *) malloc ( 2 * (dMesh->Nfc) * sizeof(int) );
  memset( dMesh->f2p, -1, 2 * (dMesh->Nfc) * sizeof(int) );

  for (i = 0; i < dMesh->Npol; ++i) {
    Nfc = dMesh->p2hfDim[i+1] - dMesh->p2hfDim[i];
    for (j = 0; j < Nfc; ++j) {
      hFace = dMesh->p2hf[ dMesh->p2hfDim[i] + j ];
      tFace = hFace/2;
      itmp  = dMesh->f2p[ 2*tFace ];
      if ( itmp == -1 )
	dMesh->f2p[ 2*tFace ]   = i;
      else
	dMesh->f2p[ 2*tFace+1 ] = i;
    }
  }
}


void dualMeshConstructConnectivity( dualMesh * dMesh )
{
  /* Vertex to faces */
  if ( dMesh->v2f == NULL ) vertex2FaceConnectivity( dMesh );

  /* Vertex to polyhedra */
  if ( dMesh->v2p == NULL ) vertex2PolyhedronConnectivity( dMesh );

  /* Face to polyhedra */
  if ( dMesh->f2p == NULL ) face2PolyhedronConnectivity( dMesh );
}
