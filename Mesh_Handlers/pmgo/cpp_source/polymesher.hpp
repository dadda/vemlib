#ifndef __POLYMESHER_HPP
#define __POLYMESHER_HPP

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <sys/wait.h>
#include <float.h>

#include <boost/config.hpp>
#include <vector>
#include <iostream>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/cuthill_mckee_ordering.hpp>
#include <boost/graph/properties.hpp>
#include <boost/graph/bandwidth.hpp>

using namespace std;
using namespace boost;

typedef adjacency_list<vecS, vecS, undirectedS, 
		       property<vertex_color_t, default_color_type,
				property<vertex_degree_t,int> > > Graph;
typedef graph_traits<Graph>::vertex_descriptor Vertex;
typedef graph_traits<Graph>::vertices_size_type size_type;
typedef std::pair<std::size_t, std::size_t> Pair;

typedef struct polygon {
  int Nver;
  int * vertices;
  struct polygon * next;
} polygon;

typedef struct neighbor {
  int vertexIdx;
  struct neighbor * next;
} neighbor;

int compareSize1( const void * a, const void * b );

int compareSize1double( const void * a, const void * b );

void unique( const int * const data,
	     const int M,
	     int ** iA,
	     int * iAlength,
	     int ** iC );

polygon * polygonCreate( const int Nver,
			 const int * const vertices,
			 polygon * const next );

neighbor * neighborCreate( const int idx,
			   neighbor * const next );

void rectDist( const double x,
	       const double y,
	       const double * const param,
	       double * Dist,
	       double * gradDist );

void sectorDist( const double x,
		 const double y,
		 const double * const param,
		 double * Dist,
		 double * gradDist );

void circleDist( const double x,
		 const double y,
		 const double * const param,
		 double * Dist,
		 double * gradDist );

void PolyMesher_RndPtSet( double * rndPoints,
			  const int N,
			  double const xmin,
			  double const xmax,
			  double const ymin,
			  double const ymax,
			  const char * domain,
			  const double * const param );

void PolyMesher_Rflct( const double alpha,
		       const double * const points,
		       const int M,
		       const char * domain,
		       const double * const param,
		       double * rflctPoints,
		       int * N );

void PolyMesher( const char * domain,
		 const double * const param,
		 const double area,
		 const double xmin,
		 const double xmax,
		 const double ymin,
		 const double ymax,
		 const int NElem,
		 const double tolCollapsePt,
		 double ** Nodes,
		 int * numNodes,
		 polygon ** Elements,
		 int * numElements );

void computeStatistics( const double * const Nodes,
			polygon * const Elements,
			double * totAreaFinal,
			double * minAreaFinal,
			double * hminFinal );

void writeOFFmesh( const double * const Nodes,
		   const int numNodes,
		   polygon * const Elements,
		   const int numElements,
		   const char * const filename );

void meshDestroy( double * Nodes,
                  polygon * Elements );

#endif
