/*
 * Version 0.0: rectangular boundary assumed, no Lloyd's optimization
 */

#include "polymesher.hpp"

int compareSize1( const void * a, const void * b )
{
  int *rowA, *rowB;
  rowA = *(int **)a;
  rowB = *(int **)b;
  return ( rowA[0] - rowB[0] );
}


int compareSize1double( const void * a, const void * b )
{
  double *rowA, *rowB;
  rowA = *(double **)a;
  rowB = *(double **)b;
  if ( rowA[0] < rowB[0] ) return -1;
  if ( rowA[0] > rowB[0] ) return 1;
  return 0;
}


void unique( const int * const data,
	     const int M,
	     int ** iA,
	     int * iAlength,
	     int ** iC )
{
  int i, lastUnique;
  int **sorted1, **sorted2;

  sorted1 = (int **) malloc (M * sizeof(int *));
  sorted2 = (int **) malloc (M * sizeof(int *));
  for (i = 0; i < M; ++i) {
    sorted1[i] = (int *) malloc (2 * sizeof(int));
    sorted1[i][0] = data[i];
    sorted1[i][1] = i;
    
    sorted2[i] = (int *) malloc (2 * sizeof(int));
  }

  qsort( sorted1, M, sizeof(int *), compareSize1 );

  /* Count unique elements */
  *iAlength = 1;
  for (i = 1; i < M; ++i) if ( sorted1[i][0] != sorted1[i-1][0] ) ++(*iAlength);

  /* Build iA */
  *iA        = (int *) malloc ((*iAlength) * sizeof(int));
  (*iA)[0]   = sorted1[0][1];
  lastUnique = 0;
  sorted2[0][0] = sorted1[0][1];
  sorted2[0][1] = lastUnique;

  for (i = 1; i < M; ++i) {
    if ( sorted1[i][0] != sorted1[i-1][0] ) {
      ++lastUnique;
      (*iA)[lastUnique] = sorted1[i][1];
    }
    sorted2[i][0] = sorted1[i][1];
    sorted2[i][1] = lastUnique;
  }

  /* Build iC */
  *iC = (int *) malloc (M * sizeof(int));

  qsort( sorted2, M, sizeof(int *), compareSize1 );
  for (i = 0; i < M; ++i) (*iC)[i] = sorted2[i][1];
  
  for (i = 0; i < M; ++i) { free( sorted1[i] ); free( sorted2[i] ); }
  free( sorted1 );
  free( sorted2 );
}


polygon * polygonCreate( const int Nver,
			 const int * const vertices,
			 polygon * const next )
{
  polygon * mypoly = (polygon *) malloc (sizeof(polygon));
  if ( mypoly == NULL ) {
    printf("File %s, line %i: error while creating a new polygon.\n", __FILE__, __LINE__);
    exit(1);
  }
  mypoly->Nver     = Nver;
  mypoly->vertices = (int *) malloc ( (mypoly->Nver) * sizeof(int) );
  memcpy( mypoly->vertices, vertices, (mypoly->Nver)*sizeof(int) );
  mypoly->next     = next;

  return mypoly;
}


neighbor * neighborCreate( const int idx,
			   neighbor * const next )
{
  neighbor * myneighbor = (neighbor *) malloc (sizeof(neighbor));
  if ( myneighbor == NULL ) {
    printf("File %s, line %i: error while creating a new neighbor struct.\n", __FILE__, __LINE__);
    exit(1);
  }
  myneighbor->vertexIdx = idx;
  myneighbor->next      = next;

  return myneighbor;
}


void rectDist( const double x,
	       const double y,
	       const double * const param,
	       double * Dist,
	       double * gradDist )
{
  double xmin, xmax, ymin, ymax;

  xmin = param[1]; xmax = param[2];
  ymin = param[3]; ymax = param[4];

  Dist[0] = x - xmax;
  gradDist[0] = 1.0;
  gradDist[1] = 0.0;

  Dist[1] = xmin - x;
  gradDist[2] = -1.0;
  gradDist[3] = 0.0;
  
  Dist[2] = y - ymax;
  gradDist[4] = 0.0;
  gradDist[5] = 1.0;
  
  Dist[3] = ymin - y;
  gradDist[6] = 0.0;
  gradDist[7] = -1.0;
}


void sectorDist( const double x,
		 const double y,
		 const double * const param,
		 double * Dist,
		 double * gradDist )
{
  double xmin, xc, yc, radius, xxc, yyc, dist;

  xmin   = param[1];
  xc     = param[2];
  yc     = param[3];
  radius = param[4];

  Dist[0]     = xmin - x;
  gradDist[0] = -1.0;
  gradDist[1] = 0.0;

  xxc  = x - xc;
  yyc  = y - yc;
  dist = sqrt( xxc*xxc + yyc*yyc );

  Dist[1]     = dist - radius;
  gradDist[2] = xxc / dist;
  gradDist[3] = yyc / dist;
}


void circleDist( const double x,
		 const double y,
		 const double * const param,
		 double * Dist,
		 double * gradDist )
{
  double xc, yc, radius, xxc, yyc, dist;

  xc     = param[1];
  yc     = param[2];
  radius = param[3];

  xxc  = x - xc;
  yyc  = y - yc;
  dist = sqrt( xxc*xxc + yyc*yyc );

  Dist[0]     = dist - radius;
  gradDist[0] = xxc / dist;
  gradDist[1] = yyc / dist;
}


void quarterDist( const double x,
		  const double y,
		  const double * const param,
		  double * Dist,
		  double * gradDist )
{
  double xleft, ybottom, xc, yc, radius, xxc, yyc, dist;

  ybottom     = param[1];
  Dist[0]     = ybottom - y;
  gradDist[0] = 0.0;
  gradDist[1] = -1.0;

  xc     = param[2];
  yc     = param[3];
  radius = param[4];

  xxc  = x - xc;
  yyc  = y - yc;
  dist = sqrt( xxc*xxc + yyc*yyc );

  Dist[1]     = dist - radius;
  gradDist[2] = xxc / dist;
  gradDist[3] = yyc / dist;

  xleft       = param[5];
  Dist[2]     = xleft - x;
  gradDist[4] = -1.0;
  gradDist[5] = 0.0;

  xc     = param[6];
  yc     = param[7];
  radius = param[8];

  xxc  = x - xc;
  yyc  = y - yc;
  dist = sqrt( xxc*xxc + yyc*yyc );

  Dist[3]     = radius - dist;
  gradDist[6] = - xxc / dist;
  gradDist[7] = - yyc / dist;
}


void PolyMesher_RndPtSet( double * rndPoints,
			  const int N,
			  double const xmin,
			  double const xmax,
			  double const ymin,
			  double const ymax,
			  const char * domain,
			  const double * const param )
{
  int i, j, outside;
  double point[2];
  int Nbnd;
  double *Dist, *gradDist;

  Nbnd     = (int)param[0];
  Dist     = (double *) malloc (Nbnd * sizeof(double));
  gradDist = (double *) malloc (2 * Nbnd * sizeof(double));

  for (i = 0; i < N; ++i) {
    outside = 1;
    while ( outside ) {
      point[0] = xmin + ( (double)rand() / RAND_MAX ) * (xmax-xmin);
      point[1] = ymin + ( (double)rand() / RAND_MAX ) * (ymax-ymin);
      if ( strcmp( domain, "rectangle" ) == 0 ) {
	rectDist( point[0], point[1], param, Dist, gradDist );
      } else if ( strcmp( domain, "sector" ) == 0 ) {
	sectorDist( point[0], point[1], param, Dist, gradDist );
      } else if ( strcmp( domain, "circle" ) == 0 ) {
	circleDist( point[0], point[1], param, Dist, gradDist );
      } else if ( strcmp( domain, "quarter" ) == 0 ) {
	quarterDist( point[0], point[1], param, Dist, gradDist );
      }
      outside = 0;
      for (j = 0; j < Nbnd; ++j)
	if ( Dist[j] > 0.0 ) {
	  outside = 1;
	  break;
	}
    }
    rndPoints[2*i]   = point[0];
    rndPoints[2*i+1] = point[1];
  }

  free( Dist );
  free( gradDist );
}


void PolyMesher_Rflct( const double alpha,
		       const double * const points,
		       const int M,
		       const char * domain,
		       const double * const param,
		       double * rflctPoints,
		       int * N )
{
  int i, j;
  int Nbnd;
  double *Dist, *gradDist;

  Nbnd     = (int)param[0];
  Dist     = (double *) malloc (Nbnd * sizeof(double));
  gradDist = (double *) malloc (2 * Nbnd * sizeof(double));

  *N = 0;
  for (i = 0; i < M; ++i) {
    if ( strcmp( domain, "rectangle" ) == 0 ) {
      rectDist( points[2*i], points[2*i+1], param, Dist, gradDist );
    } else if ( strcmp( domain, "sector" ) == 0 ) {
      sectorDist( points[2*i], points[2*i+1], param, Dist, gradDist );
    } else if ( strcmp( domain, "circle" ) == 0 ) {
      circleDist( points[2*i], points[2*i+1], param, Dist, gradDist );
    } else if ( strcmp( domain, "quarter" ) == 0 ) {
      quarterDist( points[2*i], points[2*i+1], param, Dist, gradDist );
    }
    for (j = 0; j < Nbnd; ++j) {
      if ( fabs(Dist[j]) < alpha ) {
	rflctPoints[2 * (*N)]     =
	  points[2*i] - 2.0 * Dist[j] * gradDist[2*j];
	rflctPoints[2 * (*N) + 1] =
	  points[2*i+1] - 2.0 * Dist[j] * gradDist[2*j+1];
	++(*N);
      }
    }
  }

  free( Dist );
  free( gradDist );
}


void PolyMesher( const char * domain,
		 const double * const param,
		 const double area,
		 const double xmin,
		 const double xmax,
		 const double ymin,
		 const double ymax,
		 const int NElem,
		 const double tolCollapsePt,
		 double ** Nodes,
		 int * numNodes,
		 polygon ** Elements,
		 int * numElements )
{
  int i, j, k, N, vNver, vNelt, itmp, status, itmp1, itmp2;
  int *itmpV = NULL, *itmpV2 = NULL;
  
  double *oldPoints = NULL, *newPoints = NULL;
  double **buffer   = NULL;
  double centroid[2];
  double c        = 1.5;
  double alpha;
  double tmp1, tmp2;
  double tolCollapsePtSq = tolCollapsePt*tolCollapsePt;

  FILE *fp = NULL;
  polygon *polygonListHead = NULL, *currentPolygon  = NULL, *tmpPolygon = NULL;
  neighbor **neighborPointers = NULL;
  neighbor *neighborCursor = NULL, *neighborTmp = NULL;

  Pair * edges;
  
  oldPoints = (double *) malloc (2 * NElem * sizeof(double));
  newPoints = (double *) malloc (2 * NElem * 4 * sizeof(double));
  
  srand( time(NULL) );
  PolyMesher_RndPtSet( oldPoints, NElem, xmin, xmax, ymin, ymax, domain, param );

  alpha = c * sqrt( area / (double)NElem );

  PolyMesher_Rflct( alpha, oldPoints, NElem, domain, param, newPoints, &N );

  /* Call qhull */

  fp = fopen("./__tmp__.in", "w"); /* Write points to a temporary file */
  fprintf(fp, "2\n");           /* Dimension */
  fprintf(fp, "%i\n", NElem+N); /* Number of points */
  for (i = 0; i < NElem; ++i) fprintf(fp, "%.16f %.16f\n", oldPoints[2*i], oldPoints[2*i+1]);
  for (i = 0; i < N; ++i)     fprintf(fp, "%.16f %.16f\n", newPoints[2*i], newPoints[2*i+1]);
  fclose(fp);
  status = system("qvoronoi o < ./__tmp__.in > ./__tmp__.out");
  if ( !WIFEXITED(status) ) {
    printf("File %s line %i: error running qhull, aborting ...\n", __FILE__, __LINE__);
    exit(1);
  }
  free( oldPoints );
  free( newPoints );
  
  /* Read qhull output */

  fp = fopen("./__tmp__.out", "r");
  fscanf(fp, "%i", &itmp);
  fscanf(fp, "%i %i %i", &vNver, &vNelt, &itmp);

  /* Read nodes */
  oldPoints = (double *) malloc ( 2 * vNver * sizeof(double) );
  for (i = 0; i < vNver; ++i) fscanf(fp, "%lf %lf", oldPoints+2*i, oldPoints+2*i+1);

  /* Read elements */

  /* Read first element */
  fscanf(fp, "%i", &itmp); itmpV = (int *) malloc ( itmp * sizeof(int) );
  for (j = 0; j < itmp; ++j) fscanf(fp, "%i", itmpV+j);
  polygonListHead = polygonCreate( itmp, itmpV, NULL );
  free( itmpV );

  currentPolygon = polygonListHead;
  
  /* Read following elements */
  for (i = 1; i < vNelt; ++i) {
    fscanf(fp, "%i", &itmp); itmpV = (int *) malloc ( itmp * sizeof(int) );
    for (j = 0; j < itmp; ++j) fscanf(fp, "%i", itmpV+j);
    currentPolygon->next = polygonCreate( itmp, itmpV, NULL );
    currentPolygon = currentPolygon->next;
    free( itmpV );
  }
  fclose(fp);
  
  /* Remove auxiliary files */
  status = system("rm ./__tmp__.in ./__tmp__.out");
  if ( !WIFEXITED(status) ) {
    printf("File %s line %i: error removing auxiliary files, aborting ...\n", __FILE__, __LINE__);
    exit(1);
  }

#ifdef DEBUG
  printf("\nPoints from qhull:\n");
  for (i = 0; i < vNver; ++i) printf("%i %.16f %.16f\n", i, oldPoints[2*i], oldPoints[2*i+1]);
    
  /* View polygon list */
  printf("\nPolygon list:\n");
  currentPolygon = polygonListHead;
  while( currentPolygon != NULL ) {
    printf("%i", currentPolygon->Nver);
    for (j = 0; j < currentPolygon->Nver; ++j)
      printf("   %i", currentPolygon->vertices[j]);
    printf("\n");
    currentPolygon = currentPolygon->next;
  }
#endif

  /* Extract first NElem elements */
  
  newPoints = (double *) malloc (2 * vNver * sizeof(double));
  itmpV     = (int *) malloc (vNver * sizeof(int));
  memset( itmpV, -1, vNver * sizeof(int) );

  itmp = 0; /* Keep track of the last inserted point */
  currentPolygon = polygonListHead;
  for (i = 0; i < NElem; ++i) {
    centroid[0] = 0.0; centroid[1] = 0.0;
    for (j = 0; j < currentPolygon->Nver; ++j) {
      if ( itmpV[ currentPolygon->vertices[j] ] == -1 ) {
	newPoints[2*itmp]   = oldPoints[ 2*(currentPolygon->vertices[j]) ];
	newPoints[2*itmp+1] = oldPoints[ 2*(currentPolygon->vertices[j])+1 ];
	itmpV[ currentPolygon->vertices[j] ] = itmp;
	++itmp;
      }
      currentPolygon->vertices[j] = itmpV[ currentPolygon->vertices[j] ];
      centroid[0] += newPoints[ 2 * (currentPolygon->vertices[j]) ];
      centroid[1] += newPoints[ 2 * (currentPolygon->vertices[j]) + 1 ];
    }
    centroid[0] /= (double)(currentPolygon->Nver);
    centroid[1] /= (double)(currentPolygon->Nver);

    /* Sort vertices in counter-clockwise order */
    buffer = (double **) malloc ((currentPolygon->Nver) * sizeof(double *));
    for (j = 0; j < currentPolygon->Nver; ++j) {
      buffer[j] = (double *) malloc (2 * sizeof(double));
      buffer[j][0] = atan2( newPoints[ 2 * (currentPolygon->vertices[j]) + 1 ] - centroid[1],
			    newPoints[ 2 * (currentPolygon->vertices[j]) ]     - centroid[0] );
      buffer[j][1] = (double)(currentPolygon->vertices[j]);
    }
    qsort( buffer, currentPolygon->Nver, sizeof(double *), compareSize1double );
    for (j = 0; j < currentPolygon->Nver; ++j) {
      currentPolygon->vertices[j] = buffer[j][1];
      free( buffer[j] );
    }
    free( buffer );
    currentPolygon = currentPolygon->next;
  }
  free( itmpV );
  free( oldPoints );
  
#ifdef DEBUG
  printf("\nSelected points:\n");
  for (i = 0; i < itmp; ++i) printf("%i %.16f %.16f\n", i, newPoints[2*i], newPoints[2*i+1]);
  
  /* View polygon list */
  printf("\nPolygon list:\n");
  currentPolygon = polygonListHead;
  for (i = 0; i < NElem; ++i) {
    printf("%i", currentPolygon->Nver);
    for (j = 0; j < currentPolygon->Nver; ++j)
      printf("   %i", currentPolygon->vertices[j]);
    printf("\n");
    currentPolygon = currentPolygon->next;
  }
#endif

  /* Remove small edges */

  /* Construct v2v connectivity */
  neighborPointers = (neighbor **) malloc (itmp * sizeof(neighbor *));
  for (i = 0; i < itmp; ++i) neighborPointers[i] = NULL;

  currentPolygon = polygonListHead;
  for (i = 0; i < NElem; ++i) {
    for (j = 0; j < currentPolygon->Nver; ++j) {
      for (k = 0; k < 2; ++k) { /* Create symmetric relations */
	neighborCursor = neighborPointers[ currentPolygon->vertices[(j+k)%(currentPolygon->Nver)] ];
	if ( neighborCursor == NULL ) {
	  neighborPointers[ currentPolygon->vertices[(j+k)%(currentPolygon->Nver)] ] =
	    neighborCreate( currentPolygon->vertices[(j+1-k)%(currentPolygon->Nver)], NULL );
	} else {
	  while ( ( neighborCursor->vertexIdx != currentPolygon->vertices[(j+1-k)%(currentPolygon->Nver)] ) &&
		  ( neighborCursor->next != NULL ) ){
	    neighborCursor = neighborCursor->next;
	  }
	  if ( neighborCursor->vertexIdx != currentPolygon->vertices[(j+1-k)%(currentPolygon->Nver)] ) {
	    neighborCursor->next = neighborCreate( currentPolygon->vertices[(j+1-k)%(currentPolygon->Nver)], NULL );
	  }
	}
      }
    }
    currentPolygon = currentPolygon->next;
  }

#ifdef DEBUG
  printf("\nPoint to point adjacency (must be symmetric):\n");
  printf("--------------------------------------------------\n     ");
  for (i = 0; i < itmp; ++i) printf("%3i|", i);
  printf("\n-------------------------------------------------------------------------\n");
    
  buffer = (double **) malloc (itmp * sizeof(double *));
  for (i = 0; i < itmp; ++i) {
    buffer[i] = (double *) malloc (itmp * sizeof(double));
    memset( buffer[i], 0.0, itmp * sizeof(double));
    
    neighborCursor = neighborPointers[i];
    while ( neighborCursor != NULL ) {
      buffer[i][neighborCursor->vertexIdx] = 1.0;
      neighborCursor = neighborCursor->next;
    }

    printf("%3i |", i);
    for (j = 0; j < itmp; ++j) if (buffer[i][j] == 1.0) printf(" * |"); else printf("   |");
    printf("\n-------------------------------------------------------------------------\n");

    free( buffer[i] );
  }
  free( buffer );
#endif
  
  /* Copy points */
  oldPoints = (double *) malloc (2 * itmp * sizeof(double));
  memcpy( oldPoints, newPoints, 2 * itmp * sizeof(double) );

  free( newPoints );
  newPoints = (double *) malloc (2 * itmp * sizeof(double));

  itmpV = (int *) malloc (itmp * sizeof(int));
  memset( itmpV, -1, itmp * sizeof(int) );

  currentPolygon = polygonListHead;
  *numNodes = 0; /* Count the number of not collapsing points */
  for (i = 0; i < NElem; ++i) {
    for (j = 0; j < currentPolygon->Nver; ++j) {
      if ( itmpV[ currentPolygon->vertices[j] ] == -1 ) {
	tmp1  = DBL_MAX;
	itmp1 = -1;
	neighborCursor = neighborPointers[ currentPolygon->vertices[j] ];
	while ( neighborCursor != NULL ) {
	  if ( itmpV[ neighborCursor->vertexIdx ] != -1 ) {
	    tmp2 = ( oldPoints[ 2 * (currentPolygon->vertices[j]) ]
		     - oldPoints[ 2 * (neighborCursor->vertexIdx) ] )
	      * ( oldPoints[ 2 * currentPolygon->vertices[j] ]
		  - oldPoints[ 2 * (neighborCursor->vertexIdx) ] )
	      + ( oldPoints[ 2 * (currentPolygon->vertices[j]) + 1 ]
		  - oldPoints[ 2 * (neighborCursor->vertexIdx) + 1 ] )
	      * ( oldPoints[ 2 * (currentPolygon->vertices[j]) + 1 ]
		  - oldPoints[ 2 * (neighborCursor->vertexIdx) + 1 ] );
	    if (tmp2 < tmp1 ) { tmp1 = tmp2; itmp1 = neighborCursor->vertexIdx; }
	  }
	  neighborCursor = neighborCursor->next;
	}
	if (tmp1 > tolCollapsePtSq) {
	  newPoints[2 * (*numNodes)]     = oldPoints[2 * currentPolygon->vertices[j]];
	  newPoints[2 * (*numNodes) + 1] = oldPoints[2 * currentPolygon->vertices[j] + 1];
	  itmpV[ currentPolygon->vertices[j] ] = *numNodes;
	  ++(*numNodes);
	} else {
	  itmpV[ currentPolygon->vertices[j] ] = itmpV[ itmp1 ];
	}
      }
    }
    currentPolygon = currentPolygon->next;
  }
  free( oldPoints );

  /* Load filtered edges */
  currentPolygon = polygonListHead;
  *numElements   = 0;
  *Elements      = NULL;
  for (i = 0; i < NElem; ++i) {
    itmp1 = currentPolygon->Nver;
    for (j = 0; j < currentPolygon->Nver; ++j)
      if ( itmpV[ currentPolygon->vertices[j] ] == itmpV[ currentPolygon->vertices[(j+1)%currentPolygon->Nver] ] )
	--itmp1;
    if ( itmp1 > 2 ) {
      itmpV2 = (int *) malloc (itmp1 * sizeof(int) );
      itmp2  = 0;
      for (j = 0; j < currentPolygon->Nver; ++j)
	if ( itmpV[ currentPolygon->vertices[j] ] != itmpV[ currentPolygon->vertices[(j+1)%currentPolygon->Nver] ] )
	  itmpV2[itmp2++] = itmpV[ currentPolygon->vertices[j] ];
      if ( (*Elements) == NULL ) {
	*Elements  = polygonCreate( itmp2, itmpV2, NULL );
	tmpPolygon = *Elements;
      } else {
	tmpPolygon->next = polygonCreate( itmp2, itmpV2, NULL );
	tmpPolygon       = tmpPolygon->next;
      }
      ++(*numElements);
      free( itmpV2 );
    }

    currentPolygon = currentPolygon->next;
  }
  free( itmpV );
  
  currentPolygon = polygonListHead;
  while( currentPolygon != NULL ) {
    tmpPolygon = currentPolygon->next;
    free( currentPolygon->vertices );
    free( currentPolygon );
    currentPolygon = tmpPolygon;
  }

  /* At this point, the list of surviving nodes is newPoints[0:2*(*numNodes)-1],
   * and the list of new elements starts with (*Elements) */

  /* Final stage: resequence node using the reverse Cuthill-McKee permutation */
  
  /* Release memory and build new symmetric adjacency list */
  for (i = 0; i < itmp; ++i) {
    neighborCursor = neighborPointers[i];
    while (neighborCursor != NULL) {
      neighborTmp = neighborCursor->next;
      free( neighborCursor );
      neighborCursor = neighborTmp;
    }
  }
  free( neighborPointers );

  neighborPointers = (neighbor **) malloc ((*numNodes) * sizeof(neighbor *));
  for (i = 0; i < (*numNodes); ++i) neighborPointers[i] = NULL;

  currentPolygon = *Elements;
  itmp = 0; /* Count number of edges */
  while( currentPolygon != NULL ) {
    for (j = 0; j < currentPolygon->Nver; ++j) {
      itmp1 = currentPolygon->vertices[j%(currentPolygon->Nver)];
      itmp2 = currentPolygon->vertices[(j+1)%(currentPolygon->Nver)];
      if (itmp1 > itmp2) { k = itmp1; itmp1 = itmp2; itmp2 = k; }
      neighborCursor = neighborPointers[ itmp1 ];
      if ( neighborCursor == NULL ) {
	neighborPointers[ itmp1 ] = neighborCreate( itmp2, NULL );
	++itmp;
      } else {
	while ( ( neighborCursor->vertexIdx != itmp2 ) &&
		( neighborCursor->next != NULL ) ){
	  neighborCursor = neighborCursor->next;
	}
	if ( neighborCursor->vertexIdx != itmp2 ) {
	  neighborCursor->next = neighborCreate( itmp2, NULL );
	  ++itmp;
	}
      }
    }
    currentPolygon = currentPolygon->next;
  }
  
  /* Reverse Cuthill-McKee permutation with boost library */
  edges = new Pair[itmp];
  itmp1 = 0; /* Edge counter */
  for (i = 0; i < (*numNodes); ++i) {
    neighborCursor = neighborPointers[i];
    while( neighborCursor != NULL ) {
      edges[itmp1++] = Pair(i, neighborCursor->vertexIdx);
      neighborCursor = neighborCursor->next;
    }
  }
  
  for (i = 0; i < (*numNodes); ++i) {
    neighborCursor = neighborPointers[i];
    while (neighborCursor != NULL) {
      neighborTmp = neighborCursor->next;
      free( neighborCursor );
      neighborCursor = neighborTmp;
    }
  }
  free( neighborPointers );
  
  Graph G(*numNodes);
  for (i = 0; i < itmp; ++i)
    add_edge(edges[i].first, edges[i].second, G);
  delete[] edges;

  graph_traits<Graph>::vertex_iterator ui, ui_end;

  property_map<Graph,vertex_degree_t>::type deg = get(vertex_degree, G);
  for (boost::tie(ui, ui_end) = vertices(G); ui != ui_end; ++ui)
    deg[*ui] = degree(*ui, G);

  property_map<Graph, vertex_index_t>::type
    index_map = get(vertex_index, G);

  std::vector<Vertex> inv_perm(num_vertices(G));
  std::vector<size_type> perm(num_vertices(G));

  //reverse cuthill_mckee_ordering
  cuthill_mckee_ordering(G, inv_perm.rbegin(), get(vertex_color, G),
			 make_degree_map(G));

  for (size_type c = 0; c != inv_perm.size(); ++c)
    perm[index_map[inv_perm[c]]] = c;
  
  *Nodes = (double *) malloc ( 2 * (*numNodes) * sizeof(double) );
  
  i = 0;
  for (std::vector<Vertex>::const_iterator l=inv_perm.begin(); l != inv_perm.end(); ++l, ++i) {
    (*Nodes)[2*i]   = fmin( fmax( newPoints[2 * (*l)], xmin ), xmax );
    (*Nodes)[2*i+1] = fmin( fmax( newPoints[2 * (*l) + 1], ymin ), ymax );
  }
  free( newPoints );
  
  /* perm is used to update Elements */
  
  currentPolygon = *Elements;
  while ( currentPolygon != NULL ) {
    for (j = 0; j < currentPolygon->Nver; ++j)
      currentPolygon->vertices[j] = perm[ currentPolygon->vertices[j] ];
    currentPolygon = currentPolygon->next;
  }
}


void computeStatistics( const double * const Nodes,
			polygon * const Elements,
			double * totAreaFinal,
			double * minAreaFinal,
			double * hminFinal )
{
  int j;
  double tmp1, tmp2;
  polygon * currentPolygon = NULL;
  
  *totAreaFinal = 0.0;
  *minAreaFinal = DBL_MAX;
  *hminFinal    = DBL_MAX;
  currentPolygon = Elements;
  while ( currentPolygon != NULL ) {
    /* Statistics */
    tmp1 = 0.0; /* Current area */
    for (j = 0; j < currentPolygon->Nver; ++j) {
      tmp1 +=
	( Nodes[2 * (currentPolygon->vertices[(j+1)%currentPolygon->Nver]) + 1 ]
	  - Nodes[2 * (currentPolygon->vertices[j%currentPolygon->Nver]) + 1 ] )
	* Nodes[2 * (currentPolygon->vertices[j%currentPolygon->Nver]) ]
	+ ( Nodes[2 * (currentPolygon->vertices[j%currentPolygon->Nver]) ]
	    - Nodes[2 * (currentPolygon->vertices[(j+1)%currentPolygon->Nver]) ] )
	* Nodes[2 * (currentPolygon->vertices[j%currentPolygon->Nver]) + 1];
      tmp2 =
	sqrt( ( Nodes[2 * (currentPolygon->vertices[j%currentPolygon->Nver])]
		- Nodes[2 * (currentPolygon->vertices[(j+1)%currentPolygon->Nver])] )
	      * ( Nodes[2 * (currentPolygon->vertices[j%currentPolygon->Nver])]
		  - Nodes[2 * (currentPolygon->vertices[(j+1)%currentPolygon->Nver])] )
	      +  ( Nodes[2 * (currentPolygon->vertices[j%currentPolygon->Nver]) + 1]
		   - Nodes[2 * (currentPolygon->vertices[(j+1)%currentPolygon->Nver]) + 1] )
	      * ( Nodes[2 * (currentPolygon->vertices[j%currentPolygon->Nver]) + 1]
		  - Nodes[2 * (currentPolygon->vertices[(j+1)%currentPolygon->Nver]) + 1] ) );
      *hminFinal = *hminFinal < tmp2 ? *hminFinal : tmp2;
    }
    *totAreaFinal += tmp1;
    *minAreaFinal = *minAreaFinal < tmp1 ? *minAreaFinal : tmp1;
    currentPolygon = currentPolygon->next;
  }
  *totAreaFinal /= 2.0;
  *minAreaFinal /= 2.0;
}


void writeOFFmesh( const double * const Nodes,
		   const int numNodes,
		   polygon * const Elements,
		   const int numElements,
		   const char * const filename )
{
  int i;
  FILE *fp = NULL;
  polygon * currentPolygon = NULL;
  
  fp = fopen(filename, "w");
  fprintf(fp, "OFF\n");
  fprintf(fp, "%i %i 0\n", numNodes, numElements);
  for (i = 0; i < numNodes; ++i)
    fprintf(fp, "%.16f %.16f 0\n", Nodes[2*i], Nodes[2*i+1]);

  currentPolygon = Elements;
  while ( currentPolygon != NULL ) {
    fprintf(fp, "%i", currentPolygon->Nver);
    for (i = 0; i < currentPolygon->Nver; ++i)
      fprintf(fp, "   %i", currentPolygon->vertices[i] );
    fprintf(fp, "\n");
    currentPolygon = currentPolygon->next;
  }
  
  fclose(fp);
}


void meshDestroy( double * Nodes,
		  polygon * Elements )
{
  polygon *currentPolygon = NULL, *tmpPolygon = NULL;
  
  free( Nodes );
  
  currentPolygon = Elements;
  while( currentPolygon != NULL ) {
    tmpPolygon = currentPolygon->next;
    free( currentPolygon->vertices );
    free( currentPolygon );
    currentPolygon = tmpPolygon;
  }
}
