#include "pmgo.h"


/*
 * Before using this function, the user MUST ensure that:
 *
 * 1. the array 'vertices' has been initialized with the initial coordinate array,
 *    otherwise those coordinates involved in the par2car conversion will have
 *    garbage values.
 *
 * 2. the same could happen if the array xRescaled is not inizialized with the non-scaled values
 *    contained in array 'x' in the case xIdx != NULL.
 */
double evalMeshMetrics( const double * const x,
			const int xDim,
			const int * const xIdx,
			const int xIdxDim,
			const short rescale,
			const double * const lbounds,
			const double * const ubounds,
			double * const xRescaled,
			double * const vertices,
			const int * const verticesIdx,
			const int verticesIdxDim,
			const int * const c2firstpIdx,
			const dualMesh * const dMesh,
			char** metricsArgv,
			double * const metricsValues )
{
  int i;
  double c, tmp, metricsVal;

  double normal[3];

  /*
   * Rescale and convert from the parametric to the cartesian space
   */

  if ( xIdx == NULL ) {
    if ( rescale ) {
      for (i = 0; i < xDim; ++i)
	if ( (lbounds[i] != -INF) && (ubounds[i] != INF) )
	  xRescaled[i] = (ubounds[i] - lbounds[i]) * x[i] + lbounds[i];
	else
	  xRescaled[i] = x[i];
      par2car( xRescaled, vertices, dMesh->Nver, verticesIdx, verticesIdxDim,
	       c2firstpIdx, dMesh->vBndMarkers );
    } else {
      par2car( x, vertices, dMesh->Nver, verticesIdx, verticesIdxDim,
	       c2firstpIdx, dMesh->vBndMarkers );
    }
  } else {
    if ( rescale ) {
      for (i = 0; i < xIdxDim; ++i) {
	if ( (lbounds[ xIdx[i] ] != -INF) && (ubounds[ xIdx[i] ] != INF) )
	  xRescaled[ xIdx[i] ] =
	    (ubounds[ xIdx[i] ] - lbounds[ xIdx[i] ]) * x[ xIdx[i] ] + lbounds[ xIdx[i] ];
	else
	  xRescaled[ xIdx[i] ] = x[ xIdx[i] ];
      }
      par2car( xRescaled, vertices, dMesh->Nver, verticesIdx, verticesIdxDim,
	       c2firstpIdx, dMesh->vBndMarkers );
    } else {
      par2car( x, vertices, dMesh->Nver, verticesIdx, verticesIdxDim,
	       c2firstpIdx, dMesh->vBndMarkers );
    }
  }

  /*
   * Evaluate a mesh metric
   */

   if ( strcmp( metricsArgv[0], "sqm" ) == 0 ) {

    /* Shape Quality Measure */

    c         = atof( metricsArgv[1] );
    metricsVal = 0.0;

#pragma omp parallel for default(none) private(i, tmp) firstprivate(c) \
  schedule(dynamic) reduction(+:metricsVal)
    for (i = 0; i < dMesh->Npol; ++i) {
      tmp = sqmPolyhedron( dMesh, vertices, c, i );
      if ( metricsValues != NULL ) metricsValues[i] = tmp;
      metricsVal += tmp;
    }
    metricsVal /= dMesh->Npol;
    
  } else if ( strcmp( metricsArgv[0], "leastSquares" ) == 0 ) {

    metricsVal = 0.0;

#pragma omp parallel for default(none) private(i, tmp, normal)	\
  schedule(dynamic) reduction(+:metricsVal)
    for (i = 0; i < dMesh->Nfc; ++i) {
      leastSquaresFace( dMesh, vertices, i, normal, &tmp);
      if ( metricsValues != NULL ) metricsValues[i] = tmp;
      metricsVal += tmp;
    }

  } else {
    metricsVal = INF;
  }

  return metricsVal;
}


void approxGradient( const short method,
		     const double * const x,
		     const int N,
		     const double * const vertices_at_x,
		     const short rescale,
		     const double * const lbounds,
		     const double * const ubounds,
		     const int * const p2cIdx,
		     const int * const c2firstpIdx,
		     const dualMesh * const dMesh,
		     char ** metricsArgv,
		     const double * const metricsValues,
		     double * const gradient )
{
  int i, j, NLoc, vertexIdx;
  double delta, metricOld, metricMinus, metricPlus, xShifted, tmp;
  double buffer[3];
  double *vertices = NULL, *xRescaled = NULL;

  delta = sqrt( DBL_EPSILON );

  if ( method == 0 ) { 

    /* Backward differences */

    for (i = 0; i < N; ++i)
      if ( rescale && ( lbounds[i] != -INF ) && ( ubounds[i] != INF ) ) {
	/* Only variables bounded from above and below have been scaled between [0,1] */
	if ( delta > x[i] ) delta = x[i];
      } else {
	if ( delta > ( x[i] - lbounds[i] ) ) delta = x[i] - lbounds[i];
      }
    
#pragma omp parallel default(none)					\
  shared(metricsArgv)							\
  private(i, j, NLoc, vertexIdx, metricOld, metricMinus, metricPlus)	\
  private(xShifted, xRescaled, vertices, buffer, tmp)		\
  firstprivate(delta)
    {
      xRescaled = (double *) malloc (N * sizeof(double));
      for (j = 0; j < N; ++j)
	if ( rescale && (lbounds[j] != -INF) && (ubounds[j] != INF) )
	  xRescaled[j] = (ubounds[j] - lbounds[j]) * x[j] + lbounds[j];
	else
	  xRescaled[j] = x[j];
    
      vertices = (double *) malloc (3 * (dMesh->Nver) * sizeof(double));
      memcpy( vertices, vertices_at_x, 3 * (dMesh->Nver) * sizeof(double) );
      
#pragma omp for schedule(dynamic)
      for (i = 0; i < N; ++i) {
	xShifted = x[i] - delta;
	if ( rescale && (lbounds[i] != -INF) && (ubounds[i] != INF) )
	  xRescaled[i] = (ubounds[i] - lbounds[i]) * xShifted + lbounds[i];
	else
	  xRescaled[i] = xShifted;
	par2car( xRescaled, vertices, dMesh->Nver, p2cIdx+i, 1, c2firstpIdx, dMesh->vBndMarkers);

	if ( strcmp(metricsArgv[0], "sqm") == 0 ) {
	  
	  vertexIdx = p2cIdx[i];
	  NLoc      = dMesh->v2pDim[vertexIdx+1] - dMesh->v2pDim[vertexIdx];
	  
	  metricOld   = 0.0;
	  metricMinus = 0.0;
	  for (j = 0; j < NLoc; ++j ) {
	    metricOld   += metricsValues[ dMesh->v2p[ dMesh->v2pDim[vertexIdx] + j ] ];
	    metricMinus += sqmPolyhedron( dMesh, vertices, atof( metricsArgv[1] ),
					  dMesh->v2p[ dMesh->v2pDim[vertexIdx] + j ] );
	  }
	  gradient[i] = ( metricOld - metricMinus ) / ( (dMesh->Npol) * delta );

	} else if ( strcmp(metricsArgv[0], "leastSquares") == 0 ) {
	  
	  vertexIdx = p2cIdx[i];
	  NLoc      = dMesh->v2fDim[vertexIdx+1] - dMesh->v2fDim[vertexIdx];
	  
	  metricOld   = 0.0;
	  metricMinus = 0.0;
	  for (j = 0; j < NLoc; ++j ) {
	    metricOld   += metricsValues[ dMesh->v2f[ dMesh->v2fDim[vertexIdx] + j ] ];
	    leastSquaresFace( dMesh, vertices, dMesh->v2f[ dMesh->v2fDim[vertexIdx] + j ],
			      buffer, &tmp );
	    metricMinus += tmp;
	  }
	  gradient[i] = ( metricOld - metricMinus ) / delta;
	  
	} else {
	  
	  gradient[i] = INF;
	  
	}
	
	if ( rescale && (lbounds[i] != -INF) && (ubounds[i] != INF) )
	  xRescaled[i] = (ubounds[i] - lbounds[i]) * x[i] + lbounds[i];
	else
	  xRescaled[i] = x[i];
	vertices[3 * p2cIdx[i]]     = vertices_at_x[3 * p2cIdx[i]];
	vertices[3 * p2cIdx[i] + 1] = vertices_at_x[3 * p2cIdx[i] + 1];
	vertices[3 * p2cIdx[i] + 2] = vertices_at_x[3 * p2cIdx[i] + 2];
      }
      free( vertices );
      free( xRescaled );
    }
  } else if ( method == 1 ) {
    
    /* Central differences */
    
    for (i = 0; i < N; ++i)
      if ( rescale && ( lbounds[i] != -INF ) && ( ubounds[i] != INF ) ) {
	if ( delta > x[i] )           delta = x[i];
	if ( delta > ( 1.0 - x[i] ) ) delta = 1.0 - x[i];
      } else {
	if ( delta > ( x[i] - lbounds[i] ) ) delta = x[i] - lbounds[i];
	if ( delta > ( ubounds[i] - x[i] ) ) delta = ubounds[i] - x[i];
      }

#pragma omp parallel default(none)					\
  shared(metricsArgv)							\
  private(i, j, NLoc, vertexIdx, metricOld, metricMinus, metricPlus)	\
  private(xShifted, xRescaled, vertices, buffer, tmp)		\
  firstprivate(delta)
    {
      xRescaled = (double *) malloc (N * sizeof(double));
      for (j = 0; j < N; ++j)
	if ( rescale && (lbounds[j] != -INF) && (ubounds[j] != INF) )
	  xRescaled[j] = (ubounds[j] - lbounds[j]) * x[j] + lbounds[j];
	else
	  xRescaled[j] = x[j];

      vertices = (double *) malloc (3 * (dMesh->Nver) * sizeof(double));
      memcpy( vertices, vertices_at_x,  3 * (dMesh->Nver) * sizeof(double));

#pragma omp for schedule(dynamic)
      for (i = 0; i < N; ++i) {

	if ( strcmp(metricsArgv[0], "sqm") == 0 ) {

	  vertexIdx = p2cIdx[i];
	  NLoc   = dMesh->v2pDim[vertexIdx+1] - dMesh->v2pDim[vertexIdx];

	  xShifted = x[i] - delta;
	  if ( rescale && (lbounds[i] != -INF) && (ubounds[i] != INF) )
	    xRescaled[i] = (ubounds[i] - lbounds[i]) * xShifted + lbounds[i];
	  else
	    xRescaled[i] = xShifted;
	  par2car( xRescaled, vertices, dMesh->Nver, p2cIdx+i, 1, c2firstpIdx, dMesh->vBndMarkers);

	  metricMinus = 0.0;
	  for (j = 0; j < NLoc; ++j)
	    metricMinus += sqmPolyhedron( dMesh, vertices, atof( metricsArgv[1] ),
					  dMesh->v2p[ dMesh->v2pDim[vertexIdx] + j ] );

	  xShifted = x[i] + delta;
	  if ( rescale && (lbounds[i] != -INF) && (ubounds[i] != INF) )
	    xRescaled[i] = (ubounds[i] - lbounds[i]) * xShifted + lbounds[i];
	  else
	    xRescaled[i] = xShifted;
	  par2car( xRescaled, vertices, dMesh->Nver, p2cIdx+i, 1, c2firstpIdx, dMesh->vBndMarkers);
	  
	  metricPlus = 0.0;
	  for (j = 0; j < NLoc; ++j)
	    metricPlus += sqmPolyhedron( dMesh, vertices, atof( metricsArgv[1] ),
					 dMesh->v2p[ dMesh->v2pDim[vertexIdx] + j ] );
	  
	  gradient[i] = ( metricPlus - metricMinus ) / ( 2.0 * (dMesh->Npol) * delta );
	  
	} else if ( strcmp(metricsArgv[0], "leastSquares") == 0 ) {

	  vertexIdx = p2cIdx[i];
	  NLoc   = dMesh->v2fDim[vertexIdx+1] - dMesh->v2fDim[vertexIdx];

	  xShifted = x[i] - delta;
	  if ( rescale && (lbounds[i] != -INF) && (ubounds[i] != INF) )
	    xRescaled[i] = (ubounds[i] - lbounds[i]) * xShifted + lbounds[i];
	  else
	    xRescaled[i] = xShifted;
	  par2car( xRescaled, vertices, dMesh->Nver, p2cIdx+i, 1, c2firstpIdx, dMesh->vBndMarkers);

	  metricMinus = 0.0;
	  for (j = 0; j < NLoc; ++j) {
	    leastSquaresFace( dMesh, vertices, dMesh->v2f[ dMesh->v2fDim[vertexIdx] + j ],
			      buffer, &tmp );
	    metricMinus += tmp;
	  }

	  xShifted = x[i] + delta;
	  if ( rescale && (lbounds[i] != -INF) && (ubounds[i] != INF) )
	    xRescaled[i] = (ubounds[i] - lbounds[i]) * xShifted + lbounds[i];
	  else
	    xRescaled[i] = xShifted;
	  par2car( xRescaled, vertices, dMesh->Nver, p2cIdx+i, 1, c2firstpIdx, dMesh->vBndMarkers);
	  
	  metricPlus = 0.0;
	  for (j = 0; j < NLoc; ++j) {
	    leastSquaresFace( dMesh, vertices, dMesh->v2f[ dMesh->v2fDim[vertexIdx] + j ],
			      buffer, &tmp );
	    metricPlus += tmp;
	  }

	  gradient[i] = ( metricPlus - metricMinus ) / ( 2.0 * delta );

	} else {

	  gradient[i] = INF;

	}
	
	if ( rescale && (lbounds[i] != -INF) && (ubounds[i] != INF) )
	  xRescaled[i] = (ubounds[i] - lbounds[i]) * x[i] + lbounds[i];
	else
	  xRescaled[i] = x[i];
	vertices[3 * (*(p2cIdx+i))]     = vertices_at_x[3 * (*(p2cIdx+i))];
	vertices[3 * (*(p2cIdx+i)) + 1] = vertices_at_x[3 * (*(p2cIdx+i)) + 1];
	vertices[3 * (*(p2cIdx+i)) + 2] = vertices_at_x[3 * (*(p2cIdx+i)) + 2];	  
      }
      free( vertices );
      free( xRescaled );
    }
  }
}


double directionalDerivative( const short method,
			      const double pNorm,
			      const double * const pUnit,
			      const double phiJ,
			      const double * const x,
			      double * const xShifted,
			      const int N,
			      const short rescale,
			      const double * const lbounds,
			      const double * const ubounds,
			      double * const xRescaled,
			      double * const vertices,
			      const int * const c2firstpIdx,
			      const dualMesh * const dMesh,
			      char ** metricsArgv )
{
  int i;
  double delta, phiMinus, phiPlus;

  delta = sqrt( DBL_EPSILON );

  if ( method == 0 ) {
    if ( rescale ) {
      for (i = 0; i < N; ++i)
	if ( ( lbounds[i] != -INF ) && ( ubounds[i] != INF ) && ( delta * pUnit[i] > x[i] ) )
	  delta = x[i] / pUnit[i];
	else if ( delta * pUnit[i] > x[i] - lbounds[i] )
	  delta = ( x[i] - lbounds[i] ) / pUnit[i];
    } else {
      for (i = 0; i < N; ++i)
	if ( delta * pUnit[i] > x[i] - lbounds[i] ) delta = ( x[i] - lbounds[i] ) / pUnit[i];
    }

    for (i = 0; i < N; ++i) xShifted[i] = x[i] - delta*pUnit[i];

    phiMinus = evalMeshMetrics(xShifted, N, NULL, 0, rescale, lbounds, ubounds, xRescaled,
			       vertices, NULL, 0, c2firstpIdx, dMesh, metricsArgv, NULL);

    return ( ( phiJ - phiMinus ) / delta ) * pNorm;
  } else {
    if ( rescale ) {
      for (i = 0; i < N; ++i) {
	if ( ( lbounds[i] != -INF ) && ( ubounds[i] != INF ) ) {
	  if ( delta * pUnit[i] > x[i] )       delta = x[i] / pUnit[i];
	  if ( delta * pUnit[i] > 1.0 - x[i] ) delta = ( 1.0 - x[i] ) / pUnit[i];
	} else {
	  if ( delta * pUnit[i] > x[i] - lbounds[i] ) delta = ( x[i] - lbounds[i] ) / pUnit[i];
	  if ( delta * pUnit[i] > ubounds[i] - x[i] ) delta = ( ubounds[i] - x[i] ) / pUnit[i];
	}
      }
    } else {
      for (i = 0; i < N; ++i) {
	if ( delta * pUnit[i] > x[i] - lbounds[i] ) delta = ( x[i] - lbounds[i] ) / pUnit[i];
	if ( delta * pUnit[i] > ubounds[i] - x[i] ) delta = ( ubounds[i] - x[i] ) / pUnit[i];
      }
    }

    for (i = 0; i < N; ++i) xShifted[i] = x[i] - delta*pUnit[i];
    phiMinus = evalMeshMetrics(xShifted, N, NULL, 0, rescale, lbounds, ubounds, xRescaled,
			       vertices, NULL, 0, c2firstpIdx, dMesh, metricsArgv, NULL);

    for (i = 0; i < N; ++i) xShifted[i] = x[i] + delta*pUnit[i];
    phiPlus = evalMeshMetrics(xShifted, N, NULL, 0, rescale, lbounds, ubounds, xRescaled,
			      vertices, NULL, 0, c2firstpIdx, dMesh, metricsArgv, NULL);

    return ( ( phiPlus - phiMinus ) / ( 2.0 * delta ) ) * pNorm;
  }
}


double cubicInterpolate( const double alphaLow,
			 const double alphaHigh,
			 const double phiAlphaLow,
			 const double phiAlphaHigh,
			 const double dphiAlphaLow,
			 const double dphiAlphaHigh,
			 const double tolAlphaLow )
{
  double d1, d2, alpha, alphaHighLow;

  alphaHighLow = alphaHigh - alphaLow;
  d1 = dphiAlphaLow + dphiAlphaHigh - 3.0 * (phiAlphaHigh - phiAlphaLow) / alphaHighLow;

  if ( alphaHighLow > 0.0 )
    d2 = sqrt(d1*d1 - dphiAlphaLow*dphiAlphaHigh);
  else
    d2 = -sqrt(d1*d1 - dphiAlphaLow*dphiAlphaHigh);

  alpha =
    alphaHigh - alphaHighLow * (dphiAlphaHigh + d2 - d1) / (dphiAlphaHigh - dphiAlphaLow + 2.0*d2);

  if ( fabs(alpha - alphaLow) < tolAlphaLow * fabs(alphaHighLow) )
    alpha = alphaLow + tolAlphaLow * alphaHighLow;

  return alpha;
}


void zoom( const double alphaLowIN,
	   const double alphaHighIN,
	   const double phiAlphaLowIN,
	   const double phiAlphaHighIN,
	   const double dphiAlphaLowIN,
	   const double dphiAlphaHighIN,
	   const double tolAlpha,
	   const double tolAlphaLow,
	   const double phi0,
	   const double dphi0,
	   const double c1,
	   const double c2,
	   const short method,
	   const double * const x,
	   double * const xTmp1,
	   double * const xTmp2,
	   const double * const p,
	   const double pNorm,
	   const double * const pUnit,
	   const int N,
	   const short rescale,
	   const double * const lbounds,
	   const double * const ubounds,
	   double * const xRescaled,
	   double * const vertices,
	   const int * const c2firstpIdx,
	   const dualMesh * const dMesh,
	   char ** metricsArgv,
	   double * alphaStar,
	   double * phiNew,
	   double * const metricsValues )
{
  int i;
  double alpha, dphiNew;
  double alphaLow = alphaLowIN, alphaHigh = alphaHighIN;
  double phiAlphaLow = phiAlphaLowIN, phiAlphaHigh = phiAlphaHighIN;
  double dphiAlphaLow = dphiAlphaLowIN, dphiAlphaHigh = dphiAlphaHighIN;

  while ( true ) {

    if ( fabs( alphaHigh - alphaLow ) < tolAlpha ) {
      *alphaStar = alphaLow;
      *phiNew    = phiAlphaLow;
      break;
    }

    alpha = cubicInterpolate( alphaLow, alphaHigh, phiAlphaLow, phiAlphaHigh,
			      dphiAlphaLow, dphiAlphaHigh, tolAlphaLow );
    
    for (i = 0; i < N; ++i) {
      xTmp1[i] = x[i] + alpha * p[i];
      if ( rescale ) {
	if ( ( lbounds[i] != -INF ) && ( ubounds[i] != INF )
	     && ( ( xTmp1[i] < 0.0 ) || ( xTmp1[i] > 1.0 ) ) ) {
	  printf("File %s line %i: error, xTmp1 should be within bounds, aborting ...\n",
		 __FILE__, __LINE__);
	  exit(1);
	} else if ( ( xTmp1[i] < lbounds[i] ) || ( xTmp1[i] > ubounds[i] ) ) {
	  printf("File %s line %i: error, xTmp1 should be within bounds, aborting ...\n",
		 __FILE__, __LINE__);
	  exit(1);
	}
      } else {
	if ( ( xTmp1[i] < lbounds[i] ) || ( xTmp1[i] > ubounds[i] ) ) {
	  printf("File %s line %i: error, xTmp1 should be within bounds, aborting ...\n",
		 __FILE__, __LINE__);
	  exit(1);
	}
      }
    }

    *phiNew = evalMeshMetrics(xTmp1, N, NULL, 0, rescale, lbounds, ubounds, xRescaled,
			      vertices, NULL, 0, c2firstpIdx, dMesh, metricsArgv, metricsValues);

    dphiNew = directionalDerivative( method, pNorm, pUnit, *phiNew, xTmp1, xTmp2,
				     N, rescale, lbounds, ubounds, xRescaled, vertices,
				     c2firstpIdx, dMesh, metricsArgv );

    if ( ( (*phiNew) > phi0 + c1 * alpha * dphi0 ) || ( (*phiNew) >= (phiAlphaLow) ) ) {
      alphaHigh     = alpha;
      phiAlphaHigh  = *phiNew;
      dphiAlphaHigh = dphiNew;
    } else {
      if ( fabs( dphiNew ) <= -c2*dphi0 ) {
	*alphaStar = alpha;
	break;
      }
      if ( dphiNew * (alphaHigh - alphaLow) >= 0.0 ) {
	alphaHigh     = alphaLow;
	phiAlphaHigh  = phiAlphaLow;
	dphiAlphaHigh = dphiAlphaLow;
      }

      alphaLow     = alpha;
      phiAlphaLow  = *phiNew;
      dphiAlphaLow = dphiNew;      
    }
  }
}


void stepLength( const double alphaOld,
		 const double phiOld,
		 const double tolAlpha,
		 const double rho,
		 const double * const x,
		 const double * const p,
		 const int N,
		 const short rescale,
		 const double * const lbounds,
		 const double * const ubounds,
		 double * const xRescaled,
		 double * const vertices,
		 const int * const c2firstpIdx,
		 const dualMesh * const dMesh,
		 char ** metricsArgv,
		 double * alphaNew,
		 double * deltaAlpha,
		 double * alphaMax,
		 double * phiNew,
		 double * const xNew,
		 double * const metricsValues )
{
  int i;
  bool isfeasible;

  *alphaNew = alphaOld + *deltaAlpha;
  while ( (*deltaAlpha > tolAlpha ) && (*alphaNew >= *alphaMax) ) {
    (*deltaAlpha) *= rho;
    *alphaNew = alphaOld + *deltaAlpha;
  }

  do {
    isfeasible = true;
    for (i = 0; i < N; ++i) {
      xNew[i] = x[i] + (*alphaNew) * p[i];
      if ( rescale ) {
	if ( ( lbounds[i] != -INF ) && ( ubounds[i] != INF )
	     && ( ( xNew[i] < 0.0 ) || ( xNew[i] > 1.0 ) ) ) {
	  isfeasible = false;
	}
	else if ( ( xNew[i] < lbounds[i] ) || ( xNew[i] > ubounds[i] ) ) {
	  isfeasible = false;
	}
      } else {
	if ( ( xNew[i] < lbounds[i] ) || ( xNew[i] > ubounds[i] ) ) {
	  isfeasible = false;
	}
      }
    }

    if ( isfeasible ) {
      *phiNew = evalMeshMetrics(xNew, N, NULL, 0, rescale, lbounds, ubounds, xRescaled,
				vertices, NULL, 0, c2firstpIdx, dMesh, metricsArgv, metricsValues);
      break;
    } else {
      if ( *deltaAlpha <= tolAlpha ) {
	printf("File %s line %i: WARNING! Minimum step size reached ...\n",
	       __FILE__, __LINE__);
	*alphaNew = alphaOld;
	*phiNew   = phiOld;
	break;
      }
      else {
	if (*alphaNew < *alphaMax) *alphaMax = *alphaNew;
	(*deltaAlpha) *= rho;
	*alphaNew = alphaOld + *deltaAlpha;
      }
    }
  } while ( true );
}


void lineSearch( const double phi0,
		 const double dphi0,
		 const double c1,
		 const double c2,
		 const double tolAlpha,
		 const double tolAlphaLow,
		 const double rho,
		 const short method,
		 const double * const x,
		 double * const xTmp1,
		 double * const xTmp2,
		 const double * const p,
		 const double pNorm,
		 const double * const pUnit,
		 const int N,
		 const short rescale,
		 const double * const lbounds,
		 const double * const ubounds,
		 double * const xRescaled,
		 double * const vertices,
		 const int * const c2firstpIdx,
		 const dualMesh * const dMesh,
		 char ** metricsArgv,
		 double * alphaNew,
		 double * deltaAlpha,
		 double * phiNew,
		 double * const metricsValues )
{
  int iter;
  double alphaOld = 0.0;
  double alphaMax = DBL_MAX;
  double phiOld   = phi0;
  double dphiOld  = dphi0;
  double dphiNew;

  iter = 0;
  while ( true ) {

    /* printf("\nalphaOld: %.16f, deltaAlpha: %.16f, alphaMax: %.16f, tolAlpha: %.16f, " */
    /* 	   "x: %.16f, %.16f, %.16f\n", alphaOld, *deltaAlpha, alphaMax, tolAlpha, x[0], x[1], x[2]); */

    stepLength( alphaOld, phiOld, tolAlpha, rho, x, p, N, rescale, lbounds, ubounds,
		xRescaled, vertices, c2firstpIdx, dMesh, metricsArgv,
		alphaNew, deltaAlpha, &alphaMax, phiNew, xTmp1, metricsValues );

    /* printf("\nalpha and phiNew after stepLength: %.16f %.16f\n", *alphaNew, *phiNew); */

    if ( *deltaAlpha <= tolAlpha ) {
      break;
    }

    dphiNew = directionalDerivative( method, pNorm, pUnit, *phiNew, xTmp1, xTmp2, N,
				     rescale, lbounds, ubounds, xRescaled, vertices, c2firstpIdx,
				     dMesh, metricsArgv );

    /* printf("\ndphiNew in lineSearch: %.16f\n", dphiNew); */
    
    if ( (*phiNew > (phi0 + c1 * (*alphaNew) * dphi0)) || ((*phiNew >= phiOld) && (iter > 0)) ) {
      zoom( alphaOld, *alphaNew, phiOld, *phiNew, dphiOld, dphiNew, tolAlpha, tolAlphaLow,
	    phi0, dphi0, c1, c2, method, x, xTmp1, xTmp2, p, pNorm, pUnit, N, rescale,
	    lbounds, ubounds, xRescaled, vertices, c2firstpIdx, dMesh, metricsArgv,
	    alphaNew, phiNew, metricsValues );
      break;
    }

    if ( fabs( dphiNew ) <= -c2*dphi0 ) {
      break;
    }

    if ( dphiNew >= 0.0 ) {
      zoom( *alphaNew, alphaOld, *phiNew, phiOld, dphiNew, dphiOld, tolAlpha, tolAlphaLow,
	    phi0, dphi0, c1, c2, method, x, xTmp1, xTmp2, p, pNorm, pUnit, N, rescale,
	    lbounds, ubounds, xRescaled, vertices, c2firstpIdx, dMesh, metricsArgv,
	    alphaNew, phiNew, metricsValues );
      break;
    }

    phiOld   = *phiNew;
    dphiOld  = dphiNew;
    alphaOld = *alphaNew;
    ++iter;
  }
}


void nlcgInitialize( nlcg_t * nlcgPtr, char const *filename )
{
  int i;
  FILE *fp;
  char buffer[1024];

  nlcgPtr->numScalarParameters = 9;
  nlcgPtr->rescaleUnknowns     = false;
  nlcgPtr->method4derivatives  = 0; /* 0 - Backward differences, 1 - central differences */
  nlcgPtr->tolG                = 1e-12;
  nlcgPtr->tolX                = 1e-12;
  nlcgPtr->tolAlphaLow         = 0.1;
  nlcgPtr->c1                  = 1e-4;
  nlcgPtr->c2                  = 0.1;
  nlcgPtr->rho                 = 0.5;
  nlcgPtr->maxit               = 1000000000;

  nlcgPtr->formats   = (char **) malloc ((nlcgPtr->numScalarParameters) * sizeof(char *));
  nlcgPtr->addresses = (void **) malloc ((nlcgPtr->numScalarParameters) * sizeof(void *));

  i = 0;
  nlcgPtr->formats[i] = "rescaleUnknowns %i";
  nlcgPtr->addresses[i++] = (void *) &(nlcgPtr->rescaleUnknowns);
  nlcgPtr->formats[i] = "method4derivatives %i";
  nlcgPtr->addresses[i++] = (void *) &(nlcgPtr->method4derivatives);
  nlcgPtr->formats[i] = "tolG %lf"; nlcgPtr->addresses[i++] = (void *) &(nlcgPtr->tolG);
  nlcgPtr->formats[i] = "tolX %lf"; nlcgPtr->addresses[i++] = (void *) &(nlcgPtr->tolX);
  nlcgPtr->formats[i] = "tolAlphaLow %lf";
  nlcgPtr->addresses[i++] = (void *) &(nlcgPtr->tolAlphaLow);
  nlcgPtr->formats[i] = "c1 %lf"; nlcgPtr->addresses[i++] = (void *) &(nlcgPtr->c1);
  nlcgPtr->formats[i] = "c2 %lf"; nlcgPtr->addresses[i++] = (void *) &(nlcgPtr->c2);
  nlcgPtr->formats[i] = "rho %lf"; nlcgPtr->addresses[i++] = (void *) &(nlcgPtr->rho);
  nlcgPtr->formats[i] = "maxit %i"; nlcgPtr->addresses[i++] = (void *) &(nlcgPtr->maxit);

  nlcgPtr->numScalarParameters = i;

  if ( filename != NULL ) {
    fp = fopen( filename, "r" );
    if ( fp == NULL ) {
      printf("File %s line %i: could not open file %s, aborting ...\n",
  	     __FILE__, __LINE__, filename);
      exit(1);
    }
    for (i = 0; i < nlcgPtr->numScalarParameters; ++i) {
      rewind(fp);
      while( fgets(buffer, sizeof(buffer), fp) != NULL ) {
  	/* Skip comments */
  	if ( buffer[0] == '#' ) continue;
  	if ( sscanf(buffer, nlcgPtr->formats[i], nlcgPtr->addresses[i]) == 1 ) break;
      }
    }
    fclose(fp);
#ifdef DEBUG
    printf("rescaleUnknowns: %i\n", nlcgPtr->rescaleUnknowns);
    printf("method4derivatives: %i\n", nlcgPtr->method4derivatives);
    printf("tolG: %e\n", nlcgPtr->tolG);
    printf("tolX: %e\n", nlcgPtr->tolX);
    printf("tolAlphaLow: %e\n", nlcgPtr->tolAlphaLow);
    printf("c1: %g\n", nlcgPtr->c1);
    printf("c2: %g\n", nlcgPtr->c2);
    printf("rho: %g\n", nlcgPtr->rho);
    printf("maxit: %i\n", nlcgPtr->maxit);
#endif
  }
}


void nlcgDestroy( nlcg_t * nlcgPtr )
{
  if ( nlcgPtr->formats )   { free( nlcgPtr->formats ); nlcgPtr->formats = NULL; }
  if ( nlcgPtr->addresses ) { free( nlcgPtr->addresses ); nlcgPtr->addresses = NULL; }
}


void globalMeshOptimNLCG( const dualMesh * const dMesh,
			  double * const newVertices,
			  char **metricsArgv,
			  const nlcg_t * const nlcgPtr )
{
  int i, iter, dimension;
  int *p2cIdx = NULL, *c2firstpIdx = NULL;
  double phi0, dphi0, phiNew;
  double gNormSq, gNormSqOld, pNorm;
  double alphaNew, deltaAlpha, tolAlpha;

  double beta;

  double *metricsValues = NULL;
  double *x_in_bounds = NULL, *xTmp1 = NULL, *xTmp2 = NULL, *xRescaled = NULL;
  double *lbounds = NULL, *ubounds = NULL;
  double *g = NULL, *gOld = NULL, *p = NULL, *pUnit = NULL;

  /* Dimension oof the search (parametric) space */
  dimension = dimParametricSpace( dMesh->Nver, dMesh->vBndMarkers );

  printf("\nDimension of the optimization problem: %i\n", dimension);

  /* Initialize boundaries */
  lbounds = (double *) malloc (dimension * sizeof(double));
  ubounds = (double *) malloc (dimension * sizeof(double));
  getParametricBoundaries( dMesh->Nver, dMesh->vBndMarkers, lbounds, ubounds );

  /* Array of unknowns and other auxiliary arrays */
  x_in_bounds = (double *) malloc (dimension * sizeof(double));
  xTmp1       = (double *) malloc (dimension * sizeof(double));
  xTmp2       = (double *) malloc (dimension * sizeof(double));
  xRescaled   = (double *) malloc (dimension * sizeof(double));

  p2cIdx      = (int *) malloc (dimension * sizeof(int));
  par2carIdx( dMesh->Nver, dMesh->vBndMarkers, p2cIdx );

  c2firstpIdx = (int *) malloc ((dMesh->Nver) * sizeof(int));
  car2firstParIdx( dMesh->Nver, dMesh->vBndMarkers, c2firstpIdx );

  if ( strcmp( metricsArgv[0], "sqm" ) == 0 )
    metricsValues = (double *) malloc ((dMesh->Npol) * sizeof(double));
  else if ( strcmp( metricsArgv[0], "leastSquares" ) == 0 )
    metricsValues = (double *) malloc ((dMesh->Nfc) * sizeof(double));
  else
    metricsValues = NULL;
  
  /* Allocate gradient and search direction */
  g     = (double *) calloc (dimension, sizeof(double));
  gOld  = (double *) calloc (dimension, sizeof(double));
  p     = (double *) calloc (dimension, sizeof(double));
  pUnit = (double *) calloc (dimension, sizeof(double));

  /* Evaluate objective function and gradient at the initial point */
  car2par( dMesh->Nver, dMesh->vBndMarkers, newVertices, x_in_bounds );
  if ( nlcgPtr->rescaleUnknowns ) {
    /* The following call is just a safe-guard, that should not be necessary since xRescaled
     * gets modified by the first call to evalMeshMetrics below */
    memcpy( xRescaled, x_in_bounds, dimension * sizeof(double) );
    for (i = 0; i < dimension; ++i)
      if ( (lbounds[i] != -INF) && (ubounds[i] != INF) )
	x_in_bounds[i] = (x_in_bounds[i] - lbounds[i]) / (ubounds[i] - lbounds[i]);
  }

  phi0 = evalMeshMetrics(x_in_bounds, dimension, NULL, 0,
			 nlcgPtr->rescaleUnknowns, lbounds, ubounds, xRescaled, newVertices,
			 NULL, 0, c2firstpIdx, dMesh, metricsArgv, metricsValues);

  approxGradient( nlcgPtr->method4derivatives, x_in_bounds, dimension, newVertices,
		  nlcgPtr->rescaleUnknowns, lbounds, ubounds, p2cIdx, c2firstpIdx,
		  dMesh, metricsArgv, metricsValues, gOld );

  gNormSqOld = 0.0;
  for (i = 0; i < dimension; ++i) { gNormSqOld += gOld[i]*gOld[i]; p[i] = -gOld[i]; }

  printf("\nObjective function and squared norm of its gradient at the initial point: "
	 "%.16f, %.16f\n", phi0, gNormSqOld);

  /* Try the unit step length at the very beginning */
  alphaNew = 1.0;
  dphi0    = -gNormSqOld;

  printf("\nOptimizing mesh vertices with the nonlinear conjugate gradient method\n\n");

  iter = 0;
  while ( ( gNormSqOld > ((nlcgPtr->tolG) * (nlcgPtr->tolG)) ) && (iter < nlcgPtr->maxit) ) {
    printf("\nNLCG iteration %i, ", iter+1);

    deltaAlpha = alphaNew;
    pNorm      = cblas_dnrm2(dimension, p, 1);
    for (i = 0; i < dimension; ++i) pUnit[i] = p[i] / pNorm;
    tolAlpha   = nlcgPtr->tolX / pNorm;

    /* Compute step length */
    lineSearch( phi0, dphi0, nlcgPtr->c1, nlcgPtr->c2, tolAlpha, nlcgPtr->tolAlphaLow,
  		nlcgPtr->rho, nlcgPtr->method4derivatives, x_in_bounds,
  		xTmp1, xTmp2, p, pNorm, pUnit, dimension, nlcgPtr->rescaleUnknowns,
  		lbounds, ubounds, xRescaled, newVertices, c2firstpIdx, dMesh, metricsArgv,
  		&alphaNew, &deltaAlpha, &phiNew, metricsValues );

    printf("alphaNew: %.16f, function value: %.16f\n", alphaNew, phiNew);

    if ( (alphaNew * pNorm) < nlcgPtr->tolX ) {
      printf("Exiting loop because argument increment falls below the given threshold.\n");
      break;
    }

    for (i = 0; i < dimension; ++i) {
      x_in_bounds[i] += alphaNew * p[i];
      if ( nlcgPtr->rescaleUnknowns ) {
  	if ( ( lbounds[i] != -INF ) && ( ubounds[i] != INF )
  	     && ( ( x_in_bounds[i] < 0.0 ) || ( x_in_bounds[i] > 1.0 ) ) ) {
  	  printf("File %s line %i: error, x should be within bounds, aborting ...\n",
  		 __FILE__, __LINE__);
  	  exit(1);
  	} else if ( ( x_in_bounds[i] < lbounds[i] ) || ( x_in_bounds[i] > ubounds[i] ) ) {
  	  printf("File %s line %i: error, x should be within bounds, aborting ...\n",
  		 __FILE__, __LINE__);
  	  exit(1);
  	}
      } else {
  	if ( ( x_in_bounds[i] < lbounds[i] ) || ( x_in_bounds[i] > ubounds[i] ) ) {
  	  printf("File %s line %i: error, x should be within bounds, aborting ...\n",
  		 __FILE__, __LINE__);
  	  exit(1);
  	}
      }
    }

    /* Compute the gradient */
    approxGradient( nlcgPtr->method4derivatives, x_in_bounds, dimension, newVertices,
  		    nlcgPtr->rescaleUnknowns, lbounds, ubounds, p2cIdx, c2firstpIdx,
  		    dMesh, metricsArgv, metricsValues, g );

    gNormSq = 0.0;
    beta    = 0.0;
    for (i = 0; i < dimension; ++i) {
      gNormSq += g[i] * g[i];
      beta    += g[i] * gOld[i];
    }
    beta = (gNormSq - beta) / gNormSqOld;
    if ( beta < 0.0 ) beta = 0.0;

    dphi0 = 0.0;
    for (i = 0; i < dimension; ++i) {
      p[i]    = -g[i] + beta * p[i];
      gOld[i] = g[i];
      dphi0   += g[i] * p[i];
    }

    phi0       = phiNew;
    gNormSqOld = gNormSq;
    ++iter;
  }

  if (iter == nlcgPtr->maxit )
    printf("NLCG did not converge within %i iterations.\n", nlcgPtr->maxit);

  if ( nlcgPtr->rescaleUnknowns ) {
    for (i = 0; i < dimension; ++i)
      if ( (lbounds[i] != -INF) && (ubounds[i] != INF) )
  	xRescaled[i] = (ubounds[i] - lbounds[i]) * x_in_bounds[i] + lbounds[i];
      else
  	xRescaled[i] = x_in_bounds[i];
    par2car( xRescaled, newVertices, dMesh->Nver, NULL, 0, c2firstpIdx, dMesh->vBndMarkers );
  } else {
    par2car( x_in_bounds, newVertices, dMesh->Nver, NULL, 0, c2firstpIdx, dMesh->vBndMarkers );
  }

  free( lbounds );
  free( ubounds );
  free( x_in_bounds );
  free( xTmp1 );
  free( xTmp2 );
  free( xRescaled );
  free( p2cIdx );
  free( c2firstpIdx );
  free( metricsValues );
  free( g );
  free( gOld );
  free( p );
  free( pUnit );
}


void globalMeshOptimCMAES( const dualMesh * const dMesh,
			   double * const newVertices,
			   const char * const cmaesInitialsFilename,
			   const char * const cmaesSignalsFilename,
			   const char * const cmaesOutputFilename,
			   char **metricsArgv )
{
  int i, dimension;
  int *c2firstpIdx = NULL;
  cmaes_t evo;
  cmaes_boundary_transformation_t boundaries;
  double *arFunvals, *x_in_bounds, *const*pop;
  double *lbounds, *ubounds, *rescaled_lbounds, *rescaled_ubounds;
  double *xRescaled;

  /* Dimension of the parametric space */
  dimension = dimParametricSpace( dMesh->Nver, dMesh->vBndMarkers );

  c2firstpIdx = (int *) malloc ((dMesh->Nver) * sizeof(int));
  car2firstParIdx( dMesh->Nver, dMesh->vBndMarkers, c2firstpIdx );

  /* Initialize boundaries */
  lbounds = (double *) malloc (dimension * sizeof(double));
  ubounds = (double *) malloc (dimension * sizeof(double));
  getParametricBoundaries( dMesh->Nver, dMesh->vBndMarkers, lbounds, ubounds );

  rescaled_lbounds = (double *) malloc (dimension * sizeof(double));
  rescaled_ubounds = (double *) malloc (dimension * sizeof(double));
  for (i = 0; i < dimension; ++i) {
    if ( ( lbounds[i] != -INF ) && ( ubounds[i] != INF ) ) {
      /* If both bounds are given, the i-th unknown will be scaled */
      rescaled_lbounds[i] = 0.0;
      rescaled_ubounds[i] = 1.0;
    } else {
      rescaled_lbounds[i] = lbounds[i];
      rescaled_ubounds[i] = ubounds[i];
    }
  }
  cmaes_boundary_transformation_init(&boundaries, rescaled_lbounds, rescaled_ubounds, dimension);

  /* Initial search point */
  x_in_bounds = (double *) malloc (dimension * sizeof(double));
  car2par( dMesh->Nver, dMesh->vBndMarkers, newVertices, x_in_bounds );
  for (i = 0; i < dimension; ++i) {
    if ( ( lbounds[i] != -INF ) && ( ubounds[i] != INF ) )
      x_in_bounds[i] = (x_in_bounds[i] - lbounds[i]) / (ubounds[i] - lbounds[i]);
  }

  /* Buffer for rescaled vector of unknowns */
  xRescaled = (double *) calloc (dimension, sizeof(double));

  /* Initialize everything into the struct evo, 0 means default */
  arFunvals = cmaes_init(&evo, dimension, x_in_bounds, NULL, 0, 0, cmaesInitialsFilename);

  /* Write header and initial values */
  cmaes_ReadSignals(&evo, cmaesSignalsFilename);

  printf("%s\n", cmaes_SayHello(&evo));

  /* Iterate until stop criterion holds */
  while(!cmaes_TestForTermination(&evo)) {
      /* Generate lambda new search points, sample population */
      pop = cmaes_SamplePopulation(&evo); /* do not change content of pop */

      /* Transform into bounds and evaluate the new search points */
      for (i = 0; i < cmaes_Get(&evo, "lambda"); ++i) {
        cmaes_boundary_transformation(&boundaries, pop[i], x_in_bounds, dimension);

	/* Evaluate */
        arFunvals[i] = evalMeshMetrics(x_in_bounds, dimension, NULL, 0,
				       1, lbounds, ubounds, xRescaled,
				       newVertices, NULL, 0, c2firstpIdx, dMesh, metricsArgv,
				       NULL );
      }

      /* Update the search distribution used for cmaes_SampleDistribution() */
      cmaes_UpdateDistribution(&evo, arFunvals);  /* assumes that pop[i] has not been modified */

      /* Read instructions for printing output or changing termination conditions */ 
      cmaes_ReadSignals(&evo, cmaesSignalsFilename);
      fflush(stdout);
  }
  printf("Stop:\n%s\n",  cmaes_TestForTermination(&evo)); /* Print termination reason */
  cmaes_WriteToFile(&evo, "all", cmaesOutputFilename);         /* Write final results */

  /* Get best estimator for the optimum, xbestever */
  cmaes_boundary_transformation(&boundaries,
				(double const *) cmaes_GetPtr(&evo, "xbestever"), /* "xmean" might be used as well */
				x_in_bounds, dimension);

  for (i = 0; i < dimension; ++i)
    if ( ( lbounds[i] != -INF ) && ( ubounds[i] != INF ) )
      xRescaled[i] = (ubounds[i] - lbounds[i]) * x_in_bounds[i] + lbounds[i];
    else
      xRescaled[i] = x_in_bounds[i];
  par2car( xRescaled, newVertices, dMesh->Nver, NULL, 0, c2firstpIdx, dMesh->vBndMarkers );

  /* Release memory */
  cmaes_exit(&evo); /* release memory */
  cmaes_boundary_transformation_exit(&boundaries); /* release memory */

  free( c2firstpIdx );
  free( lbounds );
  free( ubounds );
  free( rescaled_lbounds );
  free( rescaled_ubounds );
  free( x_in_bounds );
  free( xRescaled );
}


void localMeshOptim( const dualMesh * const dMesh,
		     double * const newVertices,
		     char** metricsArgv,
		     const double tolerance,
		     const int maxit )
{
  int i, j, k, f, hEdge, tEdge, Ned, itmp, patchSize, iter = 0;
  double tmp, oldNorm, relres = DBL_MAX;
  double *oldVertices = NULL;

  double *allNormals = NULL, *patchNormals = NULL, *rhs = NULL, *S = NULL;
  double faceCentroid[3];
  lapack_int m, n, nrhs, ldb, rank;

  oldVertices = (double *) malloc (3 * (dMesh->Nver) * sizeof(double));
  memcpy( oldVertices, newVertices, 3 * (dMesh->Nver) * sizeof(double));

  if ( strcmp( metricsArgv[0], "leastSquares" ) == 0 )
    allNormals = (double *) malloc (3 * (dMesh->Nfc) * sizeof(double));

  while ( ( relres > tolerance ) && (iter < maxit) ) {

    oldNorm = cblas_dnrm2(3 * (dMesh->Nver), oldVertices, 1);

    if ( strcmp( metricsArgv[0], "leastSquares" ) == 0 ) {

#pragma omp parallel for default(none) shared(oldVertices, allNormals) private(i, tmp) \
  schedule(dynamic)
      /* Compute the least squares place for each face */
      for (i = 0; i < dMesh->Nfc; ++i)
	leastSquaresFace( dMesh, oldVertices, i, allNormals+3*i, &tmp );

#pragma omp parallel for default(none)					\
  shared(allNormals, oldVertices)					\
  private(i, patchSize, patchNormals, ldb, rhs, j, f, k, faceCentroid, Ned, hEdge, tEdge, itmp) \
  private(m, n, nrhs, S, rank)						\
  schedule(dynamic)
      for (i = 0; i < dMesh->Nver; ++i) {
	if ( dMesh->vBndMarkers[i] != -1 ) {
	  patchSize    = dMesh->v2fDim[i+1] - dMesh->v2fDim[i];
	  patchNormals = (double *) malloc (3 * patchSize * sizeof(double));
	  if ( patchSize > 3 ) {
	    ldb = patchSize;
	    S = (double *) malloc (3 * sizeof(double));
	  }
	  else {
	    ldb = 3;
	    S = (double *) malloc (patchSize * sizeof(double));
	  }
	  rhs          = (double *) calloc (ldb, sizeof(double));
	  for (j = 0; j < patchSize; ++j) {
	    f = dMesh->v2f[ dMesh->v2fDim[i]+ j ];
	    patchNormals[j]             = allNormals[3*f];
	    patchNormals[j+patchSize]   = allNormals[3*f+1];
	    patchNormals[j+2*patchSize] = allNormals[3*f+2];

	    faceCentroid[0] = 0.0; faceCentroid[1] = 0.0; faceCentroid[2] = 0.0;
	    Ned = dMesh->f2heDim[f+1] - dMesh->f2heDim[f];
	    for (k = 0; k < Ned; ++k) {
	      hEdge = dMesh->f2he[ dMesh->f2heDim[f] + k ];
	      tEdge = hEdge/2;
	      itmp  = dMesh->edges[ 2 * tEdge + hEdge%2 ];
	      faceCentroid[0] += oldVertices[3*itmp  ];
	      faceCentroid[1] += oldVertices[3*itmp+1];
	      faceCentroid[2] += oldVertices[3*itmp+2];
	    }
	    faceCentroid[0] /= (double)(Ned);
	    faceCentroid[1] /= (double)(Ned);
	    faceCentroid[2] /= (double)(Ned);

	    rhs[j] = patchNormals[j]*faceCentroid[0]
	      + patchNormals[j+patchSize]*faceCentroid[1]
	      + patchNormals[j+2*patchSize]*faceCentroid[2];
	  }

	  m     = patchSize;
	  n     = 3;
	  nrhs  = 1;
	  LAPACKE_dgelss( LAPACK_COL_MAJOR, m, n, nrhs, patchNormals, m, rhs, ldb, S, -1.0, &rank );

	  newVertices[3*i]   = rhs[0];
	  newVertices[3*i+1] = rhs[1];
	  newVertices[3*i+2] = rhs[2];

	  free( patchNormals );
	  free( rhs );
	  free( S );
	}
      }
    }

    cblas_daxpy(3 * (dMesh->Nver), -1.0, newVertices, 1, oldVertices, 1);
    relres = cblas_dnrm2(3 * (dMesh->Nver), oldVertices, 1) / oldNorm;

    memcpy( oldVertices, newVertices, 3 * (dMesh->Nver) * sizeof(double));
    ++iter;
  }

  if ( iter == maxit )
    printf("Algorithm did not converge. Relative norm of residual: %.16f\n", relres);
  else
    printf("Algorithm converged in %i iterations\n", iter);

  /* Release memory */
  if ( strcmp( metricsArgv[0], "leastSquares" ) == 0 )
    free( allNormals );

  free( oldVertices );
}


void localMesh2dOptimCMAES( const dualMesh2d * const dMesh2d,
			    double * const newVertices,
			    const char * const cmaesInitialsFilename,
			    const char * const cmaesSignalsFilename,
			    char **metricsArgv )
{
  int i, j;
  int dimension = 2;
  double c;
  double *lb = NULL, *ub = NULL, *x_in_bounds = NULL, *arFunvals = NULL;
  double *const* pop;
  cmaes_t evo;
  cmaes_boundary_transformation_t boundaries;

  /* At present, we are rescaling all the coordinates between 0 and 1 */
  double lowerBounds[] = {0.0, 0.0};
  double upperBounds[] = {1.0, 1.0};

  lb = (double *) malloc (2 * sizeof(double));
  ub = (double *) malloc (2 * sizeof(double));
  x_in_bounds = (double *) malloc (2 * sizeof(double));

  /* Find minimum and maximum coordinates */
  lb[0] = dMesh2d->vertices[0];
  lb[1] = dMesh2d->vertices[1];
  ub[0] = lb[0];
  ub[1] = lb[1];
  for (i = 1; i < dMesh2d->Nver; ++i) {
    if ( dMesh2d->vertices[2*i]   < lb[0] ) lb[0] = dMesh2d->vertices[2*i];
    if ( dMesh2d->vertices[2*i+1] < lb[1] ) lb[1] = dMesh2d->vertices[2*i+1];
    if ( dMesh2d->vertices[2*i]   > ub[0] ) ub[0] = dMesh2d->vertices[2*i];
    if ( dMesh2d->vertices[2*i+1] > ub[1] ) ub[1] = dMesh2d->vertices[2*i+1];
  }

  /* initialize boundaries, be sure that initialSigma is smaller than upper minus lower bound */
  cmaes_boundary_transformation_init(&boundaries, lowerBounds, upperBounds, dimension);

  memcpy( newVertices, dMesh2d->vertices, 2 * (dMesh2d->Nver) * sizeof(double));

  c = atof( metricsArgv[1] );

  for (i = 0; i < dMesh2d->Nver; ++i) {
    if ( dMesh2d->vBndMarkers[i] == 0 ) {

      /* Initial search point */
      x_in_bounds[0] = ( newVertices[2*i] - lb[0] ) / ( ub[0] - lb[0] );
      x_in_bounds[1] = ( newVertices[2*i+1] - lb[1] ) / ( ub[1] - lb[1] );

      /* Initialize everything into the struct evo, 0 means default */
      arFunvals = cmaes_init(&evo, dimension, x_in_bounds, NULL, 0, 0, cmaesInitialsFilename);

      /* Write header and initial values */
      cmaes_ReadSignals(&evo, cmaesSignalsFilename);

      /* Iterate until stop criterion holds */
      while(!cmaes_TestForTermination(&evo)) {
	/* Generate lambda new search points, sample population */
	pop = cmaes_SamplePopulation(&evo); /* do not change content of pop */

	/* Transform into bounds and evaluate the new search points */
	for (j = 0; j < cmaes_Get(&evo, "lambda"); ++j) {
	  cmaes_boundary_transformation(&boundaries, pop[j], x_in_bounds, dimension);

	  /* Evaluate */
	  arFunvals[j] = sqm2dVertex(x_in_bounds, lb, ub, i, dMesh2d, newVertices, c);
	}

	/* Update the search distribution used for cmaes_SampleDistribution() */
	cmaes_UpdateDistribution(&evo, arFunvals);  /* assumes that pop[i] has not been modified */

	/* Read instructions for printing output or changing termination conditions */
	cmaes_ReadSignals(&evo, cmaesSignalsFilename);
	fflush(stdout);
      }

      /* Get best estimator for the optimum, xbestever */
      cmaes_boundary_transformation(&boundaries,
				    (double const *) cmaes_GetPtr(&evo, "xbestever"),
				    x_in_bounds, dimension);

      newVertices[2*i]   = ( ub[0] - lb[0] ) * x_in_bounds[0] + lb[0];
      newVertices[2*i+1] = ( ub[1] - lb[1] ) * x_in_bounds[1] + lb[1];

      /* Release memory */
      cmaes_exit(&evo); /* release memory */
    }
  }

  cmaes_boundary_transformation_exit(&boundaries); /* release memory */

  free(lb);
  free(ub);
  free(x_in_bounds);
}


/*
 * Choose an objective function
 */
void chooseObjFun( int * objfunArgc,
		   char *** objfunArgv )
{
  int flag;
  double tmp;

  printf("\nChoose an objective function:\n"
	 "0. shape quality measure;\n"
	 "1. leastSquares;\n"
	 "Choice: ");
  if ( scanf("%i", &flag) != 1 ) {
    printf("File %s, line %i: error, variable flag could not be read from terminal, "
	   "aborting ...\n", __FILE__, __LINE__);
    exit(1);
  }

  if ( flag == 0 ) {

    printf("\nChoose a value for constant c in sqm: ");
    if ( scanf("%lf", &tmp) != 1 ) {
      printf("File %s, line %i: error, variable c could not be read from terminal, "
	     "aborting ...\n", __FILE__, __LINE__);
      exit(1);
    }
    *objfunArgc = 2;
    *objfunArgv = (char **) malloc ((*objfunArgc) * sizeof(char *));
    (*objfunArgv)[0] = (char *) malloc (128 * sizeof(char));
    (*objfunArgv)[1] = (char *) malloc (128 * sizeof(char));
    sprintf((*objfunArgv)[0], "sqm");
    sprintf((*objfunArgv)[1], "%.16f", tmp);
    printf("\n");

  } else {

    *objfunArgc = 1;
    *objfunArgv = (char **) malloc ((*objfunArgc) * sizeof(char *));
    (*objfunArgv)[0] = (char *) malloc (128 * sizeof(char));
    sprintf((*objfunArgv)[0], "leastSquares");
    printf("\n");

  }
}


/*
 * Choose a group of nodes to optimize
 */
void chooseGroupNodes( const char * allflagsFilename,
		       const char * bndflagsFilename,
		       const char * intflagsFilename,
		       int * const vBndMarkers,
		       const int Nver )
{
  int i, flag;
  FILE *fp = NULL;

  printf("\nChoose a group of nodes to optimize:\n"
	 "0. all nodes;\n"
	 "1. boundary nodes;\n"
	 "2. internal nodes.\n\n"
	 "Choice: ");
  if ( scanf("%i", &flag) != 1 ) {
    printf("File %s, line %i: user choice could not be read from terminal, "
	   "aborting ...\n", __FILE__, __LINE__);
    exit(1);
  }
  if ( flag == 0 )
    fp = fopen(allflagsFilename, "r");
  else if ( flag == 1 )
    fp = fopen(bndflagsFilename, "r");
  else
    fp = fopen(intflagsFilename, "r");
  if ( fscanf(fp, "%i", &i) != 1 ) {
    printf("File %s line %i: encountered error while reading flags file, aborting ...\n",
	   __FILE__, __LINE__);
    exit(1);
  }
  for (i = 0; i < Nver; ++i) {
    if ( fscanf(fp, "%i", vBndMarkers+i) != 1 ) {
      printf("File %s line %i: encountered error while reading flag file, aborting ...\n",
	     __FILE__, __LINE__);
      exit(1);
    }
  }
  fclose(fp);
}
