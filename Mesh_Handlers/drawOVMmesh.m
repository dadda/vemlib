% Draw mesh written in OVM format
%
% It is assumed that an OVM mesh has already been loaded.

figure(1)
axis equal;

figure(2)
axis equal;

% for ed = 1:mesh.Ned
%     plot3(mesh.coordinates(mesh.edge2vertices(ed,:),1), ...
%           mesh.coordinates(mesh.edge2vertices(ed,:),2), ...
%           mesh.coordinates(mesh.edge2vertices(ed,:),3), ...
%           'b')
% end

% Plot elements centroids to visualize cells better
for E = 1:mesh.Nelt
    element = construct_element_connectivity(mesh, E);
    Nfc_E = numel(element);

    figure(1);
    hold on;
    for f = 1:Nfc_E
        plot3( mesh.coordinates( [element{f} element{f}(1)], 1), ...
               mesh.coordinates( [element{f} element{f}(1)], 2), ...
               mesh.coordinates( [element{f} element{f}(1)], 3), 'b' );
           hold on;
    end
    ivertices = unique( horzcat( element{:}) );
    barycenter = sum( mesh.coordinates(ivertices, :), 1 )/length(ivertices);
    plot3(barycenter(1), barycenter(2), barycenter(3), 'r+')
    hold off;

    figure(2);
    % cla;
    for f = 1:Nfc_E
        patch( mesh.coordinates( [element{f} element{f}(1)], 1), ...
               mesh.coordinates( [element{f} element{f}(1)], 2), ...
               mesh.coordinates( [element{f} element{f}(1)], 3), 'b' );
    end

end
