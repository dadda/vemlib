function [ hmax, hmin, inradii, incenters ] = computeStatisticsSingleMesh2d( mesh )

hmax      = zeros(mesh.Nelt, 1);
hmin      = zeros(mesh.Nelt, 1);
inradii   = zeros(mesh.Nelt, 1);
incenters = zeros(mesh.Nelt, 2);

options = optimoptions('fminimax', 'Display', 'off');

Nelt        = mesh.Nelt;
elements    = mesh.elements;
coordinates = mesh.coordinates;

parfor ( E = 1:Nelt, getParforArg() )
    element_E = elements(E,:);

    NverLoc  = element_E(1);
    vertices = coordinates( element_E(2:NverLoc+1), : );

    D = pdist( coordinates( unique(element_E(2:NverLoc+1)), : ) );
    hmax(E) = max(D);
    hmin(E) = min(D);
    
    barycenter = sum( vertices, 1 ) / size( vertices, 1 );
    
    Nx = vertices([2:NverLoc 1], 2) - vertices(1:NverLoc, 2);
    Ny = vertices(1:NverLoc, 1) - vertices([2:NverLoc 1], 1);
    norms = sqrt( Nx.^2 + Ny.^2 );
    N = [ Nx./norms Ny./norms ];
    
    myobj = @(x) minmaxobj(x, N, vertices);
    [incenters(E,:), ~, maxval] = fminimax(myobj, barycenter, [], [], [], [], [], [], [], options);
    inradii(E) = -maxval;
end

end
