function [faces_bnd_nodes, faces_qformula2D, faces_normals, faces_algdist, ...
          faces_edges_normals2D, faces_diameters, faces_areas, faces_centroids2D, ...
          collapsed_variable, faces_orders] = faces_features(vertices, faces, ...
                                                             k, eoa, compressquad, use_mp)

% vertices is Nver x 3
% faces as in alg3

% faces_normals: unit normals computed assuming each face is consistently oriented
% faces_edges_normals: cell array with 2d outer normals for eahc edge of
%                      each face normalized so that each normal has norm
%                      equal to the length of the edge

% Number of faces
Nfc = size(faces, 1);

faces_normals         = zeros(Nfc,3);
faces_algdist         = zeros(Nfc,1); % Algebraic distance, i.e. unit normal . x0
faces_bnd_nodes       = cell(Nfc,1);
faces_edges_normals2D = cell(Nfc,1);
faces_areas           = zeros(Nfc,1);
faces_diameters       = zeros(Nfc,1);
faces_centroids2D     = zeros(Nfc,2);
faces_qformula2D      = cell(Nfc,1);
faces_orders          = zeros(Nfc,1,'uint64');

% By default rotation is performed to align faces to the z axis
R = zeros(3,3);
collapsed_variable = 3*ones(Nfc, 1, 'uint8');

% 1d quadrature rule
[xqd1, wqd1] = lobatto(k+1, use_mp);
formula1d = [(1.-xqd1)/2. (xqd1+1.)/2. wqd1/2.];

for f = 1:Nfc
    %%% Compute outer unit normal for the current face
    %
    % From the post "How to efficiently determine the normal to a polygon in 3D space?"
    % on stackoverflow.com:
    % If I were you, I would have done it in the following way:
    % 1. Choose any point C near the polygon (any vertex or mass center).
    % 2. Sum cross products (P[i] - C) x (P[i+1] - C) for all i (including last and first points pair).
    % 3. Normalize the sum vector.

    Nver = length(faces{f});

    % Find mid point and use that as reference point
    C = sum(vertices(faces{f},:),1)/Nver;

    v1 = [(vertices(faces{f},1)-C(1)) ...
          (vertices(faces{f},2)-C(2)) ...
          (vertices(faces{f},3)-C(3))];
    v2 = [(vertices(faces{f}([2:Nver 1]),1)-C(1)) ...
          (vertices(faces{f}([2:Nver 1]),2)-C(2)) ...
          (vertices(faces{f}([2:Nver 1]),3)-C(3))];

    faces_normals(f,:) = sum([(v1(:,2).*v2(:,3) - v1(:,3).*v2(:,2)) ...
                              (v1(:,3).*v2(:,1) - v1(:,1).*v2(:,3)) ...
                              (v1(:,1).*v2(:,2) - v1(:,2).*v2(:,1))],1);
    faces_areas(f)     = norm(faces_normals(f,:))/2.0;
    faces_normals(f,:) = faces_normals(f,:) / (2.0 * faces_areas(f));
    % faces_algdist(f)   = faces_normals(f,:) * vertices(faces{f}(1),:)';
    faces_algdist(f)   = sum( faces_normals(f,:) * vertices(faces{f},:)' )/Nver;
    
    % Assemble rotation matrix
    if ( abs(faces_normals(f,3)) < 0.5 )
        % Rotate towards e_z
        ct    = faces_normals(f,3);
        omega = [faces_normals(f,2) -faces_normals(f,1) 0];
    else
        % Rotate towards e_x
        ct    = faces_normals(f,1);
        omega = [0 faces_normals(f,3) -faces_normals(f,2)];
        collapsed_variable(f) = 1;
    end
    %st = norm(omega);    
    st = sqrt(1.0 - ct^2);
    omega = omega / st;
    
    R(1,1) = ct + omega(1)^2*(1-ct);
    R(2,2) = ct + omega(2)^2*(1-ct);
    R(3,3) = ct + omega(3)^2*(1-ct);

    R(1,2) = -omega(3)*st + omega(1)*omega(2)*(1-ct);
    R(1,3) = omega(2)*st + omega(1)*omega(3)*(1-ct);
    R(2,1) = omega(3)*st + omega(1)*omega(2)*(1-ct);
    R(2,3) = -omega(1)*st + omega(2)*omega(3)*(1-ct);
    R(3,1) = -omega(2)*st + omega(1)*omega(3)*(1-ct);
    R(3,2) = omega(1)*st + omega(2)*omega(3)*(1-ct);

%     ct    = faces_normals(f,3);
%     omega = [faces_normals(f,2) -faces_normals(f,1)];
%     st = sqrt(1.0 - ct^2);
%     omega = omega / st;
%     omega = [0.0       0.0       omega(2);
%              0.0       0.0      -omega(1);
%              -omega(2) omega(1)       0.0];
%          
%     R = eye(3) + omega * st + (omega*omega) * (1-ct);
         
    survived_variables = [1:collapsed_variable(f)-1 collapsed_variable(f)+1:3];

    % rotated_vertices is 2 x Nver
    rotated_vertices = R*vertices(faces{f}, :)';
    rotated_vertices = rotated_vertices(survived_variables,:)';

    [ xbnd_f, ybnd_f, xqd_f, yqd_f, wqd_f, ...
      Nx_f, Ny_f, faces_diameters(f), ~, ...
      faces_centroids2D(f,1), faces_centroids2D(f,2) ] = ...
    build_coordinate_matrices_parallel( k, eoa, compressquad, ...
                                        [Nver 1:Nver], rotated_vertices, ...
                                        formula1d, use_mp );
    
    faces_bnd_nodes{f}  = [xbnd_f ybnd_f];
    faces_qformula2D{f} = [xqd_f yqd_f wqd_f];
    faces_edges_normals2D{f} = [Nx_f, Ny_f];    
    
%     % Nver x 2
%     faces_edges_normals2D{f} = [ rotated_vertices([2:Nver 1],2)-rotated_vertices(:,2) ...
%                                rotated_vertices(:,1)-rotated_vertices([2:Nver 1],1) ];
% 
%     % Areas - the following formula can be obtained from Chin2015 (11),
%     % using ||a_i|| = length of edge i, as we are doing
%     faces_areas(f) = sum( sum( rotated_vertices .* faces_edges_normals2D{f}, 2 ) ) / 2.;
% 
%     % Face diameter
%     faces_diameters(f) = max( pdist(rotated_vertices) );
%     
%     % Quadrature nodes on the face
%     if compressquad
% %         xyw = polygon_cubature_triangulation_dp([rotated_vertices; rotated_vertices(1,:)], k);
% %         [pts, w] = comprexcub(k, xyw(:,1:2), xyw(:,3), 1);
%         [x,y,w] = polygauss_2013(k, [rotated_vertices; rotated_vertices(1,:)], 0);
%         [pts, w] = comprexcub(k, [x,y], w, 1);
%         faces_qformula2D{f} = [pts w];
%     else
% %         faces_qformula2D{f} = polygon_cubature_triangulation_dp([rotated_vertices; rotated_vertices(1,:)], k);
%         [x,y,w] = polygauss_2013(k, [rotated_vertices; rotated_vertices(1,:)], 0);
%         faces_qformula2D{f} = [x,y,w];
%     end
% 
%     % Face centroid
%     faces_centroids2D(f,1) = ( faces_qformula2D{f}(:,3)' * faces_qformula2D{f}(:,1) ) / faces_areas(f);
%     faces_centroids2D(f,2) = ( faces_qformula2D{f}(:,3)' * faces_qformula2D{f}(:,2) ) / faces_areas(f);
    
    % Face order
    faces_orders(f) = Nver;
end


end
