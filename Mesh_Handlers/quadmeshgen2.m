function [mesh] = quadmeshgen2(N)
%  -------------
% generates a quad mesh on [0,1]^2 with N nodes per side
% then it perturbs all nodes but the central one and the boundary ones
%  -------------
% As output: mesh in MY format.
% See meshstruct.m for MY format info
%---------------------------------------------------
for i=1:N^2
    P(i,1) = mod(i-1,N)/(N-1);
    P(i,2) = (ceil(i/N)-1)/(N-1);
end
NN=(N-1)^2;
T=[4*ones(NN,1),zeros(NN,4)];
for k=1:NN
        T(k,2) = k + floor((k-1)/(N-1)) + N + 1;
        T(k,3) = k + floor((k-1)/(N-1)) + N;
        T(k,4) = k + floor((k-1)/(N-1));
        T(k,5) = k + floor((k-1)/(N-1)) + 1;
end    

for i=(N+1):(N^2-N)
    if ( P(i,1)*(1-P(i,1)) > 1/(2*N)  )
        P(i,1:2) = P(i,1:2) + (1/(4*(N-1)))*rand(1,2);
    end
end
P(N*(N-1)/2 + (N+1)/2,1:2) = [0.5,0.5];

mesh = meshstruct(P,T);


