function [] = meshplot2( mesh )

figure()
axis equal; hold on;
set(double(gcf), 'color',[1 1 1]);

NElem = size(mesh.Element, 1);

maxSideNum = 0;
for K = 1:NElem
    maxSideNum = max( maxSideNum, length(mesh.Element{K}) );
end

for K = 1:NElem
    myBoundary = mesh.Node([mesh.Element{K} mesh.Element{K}(1)], :);
    if mod(K,2)
        patch(myBoundary(:,1), myBoundary(:,2), 'g')
    else
        patch(myBoundary(:,1), myBoundary(:,2), 'b')
    end
    % plot(myBoundary(:,1), myBoundary(:,2), 'b', 'LineWidth', 2)
end

end

