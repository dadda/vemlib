function [] = writeNodeEle( mesh, basename )

Nver = size(mesh.Node, 1);
Nelt = length(mesh.Element);

fid = fopen([basename '.node'], 'w');
fprintf(fid, '%d 2 0 0\n', Nver);
for i = 1:Nver
    fprintf(fid, '%i %.16f %.16f\n', i, mesh.Node(i,:));
end
fclose(fid);

fid = fopen([basename '.ele'], 'w');
fprintf(fid, '%d 3 0\n', Nelt);
for i = 1:Nelt
    fprintf(fid, '%i %i %i %i\n', i, mesh.Element{i});
end
fclose(fid);

end
