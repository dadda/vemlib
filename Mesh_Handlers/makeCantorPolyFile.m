function [] = makeCantorPolyFile(filename, maxLevel)

fid = fopen(filename, 'w');

[ ~, xepts] = cantorFunction([], 0, 1, [], [], 0, maxLevel);

Npt = length(xepts);

% Node part
NptTot = 4*Npt + 4;
fprintf(fid, '%i 2 0 0\n', NptTot);

% y = 0
fprintf(fid, '1 0 0\n');
for i = 1:Npt
    fprintf(fid, '%i %.16f 0\n', i+1, xepts(i));
end

% x = 1
fprintf(fid, '%i 1 0\n', Npt+2);
for i = 1:Npt
    fprintf(fid, '%i 1 %.16f\n', i+Npt+2, xepts(i));
end

% y = 1
fprintf(fid, '%i 1 1\n', 2*Npt+3);
for i = 1:Npt
    fprintf(fid, '%i %.16f 1\n', i+2*Npt+3, 1-xepts(i));
end

% x = 0
fprintf(fid, '%i 0 1\n', 3*Npt+4);
for i = 1:Npt
    fprintf(fid, '%i 0 %.16f\n', i+3*Npt+4, 1-xepts(i));
end

% Boundary segments
fprintf(fid, '%i 0\n', NptTot);
for i = 1:NptTot
    fprintf(fid, '%i %i %i\n', i, i, mod(i,NptTot)+1);
end

% No holes
fprintf(fid, '0\n');

fclose(fid);

end
