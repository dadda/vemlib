function [ hmax, hmin, inradii, incenters, gammastar, check ] = computeStatisticsSingleMesh( mesh )
% computeStatisticsSingleMesh   Compute statistics for a polyhedral mesh
%
% Inputs:
%
% - MESH: OVM mesh data structure
%
% Outputs:
%
% - HMAX: array of elements diameters
%
% - HMIN: array of minimal distances between any pair of vertices in elements
%
% - INRADII: array of radii of elements inscribed sphere (implemented only for convex polyhedra)
%
% - INCENTERS: array of inscribed spheres centers (may not be unique - implemented only for
%              convex polyhedra)

%
% Estimate inradii by solving a minmax problem (reference K.Murty 2009)
%

hmax      = zeros(mesh.Nelt, 1);
hmin      = zeros(mesh.Nelt, 1);
gammastar = zeros(mesh.Nelt, 1);
inradii   = zeros(mesh.Nelt, 1);
incenters = zeros(mesh.Nelt, 3);
check     = zeros(mesh.Nelt, 5);

options = optimoptions('fminimax', 'Display', 'off');

parfor K = 1:mesh.Nelt
    element    = construct_element_connectivity(mesh, K);
    
    [~, ~, faces_normals, faces_algdist, faces_edges_normals2D, ...
        faces_diameters, faces_areas, faces_centroids  ] = ...
        faces_features( mesh.coordinates, element, 1, 0, 0, 0 );

    ivertices = unique( horzcat( element{:}) );
    D         = pdist( mesh.coordinates( ivertices, : ) );

    hmax(K) = max(D);
    hmin(K) = min(D);

    vol = ( faces_areas' * faces_algdist ) / 3;

    % Inradius and incenter
    barycenter = sum( mesh.coordinates(ivertices, :), 1 )/length(ivertices);

    Nfc_element = size(element, 1);
                                        
    X0  = zeros(Nfc_element, 3);
    tmp = inf;
    R   = zeros(3,3);
    collapsed_variable = 3*ones(Nfc_element, 1, 'uint8');

    for f = 1:Nfc_element
        X0(f,:) = mesh.coordinates( element{f}(1), : );
        
        % Assemble rotation matrix
        if ( abs(faces_normals(f,3)) < 0.5 )
            % Rotate towards e_z
            ct    = faces_normals(f,3);
            omega = [faces_normals(f,2) -faces_normals(f,1) 0];
        else
            % Rotate towards e_x
            ct    = faces_normals(f,1);
            omega = [0 faces_normals(f,3) -faces_normals(f,2)];
            collapsed_variable(f) = 1;
        end
        %st = norm(omega);    
        st = sqrt(1.0 - ct^2);
        omega = omega / st;

        R(1,1) = ct + omega(1)^2*(1-ct);
        R(2,2) = ct + omega(2)^2*(1-ct);
        R(3,3) = ct + omega(3)^2*(1-ct);

        R(1,2) = -omega(3)*st + omega(1)*omega(2)*(1-ct);
        R(1,3) = omega(2)*st + omega(1)*omega(3)*(1-ct);
        R(2,1) = omega(3)*st + omega(1)*omega(2)*(1-ct);
        R(2,3) = -omega(1)*st + omega(2)*omega(3)*(1-ct);
        R(3,1) = -omega(2)*st + omega(1)*omega(3)*(1-ct);
        R(3,2) = omega(1)*st + omega(2)*omega(3)*(1-ct);

        survived_variables = [1:collapsed_variable(f)-1 collapsed_variable(f)+1:3];

        % rotated_vertices is 2 x Nver
        rotated_vertices = R*mesh.coordinates(element{f}, :)';
        rotated_vertices = rotated_vertices(survived_variables,:)';

        norms = sqrt(faces_edges_normals2D{f}(2:end,1).^2 + faces_edges_normals2D{f}(2:end,2).^2);
        Nf    = [faces_edges_normals2D{f}(2:end,1)./norms faces_edges_normals2D{f}(2:end,2)./norms];

        myobj = @(x) minmaxobj(x, Nf, rotated_vertices);
        [~, ~, maxval] = fminimax(myobj, ...
            faces_centroids(f,:), [], [], [], [], [], [], [], options);
        inradiiF = -maxval;
        tmp2     = inradiiF/faces_diameters(f);

        if tmp > tmp2
            tmp = tmp2;
        end
    end
    
    myobj = @(x) minmaxobj(x, faces_normals, X0);
    [incenters(K,:), ~, maxval] = fminimax(myobj, barycenter, [], [], [], [], [], [], [], options);
    inradii(K) = -maxval;

    check(K,:) = [vol/(hmax(K)^3);
        min(faces_areas)/hmax(K)^2;
        hmin(K)/hmax(K);
        inradii(K)/hmax(K);
        tmp;
    ];

    gammastar(K) = min( check(K,:) );
end


end
