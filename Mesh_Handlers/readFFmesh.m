function mesh = readFFmesh( filename )
% Read mesh saved in FreeFem++ format

fid = fopen(filename);

tmp  = fscanf(fid, '%d', 3);
Nver = tmp(1);
Nele = tmp(2);

Node = zeros(Nver, 2);
for i = 1:Nver
    tmp = fscanf(fid, '%f', 3);
    Node(i,:) = tmp(1:2);
end

Element = cell(Nele, 1);
for i = 1:Nele
    tmp = fscanf(fid, '%d', [1,4]);
    coords = Node( tmp(1:3), : );
    center = sum( coords, 1 ) / 3;
    coords = bsxfun(@minus, coords, center);
    [~, ix] = sort( atan2( coords(:, 2), coords(:, 1) ) );
    Element{i} = tmp( ix );
end

fclose(fid);

mesh         = [];
mesh.Node    = Node;
mesh.Element = Element;

end
