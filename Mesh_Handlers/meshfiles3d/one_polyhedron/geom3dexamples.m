% Create mesh files in ovm format from geom3d examples

addpath ../../
addpath ../../../../geom3d-2016.03.02/geom3d/meshes3d/
addpath ../../../../geom3d-2016.03.02/geom3d/geom3d/

%% Cube
oldmesh = createCube;
mesh = geom3d2ovm(oldmesh);
name = '../one_polyhedron/cube.mat';
save(name, 'mesh')
drawOVMmesh
view(45,45)

%% Octahedron
oldmesh = createOctahedron;
mesh = geom3d2ovm(oldmesh);
name = '../one_polyhedron/octahedron.mat';
save(name, 'mesh')
drawOVMmesh
view(45,45)

%% Cube-octahedron
oldmesh = createCubeOctahedron;
mesh = geom3d2ovm(oldmesh);
name = '../one_polyhedron/cube-octahedron.mat';
save(name, 'mesh')
drawOVMmesh
view(45,45)

%% Icosahedron
oldmesh = createIcosahedron;
mesh = geom3d2ovm(oldmesh);
name = '../one_polyhedron/icosahedron.mat';
save(name, 'mesh')
drawOVMmesh
view(45,45)

%% Dodecahedron
oldmesh = createDodecahedron;
mesh = geom3d2ovm(oldmesh);
name = '../one_polyhedron/dodecahedron.mat';
save(name, 'mesh')
drawOVMmesh
view(45,45)

%% Soccer ball
oldmesh = createSoccerBall;
mesh = geom3d2ovm(oldmesh);
name = '../one_polyhedron/soccerball.mat';
save(name, 'mesh')
drawOVMmesh
view(45,45)

%% Tetrahedron
oldmesh = createTetrahedron;
mesh = geom3d2ovm(oldmesh);
name = '../one_polyhedron/tetrahedron.mat';
save(name, 'mesh')
drawOVMmesh
view(45,45)

%% Tetrakaidecahedron
oldmesh = createTetrakaidecahedron;
mesh = geom3d2ovm(oldmesh);
name = '../one_polyhedron/tetrakaidecahedron.mat';
save(name, 'mesh')
drawOVMmesh
view(45,45)
