function mesh = voroppcell2ovm( basename )
% Generated a mesh in OVM format from data (vertices coordinates and face
% to vertices connectivity) for a single Voronoid cell generated with Voro++

mesh.Nelt = 1;

% Read vertices
fid = fopen([basename '_v.txt']);
tline = fgetl(fid);

mesh.Nver = str2double(tline);
mesh.coordinates = zeros(mesh.Nver, 3);

for v = 1:mesh.Nver
    tline = fgetl(fid);
    mesh.coordinates(v, :) = str2num(tline); %#ok<*ST2NM>
end

fclose(fid);

%%% Check for pathological situations

% Check for coincident vertices
[itmp, jtmp] = find( squareform(pdist(mesh.coordinates)) < 1e-16 );
if sum(itmp < jtmp)
    warning('Found degenerate vertices')
end
to_be_eliminated = jtmp(itmp < jtmp);
to_be_kept       = itmp(itmp < jtmp);
mesh.coordinates(to_be_eliminated, :) = [];
mesh.Nver = mesh.Nver - length(to_be_eliminated);

% Read faces and create element2faces. We are assuming a consistent
% orientation has been given to faces, i.e. clockwise or counter-clockwise.

fid   = fopen([basename '_f.txt']);
tline = fgetl(fid);
Nfc   = str2double(tline);

faces               = cell(Nfc,1);
mesh.element2hfaces = zeros(1, Nfc);
%mesh.element2hfaces(1) = Nfc;

% Compute barycenter
barycenter_el = sum(mesh.coordinates,1)/mesh.Nver;

Ned = 0;
fc  = 1;
for f = 1:Nfc
    tline = fgetl(fid);
    face  = str2num(tline);
    face_order = face(1);
    faces{fc}  = zeros(1, face_order+1);
    faces{fc}(1) = face_order;
    previousnode = [face_order 1:face_order-1];
    nextnode     = [2:face_order 1];
    for i = 1:face_order
        % Remember C++ numbering starts from 0 ...
        node  = face(i+1) + 1;
        inode = find(node == to_be_eliminated);
        if inode
            if ( face(previousnode(i)+1)+1 == to_be_kept(inode) ) || ...
               ( face(nextnode(i)+1)+1 == to_be_kept(inode) )
                % If either the previous or the next node is one of the survivals, the current
                % node just collapses (this will be done by removing zero 
                % entries later) and the order of the face is
                % decreased
                faces{fc}(1)   = faces{fc}(1)-1;
            else
                % If the current node has to be removed but its neighbors
                % with respect to the current face are not in the survival
                % list, then we simply replace the current node without
                % decreasing the face order.
                faces{fc}(i+1) = to_be_kept(inode) - sum(to_be_kept(inode) > to_be_eliminated);
            end
        else
            faces{fc}(i+1) = node - sum(node > to_be_eliminated);
        end
    end
    faces{fc}(faces{fc} == 0) = [];

    % Check whether to include the current face or not. More criteria to
    % exclude faces could be implemented here.
    if length(faces{fc}) > 3
        
%         C = sum(mesh.coordinates(faces{fc}(2:end),:),1)/faces{fc}(1);
%         
%         v1 = [(mesh.coordinates(faces{fc}(2:end),1)-C(1)) ...
%               (mesh.coordinates(faces{fc}(2:end),2)-C(2)) ...
%               (mesh.coordinates(faces{fc}(2:end),3)-C(3))];
%         v2 = [(mesh.coordinates(faces{fc}([3:faces{fc}(1)+1 2]),1)-C(1)) ...
%               (mesh.coordinates(faces{fc}([3:faces{fc}(1)+1 2]),2)-C(2)) ...
%               (mesh.coordinates(faces{fc}([3:faces{fc}(1)+1 2]),3)-C(3))];
        v1 = [(mesh.coordinates(faces{fc}(3:end-1),1)-mesh.coordinates(faces{fc}(2),1)) ...
              (mesh.coordinates(faces{fc}(3:end-1),2)-mesh.coordinates(faces{fc}(2),2)) ...
              (mesh.coordinates(faces{fc}(3:end-1),3)-mesh.coordinates(faces{fc}(2),3))];

        v2 = [(mesh.coordinates(faces{fc}(4:end),1)-mesh.coordinates(faces{fc}(2),1)) ...
              (mesh.coordinates(faces{fc}(4:end),2)-mesh.coordinates(faces{fc}(2),2)) ...
              (mesh.coordinates(faces{fc}(4:end),3)-mesh.coordinates(faces{fc}(2),3))];
        
%         v1 = mesh.coordinates(faces{fc}(3),:) - mesh.coordinates(faces{fc}(2),:);
%         v2 = mesh.coordinates(faces{fc}(4),:) - mesh.coordinates(faces{fc}(3),:);
        normal = sum([(v1(:,2).*v2(:,3) - v1(:,3).*v2(:,2)) ...
                      (v1(:,3).*v2(:,1) - v1(:,1).*v2(:,3)) ...
                      (v1(:,1).*v2(:,2) - v1(:,2).*v2(:,1))],1);
        normal = normal / norm(normal);

        % Check on which size the barycenter is to find the correct half-face
        check_test = dot(normal, barycenter_el - mesh.coordinates(faces{fc}(2),:));
        if check_test > 0
            % The face is clockwise-oriented. Assign
            % even half-face (slightly different from OpenVolumeMesh)
            mesh.element2hfaces(fc) = 2*fc;
        else        
            % The face is counter-clockwise oriented. Assign
            % odd half-face (slightly different from OpenVolumeMesh)
            mesh.element2hfaces(fc) = 2*fc-1;
        end

        % Count total edge number (with duplicates included) for computational efficiency
        Ned = Ned + faces{fc}(1);
        
        fc = fc + 1;
    end
end
fclose(fid);

Nfc = fc - 1;
mesh.Nfc = Nfc;

mesh.face2elements = -ones(Nfc,2);
mesh.face2elements(:,1) = 1;

faces = faces(1:Nfc);

%mesh.element2hfaces(1) = Nfc;
mesh.element2hfaces(Nfc+1:end) = [];
mesh.element2hfaces = mat2cell(mesh.element2hfaces, 1, Nfc);

% Create all edges by looping over faces
edges = zeros(Ned,2);
counter = 0;
for fc = 1:Nfc
    iterfc = [2:faces{fc}(1) 1];
    for i = 1:faces{fc}(1)
        counter = counter + 1;
        edges(counter, :) = faces{fc}([1+i 1+iterfc(i)]);
    end
end
edges = sort(edges, 2);
mesh.edge2vertices = unique(edges, 'rows');
mesh.Ned = size(mesh.edge2vertices, 1);

% Create face2edges
mesh.face2hedges = cell(Nfc,1);
for fc = 1:Nfc
    iterfc = [2:faces{fc}(1) 1];
    %mesh.face2hedges{fc}(1) = faces{fc}(1);
    mesh.face2hedges{fc} = zeros(1,faces{fc}(1));
    for i = 1:faces{fc}(1)
        sorted_edge = sort(faces{fc}([1+i 1+iterfc(i)]));
        [~, ~, e] = intersect(sorted_edge, mesh.edge2vertices, 'rows');
        if issorted(faces{fc}([1+i 1+iterfc(i)]))
            mesh.face2hedges{fc}(i) = 2*e-1;
        else
            mesh.face2hedges{fc}(i) = 2*e;
        end
    end
end

save([basename '.mat'], 'mesh')

end
