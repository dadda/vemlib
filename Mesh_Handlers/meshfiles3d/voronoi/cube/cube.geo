lc = 1.75;

Point(1) = {-1,-1,-1,lc};
Point(2) = {1,-1,-1,lc};
Point(3) = {1,1,-1,lc};
Point(4) = {-1,1,-1,lc};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};

Line Loop(1) = {1,2,3,4};

Plane Surface(1) = {1};

Extrude{0,0,2} { Surface{1}; }

Surface Loop(10) = {1, 13, 17, 21, 25, 26};
Volume(20) = {10};

Physical Volume(30) = {20};