function [] = writeNodeEleEdge( mesh, basename )

fid = fopen([basename '.node'], 'w');
fprintf(fid, '%d 2 0 1\n', mesh.Nver);
for i = 1:mesh.Nver
    fprintf(fid, '%i %.16f %.16f 0\n', i, mesh.coordinates(i,:));
end
fclose(fid);

fid = fopen([basename '.ele'], 'w');
fprintf(fid, '%d 3 0\n', mesh.Nelt);
for i = 1:mesh.Nelt
    NverLoc = mesh.elements(i,1);
    fprintf(fid, '%i %i %i %i\n', i, mesh.elements(i,2:NverLoc+1));
end
fclose(fid);

fid = fopen([basename '.edge'], 'w');
fprintf(fid, '%d 1\n', mesh.Nfc);
for i = 1:mesh.Nfc
    flag = mesh.edges(i,3);
    if flag == 1 && all( abs( mesh.coordinates(mesh.edges(i,1:2), 2) ) < 1e-12 )
        flag = 2;
    end
    fprintf(fid, '%i %i %i %i\n', i, mesh.edges(i,1:2), flag);
end
fclose(fid);

end
