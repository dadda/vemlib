%% Choose the hexahedral mesh to generate

% % Create regular hexahedral (cubes) mesh
% n = 33;
% d = linspace(0,1,n);
% [x,y,z] = meshgrid(d,d,d);
% name = 'cubes';

% Create slightly nonuniform hexahedral mesh
d = random_refine(nodes);
n = length(d);
[x,y,z] = meshgrid(d,d,d);
name = 'hexa';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mesh.coordinates = [x(:) y(:) z(:)];
mesh.Nver        = size(mesh.coordinates,1);

mesh.Nelt = (n-1)^3;
elements  = zeros(mesh.Nelt,8);

E = 0;
for k = 1:n-1
    for i = 1:n-1
        for j = 1:n-1
            E = E + 1;
            elements(E,1) = (k-1)*n^2 + (i-1)*n + j;
            elements(E,2) = (k-1)*n^2 + (i-1)*n + j+1;
            elements(E,3) = (k-1)*n^2 + i*n + j;
            elements(E,4) = (k-1)*n^2 + i*n + j+1;
            elements(E,5) = k*n^2 + (i-1)*n + j;
            elements(E,6) = k*n^2 + (i-1)*n + j+1;
            elements(E,7) = k*n^2 + i*n + j;
            elements(E,8) = k*n^2 + i*n + j+1;
        end
    end
end

%% Create data structures required by the VEM code

% Form all edges
edges = [elements(:,[1 2]);
         elements(:,[2 4]);
         elements(:,[4 3]);
         elements(:,[3 1]);
         elements(:,[1 5]);
         elements(:,[2 6]);
         elements(:,[3 7]);
         elements(:,[4 8]);
         elements(:,[5 6]);
         elements(:,[6 8]);
         elements(:,[8 7]);
         elements(:,[7 5])];     
edges = sort(edges, 2);

mesh.edge2vertices = unique(edges, 'rows');
mesh.Ned = size(mesh.edge2vertices, 1);

% Form all faces
faces = [elements(:, [1 2 4 3]);
         elements(:, [1 5 6 2]);
         elements(:, [2 6 8 4]);
         elements(:, [3 4 8 7]);
         elements(:, [1 3 7 5]);
         elements(:, [5 7 8 6])];

% Code is slightly different from tetrahedral case - need to keep
% distinction between sorted and unsorted objects
sorted_faces = sort(faces, 2);
[~, ix, jx] = unique(sorted_faces, 'rows');
faces = faces(ix,:);
mesh.Nfc = size(faces, 1);

% Use geom3d toolbox to draw surface mesh
addpath 'geom3d/'
drawMesh(mesh.coordinates, faces);

element2faces = zeros(mesh.Nelt, 6);
element2faces(:, 1) = jx(1:mesh.Nelt);
element2faces(:, 2) = jx(mesh.Nelt+1:2*mesh.Nelt);
element2faces(:, 3) = jx(2*mesh.Nelt+1:3*mesh.Nelt);
element2faces(:, 4) = jx(3*mesh.Nelt+1:4*mesh.Nelt);
element2faces(:, 5) = jx(4*mesh.Nelt+1:5*mesh.Nelt);
element2faces(:, 6) = jx(5*mesh.Nelt+1:end);

mesh.face2elements = -ones(mesh.Nfc, 2);
for E = 1:mesh.Nelt
    for fc = 1:6
        if mesh.face2elements(element2faces(E,fc),1) == -1
            mesh.face2elements(element2faces(E,fc),1) = E;
        else
            mesh.face2elements(element2faces(E,fc),2) = E;
        end
    end
end

mesh.element2hfaces = zeros(mesh.Nelt, 6);
%mesh.element2hfaces(:, 1) = 6;
for fc = 1:mesh.Nfc
    Ktmp = mesh.face2elements(fc,1);
	% Local face numbering
	localfc = find(element2faces(Ktmp,:) == fc);

	% Compute barycenter
	barycenter_el = sum(mesh.coordinates(elements(Ktmp,:),:),1)/8.;
	% Compute normal
	v1 = mesh.coordinates(faces(fc,2),:) - mesh.coordinates(faces(fc,1),:);
	v2 = mesh.coordinates(faces(fc,3),:) - mesh.coordinates(faces(fc,2),:);
    normal = [v1(2)*v2(3) - v1(3)*v2(2), ...
              v1(3)*v2(1) - v1(1)*v2(3), ...
              v1(1)*v2(2) - v1(2)*v2(1)];
    
    % Check on which size the barycenter is to find the correct half-face
    check_test = dot(normal, barycenter_el - mesh.coordinates(faces(fc,1),:));
    if check_test > 0
        % The face is clockwise-oriented. Assign
        % even half-face (slightly different from OpenVolumeMesh)
        mesh.element2hfaces(Ktmp, localfc) = 2*fc;
        Ktmp = mesh.face2elements(fc,2);
        if Ktmp ~= -1
            % For the adjacent element, fc is counter-clockwise oriented
            localfc = find(element2faces(Ktmp,:) == fc);
            mesh.element2hfaces(Ktmp, localfc) = 2*fc-1;
        end
    else        
        % The face is counter-clockwise oriented. Assign
        % odd half-face (slightly different from OpenVolumeMesh)
        mesh.element2hfaces(Ktmp, localfc) = 2*fc-1;
        Ktmp = mesh.face2elements(fc,2);
        if Ktmp ~= -1
            % For the adjacent element, fc is counter-clockwise oriented
            localfc = find(element2faces(Ktmp,:) == fc);
            mesh.element2hfaces(Ktmp, localfc) = 2*fc;
        end        
    end
end
mesh.element2hfaces = mat2cell(mesh.element2hfaces, ones(mesh.Nelt,1), 6);

mesh.face2hedges = zeros(mesh.Nfc,4);
%mesh.face2hedges(:,1) = 4;
nextfc = [2,3,4,1];
for fc = 1:mesh.Nfc
    for j = 1:4
        sorted_edge = sort(faces(fc,[j nextfc(j)]));
        [~, ~, e] = intersect(sorted_edge, mesh.edge2vertices, 'rows');
        if issorted(faces(fc,[j nextfc(j)]))
            mesh.face2hedges(fc,j) = 2*e-1;
        else
            mesh.face2hedges(fc,j) = 2*e;
        end
    end
end
mesh.face2hedges = mat2cell(mesh.face2hedges, ones(mesh.Nfc,1), 4);

%% Save mesh
name = sprintf('meshfiles3d/hexahedra/%s_Nelt%06d.mat', name, mesh.Nelt);
save(name, 'mesh')



