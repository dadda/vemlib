%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate Voronoi mesh of DICAr logo

addpath ../
addpath ../PolyMesher_Addons/
addpath ../../../PolyMesher/

rng('shuffle')

% Domain         = @DICArDomain;
% xMin           = 0;
% xMax           = 12;
% yMin           = 0;
% yMax           = 5;
% BdBox          = [xMin xMax yMin yMax];
% myElementNodes = BdBox;

% Domain         = @ponteCoperto;
% xMin           = 0;
% xMax           = 30;
% yMin           = 0;
% yMax           = 8;
% BdBox          = [xMin xMax yMin yMax];
% myElementNodes = BdBox;

% Domain         = @thankYou;
% xMin           = 0;
% xMax           = 7;
% yMin           = 0;
% yMax           = 5;
% BdBox          = [xMin xMax yMin yMax];
% myElementNodes = BdBox;

Domain         = @voroDomain;
xMin           = 0;
xMax           = 1;
yMin           = 0;
yMax           = 1;
BdBox          = [ xMin xMax yMin yMax ];
myElementNodes = [ 0 0;
                   1 0;
                   1 1;
                   0 1 ];

% Number of polygons
NElem   = 30;

% Number of Lloyd iterations
MaxIter = 100;

% Random distribution for seed points
randType = 'uniform';

[Node,Element] = PolyMesher_modified(Domain, NElem, MaxIter, randType, myElementNodes, BdBox);
