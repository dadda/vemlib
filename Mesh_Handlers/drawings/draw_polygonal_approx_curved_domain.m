
addpath ../
addpath ../../../PolyMesher/
addpath ../PolyMesher_Addons/

rng('shuffle')

Nver     = 8;
NElem    = 30;
MaxIter  = 100;
randType = 'uniform';

dtheta = 2 * pi / Nver;
theta  = (0:Nver-1) * dtheta;
nodes  = exp(1i*theta);

xver = real(nodes);
yver = imag(nodes);

Domain         = @voroDomain;
myElementNodes = [xver' yver'];
xMin           = -1;
xMax           = 1;
yMin           = -1;
yMax           = 1;
BdBox          = [xMin xMax yMin yMax];

%
%
%

figure()
hold on
axis equal
axis off
% grid on
set(gcf, 'color', [1 1 1]);

% Curved domain (circle)
theta = linspace(0, 2*pi, 10000);

plot(cos(theta), sin(theta), 'r', 'Linewidth', 2)
legend({'\Omega'},'Fontsize',20,'Location','bestoutside')

% Add polygonal approximation
plot([xver, xver(1)], [yver, yver(1)], 'g', 'Linewidth', 2)
legend({'\Omega', '\Omega_h'},'Fontsize',20,'Location','bestoutside')

% Add Voronoi mesh
[Node,Element] = PolyMesher_modified(Domain,NElem,MaxIter,randType,myElementNodes,BdBox);
for K = 1:NElem
    myBoundary = Node([Element{K} Element{K}(1)], :);
    plot(myBoundary(:,1), myBoundary(:,2), 'b', 'LineWidth', 2)
end
legend({'\Omega', '\Omega_h', 'T_h'},'Fontsize',20,'Location','bestoutside')

% Replot polygonal boundary
plot([xver, xver(1)], [yver, yver(1)], 'g', 'Linewidth', 2)
