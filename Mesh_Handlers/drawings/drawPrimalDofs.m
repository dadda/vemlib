
%
% Draw potential primal degrees of freedom
%

addpath ../../FETI3D
addpath ../
addpath /home/daniele/Downloads/altmany-export_fig-5be2ca4/

load ../meshfiles3d/voronoi/cube/unitcube_voro_v02_Nelt000128.mat

k = 1;

[ vertexDofs, edgeInternalDofs, faceInternalDofs, ...
  internalDofs, boundaryDofs ] = createPreliminaryDofPartition( mesh, k );

refElementFacesByBoundary = groupFacesByBoundary( mesh );

selectPrimal = input('Which primal dofs do you want to plot (V/E/F = 1/2/3)? ');

figure()
axis off;
view(45,45);
set(gcf,'color',[1 1 1]);

for E = 1:mesh.Nelt
    element = construct_element_connectivity(mesh, E);
    Nfc_E = numel(element);

    tfaces = floor( (mesh.element2hfaces{E}+1)/2 );
    for f = 1:Nfc_E
        if mesh.face2elements( tfaces(f), 2 ) == -1
            patch( mesh.coordinates( [element{f} element{f}(1)], 1), ...
                   mesh.coordinates( [element{f} element{f}(1)], 2), ...
                   mesh.coordinates( [element{f} element{f}(1)], 3), ...
                   'w', 'EdgeAlpha', 0.2, 'FaceAlpha', 0 );
        end
    end

end

hold on;
m = 0.99;
refEdge2Vertices = [1 2;
                    2 4;
                    3 4;
                    1 3;
                    1 5;
                    2 6;
                    3 7;
                    4 8;
                    5 6;
                    6 8;
                    7 8;
                    5 7];

switch selectPrimal
    case 1
        % Draw primal vertices
        plot3(mesh.coordinates(vertexDofs,1), mesh.coordinates(vertexDofs,2), mesh.coordinates(vertexDofs,3), ...
              'ro', 'MarkerFaceColor', 'r', 'MarkerSize', 10)
          
        for i = 1:12
            plot3( mesh.coordinates( vertexDofs( refEdge2Vertices(i,:) ), 1 ), ...
                   mesh.coordinates( vertexDofs( refEdge2Vertices(i,:) ), 2 ), ...
                   mesh.coordinates( vertexDofs( refEdge2Vertices(i,:) ), 3 ), ...
                   'k' )
        end

        savefig('primalV');
        export_fig('primalV', '-pdf', '-transparent')

    case 2
        % edge averages in the primal space
        
        myShrink = @(coords,a)([coords(:,1:a-1), m*coords(:,a) + (1-m)/2, coords(:,a+1:end)]);
        
        shrinkMap = [1 2 1 2 3 3 3 3 1 2 1 2];
                        
        for i = 1:12
            newCoords = myShrink( mesh.coordinates( vertexDofs( refEdge2Vertices(i,:) ), : ), ...
                                  shrinkMap(i) );
            plot3( newCoords(:,1), newCoords(:,2), newCoords(:,3), ...
                   'b', 'LineWidth', 3 )
               
%             plot3( mesh.coordinates(edgeInternalDofs{i},1), mesh.coordinates(edgeInternalDofs{i},2), mesh.coordinates(edgeInternalDofs{i},3), ...
%                    'yo', 'MarkerFaceColor', 'y', 'MarkerSize', 7, 'MarkerEdgeColor', 'k' )
        end

        savefig('primalE');
        export_fig('primalE', '-pdf', '-transparent')
        
    case 3

        myShrink2 = @(coords,a)( [ coords(:,1:a(1)-1) ...
                                   m*coords(:,a(1)) + (1-m)/2 ...
                                   coords(:,a(1)+1:a(2)-1) ...
                                   m*coords(:,a(2)) + (1-m)/2 ...
                                   coords(:,a(2)+1:3) ] );
                               
        refFace2Vertices = [ 1 3 7 5;
                             2 4 8 6;
                             1 2 6 5;
                             3 4 8 7;
                             1 2 4 3;
                             5 6 8 7 ];
                         
        shrinkMap = [ 2 3;
                      2 3;
                      1 3;
                      1 3;
                      1 2;
                      1 2 ];

        for i = 1:12
            plot3( mesh.coordinates( vertexDofs( refEdge2Vertices(i,:) ), 1 ), ...
                   mesh.coordinates( vertexDofs( refEdge2Vertices(i,:) ), 2 ), ...
                   mesh.coordinates( vertexDofs( refEdge2Vertices(i,:) ), 3 ), ...
                   'k' )
        end

        for i = 3 %[2 3 6]
            newCoords = myShrink2( mesh.coordinates( vertexDofs( refFace2Vertices(i,:) ), : ), ...
                                   shrinkMap(i,:) );
            patch( newCoords(:,1), newCoords(:,2), newCoords(:,3), ...
                   [1, 165/255, 0], 'EdgeAlpha', 0.3, 'FaceAlpha', 0.3 );
               
%             plot3( mesh.coordinates(faceInternalDofs{i},1), mesh.coordinates(faceInternalDofs{i},2), mesh.coordinates(faceInternalDofs{i},3), ...
%                    'go', 'MarkerFaceColor', 'g', 'MarkerSize', 7, 'MarkerEdgeColor', 'k' )
               
            % Draw central patch
%             zeroCoord = floor( (i+1)/2 );
%             centralNode = [0.5 0.5 0.5];
%             centralNode(zeroCoord) = 0;
            centralNode = [0.5494 0 0.3338]; % This one has been selected ad hoc

            [~, tmp] = min( pdist2( mesh.coordinates, centralNode ) );
            
            plot3( mesh.coordinates(tmp,1), ...
                   mesh.coordinates(tmp,2), ...
                   mesh.coordinates(tmp,3), ...
                   'go', 'MarkerFaceColor', 'g', 'MarkerSize', 7, 'MarkerEdgeColor', 'k' )

            for f = (refElementFacesByBoundary{i})'
                % Connectivity info for a face
                tedges = floor( ( mesh.face2hedges{f}+1 )/2 );
                edges_orientation = mod(mesh.face2hedges{f}, 2);
                edges = mesh.edge2vertices(tedges,:)';
                face = edges_orientation .* edges(1,:) + (1-edges_orientation) .* edges(2,:);
                if intersect(tmp, face)
                    patch( mesh.coordinates( face, 1), ...
                           mesh.coordinates( face, 2), ...
                           mesh.coordinates( face, 3), ...
                    [1, 165/255, 0], 'EdgeAlpha', 0.3, 'FaceAlpha', 1 );
                end
            end
        end
        
        savefig('singlePrimalFSingleNode');
        export_fig('singlePrimalFSingleNode', '-pdf', '-transparent')
end
