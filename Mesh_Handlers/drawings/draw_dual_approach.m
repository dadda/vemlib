
%
% Script used to generate plots of dual approach for 2d meshes and to plot
% a polygonal dual mesh of THANK YOU
%

addpath ..
addpath ../../Optimize
addpath ../pmgo/matlab_source

% basename = '../../../triangle/meshes/unitsquare.1';
basename = '../../../triangle/meshes/thankYou.1';
tolCollapsePt = 1e-12;
tol = tolCollapsePt;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Mark primal mesh

fid  = fopen([basename '.node']);
fid2 = fopen([basename '_marked.node'], 'w');

tmp       = fscanf(fid, '%d', 4);
Nver      = tmp(1);
vertices  = zeros(Nver, 2);
hasMarker = tmp(4);

fprintf(fid2, '%d 2 0 1\n', Nver);

xmin = 0;
xmax = 1;
ymin = 0;
ymax = 1;

for i = 1:Nver
    tmp           = fscanf(fid, '%f', [1,3+hasMarker]);
    vertices(i,:) = tmp(2:3);

%     % Mark generic unit square mesh
%     if ( abs ( vertices(i,1) - xmin ) < tol || abs ( xmax - vertices(i,1) ) < tol )
%         if ( abs ( vertices(i,2) - ymin ) < tol || abs ( ymax - vertices(i,2) ) < tol )
%             flag = 2;
%         else
%             flag = 1;
%         end
%     else
%         if ( abs ( vertices(i,2) - ymin ) < tol || abs ( ymax - vertices(i,2) ) < tol )
%             flag = 1;
%         else
%             flag = 0;
%         end
%     end

    % Mark meshes in order to keep all boundary nodes
    if tmp(4) == 1
        flag = 2;
    else
        flag = 0;
    end

    fprintf(fid2, '%d %.16f %.16f %d\n', tmp(1:3), flag);
end

fclose(fid);
fclose(fid2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Vertices
fid  = fopen([basename '_marked.node']);
tmp  = fscanf(fid, '%d', 4);
Nver = tmp(1);

vertices = zeros(Nver, 3);
for i = 1:Nver
    tmp = fscanf(fid, '%f', [1,4]);
    vertices(i,:)  = tmp(2:4);
end
fclose(fid);

% Edges
fid = fopen([basename '.edge']);
tmp = fscanf(fid, '%d', 2);
Ned = tmp(1);

edges = zeros(Ned, 3);
for i = 1:Ned
    tmp = fscanf(fid, '%d', [1,4]);
    edges(i,:) = tmp(2:4);
end
fclose(fid);

% Elements (triangles)
fid     = fopen([basename '.ele']);
tmp     = fscanf(fid, '%d', 3);
Nelt    = tmp(1);
eleAttr = tmp(3);

elements = zeros(Nelt, 3);
for i = 1:Nelt
    tmp = fscanf(fid, '%d', [1,4]);
    elements(i,:) = tmp(2:4);
    if eleAttr
        fscanf(fid, '%f');
    end
end

fclose(fid);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create auxiliary connectivity information

edges(:,1:2) = sort( edges(:,1:2), 2 );
edges        = unique(edges, 'rows');

%
% Create vertices to edges connectivity information
%

v2e    = cell(Nver, 1);
v2eDim = zeros(Nver, 1);

for i = 1:Ned
    v2eDim( edges(i,1) ) = v2eDim( edges(i,1) ) + 1;
    v2eDim( edges(i,2) ) = v2eDim( edges(i,2) ) + 1;
end
v2eDimCopy = v2eDim;

% Second loop to allocate memory
for i = 1:Nver
    v2e{i} = zeros( 1, v2eDim(i) );
end

for i = 1:Ned
    v2e{ edges(i,1) }(v2eDim(edges(i,1)) - v2eDimCopy(edges(i,1)) + 1) = i;
    v2e{ edges(i,2) }(v2eDim(edges(i,2)) - v2eDimCopy(edges(i,2)) + 1) = i;

    v2eDimCopy(edges(i,1:2)) = v2eDimCopy(edges(i,1:2)) - 1;
end

% Check
if any(v2eDimCopy)
    error('Something is wrong')
end

%
% Triangle to edge connectivity
%

tmp = [ elements(:,1) elements(:,2);
        elements(:,1) elements(:,3);
        elements(:,2) elements(:,3) ];
tmp = sort(tmp,2);
[tmpEdges, ix, jx] = unique( tmp, 'rows' );
t2e = [ jx(1:Nelt) jx(Nelt+1:2*Nelt) jx(2*Nelt+1:end) ];

if any( any( tmpEdges ~= edges(:,1:2) ) )
    error('Error in t2e construction')
end

%
% Create edge to triangle connectivity
%

e2t = -ones(Ned, 2);

for i = 1:Nelt
    for j = 1:3
        if e2t(t2e(i,j),1) == -1
            e2t(t2e(i,j),1) = i;
        else
            e2t(t2e(i,j),2) = i;
        end
    end
end

%
% Plot original triangular mesh
%

figure()
hold on
axis equal
axis off
set(gcf, 'color', [1 1 1]);

for i = 1:Ned
    x = vertices(edges(i,1:2),1);
    y = vertices(edges(i,1:2),2);
    plot(x,y,'k.-','Markersize',20)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Circumcenters
fid  = fopen([basename '.v.node']);
fscanf(fid, '%d', 4);

cc = zeros(Nelt, 2);
for i = 1:Nelt
    tmp = fscanf(fid, '%f', [1,3]);
    cc(i,:)  = tmp(2:3);
    plot(cc(i,1), cc(i,2), 'ko')
end
fclose(fid);

% Voronoi edges
fid     = fopen([basename '.v.edge']);
tmp     = fscanf(fid, '%d', 2);
NedVoro = tmp(1);

for i = 1:NedVoro
    tmp = fscanf(fid, '%f', [1,3]);
    if tmp(3) == -1
        tmpB = fscanf(fid, '%f', 2);
        quiver(cc(tmp(2),1), cc(tmp(2),2), tmpB(1), tmpB(2), ...
               ':', 'ShowArrowHead', 'off', 'LineWidth', 1, 'Color', 'k')
    else
        plot(cc(tmp(2:3),1), cc(tmp(2:3),2), 'k:', 'Linewidth', 1)
    end
end
fclose(fid);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Dual mesh generation

numDualVertices = Nelt ...
                  + sum(edges(:,3) == 1) ...
                  + sum(vertices(:,3) == 2);

dualVertices         = zeros(numDualVertices, 2);
dualVerticesPointers = -ones(numDualVertices, 1);
dualVerticesCounter  = 0;

%
% Step 1
%
% Create a dual vertex at a central point in each primal mesh triangle.
%

for i = 1:Nelt

    [cc, xi, eta] = tricircumcenter( vertices(elements(i,1),1:2), ...
                                     vertices(elements(i,2),1:2), ...
                                     vertices(elements(i,3),1:2) );

    ccIsInside = xi >= 0 && eta >= 0 && (xi+eta) <= 1;
    if ccIsInside
        point = cc + vertices(elements(i,1),1:2);
    else
        % point = sum(vertices(elements(i,1:3),1:2),1) / 3;
        point = cc + vertices(elements(i,1),1:2);
    end

    % Check whether the point should be added or not
    itmp = rangesearch( dualVertices(1:dualVerticesCounter,:), point, tolCollapsePt );
    itmp = itmp{1};
    if isempty(itmp)
        dualVerticesCounter = dualVerticesCounter + 1;

        dualVertices(dualVerticesCounter,:) = point;
        dualVerticesPointers(i)             = dualVerticesCounter;
    else
        dualVerticesPointers(i)             = itmp(1);
    end
end

counter = Nelt;

%
% Step 2
%
% Create a dual vertex at mid-point of each primal mesh edge classified
% on a model edge.
%

edges2dualVertices = -ones(Ned, 1);

for i = 1:Ned
    if edges(i,3) == 1
        counter = counter + 1;
        edges2dualVertices(i) = counter;

        newPt = sum( vertices(edges(i,1:2),1:2), 1 ) / 2.;

        itmp = rangesearch( dualVertices(1:dualVerticesCounter,:), newPt, tolCollapsePt );
        itmp = itmp{1};
        if isempty(itmp)
            dualVerticesCounter = dualVerticesCounter + 1;

            dualVertices(dualVerticesCounter,:) = newPt;
            dualVerticesPointers(counter)       = dualVerticesCounter;
        else
            dualVerticesPointers(counter)       = itmp(1);
        end
    end
end

%
% Step 3
%
% Create a dual vertex at each primal mesh vertex classified on a model
% vertex.
%

vertex2dualVertices = -ones(Nver, 1);

for i = 1:Nver
    if vertices(i,3) == 2
        counter = counter + 1;
        vertex2dualVertices(i) = counter;

        itmp = rangesearch( dualVertices(1:dualVerticesCounter,:), vertices(i,1:2), tolCollapsePt );
        itmp = itmp{1};
        if isempty(itmp)
            dualVerticesCounter = dualVerticesCounter + 1;

            dualVertices(dualVerticesCounter,:) = vertices(i,1:2);
            dualVerticesPointers(counter)       = dualVerticesCounter;
        else
            dualVerticesPointers(counter)       = itmp(1);
        end

    end
end

%
% Trim dualVertices
%
dualVertices(dualVerticesCounter+1:end,:) = [];
numDualVertices                           = dualVerticesCounter;

%
% Step 4
%

dualElements       = cell(Nver, 1);
dualElementCounter = 0;

for i = 1:Nver
    if vertices(i,3) == 0
        NverLoc     = v2eDim(i);
        dualElement = zeros(1, NverLoc);

        currentEdge = v2e{i}(1);
        idx1 = e2t(currentEdge,1);
        idx2 = e2t(currentEdge,2);

        % If the edge is not collapsing, add it
        if dualVerticesPointers(idx1) ~= dualVerticesPointers(idx2)
            dualElement(1) = dualVerticesPointers(idx1);
        end

        idx1 = idx2;
        for m = 2:NverLoc
            tmp = intersect( t2e(idx1,:), v2e{i} );
            if length(tmp) ~= 2
                error('Error while creating element from internal vertex')
            else
                if tmp(1) == currentEdge
                    currentEdge = tmp(2);
                else
                    currentEdge = tmp(1);
                end
                if e2t(currentEdge,1) == idx1
                    idx2 = e2t(currentEdge,2);
                else
                    idx2 = e2t(currentEdge,1);
                end

                % If the edge is not collapsing, add it
                if dualVerticesPointers(idx1) ~= dualVerticesPointers(idx2)
                    dualElement(m) = dualVerticesPointers(idx1);
                end

                idx1 = idx2;
            end
        end

    else
        NverLoc = v2eDim(i) + 1;
        if vertices(i,3) == 2
            NverLoc = NverLoc + 1;
        end
        dualElement = zeros(1, NverLoc);

        test     = edges(v2e{i},3) == 1;
        bndEdges = v2e{i}(test);
        if numel(bndEdges) ~= 2
            error('Error while creating an edge from a boundary vertex')
        end

        currentEdge = bndEdges(1);
        idx1 = edges2dualVertices(currentEdge);
        idx2 = e2t(currentEdge,1);

        % If the edge is not collapsing, add it
        if dualVerticesPointers(idx1) ~= dualVerticesPointers(idx2)
            dualElement(1) = dualVerticesPointers(idx1);
        end

        idx1 = idx2;
        for m = 2:v2eDim(i)-1
            tmp = intersect( t2e(idx1,:), v2e{i} );
            if length(tmp) ~= 2
                error('Error while creating element from internal vertex')
            else
                if tmp(1) == currentEdge
                    currentEdge = tmp(2);
                else
                    currentEdge = tmp(1);
                end
                if e2t(currentEdge,1) == idx1
                    idx2 = e2t(currentEdge,2);
                else
                    idx2 = e2t(currentEdge,1);
                end

                % If the edge is not collapsing, add it
                if dualVerticesPointers(idx1) ~= dualVerticesPointers(idx2)
                    dualElement(m) = dualVerticesPointers(idx1);
                end

                idx1 = idx2;
            end
        end

        % At this point we reached the other boundary edge
        idx2 = edges2dualVertices(bndEdges(2));

        % If the edge is not collapsing, add it
        if dualVerticesPointers(idx1) ~= dualVerticesPointers(idx2)
            dualElement(v2eDim(i)) = dualVerticesPointers(idx1);
        end
        idx1 = idx2;

        if vertices(i,3) == 1
            idx2 = edges2dualVertices(bndEdges(1));
            if dualVerticesPointers(idx1) ~= dualVerticesPointers(idx2)
                dualElement(v2eDim(i)+1) = dualVerticesPointers(idx1);
            end
        else
            idx2 = vertex2dualVertices(i);
            if dualVerticesPointers(idx1) ~= dualVerticesPointers(idx2)
                dualElement(v2eDim(i)+1) = dualVerticesPointers(idx1);
            end
            idx1 = idx2;
            idx2 = edges2dualVertices(bndEdges(1));
            if dualVerticesPointers(idx1) ~= dualVerticesPointers(idx2)
                dualElement(v2eDim(i)+2) = dualVerticesPointers(idx1);
            end
        end
    end


    dualElement(dualElement == 0) = [];
    NverLoc = numel(dualElement);
    if NverLoc >= 3
        % Check orientation
        v1 = [ dualVertices(dualElement,1)-vertices(i,1) ...
               dualVertices(dualElement,2)-vertices(i,2) ];
        v2 = [ dualVertices(dualElement([2:NverLoc 1]),1)-vertices(i,1) ...
               dualVertices(dualElement([2:NverLoc 1]),2)-vertices(i,2) ];
        test = sum( v1(:,1).*v2(:,2) - v1(:,2).*v2(:,1) );
        if test < 0
            dualElement = dualElement(end:-1:1);
        end

        dualElementCounter = dualElementCounter + 1;
        dualElements{dualElementCounter} = dualElement;
    end

end

mesh         = [];
mesh.Node    = dualVertices;
mesh.Element = dualElements(1:dualElementCounter);

mesh.Node = localMeshOptimization2d( mesh );

close
figure()
hold on
axis equal
axis off
set(gcf, 'color', [1 1 1]);

for E = 1:dualElementCounter
    myBoundary = mesh.Node(mesh.Element{E}, :);

%     plot([myBoundary(:,1); myBoundary(1,1)], ...
%          [myBoundary(:,2); myBoundary(1,2)], 'k')
    patch(myBoundary(:,1), myBoundary(:,2), 'c')
end
