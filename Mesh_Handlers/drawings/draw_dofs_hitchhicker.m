
%
% Draw standard degrees of freedom for laplacian
%

addpath ../../Quadrature

% Number of vertices of the reference regular polygon
Nver    = 7;

% Order of the VEM space
k       = 3;

% Scaling factor for plotting internal degrees of freedom
scaling = 0.2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dx   = 1/(k-2);
dy   = 0.5;

dtheta = 2 * pi / Nver;
theta  = (0:Nver-1) * dtheta;
nodes  = exp(1i*theta);

xver = real(nodes);
yver = imag(nodes);

xqd1      = lobatto(k+1, 0);
formula1d = [(1.-xqd1)/2. (xqd1+1.)/2.];

%
%
%

figure()
hold on
axis equal
axis off
% grid on
set(gcf, 'color', [1 1 1]);

% Polygon
plot([xver, xver(1)], [yver, yver(1)], 'k')

% Vertex dofs
plot(xver, yver, 'ko', 'Markersize', 15, 'Markerfacecolor', 'k')

% Edge dofs
iterator = [2:Nver 1];
for e = 1:Nver
    xqEdge = xver([e, iterator(e)])'; xqEdge = formula1d*xqEdge;
    yqEdge = yver([e, iterator(e)])'; yqEdge = formula1d*yqEdge;
    plot(xqEdge(2:k), yqEdge(2:k), 'gs', 'Markersize', 15, 'Markerfacecolor', 'g')
end

% Internal dofs
if k >= 2
    x = []; y = [];
    dPkm2 = k*(k-1)/2;
    for m = 0:k-2
        for n = 0:k-2-m
            x = [x n*dx];
            y = [y m*dx];
        end
    end
    x3 = 1/2; y3 = -1/(2*sqrt(3));
    x1 = 0;   y1 = 1/sqrt(3);
    x2 = -1/2; y2 = -1/(2*sqrt(3));
    B  = [ x2-x1 x3-x1;
           y2-y1 y3-y1 ];
    b  =  [ x1; y1 ];
    transformed = scaling * ( B*[x; y] + b );
    plot(transformed(1,:), transformed(2,:), 'r^', 'Markersize', 15, 'Markerfacecolor', 'r')
end
