
addpath ../
addpath ../../Quadrature/
addpath ../../../PolyMesher/
addpath ../PolyMesher_Addons/

NElem    = 9;
MaxIter  = 100;
randType = 'uniform';

% Font size
fs  = 22;

% Line width
lw  = 3;

% Marker size
ms  = 7;

% Marker head size
mhs = 2;

% Order of VEM space
k   = 4;

[xqd1, wqd1] = lobatto(k+1, 0);
formula1d    = [(1.-xqd1)/2. (xqd1+1.)/2. wqd1/2.];

xMin           = -1;
xMax           = 1;
yMin           = -1;
yMax           = 1;
BdBox          = [xMin xMax yMin yMax];
center         = [0 0];
radius         = 1;
myElementNodes = [center(1) center(2) radius]; % x, y coordinates of the center, radius
refArea        = pi;
Domain         = @unitdiskDomain;

[Node,Element] = PolyMesher_modified(Domain,NElem,MaxIter,randType,myElementNodes,BdBox);

%
%
%

figure()
hold on
axis equal
axis off
% grid on
set(gcf, 'color', [1 1 1]);

% Curved domain (circle)
theta = linspace(0, 2*pi, 10000);

plot(cos(theta), sin(theta), 'r', 'Linewidth', lw)

mesh.Node    = Node;
mesh.Element = Element;
mesh.Node    = correctNodes( mesh, 'unitcircle' );

mesh = loadmesh(mesh, 0);
P    = mesh.coordinates;
E    = mesh.edges;
for i = 1:size(E,1)
    x = P(E(i,1:2),1);
    y = P(E(i,1:2),2);
    if E(i,3) == 1
        plot(x,y,'g','Linewidth',lw)

        if mod(i,2) == 1

            % Alternate edge processing in order not to fill the plot with
            % too many annotations

            edgeLength = sqrt((x(1)-x(2))^2 + (y(1)-y(2))^2);
            nx         = y(2) - y(1);
            ny         = x(1) - x(2);
            nx         = nx/edgeLength;
            ny         = ny/edgeLength;

            % Check edge orientation
            if (x(1)*y(2) - x(2)*y(1)) < 0
                nx = -nx;
                ny = -ny;
            end

            lhalf      = edgeLength/2;
            d          = sqrt(1 - lhalf^2);
            x1d        = [ -lhalf; lhalf ];
            x1d        = formula1d(2:k,1:2) * x1d;
            delta      = sqrt(1 - x1d.^2) - d;

            xNodes = formula1d(2:k,1:2)*x;
            yNodes = formula1d(2:k,1:2)*y;
            plot(xNodes, yNodes, 'ko', 'Markersize', ms, 'Markerfacecolor', 'k')

            xNodes2 = xNodes + delta*nx;
            yNodes2 = yNodes + delta*ny;
            plot(xNodes2, yNodes2, 'ko', 'Markersize', ms, 'Markerfacecolor', 'k')

            for l = 1:k-1
                plot([xNodes(l), xNodes2(l)], [yNodes(l), yNodes2(l)], 'Color', [255/255,127/255,80/255], 'Linewidth', lw)
                text(xNodes(l), yNodes(l), ['$$\underline x_', num2str(l), '$$'], 'Interpreter', 'latex', 'Fontsize', fs, 'HorizontalAlignment', 'left', 'VerticalAlignment', 'top')
                text((xNodes(l)+xNodes2(l))/2, (yNodes(l)+yNodes2(l))/2, ['$$\delta(\underline x_', num2str(l), ')$$'], 'Interpreter', 'latex', 'Fontsize', fs, 'HorizontalAlignment', 'left', 'VerticalAlignment', 'bottom')
            end
        end
    else
       plot(x,y,'b','Linewidth',lw)
    end
end
