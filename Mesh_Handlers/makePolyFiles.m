rootdir  = '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/unitcircle/dual';
basename = 'unitcircle';

constraintStr = {'0.01', '0.0025', '0.000625', '0.00015625', '0.0000390625', '0.000009765625', '0.00000244140625'};
% constraintStr = {'0.02', '0.005', '0.00125', '0.0003125', '0.000078125', '0.00001953125'};
% constraintStr = {'0.05', '0.0125', '0.003125', '0.00078125', '0.0001953125', '0.000048828125', '0.00001220703125'};
% constraintStr = {'0.0025', '0.000625', '0.00015625', '0.0000390625', '0.000009765625'};

numRefinements = numel(constraintStr);
minAngle       = '20.0';

for i = 1:numRefinements
    constraint = str2double(constraintStr{i});
    tmp        = replace(constraintStr{i}, '.', 'p');
    filename   = sprintf('%s/%s_a%s.poly', rootdir, basename, tmp);
    makePolarCurvePolyFile(filename, constraint);
%     makeQuarterMesh(filename, constraint);
%     makeSectorPolyFile(filename, constraint);
    eval(sprintf('!/home/daniele/Documents/codes/triangle/triangle -pega%sq%s %s', constraintStr{i}, minAngle, filename));
end
