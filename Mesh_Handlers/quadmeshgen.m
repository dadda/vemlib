function [mesh] = quadmeshgen(N)
% --------------------------------------------------
% Generates a quad mesh on [0,1]^2 with N nodes per side
% As output: mesh in MY format.
% See meshstruct.m for MY format info
%---------------------------------------------------
% vec=linspace(0,1,N);
%
for i=1:N^2
    P(i,1) = mod(i-1,N)/(N-1);
    P(i,2) = (ceil(i/N)-1)/(N-1);
end
NN=(N-1)^2;
T=[4*ones(NN,1),zeros(NN,4)];
for k=1:NN
        T(k,2) = k + floor((k-1)/(N-1)) + N + 1;
        T(k,3) = k + floor((k-1)/(N-1)) + N;
        T(k,4) = k + floor((k-1)/(N-1));
        T(k,5) = k + floor((k-1)/(N-1)) + 1;
end    
        
mesh = meshstruct(P,T);