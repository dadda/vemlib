#include <ginac/ginac.h>
#include "pmgo.h"
#include "quadrature.h"

using namespace GiNaC;


int compareSize1( const void * a, const void * b )
{
  int *rowA, *rowB;
  rowA = *(int **)a;
  rowB = *(int **)b;
  return rowA[0] - rowB[0];
}


double CVT( const dualMesh * const dMesh,
	    const double * const vertices,
	    const int pIdx )
{
  size_t itmpBis, Ned, counter;
  size_t hFace, tFace, hEdge, tEdge;
  
  size_t j, k, itmp, Nfc;
  size_t *polyhedronLocal = NULL, *facets_cumsizes = NULL;
  int **polyhedron = NULL, **buffer = NULL;

  double xTmp, yTmp, zTmp, diamSq;
  double faceArea, faceNormalNorm, polyhedronDiameter, polyhedronVolume;
  double v1[3], v2[3], faceOuterNormal[3], faceCenter[3], polyhedronCentroid[3];
  double *polyVertices = NULL, *outerNormals = NULL, *algDist = NULL;

  symbol x("x"), y("y"), z("z");
  symtab table;
  table["x"] = x; table["y"] = y; table["z"] = z;
  parser reader(table);
  lst l;
  ex Int;
  char energyStr[512];


  Nfc = dMesh->p2hfDim[pIdx+1] - dMesh->p2hfDim[pIdx];
      
  polyhedron      = (int **) malloc ( Nfc * sizeof(int *) );
  facets_cumsizes = (size_t *) calloc (Nfc+1, sizeof(size_t));
  outerNormals    = (double *) calloc ( 3*Nfc, sizeof(double) );
  algDist         = (double *) calloc ( Nfc, sizeof(double) );
      
  counter = 0;
  polyhedronVolume = 0.0;
  for (j = 0; j < Nfc; ++j) {
    hFace         = dMesh->p2hf[ dMesh->p2hfDim[pIdx] + j ];
    tFace         = hFace/2;
    Ned           = dMesh->f2heDim[ tFace + 1 ] - dMesh->f2heDim[ tFace ];
    counter       += Ned;
    facets_cumsizes[j+1] = facets_cumsizes[j] + Ned;
    polyhedron[j] = (int *) malloc ( Ned * sizeof(int) );
    faceCenter[0] = 0.0; faceCenter[1] = 0.0; faceCenter[2] = 0.0;
    if ( hFace%2 ) {
      for (k = 0; k < Ned; ++k) {
  	hEdge = dMesh->f2he[ dMesh->f2heDim[tFace] + Ned - 1 - k ];
  	tEdge = hEdge/2;
  	polyhedron[j][k] = dMesh->edges[ 2 * tEdge + 1 - hEdge%2 ];
  	faceCenter[0] += vertices[ 3 * polyhedron[j][k]     ];
  	faceCenter[1] += vertices[ 3 * polyhedron[j][k] + 1 ];
  	faceCenter[2] += vertices[ 3 * polyhedron[j][k] + 2 ];
      }
    } else {
      for (k = 0; k < Ned; ++k) {
  	hEdge = dMesh->f2he[ dMesh->f2heDim[tFace] + k ];
  	tEdge = hEdge/2;
  	polyhedron[j][k] = dMesh->edges[ 2 * tEdge + hEdge%2 ];
  	faceCenter[0] += vertices[ 3 * polyhedron[j][k]     ];
  	faceCenter[1] += vertices[ 3 * polyhedron[j][k] + 1 ];
  	faceCenter[2] += vertices[ 3 * polyhedron[j][k] + 2 ];
      }
    }
    faceCenter[0] /= (double)(Ned);
    faceCenter[1] /= (double)(Ned);
    faceCenter[2] /= (double)(Ned);

    faceOuterNormal[0] = 0.0; faceOuterNormal[1] = 0.0; faceOuterNormal[2] = 0.0;
    for (k = 0; k < Ned; ++k) {
      v1[0] = vertices[3*polyhedron[j][k]]   - faceCenter[0];
      v1[1] = vertices[3*polyhedron[j][k]+1] - faceCenter[1];
      v1[2] = vertices[3*polyhedron[j][k]+2] - faceCenter[2];

      v2[0] = vertices[3*polyhedron[j][(k+1)%Ned]]   - faceCenter[0];
      v2[1] = vertices[3*polyhedron[j][(k+1)%Ned]+1] - faceCenter[1];
      v2[2] = vertices[3*polyhedron[j][(k+1)%Ned]+2] - faceCenter[2];

      faceOuterNormal[0] += v1[1]*v2[2] - v1[2]*v2[1];
      faceOuterNormal[1] += v1[2]*v2[0] - v1[0]*v2[2];
      faceOuterNormal[2] += v1[0]*v2[1] - v1[1]*v2[0];
    }
    faceNormalNorm = cblas_dnrm2(3, faceOuterNormal, 1);
    faceOuterNormal[0] /= faceNormalNorm;
    faceOuterNormal[1] /= faceNormalNorm;
    faceOuterNormal[2] /= faceNormalNorm;

    outerNormals[j]       = faceOuterNormal[0];
    outerNormals[j+Nfc]   = faceOuterNormal[1];
    outerNormals[j+2*Nfc] = faceOuterNormal[2];

    for (k = 0; k < Ned; ++k) {
      algDist[j] += faceOuterNormal[0] * vertices[3*polyhedron[j][k]]
  	+ faceOuterNormal[1] * vertices[3*polyhedron[j][k]+1]
  	+ faceOuterNormal[2] * vertices[3*polyhedron[j][k]+2];
    }
    algDist[j] /= (double)(Ned);
    faceArea = faceNormalNorm / 2.0;
    polyhedronVolume += algDist[j] * faceArea;
  }
  polyhedronVolume /= 3.0;

  buffer = (int **) malloc (counter * sizeof(int *));
  for (j = 0; j < counter; ++j) buffer[j] = (int *) malloc (3 * sizeof(int));

  itmp  = 0;
  for (j = 0; j < Nfc; ++j) {
    hFace = dMesh->p2hf[ dMesh->p2hfDim[pIdx] + j ];
    tFace = hFace/2;
    Ned   = dMesh->f2heDim[ tFace + 1 ] - dMesh->f2heDim[ tFace ];
    for (k = 0; k < Ned; ++k, ++itmp) {
      buffer[itmp][0] = polyhedron[j][k];
      buffer[itmp][1] = itmp;
      buffer[itmp][2] = buffer[itmp][0];
    }
  }
  qsort( buffer, counter, sizeof(int *), compareSize1 );

  itmp    = 0;
  buffer[0][0] = buffer[0][1];
  buffer[0][1] = itmp;
  for (j = 1; j < counter; ++j) {
    buffer[j][0] = buffer[j][1];
    if ( buffer[j][2] != buffer[j-1][2] ) {
      buffer[j][1] = ++itmp;
    } else {
      buffer[j][1] = itmp;
    }
  }
  ++itmp; /* Now itmp is the number of unique vertices */

  polyVertices = (double *) malloc (3 * itmp * sizeof(double));
  polyVertices[0]      = vertices[ 3*buffer[0][2] ];
  polyVertices[itmp]   = vertices[ 3*buffer[0][2]+1 ];
  polyVertices[2*itmp] = vertices[ 3*buffer[0][2]+2 ];

  itmpBis = 0;
  for (j = 1; j < counter; ++j)
    if ( buffer[j][2] != buffer[j-1][2] ) {
      ++itmpBis;
      polyVertices[itmpBis]        = vertices[ 3*buffer[j][2] ];
      polyVertices[itmpBis+itmp]   = vertices[ 3*buffer[j][2]+1 ];
      polyVertices[itmpBis+2*itmp] = vertices[ 3*buffer[j][2]+2 ];
    }

  qsort( buffer, counter, sizeof(int *), compareSize1 );

  polyhedronLocal = (size_t *) malloc (counter * sizeof(size_t));
  for (j = 0; j < counter; ++j) {
    polyhedronLocal[j] = buffer[j][1] + 1; /* We are adding 1 because chinAlgorithm3D
  					    * was coded having MATLAB numbering convention
  					    * in mind */
    free( buffer[j] );
  }

  /* Compute diameter */
  polyhedronDiameter = -DBL_MAX;
  for (j = 0; j < itmp; ++j) {
    for (k = j+1; k < itmp; ++k) {
      xTmp = polyVertices[j]        - polyVertices[k];
      yTmp = polyVertices[j+itmp]   - polyVertices[k+itmp];
      zTmp = polyVertices[j+2*itmp] - polyVertices[k+2*itmp];
      diamSq = xTmp*xTmp + yTmp*yTmp + zTmp*zTmp;
      if ( polyhedronDiameter < diamSq ) polyhedronDiameter = diamSq;
    }
  }
  polyhedronDiameter = sqrt(polyhedronDiameter);
    
  /* Compute centroid */
  l = {x, y, z};

  /* x component */
  Int = chinAlgorithm3D( reader("x"), polyVertices, itmp, polyhedronLocal,
  			 Nfc, facets_cumsizes, outerNormals, algDist, l );
  if (is_a<numeric>(Int)) {
    polyhedronCentroid[0] = ex_to<numeric>(Int).to_double() / polyhedronVolume;
  } else {
    printf("File %s line %i: encountered error while computing centroid, aborting ...\n",
  	   __FILE__, __LINE__);
    exit(1);
  }
  /* y component */
  Int = chinAlgorithm3D( reader("y"), polyVertices, itmp, polyhedronLocal,
  			 Nfc, facets_cumsizes, outerNormals, algDist, l );
  if (is_a<numeric>(Int)) {
    polyhedronCentroid[1] = ex_to<numeric>(Int).to_double() / polyhedronVolume;
  } else {
    printf("File %s line %i: encountered error while computing centroid, aborting ...\n",
  	   __FILE__, __LINE__);
    exit(1);
  }
  /* z component */
  Int = chinAlgorithm3D( reader("z"), polyVertices, itmp, polyhedronLocal,
  			 Nfc, facets_cumsizes, outerNormals, algDist, l );
  if (is_a<numeric>(Int)) {
    polyhedronCentroid[2] = ex_to<numeric>(Int).to_double() / polyhedronVolume;
  } else {
    printf("File %s line %i: encountered error while computing centroid, aborting ...\n",
  	   __FILE__, __LINE__);
    exit(1);
  }

  /*
   * FINALLY COMPUTE THE CVT METRIC!!!
   */

  sprintf(energyStr, "(x-(%.16f))*(x-(%.16f)) + (y-(%.16f))*(y-(%.16f)) + (z-(%.16f))*(z-(%.16f))",
	  polyhedronCentroid[0], polyhedronCentroid[0],
	  polyhedronCentroid[1], polyhedronCentroid[1],
	  polyhedronCentroid[2], polyhedronCentroid[2]);

  Int = chinAlgorithm3D( reader(energyStr), polyVertices, itmp, polyhedronLocal,
  			 Nfc, facets_cumsizes, outerNormals, algDist, l );

  if (!is_a<numeric>(Int)) {
    printf("File %s line %i: encountered error while computing energy functional, aborting ...\n",
  	   __FILE__, __LINE__);
    exit(1);
  }

  for (j = 0; j < Nfc; ++j) free( polyhedron[j] );
  free( polyhedron );
  free( facets_cumsizes );
  free( outerNormals );
  free( algDist );
  free( buffer );
  free( polyhedronLocal );
  free( polyVertices );

  return ex_to<numeric>(Int).to_double();
}
