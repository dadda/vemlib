function newmesh = loadmesh(meshname, use_mp)
% Extended Mesh Structure.
%    NEWMESH = LOADMESH(MESHNAME, USE_MP) loads one of the meshes by
%    A. Russo or a nested Voronoi mesh generated with PolyMesher (containing
%    only Node and Element fields)
%    and transforms it into an extended mesh data structure.
%    MESHNAME is either the (absolute or relative) path to the mesh file or
%    a mesh data structure with fields 'Node' and 'Element' or in the
%    Alessandro Russo format.
%    USE_MP is a flag telling whether the Multiprecision Toolbox
%    (www.advanpix.com) should be used (USE_MP=1) or not (USE_MP=0).
%    The extended mesh data structure contains the following fields:
%
%    - NEWMESH.NVER: number of vertices of the mesh.
%
%    - NEWMESH.NVER_MAX: maximum number of vertices for a polygon in the
%      mesh.
%
%    - NEWMESH.INVER_MAX: index of any of the polygon with the maximum
%      number of vertices in the mesh.
%
%    - NEWMESH.COORDINATES: a NEWMESH.NVER x 2 matrix with the coordinates of the
%      vertices of the mesh.
%
%    - NEWMESH.NELT: number of elements of the mesh.
%
%    - NEWMESH.ELEMENTS: a NEWMESH.NELT x (NEWMESH.NVER_MAX+1) matrix. The E-th
%      row of this matrix contains, in order: the number of vertices of
%      element E, the indices of the vertices of E in counterclockwise
%      order, followed by as many zeros as needed to accomodate the
%      presence of elements with higher number of vertices in the mesh.
%
%    - NEWMESH.NFC: number of edges of the mesh.
%
%    - NEWMESH.EDGES: a NEWMESH.NFC x 3 matrix with a list of edges. Columns 1 and
%      2 contain the global vertex numbers for the edges. Column 3 contains
%      0 for boundary edges and 1 for dirichlet edges.
%
%    - NEWMESH.DIRFACES: a column vector with the list of Dirichlet edges,
%      that is, they point out what rows of NEWMESH.EDGES represent
%      Dirichlet edge. >> IMPORTANT REMARK <<: only Dirichlet edges are
%      supported at present.
%
%    - NEWMESH.B: a column vector with a list of boundary points.
%
%    - NEWMESH.EDGEBYELE: a NEWMESH.NELT x NEWMESH.NVER_MAX matrix, whose E-th row
%      contains the number of edges that make up \partial E, followed by as
%      many zeros as needed to accomodate the presence of elements with
%      higher number of vertices in the mesh.
%
%    - NEWMESH.PERM: a NEWMESH.NELT x NEWMESH.NVER_MAX matrix, whose E-th row indicates
%      what permutations are needed (1 (no permutation) or 2 (switch the
%      order)) for the edges of element E to match their global orientation
%      provided by field EDGES in the extended mesh data structure.

if isstruct(meshname)
    data.mesh = meshname;
else
    % meshname is the actual mesh file name
    data = load(meshname);
end

%%% Create coordinate matrix

if isfield(data.mesh, 'NV')
    newmesh.Nver = data.mesh.NV;
elseif isfield(data.mesh, 'Node')
    newmesh.Nver = size(data.mesh.Node, 1);
end

if isfield(data.mesh, 'NP')
    newmesh.Nelt = data.mesh.NP;
elseif isfield(data.mesh, 'Element')
    newmesh.Nelt = size(data.mesh.Element, 1);
end

%%% Create elements matrix

if isfield(data.mesh, 'polygon')
    [newmesh.Nver_max, newmesh.iNver_max] = max([data.mesh.polygon.NV]);    
elseif isfield(data.mesh, 'Element')
    numNodesByEle = zeros(newmesh.Nelt, 1);
    for K = 1:newmesh.Nelt
        numNodesByEle(K) = length(data.mesh.Element{K});
    end
    [newmesh.Nver_max, newmesh.iNver_max] = max(numNodesByEle);    
end

newmesh.elements = zeros([newmesh.Nelt newmesh.Nver_max+1]);

if isfield(data.mesh, 'polygon')
    newmesh.elements(:,1) = [data.mesh.polygon.NV]';    
elseif isfield(data.mesh, 'Element')
    newmesh.elements(:,1) = numNodesByEle;
end

edges = zeros( [sum(newmesh.elements(:,1)) 2] );
counter = 0;
for i = 1:newmesh.Nelt
    if isfield(data.mesh, 'polygon')
        newmesh.elements(i,2:newmesh.elements(i,1)+1) = data.mesh.polygon(i).vertices;
    elseif isfield(data.mesh, 'Element')
        newmesh.elements(i,2:newmesh.elements(i,1)+1) = data.mesh.Element{i};
    end
    edges(counter+1:counter+newmesh.elements(i,1),1:2) = ...
        [newmesh.elements(i,2:newmesh.elements(i,1)+1)' newmesh.elements(i,[3:newmesh.elements(i,1)+1,2])'];
    counter = counter + newmesh.elements(i,1);
end

%%% Create edges matrix

[sorted_edges, isorted_edges] = sort(edges,2);
[newmesh.edges, ix, jx] = unique(sorted_edges, 'rows');
newmesh.Nfc = length(ix);

%%% Create dirfaces matrix and boundary points matrix

N = histc(jx, 1:newmesh.Nfc);
newmesh.dirfaces = find(N == 1);
newmesh.edges = [newmesh.edges zeros(newmesh.Nfc,1)];
newmesh.edges(newmesh.dirfaces, 3) = 1;
newmesh.B = newmesh.edges(newmesh.dirfaces,1:2);
newmesh.B = unique(newmesh.B(:));

%%% Create edgebyele and permutation matrix

newmesh.edgebyele = zeros([newmesh.Nelt newmesh.Nver_max]);
newmesh.perm      = newmesh.edgebyele;
newmesh.elebyedge = zeros(newmesh.Nfc, 2);
counter = 0;
for i = 1:newmesh.Nelt
    Nver_loc = newmesh.elements(i,1);
    tmp      = jx(counter+1:counter+Nver_loc)';
    newmesh.edgebyele(i,1:Nver_loc) = tmp;
    newmesh.perm(i,1:Nver_loc) = isorted_edges(counter+1:counter+Nver_loc,1)';
    for j = 1:Nver_loc
        if newmesh.elebyedge(tmp(j),1) == 0
            newmesh.elebyedge(tmp(j),1) = i;
        else
            newmesh.elebyedge(tmp(j),2) = i;
        end
    end
    counter = counter + Nver_loc;
end

if isfield(data.mesh, 'vertex')
    newmesh.coordinates = [[data.mesh.vertex.x]' [data.mesh.vertex.y]'];
elseif isfield(data.mesh, 'Node')
    newmesh.coordinates = data.mesh.Node;
end

if use_mp
    newmesh.coordinates = mp(newmesh.coordinates);
    newmesh.Nelt = mp(newmesh.Nelt);
end


end
