############## Create .node, .face, .ele from .msh

import sys

try:
    basename = sys.argv[1]
except:
    print 'Usage: ', sys.argv[0], '[infile basename]'; sys.exit(1)

mshid    = open(basename + '.msh', 'r')

### Create .poly file
#polyid = open(basename + '.poly', 'w')

### Create .node file
nodeid = open(basename + '.node', 'w')

while True:
    line = mshid.readline().strip()
    if line == '$Nodes':
        break

Nver = int(mshid.readline().strip())

nodeid.write('%d 3 0 0\n' % Nver)
#polyid.write('%d 3 0 0\n' % Nver)

for i in range(Nver):
    line = mshid.readline()
    nodeid.write(line)
    #polyid.write(line)
nodeid.close()

### End of .node file

### Create .face file

# Read number of elements
while True:
    line = mshid.readline().strip()
    if line == '$Elements':
        break

Ntot = int(mshid.readline().strip())

Nfc = 0
# Count the number of boundary faces
while True:
    line = mshid.readline().strip().split()
    if line[1] == '4':
        break
    else:
        Nfc = Nfc + 1
mshid.close()

# Re-open .msh file
mshid    = open(basename + '.msh', 'r')
while True:
    line = mshid.readline().strip()
    if line == '$Elements':
        break

# Skip another line
mshid.readline()

# Use Nfc to write .face file
faceid = open(basename + '.face', 'w')
faceid.write('%d 1\n' % Nfc)
#polyid.write('%d 1\n' % Nfc)
for i in range(Nfc):
    line = mshid.readline().strip().split()
    faceid.write('%d %s %s %s %s\n' % (i+1, line[5], line[6], line[7], line[3]))
    #polyid.write('1 0 %s\n' % (line[3]))
    #polyid.write('3 %s %s %s\n' % (line[5], line[6], line[7]))
faceid.close()
#polyid.close()

### End of .face file

### Write .ele file

eleid = open(basename + '.ele', 'w')
eleid.write('%d 4 0\n' % (Ntot - Nfc))
for i in range(Ntot-Nfc):
    line = mshid.readline().strip().split()
    eleid.write('%d %s %s %s %s\n' % (i+1, line[5], line[6], line[7], line[8]))
eleid.close()    

mshid.close()
