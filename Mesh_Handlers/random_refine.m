function nodes = random_refine(oldnodes)
    l = length(oldnodes);
    nodes = zeros(1,l-1);
    for i = 1:l-1
        nodes(i) = rand(1)*(oldnodes(i+1) - oldnodes(i)) + oldnodes(i);
    end
    nodes = sort([nodes oldnodes]);
end
