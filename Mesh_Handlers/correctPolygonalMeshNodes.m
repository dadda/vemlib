addpath ../VEM_curved/

meshNames = {
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/astroid/dual/astroid_a0p01.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/astroid/dual/astroid_a0p0025.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/astroid/dual/astroid_a0p000625.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/astroid/dual/astroid_a0p00015625.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/astroid/dual/astroid_a0p0000390625.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/astroid/dual/astroid_a0p000009765625.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/astroid/dual/astroid_a0p00000244140625.1_marked_dual';
%     ...
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/flower/dual/flower_a0p02.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/flower/dual/flower_a0p005.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/flower/dual/flower_a0p00125.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/flower/dual/flower_a0p0003125.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/flower/dual/flower_a0p000078125.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/flower/dual/flower_a0p00001953125.1_marked_dual';
%     ...
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/quarter__Nelt_000256__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/quarter__Nelt_000512__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/quarter__Nelt_001024__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/quarter__Nelt_002048__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/quarter__Nelt_004096__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/quarter__Nelt_008192__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/quarter__Nelt_016384__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/quarter__Nelt_032768__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/quarter__Nelt_065536__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/quarter__Nelt_131072__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/quarter__Nelt_262144__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     ...
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/dual/quarter_a0p01.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/dual/quarter_a0p0025.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/dual/quarter_a0p000625.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/dual/quarter_a0p00015625.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/dual/quarter_a0p0000390625.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/dual/quarter_a0p000009765625.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/quarter/dual/quarter_a0p00000244140625.1_marked_dual';
%     ...
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/sector/dual/sector_a0p05.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/sector/dual/sector_a0p0125.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/sector/dual/sector_a0p003125.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/sector/dual/sector_a0p00078125.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/sector/dual/sector_a0p0001953125.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/sector/dual/sector_a0p000048828125.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/sector/dual/sector_a0p00001220703125.1_marked_dual';
%     ...
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/sector/sector__Nelt_000256__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/sector/sector__Nelt_000512__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/sector/sector__Nelt_001024__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/sector/sector__Nelt_002048__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/sector/sector__Nelt_004096__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/sector/sector__Nelt_008192__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/sector/sector__Nelt_016384__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/sector/sector__Nelt_032768__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/sector/sector__Nelt_065536__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/sector/sector__Nelt_131072__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/sector/sector__Nelt_262144__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     ...
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/spiral/dual/spiral_a0p01.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/spiral/dual/spiral_a0p0025.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/spiral/dual/spiral_a0p000625.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/spiral/dual/spiral_a0p00015625.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/spiral/dual/spiral_a0p0000390625.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/spiral/dual/spiral_a0p000009765625.1_marked_dual';
%     ...
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/unitcircle/dual/unitcircle_a0p01.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/unitcircle/dual/unitcircle_a0p0025.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/unitcircle/dual/unitcircle_a0p000625.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/unitcircle/dual/unitcircle_a0p00015625.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/unitcircle/dual/unitcircle_a0p0000390625.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/unitcircle/dual/unitcircle_a0p000009765625.1_marked_dual';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/unitcircle/dual/unitcircle_a0p00000244140625.1_marked_dual';
%     ...
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/unitcircle/ucirc__Nelt_000256__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/unitcircle/ucirc__Nelt_000512__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/unitcircle/ucirc__Nelt_001024__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/unitcircle/ucirc__Nelt_002048__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/unitcircle/ucirc__Nelt_004096__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/unitcircle/ucirc__Nelt_008192__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/unitcircle/ucirc__Nelt_016384__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/unitcircle/ucirc__Nelt_032768__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/unitcircle/ucirc__Nelt_065536__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/unitcircle/ucirc__Nelt_131072__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
%     '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/unitcircle/ucirc__Nelt_262144__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00';
    };

% meshExt = '.off';
meshExt = '.mat';

% meshOutputExt = '.off';
meshOutputExt = '.mat';

bnd = 'sector';

numFiles = numel(meshNames);

fprintf('\n');
for m = 1:numFiles
    fprintf('Processing mesh %i of %i ...\n', m, numFiles);
    meshName = [ meshNames{m} meshExt ];

    if strcmp(meshExt, '.off')
        mesh = readOFFmesh(meshName);
    else
        load(meshName);
    end
    mesh.Node = correctNodes(mesh, bnd);

    if strcmp( meshOutputExt, '.mat' )
        save([meshNames{m} '_corrected.mat'], 'mesh')
    else
        writeOFFmesh( mesh, [meshNames{m} '_corrected.off'] );
    end
    fprintf('... done\n');
end
