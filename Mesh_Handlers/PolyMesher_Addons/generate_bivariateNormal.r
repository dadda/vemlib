library(mvtnorm)

args <- commandArgs(TRUE)
# args <- 10

# m <- c(0.25, 0.25)
# B <- matrix(c(1, -1, 1, 1), nrow=2, ncol=2, byrow=T)
# D <- matrix(c(0.3, 0., 0., 0.15), nrow=2, ncol=2)

m <- c(0.5, 0.5)
B <- matrix(c(1, 0, 0, 1), nrow=2, ncol=2, byrow=T)
D <- matrix(c(0.16, 0., 0., 0.16), nrow=2, ncol=2)

s <- B %*% (D*D) %*% t(B)

n <- as.double(args[1])

x <- rmvnorm(n, m, s)
write.table(x, "mvtnorm.txt", row.names=F, col.names=F)
