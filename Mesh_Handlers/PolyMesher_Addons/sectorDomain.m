function [x] = sectorDomain(data,BdBox,Demand,Arg,varargin)
switch(Demand)
    case('Dist');  x = DistFnc(Arg,data);
    case('BC');    x = BndryCnds(Arg{:},BdBox);
    case('BdBox'); x = BdBox;
    case('PFix');  x = FixedPoints(BdBox);
end
end
%----------------------------------------------- COMPUTE DISTANCE FUNCTIONS
function Dist = DistFnc(P,data)
  xc    = data(1); yc = data(2); b = data(3); a = data(4);
  d1    = dCircle(P,xc,yc,b);
  delta = sqrt(b^2 - (a-xc)^2);
  d2    = dLine(P, a, yc+delta, a, yc-delta);
  Dist  = dIntersect(d1, d2);
end
%---------------------------------------------- SPECIFY BOUNDARY CONDITIONS
function [x] = BndryCnds(Node,Element,BdBox)
  x = cell(2,1); %No boundary conditions specified for this problem
end
%----------------------------------------------------- SPECIFY FIXED POINTS
function [PFix] = FixedPoints(BdBox)
  PFix = [];
end
%-------------------------------------------------------------------------%
