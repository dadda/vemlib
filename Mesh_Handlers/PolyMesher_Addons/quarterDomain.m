function [x] = quarterDomain(data,BdBox,Demand,Arg,varargin)
switch(Demand)
    case('Dist');  x = DistFnc(Arg,data);
    case('BC');    x = BndryCnds(Arg{:},BdBox);
    case('BdBox'); x = BdBox;
    case('PFix');  x = FixedPoints(BdBox);
end
end
%----------------------------------------------- COMPUTE DISTANCE FUNCTIONS
function Dist = DistFnc(P,BdNodes)
  Dist = dCircle(P,0,0,2);
  Dist = dIntersect(Dist, dLine(P, 0, 2, 0, 1));
  Dist = dIntersect(Dist, -dCircle(P,0,0,1));
  Dist = dIntersect(Dist, dLine(P, 1, 0, 2, 0));
end
%---------------------------------------------- SPECIFY BOUNDARY CONDITIONS
function [x] = BndryCnds(Node,Element,BdBox)
  x = cell(2,1); %No boundary conditions specified for this problem
end
%----------------------------------------------------- SPECIFY FIXED POINTS
function [PFix] = FixedPoints(BdBox)
  PFix = [];
end
%-------------------------------------------------------------------------%
