function [x] = voroDomain(BdNodes,BdBox,Demand,Arg,varargin)
  
  switch(Demand)
    case('Dist');  x = DistFnc(Arg,BdNodes);
    case('BC');    x = BndryCnds(Arg{:},BdBox);
    case('BdBox'); x = BdBox;
    case('PFix');  x = FixedPoints(BdBox);
  end
%----------------------------------------------- COMPUTE DISTANCE FUNCTIONS
function Dist = DistFnc(P,BdNodes)    
    numNodes = size(BdNodes,1);
    iterator = [2:numNodes 1];

    Dist = dLine(P, BdNodes(1,1), BdNodes(1,2), ...
                    BdNodes(2,1), BdNodes(2,2) );
    for v = 2:numNodes
        Dist = dIntersect( Dist, ...
                           dLine(P, BdNodes(v,1), BdNodes(v,2), ...
                                    BdNodes(iterator(v),1), BdNodes(iterator(v),2) ) );
    end
%---------------------------------------------- SPECIFY BOUNDARY CONDITIONS    
function [x] = BndryCnds(Node,Element,BdBox)
x = cell(2,1); %No boundary conditions specified for this problem
%----------------------------------------------------- SPECIFY FIXED POINTS
function [PFix] = FixedPoints(BdBox)
  PFix = [];
%-------------------------------------------------------------------------%
