function [x] = ponteCoperto(data,BdBox,Demand,Arg,varargin)
switch(Demand)
    case('Dist');  x = DistFnc(Arg,data);
    case('BC');    x = BndryCnds(Arg{:},BdBox);
    case('BdBox'); x = BdBox;
    case('PFix');  x = FixedPoints(BdBox);
end
end
%----------------------------------------------- COMPUTE DISTANCE FUNCTIONS
function Dist = DistFnc(P,BdNodes)
    Dist = dLine(P, 0, 0, 30, 0);
    Dist = dIntersect(Dist, dLine(P, 30, 0, 30, 3));
    Dist = dIntersect(Dist, dLine(P, 30, 3, 15, 5));
    Dist = dIntersect(Dist, dLine(P, 15, 5, 13, 5));
    Dist = dIntersect(Dist, dLine(P, 13, 5, 0, 3));
    Dist = dIntersect(Dist, dLine(P, 0, 3, 0, 0));
    Dist = dDiff(Dist, dCircle(P, 3.5, 0, 2.5));
    Dist = dDiff(Dist, dCircle(P, 10, 0, 3));
    Dist = dDiff(Dist, dCircle(P, 17.5, 0, 2.5));
    Dist = dDiff(Dist, dCircle(P, 23, 0, 2));
    Dist = dDiff(Dist, dCircle(P, 27.5, 0, 1.5));

    house = dLine(P, 13, 5, 15, 5);
    house = dIntersect(house, dLine(P, 15, 5, 15, 7));
    house = dIntersect(house, dLine(P, 15, 7, 14, 8));
    house = dIntersect(house, dLine(P, 14, 8, 13, 7));
    house = dIntersect(house, dLine(P, 13, 7, 13, 5));
    Dist   = dUnion(Dist, house);
end
%---------------------------------------------- SPECIFY BOUNDARY CONDITIONS
function [x] = BndryCnds(Node,Element,BdBox)
  x = cell(2,1); %No boundary conditions specified for this problem
end
%----------------------------------------------------- SPECIFY FIXED POINTS
function [PFix] = FixedPoints(BdBox)
  PFix = [];
end
%-------------------------------------------------------------------------%
