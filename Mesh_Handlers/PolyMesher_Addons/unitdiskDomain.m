function [x] = unitdiskDomain(circle,BdBox,Demand,Arg,varargin)
switch(Demand)
    case('Dist');  x = DistFnc(Arg,circle);
    case('BC');    x = BndryCnds(Arg{:},BdBox);
    case('BdBox'); x = BdBox;
    case('PFix');  x = FixedPoints(BdBox);
end
end
%----------------------------------------------- COMPUTE DISTANCE FUNCTIONS
function d = DistFnc(P,circle)
  % Here, BdNodes represents: x coordinate of the center, y coordinate,
  % radius
  d = dCircle(P,circle(1),circle(2),circle(3));
end
%---------------------------------------------- SPECIFY BOUNDARY CONDITIONS
function [x] = BndryCnds(Node,Element,BdBox)
  x = cell(2,1); %No boundary conditions specified for this problem
end
%----------------------------------------------------- SPECIFY FIXED POINTS
function [PFix] = FixedPoints(BdBox)
  PFix = [];
end
%-------------------------------------------------------------------------%

