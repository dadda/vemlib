function [x] = DICArDomain(data,BdBox,Demand,Arg,varargin)
switch(Demand)
    case('Dist');  x = DistFnc(Arg,data);
    case('BC');    x = BndryCnds(Arg{:},BdBox);
    case('BdBox'); x = BdBox;
    case('PFix');  x = FixedPoints(BdBox);
end
end
%----------------------------------------------- COMPUTE DISTANCE FUNCTIONS
function Dist = DistFnc(P,BdNodes)
    %
    % Letter D
    %
    
    % Outer boundary
    xc1 = 0; yc1 = 3; r1 = sqrt((xc1-1)^2 + yc1^2);
    xc2 = 0; yc2 = 2;
%     xc3 = -1; yc3 = 2.5; r3 = sqrt((xc3-1)^2+yc3^2);

    Dist = dLine(P, 0, 0, 1, 0);

%     Dist = dIntersect(Dist, dCircle(P, xc3, yc3, r3));
    Dist = dIntersect(Dist, dCircle(P, xc1, yc1, r1));
    Dist = dIntersect(Dist, dLine(P, 3, 2, 3, 3));
    Dist = dIntersect(Dist, dCircle(P, xc2, yc2, r1));

    Dist = dIntersect(Dist, dLine(P, 1, 5, 0, 5));
    Dist = dIntersect(Dist, dLine(P, 0, 5, 0, 0));

    % Internal hole
    r2 = sqrt((1-xc2)^2 + (4-yc2)^2);
    Dhole = dLine(P, 1, 1, 1, 4);
    Dhole = dUnion(Dhole, -dCircle(P, xc2, yc2, r2));
    Dhole = dUnion(Dhole, dLine(P, 2, 3, 2, 2));
    Dhole = dUnion(Dhole, -dCircle(P, xc1, yc1, r2));
    
    Dist = dIntersect(Dist, Dhole);
    
    %
    % Letter I
    %
    
    Idist = dLine(P, 3, 0, 4, 0);
    Idist = dIntersect(Idist, dLine(P, 4, 0, 4, 5));
    Idist = dIntersect(Idist, dLine(P, 4, 5, 3, 5));
    Idist = dIntersect(Idist, dLine(P, 3, 5, 3, 0));
    
    Dist = dUnion(Dist, Idist);

    %
    % Letter C
    %

    % Outer border
    Cdist = dLine(P, 6, 0, 7, 0);
    Cdist = dIntersect(Cdist, dLine(P, 7, 0, 7, 5));
    Cdist = dIntersect(Cdist, dLine(P, 7, 5, 6, 5));
    Cdist = dIntersect(Cdist, dCircle(P, 7, 2, r1));
    Cdist = dIntersect(Cdist, dLine(P, 4, 3, 4, 2));
    Cdist = dIntersect(Cdist, dCircle(P, 7, 3, r1));

    % Internal
    Chole = dLine(P, 7, 1, 6, 1);
    Chole = dUnion(Chole, -dCircle(P, 7, 3, r2));
    Chole = dUnion(Chole, dLine(P, 5, 2, 5, 3));
    Chole = dUnion(Chole, -dCircle(P, 7, 2, r2));
    Chole = dUnion(Chole, dLine(P, 6, 4, 7, 4));
    
    Cdist = dIntersect(Cdist, Chole);
    Dist  = dUnion(Dist, Cdist);

    %
    % Letter A
    %

    % Outer border
    Adist = dLine(P, 7, 0, 10, 0);
    Adist = dIntersect(Adist, dLine(P, 10, 0, 10, 1));
    Adist = dIntersect(Adist, dLine(P, 10, 1, 9, 5));
    Adist = dIntersect(Adist, dLine(P, 9, 5, 8, 5));
    Adist = dIntersect(Adist, dLine(P, 8, 5, 7, 1));
    Adist = dIntersect(Adist, dLine(P, 7, 1, 7, 0));
    
    % Cut 1
    Ahole = dLine(P, 8, 0, 8.5, 2);
    Ahole = dUnion(Ahole, dLine(P, 8.5, 2, 9, 0));
    Adist = dIntersect(Adist, Ahole);

    % Cut 2
    Ahole = dLine(P, 8, 2.5, 8.5, 4);
    Ahole = dUnion(Ahole, dLine(P, 8.5, 4, 9, 2.5));
    Ahole = dUnion(Ahole, dLine(P, 9, 2.5, 8, 2.5));
    Adist = dIntersect(Adist, Ahole);

    Dist = dUnion(Dist, Adist);

    %
    % Letter r
    %

    % Left part
    rDist = dLine(P, 10, 0, 11, 0);
    rDist = dIntersect(rDist, dLine(P, 11, 0, 11, 3));
    rDist = dIntersect(rDist, dLine(P, 11, 3, 10, 3));
    rDist = dIntersect(rDist, dLine(P, 10, 3, 10, 0));

    % Right part
    rAdd = dLine(P, 12, 1.5, 12, 3);
    rAdd = dIntersect(rAdd, dCircle(P, 12, 2, 1));
    rAdd = dIntersect(rAdd, dLine(P, 11, 2, 11, 1.5));
    rAdd = dIntersect(rAdd, dLine(P, 11, 1.5, 12, 1.5));
    rDist = dUnion(rDist, rAdd);

%     % Right part, bottom
%     rAdd = dLine(P, 12, 2, 11, 2);
%     rAdd = dIntersect(rAdd, dLine(P, 11, 2, 11, 1));
%     rAdd = dIntersect(rAdd, -dCircle(P, 12, 1, 1));
% 
%     rDist = dUnion(rDist, rAdd);

    Dist = dUnion(Dist, rDist);
end
%---------------------------------------------- SPECIFY BOUNDARY CONDITIONS
function [x] = BndryCnds(Node,Element,BdBox)
  x = cell(2,1); %No boundary conditions specified for this problem
end
%----------------------------------------------------- SPECIFY FIXED POINTS
function [PFix] = FixedPoints(BdBox)
  PFix = [];
end
%-------------------------------------------------------------------------%
