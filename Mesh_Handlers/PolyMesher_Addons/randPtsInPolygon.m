n = 50;

vertices = [1.220 -0.827;
            -1.490 -4.503;
            -3.766 -1.622;
            -4.240 -0.091;
            -3.160 4;
            -0.981 4.447;
            0.132 4.027];

Nver = size(vertices, 1);
C = sum(vertices,1) / Nver;

v1 = [(vertices(:,1)-C(1)) (vertices(:,2)-C(2))];
v2 = [(vertices([2:Nver 1],1)-C(1)) (vertices([2:Nver 1],2)-C(2))];

areas = (v1(:,1).*v2(:,2) - v1(:,2).*v2(:,1)) / 2;
cumAreas = cumsum(areas);
cumAreas = cumAreas / cumAreas(end);

r = rand(n,1);
[~, pos] = histc(r, [0; cumAreas(1:end-1); 1]);

vertices = [vertices; vertices(1,:)];
pa = vertices(pos,:);
pb = vertices(pos+1,:);

s = sqrt(rand(n,1)); s2 = [s, s];
t = rand(n,1); t2 = [t, t];

p = (1-s) * C + s2 .* ((1-t2).*pa + t2.*pb);

figure()
axis equal; hold on;
plot(vertices(:,1), vertices(:,2), 'b')
plot(p(:,1), p(:,2), 'r*')
