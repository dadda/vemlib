% numerates elements with colnumele

if ~exist('colnumele')
    colnumele = 'red';
end

for iele=1:nele
    xvc = sum(xn(nodi(iele,:)))/4;
    yvc = sum(yn(nodi(iele,:)))/4;
    text(xvc,yvc,sprintf('%d',iele),'Color',colnumele,'HorizontalAlignment','center');
end
