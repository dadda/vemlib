if exist('mynnod')
    nnod1=mynnod;
else
    nnod1=1:nnod;
end

if ~exist('colnumnod')
    colnumnod = 'blue';
end

s= '';
for inod=nnod1
  s = strvcat(s,int2str(inod));
end

text(xn(nnod1),yn(nnod1),zeros(length(nnod1),1),s,'Color',colnumnod,'BackgroundColor','w')

