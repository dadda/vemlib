% numerates elements with colnumele

if ~exist('colnumele')
    colnumele = 'red';
end

for iele=1:nele
    xvc = sum(xn(nodi(iele,:)))/size(nodi,2);
    yvc = sum(yn(nodi(iele,:)))/size(nodi,2);
    text(xvc,yvc,sprintf('%d',iele),'Color',colnumele,'HorizontalAlignment','center');
end
