% take a master polygon

% squares
%
% xm = [1 3 3 2 2 0 0 1];
% ym = [0 0 1 1 2 2 1 1];
% Nm = 8;

% crosses
%
% xm = [1 2 3 4 3 4 3 2 1 0 1 0];
% ym = [0 1 0 1 2 3 4 3 4 3 2 1];
% Nm = 12;

horse

N = input('N: ');
% omega = input('file name: ','s');

%
% Build initial mesh
%

nnod = 0;
nele = 0;
xn = zeros(2,1);
yn = zeros(2,1);

for ix=0:N-1
    % ix
    for iy=0:N-1
        % iy
        deltax = 8*ix;
        deltay = 8*iy;
        
        % duplicate the polygon
        
        xd = xm + deltax;
        yd = ym + deltay;
        
        % add poligon to the mesh
        
        nele = nele+1;
        
        nodi(nele,:) = (1:Nm) + nnod;
        
        for i=1:Nm
            xn(nnod+i) = xd(i);
            yn(nnod+i) = yd(i);
        end
        
        nnod = nnod+Nm;
        
    end
end

%
% Coalesce duplicate nodes
%
TOL = 1e-10;

toberemoved = [];

for jnod=1:nnod-1

    if ~ismember(jnod,toberemoved)
        
        fprintf('%d out of %d\n',jnod,nnod);
        xp = xn(jnod);
        yp = yn(jnod);
        v = [xn(jnod+1:end,:)-xp yn(jnod+1:end,:)-yp];
        nv = sqrt(v(:,1).^2+v(:,2).^2);
        
        I = find(nv<TOL)+jnod;
        
        for i=I'
            nodi(find(nodi==i)) = jnod;
        end
        
        toberemoved = [toberemoved I'];
        
    end
    
%     for inod=jnod+1:nnod
%         d = norm([xp yp]-[xn(inod) yn(inod)]);
%         if d<TOL
%             nodi(find(nodi==jnod))=inod;
%             toberemoved = [toberemoved jnod];
%         end
%     end
end

toberemoved = sort(unique(toberemoved),'descend');

ltoberemoved = length(toberemoved);

dnnod = nnod;

for jnod=toberemoved
    
    xn(jnod:nnod-1) = xn(jnod+1:nnod);
    yn(jnod:nnod-1) = yn(jnod+1:nnod);
    
    I = find(nodi>jnod & nodi<=nnod);
    nodi(I) = nodi(I)-1;

    nnod = nnod-1;

    fprintf('%d out of %d\n',dnnod-nnod,ltoberemoved)
    
end

nodemarker = zeros(nnod,1);

for inod=1:nnod
    if length(find(nodi==inod))<=2
        nodemarker(inod) = 1;
    end
end

xn = xn/(8*N);
yn = yn/(8*N);

maxV = size(nodi,2);

% matrice delle facce

Faces = NaN*zeros(nele,maxV);
maxiv = -Inf;
miniv = +Inf;
for iele=1:nele
    Ic = find(nodi(iele,:)>0);
    Faces(iele,Ic) = nodi(iele,Ic);
end

xn = xn(1:nnod); yn = yn(1:nnod);

% matrice dei vertici
% mu = [0.5 0.5];
% B = eye(2);
% D = 0.8 * eye(2);
% sigma = B * D.^2 * B';
% myGaussian = exp( - [xn-mu(1) yn-mu(2)] * inv(sigma) * [xn-mu(1) yn-mu(2)]' );
% myGaussian = diag(myGaussian);
% 
% xn = mu(1) + myGaussian .* (xn-mu(1));
% yn = mu(2) + myGaussian .* (yn-mu(2));

mu = [0.5, 0.5];

distances = pdist2(mu, [xn yn]);

xn = mu(1) + distances' .* (xn-mu(1));
yn = mu(2) + distances' .* (yn-mu(2));

mesh.Node = [xn yn];

mesh.Element = cell(nele, 1);
for K = 1:nele
    mesh.Element{K} = unique(nodi(K,:), 'stable');
end

filename = sprintf('horse_pdist2_%06i.mat', N*N);
save(filename, 'mesh');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

zn = zeros(size(xn));
Vertices = [xn yn zn];

% mesh

fc = zeros(nele,3);

for iele=1:nele
    fc(iele,:) = [randi(2,1)-1 randi(2,1)-1 randi(2,1)-1];
end

figure(1)

%patch('Faces',Faces,'Vertices',Vertices,'FaceColor','flat','FaceVertexCData',randi(),'EdgeColor',[0 0 0])

%patch('Faces',Faces,'Vertices',Vertices,'FaceColor','flat','FaceVertexCData',randi(64,nele,1),'EdgeColor',[0 0 0])
patch('Faces',Faces,'Vertices',Vertices,'FaceColor','none')
colormap('hsv');

title(sprintf('%d polygons, %d vertices',nele,nnod))

ex = 0.05;
axis([min(xn)-ex, max(xn)+ex, min(yn)-ex, max(yn)+ex]);
axis equal
grid on

%numnod
%numele

% adesso abbiamo:

% N poligoni
% nnod vertici
% x y sono le loro coordinate
% i vertici di ogni poligono p sono in Vpi(p,:)

% if isempty(omega)
%     return
% end
% 
% % omegaf = ['meshes/' omega];
% omegaf = omega;
% 
% fprintf('writemesh: writing mesh %s\n\n',omegaf)
% 
% % scriviamo i nodi
% 
% fid = fopen([omegaf '.node'],'w');
% 
% % scriviamo la prima riga
% 
% fprintf(fid,'%d %d %d %d\n',nnod,2,0,1);
% 
% % scriviamo le coord e i flag dei nodi
% 
% for inod=1:nnod
%     
%     fprintf(fid,'%d %4.20e %4.20e %d\n',inod,xn(inod),yn(inod),nodemarker(inod));
%     
% end
% 
% fclose(fid); % chiudo il file dei nodi
% 
% % adesso scrivo i poligoni
% 
% fid = fopen([omegaf '.ele'],'w');
% 
% % prima riga
% 
% fprintf(fid,'%d %d %d\n',nele,3,0);
% 
% for iele=1:nele
%     Ic = find(nodi(iele,:)>0);
%     fprintf(fid,'%d',iele);
%     for i=Ic
%         fprintf(fid,' %d',nodi(iele,i));
%     end
%     fprintf(fid,'\n');
% end
% 
% fclose(fid);


