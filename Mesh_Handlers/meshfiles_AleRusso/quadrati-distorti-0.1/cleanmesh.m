clear all
close all

% read mesh names

[a, b]=system('ls -1 ../*/*.mat');

m = strread(b,'%s','delimiter','\n');

nm = length(m);

for i=1:nm
    ms = char(m(i));
    load(ms);
    p = strread(mesh.name,'%s','delimiter','/');
    fprintf('original name: %s \nnew name: %s \n',mesh.name,char(p(end)));
    if strcmp(mesh.name,char(p(end)))
        fprintf('already ok!\n\n');
    else
        fprintf('updating mesh.name...\n\n');
        mesh.name = char(p(end));
        save(ms,'mesh');
    end
end
