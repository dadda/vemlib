#ifndef _GEOMETRY_H
#define _GEOMETRY_H

#include "quadrature.h"
#include "pmgo.h"

using namespace GiNaC;


typedef struct polyGeoQuad {
  int    Nfc;

  int    **polyhedron;
  int    *facesCumsizes;

  double **facesOuterNormals;

  double **facesBndNodesX;
  double **facesBndNodesY;

  double **facesEdgeNormalsX;
  double **facesEdgeNormalsY;

  int    *facesNq2d;
  double **facesXquad2d;
  double **facesYquad2d;
  double **facesWquad2d;

  double *facesAreas;
  double *facesAlgDists;
  double *facesDiameters;
  double *facesCentroidX;
  double *facesCentroidY;

  double polyhedronVolume;
  double polyhedronDiameter;
  double *polyhedronCentroid;

  double *quadNodes3d;
  double *quadWeights3d;
} polyGeoQuad;


void polyGeoQuadInitialize( polyGeoQuad * pGeoQuad );
void polyGeoQuadAllocate(
			 const int pIdx,
			 const dualMesh * const dMesh,
			 const int k,
			 const int deg,
			 polyGeoQuad * pGeoQuad
			 );
void polyGeoQuadDestroy( polyGeoQuad * pGeoQuad );

void polygonFeatures(
		     const int k,
		     const int eoa,
		     const double * const vertices,
		     const int * const face,
		     const int NvLoc,
		     const double * const quadNodes1d,
		     double * const xbnd,
		     double * const ybnd,
		     double * const Nx,
		     double * const Ny,
		     double * const xquad2d,
		     double * const yquad2d,
		     double * const wquad2d,
		     int    * Nq2d,
		     double * h,
		     double * area,
		     double * xc,
		     double * yc
		     );


void polygonalFacesFeatures(
			    const int k,
			    const int eoa,
			    const double * const vertices,
			    int ** const polyhedron,
			    const int * const facesCumsizes,
			    const int Nfc,
			    const double * const quadNodes1d,
			    double ** const facesOuterNormals,
			    double * const facesAreas,
			    double * const facesAlgDists,
			    double ** const facesBndNodesX,
			    double ** const facesBndNodesY,
			    double ** const facesEdgeNormalsX,
			    double ** const facesEdgeNormalsY,
			    double ** const facesXquad2d,
			    double ** const facesYquad2d,
			    double ** const facesWquad2d,
			    int * const facesNq2d,
			    double * const facesDiameters,
			    double * const facesCentroidX,
			    double * const facesCentroidY
			    );


void polyhedronFeatures(
			const double * const vertices,
			const int Nfc,
			int ** const polyhedron,
			const int * const facesCumsizes,
			double ** const facesOuterNormals,
			const double * const facesAreas,
			const double * const facesAlgDists,
			const int deg,
			double * polyhedronVolume,
			double * polyhedronDiameter,
			double * polyhedronCentroid,
			double * quadNodes3d,
			double * quadWeights3d
			);

#endif
