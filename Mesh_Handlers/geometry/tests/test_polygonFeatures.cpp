#include <iostream>
#include <cmath>
#include "geometry.h"
#include "quadrature.h"

using namespace std;

int main()
{
  int i, k, eoa, Nq1d, Nq2d, N, NvLoc;
  int *face = NULL;
  double *vertices = NULL;
  double *tmpN = NULL, *tmpW = NULL;
  double *quadNodes1d = NULL, *quadWeights1d = NULL;
  double *xbnd = NULL, *ybnd = NULL, *nx = NULL, *ny = NULL;
  double *xquad2d = NULL, *yquad2d = NULL, *wquad2d = NULL;
  double h, area, xc, yc, fromPaper, Int;

  /***************************************************************************************
   * Test 1: unit square
   ***************************************************************************************/

  k     = 1;
  eoa   = 0;
  NvLoc = 4;
  Nq1d  = k+1;
  N     = 2*k+eoa;

  vertices = (double *) malloc (2 * NvLoc * sizeof(double));
  vertices[0] = 0.0; vertices[1] = 0.0;
  vertices[2] = 1.0; vertices[3] = 0.0;
  vertices[4] = 1.0; vertices[5] = 1.0;
  vertices[6] = 0.0; vertices[7] = 1.0;

  face = (int *) malloc (NvLoc * sizeof(int));
  face[0] = 0; face[1] = 1; face[2] = 2; face[3] = 3;

  tmpN = (double *) malloc (Nq1d * sizeof(double));
  tmpW = (double *) malloc (Nq1d * sizeof(double));
  lobatto_compute(Nq1d, tmpN, tmpW);
  quadNodes1d   = (double *) malloc (2 * (k-1) * sizeof(double));
  quadWeights1d = (double *) malloc ((k-1) * sizeof(double));
  for (i = 0; i < k-1; ++i) {
    quadNodes1d[2*i]   = (1.0 - tmpN[i+1]) * 0.5;
    quadNodes1d[2*i+1] = (1.0 + tmpN[i+1]) * 0.5;
    quadWeights1d[i]   = tmpW[i+1] * 0.5;
  }

  xbnd = (double *) malloc (k * NvLoc * sizeof(double));
  ybnd = (double *) malloc (k * NvLoc * sizeof(double));
  nx   = (double *) malloc (NvLoc * sizeof(double));
  ny   = (double *) malloc (NvLoc * sizeof(double));

  xquad2d = (double *) calloc(NvLoc * (N+1) * N, sizeof(double));
  yquad2d = (double *) calloc(NvLoc * (N+1) * N, sizeof(double));
  wquad2d = (double *) calloc(NvLoc * (N+1) * N, sizeof(double));

  polygonFeatures(k, eoa, vertices, face, NvLoc, quadNodes1d,
  		  xbnd, ybnd, nx, ny, xquad2d, yquad2d, wquad2d, &Nq2d, &h, &area, &xc, &yc);

  cout << endl << "Test 1: unit square" << endl;
  cout << "Normals to edges:" << endl;
  for (i = 0; i < NvLoc; ++i)
    cout << nx[i] << " " << ny[i] << endl;
  cout << "Diameter: " << h << endl;
  cout << "Area: " << area << endl;
  cout << "Centroid: " << xc << " " << yc << endl;

  free(vertices);
  free(face);
  free(tmpN);
  free(tmpW);
  free(quadNodes1d);
  free(quadWeights1d);
  free(xbnd);
  free(ybnd);
  free(nx);
  free(ny);
  free(xquad2d);
  free(yquad2d);

  /***************************************************************************************
   * Test 2: polygon a) from Chin et al., 2015
   ***************************************************************************************/

  k     = 1;
  eoa   = 0;
  NvLoc = 7;
  Nq1d  = k+1;
  N     = 2*k+eoa;

  vertices = (double *) malloc (2 * NvLoc * sizeof(double));
  vertices[0] = 1.220; vertices[1] = -0.827;
  vertices[2] = -1.490; vertices[3] = -4.503;
  vertices[4] = -3.766; vertices[5] = -1.622;
  vertices[6] = -4.240; vertices[7] = -0.091;
  vertices[8] = -3.160; vertices[9] = 4.0;
  vertices[10] = -0.981; vertices[11] = 4.447;
  vertices[12] = 0.132; vertices[13] = 4.027;

  face = (int *) malloc (NvLoc * sizeof(int));
  face[0] = 6; face[1] = 5; face[2] = 4; face[3] = 3;
  face[4] = 2; face[5] = 1; face[6] = 0;

  tmpN = (double *) malloc (Nq1d * sizeof(double));
  tmpW = (double *) malloc (Nq1d * sizeof(double));
  lobatto_compute(Nq1d, tmpN, tmpW);
  quadNodes1d   = (double *) malloc (2 * (k-1) * sizeof(double));
  quadWeights1d = (double *) malloc ((k-1) * sizeof(double));
  for (i = 0; i < k-1; ++i) {
    quadNodes1d[2*i]   = (1.0 - tmpN[i+1]) * 0.5;
    quadNodes1d[2*i+1] = (1.0 + tmpN[i+1]) * 0.5;
    quadWeights1d[i]   = tmpW[i+1] * 0.5;
  }

  xbnd = (double *) malloc (k * NvLoc * sizeof(double));
  ybnd = (double *) malloc (k * NvLoc * sizeof(double));
  nx   = (double *) malloc (NvLoc * sizeof(double));
  ny   = (double *) malloc (NvLoc * sizeof(double));

  xquad2d = (double *) calloc(NvLoc * (N+1) * N, sizeof(double));
  yquad2d = (double *) calloc(NvLoc * (N+1) * N, sizeof(double));
  wquad2d = (double *) calloc(NvLoc * (N+1) * N, sizeof(double));

  polygonFeatures(k, eoa, vertices, face, NvLoc, quadNodes1d,
  		  xbnd, ybnd, nx, ny, xquad2d, yquad2d, wquad2d, &Nq2d, &h, &area, &xc, &yc);

  cout << endl << "Test 2: polygon a) from Chin et al., 2015" << endl;
  cout << "Diameter: " << h << endl;
  cout << "Area: " << area << endl;
  cout << "Centroid: " << xc << " " << yc << endl;

  fromPaper = 2031627344735367./8000000000000.;
  Int       = 0.0;
  for (i = 0; i < Nq2d; ++i)
    Int += wquad2d[i] * (xquad2d[i]*xquad2d[i] + xquad2d[i]*yquad2d[i] + yquad2d[i]*yquad2d[i]);
  cout << "Integration error: " << fabs(Int - fromPaper) << endl;

  free(vertices);
  free(face);
  free(tmpN);
  free(tmpW);
  free(quadNodes1d);
  free(quadWeights1d);
  free(xbnd);
  free(ybnd);
  free(nx);
  free(ny);
  free(xquad2d);
  free(yquad2d);

  /***************************************************************************************
   * Test 3: polygon b) from Chin et al., 2015
   ***************************************************************************************/

  k     = 1;
  eoa   = 0;
  NvLoc = 5;
  Nq1d  = k+1;
  N     = 2*k+eoa;

  vertices = (double *) malloc (2 * NvLoc * sizeof(double));
  vertices[0] = 4.561; vertices[1] = 2.317;
  vertices[2] = 1.491; vertices[3] = -1.315;
  vertices[4] = -3.310; vertices[5] = -3.164;
  vertices[6] = -4.845; vertices[7] = -3.11;
  vertices[8] = -4.569; vertices[9] = 1.867;

  face = (int *) malloc (NvLoc * sizeof(int));
  face[0] = 4; face[1] = 3; face[2] = 2; face[3] = 1;
  face[4] = 0;

  tmpN = (double *) malloc (Nq1d * sizeof(double));
  tmpW = (double *) malloc (Nq1d * sizeof(double));
  lobatto_compute(Nq1d, tmpN, tmpW);
  quadNodes1d   = (double *) malloc (2 * (k-1) * sizeof(double));
  quadWeights1d = (double *) malloc ((k-1) * sizeof(double));
  for (i = 0; i < k-1; ++i) {
    quadNodes1d[2*i]   = (1.0 - tmpN[i+1]) * 0.5;
    quadNodes1d[2*i+1] = (1.0 + tmpN[i+1]) * 0.5;
    quadWeights1d[i]   = tmpW[i+1] * 0.5;
  }

  xbnd = (double *) malloc (k * NvLoc * sizeof(double));
  ybnd = (double *) malloc (k * NvLoc * sizeof(double));
  nx   = (double *) malloc (NvLoc * sizeof(double));
  ny   = (double *) malloc (NvLoc * sizeof(double));

  xquad2d = (double *) calloc(NvLoc * (N+1) * N, sizeof(double));
  yquad2d = (double *) calloc(NvLoc * (N+1) * N, sizeof(double));
  wquad2d = (double *) calloc(NvLoc * (N+1) * N, sizeof(double));

  polygonFeatures(k, eoa, vertices, face, NvLoc, quadNodes1d,
  		  xbnd, ybnd, nx, ny, xquad2d, yquad2d, wquad2d, &Nq2d, &h, &area, &xc, &yc);

  cout << endl << "Test 3: polygon b) from Chin et al., 2015" << endl;
  cout << "Diameter: " << h << endl;
  cout << "Area: " << area << endl;
  cout << "Centroid: " << xc << " " << yc << endl;

  fromPaper = 517091313866043./1600000000000.;
  Int       = 0.0;
  for (i = 0; i < Nq2d; ++i)
    Int += wquad2d[i] * (xquad2d[i]*xquad2d[i] + xquad2d[i]*yquad2d[i] + yquad2d[i]*yquad2d[i]);
  cout << "Integration error: " << fabs(Int - fromPaper) << endl;

  free(vertices);
  free(face);
  free(tmpN);
  free(tmpW);
  free(quadNodes1d);
  free(quadWeights1d);
  free(xbnd);
  free(ybnd);
  free(nx);
  free(ny);
  free(xquad2d);
  free(yquad2d);

  return 0;
}
