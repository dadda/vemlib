#include <iostream>
#include "geometry.h"

#define NUMMESHES 1

using namespace std;

int main()
{
  int i, j, l, choice, Nv, NvLoc, Nfc, k, eoa, Nq1d, deg, d3;

  double *tmpN = NULL, *tmpW = NULL;
  double *internalQuadNodes1d = NULL, *internalQuadWeights1d = NULL;
  double *vertices = NULL;

  int **polyhedron   = NULL;
  int *facesCumsizes = NULL;

  double **facesOuterNormals = NULL, **facesBndNodesX = NULL, **facesBndNodesY = NULL;
  double **facesEdgeNormalsX = NULL, **facesEdgeNormalsY = NULL;
  double *facesAreas = NULL, *facesAlgDists = NULL;
  double **facesXquad2d = NULL, **facesYquad2d = NULL, **facesWquad2d = NULL;
  int *facesNqd2 = NULL;
  double *facesDiameters = NULL, *facesCentroidX = NULL, *facesCentroidY = NULL;

  double *quadNodes3d = NULL, *quadWeights3d = NULL;

  double polyhedronVolume, polyhedronDiameter;
  double polyhedronCentroid[3];

#ifdef DEBUG
  double polyhedronVolumeCheck;
#endif

  dualMesh dMesh;
  const char *filenames[NUMMESHES];
  polyGeoQuad pGeoQuad;

  /*
   *
   */

  filenames[0] = "/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles3d/voronoi/cube/unitcube_voro_v02_Nelt000064.ovm";

  /*
   *
   */

  cout << "\nChoose a test to perform:\n";
  cout << "1: Test (g) from Chin et al. 2015;\n";
  cout << "2: Test (h) from Chin et al. 2015;\n";
  cout << "3: Test (i) from Chin et al. 2015.\n\n";
  cout << "Choice: ";
  cin >> choice;

  switch (choice) {
  case 1:
    {
      Nv   = 8;
      Nfc  = 6;
      k    = 1;
      eoa  = 0;
      Nq1d = k+1;
      deg  = 2;

      double verticesEx[]   = { 0.0, 0.0, 0.0,
				5.0, 0.0, 0.0,
				5.0, 5.0, 0.0,
				0.0, 5.0, 0.0,
				0.0, 0.0, 5.0,
				5.0, 0.0, 5.0,
				5.0, 5.0, 5.0,
				0.0, 5.0, 5.0 };

      int polyhedronEx[]    = { 0, 3, 2, 1,
				0, 1, 5, 4,
				1, 2, 6, 5,
				2, 3, 7, 6,
				3, 0, 4, 7,
				4, 5, 6, 7 };

      int facesCumsizesEx[] = {0, 4, 8, 12, 16, 20, 24};

      vertices = (double *) malloc (3 * Nv * sizeof(double));
      memcpy(vertices, verticesEx, 3 * Nv * sizeof(double));

      facesCumsizes = (int *) malloc ((Nfc+1) * sizeof(int));
      memcpy(facesCumsizes, facesCumsizesEx, (Nfc+1) * sizeof(int));

      polyhedron = (int **) malloc (Nfc * sizeof(int *));
      l          = 0;
      for (i = 0; i < Nfc; ++i) {
	NvLoc         = facesCumsizes[i+1]-facesCumsizes[i];
	polyhedron[i] = (int *) malloc (NvLoc * sizeof(int));
	for (j = 0; j < NvLoc; ++j, ++l) polyhedron[i][j] = polyhedronEx[l];
      }
    }
    break;
  case 2:
    {
      Nv   = 12;
      Nfc  = 8;
      k    = 1;
      eoa  = 0;
      Nq1d = k+1;
      deg  = 2;

      double verticesEx[] = {0.0, 0.0, 0.0, 5.0, 0.0, 0.0, 5.0, 4.0, 0.0, 3.0, 2.0, 0.0,
			     3.0, 5.0, 0.0, 0.0, 5.0, 0.0, 0.0, 0.0, 5.0, 5.0, 0.0, 5.0,
			     5.0, 4.0, 5.0, 3.0, 2.0, 5.0, 3.0, 5.0, 5.0, 0.0, 5.0, 5.0};

      int polyhedronEx[]  = { 0, 5, 4, 3, 2, 1, 0, 1, 7, 6, 1, 2, 8, 7, 2, 3, 9, 8, 3, 4,
			      10, 9, 4, 5, 11, 10, 5, 0, 6, 11, 6, 7, 8, 9, 10, 11 };

      int facesCumsizesEx[] = {0, 6, 10, 14, 18, 22, 26, 30, 36};

      vertices = (double *) malloc (3 * Nv * sizeof(double));
      memcpy(vertices, verticesEx, 3 * Nv * sizeof(double));

      facesCumsizes = (int *) malloc ((Nfc+1) * sizeof(int));
      memcpy(facesCumsizes, facesCumsizesEx, (Nfc+1) * sizeof(int));

      polyhedron = (int **) malloc (Nfc * sizeof(int *));
      l          = 0;
      for (i = 0; i < Nfc; ++i) {
	NvLoc         = facesCumsizes[i+1]-facesCumsizes[i];
	polyhedron[i] = (int *) malloc (NvLoc * sizeof(int));
	for (j = 0; j < NvLoc; ++j, ++l) polyhedron[i][j] = polyhedronEx[l];
      }
    }
    break;
  case 3:
    {
      Nv   = 5;
      Nfc  = 6;
      k    = 1;
      eoa  = 0;
      Nq1d = k+1;
      deg  = 2;

      double verticesEx[]   = {0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0,
			       0.0, 0.0, 1.0, 0.25, 0.25, 0.25};
      int polyhedronEx[]    = {0, 2, 1, 0, 1, 3, 0, 3, 2, 4, 2, 3, 1, 4, 3, 1, 2, 4};
      int facesCumsizesEx[] = {0, 3, 6, 9, 12, 15, 18};

      vertices = (double *) malloc (3 * Nv * sizeof(double));
      memcpy(vertices, verticesEx, 3 * Nv * sizeof(double));

      facesCumsizes = (int *) malloc ((Nfc+1) * sizeof(int));
      memcpy(facesCumsizes, facesCumsizesEx, (Nfc+1) * sizeof(int));

      polyhedron = (int **) malloc (Nfc * sizeof(int *));
      l          = 0;
      for (i = 0; i < Nfc; ++i) {
	NvLoc         = facesCumsizes[i+1]-facesCumsizes[i];
	polyhedron[i] = (int *) malloc (NvLoc * sizeof(int));
	for (j = 0; j < NvLoc; ++j, ++l) polyhedron[i][j] = polyhedronEx[l];
      }
    }
    break;
  }

  facesOuterNormals = (double **) malloc (Nfc * sizeof(double *));
  facesBndNodesX    = (double **) malloc (Nfc * sizeof(double *));
  facesBndNodesY    = (double **) malloc (Nfc * sizeof(double *));
  facesEdgeNormalsX = (double **) malloc (Nfc * sizeof(double *));
  facesEdgeNormalsY = (double **) malloc (Nfc * sizeof(double *));
  facesXquad2d      = (double **) malloc (Nfc * sizeof(double *));
  facesYquad2d      = (double **) malloc (Nfc * sizeof(double *));
  facesWquad2d      = (double **) malloc (Nfc * sizeof(double *));

  facesAreas     = (double *) malloc (Nfc * sizeof(double));
  facesAlgDists  = (double *) malloc (Nfc * sizeof(double));
  facesNqd2      = (int *) malloc (Nfc * sizeof(int));
  facesDiameters = (double *) malloc (Nfc * sizeof(double));
  facesCentroidX = (double *) malloc (Nfc * sizeof(double));
  facesCentroidY = (double *) malloc (Nfc * sizeof(double));

  for (i = 0; i < Nfc; ++i) {
    NvLoc = facesCumsizes[i+1] - facesCumsizes[i];

    facesOuterNormals[i] = (double *) malloc (3 * sizeof(double));
    facesBndNodesX[i]    = (double *) malloc (k * NvLoc * sizeof(double));
    facesBndNodesY[i]    = (double *) malloc (k * NvLoc * sizeof(double));
    facesEdgeNormalsX[i] = (double *) malloc (NvLoc * sizeof(double));
    facesEdgeNormalsY[i] = (double *) malloc (NvLoc * sizeof(double));
    facesXquad2d[i]      = (double *) malloc (NvLoc * (deg+1) * deg * sizeof(double));
    facesYquad2d[i]      = (double *) malloc (NvLoc * (deg+1) * deg * sizeof(double));
    facesWquad2d[i]      = (double *) malloc (NvLoc * (deg+1) * deg * sizeof(double));
  }

  tmpN = (double *) malloc (Nq1d * sizeof(double));
  tmpW = (double *) malloc (Nq1d * sizeof(double));
  lobatto_compute(Nq1d, tmpN, tmpW);
  internalQuadNodes1d   = (double *) malloc (2 * (k-1) * sizeof(double));
  internalQuadWeights1d = (double *) malloc ((k-1) * sizeof(double));
  for (i = 0; i < k-1; ++i) {
    internalQuadNodes1d[2*i]   = (1.0 - tmpN[i+1]) * 0.5;
    internalQuadNodes1d[2*i+1] = (1.0 + tmpN[i+1]) * 0.5;
    internalQuadWeights1d[i]   = tmpW[i+1] * 0.5;
  }

  polygonalFacesFeatures( k, eoa, vertices, polyhedron, facesCumsizes, Nfc, internalQuadNodes1d,
  			  facesOuterNormals, facesAreas, facesAlgDists,
  			  facesBndNodesX, facesBndNodesY,
  			  facesEdgeNormalsX, facesEdgeNormalsY,
  			  facesXquad2d, facesYquad2d, facesWquad2d, facesNqd2, facesDiameters,
  			  facesCentroidX, facesCentroidY );

#ifdef DEBUG
  cout << "\nFaces outer normals:\n";
  for (i = 0; i < Nfc; ++i) {
    cout << facesOuterNormals[i][0] << '\t'
	 << facesOuterNormals[i][1] << '\t'
	 << facesOuterNormals[i][2] << endl;
  }
  cout << "Faces cumulative sizes:" << endl;
  for (i = 0; i < Nfc; ++i) cout << facesCumsizes[i+1] << endl;
  cout << "Faces areas:" << endl;
  for (i = 0; i < Nfc; ++i) cout << facesAreas[i] << endl;
  cout << "Faces algebraic distances:" << endl;
  for (i = 0; i < Nfc; ++i) cout << facesAlgDists[i] << endl;
#endif

  d3            = ((deg+3)*(deg+2)*(deg+1))/6;
  quadNodes3d   = (double *) malloc (3 * d3 * sizeof(double));
  quadWeights3d = (double *) malloc (d3 * sizeof(double));

  polyhedronFeatures( vertices, Nfc, polyhedron, facesCumsizes, facesOuterNormals,
		      facesAreas, facesAlgDists, deg, &polyhedronVolume, &polyhedronDiameter,
		      polyhedronCentroid, quadNodes3d, quadWeights3d );

#ifdef DEBUG
  cout.precision(20);
  cout << "Polyhedron volume: " << polyhedronVolume << endl;
  cout << "Polyhedron diameter: " << polyhedronDiameter << endl;
  cout << "Quadrature rule:" << endl;
  polyhedronVolumeCheck = 0.0;
  for (i = 0; i < d3; ++i) {
    cout << quadWeights3d[i]
	 << '\t' << quadNodes3d[i]
	 << '\t' << quadNodes3d[i+d3]
	 << '\t' << quadNodes3d[i+2*d3]
	 << endl;
    polyhedronVolumeCheck += quadWeights3d[i];
  }
  cout << "Polyhedron volume check: " << fabs(polyhedronVolume - polyhedronVolumeCheck) << endl;
#endif

  double integral, fromPaper;

  switch(choice) {
  case 1:
    integral = 0.0;
    for (i = 0; i < d3; ++i)
      integral += quadWeights3d[i] *
  	( quadNodes3d[i]*quadNodes3d[i]
  	  + quadNodes3d[i]*quadNodes3d[i+d3]
  	  + quadNodes3d[i+d3]*quadNodes3d[i+d3]
  	  + quadNodes3d[i+2*d3]*quadNodes3d[i+2*d3] );
    fromPaper = 15625.0 / 4.0;
    break;
  case 2:
    integral = 0.0;
    for (i = 0; i < d3; ++i)
      integral += quadWeights3d[i] *
  	( quadNodes3d[i]*quadNodes3d[i]
  	  + quadNodes3d[i]*quadNodes3d[i+d3]
  	  + quadNodes3d[i+d3]*quadNodes3d[i+d3]
  	  + quadNodes3d[i+2*d3]*quadNodes3d[i+2*d3] );
    fromPaper = 33835.0 / 12.0;
    break;
  case 3:
    integral = 0.0;
    for (i = 0; i < d3; ++i)
      integral += quadWeights3d[i] *
  	( quadNodes3d[i]*quadNodes3d[i]
  	  + quadNodes3d[i]*quadNodes3d[i+d3]
  	  + quadNodes3d[i+d3]*quadNodes3d[i+d3]
  	  + quadNodes3d[i+2*d3]*quadNodes3d[i+2*d3] );
    fromPaper = 37.0 / 960.0;
    break;
  }
  cout << "Computed integral: " << integral << endl;
  cout << "From paper: " << fromPaper << endl;
  cout << "Integration error: " << fabs(integral - fromPaper) << endl;

  for (i = 0; i < Nfc; ++i) {
    NvLoc = facesCumsizes[i+1] - facesCumsizes[i];

    free(polyhedron[i]);
    free(facesOuterNormals[i]);
    free(facesBndNodesX[i]);
    free(facesBndNodesY[i]);
    free(facesEdgeNormalsX[i]);
    free(facesEdgeNormalsY[i]);
    free(facesXquad2d[i]);
    free(facesYquad2d[i]);
    free(facesWquad2d[i]);
  }

  free(tmpN);
  free(tmpW);
  free(internalQuadNodes1d);
  free(internalQuadWeights1d);
  free(vertices);
  free(polyhedron);
  free(facesCumsizes);
  free(facesAreas);
  free(facesAlgDists);
  free(facesNqd2);
  free(facesDiameters);
  free(facesCentroidX);
  free(facesCentroidY);
  free(facesOuterNormals);
  free(facesBndNodesX);
  free(facesBndNodesY);
  free(facesEdgeNormalsX);
  free(facesEdgeNormalsY);
  free(facesXquad2d);
  free(facesYquad2d);
  free(facesWquad2d);
  free(quadNodes3d);
  free(quadWeights3d);

  /*
   * Second part of the test: generating quadrature formulas for all the elements of a mesh
   */

  cout << "\n\nSecond part of the test: mesh processing\n\n";
  cout << "Approximation order: ";
  cin  >> k;
  cout << "Extra order of accuracy: ";
  cin  >> eoa;

  deg = 2*k + eoa;
  d3  = ((deg+3)*(deg+2)*(deg+1))/6;

  dualMeshInitialize( &dMesh );

  Nq1d = k+1;
  tmpN = (double *) malloc (Nq1d * sizeof(double));
  tmpW = (double *) malloc (Nq1d * sizeof(double));
  lobatto_compute(Nq1d, tmpN, tmpW);
  internalQuadNodes1d   = (double *) malloc (2 * (k-1) * sizeof(double));
  internalQuadWeights1d = (double *) malloc ((k-1) * sizeof(double));
  for (i = 0; i < k-1; ++i) {
    internalQuadNodes1d[2*i]   = (1.0 - tmpN[i+1]) * 0.5;
    internalQuadNodes1d[2*i+1] = (1.0 + tmpN[i+1]) * 0.5;
    internalQuadWeights1d[i]   = tmpW[i+1] * 0.5;
  }

  polyGeoQuadInitialize( &pGeoQuad );

  for (i = 0; i < NUMMESHES; ++i) {

    dualMeshReadOVM( filenames[i], &dMesh );

    for (j = 0; j < dMesh.Npol; ++j) {

      polyGeoQuadAllocate(j, &dMesh, k, deg, &pGeoQuad );

      polygonalFacesFeatures( k, eoa, dMesh.vertices,
			      pGeoQuad.polyhedron, pGeoQuad.facesCumsizes, pGeoQuad.Nfc,
			      internalQuadNodes1d,
			      pGeoQuad.facesOuterNormals,
			      pGeoQuad.facesAreas, pGeoQuad.facesAlgDists,
			      pGeoQuad.facesBndNodesX, pGeoQuad.facesBndNodesY,
			      pGeoQuad.facesEdgeNormalsX, pGeoQuad.facesEdgeNormalsY,
			      pGeoQuad.facesXquad2d, pGeoQuad.facesYquad2d,
			      pGeoQuad.facesWquad2d, pGeoQuad.facesNqd2, pGeoQuad.facesDiameters,
			      pGeoQuad.facesCentroidX, pGeoQuad.facesCentroidY );

      polyhedronFeatures( dMesh.vertices, pGeoQuad.Nfc,
			  pGeoQuad.polyhedron, pGeoQuad.facesCumsizes,
			  pGeoQuad.facesOuterNormals,
			  pGeoQuad.facesAreas, pGeoQuad.facesAlgDists, deg,
			  &(pGeoQuad.polyhedronVolume), &(pGeoQuad.polyhedronDiameter),
			  pGeoQuad.polyhedronCentroid,
			  pGeoQuad.quadNodes3d, pGeoQuad.quadWeights3d );

      polyGeoQuadDestroy( &pGeoQuad );
    }
    dualMeshDestroy( &dMesh );
  }

  free(tmpN);
  free(tmpW);
  free(internalQuadNodes1d);
  free(internalQuadWeights1d);

  return 0;
}
