#include "geometry.h"


void polyGeoQuadInitialize( polyGeoQuad * pGeoQuad )
{
  pGeoQuad->Nfc = 0;

  pGeoQuad->polyhedron        = NULL;
  pGeoQuad->facesCumsizes     = NULL;

  pGeoQuad->facesOuterNormals = NULL;

  pGeoQuad->facesBndNodesX    = NULL;
  pGeoQuad->facesBndNodesY    = NULL;

  pGeoQuad->facesEdgeNormalsX = NULL;
  pGeoQuad->facesEdgeNormalsY = NULL;

  pGeoQuad->facesNq2d         = NULL;
  pGeoQuad->facesXquad2d      = NULL;
  pGeoQuad->facesYquad2d      = NULL;
  pGeoQuad->facesWquad2d      = NULL;

  pGeoQuad->facesAreas        = NULL;
  pGeoQuad->facesAlgDists     = NULL;
  pGeoQuad->facesDiameters    = NULL;
  pGeoQuad->facesCentroidX    = NULL;
  pGeoQuad->facesCentroidY    = NULL;

  pGeoQuad->polyhedronVolume   = 0.0;
  pGeoQuad->polyhedronDiameter = 0.0;
  pGeoQuad->polyhedronCentroid = NULL;

  pGeoQuad->quadNodes3d   = NULL;
  pGeoQuad->quadWeights3d = NULL;
}


void polyGeoQuadAllocate(
			 const int pIdx,
			 const dualMesh * const dMesh,
			 const int k,
			 const int deg,
			 polyGeoQuad * pGeoQuad
			 )
{
  int d3, f, hFace, tFace, hEdge, tEdge, NvLoc, v;

  pGeoQuad->Nfc = dMesh->p2hfDim[pIdx+1] - dMesh->p2hfDim[pIdx];

  pGeoQuad->polyhedron    = (int **) malloc (pGeoQuad->Nfc * sizeof(int *));
  pGeoQuad->facesCumsizes = (int *) calloc (pGeoQuad->Nfc+1, sizeof(int));

  pGeoQuad->facesOuterNormals = (double **) malloc (pGeoQuad->Nfc * sizeof(double *));
  pGeoQuad->facesBndNodesX    = (double **) malloc (pGeoQuad->Nfc * sizeof(double *));
  pGeoQuad->facesBndNodesY    = (double **) malloc (pGeoQuad->Nfc * sizeof(double *));
  pGeoQuad->facesEdgeNormalsX = (double **) malloc (pGeoQuad->Nfc * sizeof(double *));
  pGeoQuad->facesEdgeNormalsY = (double **) malloc (pGeoQuad->Nfc * sizeof(double *));

  pGeoQuad->facesNq2d         = (int *) malloc (pGeoQuad->Nfc * sizeof(int));
  pGeoQuad->facesXquad2d      = (double **) malloc (pGeoQuad->Nfc * sizeof(double *));
  pGeoQuad->facesYquad2d      = (double **) malloc (pGeoQuad->Nfc * sizeof(double *));
  pGeoQuad->facesWquad2d      = (double **) malloc (pGeoQuad->Nfc * sizeof(double *));

  pGeoQuad->facesAreas     = (double *) malloc (pGeoQuad->Nfc * sizeof(double));
  pGeoQuad->facesAlgDists  = (double *) malloc (pGeoQuad->Nfc * sizeof(double));
  pGeoQuad->facesDiameters = (double *) malloc (pGeoQuad->Nfc * sizeof(double));
  pGeoQuad->facesCentroidX = (double *) malloc (pGeoQuad->Nfc * sizeof(double));
  pGeoQuad->facesCentroidY = (double *) malloc (pGeoQuad->Nfc * sizeof(double));

  pGeoQuad->polyhedronCentroid = (double *) malloc (3 * sizeof(double));

  d3 = ((deg+3)*(deg+2)*(deg+1))/6;

  pGeoQuad->quadNodes3d   = (double *) malloc (3 * d3 * sizeof(double));
  pGeoQuad->quadWeights3d = (double *) malloc (d3 * sizeof(double));

  for (f = 0; f < pGeoQuad->Nfc; ++f) {
    hFace = dMesh->p2hf[ dMesh->p2hfDim[pIdx] + f ];
    tFace = hFace / 2;
    NvLoc = dMesh->f2heDim[ tFace + 1 ] - dMesh->f2heDim[ tFace ];

    pGeoQuad->polyhedron[f]      = (int *) malloc (NvLoc * sizeof(int));
    pGeoQuad->facesCumsizes[f+1] = pGeoQuad->facesCumsizes[f] + NvLoc;

    pGeoQuad->facesOuterNormals[f] = (double *) malloc (3 * sizeof(double));
    pGeoQuad->facesBndNodesX[f]    = (double *) malloc (k * NvLoc * sizeof(double));
    pGeoQuad->facesBndNodesY[f]    = (double *) malloc (k * NvLoc * sizeof(double));
    pGeoQuad->facesEdgeNormalsX[f] = (double *) malloc (NvLoc * sizeof(double));
    pGeoQuad->facesEdgeNormalsY[f] = (double *) malloc (NvLoc * sizeof(double));
    pGeoQuad->facesXquad2d[f]      = (double *) malloc (NvLoc * (deg+1) * deg * sizeof(double));
    pGeoQuad->facesYquad2d[f]      = (double *) malloc (NvLoc * (deg+1) * deg * sizeof(double));
    pGeoQuad->facesWquad2d[f]      = (double *) malloc (NvLoc * (deg+1) * deg * sizeof(double));

    if ( hFace%2 ) {
      for (v = 0; v < NvLoc; ++v) {
	hEdge = dMesh->f2he[ dMesh->f2heDim[tFace] + NvLoc - 1 - v ];
	tEdge = hEdge/2;
	pGeoQuad->polyhedron[f][v] = dMesh->edges[ 2 * tEdge + 1 - hEdge%2 ];
      }
    } else {
      for (v = 0; v < NvLoc; ++v) {
	hEdge = dMesh->f2he[ dMesh->f2heDim[tFace] + v ];
	tEdge = hEdge/2;
	pGeoQuad->polyhedron[f][v] = dMesh->edges[ 2 * tEdge + hEdge%2 ];
      }
    }
  }
}


void polyGeoQuadDestroy( polyGeoQuad * pGeoQuad )
{
  int f;

  for (f = 0; f < pGeoQuad->Nfc; ++f) {
    free( pGeoQuad->polyhedron[f] );        pGeoQuad->polyhedron[f]       = NULL;
    free( pGeoQuad->facesOuterNormals[f] ); pGeoQuad->facesOuterNormals[f] = NULL;
    free( pGeoQuad->facesBndNodesX[f] );    pGeoQuad->facesBndNodesX[f]    = NULL;
    free( pGeoQuad->facesBndNodesY[f] );    pGeoQuad->facesBndNodesY[f]    = NULL;
    free( pGeoQuad->facesEdgeNormalsX[f] ); pGeoQuad->facesEdgeNormalsX[f] = NULL;
    free( pGeoQuad->facesEdgeNormalsY[f] ); pGeoQuad->facesEdgeNormalsY[f] = NULL;
    free( pGeoQuad->facesXquad2d[f] );      pGeoQuad->facesXquad2d[f]      = NULL;
    free( pGeoQuad->facesYquad2d[f] );      pGeoQuad->facesYquad2d[f]      = NULL;
    free( pGeoQuad->facesWquad2d[f] );      pGeoQuad->facesWquad2d[f]      = NULL;
  }

  free( pGeoQuad->polyhedron );         pGeoQuad->polyhedron         = NULL;
  free( pGeoQuad->facesCumsizes );      pGeoQuad->facesCumsizes      = NULL;

  free( pGeoQuad->facesOuterNormals );  pGeoQuad->facesOuterNormals  = NULL;

  free( pGeoQuad->facesBndNodesX );     pGeoQuad->facesBndNodesX     = NULL;
  free( pGeoQuad->facesBndNodesY );     pGeoQuad->facesBndNodesY     = NULL;

  free( pGeoQuad->facesEdgeNormalsX );  pGeoQuad->facesEdgeNormalsX  = NULL;
  free( pGeoQuad->facesEdgeNormalsY );  pGeoQuad->facesEdgeNormalsY  = NULL;

  free( pGeoQuad->facesNq2d );          pGeoQuad->facesNq2d          = NULL;
  free( pGeoQuad->facesXquad2d );       pGeoQuad->facesXquad2d       = NULL;
  free( pGeoQuad->facesYquad2d );       pGeoQuad->facesYquad2d       = NULL;
  free( pGeoQuad->facesWquad2d );       pGeoQuad->facesWquad2d       = NULL;

  free( pGeoQuad->facesAreas );         pGeoQuad->facesAreas         = NULL;
  free( pGeoQuad->facesAlgDists );      pGeoQuad->facesAlgDists      = NULL;
  free( pGeoQuad->facesDiameters );     pGeoQuad->facesDiameters     = NULL;
  free( pGeoQuad->facesCentroidX );     pGeoQuad->facesCentroidX     = NULL;
  free( pGeoQuad->facesCentroidY );     pGeoQuad->facesCentroidY     = NULL;

  free( pGeoQuad->polyhedronCentroid ); pGeoQuad->polyhedronCentroid = NULL;

  free( pGeoQuad->quadNodes3d );       pGeoQuad->quadNodes3d         = NULL;
  free( pGeoQuad->quadWeights3d );     pGeoQuad->quadWeights3d       = NULL;

  pGeoQuad->Nfc = 0;
  pGeoQuad->polyhedronVolume      = 0.0;
  pGeoQuad->polyhedronDiameter    = 0.0;
}


void polygonFeatures(
		     const int k,
		     const int eoa,
		     const double * const vertices,
		     const int * const face,
		     const int NvLoc,
		     const double * const internalQuadNodes1d,
		     double * const xbnd,
		     double * const ybnd,
		     double * const Nx,
		     double * const Ny,
		     double * const xquad2d,
		     double * const yquad2d,
		     double * const wquad2d,
		     int    * Nq2d,
		     double * h,
		     double * area,
		     double * xc,
		     double * yc
		     )
{
  int i, j, idx, N, counter, counter2;
  double x[2], y[2];
  double * polygon = NULL;
  double **tmpX = NULL, **tmpY = NULL, **tmpW = NULL;

  polygon = (double *) malloc (2 * (NvLoc+1) * sizeof(double));

  for (i = 0; i < NvLoc; ++i) {
    idx  = 2*face[i];
    x[0] = vertices[idx]; y[0] = vertices[idx+1];
    idx  = 2*face[(i+1)%NvLoc];
    x[1] = vertices[idx]; y[1] = vertices[idx+1];

    xbnd[i] = x[0]; ybnd[i] = y[0];
    polygon[i] = x[0]; polygon[i+NvLoc+1] = y[0];

    /* internalQuadNodes1d and internalQuadWeights1d store only the k-1 internal nodes and weights
     * of the 1d Gauss Lobatto rule on [-1,1] of order k */
    for (j = 0; j < k-1; ++j) {
      xbnd[NvLoc+(k-1)*i+j] = internalQuadNodes1d[2*j]*x[0] + internalQuadNodes1d[2*j+1]*x[1];
      ybnd[NvLoc+(k-1)*i+j] = internalQuadNodes1d[2*j]*y[0] + internalQuadNodes1d[2*j+1]*y[1];
    }

    Nx[i] = y[1] - y[0];
    Ny[i] = x[0] - x[1];
  }
  polygon[NvLoc]     = polygon[0];
  polygon[2*NvLoc+1] = polygon[NvLoc+1];

  /* Compute 2d quadrature rule */
  N = 2*k+eoa;

  tmpX = (double **) malloc (NvLoc * (N+1) * sizeof(double *));
  tmpY = (double **) malloc (NvLoc * (N+1) * sizeof(double *));
  tmpW = (double **) malloc (NvLoc * (N+1) * sizeof(double *));

  for (i = 0; i < NvLoc*(N+1); ++i) {
    tmpX[i] = (double *) malloc (N * sizeof(double));
    tmpY[i] = (double *) malloc (N * sizeof(double));
    tmpW[i] = (double *) malloc (N * sizeof(double));
  }

  polygauss_2013(polygon, NvLoc+1, N, tmpX, tmpY, tmpW, &counter, h);
  *Nq2d = counter * N;

  for (i = 0, counter2 = 0; i < counter; ++i, counter2 += N) {
    memcpy(xquad2d + counter2, tmpX[i], N * sizeof(double));
    memcpy(yquad2d + counter2, tmpY[i], N * sizeof(double));
    memcpy(wquad2d + counter2, tmpW[i], N * sizeof(double));
  }

  *area = ( cblas_ddot(NvLoc, Nx, 1, xbnd, 1) + cblas_ddot(NvLoc, Ny, 1, ybnd, 1) ) * 0.5;
  *xc   = cblas_ddot(*Nq2d, wquad2d, 1, xquad2d, 1) / (*area);
  *yc   = cblas_ddot(*Nq2d, wquad2d, 1, yquad2d, 1) / (*area);

  for (i = 0; i < NvLoc*(N+1); ++i) {
    free(tmpX[i]);
    free(tmpY[i]);
    free(tmpW[i]);
  }
  free(polygon);
  free(tmpX);
  free(tmpY);
  free(tmpW);
}


void polygonalFacesFeatures(
			    const int k,
			    const int eoa,
			    const double * const vertices,
			    int ** const polyhedron,
			    const int * const facesCumsizes,
			    const int Nfc,
			    const double * const internalQuadNodes1d,
			    double ** const facesOuterNormals,
			    double * const facesAreas,
			    double * const facesAlgDists,
			    double ** const facesBndNodesX,
			    double ** const facesBndNodesY,
			    double ** const facesEdgeNormalsX,
			    double ** const facesEdgeNormalsY,
			    double ** const facesXquad2d,
			    double ** const facesYquad2d,
			    double ** const facesWquad2d,
			    int * const facesNq2d,
			    double * const facesDiameters,
			    double * const facesCentroidX,
			    double * const facesCentroidY
			    )
{
  int e, f, Ned;
  int *tmpInt = NULL;
  double faceNormalNorm;
  double faceCenter[3], faceOuterNormal[3];
  double buffer1[2];
#ifdef EXACT
  double buffer2[2], buffer3[2];
#else
  double xba, yba, zba, xca, yca, zca;
#endif
  double *tmpDouble = NULL;
  double R[6], ct, ctm, st, checkFaceArea;

  for(f = 0; f < Nfc; ++f) {
    faceCenter[0] = 0.0; faceCenter[1] = 0.0; faceCenter[2] = 0.0;
    Ned = facesCumsizes[f+1] - facesCumsizes[f];
    for (e = 0; e < Ned; ++e) {
      faceCenter[0] += vertices[ 3 * polyhedron[f][e]     ];
      faceCenter[1] += vertices[ 3 * polyhedron[f][e] + 1 ];
      faceCenter[2] += vertices[ 3 * polyhedron[f][e] + 2 ];
    }
    faceCenter[0] /= (double)(Ned);
    faceCenter[1] /= (double)(Ned);
    faceCenter[2] /= (double)(Ned);

    faceOuterNormal[0] = 0.0; faceOuterNormal[1] = 0.0; faceOuterNormal[2] = 0.0;
    for (e = 0; e < Ned; ++e) {
#ifdef EXACT
      faceOuterNormal[0] += orient2d( vertices + 3*polyhedron[f][e] + 1,
				      vertices + 3*polyhedron[f][(e+1)%Ned] + 1,
				      faceCenter + 1 );
      buffer1[0] = vertices[ 3*polyhedron[f][e] + 2 ];
      buffer1[1] = vertices[ 3*polyhedron[f][e]     ];
      buffer2[0] = vertices[ 3*polyhedron[f][(e+1)%Ned] + 2 ];
      buffer2[1] = vertices[ 3*polyhedron[f][(e+1)%Ned]     ];
      buffer3[0] = faceCenter[2];
      buffer3[1] = faceCenter[0];
      faceOuterNormal[1] += orient2d( buffer1, buffer2, buffer3 );
      faceOuterNormal[2] += orient2d( vertices + 3*polyhedron[f][e],
				      vertices + 3*polyhedron[f][(e+1)%Ned],
				      faceCenter );
#else
      xba = vertices[ 3*polyhedron[f][e]   ] - faceCenter[0];
      yba = vertices[ 3*polyhedron[f][e]+1 ] - faceCenter[1];
      zba = vertices[ 3*polyhedron[f][e]+2 ] - faceCenter[2];
      xca = vertices[ 3*polyhedron[f][(e+1)%Ned]   ] - faceCenter[0];
      yca = vertices[ 3*polyhedron[f][(e+1)%Ned]+1 ] - faceCenter[1];
      zca = vertices[ 3*polyhedron[f][(e+1)%Ned]+2 ] - faceCenter[2];
      faceOuterNormal[0] += yba * zca - yca * zba;
      faceOuterNormal[1] += zba * xca - zca * xba;
      faceOuterNormal[2] += xba * yca - xca * yba;
#endif
    }
    faceNormalNorm = cblas_dnrm2(3, faceOuterNormal, 1);
    faceOuterNormal[0] /= faceNormalNorm;
    faceOuterNormal[1] /= faceNormalNorm;
    faceOuterNormal[2] /= faceNormalNorm;

    facesOuterNormals[f][0] = faceOuterNormal[0];
    facesOuterNormals[f][1] = faceOuterNormal[1];
    facesOuterNormals[f][2] = faceOuterNormal[2];

    facesAreas[f] = faceNormalNorm / 2.0;

    facesAlgDists[f] = 0.0;
    for (e = 0; e < Ned; ++e) {
      facesAlgDists[f] += faceOuterNormal[0] * vertices[ 3*polyhedron[f][e] ]
	+ faceOuterNormal[1] * vertices[ 3*polyhedron[f][e] + 1 ]
	+ faceOuterNormal[2] * vertices[ 3*polyhedron[f][e] + 2 ];
    }
    facesAlgDists[f] /= (double)(Ned);

    /* Array for rotated vertices */
    tmpDouble = (double *) malloc ( 2 * Ned * sizeof(double) );
    tmpInt    = (int *) malloc ( Ned * sizeof(int) );

    if ( ( faceOuterNormal[2] > -0.5 ) && ( faceOuterNormal[2] < 0.5 ) ) {
      /* Rotate towards e_z, x and y survive */
      ct  = faceOuterNormal[2];
      ctm = 1.0 - ct;
      st  = sqrt( 1.0 - ct*ct );

      buffer1[0] = faceOuterNormal[1] / st;  /* omega_x */
      buffer1[1] = -faceOuterNormal[0] / st; /* omega_y */

      /* First two rows of the rotation matrix */
      R[0] = ct + buffer1[0]*buffer1[0]*ctm;
      R[1] = buffer1[0]*buffer1[1]*ctm;
      R[2] = R[1];
      R[3] = ct + buffer1[1]*buffer1[1]*ctm;
      R[4] = st * buffer1[1];
      R[5] = -st * buffer1[0];
    } else {
      /* Rotate towards e_x, y and z survive */
      ct  = faceOuterNormal[0];
      ctm = 1.0 - ct;
      st  = sqrt( 1.0 - ct*ct );

      buffer1[0] = faceOuterNormal[2] / st;  /* omega_y */
      buffer1[1] = -faceOuterNormal[1] / st; /* omega_z */

      /* Second and third rows of the rotation matrix */
      R[0] = st * buffer1[1];
      R[1] = -st * buffer1[0];
      R[2] = ct + buffer1[0]*buffer1[0]*ctm;
      R[3] = buffer1[0]*buffer1[1]*ctm;
      R[4] = R[3];
      R[5] = ct + buffer1[1]*buffer1[1]*ctm;
    }

    for (e = 0; e < Ned; ++e) {
      tmpDouble[2*e]   = R[0] * vertices[ 3*polyhedron[f][e] ]
	+ R[2] * vertices[ 3*polyhedron[f][e] + 1 ]
	+ R[4] * vertices[ 3*polyhedron[f][e] + 2 ];
      tmpDouble[2*e+1] = R[1] * vertices[ 3*polyhedron[f][e] ]
	+ R[3] * vertices[ 3*polyhedron[f][e] + 1 ]
	+ R[5] * vertices[ 3*polyhedron[f][e] + 2 ];
      tmpInt[e] = e;
    }

    polygonFeatures(k, eoa, tmpDouble, tmpInt, Ned, internalQuadNodes1d,
		    facesBndNodesX[f], facesBndNodesY[f],
		    facesEdgeNormalsX[f], facesEdgeNormalsY[f],
		    facesXquad2d[f], facesYquad2d[f], facesWquad2d[f], facesNq2d+f,
		    facesDiameters+f, &checkFaceArea, facesCentroidX+f, facesCentroidY+f);

    if ( fabs(checkFaceArea - facesAreas[f]) > 1e-12 ) {
      printf("File %s line %i: mismatch while computing area of face %i. "
	     "First estimate: %e. Second estimate: %e. Aborting ...\n",
	     __FILE__, __LINE__, f, facesAreas[f], checkFaceArea);
      exit(1);
    }

    free(tmpDouble);
    free(tmpInt);
  }
}


void polyhedronFeatures(
			const double * const vertices,
			const int Nfc,
			int ** const polyhedron,
			const int * const facesCumsizes,
			double ** const facesOuterNormals,
			const double * const facesAreas,
			const double * const facesAlgDists,
			const int deg,
			double * polyhedronVolume,
			double * polyhedronDiameter,
			double * polyhedronCentroid,
			double * quadNodes3d,
			double * quadWeights3d
			)
{
  int i, j, Ned, NvUnique, vCounter, itmp;
  int **buffer = NULL;

  size_t facesCumsizesCast[Nfc+1];
  size_t *polyhedronLocal = NULL;

  double xTmp, yTmp, zTmp, diamSq;
  double facesOuterNormalsReshaped[3*Nfc];
  double *polyVertices = NULL;

  symbol x("x"), y("y"), z("z");
  symtab table;
  table["x"] = x; table["y"] = y; table["z"] = z;
  parser reader(table);
  lst l;
  ex Int;

  /*
   * Polyhedron volume. Reference: Chin et al. 2015
   */

  *polyhedronVolume = cblas_ddot(Nfc, facesAreas, 1, facesAlgDists, 1);
  *polyhedronVolume /= 3.0;

  /*
   * Select unique polyhedron vertices
   */

  vCounter = facesCumsizes[Nfc];

  /* buffer[i,0]: position to be used for ordering;
   * buffer[i,1]: sequential index to be used for second ordering
   * buffer[i,2]: original global numbering */

  buffer = (int **) malloc (vCounter * sizeof(int *));
  for (i = 0; i < vCounter; ++i) buffer[i] = (int *) malloc (3 * sizeof(int));

  itmp = 0;
  for (i = 0; i < Nfc; ++i) {
    Ned = facesCumsizes[i+1] - facesCumsizes[i];
    for (j = 0; j < Ned; ++j, ++itmp) {
      buffer[itmp][0] = polyhedron[i][j];
      buffer[itmp][1] = itmp;
      buffer[itmp][2] = buffer[itmp][0];
    }
  }
  qsort( buffer, vCounter, sizeof(int *), compareSize1 );

  itmp         = 0;
  buffer[0][0] = buffer[0][1];
  buffer[0][1] = itmp;
  for (i = 1; i < vCounter; ++i) {
    buffer[i][0] = buffer[i][1];
    if ( buffer[i][2] != buffer[i-1][2] ) {
      buffer[i][1] = ++itmp;
    } else {
      buffer[i][1] = itmp;
    }
  }
  ++itmp;

  NvUnique = itmp;

  /* polyVertices stores all x-coordinates first, then y-coordinates, and z-coordinates last */
  polyVertices = (double *) malloc (3 * NvUnique * sizeof(double));

  polyVertices[0]          = vertices[ 3*buffer[0][2] ];
  polyVertices[NvUnique]   = vertices[ 3*buffer[0][2]+1 ];
  polyVertices[2*NvUnique] = vertices[ 3*buffer[0][2]+2 ];

  itmp = 0;
  for (i = 1; i < vCounter; ++i)
    if ( buffer[i][2] != buffer[i-1][2] ) {
      ++itmp;
      polyVertices[itmp]            = vertices[ 3*buffer[i][2] ];
      polyVertices[itmp+NvUnique]   = vertices[ 3*buffer[i][2]+1 ];
      polyVertices[itmp+2*NvUnique] = vertices[ 3*buffer[i][2]+2 ];
    }

  /* Falling back to original ordering */
  qsort( buffer, vCounter, sizeof(int *), compareSize1 );

  polyhedronLocal = (size_t *) malloc (vCounter * sizeof(size_t));
  for (i = 0; i < vCounter; ++i) {
    polyhedronLocal[i] = (size_t)(buffer[i][1] + 1); /* For details, see chinAlgorithm3D */
    free( buffer[i] );
  }
  free( buffer );

  /*
   * Polyhedron diameter
   */

  *polyhedronDiameter = -DBL_MAX;
  for (i = 0; i < NvUnique; ++i) {
    for (j = i+1; j < NvUnique; ++j) {
      xTmp   = polyVertices[i] - polyVertices[j];
      yTmp   = polyVertices[i+NvUnique] - polyVertices[j+NvUnique];
      zTmp   = polyVertices[i+2*NvUnique] - polyVertices[j+2*NvUnique];
      diamSq = xTmp*xTmp + yTmp*yTmp + zTmp*zTmp;
      if ( (*polyhedronDiameter) < diamSq ) *polyhedronDiameter = diamSq;
    }
  }
  *polyhedronDiameter = sqrt(*polyhedronDiameter);

  /*
   * Polyhedron centroid
   */

  facesCumsizesCast[0] = 0;
  for (i = 1; i <= Nfc; ++i) {
    facesCumsizesCast[i] = (size_t)facesCumsizes[i];

    j = i-1;
    facesOuterNormalsReshaped[j]       = facesOuterNormals[j][0];
    facesOuterNormalsReshaped[j+Nfc]   = facesOuterNormals[j][1];
    facesOuterNormalsReshaped[j+2*Nfc] = facesOuterNormals[j][2];
  }

  l = {x, y, z};

  Int = chinAlgorithm3D( reader("x"), polyVertices, (size_t)NvUnique, polyhedronLocal, (size_t)Nfc,
			 facesCumsizesCast, facesOuterNormalsReshaped, facesAlgDists, l );
  if (is_a<numeric>(Int)) {
    polyhedronCentroid[0] = ex_to<numeric>(Int).to_double() / (*polyhedronVolume);
  } else {
    printf("File %s line %i: encountered error while computing centroid, aborting ...\n",
	   __FILE__, __LINE__);
    exit(1);
  }
  Int = chinAlgorithm3D( reader("y"), polyVertices, (size_t)NvUnique, polyhedronLocal, (size_t)Nfc,
			 facesCumsizesCast, facesOuterNormalsReshaped, facesAlgDists, l );
  if (is_a<numeric>(Int)) {
    polyhedronCentroid[1] = ex_to<numeric>(Int).to_double() / (*polyhedronVolume);
  } else {
    printf("File %s line %i: encountered error while computing centroid, aborting ...\n",
	   __FILE__, __LINE__);
    exit(1);
  }
  Int = chinAlgorithm3D( reader("z"), polyVertices, (size_t)NvUnique, polyhedronLocal, (size_t)Nfc,
			 facesCumsizesCast, facesOuterNormalsReshaped, facesAlgDists, l );
  if (is_a<numeric>(Int)) {
    polyhedronCentroid[2] = ex_to<numeric>(Int).to_double() / (*polyhedronVolume);
  } else {
    printf("File %s line %i: encountered error while computing centroid, aborting ...\n",
	   __FILE__, __LINE__);
    exit(1);
  }

  /*
   * Quadrature rule on polyhedron
   */

  constructQuadrature( polyVertices, (size_t)NvUnique, polyhedronLocal, (size_t)Nfc,
		       facesCumsizesCast, deg, *polyhedronDiameter, *polyhedronVolume,
		       polyhedronCentroid, facesOuterNormalsReshaped, facesAlgDists,
		       quadNodes3d, quadWeights3d );

  free( polyVertices );
  free( polyhedronLocal );
}
