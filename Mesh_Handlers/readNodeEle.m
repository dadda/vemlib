function mesh = readNodeEle( basename )
% readNodeEle   Create minimal mesh data structure from text files.
%
%    MESH = READNODEELE( BASENAME ) creates a struct MESH containing two fields:
%
%    * MESH.NODE : NVER x 2 array of mesh node coordinates, where NVER is the number
%                  of mesh nodes.
%
%    * MESH.ELEMENT : NELE x 1 cella array with element connectivity information, where NELE
%                  is the number of elements (polygons) of the mesh.
%                  MESH.ELEMENT{K} is a row array with the indices of the mesh nodes
%                  forming the boundary of element K in counter-clockwise order.
%
%    BASENAME is the basename of two files, .node and .ele, which containing, respectively:
%
%    * BASENAME.node contains a list of 2d points.
%
%      First line : <# of points> <# of boundary markers (0 or 1)>
%      Remaining lines : <x> <y> <boundary marker (if 1 is in first line)>
%
%    * BASENAME.ele contains a list of polygons.
%
%      First line : <# of polygons>
%      Remaining lines : <# of nodes of current polygon> <node 1> <node 2> ... <node #>

% Read node file
fid     = fopen([basename '.node']);
tmp     = fscanf(fid, '%d', 2);
Nver    = tmp(1);
hasFlag = tmp(2);

Node = zeros(Nver, 2);
for i = 1:Nver
    tmp = fscanf(fid, '%f', 2+hasFlag);
    Node(i,:) = tmp(1:2);
end
fclose(fid);

% Read element file
fid     = fopen([basename '.ele']);
Nele    = fscanf(fid, '%d', 1);

Element = cell(Nele, 1);
for i = 1:Nele
    NverLoc    = fscanf(fid, '%d', 1);
    tmp        = fscanf(fid, '%d', [1,NverLoc]);
    coords     = Node( tmp, : );
    center     = sum( coords, 1 ) / NverLoc;
    coords     = bsxfun(@minus, coords, center);
    coords     = coords([1:end 1],:);
    test       = sum( coords(1:end-1,1) .* coords(2:end,2) - coords(1:end-1,2) .* coords(2:end,1) );
    if test > 0
        Element{i} = tmp;
    else
        Element{i} = tmp(end:-1:1);
    end
end
fclose(fid);

mesh.Node    = Node;
mesh.Element = Element;

end
