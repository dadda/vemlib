function [mesh] = tmeshgen(hmax)
% --------------------------------------------------
% generates triangular mesh in MY format
% See meshstruct.m for format
%---------------------------------------------------
% generate triangular mesh with matlab
M = [2 4 0 0 1 1 0 1 1 0]';  % domain data
D=decsg(M);
[P,E,T]=initmesh(D,'Hmax',hmax);
pdemesh(P,E,T);
P=P';
T=T';
m=size(T,1);
T=[3*ones(m,1),T(:,1:3)];

mesh = meshstruct(P,T);  % build the mesh structure file