%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Script for generating a Voronoi mesh from scratch

addpath ../../PolyMesher/
addpath PolyMesher_Addons/
addpath ../VEM_curved

rng('shuffle')

fprintf('Generating Voronoi mesh ...\n');

% %
% % Square
% %
% 
% xMin           = 0.25;
% xMax           = 0.75;
% yMin           = 0.25;
% yMax           = 0.75;
% BdBox          = [xMin xMax yMin yMax];
% myElementNodes = [xMin yMin;
%                   xMax yMin;
%                   xMax yMax;
%                   xMin yMax];
% refArea        = ( xMax - xMin ) * ( yMax - yMin );
% Domain         = @voroDomain;

% %
% % Unit disk
% %
% 
% xMin           = -1;
% xMax           = 1;
% yMin           = -1;
% yMax           = 1;
% BdBox          = [xMin xMax yMin yMax];
% center         = [0 0];
% radius         = 1;
% myElementNodes = [center(1) center(2) radius]; % x, y coordinates of the center, radius
% refArea        = pi;
% Domain         = @unitdiskDomain;

%
% Sector
%

xMin           = 1;
xMax           = 4;
yMin           = -sqrt(15);
yMax           = sqrt(15);
BdBox          = [xMin xMax yMin yMax];
center         = [0 0];
externalRadius = 4;
internalRadius = 1;
myElementNodes = [center(1) center(2) externalRadius internalRadius];
refArea        = 16*asin(1) - sqrt(15) - 16*asin(0.25);
Domain         = @sectorDomain;

% %
% % Quarter
% %
% 
% xMin           = 0;
% xMax           = 2;
% yMin           = 0;
% yMax           = 2;
% BdBox          = [xMin xMax yMin yMax];
% myElementNodes = BdBox;
% refArea        = 3*pi/4;
% Domain         = @quarterDomain;

% Tolerance for the checking the overall area of the output mesh
tolerance = inf;

% Minimum side length allowed
minH = 1e-10;

% Number of polygons
NElemVect = 2.^(11:18);
%input('Enter the number of elements: ');

MaxIterVect = 100;

bnd = 'sector';

% meshOutputExt = '.off';
meshOutputExt = '.mat';

for NElem = NElemVect

    % Choose random distribution for seed points:
    % 'uniform': uniform distribution
    % 'weighted': distribute control points according to subtriangulation
    % 'normal': bivariate normal distribution with mean and standard deviation as prescribed in
    %    ./generate_bivariateNormal.r. This option requires the R statistics software to be
    %    installed
    randType = 'uniform';

    for l = 1:length(MaxIterVect)

        % By setting MaxIter = 0, completely random seed points will be used to generate
        % the Voronoi mesh. On the other hand, setting MaxIter > 0 will result in a smoother
        % mesh
        MaxIter = MaxIterVect(l);

        % Output file
        filename = sprintf( 'meshfiles2d/sector/sector_%06i_hmin_1e-10_MaxIter_%03i_corrected', ...
                            NElem, MaxIter );

        % Numerical roundoff errors may cause PolyMesher to generate a mesh that does not cover the original
        % domain. In this happens, we relaunch PolyMesher. Otherwise, the minimum edge length of the
        % generated mesh is displayed and the user is asked either to keep the output mesh or
        % or to restart PolyMesher.

        p = gcp;

        spmd
            while true
                [Node,Element] = PolyMesher_modified(Domain,NElem,MaxIter,randType,myElementNodes,BdBox);
                xNode = max(min(Node(:,1), xMax), xMin);
                yNode = max(min(Node(:,2), yMax), yMin);
                Node = [xNode yNode];

                checkArea = 0.0;
                for K = 1:NElem
                    NverLoc = length(Element{K});
                    Nx = Node(Element{K}([2:NverLoc 1]),2) - Node(Element{K},2);
                    Ny = Node(Element{K},1) - Node(Element{K}([2:NverLoc 1]),1);
                    checkArea = checkArea + (Nx' * Node(Element{K},1) + Ny' * Node(Element{K},2))/2.;
                end

                relError = abs(refArea - checkArea)/refArea;
                if (relError < tolerance)
                    he_min_sq = Inf;
                    Nver_min  = Inf;
                    for K = 1:NElem
                        tmp       = sum ( ( Node( Element{K}(2:end), : ) - Node( Element{K}(1:end-1), : ) ).^ 2, 2 );
                        he_min_sq = min( min( tmp ), he_min_sq );
                        Nver_min  = min( Nver_min, length(Element{K}) );
                    end
                    fprintf('%i %e\n', Nver_min, he_min_sq);
                    if Nver_min > 2 && he_min_sq > minH^2
        %                 flag = input(sprintf('Reference area checked. he_min = %.4e. Restart? (0/1): ', ...
        %                                      he_min));
        %                 if flag == 0
                            break
        %                 end
        %             else
        %                 warning('Degenerate polygon. Restarting PolyMesher...');
                    end
        %         else
        %             warning('Relative error: %.4e. Restarting Polymesher...', relError);
                end
            end

            Nver = size(Node,1);
            Nelt = size(Element,1);

            fid = fopen(sprintf('tmp%i.off',labindex),'w');
            fprintf(fid, 'OFF\n');

            % Number of nodes, number of elements, 0
            fprintf(fid, '%d %d 0\n', Nver, Nelt);

            for i = 1:Nver
                fprintf(fid, '%.16f %.16f 0\n', Node(i,:));
            end

            for i = 1:Nelt
                NverLoc = length(Element{i});
                fprintf( fid, '%s\n', num2str( [ NverLoc Element{i}-1 ] ) );
            end
            fclose(fid);

            fprintf('I am worker %i and I am done\n', labindex);
        end

        fprintf('\nEveryone is done. Relative errors: %s\n', num2str(horzcat(relError{:})));

        % Save the mesh realizing the minimum error
        [~, idx]     = min(horzcat(relError{:}));
        mesh         = [];
        mesh.Node    = Node{idx};
        mesh.Element = Element{idx};
        mesh.Node    = correctNodes(mesh, bnd);

        if strcmp( meshOutputExt, '.mat' )
            save([filename '.mat'], 'mesh')
        else
            writeOFFmesh( mesh, [filename '.off'] );
        end

        fprintf('Mesh saved\n');

        % %
        % % Plot the output mesh
        % %
        % 
        % figure()
        % axis equal; hold on;
        % 
        % % maxSideNum = 0;
        % % for K = 1:NElem
        % %     maxSideNum = max( maxSideNum, length(mesh.Element{K}) );
        % % end
        % 
        % % At the center of each element, the corresponding number of sides is reported.
        % % Element having the maximum number of sides are painted in yellow.
        % 
        % for K = 1:NElem
        %     myBoundary = mesh.Node(mesh.Element{K}, :);
        % 
        %     plot([myBoundary(:,1); myBoundary(1,1)], ...
        %          [myBoundary(:,2); myBoundary(1,2)], 'b')
        % %     if length(mesh.Element{K}) == maxSideNum
        % %         patch(myBoundary(:,1), myBoundary(:,2), 'y')
        % %     else
        % %         patch(myBoundary(:,1), myBoundary(:,2), 'b')
        % %     end
        % %     barycenter = sum(myBoundary,1)/length(mesh.Element{K});
        % %     text(barycenter(1), barycenter(2), num2str(length(mesh.Element{K})), 'HorizontalAlignment', 'center')
        % end
    end
end
