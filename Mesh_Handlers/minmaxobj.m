function out = minmaxobj(x, N, X0)

x = x(:);

out = N*x - sum(N.*X0, 2);

return
