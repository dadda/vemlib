function points = makeSectorPolyFile(filename, Vmax)

Lmax = sqrt( 4 * Vmax / sqrt(3) );

L      = Lmax; %Lmax/2;
dtheta = asin(L/2);

Rin  = 1;
Rout = 4;

xbar     = Rin;
ybar     = sqrt( Rout^2 - xbar^2 );
thetabar = atan( ybar / xbar );

Npt   = ceil(2*thetabar/dtheta);
theta = (1:Npt-2) * dtheta;

% Create file
fid = fopen(filename, 'w');

%
% Node part
%

points = [ xbar, -ybar;
           Rout * cos(-thetabar + theta'), Rout * sin(-thetabar + theta');
           xbar, ybar ];

Npt = size(points, 1);

fprintf(fid, '%i 2 0 0\n', Npt);
for i = 1:Npt
    fprintf(fid, '%i %.16f %.16f\n', i, points(i,1), points(i,2));
end

% Boundary segments
fprintf(fid, '%i 0\n', Npt);
for i = 1:Npt
    fprintf(fid, '%i %i %i\n', i, i, mod(i,Npt)+1);
end

% No holes
fprintf(fid, '0\n');

fclose(fid);

end
