function mesh = readOFFmesh( filename )

fid = fopen(filename);

% Read file header
fgets(fid);

% Second line contains the following information:
% - number of nodes
% - number of elements
% - number of flags per node
tmp = fscanf(fid, '%d', 3);

Nver = tmp(1);
Nele = tmp(2);

% Read nodes
Node = zeros(Nver, 2);
for i = 1:Nver
    tmp = fscanf(fid, '%f', 3);
    Node(i,:) = tmp(1:2);
end

% Read elements
Element = cell(Nele, 1);
for i = 1:Nele
    NverLoc = fscanf(fid, '%d', 1);
    tmp     = fscanf(fid, '%d', [1,NverLoc]) + 1;
    coords  = Node( tmp, : );
    center  = sum( coords, 1 ) / NverLoc;
    coords  = bsxfun(@minus, coords, center);
    coords  = coords([1:end 1],:);
    test    = sum( coords(1:end-1,1) .* coords(2:end,2) - coords(1:end-1,2) .* coords(2:end,1) );
    if test > 0
        Element{i} = tmp;
    else
        Element{i} = tmp(end:-1:1);
    end
end

% Close OFF file
fclose(fid);

mesh.Node = Node;
mesh.Element = Element;

end
