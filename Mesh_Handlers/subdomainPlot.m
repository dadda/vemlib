
addpath ../Quadrature
addpath ../FETI

Nx = 4;
Ny = 4;

% meshName = './meshfiles2d/voronoi-dual/voronoi_dual_1em6_cut15x15.off';
meshName = sprintf('./meshfiles2d/voronoi-notnested/randVoro__Nelt_000100__hmin_1e-16__minArea_1e-16__tolTotArea_1e-12__cut%02ix%02i.off', Nx, Ny);
subdomainMeshApproach = 1;

% meshName = './meshfiles_AleRusso/poligoni-random/poligoni10';
% subdomainMeshApproach = 0;

k             = 1;
use_mp        = 0;
numSubdomains = Nx*Ny;

if use_mp
    addpath './../../AdvanpixMCT-4.4.4.12666'
    precision = input('Set default precision: ');
    mp.Digits(precision);
    k = mp(k);
end

[xqd1, wqd1] = lobatto(k+1, use_mp);
formula1d    = [(1.-xqd1)/2. (xqd1+1.)/2. wqd1/2.];

[ meshArray, iwest, ieast, isouth, inorth ] = ...
    createMeshArray2d( meshName, subdomainMeshApproach, Nx, Ny, k, formula1d, use_mp );

iCheck = 1;
jCheck = 16;
sCheck = Nx * (jCheck-1) + iCheck;

figure()
set(gcf, 'color',[1 1 1]);
% set(gca, 'color', 'white');
set(gca, 'fontsize', 36);
axis equal; hold on;
CM = jet(numSubdomains);

for s = 1:numSubdomains
    for K = 1:meshArray{s}.Nelt
        NverLoc = meshArray{s}.elements(K,1);
        patch( meshArray{s}.coordinates( meshArray{s}.elements(K,2:NverLoc+1), 1 ), ...
               meshArray{s}.coordinates( meshArray{s}.elements(K,2:NverLoc+1), 2 ), ...
               CM(s, :), 'LineWidth', 2 );
    end
    
%     if s == sCheck
%         for K = 1:meshArray{s}.Nelt
%             NverLoc = meshArray{s}.elements(K,1);
% 
%             patch( meshArray{s}.coordinates( meshArray{s}.elements(K,2:NverLoc+1), 1 ), ...
%                    meshArray{s}.coordinates( meshArray{s}.elements(K,2:NverLoc+1), 2 ), ...
%                    'b' ); %CM(s, :) );
% %             plot( meshArray{s}.coordinates( meshArray{s}.elements(K,2:NverLoc+1), 1 ), ...
% %                   meshArray{s}.coordinates( meshArray{s}.elements(K,2:NverLoc+1), 2 ), ...
% %                   'b.' );
%         end
%         plot( meshArray{s}.coordinates( unique( [ iwest{s}; ieast{s}; isouth{s}; inorth{s} ] ), 1 ), ...
%               meshArray{s}.coordinates( unique( [ iwest{s}; ieast{s}; isouth{s}; inorth{s} ] ), 2 ), ...
%               'k.' );        
%         plot( meshArray{s}.coordinates( iwest{s}, 1 ), ...
%               meshArray{s}.coordinates( iwest{s}, 2 ), ...
%               'g+' );
%         plot( meshArray{s}.coordinates( isouth{s}, 1 ), ...
%               meshArray{s}.coordinates( isouth{s}, 2 ), ...
%               'rx' );
%     end
end
