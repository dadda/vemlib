meshnames = {'meshfiles2d/voronoi-nested/regularized_MaxIter10_000016.mat';
	     'meshfiles2d/voronoi-nested/regularized_MaxIter10_000064.mat';
	     'meshfiles2d/voronoi-nested/regularized_MaxIter10_000256.mat';
	     'meshfiles2d/voronoi-nested/regularized_MaxIter10_001012.mat'};

nmeshes = length(meshnames);

for i = 1:nmeshes
  load(meshname)
  figure()
  
  Nelt = length(mesh.Element);

				% Number of edges per element
  Nedges = zeros(Nelt,1);
  for K = 1:Nelt
    Nedges(K) = length(mesh.Element{K});
  end
  maxNed = max(Nedges);

  hist(Nedges, 1:maxNed)
end

