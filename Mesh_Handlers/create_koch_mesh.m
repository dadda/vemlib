% Generate a mesh by embedding Koch snowflakes into rectangles

for N = 4:2:20

    % Nx = input('Number of elements in the x-direction: ');
    % Ny = input('Number of elements in the y-direction: ');
    Nx = N;
    Ny = N;

    % niter = input('Number of iterations of the Koch snowflake: ');
    % scalingFactor = input('Scaling factor for embedding Koch snowflakes: ');
    niter = 4;
    scalingFactor = 0.618;

    % refineSides = input('Do you want to refine the sides of the embedding rectangles? 0/1 [0]: ');
    % if isempty(refineSides)
    %     refineSides = 0;
    % end
    refineSides = 0;

    Hx = 1./Nx;
    Hy = 1./Ny;

    flake_points = koch_snowflake(niter);
    numKoch      = size(flake_points, 1);
    minLength    = min( Hx*scalingFactor*(1./3)^niter, ...
                        Hy*scalingFactor*(1./3)^niter );

    % Locate the bottom tip of the flake
    [yminVal, yminIdx] = min(flake_points(:,2));

    nodes    = cell(Nx, Ny);
    Element  = cell(2*Nx*Ny, 1);

    sideDofs = cell(Nx, Ny, 4); % south, north, west, east

    if refineSides
        npts0 = round( ( Hy * (1-scalingFactor)/2 ) / minLength ) - 1; npts0 = max(npts0, 0);
        nptsX = round( ( Hx/2 ) / minLength ) - 1; nptsX = max(nptsX, 0);
        nptsY = round( Hy / minLength ) - 1; nptsY = max(nptsY, 0);
    else
        npts0 = 0; nptsX = 0; nptsY = 0;
    end

    points0 = minLength*(npts0:-1:1)';
    pointsX = minLength*(1:nptsX)';
    pointsY = minLength*(1:nptsY)';

    nodeCounter = 0;

    %
    % Bottom row, first element
    %
    xmin = 0; xmax = Hx;
    ymin = 0; ymax = Hy;

    nodesFromKoch = flake_points;
    nodesFromKoch(:,1) = nodesFromKoch(:,1) * (xmax - xmin) * scalingFactor ...
                         + xmin + (xmax - xmin) * (1-scalingFactor)/2;
    nodesFromKoch(:,2) = nodesFromKoch(:,2) * (ymax - ymin) * scalingFactor ...
                         + ymin + (ymax - ymin) * (1-scalingFactor)/2;

    nodesFromFrame = [Hx/2*ones(npts0,1)   points0;
                      Hx/2  0;
                      Hx/2+pointsX       zeros(nptsX,1);
                      Hx    0;
                      Hx*ones(nptsY,1)   pointsY;
                      Hx   Hy;
                      Hx/2+pointsX(end:-1:1)   Hy*ones(nptsX,1);
                      Hx/2 Hy;
                      pointsX(end:-1:1)   Hy*ones(nptsX,1);
                      0    Hy;
                      zeros(nptsY,1)   pointsY(end:-1:1);
                      0,    0;
                      pointsX       zeros(nptsX,1)];

    numFrame = size(nodesFromFrame, 1);

    nodes{1,1}  = [nodesFromKoch; nodesFromFrame];

    Element{1} = 1:numKoch;
    Element{2} = [ numKoch:-1:yminIdx ...
                   numKoch+(1:numFrame) ...
                   numKoch+npts0+1 ...
                   numKoch+(npts0:-1:1) ...
                   yminIdx:-1:1];

    % South
    sideDofs{1,1,1} = [numKoch+numFrame-nptsX+(1:nptsX) numKoch+npts0+(1:2+nptsX)];

    % North
    sideDofs{1,1,2} = numKoch+npts0+(3+nptsX+nptsY:5+3*nptsX+nptsY);

    % West
    sideDofs{1,1,3} = size(nodes{1,1},1) - nptsX - 1 - nptsY + (1:nptsY);

    % East
    sideDofs{1,1,4} = numKoch + npts0 + nptsX + 2 + (1:nptsY);

    % Update node counter
    nodeCounter = nodeCounter + size(nodes{1,1},1);

    %
    % Bottom row, remaining elements
    %

    for is = 2:Nx
        xmin = Hx * (is-1); xmax = Hx * is;
        ymin = 0; ymax = Hy;

        nodesFromKoch = flake_points;
        nodesFromKoch(:,1) = nodesFromKoch(:,1) * (xmax - xmin) * scalingFactor ...
                             + xmin + (xmax - xmin) * (1-scalingFactor)/2;
        nodesFromKoch(:,2) = nodesFromKoch(:,2) * (ymax - ymin) * scalingFactor ...
                             + ymin + (ymax - ymin) * (1-scalingFactor)/2;

        nodesFromFrame = [(xmin+Hx/2)*ones(npts0,1)   points0+ymin;
                          xmin+Hx/2 ymin;
                          (xmin+Hx/2)+pointsX   ymin*ones(nptsX,1);
                          xmax      ymin;
                          xmax*ones(nptsY,1)   ymin+pointsY;
                          xmax      ymax;
                          (xmin+Hx/2)+pointsX(end:-1:1)   ymax*ones(nptsX,1);
                          xmin+Hx/2 ymax;
                          xmin+pointsX(end:-1:1)   ymax*ones(nptsX,1);
                          xmin+pointsX   ymin*ones(nptsX,1)];

        numFrame = size(nodesFromFrame, 1);

        nodes{is,1} = [ nodesFromKoch; nodesFromFrame ];

        Element{2*is-1} = nodeCounter + (1:numKoch);
        Element{2*is}   = [ nodeCounter+(numKoch:-1:yminIdx) ...
                            nodeCounter+numKoch+(1:numFrame-nptsX) ...
                            sideDofs{is-1,1,2}(1) ...
                            sideDofs{is-1,1,4}(end:-1:1) ...
                            sideDofs{is-1,1,1}(end) ...
                            nodeCounter+numKoch+numFrame-nptsX+(1:nptsX) ...
                            nodeCounter+numKoch+1+npts0 ...
                            nodeCounter+numKoch+(npts0:-1:1) ...
                            nodeCounter+(yminIdx:-1:1) ];

        % South
        sideDofs{is,1,1} = [ sideDofs{is-1,1,1}(end) ...
                             nodeCounter+numKoch+numFrame-nptsX+(1:nptsX) ...
                             nodeCounter+numKoch+npts0+(1:2+nptsX) ];

        % North
        sideDofs{is,1,2} = [ nodeCounter+numKoch+npts0+(3+nptsX+nptsY:4+3*nptsX+nptsY) ...
                             sideDofs{is-1,1,2}(1) ];

        % West
        sideDofs{is,1,3} = sideDofs{is-1,1,4}(end:-1:1);

        % East
        sideDofs{is,1,4} = nodeCounter + numKoch + npts0 + nptsX + 2 + (1:nptsY);

        nodeCounter = nodeCounter + size(nodes{is,1},1);
    end

    %
    % Remaining rows
    %

    for js = 2:Ny

        % First element
        xmin = 0; xmax = Hx;
        ymin = Hy * (js-1); ymax = Hy * js;

        nodesFromKoch = flake_points;
        nodesFromKoch(:,1) = nodesFromKoch(:,1) * (xmax - xmin) * scalingFactor ...
                             + xmin + (xmax - xmin) * (1-scalingFactor)/2;
        nodesFromKoch(:,2) = nodesFromKoch(:,2) * (ymax - ymin) * scalingFactor ...
                             + ymin + (ymax - ymin) * (1-scalingFactor)/2;

        nodesFromFrame = [ (xmin+Hx/2)*ones(npts0,1)   points0+ymin;
                           xmax*ones(nptsY,1)   ymin+pointsY;
                           xmax      ymax;
                           (xmin+Hx/2)+pointsX(end:-1:1)   ymax*ones(nptsX,1);
                           xmin+Hx/2 ymax;
                           xmin+pointsX(end:-1:1)   ymax*ones(nptsX,1);
                           xmin      ymax;
                           xmin*ones(nptsY,1)   ymin+pointsY(end:-1:1) ];

        numFrame = size(nodesFromFrame, 1);

        nodes{1,js} = [ nodesFromKoch; nodesFromFrame ];

        Element{2*Nx*(js-1)+1} = nodeCounter + (1:numKoch);
        Element{2*Nx*(js-1)+2} = [ nodeCounter+(numKoch:-1:yminIdx) ...
                                   nodeCounter+numKoch+(1:npts0) ...
                                   sideDofs{1,js-1,2}(2+nptsX:-1:1) ...
                                   nodeCounter+numKoch+(npts0+1:numFrame) ...
                                   sideDofs{1,js-1,2}(end:-1:2+nptsX) ...
                                   nodeCounter+numKoch+(npts0:-1:1) ...
                                   nodeCounter+(yminIdx:-1:1) ];

        % South
        sideDofs{1,js,1} = sideDofs{1,js-1,2}(end:-1:1);

        % North
        sideDofs{1,js,2} = nodeCounter+numKoch+npts0+nptsY+(1:3+2*nptsX);

        % West
        sideDofs{1,js,3} = nodeCounter+numKoch+npts0+nptsY+3+2*nptsX+(1:nptsY);

        % East
        sideDofs{1,js,4} = nodeCounter+numKoch+npts0+(1:nptsY);

        nodeCounter = nodeCounter + size(nodes{1,js},1);

        % Remaining elements
        for is = 2:Nx
            xmin = Hx * (is-1); xmax = Hx * is;
            ymin = Hy * (js-1); ymax = Hy * js;

            nodesFromKoch = flake_points;
            nodesFromKoch(:,1) = nodesFromKoch(:,1) * (xmax - xmin) * scalingFactor ...
                                 + xmin + (xmax - xmin) * (1-scalingFactor)/2;
            nodesFromKoch(:,2) = nodesFromKoch(:,2) * (ymax - ymin) * scalingFactor ...
                                 + ymin + (ymax - ymin) * (1-scalingFactor)/2;

            nodesFromFrame = [ (xmin+Hx/2)*ones(npts0,1)   points0+ymin;
                               xmax*ones(nptsY,1)   ymin+pointsY;
                               xmax ymax;
                               (xmin+Hx/2)+pointsX(end:-1:1)   ymax*ones(nptsX,1);
                               xmin+Hx/2 ymax;
                               xmin+pointsX(end:-1:1)   ymax*ones(nptsX,1) ];

            numFrame = size(nodesFromFrame, 1);

            nodes{is, js} = [ nodesFromKoch; nodesFromFrame ];

            Element{2*Nx*(js-1) + 2*is - 1} = nodeCounter + (1:numKoch);
            Element{2*Nx*(js-1) + 2*is}     = [ nodeCounter+(numKoch:-1:yminIdx) ...
                                                nodeCounter+numKoch+(1:npts0) ...
                                                sideDofs{is,js-1,2}(2+nptsX:-1:1) ...
                                                nodeCounter+numKoch+(npts0+1:numFrame) ...
                                                sideDofs{is-1,js,2}(1) ...
                                                sideDofs{is-1,js,4}(end:-1:1) ...
                                                sideDofs{is,js-1,2}(end:-1:2+nptsX) ...
                                                nodeCounter+numKoch+(npts0:-1:1) ...
                                                nodeCounter+(yminIdx:-1:1) ];

            % South
            sideDofs{is,js,1} = sideDofs{is,js-1,2}(end:-1:1);

            % North
            sideDofs{is,js,2} = [ nodeCounter+numKoch+npts0+nptsY+(1:2+2*nptsX) ...
                                  sideDofs{is-1,js,2}(1) ];

            % West
            sideDofs{is,js,3} = sideDofs{is,js,4}(end:-1:1);

            % East
            sideDofs{is,js,4} = nodeCounter+numKoch+npts0+(1:nptsY);

            nodeCounter = nodeCounter + size(nodes{is,js},1);
        end
    end

    %
    % Save mesh
    %

    mesh.Node    = vertcat( nodes{:} );
    mesh.Element = Element;

    % figure();
    % hold on;
    % for K = 1:size(mesh.Element,1)
    %     element = [ mesh.Node( [ mesh.Element{K} mesh.Element{K}(1) ], 1 ) ...
    %                 mesh.Node( [ mesh.Element{K} mesh.Element{K}(1) ], 2 )];
    %     Nver_loc = size(element,1);
    %     if Nver_loc == numKoch+1
    %         patch(element(:,1), element(:,2), 'g')
    %     else
    %         patch(element(:,1), element(:,2), 'b')
    %     end
    % end
    % 
    % plot(mesh.Node(:,1), mesh.Node(:,2), 'y.');

    % filename = input('Enter mesh filename: ', 's');
    filename = sprintf('koch_iter_%i_%02ix%02i_bndRefined_%i.mat', niter, N, N, refineSides);
    save(filename, 'mesh');

end
