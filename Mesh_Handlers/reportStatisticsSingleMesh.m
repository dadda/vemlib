
addpath ../Quadrature/
addpath ../Quadrature/cpp_modules/
addpath ../Scripts
addpath ../VEM_diffusion

meshRoot = './meshfiles3d/voronoi/cube';

meshNames = {
    'unitcube_voro_v02_Nelt000032';
    'unitcube_voro_v02_Nelt000064';
    'unitcube_voro_v02_Nelt000128';
    'unitcube_voro_v02_Nelt000256';
    'unitcube_voro_v02_Nelt000512';
    'unitcube_voro_v02_Nelt001024';
    'unitcube_voro_v02_Nelt002048';
    'unitcube_voro_v02_Nelt004096';
    'unitcube_voro_v02_Nelt008192';
};

meshExt = '.mat';

for i = 1:length(meshNames)

    meshname = [ meshRoot '/' meshNames{i} meshExt ];

    fprintf('Loading mesh %s ... ', meshname);
    load(meshname)
    fprintf('done\n');
    
    [ hmax, hmin, inradii, incenters, gammastar, check ] = computeStatisticsSingleMesh( mesh );

    [ Hmax, iHmax ] = max(hmax);
    [ Hmin, iHmin ] = min(hmin);

    [ gamma0, igamma0 ]       = min( inradii ./ hmax );
    [ gamma1, igamma1 ]       = min( hmin ./ hmax );
    [ GammaStar, iGammaStar ] = min( gammastar );

    [ minhmax, iminhmax ] = min(hmax);
    alpha0 = minhmax / Hmax;

    fprintf('& %e & %e & %e\n', Hmax, Hmin, GammaStar);
    fprintf('Check: %s\n', num2str(min(check,[],1)));

%     fprintf('Maximum element diameter: %1.2e\n', Hmax);
%     fprintf('Minimum element vertices pairwise distance: %1.2e\n', Hmin);
%     fprintf('gamma0 = min( inradii ./ hmax ): %1.2e\n', gamma0);
%     fprintf('gamma1 = min( hmin ./ hmax ): %1.2e\n', gamma1);
%     fprintf('alpha0 = min( hmax ) / max( hmax ): %1.2e\n', alpha0);

%     %
%     % Global plots
%     %
% 
%     % Hmax
%     figure(1)
%     axis equal;
%     view(45,45);
%     set(gcf,'color',[1 1 1]);
% 
%     % Hmin
%     figure(2)
%     axis equal;
%     view(45,45);
%     set(gcf,'color',[1 1 1]);
% 
%     % gamma0
%     figure(3)
%     axis equal;
%     view(45,45);
%     set(gcf,'color',[1 1 1]);
% 
%     % gamma1
%     figure(4)
%     axis equal;
%     view(45,45);
%     set(gcf,'color',[1 1 1]);
% 
%     % alpha0
%     figure(5)
%     axis equal;
%     view(45,45);
%     set(gcf,'color',[1 1 1]);
% 
%     for K = 1:mesh.Nelt
%         element = construct_element_connectivity(mesh, K);
%         Nfc = numel(element);
% 
%         for f = 1:Nfc
%             figure(1)
%             if K == iHmax
%                 patch( mesh.coordinates( [element{f} element{f}(1)], 1), ...
%                        mesh.coordinates( [element{f} element{f}(1)], 2), ...
%                        mesh.coordinates( [element{f} element{f}(1)], 3), 'b' );
%             else
%                 patch( mesh.coordinates( [element{f} element{f}(1)], 1), ...
%                        mesh.coordinates( [element{f} element{f}(1)], 2), ...
%                        mesh.coordinates( [element{f} element{f}(1)], 3), ...
%                        'w', 'EdgeAlpha', 0.2, 'FaceAlpha', 0 );
%             end
% 
%             figure(2)
%             if K == iHmin
%                 patch( mesh.coordinates( [element{f} element{f}(1)], 1), ...
%                        mesh.coordinates( [element{f} element{f}(1)], 2), ...
%                        mesh.coordinates( [element{f} element{f}(1)], 3), 'b' );
%             else
%                 patch( mesh.coordinates( [element{f} element{f}(1)], 1), ...
%                        mesh.coordinates( [element{f} element{f}(1)], 2), ...
%                        mesh.coordinates( [element{f} element{f}(1)], 3), ...
%                        'w', 'EdgeAlpha', 0.2, 'FaceAlpha', 0 );
%             end
% 
%             figure(3)
%             if K == igamma0
%                 patch( mesh.coordinates( [element{f} element{f}(1)], 1), ...
%                        mesh.coordinates( [element{f} element{f}(1)], 2), ...
%                        mesh.coordinates( [element{f} element{f}(1)], 3), 'b' );
%             else
%                 patch( mesh.coordinates( [element{f} element{f}(1)], 1), ...
%                        mesh.coordinates( [element{f} element{f}(1)], 2), ...
%                        mesh.coordinates( [element{f} element{f}(1)], 3), ...
%                        'w', 'EdgeAlpha', 0.2, 'FaceAlpha', 0 );
%             end
% 
%             figure(4)
%             if K == igamma1
%                 patch( mesh.coordinates( [element{f} element{f}(1)], 1), ...
%                        mesh.coordinates( [element{f} element{f}(1)], 2), ...
%                        mesh.coordinates( [element{f} element{f}(1)], 3), 'b' );
%             else
%                 patch( mesh.coordinates( [element{f} element{f}(1)], 1), ...
%                        mesh.coordinates( [element{f} element{f}(1)], 2), ...
%                        mesh.coordinates( [element{f} element{f}(1)], 3), ...
%                        'w', 'EdgeAlpha', 0.2, 'FaceAlpha', 0 );
%             end
% 
%             figure(5)
%             if K == iminhmax
%                 patch( mesh.coordinates( [element{f} element{f}(1)], 1), ...
%                        mesh.coordinates( [element{f} element{f}(1)], 2), ...
%                        mesh.coordinates( [element{f} element{f}(1)], 3), 'c' );
%             elseif K == iHmax
%                 patch( mesh.coordinates( [element{f} element{f}(1)], 1), ...
%                        mesh.coordinates( [element{f} element{f}(1)], 2), ...
%                        mesh.coordinates( [element{f} element{f}(1)], 3), 'b' );
%             else
%                 patch( mesh.coordinates( [element{f} element{f}(1)], 1), ...
%                        mesh.coordinates( [element{f} element{f}(1)], 2), ...
%                        mesh.coordinates( [element{f} element{f}(1)], 3), ...
%                        'w', 'EdgeAlpha', 0.2, 'FaceAlpha', 0 );
%             end
%         end
%     end
% 
% 
%     %
%     % Local plots
%     %
% 
% 
%     % Hmax
%     figure(6)
%     axis equal;
%     set(gcf,'color',[1 1 1]);
% 
%     element = construct_element_connectivity(mesh, iHmax);
%     Nfc     = numel(element);
%     for f = 1:Nfc
%         patch( mesh.coordinates( [element{f} element{f}(1)], 1), ...
%                mesh.coordinates( [element{f} element{f}(1)], 2), ...
%                mesh.coordinates( [element{f} element{f}(1)], 3), ...
%                'b', 'EdgeAlpha', 0.3, 'FaceAlpha', 0.3 );
%     end
% 
%     ivertices = unique( horzcat( element{:}) );
%     Nver      = length(ivertices);
% 
%     Z = squareform( pdist( mesh.coordinates( ivertices, : ) ) );
%     [~, tmp] = max(Z(:));
%     node1 = ceil( tmp/Nver );
%     node2 = tmp - (node1-1)*Nver;
% 
%     hold on;
%     plot3( mesh.coordinates( ivertices( [ node1 node2 ] ), 1 ), ...
%            mesh.coordinates( ivertices( [ node1 node2 ] ), 2 ), ...
%            mesh.coordinates( ivertices( [ node1 node2 ] ), 3 ), ...
%            'b', 'LineWidth', 2 )
%     hold off;
% 
% 
%     % Hmin
%     figure(7)
%     axis equal;
%     set(gcf,'color',[1 1 1]);
% 
%     element = construct_element_connectivity(mesh, iHmin);
%     Nfc     = numel(element);
%     for f = 1:Nfc
%         patch( mesh.coordinates( [element{f} element{f}(1)], 1), ...
%                mesh.coordinates( [element{f} element{f}(1)], 2), ...
%                mesh.coordinates( [element{f} element{f}(1)], 3), ...
%                'b' );
%     end
% 
%     ivertices = unique( horzcat( element{:}) );
%     Nver      = length(ivertices);
% 
%     Z = squareform( pdist( mesh.coordinates( ivertices, : ) ) );
%     Z((1:Nver)*(Nver+1) - Nver) = Inf;
%     [~, tmp] = min(Z(:));
%     node1 = ceil( tmp/Nver );
%     node2 = tmp - (node1-1)*Nver;
% 
%     hold on;
%     plot3( mesh.coordinates( ivertices( [ node1 node2 ] ), 1 ), ...
%            mesh.coordinates( ivertices( [ node1 node2 ] ), 2 ), ...
%            mesh.coordinates( ivertices( [ node1 node2 ] ), 3 ), ...
%            'y-x', 'LineWidth', 2, 'Markersize', 20 )
%     hold off;
% 
% 
%     % gamma0
%     figure(8)
%     axis equal;
%     set(gcf,'color',[1 1 1]);
% 
%     element = construct_element_connectivity(mesh, igamma0);
%     Nfc     = numel(element);
%     for f = 1:Nfc
%         patch( mesh.coordinates( [element{f} element{f}(1)], 1), ...
%                mesh.coordinates( [element{f} element{f}(1)], 2), ...
%                mesh.coordinates( [element{f} element{f}(1)], 3), ...
%                'b', 'EdgeAlpha', 0.5, 'FaceAlpha', 0.3 );
%     end
% 
%     ivertices = unique( horzcat( element{:}) );
%     Nver      = length(ivertices);
% 
%     Z = squareform( pdist( mesh.coordinates( ivertices, : ) ) );
%     [~, tmp] = max(Z(:));
%     node1 = ceil( tmp/Nver );
%     node2 = tmp - (node1-1)*Nver;
% 
%     hold on;
%     plot3( mesh.coordinates( ivertices( [ node1 node2 ] ), 1 ), ...
%            mesh.coordinates( ivertices( [ node1 node2 ] ), 2 ), ...
%            mesh.coordinates( ivertices( [ node1 node2 ] ), 3 ), ...
%            'b', 'LineWidth', 2 )
% 
%     funX = @(x,y) ( inradii(igamma0)*sin(x).*cos(y) + incenters(igamma0,1) );
%     funY = @(x,y) ( inradii(igamma0)*sin(x).*sin(y) + incenters(igamma0,2) );
%     funZ = @(x,y) ( inradii(igamma0)*cos(x) + incenters(igamma0,3) );
%     ezsurf( funX, funY, funZ, [0,pi,0,2*pi] );
%     title('');
% 
%     hold off;
% 
% 
%     % gamma1
%     figure(9)
%     axis equal;
%     set(gcf,'color',[1 1 1]);
% 
%     element = construct_element_connectivity(mesh, igamma1);
%     Nfc     = numel(element);
%     for f = 1:Nfc
%         patch( mesh.coordinates( [element{f} element{f}(1)], 1), ...
%                mesh.coordinates( [element{f} element{f}(1)], 2), ...
%                mesh.coordinates( [element{f} element{f}(1)], 3), ...
%                'b', 'EdgeAlpha', 0.5, 'FaceAlpha', 0.3 );
%     end
% 
%     ivertices = unique( horzcat( element{:}) );
%     Nver      = length(ivertices);
% 
%     Z = squareform( pdist( mesh.coordinates( ivertices, : ) ) );
%     [~, tmp] = max(Z(:));
%     node1 = ceil( tmp/Nver );
%     node2 = tmp - (node1-1)*Nver;
% 
%     hold on;
%     plot3( mesh.coordinates( ivertices( [ node1 node2 ] ), 1 ), ...
%            mesh.coordinates( ivertices( [ node1 node2 ] ), 2 ), ...
%            mesh.coordinates( ivertices( [ node1 node2 ] ), 3 ), ...
%            'b', 'LineWidth', 2 )
%     hold off;
% 
%     Z((1:Nver)*(Nver+1) - Nver) = Inf;
%     [~, tmp] = min(Z(:));
%     node1 = ceil( tmp/Nver );
%     node2 = tmp - (node1-1)*Nver;
% 
%     hold on;
%     plot3( mesh.coordinates( ivertices( [ node1 node2 ] ), 1 ), ...
%            mesh.coordinates( ivertices( [ node1 node2 ] ), 2 ), ...
%            mesh.coordinates( ivertices( [ node1 node2 ] ), 3 ), ...
%            'y-x', 'LineWidth', 2, 'Markersize', 20 )
%     hold off;
% 
% 
%     % alpha0
%     figure(10)
%     axis equal;
%     set(gcf,'color',[1 1 1]);
% 
%     colors = {'b', 'c'};
% 
%     elementIdx = [iHmax iminhmax];
%     for E = 1:2
%         element = construct_element_connectivity(mesh, elementIdx(E));
%         Nfc     = numel(element);
%         for f = 1:Nfc
%             patch( mesh.coordinates( [element{f} element{f}(1)], 1), ...
%                    mesh.coordinates( [element{f} element{f}(1)], 2), ...
%                    mesh.coordinates( [element{f} element{f}(1)], 3), ...
%                    colors{E}, 'EdgeAlpha', 0.3, 'FaceAlpha', 0.3 );
%         end
% 
%         ivertices = unique( horzcat( element{:}) );
%         Nver      = length(ivertices);
% 
%         Z = squareform( pdist( mesh.coordinates( ivertices, : ) ) );
%         [~, tmp] = max(Z(:));
%         node1 = ceil( tmp/Nver );
%         node2 = tmp - (node1-1)*Nver;
% 
%         hold on;
%         plot3( mesh.coordinates( ivertices( [ node1 node2 ] ), 1 ), ...
%                mesh.coordinates( ivertices( [ node1 node2 ] ), 2 ), ...
%                mesh.coordinates( ivertices( [ node1 node2 ] ), 3 ), ...
%                'b', 'LineWidth', 2 )
%         hold off;
%     end
    
end
