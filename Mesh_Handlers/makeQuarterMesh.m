function points = makeQuarterMesh(filename, Vmax)

Lmax = sqrt( 4 * Vmax / sqrt(3) );

L      = Lmax; %Lmax/2;
dtheta = asin(L/2);

Rin  = 1;
Rout = 2;


Npt   = ceil(pi/(2*dtheta));
theta = (1:Npt-2) * dtheta;
theta = theta(:);

% Create file
fid = fopen(filename, 'w');

%
% Node part
%

points = [ Rin, 0;
           Rout, 0;
           Rout * cos(theta), Rout * sin(theta);
           0, Rout;
           0, Rin;
           Rin * cos(pi/2-theta), Rin * sin(pi/2-theta) ];

Npt = size(points, 1);

fprintf(fid, '%i 2 0 0\n', Npt);
for i = 1:Npt
    fprintf(fid, '%i %.16f %.16f\n', i, points(i,1), points(i,2));
end

% Boundary segments
fprintf(fid, '%i 0\n', Npt);
for i = 1:Npt
    fprintf(fid, '%i %i %i\n', i, i, mod(i,Npt)+1);
end

% No holes
fprintf(fid, '0\n');

fclose(fid);

end
