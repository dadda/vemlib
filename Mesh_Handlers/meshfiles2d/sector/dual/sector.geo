// {(x,y) | x >= 1 && x^2+y^2 <= 16}
lc1 = 0.1;

Rout = 4;
Rin  = 1;
ybar = Sqrt(Rout^2 - Rin^2);

Point(1) = {Rin, -ybar, 0.0, lc1};
Point(2) = {Rin, ybar, 0.0, lc1};
Point(3) = {0.0, 0.0, 0.0, lc1};

Circle(1) = {1,3,2};
Line(2)   = {2, 1};

Line Loop(1)     = {1,2};
Plane Surface(1) = {1};
