function []=meshplot(mesh)
% ---------------
% ---------------
figure()
hold on
P = mesh.coordinates;
E = mesh.edges;
for i = 1:size(E,1)
    x = P(E(i,1:2),1);
    y = P(E(i,1:2),2);
    if E(i,3) == 1
       plot(x,y,'r')
    else
       plot(x,y,'b')
    end
end
% % --- option for plotting boundary nodes -----
% V = mesh.B;
% for k = 1:length(V)
%     plot(P(V(k),1) , P(V(k),2) , 'o')
% end
