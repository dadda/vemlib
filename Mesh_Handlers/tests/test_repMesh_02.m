%
% Test repMesh
%

addpath ../
addpath ../../Quadrature
addpath ../../VEM_diffusion
addpath ../../FETI3D

load ../meshfiles3d/voronoi/cube/unitcube_voro_Nelt512.mat

Nx = input('Number of subdomains in x: ');
Ny = input('Number of subdomains in y: ');
Nz = input('Number of subdomains in z: ');

k = 1;

[ refVertexDofs, refEdgeInternalDofs, refFaceInternalDofs, ...
  refInternalDofs, refBoundaryDofs ] = createPreliminaryDofPartition( mesh, k );

% Group element faces by interface face
refElementFacesByBoundary = groupFacesByBoundary( mesh );

% Replicate reference mesh to create a domain decomposition
meshArray = repMesh( mesh, Nx, Ny, Nz, ...
                     refVertexDofs, refEdgeInternalDofs, refFaceInternalDofs, ...
                     refElementFacesByBoundary );

figure()
hold on; axis equal; view(45, 15)
xlim([0,1]); ylim([0,1]); zlim([0,1]);

for k = 1:Nz
    for j = 1:Ny
        for i = 1:Nx
            mesh = meshArray{i,j,k};
            if mod(i,2)
                plot3( mesh.coordinates(vertcat(mesh.faceInternalDofs{[1,2]}),1), ...
                       mesh.coordinates(vertcat(mesh.faceInternalDofs{[1,2]}),2), ...
                       mesh.coordinates(vertcat(mesh.faceInternalDofs{[1,2]}),3), 'b*' )
            else
                plot3( mesh.coordinates(vertcat(mesh.faceInternalDofs{[1,2]}),1), ...
                       mesh.coordinates(vertcat(mesh.faceInternalDofs{[1,2]}),2), ...
                       mesh.coordinates(vertcat(mesh.faceInternalDofs{[1,2]}),3), 'r*' )
            end
            pause
        end
    end
end



