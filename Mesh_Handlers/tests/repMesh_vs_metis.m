
addpath ../
addpath ../../Quadrature
addpath ../../VEM_diffusion
addpath ../../FETI3D
addpath /home/daniele/Downloads/altmany-export_fig-5be2ca4/

load ../meshfiles3d/voronoi/cube/unitcube_voro_v02_Nelt000128.mat
%load ../meshfiles3d/voronoi/cube/unitcube_voro_Nelt8.mat

Nx = input('Number of subdomains in x: ');
Ny = input('Number of subdomains in y: ');
Nz = input('Number of subdomains in z: ');

[ refVertexDofs, refEdgeInternalDofs, refFaceInternalDofs, ...
  refInternalDofs ] = createPreliminaryDofPartition( mesh, 1 );

% Group element faces by interface face
refElementFacesByBoundary = groupFacesByBoundary( mesh );

meshArray = repMesh( mesh, Nx, Ny, Nz, ...
                     refVertexDofs, refEdgeInternalDofs, refFaceInternalDofs, ...
                     refElementFacesByBoundary );

numSubdomains = Nx * Ny * Nz;
CM = jet(numSubdomains);

figure()

% Plot regular partition
s = 0;
for ks = 1:Nz
    for js = 1:Ny
        for is = 1:Nx
            s = s + 1;
            
            % Plot a subdomain
            for E = 1:meshArray{is,js,ks}.Nelt
                element = construct_element_connectivity(meshArray{is,js,ks}, E);
                Nfc_E = numel(element);
                for f = 1:Nfc_E
                    patch( meshArray{is,js,ks}.coordinates( [element{f} element{f}(1)], 1), ...
                           meshArray{is,js,ks}.coordinates( [element{f} element{f}(1)], 2), ...
                           meshArray{is,js,ks}.coordinates( [element{f} element{f}(1)], 3), ...
                           CM(s, :) );
                end
            end

        end
    end
end

view(45,45)
set(gcf,'color',[1 1 1]);
savefig('domDecRegular3D');
export_fig('domDecRegular3D', '-pdf', '-transparent')

% Collect all the subdomain meshes into a single OVM mesh in order to run
% metis or another mesh partitioning algorithm

tolerance = 1e-12;

Nver = 0;
Nelt = 0;
for s = 1:numSubdomains
    Nver = Nver + meshArray{s}.Nver;
    Nelt = Nelt + meshArray{s}.Nelt;
end

gatheredNodes = nan(Nver,3);
gatheredNodes(1:meshArray{1}.Nver,:) = meshArray{1}.coordinates;
nodeCounter = meshArray{1}.Nver;

nodeFlags = cell(numSubdomains,1);
nodeFlags{1} = ( 1:nodeCounter )';

for s = 2:numSubdomains
    nodeFlags{s} = zeros( meshArray{s}.Nver, 1 );
    for l = 1:meshArray{s}.Nver
        D = pdist2(gatheredNodes(1:nodeCounter,:), meshArray{s}.coordinates(l,:));
        test = find(D < tolerance);
        if isempty(test)
            gatheredNodes(nodeCounter+1,:) = meshArray{s}.coordinates(l,:);
            nodeFlags{s}(l) = nodeCounter+1;
            nodeCounter = nodeCounter + 1;
        else
            if length(test) > 1
                warning('More than two neighboring nodes')
            end
            nodeFlags{s}(l) = test;
        end
    end
end

% Write METIS file

fid = fopen('repMeshForMetis.txt', 'w');
fprintf(fid, '%d\n', Nelt);

for s = 1:numSubdomains
    for K = 1:meshArray{s}.Nelt
        element = construct_element_connectivity(meshArray{s}, K);
        uniqueNodesLocal = unique( horzcat( element{:} ) );
        uniqueNodesGlobal = nodeFlags{s}(uniqueNodesLocal);
        fprintf(fid, '%d', uniqueNodesGlobal(1));
        fprintf(fid, ' %d', uniqueNodesGlobal(2:end));
        fprintf(fid, '\n');
    end
end

fclose(fid);

% Run metis
eval(sprintf('!/home/daniele/Documents/codes/metis-5.1.0/build/Linux-x86_64/programs/mpmetis repMeshForMetis.txt %d', numSubdomains));

% Open METIs output file

fid = fopen( sprintf('repMeshForMetis.txt.epart.%d', numSubdomains) );

figure()

for s = 1:numSubdomains
    % Plot a subdomain
    for K = 1:meshArray{s}.Nelt
        element = construct_element_connectivity(meshArray{s}, K);
        Nfc_E = numel(element);
        
        % Read partition the element belongs to
        myNewSub = fscanf(fid, '%d', 1) + 1;
        
        for f = 1:Nfc_E
            patch( meshArray{s}.coordinates( [element{f} element{f}(1)], 1), ...
                   meshArray{s}.coordinates( [element{f} element{f}(1)], 2), ...
                   meshArray{s}.coordinates( [element{f} element{f}(1)], 3), ...
                   CM(myNewSub, :) );
        end
    end
end

fclose(fid);
view(45,45);
set(gcf,'color',[1 1 1]);
savefig('domDec3D');
export_fig('domDec3D', '-pdf', '-transparent')
