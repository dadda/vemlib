%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COmpare the new baric function and the original one

addpath('./../Mesh_Handlers');
addpath('./../Quadrature');
addpath('./../VEM_diffusion');
addpath('./vem_matlab_original/VEM_diffusion');

k = input('Polynomial degree (>= 1): ');
eoa = input('Extra order of accuracy for quadrature formulas: ');

meshroot = './../Mesh_Handlers/meshfiles_AleRusso';
%meshname = 'esagoni-regolari/esagoni8x10';
meshname = 'esagoni-deformati/esagoni70x80d';
%meshname = 'triangoli-triangle/square0.01';
%meshname = 'boffi/boffi-64';
%meshname = 'hanging-nodes/hole';
%meshname = 'poligono/centagono';
%meshname = 'poligono/quadrato';
%meshname = 'poligono/stella10';
%meshname = 'poligono/esagono-irregolare';
meshname = [meshroot '/' meshname];

fprintf('Loading mesh %s...\n', meshname);
mesh = loadmesh(meshname, 0);
fprintf('...mesh %s loaded\n', meshname);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute areas and baricenters of elements with original code

tic;
areas = zeros(1, mesh.Nelt);
xb    = areas;
yb    = areas;

for i=1:mesh.Nelt
    m = mesh.elements(i,1);                           % number of vertexes of element 
    X = mesh.coordinates(mesh.elements(i,2:m+1),1:2); % build X with vertex coordinates
    [E, bb] = baric_original(X);
    areas(i) = E;
    xb(i) = bb(1);
    yb(i) = bb(2);
end 
toc;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute areas and baricenters of elements with new code

tic;
mesh = build_coordinate_matrices_sequential( mesh, k, eoa, 0 );
toc;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Print errors

fprintf('Inf norm of difference for areas: %5.15e\n', norm(areas-mesh.areas, inf));
[xmaxerr, xargmaxerr] = max(abs(xb-mesh.xb));
fprintf('Inf norm of difference for xb: %5.15e. Corresponding element: %d\n', xmaxerr, xargmaxerr);
[ymaxerr, yargmaxerr] = max(abs(yb-mesh.yb));
fprintf('Inf norm of difference for yb: %5.15e. Corresponding element: %d\n', ymaxerr, yargmaxerr);

meshplot(mesh);
hold on
plot(xb([xargmaxerr,yargmaxerr]),yb([xargmaxerr,yargmaxerr]),'bo')
plot(mesh.xb([xargmaxerr,yargmaxerr]),mesh.yb([xargmaxerr,yargmaxerr]),'ro')
