% Test algorithms to find outer normals for polygons in 3D

% Create "crazy" non convex polygon in the x-y plane
x = [1 5 5 4 2 2 3 3 1];
y = [1 1 3 2 2 4 3 5 5];
z = zeros(size(x));

figure()
plot3(x,y,z)

% Rotate the polygon of alpha radians in the y-z plane
alpha = 5*pi/4;
R = [1 0 0;
    0 cos(alpha) -sin(alpha);
    0 sin(alpha) cos(alpha)];

newcoords = R*[x;y;z];

figure()
plot3(newcoords(1,:), newcoords(2,:), newcoords(3,:))

v1 = [newcoords(1,2:end-1) - newcoords(1,1);
      newcoords(2,2:end-1) - newcoords(2,1);
      newcoords(3,2:end-1) - newcoords(3,1)];
v2 = [newcoords(1,3:end) - newcoords(1,1);
      newcoords(2,3:end) - newcoords(2,1);
      newcoords(3,3:end) - newcoords(3,1)];

a = sum([v1(2,:).*v2(3,:) - v1(3,:).*v2(2,:);
         v1(3,:).*v2(1,:) - v1(1,:).*v2(3,:);
         v1(1,:).*v2(2,:) - v1(2,:).*v2(1,:)],2);
