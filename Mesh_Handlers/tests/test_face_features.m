% Test face_features function with a ONE POLYHEDRON mesh
%
% If you change in Voro++ the way the statistics are printed, please update
% the second part of the script.

addpath '../'
addpath '../../Quadrature'
addpath '../../VEM_diffusion'

% Test meshes generated with Voro++
meshfile = '../meshfiles3d/one_polyhedron/degenerate3.mat';
meshfile_statistics = '../meshfiles3d/one_polyhedron/degenerate3_statistics.txt';

load(meshfile)

faces = construct_element_connectivity(mesh,1);

% for f = 1:mesh.Nfc
%     % Topological face index and face orientation
% 	tface = floor( ( mesh.element2hfaces{1}(f)+1 )/2 );
%     face_orientation = mod(mesh.element2hfaces{1}(f), 2);
%     
%     % Topological edge index and edge orientation
%     tedge = floor( ( mesh.face2hedges{tface}+1 )/2 );
%     edge_orientation = mod(mesh.face2hedges{tface}, 2);
% 
%     edges = mesh.edge2vertices(tedge,:)';
%     
%     % Define face by providing the indices of the bounding vertices in the
%     % order specified by its half face and half edges.
%     % If the current half face is odd, then its half
%     % edges are given by mesh.face2hedges{tface}. Otherwise, half
%     % face = opp(mesh.face2hedges{tface}) = opp of each single half
%     % edge mesh.face2hedges{tface}. To find the opp half edges, we
%     % have to check whether mesh.face2hedges{tface} are odd or even.
%     % In the first case, opp(half edge) = half edge + 1, whereas in the
%     % second case, opp(half edge) = half edge - 1.
%     
%     faces{f} = face_orientation * (edge_orientation .* edges(1,:) + (1-edge_orientation).*edges(2,:)) + ...
%                (1-face_orientation) * ((1-edge_orientation(end:-1:1)) .* edges(1,end:-1:1) + edge_orientation(end:-1:1).*edges(2,end:-1:1));
% end

[~, ~, faces_normals, ~, ~, ~, faces_areas] = ...
    faces_features(mesh.coordinates, faces, 1, 0, 0, 0);

fid   = fopen(meshfile_statistics);
tline = fgetl(fid);
Nfc   = str2double(tline);
if Nfc ~= mesh.Nfc
    error('Something wrong with either the .mat or the .txt file')
end

% Check areas vs Voro++
areas = zeros(Nfc,1);
for f = 1:Nfc
    areas(f) = str2double(fgetl(fid));
end

tmp = faces_areas-areas;
fprintf('\nMaximum relative difference between faces areas computed with MATLAB and Voro++: %g\n', norm(tmp,inf)/norm(areas,inf));

% Check normals vs Voro++
normals = zeros(Nfc,3);
for f = 1:Nfc
    normals(f,:) = str2num( fgetl(fid) ); %#ok<*ST2NM>
    normals(f,:) = normals(f,:)/norm(normals(f,:));
end

tmp = faces_normals - normals;
fprintf('\nMaximum relative difference between faces normals computed with MATLAB and Voro++: %g\n', norm(tmp,inf)/norm(faces_normals));

fclose(fid);
