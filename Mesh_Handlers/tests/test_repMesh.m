%
% Test repMesh
%

addpath ../
addpath ../../Quadrature
addpath ../../VEM_diffusion
addpath ../../FETI3D

%load ../meshfiles3d/one_polyhedron/cube.mat
load ../meshfiles3d/voronoi/cube/unitcube_voro_Nelt8.mat

Nx = input('Number of subdomains in x: ');
Ny = input('Number of subdomains in y: ');
Nz = input('Number of subdomains in z: ');

[ refVertexDofs, refEdgeInternalDofs, refFaceInternalDofs, ...
  refInternalDofs, refBoundaryDofs ] = createPreliminaryDofPartition( mesh, k );

% Group element faces by interface face
refElementFacesByBoundary = groupFacesByBoundary( mesh );

% Replicate reference mesh to create a domain decomposition
meshArray = repMesh( mesh, Nx, Ny, Nz, ...
                     refVertexDofs, refEdgeInternalDofs, refFaceInternalDofs, ...
                     refElementFacesByBoundary );

figure()
axis equal
hold on

for k = 1:Nz
    for j = 1:Ny
        for i = 1:Nx
            mesh = meshArray{i,j,k};
                        
            for K = 1:mesh.Nelt
                % Draw outer normals scaled by face areas for each element
                % of the mesh
                element = construct_element_connectivity(mesh, K);
                
                % Draw approximated element centroid to identify the
                % current element being processed
                
                unique_vertices = unique(horzcat(element{:}));
                barycenter = sum( mesh.coordinates(unique_vertices, :), 1 )/length(unique_vertices);
                plot3(barycenter(1), barycenter(2), barycenter(3), 'r*', 'markersize', 10)
                
                [~, ~, faces_normals, faces_algdist, ...
                 ~, ~, faces_areas, faces_centroids2D, ...
                 collapsed_variable] = ...
                    faces_features(mesh.coordinates, element, 1, 0, 0, 0);
                
                for f = 1:length(element)
                    faceCentroid3D = zeros(1,3);
                    survived_variables = setdiff(1:3, collapsed_variable(f));
                    faceCentroid3D(survived_variables) = faces_centroids2D(f,:);
                    faceCentroid3D(collapsed_variable(f)) = faces_algdist(f);

                    % Assemble rotation matrix
                    if ( abs(faces_normals(f,3)) < 0.5 )
                        % Rotate towards e_z
                        ct    = faces_normals(f,3);
                        omega = [faces_normals(f,2) -faces_normals(f,1) 0];
                    else
                        % Rotate towards e_x
                        ct    = faces_normals(f,1);
                        omega = [0 faces_normals(f,3) -faces_normals(f,2)];
                    end
                    %st = norm(omega);    
                    st = sqrt(1.0 - ct^2);
                    omega = omega / st;

                    R(1,1) = ct + omega(1)^2*(1-ct);
                    R(2,2) = ct + omega(2)^2*(1-ct);
                    R(3,3) = ct + omega(3)^2*(1-ct);

                    R(1,2) = -omega(3)*st + omega(1)*omega(2)*(1-ct);
                    R(1,3) = omega(2)*st + omega(1)*omega(3)*(1-ct);
                    R(2,1) = omega(3)*st + omega(1)*omega(2)*(1-ct);
                    R(2,3) = -omega(1)*st + omega(2)*omega(3)*(1-ct);
                    R(3,1) = -omega(2)*st + omega(1)*omega(3)*(1-ct);
                    R(3,2) = omega(1)*st + omega(2)*omega(3)*(1-ct);

                    % Invert rotation by using R'
                    faceCentroid3D = R'*faceCentroid3D';
                    
                    % Scale the outward normal
                    faces_normals(f,:) = faces_normals(f,:) * faces_areas(f) / 2;

                    % Plot face
                    plot3(mesh.coordinates([element{f} element{f}(1)],1), ...
                          mesh.coordinates([element{f} element{f}(1)],2), ...
                          mesh.coordinates([element{f} element{f}(1)],3), ...
                          'b')
                    
                    quiver3(faceCentroid3D(1), faceCentroid3D(2), faceCentroid3D(3), ...
                            faces_normals(f,1), faces_normals(f,2), faces_normals(f,3), ...
                            'Linewidth', 2, 'MaxHeadSize', 8)
                end
                
                pause
            end
        end
    end
end
