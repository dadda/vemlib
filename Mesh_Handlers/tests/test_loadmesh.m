%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check and time old and new versions of loadmesh

addpath('./../Mesh_Handlers');

fprintf('\nTesting two different functions for loading and creating mesh data structure\n\n');

use_mp = input('Use multiprecision toolbox (0/1)? ');
if use_mp
    addpath('./../AdvanpixMCT-4.0.0.11272');
    precision = input('Set default precision: ');
    mp.Digits(precision);
end

meshroot = './../Mesh_Handlers/meshfiles_AleRusso';
% meshname = 'quadrati-regolari/quadrati1x1';
meshname = 'esagoni-deformati/esagoni70x80d';
% meshname = 'poligoni-random/poligoni10000';
meshname = [meshroot '/' meshname];

fprintf('\nChosen mesh: %s\n', meshname);

fprintf('\nNew version:\n')
tic;
mesh1 = loadmesh(meshname, use_mp);
toc;

fprintf('\nOld version:\n')
tic;
mesh2 = loadmesh_old(meshname, use_mp);
toc;

% Check edges and edgebyele fields
error1 = 0;
for E = 1:mesh1.Nelt
    error1 = error1 + norm(sort(sort(mesh1.edges(mesh1.edgebyele(E,1:mesh1.elements(E,1)),1:2),2),1) - ...
        sort(sort(mesh2.edges(mesh2.edgebyele(E,1:mesh2.elements(E,1)),1:2),2),1), inf);
end

% Check edges and dirfaces fields
error2 = norm(sort(sort(mesh1.edges(mesh1.dirfaces,1:2),2),1) - ...
              sort(sort(mesh2.edges(mesh2.dirfaces,1:2),2),1),inf);
          
if error1 == 0 && error2 == 0
    fprintf('\nThe two versions are consistent.\n')
end
