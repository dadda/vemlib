
def writeOVMmesh( node, edges, f2he, p2hf, filename ):
    fid = open( filename, 'w' )
    fid.write('OVM ASCII\n')

    Nver = node.shape[0]
    fid.write('Vertices\n')
    fid.write('%i\n' % Nver)
    for i in range(Nver):
        fid.write('%.16f %.16f %.16f\n' % tuple( node[i,:].tolist() ) )

    Ned = edges.shape[0]
    fid.write('Edges\n')
    fid.write('%i\n' % Ned)
    for i in range(Ned):
        fid.write('%i %i\n' % tuple( edges[i,:].tolist() ) )

    Nfc = len(f2he)
    fid.write('Faces\n')
    fid.write('%i\n' % Nfc)
    for i in range(Nfc):
        NedLoc = len(f2he[i])
        row    = [NedLoc] + f2he[i].tolist()
        rowStr = [ str(j) for j in row ]
        rowStr = ' '.join(rowStr)
        fid.write('%s\n' % rowStr )

    Npol = len(p2hf)
    fid.write('Polyhedra\n')
    fid.write('%i\n' % Npol)
    for i in range(Npol):
        NfcLoc = len(p2hf[i])
        row    = [NfcLoc] + p2hf[i].tolist()
        rowStr = [ str(j) for j in row ]
        rowStr = ' '.join(rowStr)
        fid.write('%s\n' % rowStr )

    fid.close()
