function ovmMesh = geom3d2ovm( geom3dMesh )

ovmMesh.Nelt = 1;

ovmMesh.coordinates = geom3dMesh.vertices;
ovmMesh.Nver        = size(ovmMesh.coordinates,1);

ovmMesh.edge2vertices = sort(geom3dMesh.edges, 2);
ovmMesh.Ned           = size(ovmMesh.edge2vertices, 1);

ovmMesh.Nfc = length(geom3dMesh.faces);

ovmMesh.face2elements = -ones(ovmMesh.Nfc,2);
ovmMesh.face2elements(:,1) = 1;

ovmMesh.element2hfaces = zeros(1, ovmMesh.Nfc);
%ovmMesh.element2hfaces(1) = ovmMesh.Nfc;

if ~iscell(geom3dMesh.faces)
    [nr, nc] = size(geom3dMesh.faces);
    geom3dMesh.faces = mat2cell(geom3dMesh.faces, ones(nr,1), nc);
end

for fc = 1:ovmMesh.Nfc
	% Compute barycenter
	barycenter_el = sum(ovmMesh.coordinates,1)/ovmMesh.Nver;

    % Compute normal
	v1 = ovmMesh.coordinates(geom3dMesh.faces{fc}(2),:) - ovmMesh.coordinates(geom3dMesh.faces{fc}(1),:);
	v2 = ovmMesh.coordinates(geom3dMesh.faces{fc}(3),:) - ovmMesh.coordinates(geom3dMesh.faces{fc}(2),:);
    normal = [v1(2)*v2(3) - v1(3)*v2(2), ...
              v1(3)*v2(1) - v1(1)*v2(3), ...
              v1(1)*v2(2) - v1(2)*v2(1)];
    
    % Check on which size the barycenter is to find the correct half-face
    check_test = dot(normal, barycenter_el - ovmMesh.coordinates(geom3dMesh.faces{fc}(1),:));
    if check_test > 0
        % The face is clockwise-oriented. Assign
        % even half-face (slightly different from OpenVolumeMesh)
        ovmMesh.element2hfaces(fc) = 2*fc;
    else        
        % The face is counter-clockwise oriented. Assign
        % odd half-face (slightly different from OpenVolumeMesh)
        ovmMesh.element2hfaces(fc) = 2*fc-1;
    end
end
ovmMesh.element2hfaces = mat2cell(ovmMesh.element2hfaces, 1, ovmMesh.Nfc);

ovmMesh.face2hedges = cell(ovmMesh.Nfc,1);
for fc = 1:ovmMesh.Nfc
    Nver_loc = length(geom3dMesh.faces{fc});
    ovmMesh.face2hedges{fc} = zeros(1,Nver_loc);
    % ovmMesh.face2hedges{fc}(1) = Nver_loc; % Same as number of edges
    nextfc = [2:Nver_loc 1];
    for j = 1:Nver_loc
        sorted_edge = sort(geom3dMesh.faces{fc}([j nextfc(j)]));
        [~, ~, e] = intersect(sorted_edge, ovmMesh.edge2vertices, 'rows');
        if issorted(geom3dMesh.faces{fc}([j nextfc(j)]))
            ovmMesh.face2hedges{fc}(j) = 2*e-1;
        else
            ovmMesh.face2hedges{fc}(j) = 2*e;
        end
    end
end

end

