function [mesh] = create_hexagonalmesh(nx, ny, bbox)
% Generate exagonal grid

xmin = bbox(1);
ymin = bbox(2);
xmax = bbox(3);
ymax = bbox(4);

% n: number of hexagons along the x direction.

xnodes = linspace(xmin, xmax, 2*nx+1);
ynodes = linspace(ymin, ymax, 3*ny+1);
ynodes(4:3:end) = [];
ynodes = [ynodes ymax];

NV = 3*nx + 3 + ...              % first row of elements
     (2*nx + 1) * (ny - 1) + ... % up to second to last row
     nx + 2 - mod(ny,2);                     % last row contribution. If ny is odd
                                             % the last row contributes one
                                             % node less

NP = length(1:2:ny+1-mod(ny,2)) * (nx+1) + length(2:2:ny+mod(ny,2)) * nx;

mesh = struct('NV', NV, ...
              'NP', NP, ...
              'vertex', struct('x', cell(1,NV), 'y', cell(1,NV)), ...
              'polygon', struct('NV', cell(1,NP), 'vertices', cell(1,NP)) );

% Load first row
mesh.vertex(1).x = xmin;
mesh.vertex(1).y = ymin;

iNV = 1;

for E = 1:nx
    mesh.vertex(1+E).x  = xnodes(2*(E-1)+2);
    mesh.vertex(1+E).y  = ynodes(1);
    
    mesh.vertex(nx+2+2*(E-1)+1).x = xnodes(2*(E-1)+1);
    mesh.vertex(nx+2+2*(E-1)+1).y = ynodes(3);

    mesh.vertex(nx+2+2*(E-1)+2).x = xnodes(2*(E-1)+2);
    mesh.vertex(nx+2+2*(E-1)+2).y = ynodes(2); % it has to be the same as before
    
    mesh.polygon(E).NV = 5;
    mesh.polygon(E).vertices = [E E+1 nx+2+2*(E-1)+2 nx+2+2*(E-1)+1 nx+2+2*(E-2)+2];
    
    iNV = iNV + 3;
end

mesh.vertex(nx+2).x = xnodes(end);
mesh.vertex(nx+2).y = ynodes(1);

mesh.vertex(iNV+2).x = xnodes(end);
mesh.vertex(iNV+2).y = ynodes(3);

mesh.polygon(1).NV    = 4;
mesh.polygon(nx+1).NV = 4;

mesh.polygon(1).vertices = [1 2 nx+4 nx+3];
mesh.polygon(nx+1).vertices = [nx+1 nx+2 iNV+2 iNV+1];

iNV = iNV + 2;
iNP = nx + 1;

for i = 2:ny
    if mod(i,2)
        % Odd row
        for j = 1:nx
            mesh.vertex(iNV+1).x = xnodes(1+2*(j-1));
            mesh.vertex(iNV+1).y = ynodes(1+2*(i-1)+2);

            mesh.vertex(iNV+2).x = xnodes(2+2*(j-1));
            mesh.vertex(iNV+2).y = ynodes(1+2*(i-1)+1);

            mesh.polygon(iNP+j).NV = 6;
            mesh.polygon(iNP+j).vertices = [nx+2+(2*nx+1)*(i-2)+2*(j-1) ...
                                            nx+2+(2*nx+1)*(i-2)+2*(j-1)+1 ...
                                            nx+2+(2*nx+1)*(i-2)+2*(j-1)+2 ...
                                            nx+2+(2*nx+1)*(i-1)+2*(j-1)+2 ...
                                            nx+2+(2*nx+1)*(i-1)+2*(j-1)+1 ...
                                            nx+2+(2*nx+1)*(i-1)+2*(j-1)];
            
            iNV = iNV + 2;
        end
        mesh.vertex(iNV+1).x = xnodes(end);
        mesh.vertex(iNV+1).y = ynodes(1+2*(i-1)+2);        
        
        mesh.polygon(iNP+1).NV = 4;
        mesh.polygon(iNP+nx+1).NV = 4;
        
        mesh.polygon(iNP+1).vertices = [nx+2+(2*nx+1)*(i-2)+1 ...
                                        nx+2+(2*nx+1)*(i-2)+2 ...
                                        nx+2+(2*nx+1)*(i-1)+2 ...
                                        nx+2+(2*nx+1)*(i-1)+1];
        mesh.polygon(iNP+nx+1).vertices = [nx+2+(2*nx+1)*(i-2)+2*nx ...
                                           nx+2+(2*nx+1)*(i-2)+2*nx+1 ...
                                           nx+2+(2*nx+1)*(i-1)+2*nx+1 ...
                                           nx+2+(2*nx+1)*(i-1)+2*nx];
        
        iNV = iNV + 1;
        iNP = iNP + nx + 1;
    else
        % Even row
        for j = 1:nx
            mesh.vertex(iNV+1).x = xnodes(1+2*(j-1));
            mesh.vertex(iNV+1).y = ynodes(1+2*(i-1)+1);

            mesh.vertex(iNV+2).x = xnodes(1+2*(j-1)+1);
            mesh.vertex(iNV+2).y = ynodes(1+2*(i-1)+2);
            
            mesh.polygon(iNP+j).NV = 6;
            mesh.polygon(iNP+j).vertices = [nx+2+(2*nx+1)*(i-2)+2*(j-1)+1 ...
                                            nx+2+(2*nx+1)*(i-2)+2*(j-1)+2 ...
                                            nx+2+(2*nx+1)*(i-2)+2*(j-1)+3 ...
                                            nx+2+(2*nx+1)*(i-1)+2*(j-1)+3 ...
                                            nx+2+(2*nx+1)*(i-1)+2*(j-1)+2 ...
                                            nx+2+(2*nx+1)*(i-1)+2*(j-1)+1];

            iNV = iNV + 2;
        end

        mesh.vertex(iNV+1).x = xnodes(end);
        mesh.vertex(iNV+1).y = ynodes(1+2*(i-1)+1);

        iNV = iNV + 1;
        iNP = iNP + nx;
    end
end

% Now let us take care of the last row

if mod(ny+1,2)
    mesh.vertex(iNV+1).x = xnodes(1);
    mesh.vertex(iNV+1).y = ynodes(end);
    
    mesh.polygon(iNP+1).NV = 4;
    mesh.polygon(iNP+1).vertices = [nx+2+(2*nx+1)*(ny-1)+1 ...
                                    nx+2+(2*nx+1)*(ny-1)+2 ...
                                    iNV+2 ...
                                    iNV+1];

    iNV = iNV + 1;
    iNP = iNP + 1;
    for j = 1:nx
        mesh.vertex(iNV+j).x = xnodes(2+2*(j-1));
        mesh.vertex(iNV+j).y = ynodes(end);
        
        mesh.polygon(iNP+j).NV = 5;
        mesh.polygon(iNP+j).vertices = [nx+2+(2*nx+1)*(ny-1)+2*j ...
                                        nx+2+(2*nx+1)*(ny-1)+2*j+1 ...
                                        nx+2+(2*nx+1)*(ny-1)+2*j+2 ...
                                        iNV+j+1 ...
                                        iNV+j];
    end
    iNV = iNV + nx;
    iNP = iNP + nx;
    mesh.vertex(iNV+1).x = xnodes(end);
    mesh.vertex(iNV+1).y = ynodes(end);
    
    mesh.polygon(iNP).NV = 4;
    mesh.polygon(iNP).vertices = [nx+2+(2*nx+1)*(ny-1)+2*nx ...
                                  nx+2+(2*nx+1)*(ny-1)+2*nx+1 ...
                                  iNV+1 ...
                                  iNV];
    
else

    for j = 1:nx
        mesh.vertex(iNV+j).x = xnodes(1+2*(j-1));
        mesh.vertex(iNV+j).y = ynodes(end);

        mesh.polygon(iNP+j).NV = 5;
        mesh.polygon(iNP+j).vertices = [nx+2+(2*nx+1)*(ny-1)+2*(j-1)+1 ...
                                        nx+2+(2*nx+1)*(ny-1)+2*(j-1)+2 ...
                                        nx+2+(2*nx+1)*(ny-1)+2*(j-1)+3 ...
                                        iNV+j+1 ...
                                        iNV+j];
    end
    iNV = iNV + nx;
    iNP = iNP + nx;

    mesh.vertex(iNV+1).x = xnodes(end);
    mesh.vertex(iNV+1).y = ynodes(end);

    iNV = iNV + 1;
    iNP = iNP + nx;
    
end


end







