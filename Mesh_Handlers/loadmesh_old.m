function newmesh = loadmesh_old(meshname, use_mp)
% function mesh = loadmesh_old(meshname, use_mp)
%
% Loads one of the meshes by A.Russo and create additional geometric
% structures. This version does not create the permutation matrix.
% -----------------------------------------------

load([meshname '.mat'])

%%% Create coordinate matrix mesh.V

newmesh.Nver = mesh.NV;

newmesh.coordinates = [[mesh.vertex.x]' [mesh.vertex.y]'];
if use_mp
    newmesh.coordinates = mp(newmesh.coordinates);
    newmesh.Nelt = mp(mesh.NP);
else
    newmesh.Nelt = mesh.NP;
end

%%% Create elements matrix

[newmesh.Nver_max, newmesh.iNver_max] = max([mesh.polygon.NV]);

newmesh.elements = zeros([newmesh.Nelt newmesh.Nver_max+1]);
newmesh.elements(:,1) = [mesh.polygon.NV]';
for i = 1:newmesh.Nelt
    newmesh.elements(i,2:newmesh.elements(i,1)+1) = mesh.polygon(i).vertices;
end

newmesh.edges = edgefinder(newmesh.elements);
newmesh.Nfc   = size(newmesh.edges,1);
newmesh.B     = bvfinder(newmesh.edges); 
newmesh.B     = newmesh.B(:);

% For the moment, we assume that boundary faces can only be Dirichlet
newmesh.dirfaces = find(newmesh.edges(:,4) == 0);

newmesh.edgebyele = zeros([newmesh.Nelt newmesh.Nver_max]);
M = sparse(newmesh.Nver);
for i = 1:size(newmesh.edges,1)    % build auxiliary matrix M
    M(newmesh.edges(i,1),newmesh.edges(i,2)) = i;
end    
M = M+M';
for i = 1:newmesh.Nelt
    for j = 1:newmesh.elements(i,1)-1
        newmesh.edgebyele(i,j) = M(newmesh.elements(i,j+1),newmesh.elements(i,j+2));
    end
    newmesh.edgebyele(i,newmesh.elements(i,1)) = ...
        M(newmesh.elements(i,newmesh.elements(i,1)+1),newmesh.elements(i,2));
end


end
