function [] = writeGMSHmesh( mesh, filename )

fid = fopen(filename, 'w');

fprintf(fid, '$MeshFormat\n');
fprintf(fid, '2.2 0 8\n');
fprintf(fid, '$EndMeshFormat\n');

%
%
%

fprintf(fid, '$Nodes\n');
fprintf(fid, '%i\n', mesh.Nver);
for i = 1:mesh.Nver
    fprintf(fid, '%i %.16f %.16f 0\n', i, mesh.coordinates(i,:));
end
fprintf(fid, '$EndNodes\n');

%
%
%

Ndiri = length(mesh.dirfaces);

fprintf(fid, '$Elements\n');
fprintf(fid, '%i\n', Ndiri+mesh.Nelt);
for i = 1:Ndiri
    fprintf(fid, '%i 1 2 0 1 %i %i\n', i, mesh.edges(mesh.dirfaces(i),1:2));
end
for i = 1:mesh.Nelt
    fprintf(fid, '%i 2 2 0 1 %i %i %i\n', i+Ndiri, mesh.elements(i,2:4));
end
fprintf(fid, '$EndElements\n');


fclose(fid);

end
