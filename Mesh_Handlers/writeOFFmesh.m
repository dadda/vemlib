function [] = writeOFFmesh( mesh, filename )

Nver = size(mesh.Node, 1);
Nelt = length(mesh.Element);

% Note: indices in OFF meshes start from 0

fid = fopen( filename, 'w' );

% Header
fprintf(fid, 'OFF\n');

% Number of nodes, number of elements, 0
fprintf(fid, '%d %d 0\n', Nver, Nelt);

% Write nodes
for i = 1:Nver
    fprintf(fid, '%.16f %.16f 0\n', mesh.Node(i, 1), mesh.Node(i, 2) );
end

% Write elements
for i = 1:Nelt
    NverLoc = length(mesh.Element{i});
    fprintf( fid, '%s\n', num2str( [ NverLoc reshape(mesh.Element{i}, 1, NverLoc)-1 ] ) );
end

fclose( fid );

end
