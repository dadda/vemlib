function meshArray = repMesh( refMesh, Nx, Ny, Nz, ...
                              refVertexDofs, refEdgeInternalDofs, refFaceInternalDofs, ...
                              refElementFacesByBoundary )

meshArray = cell(Nx, Ny, Nz);

x0 = refMesh.coordinates(:,1);
y0 = refMesh.coordinates(:,2);
z0 = refMesh.coordinates(:,3);

Hx = 1/Nx;
Hy = 1/Ny;
Hz = 1/Nz;

xMapV = [2 1 4 3 6 5 8 7];
xMapE = [2 1 3 4 6 5 8 7 10 9 11 12];
xFlip = [3 4 11 12]; % Edges whose dofs order has to be flipped
xMapF = [2 1 3:6];

yMapV = [3 4 1 2 7 8 5 6];
yMapE = [1 2 4 3 7 8 5 6 9 10 12 11];
yFlip = [1 2 9 10];
yMapF = [1 2 4 3 5 6];

zMapV = [5:8 1:4];
zMapE = [9:12 5:8 1:4];
zFlip = 5:8;
zMapF = [1:4 6 5];

s = 0;

for k = 1:Nz
    for j = 1:Ny
        for i = 1:Nx
            s = s + 1;

            tmpMesh = refMesh;

            x = x0 * Hx * (2*mod(i,2)-1) + Hx * (i-mod(i,2));
            y = y0 * Hy * (2*mod(j,2)-1) + Hy * (j-mod(j,2));
            z = z0 * Hz * (2*mod(k,2)-1) + Hz * (k-mod(k,2));

            tmpMesh.coordinates(:,1) = x;
            tmpMesh.coordinates(:,2) = y;
            tmpMesh.coordinates(:,3) = z;
            
            if ( mod(i+j+k+1,2) )
                % Update tmpMesh.element2hfaces by 
                % going through element2hfaces of refMesh and switch them:
                % odd -> next even
                % even -> previous odd
                for K = 1:refMesh.Nelt
                    tmpMesh.element2hfaces{K} = refMesh.element2hfaces{K} + ...
                        ( 2*mod(refMesh.element2hfaces{K},2) - 1 );
                end
            end

            if s == 1
                tmpMesh.vertexDofs             = refVertexDofs;
                tmpMesh.edgeInternalDofs       = refEdgeInternalDofs;
                tmpMesh.faceInternalDofs       = refFaceInternalDofs;
                tmpMesh.elementFacesByBoundary = refElementFacesByBoundary;
            elseif mod( s-1, Nx*Ny ) == 0 % True if we move one layer up, in the z-direction
                tmpMesh.vertexDofs             = meshArray{i, j, k-1}.vertexDofs(zMapV);
                tmpMesh.edgeInternalDofs       = meshArray{i, j, k-1}.edgeInternalDofs(zMapE);
                for edge = zFlip
                    tmpMesh.edgeInternalDofs{edge} = tmpMesh.edgeInternalDofs{edge}(end:-1:1);
                end
                tmpMesh.faceInternalDofs       = meshArray{i, j, k-1}.faceInternalDofs(zMapF);
                tmpMesh.elementFacesByBoundary = meshArray{i, j, k-1}.elementFacesByBoundary(zMapF);
            elseif mod( s-1, Nx ) == 0    % True if we move one row in the y-direction
                tmpMesh.vertexDofs             = meshArray{i, j-1, k}.vertexDofs(yMapV);
                tmpMesh.edgeInternalDofs       = meshArray{i, j-1, k}.edgeInternalDofs(yMapE);
                for edge = yFlip
                    tmpMesh.edgeInternalDofs{edge} = tmpMesh.edgeInternalDofs{edge}(end:-1:1);
                end
                tmpMesh.faceInternalDofs       = meshArray{i, j-1, k}.faceInternalDofs(yMapF);
                tmpMesh.elementFacesByBoundary = meshArray{i, j-1, k}.elementFacesByBoundary(yMapF);
            else                     % We move in the x-direction
                tmpMesh.vertexDofs             = meshArray{i-1, j, k}.vertexDofs(xMapV);
                tmpMesh.edgeInternalDofs       = meshArray{i-1, j, k}.edgeInternalDofs(xMapE);
                for edge = xFlip
                    tmpMesh.edgeInternalDofs{edge} = tmpMesh.edgeInternalDofs{edge}(end:-1:1);
                end
                tmpMesh.faceInternalDofs       = meshArray{i-1, j, k}.faceInternalDofs(xMapF);
                tmpMesh.elementFacesByBoundary = meshArray{i-1, j, k}.elementFacesByBoundary(xMapF);
            end
            
            meshArray{i,j,k} = tmpMesh;
        end
    end
end

end
