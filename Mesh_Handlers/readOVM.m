function mesh = readOVM( filename )

fid = fopen(filename, 'r');

fgets(fid);
fgets(fid);

mesh.Nver        = fscanf(fid, '%i', 1);
mesh.coordinates = zeros(mesh.Nver, 3);
for i = 1:mesh.Nver
    mesh.coordinates(i,:) = fscanf(fid, '%f', 3);
end

fgets(fid);
fgets(fid);

mesh.Ned           = fscanf(fid, '%i', 1);
mesh.edge2vertices = zeros(mesh.Ned, 2);

for i = 1:mesh.Ned
    mesh.edge2vertices(i,:) = fscanf(fid, '%i', 2);
    mesh.edge2vertices(i,:) = mesh.edge2vertices(i,:) + 1;
end

fgets(fid);
fgets(fid);

mesh.Nfc         = fscanf(fid, '%i', 1);
mesh.face2hedges = cell(mesh.Nfc, 1);

fgets(fid);
for i = 1:mesh.Nfc
    line = fgets(fid);
    tmp  = str2num(line); %#ok<ST2NM>
    mesh.face2hedges{i} = tmp(2:end) + 1;
end

fgets(fid);
mesh.Nelt           = fscanf(fid, '%i', 1);
mesh.element2hfaces = cell(mesh.Nelt, 1);

fgets(fid);
for i = 1:mesh.Nelt
    line = fgets(fid);
    tmp  = str2num(line); %#ok<ST2NM>
    mesh.element2hfaces{i} = tmp(2:end) + 1;
end

fclose(fid);

end