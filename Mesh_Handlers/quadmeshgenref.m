function [mesh] = quadmeshgenref
% --------------------------------------------------
% Generates a quad mesh on [0,1]^2 with N nodes per side
% ALLOWS for tensor product refined meshes, t be built by defining
% ... vectors X and Y
% As output: mesh in MY format.
% See meshstruct.m for MY format info
%---------------------------------------------------
% vec=linspace(0,1,N);
%
% X=[0:1/(N-1):1];
% Y=[0:1/(N-1):1];



X = [0:1/16:2/16];
X = [X,[2/16+1/64:1/64:6/16]];
X = [X, [7/16:1/16:10/16]];
X = [X, [10/16+1/64:1/64:14/16]];
X = [X, 15/16, 1];

N=length(X);

Y=X;

for i=1:N^2
    P(i,1) = X(mod(i-1,N)+1);   % /(N-1);
    P(i,2) = Y((ceil(i/N)-1)+1); % /(N-1);
end
NN=(N-1)^2;
T=[4*ones(NN,1),zeros(NN,4)];
for k=1:NN
        T(k,2) = k + floor((k-1)/(N-1)) + N + 1;
        T(k,3) = k + floor((k-1)/(N-1)) + N;
        T(k,4) = k + floor((k-1)/(N-1));
        T(k,5) = k + floor((k-1)/(N-1)) + 1;
end    
        
mesh = meshstruct(P,T);

meshplot(mesh)