function [mesh] = meshstruct(P,T);
% INPUT: P,T matrixes in the following form:
% P has on i-th row the x and y of i-th vertex
% T has on i-th row data for i-th Polygon, ordered as:
% ... number of vertexes, then vertex numbers in anti-clockwise sense
% -----------------------------
% OUTPUT: a struct mesh file to be used in VEM.m
% mesh.V has on i-th row the x and y of i-th Vertex
% mesh.P has on i-th row data for i-th Polygon, ordered as:
% ... number of vertexes, then vertex numbers in anti-clockwise sense
% mesh.E has on row i the data for i^{th} edge, ordered as follows: 
% ... vertex number 1 number, vertex number 2 number, 
% ... and then left and right element numbers. 
% ... (NOTE: mesh.E has a zero in the last column instead of 
% ... an element number if this is a boundary edge.)
% mesh.D has matrix with useful data for k^{th} element ...
% ... in position (k,1) area, in position (k,2:3) coordinates of ...
% ... baricenter. ---> REPLACED by mesh.areas, mesh.xb, mesh.yb
% mesh.B vector with boundary node numbers
% mesh.PE has on i-th row the edge numbers in anti-clockwise sense
% ... for the i^{th} polygon, the first edge corresponding to the
% ... first vertex
% ----------------------------
% quad(k).W (quadrature rule) for k=1,...,# elem, on ...
% component (i,1:2) has coordinates of i^{th} quadrature node, ...
% on component (i,3) the associated weight. 
% This is only for LINEAR quad. rule, for better rules use the
% files in the "quadrature" directory
% -----------------------------
% NOTE : mesh.D, mesh.PE and quad are optional and currently not used
% -----------------------------

mesh = struct ('V',P,'P',T);
mesh.E = edgefinder(T);
mesh.B = bvfinder(mesh.E); 

% -----------------------------------------
%     OPTIONAL part for mesh.PE
% -----------------------------------------

% M=sparse(zeros(size(mesh.V,1)));
% for k=1:size(mesh.E,1)    % build auxiliary matrix M
%     M(mesh.E(k,1),mesh.E(k,2))=k;
% end    
% M=M+M';
% for k=1:size(mesh.P,1)
%     for j=1:mesh.P(k,1)-1
%         mesh.PE(k,j)=M(mesh.P(k,j+1),mesh.P(k,j+2));
%     end
%     mesh.PE(k,mesh.P(k,1))=M(mesh.P(k,mesh.P(k,1)+1),mesh.P(k,2));
% end


% -----------------------------------------
% OPTIONAL part for quad and mesh.D
% need also to insert quad in line one among the outputs
% -----------------------------------------
% mm = size(T,1);  % number of elements
% quad = struct;
% for k=1:mm       % cycle on elements
%    m=T(k,1);  % number of vertexes of element 
%    G = P(T(k,2:m+1),1:2);  % local coordinates of vertexes 
%    [E,BB,w]=baric(G);
%    mesh.D(k,1:3)=[E,BB];  
%    quad(k).W = [G,w];   % in this case the nodes are the vertexes 
% end   


