function [ hmin, gamma0, gamma1 ] = computationalIntensiveStatistics( meshArray, Nvect, runAsFor )

numSubdomains = prod( Nvect );

hmin   = zeros(numSubdomains, 1);
gamma0 = zeros(numSubdomains, 1);
gamma1 = zeros(numSubdomains, 1);

if length(Nvect) == 2
    parfor ( s = 1:numSubdomains, getParforArg(runAsFor) )
        [ sHmax, sHmin, sInradii ] = computeStatisticsSingleMesh2d( meshArray{s} );
        
        hmin(s)   = min( sHmin );
        gamma0(s) = min( sInradii ./ sHmax );
        gamma1(s) = min( sHmin ./ sHmax );
    end
end

hmin   = min( hmin );
gamma0 = min( gamma0 );
gamma1 = min( gamma1 );

end
