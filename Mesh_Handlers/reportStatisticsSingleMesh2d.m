
addpath ../Scripts

meshRoot = './meshfiles2d/voronoi-notnested/enumath2017';

meshNames = {
    'randVoro__Nelt_010000__hmin_3e-05__minArea_1e-15__tolTotArea_1e-8__cut32x32';
    'randVoro__Nelt_020000__hmin_2e-05__minArea_1e-15__tolTotArea_1e-8__cut32x32';
    'randVoro__Nelt_040000__hmin_2e-05__minArea_1e-15__tolTotArea_1e-6__cut32x32';
    'randVoro__Nelt_080000__hmin_2e-05__minArea_1e-15__tolTotArea_1e-6__cut32x32';
    'randVoro__Nelt_160000__hmin_2e-05__minArea_1e-07__tolTotArea_1e-7__cut32x32';
    };

meshExt = '.off';

% meshRoot = 'meshfiles2d/koch';

% meshNames = {
%     'koch_iter_3_12x12_bndRefined_0';
%     'koch_iter_3_16x16_bndRefined_0';
%     'koch_iter_3_20x20_bndRefined_0';
%     'koch_iter_3_24x24_bndRefined_0';
%     'koch_iter_3_28x28_bndRefined_0';
%     'koch_iter_3_32x32_bndRefined_0';
%     'koch_iter_3_36x36_bndRefined_0';
%     'koch_iter_3_40x40_bndRefined_0';
%     'koch_iter_3_44x44_bndRefined_0';
%     'koch_iter_3_48x48_bndRefined_0';
%     'koch_iter_3_52x52_bndRefined_0';
%     'koch_iter_3_56x56_bndRefined_0';
%     'koch_iter_3_60x60_bndRefined_0';
%     };

% meshRoot = '/home/daniele/Documents/codes/triangle/meshes/conforming_delaunay';
% 
% meshNames = {
% 	'unitsquare_a1em7.off.metis_nparts_00000064';
% 	'unitsquare_a1em7.off.metis_nparts_00000128';
% 	'unitsquare_a1em7.off.metis_nparts_00000256';
% 	'unitsquare_a1em7.off.metis_nparts_00000512';
% 	'unitsquare_a1em7.off.metis_nparts_00001024';
% 	'unitsquare_a1em7.off.metis_nparts_00002048';
% 	'unitsquare_a1em7.off.metis_nparts_00004096';
% 	'unitsquare_a1em7.off.metis_nparts_00008192';
% 	'unitsquare_a1em7.off.metis_nparts_00016384';
% 	'unitsquare_a1em7.off.metis_nparts_00032768';
% 	'unitsquare_a1em7.off.metis_nparts_00065536';
% 	'unitsquare_a1em7.off.metis_nparts_00131072';
% 	'unitsquare_a1em7.off.metis_nparts_00262144';
% 	'unitsquare_a1em7.off.metis_nparts_00524288';
% 	'unitsquare_a1em7.off.metis_nparts_01048576';
% 	'unitsquare_a1em7.off.metis_nparts_02097152';
%     };
% 
% meshExt = '.mat';

for i = 1:length(meshNames)
    
    meshname = [ meshRoot '/' meshNames{i} meshExt ];

    fprintf('Loading mesh %s ... ', meshname);
    if strcmp(meshExt, '.off')
        mesh = readOFFmesh(meshname);
        mesh = loadmesh(mesh, 0);
    else
        mesh = loadmesh(meshname, 0);
    end
    fprintf('done\n');

    [ hmax, hmin, inradii ] = computeStatisticsSingleMesh2d( mesh );
    [ Hmax, iHmax ] = max(hmax);
    [ Hmin, iHmin ] = min(hmin);

    [ gamma0, igamma0 ] = max( hmax ./ inradii );
    [ gamma1, igamma1 ] = max( hmax ./ hmin );

    [ minhmax, iminhmax ] = min(hmax);
    alpha0 = Hmax / minhmax;

%     fprintf(' & %i & %i & %i & %e & %e & %e & %e\n', ...
%         mesh.Nelt, mesh.Nfc, mesh.Nver, Hmax, Hmin, gamma0, gamma1);
%     fprintf(' & %i & %i & %i & %i & %e & %e & %e & %e\n', ...
%         mesh.Nelt, mesh.Nver, max(mesh.elements(:,1)), min(mesh.elements(:,1)), ...
%         Hmax, Hmin, gamma0, gamma1);
    fprintf(' & %i & %i & %e & %e & %e & %e\n', ...
        mesh.Nelt, mesh.Nver, Hmax, Hmin, gamma0, gamma1);

%     fprintf('Maximum element diameter: %1.2e\n', Hmax);
%     fprintf('Minimum element vertices pairwise distance: %1.2e\n', Hmin);
%     fprintf('gamma0 = max( hmax_K ./ inradii_K ): %1.2e\n', gamma0);
%     fprintf('gamma1 = max( hmax_K ./ hmin_K ): %1.2e\n', gamma1);
%     fprintf('alpha0 = max( hmax_K ) / min( hmax_K ): %1.2e\n', alpha0);

%     %
%     % Global plots
%     %
% 
%     % Hmax
%     figure(1)
%     title('h_{max}')
%     axis equal;
%     set(gcf,'color',[1 1 1]);
% 
%     % Hmin
%     figure(2)
%     title('h_{min}')
%     axis equal;
%     set(gcf,'color',[1 1 1]);
% 
%     % gamma0
%     figure(3)
%     title('\gamma_0 = min_K (\rho_K/h_K)')
%     axis equal;
%     set(gcf,'color',[1 1 1]);
% 
%     % gamma1
%     figure(4)
%     title('\gamma_1 = min_K (h_{K,min} / h_K)')
%     axis equal;
%     set(gcf,'color',[1 1 1]);
% 
%     % alpha0
%     figure(5)
%     title('\alpha_0 = (min_K h_K) / h_{max}')
%     axis equal;
%     set(gcf,'color',[1 1 1]);
% 
%     for K = 1:mesh.Nelt
% 
%         NverLoc  = mesh.elements(K,1);
%         vertices = mesh.coordinates( mesh.elements(K,2:NverLoc+1), : );
% 
%         figure(1)
%         if K == iHmax
%             patch( vertices(:,1), vertices(:,2), 'b' );
%         else
%             patch( vertices(:,1), vertices(:,2), 'w', 'EdgeAlpha', 0.2, 'FaceAlpha', 0 );
%         end
% 
%         figure(2)
%         if K == iHmin
%             patch( vertices(:,1), vertices(:,2), 'b' );
%         else
%             patch( vertices(:,1), vertices(:,2), 'w', 'EdgeAlpha', 0.2, 'FaceAlpha', 0 );
%         end
% 
%         figure(3)
%         if K == igamma0
%             patch( vertices(:,1), vertices(:,2), 'b' );
%         else
%             patch( vertices(:,1), vertices(:,2), 'w', 'EdgeAlpha', 0.2, 'FaceAlpha', 0 );
%         end
% 
%         figure(4)
%         if K == igamma1
%             patch( vertices(:,1), vertices(:,2), 'b' );
%         else
%             patch( vertices(:,1), vertices(:,2), 'w', 'EdgeAlpha', 0.2, 'FaceAlpha', 0 );
%         end
% 
%         figure(5)
%         if K == iminhmax
%             patch( vertices(:,1), vertices(:,2), 'c' );
%         elseif K == iHmax
%             patch( vertices(:,1), vertices(:,2), 'b' );
%         else
%             patch( vertices(:,1), vertices(:,2), 'w', 'EdgeAlpha', 0.2, 'FaceAlpha', 0 );
%         end
%     end
% 
% 
%     %
%     % Local plots
%     %
% 
%     % Hmax
%     figure(6)
%     title('h_{max}')
%     axis equal;
%     set(gcf,'color',[1 1 1]);
% 
%     NverLoc  = mesh.elements(iHmax,1);
%     vertices = mesh.coordinates( mesh.elements(iHmax,2:NverLoc+1), : );
% 
%     patch( vertices(:,1), vertices(:,2), 'b', 'EdgeAlpha', 0.3, 'FaceAlpha', 0.3 );
% 
%     Z = squareform( pdist( vertices ) );
%     [~, tmp] = max(Z(:));
%     node1 = ceil( tmp/NverLoc );
%     node2 = tmp - (node1-1)*NverLoc;
% 
%     hold on;
%     plot( vertices( [ node1 node2 ] , 1 ), ...
%           vertices( [ node1 node2 ] , 2 ), ...
%           'b', 'LineWidth', 2 )
%     hold off;
% 
% 
%     % Hmin
%     figure(7)
%     title('h_{min}')
%     axis equal;
%     set(gcf,'color',[1 1 1]);
% 
%     NverLoc  = mesh.elements(iHmin,1);
%     vertices = mesh.coordinates( mesh.elements(iHmin,2:NverLoc+1), : );
% 
%     patch( vertices(:,1), vertices(:,2), 'b', 'EdgeAlpha', 0.3, 'FaceAlpha', 0.3 );
% 
%     Z = squareform( pdist( vertices ) );
%     Z((1:NverLoc)*(NverLoc+1) - NverLoc) = Inf;
%     [~, tmp] = min(Z(:));
%     node1 = ceil( tmp/NverLoc );
%     node2 = tmp - (node1-1)*NverLoc;
% 
%     hold on;
%     plot( vertices( [ node1 node2 ] , 1 ), ...
%           vertices( [ node1 node2 ] , 2 ), ...
%            'r-x', 'LineWidth', 2, 'Markersize', 10 )
%     hold off;
% 
% 
%     % gamma0
%     figure(8)
%     axis equal;
%     set(gcf,'color',[1 1 1]);
% 
%     NverLoc  = mesh.elements(igamma0,1);
%     vertices = mesh.coordinates( mesh.elements(igamma0,2:NverLoc+1), : );
% 
%     patch( vertices(:,1), vertices(:,2), 'b', 'EdgeAlpha', 0.5, 'FaceAlpha', 0.3 );
% 
%     Z = squareform( pdist( vertices ) );
%     [~, tmp] = max(Z(:));
%     node1 = ceil( tmp/NverLoc );
%     node2 = tmp - (node1-1)*NverLoc;
% 
%     hold on;
%     plot( vertices( [ node1 node2 ] , 1 ), ...
%           vertices( [ node1 node2 ] , 2 ), ...
%           'b', 'LineWidth', 2 )
% 
%     funX = @(theta) ( inradii(igamma0)*cos(theta) + incenters(igamma0,1) );
%     funY = @(theta) ( inradii(igamma0)*sin(theta) + incenters(igamma0,2) );
%     h = ezplot( funX, funY, [0,2*pi] );
%     set(h, 'LineWidth', 2);
%     title('\gamma_0 = max_K (h_K/\rho_K)')
% 
%     hold off;
% 
% 
%     % gamma1
%     figure(9)
%     axis equal;
%     set(gcf,'color',[1 1 1]);
% 
%     NverLoc  = mesh.elements(igamma1,1);
%     vertices = mesh.coordinates( mesh.elements(igamma1,2:NverLoc+1), : );
% 
%     patch( vertices(:,1), vertices(:,2), 'b', 'EdgeAlpha', 0.5, 'FaceAlpha', 0.3 );
% 
%     Z = squareform( pdist( vertices ) );
%     [~, tmp] = max(Z(:));
%     node1 = ceil( tmp/NverLoc );
%     node2 = tmp - (node1-1)*NverLoc;
% 
%     hold on;
%     plot( vertices( [ node1 node2 ] , 1 ), ...
%           vertices( [ node1 node2 ] , 2 ), ...
%           'b', 'LineWidth', 2 )
% 
%     Z((1:NverLoc)*(NverLoc+1) - NverLoc) = Inf;
%     [~, tmp] = min(Z(:));
%     node1 = ceil( tmp/NverLoc );
%     node2 = tmp - (node1-1)*NverLoc;
% 
%     plot( vertices( [ node1 node2 ] , 1 ), ...
%           vertices( [ node1 node2 ] , 2 ), ...
%            'r-x', 'LineWidth', 2, 'Markersize', 10 )
% 
%     title('\gamma_1 = max_K (h_K/h_{K,min})')
%     hold off;
% 
% 
%     % alpha0
%     figure(10)
%     title('\alpha_0 = h_{max} / (min_K h_K)')
%     axis equal;
%     set(gcf,'color',[1 1 1]);
% 
%     colors = {'b', 'c'};
% 
%     elementIdx = [iHmax iminhmax];
%     for E = 1:2
%         NverLoc  = mesh.elements(elementIdx(E),1);
%         vertices = mesh.coordinates( mesh.elements(elementIdx(E),2:NverLoc+1), : );
% 
%         patch( vertices(:,1), vertices(:,2), colors{E}, 'EdgeAlpha', 0.3, 'FaceAlpha', 0.3 );
% 
%         Z = squareform( pdist( vertices ) );
%         [~, tmp] = max(Z(:));
%         node1 = ceil( tmp/NverLoc );
%         node2 = tmp - (node1-1)*NverLoc;
% 
%         hold on;
%         plot( vertices( [ node1 node2 ] , 1 ), ...
%               vertices( [ node1 node2 ] , 2 ), ...
%               'b', 'LineWidth', 2 )
%         hold off;
%     end

end
