function flake_points = koch_snowflake(iterations)
% Nodes of the Koch snowflake circumscribed by the unit square in
% couterclockwise order

len = 1;           % Original side length of triangle
theta = pi/3;      % Angle of the equilateral triangle in radians

% Points of the original equilateral triangle
p1 = [0,0];
p2 = [1/2*len,len*sin(theta)];
p3 = [len,0];

flake_points = [p1; p2; p3]; % Vertices of the snowflake

for iteration = 1:iterations
    n_points = size(flake_points,1); % No. points currently in data
    new_triangle_points = nan(n_points*3,2); % New points to be added
    for i = 1:n_points
        p = flake_points(i,:);
        if i == n_points
            vector = flake_points(1,:)-flake_points(i,:);
        else
            % Vector between next point and current point
            vector = flake_points(i+1,:)-flake_points(i,:);
        end
        b1 = p + 1/3*vector; % Base point 1 of the new triangle
        b2 = p + 2/3*vector; % Base point 2 of the new triangle
        % top = p + 1/2*vector + perp(vector)*len/3*sin(theta);
        top = p + 1/2*vector + [-vector(2) vector(1)]/3*sin(theta);
        new_triangle_points(3*i-2:3*i,:) = [b1; top; b2];
    end
    flake_points(end+1:end+9,:) = nan(9,2); % Add rows to data
    new_flake_points = nan(n_points*3+n_points,2);
    n = 1;
    for i = 1:n_points
        new_flake_points(n,:) = flake_points(i,:);
        index = 3*i-2;
        new_flake_points(n+1:n+3,:) = new_triangle_points(index:index+2,:);
        n = n + 4; % Three new points have been inserted,
                    % so we now skip to the next empty spaces
    end
    flake_points = new_flake_points;
    len = 1/3*len; % Revise length
end

% Rescale y-coordinates to [0,1]
ymin = min(flake_points(:,2));
ymax = max(flake_points(:,2));

flake_points(:,2) = ( flake_points(:,2) - ymin ) / ( ymax - ymin );

flake_points = flake_points(end:-1:1,:);

% plot(flake_points(:,1),flake_points(:,2),'b')
% hold on; % Plot last data point with 1st (ie, close the loop)
% plot([flake_points(1,1),flake_points(end,1)],...
%     [flake_points(1,2),flake_points(end,2)],'w')
% set(gca,'color',[0.1 0.1 0.1])
% axis equal;
% str = sprintf('Iteration: %d',iteration);
% title(str, 'FontSize', 20);

end
