import numpy as np

#
# Read a polyhedral mesh in either pmgo or OVM format
#

def readPMGOmesh( basename ):
    node  = np.loadtxt( basename + '.node', dtype='float64', skiprows=1, usecols=(0,1,2) )
    edges = np.loadtxt( basename + '.edge', dtype='int64', skiprows=1, usecols=(0,1) )

    fid   = open( basename + '.face', 'r' )
    lines = fid.readlines()[1:]
    fid.close()

    Nfc  = len(lines)
    f2he = [0] * Nfc
    f2p  = np.zeros([Nfc,2])
    for i,line in enumerate(lines):
        tmp = line.split()
        NedLoc   = np.int64( tmp[0] )
        f2he[i]  = np.int64( [ j for j in tmp[1:(NedLoc+1)] ] )
        f2p[i,:] = [ tmp[NedLoc+1], tmp[NedLoc+2] ]
        
    fid = open( basename + '.polyhedron', 'r' )
    lines = fid.readlines()[1:]
    fid.close()

    p2hf = [ np.int64( [ i for i in line.split()[1:] ] ) for line in lines ]

    return node, edges, f2he, p2hf


def readOVMmesh( filename ):
    fid   = open( filename, 'r' )
    lines = fid.readlines()[1:]
    fid.close()

    Nver = np.int64( lines[1].strip() )
    node = np.zeros([Nver,3])
    for i in range(Nver):
        line      = lines[2+i].strip()
        node[i,:] = np.float64( line.split() )

    Ned   = np.int64( lines[2 + Nver + 1].strip() )
    edges = np.zeros( [Ned,2], dtype='int64' )
    for i in range(Ned):
        line       = lines[2 + Nver + 2 + i].strip()
        edges[i,:] = np.int64( line.split() )

    Nfc  = np.int64( lines[2 + Nver + 2 + Ned + 1] )
    f2he = [0] * Nfc
    for i in range(Nfc):
        line    = lines[2 + Nver + 2 + Ned + 2 + i].strip()
        f2he[i] = np.int64( line.split()[1:] )

    Npol = np.int64( lines[2 + Nver + 2 + Ned + 2 + Nfc + 1] )
    p2hf = [0] * Npol
    for i in range(Npol):
        line    = lines[2 + Nver + 2 + Ned + 2 + Nfc + 2 + i].strip()
        p2hf[i] = np.int64( line.split()[1:] )
        
    return node, edges, f2he, p2hf
