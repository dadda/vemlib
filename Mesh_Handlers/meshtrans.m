
% -------------------------------------------
% Loads one of the meshes by A.Russo and
% ... transforms it into MY format (output "mesh").
% See meshstruct.m file for description of MY format.
% -------------------------------------------

% clear all
% close all
%
addpath(['./meshfiles_AleRusso']);
%addpath(['./meshes']);
% addpath(['../../Elasticity/mesh_handlers']);
%
%%% MESHROOT (do not change) %%%
%
meshroot = './meshfiles_AleRusso';
%
%%% SCELGO LA MESH %%%

%meshname = 'poligono/triangolo';
%meshname = 'poligono/triangle';
meshname = 'poligono/decagono';

% "ROMBI"

%meshname = 'rombo/rombo-1';
%meshname = 'rombo/rombo-2';
%meshname = 'rombo/rombo-5';
%meshname = 'rombo/rombo-10';
%meshname = 'rombo/rombo-20';

% QUADRATI CHE COLLASSANO

%meshname = 'collapsing-squares/quad-0';
%meshname = 'collapsing-squares/quad-50';
%meshname = 'collapsing-squares/quad-60';
%meshname = 'collapsing-squares/quad-70';
%meshname = 'collapsing-squares/quad-80';
%meshname = 'collapsing-squares/quad-90';
%meshname = 'collapsing-squares/quad-99';
%meshname = 'collapsing-squares/quad-100';

%meshname = 'collapsing-squares-d/quad-0d';
%meshname = 'collapsing-squares-d/quad-50d';
%meshname = 'collapsing-squares-d/quad-60d';
%meshname = 'collapsing-squares-d/quad-70d';
%meshname = 'collapsing-squares-d/quad-80d';
%meshname = 'collapsing-squares-d/quad-90d';
%meshname = 'collapsing-squares-d/quad-99d';
%meshname = 'collapsing-squares-d/quad-100d';

% ESAGONI (QUASI) REGOLARI

%meshname = 'esagoni-regolari/esagoni8x10';
%meshname = 'esagoni-regolari/esagoni18x20';
%meshname = 'esagoni-regolari/esagoni26x30';
%meshname = 'esagoni-regolari/esagoni34x40';
%meshname = 'esagoni-regolari/esagoni44x50';
%meshname = 'esagoni-regolari/esagoni52x60';
%meshname = 'esagoni-regolari/esagoni60x70';
%meshname = 'esagoni-regolari/esagoni70x80';

% ESAGONI DEFORMATI

%meshname = 'esagoni-deformati/esagoni8x10d';
%meshname = 'esagoni-deformati/esagoni18x20d';
%meshname = 'esagoni-deformati/esagoni26x30d';
%meshname = 'esagoni-deformati/esagoni34x40d';
%meshname = 'esagoni-deformati/esagoni44x50d';
%meshname = 'esagoni-deformati/esagoni52x60d';
%meshname = 'esagoni-deformati/esagoni60x70d';
%meshname = 'esagoni-deformati/esagoni70x80d';

% POLIGONI DI VORONOI CON CONTROL POINTS RANDOM

%meshname = 'poligoni-random/poligoni10';
%meshname = 'poligoni-random/poligoni100';
%meshname = 'poligoni-random/poligoni200';
%meshname = 'poligoni-random/poligoni300';
%meshname = 'poligoni-random/poligoni400';
%meshname = 'poligoni-random/poligoni500';
%meshname = 'poligoni-random/poligoni600';
%meshname = 'poligoni-random/poligoni700';
%meshname = 'poligoni-random/poligoni800';
%meshname = 'poligoni-random/poligoni900';
%meshname = 'poligoni-random/poligoni1000';
%meshname = 'poligoni-random/poligoni1200';
%meshname = 'poligoni-random/poligoni1400';
%meshname = 'poligoni-random/poligoni2000';
%meshname = 'poligoni-random/poligoni3000';
%meshname = 'poligoni-random/poligoni4000';
%meshname = 'poligoni-random/poligoni5000';
%meshname = 'poligoni-random/poligoni10000';
%meshname = 'poligoni-random/poligoni20000';


% POLIGONI DI VORONOI CON CONTROL POINTS GENERATI DA TRIANGLE

%meshname = 'poligoni-triangle/triangle0.01';
%meshname = 'poligoni-triangle/triangle0.005';
%meshname = 'poligoni-triangle/triangle0.001';
%meshname = 'poligoni-triangle/triangle0.0005';
%meshname = 'poligoni-triangle/triangle0.0001';

% QUADRATI REGOLARI

%meshname = 'quadrati-regolari/quadrati1x1r';
%meshname = 'quadrati-regolari/quadrati1x1';
%meshname = 'quadrati-regolari/quadrati2x2';
%meshname = 'quadrati-regolari/quadrati4x4';
%meshname = 'quadrati-regolari/quadrati8x8';
%meshname = 'quadrati-regolari/quadrati10x10';
%meshname = 'quadrati-regolari/quadrati16x16';
%meshname = 'quadrati-regolari/quadrati20x20';
%meshname = 'quadrati-regolari/quadrati30x30';
%meshname = 'quadrati-regolari/quadrati32x32';
%meshname = 'quadrati-regolari/quadrati40x40';
%meshname = 'quadrati-regolari/quadrati50x50';
%meshname = 'quadrati-regolari/quadrati64x64';
%meshname = 'quadrati-regolari/quadrati80x80';
%meshname = 'quadrati-regolari/quadrati100x100';
%meshname = 'quadrati-regolari/quadrati160x160';

% QUADRATI DISTORTI

%meshname = 'quadrati-distorti-0.1/quadrati-distorti-0.1-10x10';
%meshname = 'quadrati-distorti-0.1/quadrati-distorti-0.1-20x20';
%meshname = 'quadrati-distorti-0.1/quadrati-distorti-0.1-40x40';

%meshname = 'quadrati-distorti-0.2/quadrati-distorti-0.2-10x10';
%meshname = 'quadrati-distorti-0.2/quadrati-distorti-0.2-20x20';
%meshname = 'quadrati-distorti-0.2/quadrati-distorti-0.2-40x40';

%meshname = 'quadrati-distorti-0.3/quadrati-distorti-0.3-10x10';
%meshname = 'quadrati-distorti-0.3/quadrati-distorti-0.3-20x20';
%meshname = 'quadrati-distorti-0.3/quadrati-distorti-0.3-40x40';

%meshname = 'quadrati-distorti-0.4/quadrati-distorti-0.4-10x10';
%meshname = 'quadrati-distorti-0.4/quadrati-distorti-0.4-20x20';
%meshname = 'quadrati-distorti-0.4/quadrati-distorti-0.4-40x40';

%meshname = 'quadrati-distorti-0.5/quadrati-distorti-0.5-10x10';
%meshname = 'quadrati-distorti-0.5/quadrati-distorti-0.5-20x20';
%meshname = 'quadrati-distorti-0.5/quadrati-distorti-0.5-40x40';

% QUADRATI CHE COLLASSANO

%meshname = 'collapsing-squares/quad-0';
%meshname = 'collapsing-squares/quad-50';
%meshname = 'collapsing-squares/quad-60';
%meshname = 'collapsing-squares/quad-70';
%meshname = 'collapsing-squares/quad-80';
%meshname = 'collapsing-squares/quad-90';
%meshname = 'collapsing-squares/quad-99';
%meshname = 'collapsing-squares/quad-100';

%meshname = 'collapsing-squares-d/quad-0d';
%meshname = 'collapsing-squares-d/quad-50d';
%meshname = 'collapsing-squares-d/quad-60d';
%meshname = 'collapsing-squares-d/quad-70d';
%meshname = 'collapsing-squares-d/quad-80d';
%meshname = 'collapsing-squares-d/quad-90d';
%meshname = 'collapsing-squares-d/quad-99d';
%meshname = 'collapsing-squares-d/quad-100d';


% TRIANGOLI (GENERATI DA TRIANGLE)

%meshname = 'triangoli-triangle/square0.01';
%meshname = 'triangoli-triangle/square0.005';
%meshname = 'triangoli-triangle/square0.001';
%meshname = 'triangoli-triangle/square0.0005';
%meshname = 'triangoli-triangle/square0.0001';
%meshname = 'triangoli-triangle/square0.00005';
%meshname = 'triangoli-triangle/square0.00001';

% TRIANGOLI REGOLARI

%meshname = 'triangoli-regolari/quadrato5';
%meshname = 'triangoli-regolari/quadrato10';
%meshname = 'triangoli-regolari/quadrato15';
%meshname = 'triangoli-regolari/quadrato17';
%meshname = 'triangoli-regolari/quadrato20';
%meshname = 'triangoli-regolari/quadrato25';
%meshname = 'triangoli-regolari/quadrato30';
%meshname = 'triangoli-regolari/quadrato40';
%meshname = 'triangoli-regolari/quadrato80';
%meshname = 'triangoli-regolari/quadrato160';
%meshname = 'triangoli-regolari/quadrato320';

% QUAD-TRI

%meshname = 'quad-tri/quad-tri-0.1';
%meshname = 'quad-tri/quad-tri-0.05';
%meshname = 'quad-tri/quad-tri-0.01';
%meshname = 'quad-tri/quad-tri-0.005';
%meshname = 'quad-tri/quad-tri-0.003';
%meshname = 'quad-tri/quad-tri-0.001';
%meshname = 'quad-tri/quad-tri-0.0003';

% TRIANGLES AS QUADRILATERALS

% meshname = 'triangles-as-quadrilaterals/tri-2';
% meshname = 'triangles-as-quadrilaterals/tri-4';
% meshname = 'triangles-as-quadrilaterals/tri-8';
% meshname = 'triangles-as-quadrilaterals/tri-16';
% meshname = 'triangles-as-quadrilaterals/tri-32';
% meshname = 'triangles-as-quadrilaterals/tri-64';

% QUADRATI BOFFI

% meshname = 'boffi/boffi-2';
% meshname = 'boffi/boffi-4';
% meshname = 'boffi/boffi-8';
% meshname = 'boffi/boffi-16';
% meshname = 'boffi/boffi-32';
% meshname = 'boffi/boffi-64';

% QUADRATI BOFFI 199

% meshname = 'boffi-199/boffi-2';
% meshname = 'boffi-199/boffi-4';
% meshname = 'boffi-199/boffi-8';
% meshname = 'boffi-199/boffi-16';
% meshname = 'boffi-199/boffi-32';
% meshname = 'boffi-199/boffi-64';

% hanging nodes

%meshname = 'hanging-nodes/10x5';
%meshname = 'hanging-nodes/hole';

%meshname = 'poligono/quadratob';
%meshname = 'poligono/centagono';
%meshname = 'poligono/stella5';
%meshname = 'poligono/esagono-irregolare';
%meshname = 'poligono/pentagono';

%
meshfilename = [meshroot '/' meshname];
%
%
load([meshfilename '.mat'])

mesh2 = mesh;

% build matrixes P and T
for i=1:mesh.NV
    P(i,1) = mesh.vertex(i).x; 
    P(i,2) = mesh.vertex(i).y;
end    
for i=1:mesh.NP    
    T(i,1) = mesh.polygon(i).NV;
    for j=1:mesh.polygon(i).NV
        T(i,j+1) = mesh.polygon(i).vertices(j); 
    end    
end

clear mesh;

% trasforma tutto nel mesh data format del file meshstruct.m

mesh = struct('V',P,'P',T);
mesh.E = edgefinder(T);
mesh.B = bvfinder(mesh.E); 

clear P
clear T
clear i
clear j


% ---------------------
% option for mesh plot
% ---------------------
meshplot(mesh);







