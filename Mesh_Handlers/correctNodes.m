function newNodes = correctNodes( mesh, bnd )

if strcmp(bnd, 'unitcircle')

    mesh     = loadmesh(mesh, 0);
    newNodes = mesh.coordinates;
    xBnd     = newNodes(mesh.B,1);
    yBnd     = newNodes(mesh.B,2);

    tol          = 1e-10;
    test         = abs(xBnd.^2 + yBnd.^2 - 1);
    Ncorrections = sum(test > tol);
    fprintf('Number of boundary nodes: %i\n', length(xBnd));
    fprintf('Maximum deviation from the constraint before correction: %e\n', max(test));
    fprintf('Number of boundary nodes requiring correction: %i\n', Ncorrections);

    correctionFactor   = 1 ./ sqrt( xBnd.^2 + yBnd.^2 );
    newNodes(mesh.B,1) = xBnd .* correctionFactor;
    newNodes(mesh.B,2) = yBnd .* correctionFactor;

    % Check correction
    xBnd         = newNodes(mesh.B,1);
    yBnd         = newNodes(mesh.B,2);
    test         = abs(xBnd.^2 + yBnd.^2 - 1);
    Ncorrections = sum(test > tol);
    fprintf('Maximum deviation from the constraint after correction: %e\n', max(test));
    fprintf('Number of boundary nodes still requiring correction: %i\n', Ncorrections);

elseif strcmp(bnd, 'astroid') || strcmp(bnd, 'flower') || strcmp(bnd, 'spiral') || strcmp(bnd, 'egg')
    mesh = loadmesh(mesh, 0);

    % Find node to node connectivity information for boundary nodes
    v2v  = zeros(mesh.Nver, 2);
    Ndir = length(mesh.B);
    for i = 1:Ndir
        v1 = mesh.edges(mesh.dirfaces(i),1);
        v2 = mesh.edges(mesh.dirfaces(i),2);
        if v2v(v1,1) == 0
            v2v(v1,1) = v2;
        else
            v2v(v1,2) = v2;
        end
        if v2v(v2,1) == 0
            v2v(v2,1) = v1;
        else
            v2v(v2,2) = v1;
        end
    end

    newNodes = mesh.coordinates;
    xBnd     = newNodes(mesh.B,1);
    yBnd     = newNodes(mesh.B,2);
    tol      = 1e-10;

    if strcmp(bnd, 'astroid')
        test  = abs( 1 - ( nthroot(xBnd,3).^2 + nthroot(yBnd,3).^2 ) );
    elseif strcmp(bnd, 'egg')
        a = 1;
        b = 0.7*a; bp = b/4; ap = a/2 - bp;
        test = abs( ( xBnd.^2 + yBnd.^2 - ap^2 ).^2 ./ ( 4 * bp^2 * (xBnd+ap).^2 ) + ...
            ( 4 * (xBnd+ap).^2 .* yBnd.^2 ) ./ ((xBnd+ap).^2+yBnd.^2).^2 - 1 );
    elseif strcmp(bnd, 'flower')
        n     = 9;
        theta = atan2(yBnd, xBnd);
        test  = abs( sqrt(xBnd.^2 + yBnd.^2) - 2 - sin(n*theta) );
    elseif strcmp(bnd, 'spiral')
        % Correct all boundary points
        test = ones(Ndir,1);
    end

    Ncorrections = sum(test > tol);
    fprintf('Number of boundary nodes: %i\n', length(xBnd));
    fprintf('Maximum deviation from the constraint before correction: %e\n', max(test));
    fprintf('Number of boundary nodes requiring correction: %i\n', Ncorrections);

    middles    = mesh.coordinates(mesh.B(test > tol),:);
    v1         = mesh.coordinates(v2v(mesh.B(test > tol),1),:);
    v2         = mesh.coordinates(v2v(mesh.B(test > tol),2),:);
    NxEdge     = v2(:,2) - v1(:,2);
    NyEdge     = v1(:,1) - v2(:,1);
    edgeLength = sqrt( NxEdge.^2 + NyEdge.^2 );

    delta = zeros(Ncorrections,1);
    parfor i = 1:Ncorrections
        middle   = middles(i,:);
        delta(i) = computeDelta( bnd, middle(1), middle(2), edgeLength(i), ...
                                 NxEdge(i), NyEdge(i), [], 0 );
    end

    newNodes(mesh.B(test > tol),1) = middles(:,1) + delta .* (NxEdge ./ edgeLength);
    newNodes(mesh.B(test > tol),2) = middles(:,2) + delta .* (NyEdge ./ edgeLength);

    % Check correction
    xBnd = newNodes(mesh.B,1);
    yBnd = newNodes(mesh.B,2);
    if strcmp(bnd, 'astroid')
        test  = abs( 1 - ( nthroot(xBnd,3).^2 + nthroot(yBnd,3).^2 ) );
    elseif strcmp(bnd, 'egg')
        a = 1;
        b = 0.7*a; bp = b/4; ap = a/2 - bp;
        test = abs( ( xBnd.^2 + yBnd.^2 - ap^2 ).^2 ./ ( 4 * bp^2 * (xBnd+ap).^2 ) + ...
            ( 4 * (xBnd+ap).^2 .* yBnd.^2 ) ./ ((xBnd+ap).^2+yBnd.^2).^2 - 1 );
    elseif strcmp(bnd, 'flower')
        n     = 9;
        theta = atan2(yBnd, xBnd);
        test  = abs( sqrt(xBnd.^2 + yBnd.^2) - 2 - sin(n*theta) );
    elseif strcmp(bnd, 'spiral')
        test = zeros(Ndir,1);
    end

    Ncorrections = sum(test > tol);
    fprintf('Maximum deviation from the constraint after correction: %e\n', max(test));
    fprintf('Number of boundary nodes still requiring correction: %i\n', Ncorrections);

elseif strcmp(bnd, 'quarter')

    Rin   = 1;
    Rout  = 2;
    tol   = 1e-10;

    mesh     = loadmesh(mesh, 0);
    newNodes = mesh.coordinates;

    Ndir = length(mesh.B);
    xBnd = newNodes(mesh.B,1);
    yBnd = newNodes(mesh.B,2);

    testA   = find(abs(xBnd) < tol);
    [~, ix] = sort( yBnd(testA) );
    testA   = testA( ix(2:end-1) );
    testB   = find(abs(yBnd) < tol);
    [~, ix] = sort( xBnd(testB) );
    testB   = testB( ix(2:end-1) );
    test1   = setdiff((1:Ndir)', [testA; testB]);
    tmp   = xBnd(test1).^2 + yBnd(test1).^2;
    test2 = abs(tmp - Rin^2) < abs(tmp - Rout^2);

    testA        = abs(tmp(test2) - Rin^2);
    testB        = abs(tmp(~test2) - Rout^2);
    Ncorrections = sum(testA > tol) + sum(testB > tol);
    fprintf('Number of boundary nodes: %i\n', length(xBnd));
    fprintf('Maximum deviation from the constraint before correction: %e\n', max(max(testA), max(testB)));
    fprintf('Number of boundary nodes requiring correction: %i\n', Ncorrections);

    correctionFactor         = zeros(length(tmp),1);
    tmp                      = mesh.B(test1);
    correctionFactor(test2)  = Rin ./ sqrt( newNodes(tmp(test2),1).^2 + newNodes(tmp(test2),2).^2 );
    correctionFactor(~test2) = Rout ./ sqrt( newNodes(tmp(~test2),1).^2 + newNodes(tmp(~test2),2).^2 );
    newNodes(tmp,1)          = newNodes(tmp,1) .* correctionFactor;
    newNodes(tmp,2)          = newNodes(tmp,2) .* correctionFactor;

    % Check correction
    xBnd     = newNodes(mesh.B,1);
    yBnd     = newNodes(mesh.B,2);

    testA   = find(abs(xBnd) < tol);
    [~, ix] = sort( yBnd(testA) );
    testA   = testA( ix(2:end-1) );
    testB   = find(abs(yBnd) < tol);
    [~, ix] = sort( xBnd(testB) );
    testB   = testB( ix(2:end-1) );
    test1   = setdiff((1:Ndir)', [testA; testB]);
    tmp   = xBnd(test1).^2 + yBnd(test1).^2;
    test2 = abs(tmp - Rin^2) < abs(tmp - Rout^2);

    testA        = abs(tmp(test2) - Rin^2);
    testB        = abs(tmp(~test2) - Rout^2);
    Ncorrections = sum(testA > tol) + sum(testB > tol);
    fprintf('Maximum deviation from the constraint after correction: %e\n', max(max(testA), max(testB)));
    fprintf('Number of boundary nodes still requiring correction: %i\n', Ncorrections);

elseif strcmp(bnd, 'sector')

    Rout = 4;
    tol1 = 1e-3;
    tol2 = 1e-10;

    mesh     = loadmesh(mesh, 0);
    newNodes = mesh.coordinates;
    bndEdges = mesh.edges(mesh.dirfaces,1:2);

    Nx          = newNodes(bndEdges(:,2),2) - newNodes(bndEdges(:,1),2);
    Ny          = newNodes(bndEdges(:,1),1) - newNodes(bndEdges(:,2),1);
    edgeLengths = sqrt( Nx.^2 + Ny.^2 );
    Ny          = Ny ./ edgeLengths;

    xc   = ( newNodes(bndEdges(:,1),1) + newNodes(bndEdges(:,2),1) )/2;
    test = abs(Ny) < tol1 & xc < 2;

    tmp  = bndEdges(~test,:); tmp = unique(tmp(:));
    xBnd = newNodes(tmp,1);
    yBnd = newNodes(tmp,2);

    testA        = abs(xBnd.^2 + yBnd.^2 - Rout^2);
    Ncorrections = sum(testA > tol2);
    fprintf('Number of boundary nodes: %i\n', length(mesh.B));
    fprintf('Maximum deviation from the constraint before correction: %e\n', max(testA));
    fprintf('Number of boundary nodes requiring correction: %i\n', Ncorrections);

    correctionFactor = Rout ./ sqrt( xBnd.^2 + yBnd.^2 );
    newNodes(tmp,1)  = newNodes(tmp,1) .* correctionFactor;
    newNodes(tmp,2)  = newNodes(tmp,2) .* correctionFactor;

    % Check correction
    Ny   = newNodes(bndEdges(:,1),1) - newNodes(bndEdges(:,2),1);
    xc   = ( newNodes(bndEdges(:,1),1) + newNodes(bndEdges(:,2),1) )/2;
    test = abs(Ny) < tol1 & xc < 2;

    tmp  = bndEdges(~test,:); tmp = unique(tmp(:));
    xBnd = newNodes(tmp,1);
    yBnd = newNodes(tmp,2);

    testA        = abs(xBnd.^2 + yBnd.^2 - Rout^2);
    Ncorrections = sum(testA > tol2);
    fprintf('Maximum deviation from the constraint after correction: %e\n', max(testA));
    fprintf('Number of boundary nodes still requiring correction: %i\n', Ncorrections);

else

    newNodes = mesh.Node;

end

end
