function [] = agglomerate( filename, isoff, isnodeele, nparts, show )

addpath /home/daniele/Documents/codes/vem_matlab/VEM_diffusion/

if isoff
    % Read .off mesh file
    mesh = readOFFmesh(filename);
end

if isnodeele
    % Read .mat file with mesh.Node and mesh.Element
    load(filename)
end

Nelt = size(mesh.Element,1);

%
% Run METIS
%

metisfilename = [filename '.metis'];

fid = fopen(metisfilename, 'w');
fprintf(fid, '%d\n', Nelt);
for i = 1:Nelt
    fprintf(fid, '%s\n', num2str(mesh.Element{i}));
end
fclose(fid);

eval(sprintf(['!/home/daniele/Documents/codes/metis-5.1.0/build/Linux-x86_64/programs/mpmetis ' ...
              '-ncommon=2 -contig %s %i'], ...
              metisfilename, nparts));
eval(sprintf('!rm %s', metisfilename));

%
% Build agglomerates
%

NeltArray = zeros(nparts, 1);

fid = fopen([metisfilename '.epart.' num2str(nparts)]);
for i = 1:Nelt
    idx = fscanf(fid, '%i', 1) + 1;
    NeltArray(idx) = NeltArray(idx) + 1;
end
fclose(fid);

elementArray = cell(nparts, 1);
for i = 1:nparts
    elementArray{i} = cell( NeltArray(i), 1 );
end

NeltArray(:) = 0;
NedArray     = zeros(nparts, 1);

fid = fopen([metisfilename '.epart.' num2str(nparts)]);
for i = 1:Nelt
    idx = fscanf(fid, '%i', 1) + 1;
    NeltArray(idx) = NeltArray(idx) + 1;
    NedArray(idx)  = NedArray(idx) + length(mesh.Element{i});
    elementArray{idx}{NeltArray(idx)} = mesh.Element{i};
end
fclose(fid);

eval(sprintf('!rm %s.epart.%i', metisfilename, nparts));
eval(sprintf('!rm %s.npart.%i', metisfilename, nparts));

coarseElements = cell(nparts, 1);

for idx = 1:nparts
    edges   = zeros(NedArray(idx), 2);
    counter = 0;
    for E = 1:NeltArray(idx)
        NedLoc = length(elementArray{idx}{E});
        edges(counter+1:counter+NedLoc,:) = [elementArray{idx}{E}' elementArray{idx}{E}([2:NedLoc 1])'];
        counter = counter + NedLoc;
    end
    sorted_edges = sort(edges, 2);
    [newedges, ix, jx] = unique(sorted_edges, 'rows');
    Ned = length(ix);
    N   = histc(jx, 1:Ned);
    bndEdges = newedges(N == 1, 1:2);
    Ndir     = size(bndEdges, 1);
    coarseElements{idx} = zeros(1, Ndir);
    
    neighbors = zeros(size(mesh.Node,1), 2);
    for j = 1:Ndir
        node1 = bndEdges(j,1);
        node2 = bndEdges(j,2);
        if neighbors(node1,1) == 0
            neighbors(node1,1) = node2;
        else
            neighbors(node1,2) = node2;
        end
        if neighbors(node2,1) == 0
            neighbors(node2,1) = node1;
        else
            neighbors(node2,2) = node1;
        end
    end
    
    firstNode = bndEdges(1,1);
    coarseElements{idx}(1) = firstNode;
    previousNode = 0;
    currentNode  = firstNode;
    for j = 2:Ndir
        if neighbors(currentNode,1) ~= previousNode
            previousNode = currentNode;
            currentNode  = neighbors(previousNode,1);
        else
            previousNode = currentNode;
            currentNode  = neighbors(previousNode,2);
        end
        coarseElements{idx}(j) = currentNode;
    end

    % Find mid point
    C = sum(mesh.Node(coarseElements{idx},:),1)/Ndir;
    
    v1 = [ mesh.Node(coarseElements{idx},1)-C(1) mesh.Node(coarseElements{idx},2)-C(2) ];
    v2 = [ mesh.Node(coarseElements{idx}([2:Ndir 1]),1)-C(1) mesh.Node(coarseElements{idx}([2:Ndir 1]),2)-C(2) ];
    normal = sum( v1(:,1).*v2(:,2) - v1(:,2).*v2(:,1) );
    if normal < 0
        coarseElements{idx} = coarseElements{idx}(end:-1:1);
    end
end

[C, ~, IC] = unique( horzcat( coarseElements{:} ) );
coarseNodes = mesh.Node( C, : );
NverCoarse  = size(coarseNodes, 1);

% Update numbering
counter = 0;
for i = 1:nparts
    NverLoc = length( coarseElements{i} );
    for n = 1:NverLoc
        counter = counter + 1;
        coarseElements{i}(n) = IC(counter);
    end
end

if show
    figure()
    axis equal; hold on;

    for K = 1:Nelt
        plot(mesh.Node([mesh.Element{K} mesh.Element{K}(1)],1), ...
             mesh.Node([mesh.Element{K} mesh.Element{K}(1)],2), 'b', 'Linewidth', 0.5)
    end

    pause

    for K = 1:nparts
        plot(coarseNodes([coarseElements{K} coarseElements{K}(1)],1), ...
             coarseNodes([coarseElements{K} coarseElements{K}(1)],2), 'r', 'Linewidth', 3)
    end

    pause

    for K = 1:NverCoarse
        text(coarseNodes(K,1), coarseNodes(K,2), num2str(K))
    end

    pause
end

mesh.Node    = coarseNodes;
mesh.Element = coarseElements;

mesh = loadmesh(mesh, 0);
[ verDofs, ~, ~, eleDofs ] = dofsByEle(mesh, 1);

coarseNodes(verDofs,:) = coarseNodes;
coarseElements         = eleDofs;

if show
    figure()
    axis equal; hold on;

    for K = 1:nparts
        plot(coarseNodes([coarseElements{K} coarseElements{K}(1)],1), ...
             coarseNodes([coarseElements{K} coarseElements{K}(1)],2), 'r', 'Linewidth', 3)
    end

    pause

    for K = 1:NverCoarse
        text(coarseNodes(K,1), coarseNodes(K,2), num2str(K))
    end

    pause
end

clear mesh

mesh.Node    = coarseNodes;
mesh.Element = coarseElements;

filename = sprintf('%s_nparts_%08i.mat', metisfilename, nparts);
save(filename, 'mesh');

end
