function [ errH1_u, relerrH1_u ] = ...
    compute_errH1_DG( mesh, k, u_h, ...
                      xqd, yqd, wqd, basis_on_quad_pts2d, grad_basis_on_quad_pts2d, use_mp )

d2 = ((k+1)*(k+2)) / 2;

[ errL2_u, ~, normL2_u ] = ...
    compute_errL2_DG( mesh, k, u_h, xqd, yqd, wqd, basis_on_quad_pts2d, use_mp );

errH1_u = 0;
norm_grad_sol_sq = 0;

parfor ( K = 1:mesh.Nelt, getParforArg() )
    [~,  grad_sol] = exactsol_DG( xqd{K}, yqd{K}, use_mp );
    norm_grad_sol_sq = norm_grad_sol_sq + wqd{K}' * ( grad_sol(:,1).^2 + grad_sol(:,2).^2 );
    errH1_u = errH1_u + wqd{K}' * ...
        ( ( grad_sol(:,1) - grad_basis_on_quad_pts2d{K}(:,1:d2,1) * u_h(:,K) ) .^ 2 + ...
          ( grad_sol(:,2) - grad_basis_on_quad_pts2d{K}(:,1:d2,2) * u_h(:,K) ) .^ 2   );
end

normH1_u = sqrt( normL2_u^2 + norm_grad_sol_sq );

errH1_u = sqrt( errL2_u^2 + errH1_u );
relerrH1_u = errH1_u / normH1_u;

end
