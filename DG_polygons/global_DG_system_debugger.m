function [ residual ] = global_DG_system_debugger( mesh, k, formula1d, basis1d, ...
                                         H, F, diridofs, use_mp )

xbnd = mesh.coordinates(:, 1); xbnd = xbnd( mesh.edges(:,1:2)' );
ybnd = mesh.coordinates(:, 2); ybnd = ybnd( mesh.edges(:,1:2)' );

xbnd = formula1d(:,1:2) * xbnd;
ybnd = formula1d(:,1:2) * ybnd;

u_ex = exactsol_DG(xbnd, ybnd, use_mp);

basis1d = bsxfun(@times, formula1d(:,3), basis1d);
basis1d = bsxfun(@times, (2*(1:k+1)-1)', basis1d');
u_ex_proj = basis1d * u_ex;
u_ex_proj = u_ex_proj(:);

left = H*u_ex_proj;

N = mesh.Nfc * (k+1);
dofs = setdiff(1:N, diridofs);

residual = norm(left(dofs) - F(dofs), inf);

end