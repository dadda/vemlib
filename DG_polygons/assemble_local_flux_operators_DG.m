function [ local_solvers, H, row_dofs_H, col_dofs_H, rhs, dofs_rhs, ...
    basis_on_quad_pts2d, grad_basis_on_quad_pts2d ] = ...
    assemble_local_flux_operators_DG( mesh, h, areas, xb, yb, Nx, Ny, ...
                                      k, basis_type, ...
                                      formula1d, legendre, interpD, ...
                                      xqd, yqd, wqd, ...
                                      t, alpha, ...
                                      refStab, PNs, P0s, S, ref_areas, ...
                                      sConsistency, sStability, sProjOnConst, ...
                                      fType, use_mp )

coordinates  = mesh.coordinates;
elements     = mesh.elements;
permutations = mesh.perm;
edgebyele    = mesh.edgebyele;

basis_on_quad_pts2d      = cell(mesh.Nelt,1);
grad_basis_on_quad_pts2d = cell(mesh.Nelt,1);
local_solvers            = cell(mesh.Nelt,1);

H   = cell(mesh.Nelt,1);
rhs = cell(mesh.Nelt,1);

row_dofs_H = cell(mesh.Nelt,1);
col_dofs_H = cell(mesh.Nelt,1);
dofs_rhs   = cell(mesh.Nelt,1);

% parfor ( K = 1:mesh.Nelt, getParforArg() )
for K = 1:mesh.Nelt
    element = elements(K,:);
    perm    = permutations(K,:);
    
    [basis_on_quad_pts2d{K}, grad_basis_on_quad_pts2d{K}] = ...
        monomial_basis(xqd{K}, yqd{K}, k+2, xb(K), yb(K), h(K), ones(size(xqd{K})));
    basis_on_quad_pts2d{K}      = permute(basis_on_quad_pts2d{K}, [1, 3, 2]);
    grad_basis_on_quad_pts2d{K} = permute(grad_basis_on_quad_pts2d{K}, [1, 3, 4, 2]);

    Nver_loc = element(1);

    if refStab == 1
        % Load stabilization matrix and/or projectors that have been
        % already computed

        if Nver_loc < length(PNs)
            PNs_K = PNs{Nver_loc};
        else
            PNs_K = [];
        end
        
        if Nver_loc < length(P0s)
            P0s_K = P0s{Nver_loc};
        else
            P0s_K = [];
        end
        
        if Nver_loc < length(S)
            S_K = S{Nver_loc};
        else
            S_K = [];
        end

        if Nver_loc < length(ref_areas)
            ref_area_K = ref_areas(Nver_loc);
        else
            ref_area_K = [];
        end
    else
        PNs_K = []; P0s_K = []; S_K = []; ref_area_K = [];
    end
    
    f_K = loadcal_DG(xqd{K}, yqd{K}, use_mp);

    if basis_type == 1
        [ ~, ~, ~, PHI, ~, ~, ~, Z, G ] = ...
                   local_solver_DG( k, coordinates, element, perm, xb(K), yb(K), h(K), areas(K), ...
                                    Nx{K}, Ny{K}, ...
                                    formula1d, legendre, interpD, ...
                                    wqd{K}, basis_on_quad_pts2d{K}, grad_basis_on_quad_pts2d{K}, ...
                                    t, alpha(K), ...
                                    refStab, PNs_K, P0s_K, S_K, ref_area_K, ...
                                    sConsistency, sStability, sProjOnConst, ...
                                    fType, f_K );        
    else
        basisL2norm                 = sqrt( wqd{K}' * basis_on_quad_pts2d{K}.^2 );
        basis_on_quad_pts2d{K}      = bsxfun(@times, 1./basisL2norm, basis_on_quad_pts2d{K});
        grad_basis_on_quad_pts2d{K} = bsxfun(@times, 1./basisL2norm, grad_basis_on_quad_pts2d{K});

        [ ~, ~, ~, PHI, ~, ~, ~, Z, G ] = ...
                   local_solver_DG( k, coordinates, element, perm, xb(K), yb(K), h(K), areas(K), ...
                                    Nx{K}, Ny{K}, ...
                                    formula1d, legendre, interpD, ...
                                    wqd{K}, basis_on_quad_pts2d{K}, grad_basis_on_quad_pts2d{K}, ...
                                    t, alpha(K), ...
                                    refStab, PNs_K, P0s_K, S_K, ref_area_K, ...
                                    sConsistency, sStability, sProjOnConst, ...
                                    fType, f_K, basisL2norm );        
    end
    

    dimLambdaK = Nver_loc * (k+1);
    d2 = ((k+1)*(k+2)) / 2.;
    
    % Remark: PHI is symmetric in general (diagonal in our particular
    % implementation), so PHI = PHI'
    
    if 0
        residual = local_solver_DG_debugger( k, xqd{K}, yqd{K}, wqd{K}, basis_on_quad_pts2d{K}, ...
                                          coordinates, element, formula1d, legendre, ...
                                          Nx{K}, Ny{K}, perm, ...
                                          PHI, Z, G, use_mp );
        fprintf('Residual on element %i = %e\n', K, residual);
    end
    
    local_solvers{K} = Z\[ [zeros(d2, dimLambdaK); PHI] G ];
    H{K}   = PHI * local_solvers{K}(d2+1:end, 1:end-1);
    H{K}   = H{K}(:);
    rhs{K} = -PHI * local_solvers{K}(d2+1:end, end);

    dofs_rhs{K} = bsxfun(@plus, (1:k+1)', (edgebyele(K,1:Nver_loc)-1)*(k+1) );
    dofs_rhs{K} = dofs_rhs{K}(:);
    
    tmp = repmat(dofs_rhs{K}, 1, (k+1)*Nver_loc);
    row_dofs_H{K} = tmp(:);
    tmp = tmp';
    col_dofs_H{K} = tmp(:);
end

H          = vertcat(H{:});
row_dofs_H = vertcat(row_dofs_H{:});
col_dofs_H = vertcat(col_dofs_H{:});

rhs      = vertcat(rhs{:});
dofs_rhs = vertcat(dofs_rhs{:});

end




