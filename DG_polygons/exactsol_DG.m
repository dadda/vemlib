function [ sol, grad_sol ] = exactsol_DG(x, y, use_mp)
% exact solution function

% % Constant
% sol      = ones(size(x));
% grad_sol = zeros( [size(x) 2] );

% % Linear
% sol = x + y;
% grad_sol = ones( [size(x) 2] );

% % Quadratic
% sol = (x.^2 + y.^2)/4.;
% grad_sol = [0.5*x 0.5*y];
% grad_sol = reshape(grad_sol, [size(x) 2]);

% % Cubic
% sol = (x.^3 + y.^3)/6.;
% grad_sol = [0.5*x.^2 0.5*y.^2];
% grad_sol = reshape(grad_sol, [size(x) 2]);

% Cinf, homogeneous
if use_mp
    mypi = mp('pi');
else
    mypi = pi;
end
sol = sin(mypi*x) .* sin(mypi*y) / (2*mypi^2); 
grad_sol = [ sin(mypi*y).*cos(mypi*x)/(2*mypi) sin(mypi*x).*cos(mypi*y)/(2*mypi) ];
grad_sol = reshape(grad_sol, [size(x) 2]);

% % Cinf, non homogeneous
% if use_mp
%     mypi = mp('pi');
% else
%     mypi = pi;
% end
% sol = cos(mypi*x).*cos(mypi*y)/(2*mypi^2); 
% grad_sol = [ -sin(mypi*x).*cos(mypi*y)/(2*mypi) -sin(mypi*y).*cos(mypi*x)/(2*mypi) ];
% grad_sol = reshape(grad_sol, [size(x) 2]);

end
