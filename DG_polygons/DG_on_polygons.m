function [ u_h, lambda_h, errL2_u, relerrL2_u, errH1_u, relerrH1_u, h, xb, yb, xqd, yqd, wqd, basis_on_quad_pts2d ] = ...
    DG_on_polygons( mesh, k, basis_type, eoa, compressquad, t, alpha, ...
                    refStab, sConsistency, sStability, sProjOnConst, fType, use_mp )

%
% Gauss-Lobatto rule with k+3 nodes
%
[xqd1, wqd1] = lobatto(k+3, use_mp);
formula1d    = [(1.-xqd1)/2. (xqd1+1.)/2. wqd1/2.];
interpD      = interpDeriv(k+2, use_mp);

%
% Legendre polynomials on [0,1]
%
basis1d = legendre_on_unit_interval(formula1d(:,2), k);

%
% Compute geometrical data structures in parallel
%
[ h, areas, xb, yb, Nx, Ny, xqd, yqd, wqd ] = ...
    build_geometrical_data_structures( mesh, k, eoa, compressquad, formula1d, use_mp );

%
% Create a pool of projectors and stabilization matrices to be used in the
% local solvers, if requested
%
PNs      = cell(mesh.Nelt,1);
P0s      = cell(mesh.Nelt,1);
S        = cell(mesh.Nelt,1);
refAreas = zeros(mesh.Nelt,1);

if refStab == 1
    [ PNs, P0s, S, refAreas ] = stabPool( mesh.Nver_max, k, basis_type, ...
                                          sConsistency, sStability, sProjOnConst, ...
                                          use_mp );
end

%
% Compute the stabilization parameter \alpha, \alpha_1, \alpha_2
%
alpha = alpha * ones(1, mesh.Nelt);

%
% Assemble local flux operators
%
[ local_solvers, H, row_dofs_H, col_dofs_H, rhs, dofs_rhs, ...
  basis_on_quad_pts2d, grad_basis_on_quad_pts2d ] = ...
    assemble_local_flux_operators_DG( mesh, h, areas, xb, yb, Nx, Ny, ...
                                      k, basis_type, ...
                                      formula1d, basis1d, interpD, ...
                                      xqd, yqd, wqd, ...
                                      t, alpha, ...
                                      refStab, PNs, P0s, S, refAreas, ...
                                      sConsistency, sStability, sProjOnConst, ...
                                      fType, use_mp );

%
% Global flux matrix
%
H = sparse( row_dofs_H, col_dofs_H, H );

%
% Global right hand side
%
rhs = sparse( dofs_rhs, ones([length(dofs_rhs),1]), rhs );

%
% Neumann boundary conditions
%
[ neu_rhs, neu_dofs ] = compute_neumann_DG( mesh, k, formula1d, basis1d, use_mp );

rhs(neu_dofs) = rhs(neu_dofs) + neu_rhs;

%
% Dirichlet boundary conditions
%
[ Udir, diridofs ] = compute_dirichlet_DG( mesh, k, formula1d, basis1d, use_mp );

%
% Solve the linear system
%
fprintf('Solving the linear system ... ');
U = solve_DG_system( mesh, k, diridofs, H, rhs, Udir );
fprintf('done\n');

%
% Debug
%
if 0
    residual = global_DG_system_debugger( mesh, k, formula1d, basis1d, H, rhs, diridofs, use_mp );
    fprintf('Residual: %e\n', residual);
end

%
% Reconstruct u_K and lambda_K element-wise
%
[ u_h, lambda_h ] = reconstruct_local_variables( mesh, k, U, local_solvers );

%
% Error computation
%
[errL2_u, relerrL2_u] = ...
    compute_errL2_DG( mesh, k, u_h, xqd, yqd, wqd, basis_on_quad_pts2d, use_mp );

[ errH1_u, relerrH1_u ] = ...
    compute_errH1_DG( mesh, k, u_h, ...
                      xqd, yqd, wqd, basis_on_quad_pts2d, grad_basis_on_quad_pts2d, use_mp );

end










