function [ u_h, lambda_h ] = reconstruct_local_variables( mesh, k, U, local_solvers )

d2 = ((k+1)*(k+2)) / 2;

u_h      = zeros(d2, mesh.Nelt);
lambda_h = cell(mesh.Nelt, 1);

edgebyele = mesh.edgebyele;
elements  = mesh.elements;

parfor ( K = 1:mesh.Nelt, getParforArg() )
    Nver_loc = elements(K,1);
    tmp = U(:, edgebyele(K,1:Nver_loc));
    tmp = tmp(:);
    
    tmp = local_solvers{K}(:, 1:end-1) * tmp + local_solvers{K}(:,end);
    u_h(:,K) = tmp(1:d2);
    lambda_h{K} = tmp(d2+1:end);
end

end








