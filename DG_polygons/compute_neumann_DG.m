function [ neu_rhs, neu_dofs ] = compute_neumann_DG( mesh, k, formula1d, basis1d, use_mp )

if isfield( mesh, 'neufaces' )
    xneu = mesh.coordinates( :, 1 ); xneu = xneu( mesh.edges(mesh.neufaces,1:2)' );
    yneu = mesh.coordinates( :, 2 ); yneu = yneu( mesh.edges(mesh.neufaces,1:2)' );
        
    basis1d = bsxfun(@times, formula1d(:,3), basis1d);

    % Compute normals
    Nx = yneu(2,:) - yneu(1,:);
    Ny = xneu(1,:) - xneu(2,:);

    % Now udate xneu, yneu with quadrature points on neumann edges
    xneu = formula1d(:,1:2) * xneu;
    yneu = formula1d(:,1:2) * yneu;
    
    g_neu = neudata_DG( xneu, yneu, use_mp );
    
    % Determine which normals need to be inverted to become outer
    % normals
    tmp = mesh.edgebyele .* (mesh.perm-1); % Now tmp contains the indices of the faces
                                           % whose orientation needs to
                                           % be switched
    tmp = double( tmp(:) );

    [~, ~, ib] = intersect( tmp, double( mesh.neufaces ) );
    Nx(ib) = -Nx(ib);
    Ny(ib) = -Ny(ib);
    
    edges_lengths = sqrt( Nx.^2 + Ny.^2 );
    
    if isstruct( g_neu )        
        neu_rhs = bsxfun(@times, Nx, basis1d' * g_neu.x) + ...
                  bsxfun(@times, Ny, basis1d' * g_neu.y);
    else
        neu_rhs = bsxfun(@times, edges_lengths, basis1d' * g_neu);
    end

    k = double(k);
    neu_dofs = bsxfun(@plus, (1:k+1)', (double(mesh.neufaces)'-1)*(k+1) );
    neu_dofs = neu_dofs(:);
    
else
    neu_rhs  = [];
    neu_dofs = [];
end

end

