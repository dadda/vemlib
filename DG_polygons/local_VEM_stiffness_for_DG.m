function [ PNs, P0s, S ] = local_VEM_stiffness_for_DG( k, Nver_loc, Nx_K, Ny_K, ...
                                                       edges_lengths, area_K, ...
                                                       formula1d, interpD, ...
                                                       weights2d, ...
                                                       basis_on_quad_pts2d, ...
                                                       grad_basis_on_quad_pts2d, ...
                                                       basis_on_bnd, grad_basis_on_bnd, ...
                                                       sConsistency, sStability, sProjOnConst )

d2   = ((k+1)*(k+2))/2;
dimW = k * Nver_loc;
d2m2 = ((k-1)*k)/2;

B = zeros(d2, dimW);
B(2:end, 1:Nver_loc) = ...
    ( bsxfun(@times, Nx_K(1:end-1)+Nx_K(2:end), grad_basis_on_bnd(1:Nver_loc, 2:end, 1) ) + ...
      bsxfun(@times, Ny_K(1:end-1)+Ny_K(2:end), grad_basis_on_bnd(1:Nver_loc, 2:end, 2) ) )' ...
    * formula1d(1,3);

B(2:end, Nver_loc+1:end) = ...
    ( bsxfun(@times, kron(Nx_K(2:end), formula1d(2:end-1,3)), ...
                     grad_basis_on_bnd(Nver_loc+1:end, 2:end, 1) ) + ...
      bsxfun(@times, kron(Ny_K(2:end), formula1d(2:end-1,3)), ...
                     grad_basis_on_bnd(Nver_loc+1:end, 2:end, 2) ) )';

% Gf must be computed from its first definition. In fact,
% it is not true anymore that G = B*D.

% Assemble \Pi^\nabla projector
G = zeros(d2, d2);
G(1,:) = (1./area_K) * (weights2d' * basis_on_quad_pts2d);
for i = 2:d2
    for j = 2:i-1
        G(i,j) = ...
            weights2d' * (grad_basis_on_quad_pts2d(:,i,1) .* grad_basis_on_quad_pts2d(:,j,1) + ...
                          grad_basis_on_quad_pts2d(:,i,2) .* grad_basis_on_quad_pts2d(:,j,2));
        G(j,i) = G(i,j);
    end
    G(i,i) = ...
        weights2d' * (grad_basis_on_quad_pts2d(:,i,1).^2 + grad_basis_on_quad_pts2d(:,i,2).^2);
end

PNs     = G\B;

% Assemble \Pi^0 projector
H = zeros(d2, d2);
for i = 1:d2
    for j = 1:i-1
        H(i,j) = weights2d' * (basis_on_quad_pts2d(:,i).*basis_on_quad_pts2d(:,j));
        H(j,i) = H(i,j);
    end
    H(i,i) = weights2d' * (basis_on_quad_pts2d(:,i).^2);
end

C = zeros(d2, dimW);
C(d2m2+1:end,:) = H(d2m2+1:end,:) * PNs;

P0s = H\C;

% Assemble stabilization matrix
S = zeros(dimW);

if sConsistency == 1
    % Classical VEM consistency term
    PN      = basis_on_bnd * PNs; % basis_on_bnd = D, for W(K)
    Gt      = G;
    Gt(1,:) = 0.;

    S = S + PNs' * Gt * PNs;
elseif sConsistency == 2
    % Mortar method recipe
    S = S + mortarStab( edges_lengths(2:end), k, formula1d(:,3), interpD );
end

if sStability == 1
    % Classical VEM stability term
    S = S + (eye(dimW) - PN)'*(eye(dimW) - PN);
elseif sStability == 2
    % Mortar method recipe
    S = S + mortarStab( edges_lengths(2:end), k, formula1d(:,3), interpD );
end

if sProjOnConst == 1
    dK      = sum( edges_lengths(2:end) );
    phi_avg = [ (formula1d(1,3)/dK) * (edges_lengths(1:end-1)+edges_lengths(2:end));
                 (1/dK) * kron( edges_lengths(2:end), formula1d(2:end-1,3) ) ];
    S = S + (phi_avg / dK) * phi_avg';
end

end
