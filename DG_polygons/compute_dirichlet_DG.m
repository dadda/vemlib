function [ Udir, diri_dofs ] = compute_dirichlet_DG( mesh, k, formula1d, basis1d, use_mp )

xdir = mesh.coordinates( :, 1 ); xdir = xdir( mesh.edges(mesh.dirfaces,1:2)' );
ydir = mesh.coordinates( :, 2 ); ydir = ydir( mesh.edges(mesh.dirfaces,1:2)' );

xdir = formula1d(:,1:2) * xdir;
ydir = formula1d(:,1:2) * ydir;

g = dirdata_DG( xdir, ydir, use_mp );

basis1d = bsxfun(@times, formula1d(:,3), basis1d);
basis1d = bsxfun(@times, (2*(1:k+1)-1)', basis1d');
Udir = basis1d * g;

% Make sure diri_dofs will be in double precision format
k = double(k);

diri_dofs = bsxfun(@plus, (1:k+1)', (double(mesh.dirfaces)'-1)*(k+1) );
diri_dofs = diri_dofs(:);

end

