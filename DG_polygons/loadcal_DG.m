function out = loadcal_DG(x, y, use_mp)
% loading function, f = -\Delta u

% out = zeros(size(x));
% out = -ones(size(x));
% out = -(x+y);

% Cinf, homogeneous
if use_mp
    mypi = mp('pi');
else
    mypi = pi;
end
out = sin(mypi*x) .* sin(mypi*y);

% % Cinf, non homogeneous
% if use_mp
%     mypi = mp('pi');
% else
%     mypi = pi;
% end
% out = cos(mypi*x) .* cos(mypi*y);

end