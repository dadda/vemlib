function [ errL2_u, relerrL2_u, normL2_u ] = ...
    compute_errL2_DG( mesh, k, u_h, xqd, yqd, wqd, basis_on_quad_pts2d, use_mp )

d2 = ((k+1)*(k+2)) / 2;

errL2_u = 0;
normL2_u = 0;

parfor ( K = 1:mesh.Nelt, getParforArg() )
    sol = exactsol_DG( xqd{K}, yqd{K}, use_mp );
    normL2_u = normL2_u + wqd{K}' * ( sol ) .^ 2;
    errL2_u = errL2_u + wqd{K}' * ( sol - basis_on_quad_pts2d{K}(:,1:d2) * u_h(:, K) ) .^ 2;
end

normL2_u = sqrt(normL2_u);

errL2_u    = sqrt(errL2_u);
relerrL2_u = errL2_u / normL2_u;

end

