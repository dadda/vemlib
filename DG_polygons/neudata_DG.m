function g_neu = neudata_DG( x, y, use_mp )
% Neumann boundary conditions can be specified in two alternative ways:
% 1: < \lambda, \psi > = <  g_neu \cdot n, \psi >, where g_neu is a vector,
%    in which case we need to provide the two components of g_neu;
% 2: < \lambda, \psi > = < g_neu, \psi >, where g_neu is a scalar, in which case we
%    only need to provide one scalar component.

% % Constant
% g_neu.x = zeros( size(x) );
% g_neu.y = g_neu.x;

% Linear
g_neu.x = ones( size(x) );
g_neu.y = g_neu.x;

% % Quadratic
% g_neu.x = 0.5 * x;
% g_neu.y = 0.5 * y;

% % Cubic
% g_neu.x = 0.5*x.^2;
% g_neu.y = 0.5*y.^2;

% % Cinf, homogeneous
% if use_mp
%     mypi = mp('pi');
% else
%     mypi = pi;
% end
% g_neu.x = sin(mypi*y).*cos(mypi*x)/(2*mypi);
% g_neu.y = sin(mypi*x).*cos(mypi*y)/(2*mypi);

% % Cinf, non homogeneous
% if use_mp
%     mypi = mp('pi');
% else
%     mypi = pi;
% end
% g_neu.x = -sin(mypi*x).*cos(mypi*y)/(2*mypi);
% g_neu.y = -sin(mypi*y).*cos(mypi*x)/(2*mypi);


end












