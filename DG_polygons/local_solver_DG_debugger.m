function residual = local_solver_DG_debugger( k, xqd, yqd, wqd, basis_on_quad_pts2d, ...
                                          coordinates, element, formula1d, basis1d, ...
                                          Nx, Ny, perm_K, ...
                                          PHI, Z, G, use_mp )

% Compute uK = u_h|_K

d2  = ((k+1) * (k+2)) / 2;

sol = exactsol_DG( xqd, yqd, use_mp );
rhs = basis_on_quad_pts2d(:,1:d2)' * (wqd .* sol);
A   = basis_on_quad_pts2d(:,1:d2)' * bsxfun(@times, wqd, basis_on_quad_pts2d(:,1:d2));

uK = A\rhs;

% Compute lambdaK = \nabla u_h\cdot n|_K

Nver_loc = element(1);

xE = [coordinates(element(2:Nver_loc+1),1)'; coordinates(element([3:Nver_loc+1,2]),1)'];
yE = [coordinates(element(2:Nver_loc+1),2)'; coordinates(element([3:Nver_loc+1,2]),2)'];

xbnd = formula1d(:,1:2)*xE;
ybnd = formula1d(:,1:2)*yE;

edge_lengths = sqrt(Nx(2:end).^2 + Ny(2:end).^2);
nx = Nx(2:end)./edge_lengths;
ny = Ny(2:end)./edge_lengths;

[u_dK, gradu_dK] = exactsol_DG(xbnd, ybnd, use_mp);

lambdaExact = bsxfun(@times, nx', gradu_dK(:,:,1)) + bsxfun(@times, ny', gradu_dK(:,:,2));

basis1d = bsxfun(@times, formula1d(:,3), basis1d);
basis1d = bsxfun(@times, (2*(1:k+1)-1)', basis1d');

lambdaK = basis1d * lambdaExact;
lambdaK = lambdaK(:);

phidK = basis1d * u_dK;
tmp = ones(k+1, Nver_loc);
tmp = bsxfun(@times, 3-2*perm_K(1:Nver_loc), tmp);
tmp(1:2:end, :) = 1.; % Restore odd dofs (they should not be reversed)
phidK = phidK .* tmp;
phidK = phidK(:);

dimLambdaK = Nver_loc * (k+1);
tmp = [zeros(d2, dimLambdaK); PHI];

left = Z * [uK; lambdaK];
right = G + tmp*phidK;

residual = norm(left-right,inf);

end

