function out = dirdata_DG(x, y, use_mp)
% Dirichlet boundary data (vector valued)
%

%  ------ AUTO DIRICHLET ------
out = exactsol_DG(x, y, use_mp);
