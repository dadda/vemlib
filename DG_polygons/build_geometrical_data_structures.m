function [ h, areas, xb, yb, Nx, Ny, xqd, yqd, wqd ] = ...
    build_geometrical_data_structures( mesh, k, eoa, compressquad, formula1d, use_mp )

h     = zeros(1, mesh.Nelt);
areas = zeros(1, mesh.Nelt);
xb    = zeros(1, mesh.Nelt);
yb    = zeros(1, mesh.Nelt);

Nx  = cell(mesh.Nelt, 1);
Ny  = cell(mesh.Nelt, 1);

xqd = cell(mesh.Nelt, 1);
yqd = cell(mesh.Nelt, 1);
wqd = cell(mesh.Nelt, 1);

coordinates = mesh.coordinates;
elements    = mesh.elements;

parfor ( K = 1:mesh.Nelt, getParforArg() )
    [ ~, ~, xqd{K}, yqd{K}, wqd{K}, Nx{K}, Ny{K}, h(K), areas(K), xb(K), yb(K) ] = ...
        build_coordinate_matrices_parallel( k+2, eoa, compressquad, ...
                                            elements(K,:), coordinates, formula1d, use_mp );
end

end

