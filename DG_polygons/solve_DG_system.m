function U = solve_DG_system( mesh, k, diridofs, H, rhs, Udir )

N = mesh.Nfc * (k+1);

U = zeros([N 1]);

N = double( N );
diridofs = double( diridofs );
dofs = setdiff(1:N, diridofs);

H11 = H(dofs, dofs);
H12 = H(dofs, diridofs);
U(dofs) = H11\(rhs(dofs) - H12*Udir(:));
% U(dofs) = pcg(H11, rhs(dofs) - H12*Udir(:), 1e-15);
% U(dofs) = gmres(H11, rhs(dofs) - H12*Udir(:), [], 1e-16);

U(diridofs) = Udir(:);

U = reshape(U, k+1, mesh.Nfc);

end

