function [ PNs, P0s, S, areas ] = stabPool( Nver_max, k, basis_type, ...
                                            sConsistency, sStability, sProjOnConst, ...
                                            use_mp )
% Create a pool of projectors and stabilization matrices by computing them
% on reference elements.

% According to the meshes used, some entries in PNs, P0s and S might end up
% being empty

Nver_max = max(Nver_max, 10); % We are building matrices up to a decagon
if use_mp
    Nver_max = mp(Nver_max);
end

PNs   = cell(Nver_max, 1);
P0s   = PNs;
S     = PNs;
areas = zeros(Nver_max, 1);

eoa          = 0;
compressquad = 0;

[xqd1, wqd1] = lobatto(k+3, use_mp);
formula1d    = [(1.-xqd1)/2. (xqd1+1.)/2. wqd1/2.];
interpD      = interpDeriv(k+2, use_mp);

meshroot  = '../Mesh_Handlers/meshfiles_AleRusso';
meshnames = {'poligono/triangle';
             'poligono/square';
             'poligono/pentagono';
             'poligono/hexagon';
             'poligono/ottagono';
             'poligono/decagono'};

nbmeshes = length(meshnames);

for i = 1:nbmeshes
    meshname = [meshroot '/' meshnames{i}];
    tmpMesh  = loadmesh(meshname, use_mp);

    [ ~, ~, xqd_K, yqd_K, wqd_K, Nx_K, Ny_K, h_K, areas(i), xb, yb ] = ...
        build_coordinate_matrices_parallel( k+2, eoa, compressquad, ...
                                            tmpMesh.elements, tmpMesh.coordinates, formula1d, use_mp );

    edges_lengths = sqrt(Nx_K.^2 + Ny_K.^2);
    edges_lengths = edges_lengths(:); % (Nver_loc + 1) entries

    d2s = double( ((k+3)*(k+4)) / 2 ); % Dimension of P_{k+2}(E)
    xE = [tmpMesh.coordinates(tmpMesh.elements(2:tmpMesh.Nver+1),1)';
          tmpMesh.coordinates(tmpMesh.elements([3:tmpMesh.Nver+1,2]),1)'];
    yE = [tmpMesh.coordinates(tmpMesh.elements(2:tmpMesh.Nver+1),2)';
          tmpMesh.coordinates(tmpMesh.elements([3:tmpMesh.Nver+1,2]),2)'];

    xbnd_K = formula1d(:,1:2) * xE;
    ybnd_K = formula1d(:,1:2) * yE;

    [basis_on_quad_pts2d, grad_basis_on_quad_pts2d] = ...
        monomial_basis(xqd_K, yqd_K, k+2, xb, yb, h_K, ones(size(xqd_K)));
    basis_on_quad_pts2d      = permute(basis_on_quad_pts2d, [1, 3, 2]);
    grad_basis_on_quad_pts2d = permute(grad_basis_on_quad_pts2d, [1, 3, 4, 2]);
    
    [ basis_on_bnd, grad_basis_on_bnd ] = ...
        monomial_basis(xbnd_K, ybnd_K, k+2, xb, yb, h_K, ones(size(xbnd_K)));
    basis_on_bnd = [ reshape(basis_on_bnd(1,:,:), tmpMesh.Nver, d2s);
                     reshape(basis_on_bnd(2:end-1,:,:), tmpMesh.Nver*(k+1), d2s) ];
    grad_basis_on_bnd = [ reshape(grad_basis_on_bnd(1, :, :, :), tmpMesh.Nver, d2s, 2);
                          reshape(grad_basis_on_bnd(2:end-1, :, :, :), tmpMesh.Nver*(k+1), d2s, 2) ];

    if basis_type == 2
        basisL2norm              = sqrt( wqd_K' * basis_on_quad_pts2d.^2 );
        basis_on_quad_pts2d      = bsxfun(@times, 1./basisL2norm, basis_on_quad_pts2d);
        grad_basis_on_quad_pts2d = bsxfun(@times, 1./basisL2norm, grad_basis_on_quad_pts2d);
        
        basis_on_bnd      = bsxfun(@times, 1./basisL2norm, basis_on_bnd);
        grad_basis_on_bnd = bsxfun(@times, 1./basisL2norm, grad_basis_on_bnd);
    end
                      
    [ PNs{tmpMesh.Nver}, P0s{tmpMesh.Nver}, S{tmpMesh.Nver} ] = ...
        local_VEM_stiffness_for_DG( k+2, tmpMesh.Nver, Nx_K, Ny_K, ...
                                    edges_lengths, areas(i), ...
                                    formula1d, interpD, wqd_K, ...
                                    basis_on_quad_pts2d, ...
                                    grad_basis_on_quad_pts2d, ...
                                    basis_on_bnd, grad_basis_on_bnd, ...
                                    sConsistency, sStability, sProjOnConst );
    
end


end

