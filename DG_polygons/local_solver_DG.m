function [ A, B, F, PHI, Atilde, Btilde, Ftilde, Z, G, xbnd_K, ybnd_K, edges_lengths ] = ...
           local_solver_DG( k, coordinates, elements_K, perm_K, ...
                            xb, yb, h_K, area_K, Nx_K, Ny_K, ...
                            formula1d, legendre, interpD, ...
                            weights2d, basis_on_quad_pts2d, grad_basis_on_quad_pts2d, ...
                            t, alpha_K, ...
                            refStab, PNs_K, P0s_K, S_K, ref_area_K, ...
                            sConsistency, sStability, sProjOnConst, ...
                            ftype, f_K, varargin )
% Inputs
%
% - weights2d: column vector with the weights of the 2D quadrature formula
%              on the current polygon.
% - formula1d: 1d Gauss-Lobatto quadrature formula with k+3 nodes.
% - legendre: (k+3) x (k+1) matrix where the j-th column contains the
%             evaluations of the j-th Legendre basis function of degree up
%             to k, on the k+3 Gauss-Lobatto quadrature nodes.

% Scan inputs
computeS = false;
if refStab == 1
    if ( isempty(S_K) || isempty(ref_area_K) || ...
         ( isempty(P0s_K) && ftype <= 3) || ...
         ( isempty(PNs_K) && ftype == 4 ) )
        warning('Stabilization matrix and/or projectors not provided. They will be computed locally')
        computeS = true;
    else
%         PNs_K = area_K * PNs_K;
%         P0s_K = area_K * P0s_K;
%         S_K   = area_K * S_K;
    end
else
    computeS = true;
end

Nver_loc = elements_K(1);
d2       = ( (k+1)*(k+2) ) / 2;
d2p1     = ( (k+2)*(k+3) ) / 2;
dimW     = (k+2)*Nver_loc;
    
xE = [coordinates(elements_K(2:Nver_loc+1),1)';
      coordinates(elements_K([3:Nver_loc+1,2]),1)'];
yE = [coordinates(elements_K(2:Nver_loc+1),2)';
      coordinates(elements_K([3:Nver_loc+1,2]),2)'];

xbnd_K = formula1d(:,1:2) * xE;
ybnd_K = formula1d(:,1:2) * yE;

% A = \int_K \nabla u \cdot \nabla v
A = grad_basis_on_quad_pts2d(:,1:d2,1)' * bsxfun(@times, weights2d, grad_basis_on_quad_pts2d(:,1:d2,1)) + ...
    grad_basis_on_quad_pts2d(:,1:d2,2)' * bsxfun(@times, weights2d, grad_basis_on_quad_pts2d(:,1:d2,2));

% B = \int_{\partial K} u^K \mu^K
%
% If ftype == 2, we evaluate basis functions up to degree k+2 to compute
% \Pi^\nabla later.
if computeS
    deg_bnd = k+2;
else
    deg_bnd = k;
end
d2s = ( (deg_bnd+1)*(deg_bnd+2) ) / 2;

[ basis_on_bnd, grad_basis_on_bnd ] = ...
    monomial_basis(xbnd_K, ybnd_K, deg_bnd, ...
                   xb, yb, h_K, ones(size(xbnd_K)));

% Rescale basis functions if needed
if ~isempty(varargin)
    basisL2norms = reshape(varargin{1}(1:d2s), 1, 1, d2s);
    basis_on_bnd = bsxfun(@times, 1./basisL2norms, basis_on_bnd);
    grad_basis_on_bnd = bsxfun(@times, 1./basisL2norms, grad_basis_on_bnd);
end

edges_lengths = sqrt(Nx_K.^2 + Ny_K.^2);
edges_lengths = edges_lengths(:);
legendre = bsxfun(@times, formula1d(:,3), legendre);

B = zeros(Nver_loc*(k+1), d2);
for j = 1:d2
    tmp = legendre' * basis_on_bnd(:,:,j);
    tmp = bsxfun(@times, edges_lengths(2:end)', tmp);
    B(:,j) = tmp(:);
end

% reshape for later use
basis_on_bnd = [ reshape(basis_on_bnd(1,:,:), Nver_loc, d2s);
                 reshape(basis_on_bnd(2:end-1,:,:), Nver_loc*(k+1), d2s) ];
grad_basis_on_bnd = [ reshape(grad_basis_on_bnd(1, :, :, :), Nver_loc, d2s, 2);
                      reshape(grad_basis_on_bnd(2:end-1, :, :, :), Nver_loc*(k+1), d2s, 2) ];

% F = \int_K f v^K
F = basis_on_quad_pts2d(:,1:d2)' * (weights2d .* f_K);

% PHI = \int_{\partial K} \mu^K \phi
tmpA = kron( edges_lengths(2:end), 1./(2*(1:k+1)'-1) );

tmpB = ones(k+1, Nver_loc);
tmpB = bsxfun(@times, 3-2*perm_K(1:Nver_loc), tmpB);
tmpB(1:2:end, :) = 1.; % Restore odd dofs (they should not be reversed)
tmpB = tmpB(:);

PHI = diag( tmpA .* tmpB );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Matrices required by the stabilization term

% Atilde = < Du^K, \phi >
%
% Remark: this matrix will be used later to assemble the Pi Nabla projector
% of the VEM basis functions. In particular, Atilde corresponds to the
% portion of matrix B from the Hitchhicker's paper corresponding to the
% boundary dofs.

Atilde = zeros(dimW, d2);

Atilde(1:Nver_loc, :) = ...
    ( bsxfun(@times, Nx_K(1:end-1)+Nx_K(2:end), grad_basis_on_bnd(1:Nver_loc, 1:d2, 1)) + ...
      bsxfun(@times, Ny_K(1:end-1)+Ny_K(2:end), grad_basis_on_bnd(1:Nver_loc, 1:d2, 2)) ) * formula1d(1,3);

Atilde(Nver_loc+1:end, :) = ...
    ( bsxfun(@times, kron(Nx_K(2:end), formula1d(2:end-1,3)), ...
                     grad_basis_on_bnd(Nver_loc+1:end, 1:d2, 1)) + ...
      bsxfun(@times, kron(Ny_K(2:end), formula1d(2:end-1,3)), ...
                     grad_basis_on_bnd(Nver_loc+1:end, 1:d2, 2)) );


% Btilde = < ( 1 - \tilde\pi )(\gamma_K^* \mu^K_i), \phi_j >
Btilde = zeros((k+1)*Nver_loc, dimW);
dK     = sum(edges_lengths(2:end));

last = [2:Nver_loc 1];
for e = 1:Nver_loc
    mapr = (k+1)*(e-1) + (1:k+1);
    mapc = [e Nver_loc+(k+1)*(e-1)+(1:k+1) last(e)];
    Btilde(mapr, mapc) = legendre' * edges_lengths(e+1);      % Remark: legendre matrix has already been scaled
                                                      % by the quadrature weights
end

mu_avg = zeros(k+1, Nver_loc);
mu_avg(1,:) = edges_lengths(2:end)/dK;
mu_avg = mu_avg(:);

int_W = [ (edges_lengths(1:end-1)+edges_lengths(2:end)) * formula1d(1,3);
          kron( edges_lengths(2:end), formula1d(2:end-1,3) )];

Btilde = Btilde - kron(mu_avg, int_W');

% Stabilization matrix S = s_K( \phi_i, \phi_j )
%
% We support two options:
% - Option 1: load the stabilization matrix and the projectors as inputs;
% - Option 2: assemble the stabilization matrix from definition in
%             "Algebraic ..." paper, paragraph 4.5, as well as the PNs, P0s
%             projectors, locally.

if computeS
    % REMARK: we do not need to pass the L^2 norm of the 2D basis functions
    % to local_VEM_stiffness_for_DG, because there are no internal dofs in
    % our VEM space (see local_assemble_k.m for details in the VEM case)
    [ PNs_K, P0s_K, S_K ] = local_VEM_stiffness_for_DG( k+2, Nver_loc, Nx_K, Ny_K, ...
                                                        edges_lengths, area_K, ...
                                                        formula1d, interpD, weights2d, ...
                                                        basis_on_quad_pts2d, ...
                                                        grad_basis_on_quad_pts2d, ...
                                                        basis_on_bnd, grad_basis_on_bnd, ...
                                                        sConsistency, sStability, sProjOnConst );
end

% Ftilde = < ( 1 - \tilde\pi ) f, \phi > = < f, \phi > - < \tilde\pi(f), \phi >
%
% We allow four options for the first term, < f, \phi > :
% - Option 1: < f, \phi > ~ < f, \Pi^0_{k+2} \phi >, where \Pi^0_{k+2}is the
%             L^2 projector onto P_{k+2};
% - Option 2: < f, \phi > ~ < f, \Pi^0_{k+1} \phi >;
% - Option 3: < f, \phi > ~ < f, \Pi^0_{k} \phi >, where \Pi^0_{k} \phi =
%             0, by definition of W(K);
% - Option 4: < f, \phi > ~ < f, \Pi^\nabla_{k+2} \phi >.

if isempty(varargin)
    q = F(1) / dK; % WE ARE ASSUMING THAT THE FIRST 2D BASIS FUNCTION IS 1
else
    q = sqrt(area_K) * F(1) / dK;
end

Ftilde = (-q) * int_W;

switch ftype
    case 1
        Fbis = [F;
                basis_on_quad_pts2d(:,d2+1:end)'*(weights2d .* f_K)];
        Ftilde = P0s_K' * Fbis + Ftilde;            
    case 2
        Fbis = [F;
                basis_on_quad_pts2d(:,d2+1:d2p1)'*(weights2d .* f_K)];
        Ftilde = P0s_K(1:d2p1,:)' * Fbis + Ftilde;            
    case 3
        Ftilde = P0s_K(1:d2,:)' * F + Ftilde;
    case 4
        Fbis = [F;
                basis_on_quad_pts2d(:,d2+1:end)'*(weights2d .* f_K)];
        Ftilde = PNs_K' * Fbis + Ftilde;
    case default
        error('Unknown option')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Assemblage of the block matrices for computing u^K \lambda^K

x = S_K\[Atilde Btilde' Ftilde];

Z = [ ( A + t*alpha_K*Atilde'*x(:,1:d2) )      -( B' + t*alpha_K*Atilde'*x(:,d2+1:d2+(k+1)*Nver_loc) ) ;
      ( B - alpha_K*Btilde*x(:,1:d2)    )       ( alpha_K*Btilde*x(:,d2+1:d2+(k+1)*Nver_loc)    ) ];

G = [ F + t*alpha_K*Atilde'*x(:,end) ;
      -alpha_K*Btilde*x(:,end)       ];

end











