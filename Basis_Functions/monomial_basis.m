function [basis, grad_basis] = monomial_basis(x, y, k, xb, yb, hE, Xi)
% Scaled Monomial Basis.
%   BASIS = MONOMIAL_BASIS(X, Y, K, XB, YB, HE, XI) evaluates monomial
%   basis functions up to degree up to K at the points (X,Y) given the coordinates
%   of the centroid (XB,YB) and the diameter HE of an element.
%   X and Y are coordinate matrices with as many columns as the number of
%   elements for which we want to compute basis functions. XB, YB, and HE
%   are row vectors with as many elements as the columns of X (or Y). XI is
%   an indicator array with the same size as X (or Y). XI(i,j) = 1 if
%   X(i,j), Y(i,j) is a point of element j, 0 otherwise. This is useful in
%   the sequential version of the code where X and Y contain data from all
%   the elements of a mesh, which, in turn, may have different number of
%   vertices from one another.
%   BASIS is a 3D array of dimension [size(X,1) x size(X,2) x d2], where d2
%   is the dimension of the 2D monomial basis of degree k.
%
%   [BASIS, GRAD_BASIS] = MONOMIAL_BASIS(X, Y, K, XB, YB, HE, XI) evaluates
%   also the gradient of the monomial basis functions. GRAD_BASIS is an
%   array of dimension [size(X,1) x size(X,2) x d2 x 2], where the last two
%   dimensions represent derivatives with respect to x and y, respectively.

% Monomial basis functions are considered in the order given by traversing
% the Pascal triangle from top to bottom, from left to right.

% Dimension of the 2D basis
d2  = ((k+1)*(k+2))/2;

basis      = zeros([size(x) d2]);
grad_basis = zeros([size(x) d2 2]);

basis(:,:,1) = Xi;

x = bsxfun(@ldivide, hE, x - bsxfun(@times, xb, Xi));
y = bsxfun(@ldivide, hE, y - bsxfun(@times, yb, Xi));

for p = 1:k
    basis( :, :, (p*(p+1))/2+1 ) = basis( :, :, ((p-1)*p)/2+1 ) .* x;
    grad_basis( :, :, (p*(p+1))/2+1, 1 ) = ...
        grad_basis( :, :, ((p-1)*p)/2+1, 1 ) .* x + ...
        bsxfun(@times, 1./hE, basis( :, :, ((p-1)*p)/2+1 ));
    grad_basis( :, :, (p*(p+1))/2+1, 2 ) = ...
        grad_basis( :, :, ((p-1)*p)/2+1, 2 ) .* x;
    for q = 1:p
        basis( :, :, (p*(p+1))/2+1+q ) = basis( :, :, ((p-1)*p)/2+q ) .* y;
        grad_basis( :, :, (p*(p+1))/2+1+q, 1 ) = ...
            grad_basis( :, :, ((p-1)*p)/2+q, 1 ) .* y;
        grad_basis( :, :, (p*(p+1))/2+1+q, 2 ) = ...
            grad_basis( :, :, ((p-1)*p)/2+q, 2 ) .* y + ...
            bsxfun(@times, 1./hE, basis( :, :, ((p-1)*p)/2+q ));
    end
end

% basis = bsxfun(@times, Xi, basis);
% grad_basis = bsxfun(@times, Xi, grad_basis);

end






