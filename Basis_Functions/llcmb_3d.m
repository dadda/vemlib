function D = llcmb_3d(p, hE)

d3   = ((p+3)*(p+2)*(p+1))/6;
d3m2 = ((p+1)*p*(p-1))/6;
Nelt = length(hE);

hE = hE(:);
D  = zeros([d3-4 d3m2]);

m = 1;
for s = 2:p
    for q = s:-1:0
        for r = s-q:-1:0
            if (q-2 >= 0)
                col_xx = (s*(s-1)*(s-2))/6 + ((s-q+1)*(s-q))/2 + s - q - r + 1;
                D(m, col_xx) = q*(q-1);
            end
            if (r-2 >= 0)
                col_yy = (s*(s-1)*(s-2))/6 + (((s-2)-q+1)*((s-2)-q))/2 + (s-2) - q - (r-2) + 1;
                D(m, col_yy) = r*(r-1);
            end
            t = s-q-r;
            if (t-2 >= 0)
                col_zz = (s*(s-1)*(s-2))/6 + (((s-2)-q+1)*((s-2)-q))/2 + (s-2) - q - r + 1;
                D(m, col_zz) = t*(t-1);
            end
            m = m + 1;
        end
    end
end

D = kron(1./(hE.^2)', D);
D = reshape(D, [d3-4, d3m2, Nelt]);

end
