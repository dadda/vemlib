function Dn = directional_derivative(k, x, y, xb, yb, hE, a, b)

Npt  = length(x);
d2   = ((k+1)*(k+2))/2;
dimD = max( floor(k/2) + 1, 2 ); % Compute the normal derivative of order 1 at least

x = (x-xb) / hE;
y = (y-yb) / hE;

Dn = zeros(Npt, d2, dimD);

Dn(:,1,1) = 1.;

for p = 1:k
    Dn( :, (p*(p+1))/2+1, 1 ) = Dn( :, ((p-1)*p)/2+1, 1 ) .* x;

    for i = 2:dimD
        Dn( :, (p*(p+1))/2+1, i ) = Dn( :, ((p-1)*p)/2+1, i ) .* x + ...
            (i-1) * Dn( :, ((p-1)*p)/2+1, i-1 ) * (a / hE);
    end

    for q = 1:p
        Dn( :, (p*(p+1))/2+1+q, 1 ) = Dn( :, ((p-1)*p)/2+q, 1 ) .* y;

        for i = 2:dimD
            Dn( :, (p*(p+1))/2+1+q, i ) = Dn( :, ((p-1)*p)/2+q, i ) .* y + ...
                (i-1) * Dn( :, ((p-1)*p)/2+q, i-1 ) * (b / hE);
        end
    end
end

end
