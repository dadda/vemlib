% Test legendre

addpath('../')

x = linspace(0, 1, 100)';
deg = 5;

basis = legendre_on_unit_interval(x, deg);

figure()
grid on
hold on

for i = 1:deg+1
    plot(x, basis(:,i))
end

legend('deg = 0', 'deg = 1', 'deg = 2', 'deg = 3', 'deg = 4', 'deg = 5')