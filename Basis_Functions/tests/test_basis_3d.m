% Test 3d monomial basis functions routine
%
% This script requires calls to bsxfun to be commented out in
% monomial_basis_3d.m.

addpath '../Basis_Functions/.'

syms x y z

k = 1;

basis = monomial_basis_3d(x, y, z, k, nan, nan, nan, nan, 1);
basis = permute(basis, [1,3,2])

k = 2;

basis = monomial_basis_3d(x, y, z, k, nan, nan, nan, nan, 1);
basis = permute(basis, [1,3,2])

k = 3;

basis = monomial_basis_3d(x, y, z, k, nan, nan, nan, nan, 1);
basis = permute(basis, [1,3,2])

k = 4;

basis = monomial_basis_3d(x, y, z, k, nan, nan, nan, nan, 1);
basis = permute(basis, [1,3,2])