addpath ../

p = 15;
D = llcmb_3d(p, 1);

% Compute basis functions up to degree p

syms x y z

d3m2 = ((p+1)*p*(p-1))/6;
d3 = ((p+3)*(p+2)*(p+1))/6;

basis = sym(zeros([d3,1]));
laplacian = sym(zeros([d3,1]));

basis(1) = 1;
laplacian(1) = 0;

m = 2;
for s = 1:p
    basis(m) = basis( ((s+1)*s*(s-1))/6+1 ) * x;
    laplacian(m) = diff(basis(m), 'x', 2) ...
        + diff(basis(m), 'y', 2) ...
        + diff(basis(m), 'z', 2);
    
    m = m + 1;
    previous_level_offset = 1;
    for q = s-1:-1:0
        basis(m) = basis( ((s+1)*s*(s-1))/6+previous_level_offset ) * y;
        laplacian(m) = diff(basis(m), 'x', 2) ...
            + diff(basis(m), 'y', 2) ...
            + diff(basis(m), 'z', 2);

        m = m + 1;
        for r = s-q-1:-1:0
            basis(m) = basis( ((s+1)*s*(s-1))/6+previous_level_offset ) * z;
            laplacian(m) = diff(basis(m), 'x', 2) ...
                + diff(basis(m), 'y', 2) ...
                + diff(basis(m), 'z', 2);
            
            m = m + 1;
            previous_level_offset = previous_level_offset + 1;
        end
    end
end

% Select appropriate basis
higher_basis = basis(5:end);
lower_basis  = basis(1:d3m2);
laplacian = laplacian(5:end);

% Check that D*lower_basis provides the laplaciand of higher_basis
dummy = laplacian - D*lower_basis;
find(dummy ~= 0)

