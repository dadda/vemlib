%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check the ordering of monomial basis functions

addpath('./../Basis_Functions');

close all

% Monomial basis functions are considered in the order given by traversing
% the Pascal triangle from top to bottom, from left to right.

n = 100;
x = linspace(0,1,n);
y = 2*x;

k = 4;
basis = monomial_basis(x, y, k, 0, 0, 1., 1.);

figure()
plot(x,x.^4,'b',x,basis(:,:,11),'r','Linewidth',2)
title('Checking x^4')

figure()
plot(y,y.^3,'b',y,basis(:,:,10),'r','Linewidth',2)
title('Checking y^3')

figure()
[X, Y] = meshgrid(x, y);
Z = X.^2 .* Y;
surf(X,Y,Z)
hold on

basis = monomial_basis(X(:), Y(:), k, 0, 0, 1, 1);
Z2 = reshape(basis(:,:,8), [n,n]);
surf(X,Y,Z2)

title('Checking x^2*y')
