d = 2;

xp = 0.1;
yp = 0.1;
zp = 0.1;
hp = 0.001;
basis_str = 'pow((x-%g)/%g,%d) * pow((y-%g)/%g,%d) * pow((z-%g)/%g,%d)';
for s = 0:d
    for p = s:-1:0
        for q = s-p:-1:0
            r = s-p-q;
            basis_fun = sprintf(basis_str, xp, hp, p, yp, hp, q, zp, hp, r);
            disp(basis_fun);
        end
    end
end