
addpath ..
addpath ../../Quadrature

k = 4;

xq = lobatto( k+1, 0 );
xq = (xq+1)/2;
x  = linspace(0,1,1000); x = x(:);
h = 1e-10;
xh = x + h;

basis = legendre_on_unit_interval(xq, k);
basis = basis(:,end);
[ ~, gradBasis ] = legendre_on_unit_interval(x, k);
gradBasis = gradBasis(:,end);

figure();
hold on; grid on;
plot( xq, zeros(size(xq)), 'k*' );

for i = 1:k+1
    phi = -1/(k*(k+1)) * ( (1-(2*x-1).^2) .* gradBasis ) ./ (4 * (x-xq(i)) * basis(i));
    plot(x, phi)    
end

[ ~, gradBasish ] = legendre_on_unit_interval(xh, k);
gradBasish = gradBasish(:,end);

D = interpDeriv(k, 0);

figure();

for i = 1:k+1
    phi  = -1/(k*(k+1)) * ( (1-(2*x-1).^2) .* gradBasis ) ./ (4 * (x-xq(i)) * basis(i));
    phih = -1/(k*(k+1)) * ( (1-(2*xh-1).^2) .* gradBasish ) ./ (4 * (xh-xq(i)) * basis(i));
    dphi = (phih-phi)/h;
    
    plot( xq, zeros(size(xq)), 'k*' );
    hold on; grid on;
    plot( xq, D(:,i), 'go' );
    plot(x, phi, 'b')
    plot(x, dphi, 'r')
    hold off;
end
