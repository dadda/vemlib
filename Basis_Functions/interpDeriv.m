function D = interpDeriv(k, use_mp)

% Matrix of interpolation derivatives on [0,1]

% GL nodes on [-1,1]
xq1d = lobatto(k+1, use_mp);
LN   = legendre_on_unit_interval( (xq1d+1)/2, k );
LN   = LN(:,end);

D = ( LN * (1 ./ LN') ) ./ bsxfun(@minus, xq1d, xq1d');
D( 1:k+2:(k+1)*(k+1) ) = 0;
D( 1 )   = -(k+1)*k/4; 
D( end ) = (k+1)*k/4;

% Take the change of basis into account
D = 2*D;

end
