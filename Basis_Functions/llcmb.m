function D = llcmb( k, hE )
% LLCMB Laplacian as a Linear Combination of Monomial Basis functions.
%       D = LLCMB( K, HE ) compute the coefficients of the laplacian of
%       monomial basis functions of degree up to K as a linear combination
%       of the monomial functions of degree up to K-2. HE is vector (or
%       just a scalar) with elements diameters used to define the scaled
%       monomial basis.

d2   = ((k+1)*(k+2))/2;
d2m2 = ((k-1)*k)/2;
Nelt = length(hE);

hE = hE(:);
D = zeros([d2 d2m2]);

% Monomial basis functions are considered in the order given by traversing
% the Pascal triangle from top to bottom, from left to right.

for p = 2:k
    for q = 0:p
        row = (p*(p+1))/2 + 1 + q;
        col = ((p-2)*(p-1))/2 + 1 + q;
        if col <= ((p-1)*p)/2
            D(row, col)  = (p-q)*(p-q-1);
        end
        col = ((p-2)*(p-1))/2 + 1 + q - 2;
        if col > 0
            D(row, col)  = q*(q-1);
        end
    end
end

D = kron(1./(hE.^2)', D);
D = reshape(D, [d2, d2m2, Nelt]);

end
