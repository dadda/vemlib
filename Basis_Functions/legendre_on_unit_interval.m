function [ basis, gradBasis ] = legendre_on_unit_interval(x, deg)
% Legendre polynomials on [0,1]

basis     = ones(length(x), deg+1);
gradBasis = zeros(length(x), deg+1);

basis(:,2)     = 2. * x - 1.;
gradBasis(:,2) = 2.;

for i = 1:deg-1
    basis(:,i+2)     = (2.*i + 1) / (i + 1.) * (2. * x - 1.) .* basis(:,i+1) ...
                       - i/(i+1.) * basis(:,i);
	gradBasis(:,i+2) = (2.*i + 1) / (i + 1.) * (2. * basis(:,i+1) + (2.*x-1.) .* gradBasis(:,i+1) ) ...
                       - i/(i+1.) * gradBasis(:,i);
end

end
