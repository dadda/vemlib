function [ Gx, Gy ] = glcmb( k, hE )

d2   = ((k+1)*(k+2))/2;
d2m1 = (k*(k+1))/2;
Nelt = length(hE);

hE = hE(:);

Gx = zeros(d2, d2m1);
Gy = zeros(d2, d2m1);

for p = 1:k
    for q = 0:p
        row  = (p*(p+1))/2 + 1 + q;
        colX = ((p-1)*p)/2 + 1 + q;
        if colX <= (p*(p+1))/2
            Gx(row, colX) = p-q;
        end
        colY = ((p-1)*p)/2 + 1 + q - 1;
        if colY > 0
            Gy(row, colY) = q;
        end
    end
end

Gx = kron(1./hE', Gx);
Gx = reshape(Gx, [d2, d2m1, Nelt]);

Gy = kron(1./hE', Gy);
Gy = reshape(Gy, [d2, d2m1, Nelt]);

end
