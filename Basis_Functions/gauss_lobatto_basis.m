function basis = gauss_lobatto_basis(x, GLnodes)

dim   = length(GLnodes);
basis = zeros(dim, 1);

for i = 1:dim
    me       = i;
    others   = [1:me-1 me+1:dim];
    basis(i) = prod( (x - GLnodes(others)) ./ (GLnodes(me) - GLnodes(others)) );
end

end
