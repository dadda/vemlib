function [ basis, grad_basis ] = monomial_basis_3d( x, y, z, k, xb, yb, zb, hE, Xi )
% Scaled Monomial Basis from Hitchhicker paper.
%   BASIS = MONOMIAL_BASIS_3D(X, Y, Z, K, XB, YB, ZB, HE, XI) evaluates monomial
%   basis functions up to degree K at the points (X,Y,Z) given the coordinates
%   of the centroid (XB,YB,ZB) and the diameter HE of an element.
%   X, Y, and Z are coordinate matrices with as many columns as the number of
%   elements for which we want to compute basis functions. XB, YB, ZB, and HE
%   are row vectors with as many elements as the columns of X (or Y, or Z). XI is
%   an indicator array with the same size as X (or Y, or Z). XI(i,j) = 1 if
%   X(i,j), Y(i,j), Z(i,j) is a point of element j, 0 otherwise.
%   BASIS is a 3D array of dimension [size(X,1) x size(X,2) x d3], where d3
%   is the dimension of the 3d monomial basis of degree K.
%
%   [BASIS, GRAD_BASIS] = MONOMIAL_BASIS_3D(X, Y, Z, K, XB, YB, ZB, HE, XI) evaluates
%   also the gradient of the monomial basis functions. GRAD_BASIS is an
%   array of dimension [size(X,1) x size(X,2) x d3 x 3], where the last
%   three dimensions represent derivatives with respect to x, y, and z, respectively.

% Monomial basis functions are considered in the order given by traversing
% the Pascal triangle from top to bottom, from left to right.

% Dimension of the 3d basis
d3 = ((k+3)*(k+2)*(k+1))/6;

% % When testing with symbolic variables, you have to:
% % 1. uncomment the following line;
% % basis      = sym(zeros([size(x) d3]));
% % 2. comment the calls to bsxfun;
% % 3. replace .* operators with *

basis      = zeros([size(x) d3]);
grad_basis = zeros([size(x) d3 3]);

basis(:,:,1) = Xi;

x = bsxfun(@ldivide, hE, x - bsxfun(@times, xb, Xi));
y = bsxfun(@ldivide, hE, y - bsxfun(@times, yb, Xi));
z = bsxfun(@ldivide, hE, z - bsxfun(@times, zb, Xi));

m = 2;
for s = 1:k
    basis( :, :, m ) = basis( :, :, ((s+1)*s*(s-1))/6+1 ) .* x;

    % dx
    grad_basis( :, :, m, 1 ) = ...
        grad_basis( :, :, ((s+1)*s*(s-1))/6+1, 1 ) .* x + ...
        bsxfun(@times, 1./hE, basis( :, :, ((s+1)*s*(s-1))/6+1 ) );
    % dy
    grad_basis( :, :, m, 2 ) = ...
        grad_basis( :, :, ((s+1)*s*(s-1))/6+1, 2 ) .* x;
    % dz
    grad_basis( :, :, m, 3 ) = ...
        grad_basis( :, :, ((s+1)*s*(s-1))/6+1, 3 ) .* x;
    
    m = m + 1;
    previous_level_offset = 1;
    for q = s-1:-1:0
        basis( :, :, m ) = basis( :, :, ((s+1)*s*(s-1))/6+previous_level_offset ) .* y;

        % dx
        grad_basis( :, :, m, 1 ) = ...
            grad_basis( :, :, ((s+1)*s*(s-1))/6+previous_level_offset, 1 ) .* y;
        % dy
        grad_basis( :, :, m, 2 ) = ...
            grad_basis( :, :, ((s+1)*s*(s-1))/6+previous_level_offset, 2 ) .* y + ...
            bsxfun(@times, 1./hE, basis( :, :, ((s+1)*s*(s-1))/6+previous_level_offset ) );
        % dz
        grad_basis( :, :, m, 3 ) = ...
            grad_basis( :, :, ((s+1)*s*(s-1))/6+previous_level_offset, 3 ) .* y;
        
        m = m + 1;
        for r = s-q-1:-1:0
            basis( :, :, m ) = basis( :, :, ((s+1)*s*(s-1))/6+previous_level_offset ) .* z;
            
            % dx
            grad_basis( :, :, m, 1 ) = ...
                grad_basis( :, :, ((s+1)*s*(s-1))/6+previous_level_offset, 1 ) .* z;
            % dy
            grad_basis( :, :, m, 2 ) = ...
                grad_basis( :, :, ((s+1)*s*(s-1))/6+previous_level_offset, 2 ) .* z;
            % dz
            grad_basis( :, :, m, 3 ) = ...
                grad_basis( :, :, ((s+1)*s*(s-1))/6+previous_level_offset, 3 ) .* z + ...
                bsxfun(@times, 1./hE, basis( :, :, ((s+1)*s*(s-1))/6+previous_level_offset ) );
            
            m = m + 1;
            previous_level_offset = previous_level_offset + 1;
        end
    end
end

end

