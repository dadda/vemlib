#include <cstdio>
#include <cstring>
#include <iostream>
#include <ginac/ginac.h>
#include "StrumpackSparseSolver.hpp"

using namespace std;
using namespace GiNaC;

using namespace strumpack;


void get_Pk_2d( const int k,
		const symbol & x,
		const symbol & y,
		const symbol & h,
		lst & Pk )
{
  Pk.append(1);
  for (int s = 0; s < k; ++s) {
    Pk.append( Pk[ ((s+1)*s)/2 ] * x/h );
    for (int q = 0; q <= s; ++q) {
      Pk.append( Pk[ ((s+1)*s)/2 + q ] * y/h );
    }
  }
}


void get_Pk_3d( const int k,
		const symbol & x,
		const symbol & y,
		const symbol & z,
		const symbol & h,
		lst & Pk )
{
  int previous_level_offset;

  Pk.append(1);
  for (int s = 0; s < k; ++s) {
    Pk.append( Pk[ ((s+2)*(s+1)*s)/6 ] * x/h );
    previous_level_offset = 0;
    for (int q = s; q >= 0; --q) {
      Pk.append( Pk[ ((s+2)*(s+1)*s)/6 + previous_level_offset ] * y/h );
      for (int r = s-q; r >= 0; --r, ++previous_level_offset) {
	Pk.append( Pk[ ((s+2)*(s+1)*s)/6 + previous_level_offset ] * z/h );
      }
    }
  }
}


void get_Gperp_2d( const int k,
		   const symbol & x,
		   const symbol & y,
		   const symbol & h,
		   lst & GperpX,
		   lst & GperpY )
{
  if ( k >= 1 ) {
    GperpX.append(y/(h*h));
    GperpY.append(-x/(h*h));
  }
  if ( k >= 2 ) {
    GperpX.append(2*x*y/(h*h*h));
    GperpX.append(y*y/(h*h*h));

    GperpY.append(-x*x/(h*h*h));
    GperpY.append(-2*x*y/(h*h*h));
  }
  if ( k >= 3 ) {
    GperpX.append(3*x*x*y/(h*h*h*h));
    GperpX.append(2*x*y*y/(h*h*h*h));
    GperpX.append(y*y*y/(h*h*h*h));

    GperpY.append(-x*x*x/(h*h*h*h));
    GperpY.append(-2*x*x*y/(h*h*h*h));
    GperpY.append(-3*x*y*y/(h*h*h*h));
  }
  if ( k >= 4 ) {
    cout << "Error: not yet implemented" << endl;
    exit(1);
  }
}


void get_Gperp_3d( const int k,
		   const symbol & x,
		   const symbol & y,
		   const symbol & z,
		   const symbol & h,
		   lst & GperpX,
		   lst & GperpY,
		   lst & GperpZ )
{
  if ( k >= 1 ) {
    GperpX.append(y/(h*h));
    GperpX.append(z/(h*h));
    GperpX.append(0);

    GperpY.append(-x/(h*h));
    GperpY.append(0);
    GperpY.append(z/(h*h));

    GperpZ.append(0);
    GperpZ.append(-x/(h*h));
    GperpZ.append(-y/(h*h));
  }
  if ( k >= 2 ) {
    GperpX.append(2*x*y/(h*h*h));
    GperpX.append(2*x*z/(h*h*h));
    GperpX.append(y*y/(h*h*h));
    GperpX.append(y*z/(h*h*h));
    GperpX.append(y*z/(h*h*h));
    GperpX.append(z*z/(h*h*h));
    GperpX.append(0);
    GperpX.append(0);

    GperpY.append(-x*x/(h*h*h));
    GperpY.append(0);
    GperpY.append(-2*x*y/(h*h*h));
    GperpY.append(-x*z/(h*h*h));
    GperpY.append(x*z/(h*h*h));
    GperpY.append(0);
    GperpY.append(2*y*z/(h*h*h));
    GperpY.append(z*z/(h*h*h));

    GperpZ.append(0);
    GperpZ.append(-x*x/(h*h*h));
    GperpZ.append(0);
    GperpZ.append(x*y/(h*h*h));
    GperpZ.append(-x*y/(h*h*h));
    GperpZ.append(-2*x*z/(h*h*h));
    GperpZ.append(-y*y/(h*h*h));
    GperpZ.append(-2*y*z/(h*h*h));
  }
  if ( k >= 3 ) {
    GperpX.append(3*x*x*y/(h*h*h*h));
    GperpX.append(3*x*x*z/(h*h*h*h));
    GperpX.append(2*y*y*x/(h*h*h*h));
    GperpX.append(2*x*y*z/(h*h*h*h));
    GperpX.append(2*x*y*z/(h*h*h*h));
    GperpX.append(2*x*z*z/(h*h*h*h));
    GperpX.append(y*y*y/(h*h*h*h));
    GperpX.append(y*y*z/(h*h*h*h));
    GperpX.append(y*y*z/(h*h*h*h));
    GperpX.append(y*z*z/(h*h*h*h));
    GperpX.append(y*z*z/(h*h*h*h));
    GperpX.append(z*z*z/(h*h*h*h));
    GperpX.append(0);
    GperpX.append(0);
    GperpX.append(0);

    GperpY.append(-x*x*x/(h*h*h*h));
    GperpY.append(0);
    GperpY.append(-2*y*x*x/(h*h*h*h));
    GperpY.append(-z*x*x/(h*h*h*h));
    GperpY.append(z*x*x/(h*h*h*h));
    GperpY.append(0);
    GperpY.append(-3*y*y*x/(h*h*h*h));
    GperpY.append(-2*x*y*z/(h*h*h*h));
    GperpY.append(2*x*y*z/(h*h*h*h));
    GperpY.append(-x*z*z/(h*h*h*h));
    GperpY.append(x*z*z/(h*h*h*h));
    GperpY.append(0);
    GperpY.append(3*y*y*z/(h*h*h*h));
    GperpY.append(2*y*z*z/(h*h*h*h));
    GperpY.append(z*z*z/(h*h*h*h));

    GperpZ.append(0);
    GperpZ.append(-x*x*x/(h*h*h*h));
    GperpZ.append(0);
    GperpZ.append(y*x*x/(h*h*h*h));
    GperpZ.append(-x*x*y/(h*h*h*h));
    GperpZ.append(-2*z*x*x/(h*h*h*h));
    GperpZ.append(0);
    GperpZ.append(y*y*x/(h*h*h*h));
    GperpZ.append(-x*y*y/(h*h*h*h));
    GperpZ.append(2*x*y*z/(h*h*h*h));
    GperpZ.append(-2*x*y*z/(h*h*h*h));
    GperpZ.append(-3*z*z*x/(h*h*h*h));
    GperpZ.append(-y*y*y/(h*h*h*h));
    GperpZ.append(-2*y*y*z/(h*h*h*h));
    GperpZ.append(-3*y*z*z/(h*h*h*h));
  }
  if ( k >= 4 ) {
    cout << "Error: not yet implemented" << endl;
    exit(1);
  }
}


void get_G_2d( const int k,
	       const symbol & x,
	       const symbol & y,
	       const lst & Pk,
	       lst & Gx,
	       lst & Gy )
{
  int dGk = ( (k+3) * (k+2) ) / 2 - 1;

  for (int s = 0; s < dGk; ++s) {
    Gx.append( Pk[s+1].diff(x) );
    Gy.append( Pk[s+1].diff(y) );
  }
}


void get_G_3d( const int k,
	       const symbol & x,
	       const symbol & y,
	       const symbol & z,
	       const lst & Pk,
	       lst & Gx,
	       lst & Gy,
	       lst & Gz )
{
  int dGk = ( (k+4) * (k+3) * (k+2) ) / 6 - 1;

  for (int s = 0; s < dGk; ++s) {
    Gx.append( Pk[s+1].diff(x) );
    Gy.append( Pk[s+1].diff(y) );
    Gz.append( Pk[s+1].diff(z) );
  }
}


void get_laplacian_2d( const int k,
		       const symbol & x,
		       const symbol & y,
		       const lst & poly,
		       lst & lap )
{
  int dPk = ( (k+2) * (k+1) ) / 2;

  for (int s = 0; s < dPk-3; ++s) { // The laplacian of the first 3 basis functions is 0
    lap.append( poly[s+3].diff(x,2) + poly[s+3].diff(y,2) );
  }
}


void get_laplacian_3d( const int k,
		       const symbol & x,
		       const symbol & y,
		       const symbol & z,
		       const lst & poly,
		       lst & lap )
{
  int dPk = ( (k+3) * (k+2) * (k+1) ) / 6;

  for (int s = 0; s < dPk-4; ++s) { // The laplacian of the first 4 basis functions is 0
    lap.append( poly[s+4].diff(x,2) + poly[s+4].diff(y,2) + poly[s+4].diff(z,2) );
  }
}


void decompose_Pkv_2d( const int k,
		       const symbol & x,
		       const symbol & y,
		       const symbol & h,
		       const lst & poly,
		       const lst & Gx,
		       const lst & Gy,
		       const lst & GperpX,
		       const lst & GperpY,
		       const lst & Pkv,
		       const lst & vars,
		       double * const sol )
{
  int dPk     = ( (k+2) * (k+1) ) / 2;
  int dPkv    = 2*dPk;
  int dGk     = ( (k+3) * (k+2) ) / 2 - 1;
  int dGkperp = dPkv - dGk;

  ex tmp1, tmp2, tmpRHS1, tmpRHS2, tmpA1, tmpA2;

  int degX, degY;

  int nnz, rowIdx;
  int row_ptr[dPkv+1], col_ind[dPkv*dPkv];
  int *col_ind_copy = NULL;

  double rhs[dPkv];
  double val[dPkv*dPkv];
  double *val_copy = NULL;

  nnz = 0; rowIdx = 0;
  row_ptr[0] = 0;
  for (int i = 0; i < 2; ++i) {
    tmp1 = 0;
    switch (i) {
    case 0:
      for (int j = 0; j < dGk; ++j)
	tmp1 += Gx[j]*vars[j];
      for (int j = 0; j < dGkperp; ++j)
	tmp1 += GperpX[j]*vars[dGk+j];
      break;
    case 1:
      for (int j = 0; j < dGk; ++j)
	tmp1 += Gy[j]*vars[j];
      for (int j = 0; j < dGkperp; ++j)
	tmp1 += GperpY[j]*vars[dGk+j];
      break;
    }

    for (int s = 0; s < dPk; ++s, ++rowIdx) {
      degX = poly[s].degree(x);
      degY = poly[s].degree(y);
      // rhs
      tmpRHS1 = Pkv[i].coeff(x,degX);
      tmpRHS2 = tmpRHS1.coeff(y,degY);
      tmpRHS1 = tmpRHS2.subs(lst{h},lst{1});
      // coeff matrix
      tmpA1 = tmp1.coeff(x,degX);
      tmpA2 = tmpA1.coeff(y,degY);
      tmpA1 = tmpA2.subs(lst{h},lst{1});

      for (int c = 0; c < dPkv; ++c) {
	tmp2 = tmpA1.coeff(vars[c],1);
	if ( tmp2 != 0 ) {
	  val[nnz] = ex_to<numeric>(tmp2).to_double();
	  col_ind[nnz++] = c;
	}
      }
      row_ptr[rowIdx+1] = nnz;
      rhs[rowIdx] = ex_to<numeric>(tmpRHS1).to_double();
    }
  }

  StrumpackSparseSolver<double> sp(false);
  sp.options().set_rel_tol(1e-10);
  sp.options().set_gmres_restart(10);
  sp.options().enable_HSS();

  col_ind_copy = (int *) malloc (nnz * sizeof(int));
  memcpy(col_ind_copy, col_ind, nnz * sizeof(int));

  val_copy = (double *) malloc (nnz * sizeof(double));
  memcpy(val_copy, val, nnz * sizeof(double));

  sp.set_csr_matrix(dPkv, row_ptr, col_ind_copy, val_copy);
  sp.reorder();
  sp.factor();
  sp.solve(rhs, sol);

  free(col_ind_copy);
  free(val_copy);
}


void decompose_Pkv_3d( const int k,
		       const symbol & x,
		       const symbol & y,
		       const symbol & z,
		       const symbol & h,
		       const lst & poly,
		       const lst & Gx,
		       const lst & Gy,
		       const lst & Gz,
		       const lst & GperpX,
		       const lst & GperpY,
		       const lst & GperpZ,
		       const lst & Pkv,
		       const lst & vars,
		       double * const sol )
{
  int dPk     = ( (k+3) * (k+2) * (k+1) ) / 6;
  int dPkv    = 3*dPk;
  int dGk     = ( (k+4) * (k+3) * (k+2) ) / 6 - 1;
  int dGkperp = dPkv - dGk;

  ex tmp1, tmp2, tmpRHS1, tmpRHS2, tmpA1, tmpA2;

  int degX, degY, degZ;

  int nnz, rowIdx;
  int row_ptr[dPkv+1], col_ind[dPkv*dPkv];
  int *col_ind_copy = NULL;

  double rhs[dPkv];
  double val[dPkv*dPkv];
  double *val_copy = NULL;

  nnz = 0; rowIdx = 0;
  row_ptr[0] = 0;
  for (int i = 0; i < 3; ++i) {
    tmp1 = 0;
    switch (i) {
    case 0:
      for (int j = 0; j < dGk; ++j)
	tmp1 += Gx[j]*vars[j];
      for (int j = 0; j < dGkperp; ++j)
	tmp1 += GperpX[j]*vars[dGk+j];
      break;
    case 1:
      for (int j = 0; j < dGk; ++j)
	tmp1 += Gy[j]*vars[j];
      for (int j = 0; j < dGkperp; ++j)
	tmp1 += GperpY[j]*vars[dGk+j];
      break;
    case 2:
      for (int j = 0; j < dGk; ++j)
	tmp1 += Gz[j]*vars[j];
      for (int j = 0; j < dGkperp; ++j)
	tmp1 += GperpZ[j]*vars[dGk+j];
      break;
    }

    for (int s = 0; s < dPk; ++s, ++rowIdx) {
      degX = poly[s].degree(x);
      degY = poly[s].degree(y);
      degZ = poly[s].degree(z);
      // rhs
      tmpRHS1 = Pkv[i].coeff(x,degX);
      tmpRHS2 = tmpRHS1.coeff(y,degY);
      tmpRHS1 = tmpRHS2.coeff(z,degZ);
      tmpRHS2 = tmpRHS1.subs(lst{h},lst{1});
      // coeff matrix
      tmpA1 = tmp1.coeff(x,degX);
      tmpA2 = tmpA1.coeff(y,degY);
      tmpA1 = tmpA2.coeff(z,degZ);
      tmpA2 = tmpA1.subs(lst{h},lst{1});

      for (int c = 0; c < dPkv; ++c) {
	tmp2 = tmpA2.coeff(vars[c],1);
	if ( tmp2 != 0 ) {
	  val[nnz] = ex_to<numeric>(tmp2).to_double();
	  col_ind[nnz++] = c;
	}
      }
      row_ptr[rowIdx+1] = nnz;
      rhs[rowIdx] = ex_to<numeric>(tmpRHS2).to_double();
    }
  }

  StrumpackSparseSolver<double> sp(false);
  sp.options().set_rel_tol(1e-10);
  sp.options().set_gmres_restart(10);
  sp.options().enable_HSS();

  col_ind_copy = (int *) malloc (nnz * sizeof(int));
  memcpy(col_ind_copy, col_ind, nnz * sizeof(int));

  val_copy = (double *) malloc (nnz * sizeof(double));
  memcpy(val_copy, val, nnz * sizeof(double));

  sp.set_csr_matrix(dPkv, row_ptr, col_ind_copy, val_copy);
  sp.reorder();
  sp.factor();
  sp.solve(rhs, sol);

  free(col_ind_copy);
  free(val_copy);
}


void decompose_laplacian_Pk_2d( const int k )
{
  int dPk    = ( (k+2) * (k+1) ) / 2;
  int dPkm1  = ( (k+1) * k ) / 2;
  int dPkm2v = k * (k-1);

  symbol x("x"), y("y"), h("h");
  lst poly;
  lst Gx, Gy;
  lst GperpX, GperpY;
  lst laplacian;
  lst vars, tmp;
  char name[16];

  double sol[dPkm2v];

  get_Pk_2d(k, x, y, h, poly); // degree k to accomodate both gradient and laplacian computation
  get_G_2d(k-2, x, y, poly, Gx, Gy);
  get_Gperp_2d(k-2, x, y, h, GperpX, GperpY);
  get_laplacian_2d(k, x, y, poly, laplacian);

  for (int s = 0; s < dPkm2v; ++s) {
    name[0] = '\0';
    sprintf(name, "a%i", s+1);
    vars.append(symbol(name));
  }

  for (int s = 0; s < dPk-3; ++s) {
    for (int i = 0; i < 2; ++i) {
      tmp.remove_all();
      for (int j = 0; j < i; ++j)
	tmp.append(0);
      tmp.append( laplacian[s] );
      for (int j = i+1; j < 2; ++j)
	tmp.append(0);

      decompose_Pkv_2d(k-2, x, y, h, poly, Gx, Gy, GperpX, GperpY, tmp, vars, sol);
      for (int j = 0; j < dPkm2v; ++j)
	cout << "   " << sol[j] << ",";
      cout << endl;
    }
  }
}


void decompose_laplacian_Pk_3d( const int k )
{
  int dPk    = ( (k+3) * (k+2) * (k+1) ) / 6;
  int dPkm1  = ( (k+2) * (k+1) * k ) / 6;
  int dPkm2v = ( (k+1) * k * (k-1) ) / 2;

  symbol x("x"), y("y"), z("z"), h("h");
  lst poly;
  lst Gx, Gy, Gz;
  lst GperpX, GperpY, GperpZ;
  lst laplacian;
  lst vars, tmp;
  char name[16];

  double sol[dPkm2v];

  get_Pk_3d(k, x, y, z, h, poly); // degree k to accomodate both gradient and laplacian computation
  get_G_3d(k-2, x, y, z, poly, Gx, Gy, Gz);
  get_Gperp_3d(k-2, x, y, z, h, GperpX, GperpY, GperpZ);
  get_laplacian_3d(k, x, y, z, poly, laplacian);

  for (int s = 0; s < dPkm2v; ++s) {
    name[0] = '\0';
    sprintf(name, "a%i", s+1);
    vars.append(symbol(name));
  }

  for (int s = 0; s < dPk-4; ++s) {
    for (int i = 0; i < 3; ++i) {
      tmp.remove_all();
      for (int j = 0; j < i; ++j)
      	tmp.append(0);
      tmp.append( laplacian[s] );
      for (int j = i+1; j < 3; ++j)
      	tmp.append(0);

      decompose_Pkv_3d(k-2, x, y, z, h, poly, Gx, Gy, Gz, GperpX, GperpY, GperpZ, tmp, vars, sol);
      for (int j = 0; j < dPkm2v; ++j)
	cout << "   " << sol[j] << ",";
      cout << endl;
    }
  }
}


void get_laplacian_decomposition_3d( const int k,
				     const double h,
				     double **L )
{
  int dPk    = ( (k+3) * (k+2) * (k+1) ) / 6;
  int dPkm2v = ( (k+1) * k * (k-1) ) / 2;

  double buffer2[] = {
    2.0/h,   0.0,   0.0,
    0.0,   2.0/h,   0.0,
    0.0,   0.0,   2.0/h,
    0.0,   0.0,   0.0,
    0.0,   0.0,   0.0,
    0.0,   0.0,   0.0,
    0.0,   0.0,   0.0,
    0.0,   0.0,   0.0,
    0.0,   0.0,   0.0,
    2.0/h,   0.0,   0.0,
    0.0,   2.0/h,   0.0,
    0.0,   0.0,   2.0/h,
    0.0,   0.0,   0.0,
    0.0,   0.0,   0.0,
    0.0,   0.0,   0.0,
    2.0/h,   0.0,   0.0,
    0.0,   2.0/h,   0.0,
    0.0,   0.0,   2.0/h
  };

  double buffer3[] = {
    2.0/h,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,
    0.0,   2.0/h,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,
    0.0,   0.0,   2.0/h,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,
    0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,
    0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,
    0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,
    0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,
    0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,
    0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,
    2.0/h,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,
    0.0,   2.0/h,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,
    0.0,   0.0,   2.0/h,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,
    0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,
    0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,
    0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,
    2.0/h,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,
    0.0,   2.0/h,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,
    0.0,   0.0,   2.0/h,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,
    0.0,   0.0,   0.0,   3.0/h,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,
    0.0,   0.0,   0.0,   0.0,   3.0/h,   0.0,   0.0,   0.0,   0.0,   -3.0/h,   0.0,   0.0,
    0.0,   0.0,   0.0,   0.0,   0.0,   3.0/h,   0.0,   0.0,   0.0,   0.0,   -3.0/h,   0.0,
    0.0,   0.0,   0.0,   0.0,   1.0/h,   0.0,   0.0,   0.0,   0.0,   1.0/h,   0.0,   0.0,
    0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   1.0/h,   0.0,   0.0,   0.0,   0.0,   0.0,
    0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   1.0/h,   0.0,   0.0,   0.0,   -1.0/h,
    0.0,   0.0,   0.0,   0.0,   0.0,   1.0/h,   0.0,   0.0,   0.0,   0.0,   1.0/h,   0.0,
    0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   1.0/h,   0.0,   0.0,   0.0,   1.0/h,
    0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   1.0/h,   0.0,   0.0,   0.0,
    0.0,   0.0,   0.0,   1.0/h,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,
    0.0,   0.0,   0.0,   0.0,   1.0/h,   0.0,   0.0,   0.0,   0.0,   -1.0/h,   0.0,   0.0,
    0.0,   0.0,   0.0,   0.0,   0.0,   1.0/h,   0.0,   0.0,   0.0,   0.0,   -1.0/h,   0.0,
    0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,
    0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,
    0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,
    0.0,   0.0,   0.0,   1.0/h,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,
    0.0,   0.0,   0.0,   0.0,   1.0/h,   0.0,   0.0,   0.0,   0.0,   -1.0/h,   0.0,   0.0,
    0.0,   0.0,   0.0,   0.0,   0.0,   1.0/h,   0.0,   0.0,   0.0,   0.0,   -1.0/h,   0.0,
    0.0,   0.0,   0.0,   0.0,   3.0/h,   0.0,   0.0,   0.0,   0.0,   3.0/h,   0.0,   0.0,
    0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   3.0/h,   0.0,   0.0,   0.0,   0.0,   0.0,
    0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   3.0/h,   0.0,   0.0,   0.0,   -3.0/h,
    0.0,   0.0,   0.0,   0.0,   0.0,   1.0/h,   0.0,   0.0,   0.0,   0.0,   1.0/h,   0.0,
    0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   1.0/h,   0.0,   0.0,   0.0,   1.0/h,
    0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   1.0/h,   0.0,   0.0,   0.0,
    0.0,   0.0,   0.0,   0.0,   1.0/h,   0.0,   0.0,   0.0,   0.0,   1.0/h,   0.0,   0.0,
    0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   1.0/h,   0.0,   0.0,   0.0,   0.0,   0.0,
    0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   1.0/h,   0.0,   0.0,   0.0,   -1.0/h,
    0.0,   0.0,   0.0,   0.0,   0.0,   3.0/h,   0.0,   0.0,   0.0,   0.0,   3.0/h,   0.0,
    0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   3.0/h,   0.0,   0.0,   0.0,   3.0/h,
    0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   3.0/h,   0.0,   0.0,   0.0
  };

  switch(k) {
  case 2:
    memcpy(L, buffer2, 3*(dPk-4)*dPkm2v*sizeof(double));
    break;
  case 3:
    memcpy(L, buffer3, 3*(dPk-4)*dPkm2v*sizeof(double));
    break;
  }
}


int main()
{
  int k, dim, flag;

  cout << "Enter k: ";
  cin >> k;

  cout << "Enter dim: ";
  cin >> dim;

  cout << "Return coefficients of laplacian decomposition (0) or laplacian in symbolic form (1)? ";
  cin >> flag;

  switch(dim) {
  case 2:
    if ( flag == 0 ) {
      // Get coefficients of laplacian decomposition
      decompose_laplacian_Pk_2d(k);
    } else if ( flag == 1 ) {
      // Print out laplacian in symbolic form
      symbol x("x"), y("y"), h("h");
      lst Pk, lap;

      get_Pk_2d(k, x, y, h, Pk);
      get_laplacian_2d(k, x, y, Pk, lap);

      int counter = 0;
      int power;
      ex tmp;
      for (lst::const_iterator i = lap.begin(); i != lap.end(); ++i) {
	power = (*i).degree(h);
	tmp   = (*i).subs(h == 1);
	cout << "  if ( idx == " << counter++ << " ) {" << endl;
	if ( power == 0 ) {
	  cout << "    *Lx = " << tmp << ';' << endl;
	} else if ( power == -1 ) {
	  cout << "    *Lx = (" << tmp << ")/h;" << endl;
	} else {
	  cout << "    *Lx = (" << tmp << ")*pow(h," << power << ");" << endl;
	}
	cout << "    *Ly = 0.0;" << endl;
	cout << "  }" << endl;
	cout << "  if ( idx == " << counter++ << " ) {" << endl;
	cout << "    *Lx = 0.0;" << endl;
	if ( power == 0 ) {
	  cout << "    *Ly = " << tmp << ';' << endl;
	} else if ( power == -1 ) {
	  cout << "    *Ly = (" << tmp << ")/h;" << endl;
	} else {
	  cout << "    *Ly = (" << tmp << ")*pow(h," << power << ");" << endl;
	}
	cout << "  }" << endl;
      }
    }
    break;
  case 3:
    if ( flag == 0 ) {
      // Get coefficients of laplacian decomposition
      decompose_laplacian_Pk_3d(k);
    } else if ( flag == 1 ) {
      // Print out laplacian in symbolic form
      symbol x("x"), y("y"), z("z"), h("h");
      lst Pk, lap;

      get_Pk_3d(k, x, y, z, h, Pk);
      get_laplacian_3d(k, x, y, z, Pk, lap);

      int counter = 0;
      int power;
      ex tmp;
      for (lst::const_iterator i = lap.begin(); i != lap.end(); ++i) {
	power = (*i).degree(h);
	tmp   = (*i).subs(h == 1);
	cout << "  if ( idx == " << counter++ << " ) {" << endl;
	if ( power == 0 ) {
	  cout << "    *Lx = " << tmp << ';' << endl;
	} else if ( power == -1 ) {
	  cout << "    *Lx = (" << tmp << ")/h;" << endl;
	} else {
	  cout << "    *Lx = (" << tmp << ")*pow(h," << power << ");" << endl;
	}
	cout << "    *Ly = 0.0;" << endl;
	cout << "    *Lz = 0.0;" << endl;
	cout << "  }" << endl;
	cout << "  if ( idx == " << counter++ << " ) {" << endl;
	cout << "    *Lx = 0.0;" << endl;
	if ( power == 0 ) {
	  cout << "    *Ly = " << tmp << ';' << endl;
	} else if ( power == -1 ) {
	  cout << "    *Ly = (" << tmp << ")/h;" << endl;
	} else {
	  cout << "    *Ly = (" << tmp << ")*pow(h," << power << ");" << endl;
	}
	cout << "    *Lz = 0.0;" << endl;
	cout << "  }" << endl;
	cout << "  if ( idx == " << counter++ << " ) {" << endl;
	cout << "    *Lx = 0.0;" << endl;
	cout << "    *Ly = 0.0;" << endl;
	if ( power == 0 ) {
	  cout << "    *Lz = " << tmp << ';' << endl;
	} else if ( power == -1 ) {
	  cout << "    *Lz = (" << tmp << ")/h;" << endl;
	} else {
	  cout << "    *Lz = (" << tmp << ")*pow(h," << power << ");" << endl;
	}
	cout << "  }" << endl;
      }
    }
    break;
  }

  return 0;
}
