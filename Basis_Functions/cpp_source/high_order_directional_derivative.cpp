#include <iostream>
#include <ginac/ginac.h>

using namespace std;
using namespace GiNaC;

void get_Pk_2d( const int k,
		const symbol & x,
		const symbol & y,
		const symbol & h,
		lst & Pk )
{
  Pk.append(1);
  for (int s = 0; s < k; ++s) {
    Pk.append( Pk[ ((s+1)*s)/2 ] * x/h );
    for (int q = 0; q <= s; ++q) {
      Pk.append( Pk[ ((s+1)*s)/2 + q ] * y/h );
    }
  }
}

ex compute_directional_derivative( const int m,
				   const symbol & x,
				   const symbol & y,
				   const symbol & h,
				   const ex & poly,
				   const symbol & a,
				   const symbol & b )
{
  ex Dn = 0, tmp;
  for (int j = 0; j <= m; ++j) {
    tmp = expand( poly.diff(x,j) );
    tmp = expand( tmp.diff(y,m-j) );
    tmp = binomial(m, j) * pow(a,j) * pow(b, m-j) * tmp;
    Dn += expand( tmp );
  }

  return Dn;
}


int main()
{
  symbol x("x"), y("y"), h("h"), a("a"), b("b");
  ex Dn;
  lst Pk;

  int k, dPk;

  cout << "Enter k: ";
  cin >> k;

  get_Pk_2d( k, x, y, h, Pk );

  dPk = (k+1)*(k+2)/2;
  for (int i = 0; i < dPk; ++i)
    cout << Pk[i] << endl;

  cout << endl;
  for (int i = 0; i < dPk; ++i) {
    for (int j = 1; j <= (k/2); ++j) {
      Dn = compute_directional_derivative( j, x, y, h, Pk[i], a, b );
      cout << '\t' << Dn;
    }
    cout << endl;
  }

  return 0;
}
