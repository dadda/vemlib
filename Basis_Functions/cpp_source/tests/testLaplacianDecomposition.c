#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "basisFunctions.h"

void laplacian3d( const double x,
		  const double y,
		  const double z,
		  const double h,
		  const int idx,
		  double * Lx,
		  double * Ly,
		  double * Lz )
{
  if ( idx == 0 ) {
    *Lx = (2)*pow(h,-2);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 1 ) {
    *Lx = 0.0;
    *Ly = (2)*pow(h,-2);
    *Lz = 0.0;
  }
  if ( idx == 2 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (2)*pow(h,-2);
  }
  if ( idx == 3 ) {
    *Lx = 0;
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 4 ) {
    *Lx = 0.0;
    *Ly = 0;
    *Lz = 0.0;
  }
  if ( idx == 5 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = 0;
  }
  if ( idx == 6 ) {
    *Lx = 0;
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 7 ) {
    *Lx = 0.0;
    *Ly = 0;
    *Lz = 0.0;
  }
  if ( idx == 8 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = 0;
  }
  if ( idx == 9 ) {
    *Lx = (2)*pow(h,-2);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 10 ) {
    *Lx = 0.0;
    *Ly = (2)*pow(h,-2);
    *Lz = 0.0;
  }
  if ( idx == 11 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (2)*pow(h,-2);
  }
  if ( idx == 12 ) {
    *Lx = 0;
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 13 ) {
    *Lx = 0.0;
    *Ly = 0;
    *Lz = 0.0;
  }
  if ( idx == 14 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = 0;
  }
  if ( idx == 15 ) {
    *Lx = (2)*pow(h,-2);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 16 ) {
    *Lx = 0.0;
    *Ly = (2)*pow(h,-2);
    *Lz = 0.0;
  }
  if ( idx == 17 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (2)*pow(h,-2);
  }
  if ( idx == 18 ) {
    *Lx = (6*x)*pow(h,-3);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 19 ) {
    *Lx = 0.0;
    *Ly = (6*x)*pow(h,-3);
    *Lz = 0.0;
  }
  if ( idx == 20 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (6*x)*pow(h,-3);
  }
  if ( idx == 21 ) {
    *Lx = (2*y)*pow(h,-3);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 22 ) {
    *Lx = 0.0;
    *Ly = (2*y)*pow(h,-3);
    *Lz = 0.0;
  }
  if ( idx == 23 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (2*y)*pow(h,-3);
  }
  if ( idx == 24 ) {
    *Lx = (2*z)*pow(h,-3);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 25 ) {
    *Lx = 0.0;
    *Ly = (2*z)*pow(h,-3);
    *Lz = 0.0;
  }
  if ( idx == 26 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (2*z)*pow(h,-3);
  }
  if ( idx == 27 ) {
    *Lx = (2*x)*pow(h,-3);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 28 ) {
    *Lx = 0.0;
    *Ly = (2*x)*pow(h,-3);
    *Lz = 0.0;
  }
  if ( idx == 29 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (2*x)*pow(h,-3);
  }
  if ( idx == 30 ) {
    *Lx = 0;
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 31 ) {
    *Lx = 0.0;
    *Ly = 0;
    *Lz = 0.0;
  }
  if ( idx == 32 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = 0;
  }
  if ( idx == 33 ) {
    *Lx = (2*x)*pow(h,-3);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 34 ) {
    *Lx = 0.0;
    *Ly = (2*x)*pow(h,-3);
    *Lz = 0.0;
  }
  if ( idx == 35 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (2*x)*pow(h,-3);
  }
  if ( idx == 36 ) {
    *Lx = (6*y)*pow(h,-3);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 37 ) {
    *Lx = 0.0;
    *Ly = (6*y)*pow(h,-3);
    *Lz = 0.0;
  }
  if ( idx == 38 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (6*y)*pow(h,-3);
  }
  if ( idx == 39 ) {
    *Lx = (2*z)*pow(h,-3);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 40 ) {
    *Lx = 0.0;
    *Ly = (2*z)*pow(h,-3);
    *Lz = 0.0;
  }
  if ( idx == 41 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (2*z)*pow(h,-3);
  }
  if ( idx == 42 ) {
    *Lx = (2*y)*pow(h,-3);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 43 ) {
    *Lx = 0.0;
    *Ly = (2*y)*pow(h,-3);
    *Lz = 0.0;
  }
  if ( idx == 44 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (2*y)*pow(h,-3);
  }
  if ( idx == 45 ) {
    *Lx = (6*z)*pow(h,-3);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 46 ) {
    *Lx = 0.0;
    *Ly = (6*z)*pow(h,-3);
    *Lz = 0.0;
  }
  if ( idx == 47 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (6*z)*pow(h,-3);
  }
  if ( idx == 48 ) {
    *Lx = (12*pow(x,2))*pow(h,-4);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 49 ) {
    *Lx = 0.0;
    *Ly = (12*pow(x,2))*pow(h,-4);
    *Lz = 0.0;
  }
  if ( idx == 50 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (12*pow(x,2))*pow(h,-4);
  }
  if ( idx == 51 ) {
    *Lx = (6*x*y)*pow(h,-4);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 52 ) {
    *Lx = 0.0;
    *Ly = (6*x*y)*pow(h,-4);
    *Lz = 0.0;
  }
  if ( idx == 53 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (6*x*y)*pow(h,-4);
  }
  if ( idx == 54 ) {
    *Lx = (6*x*z)*pow(h,-4);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 55 ) {
    *Lx = 0.0;
    *Ly = (6*x*z)*pow(h,-4);
    *Lz = 0.0;
  }
  if ( idx == 56 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (6*x*z)*pow(h,-4);
  }
  if ( idx == 57 ) {
    *Lx = (2*pow(y,2)+2*pow(x,2))*pow(h,-4);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 58 ) {
    *Lx = 0.0;
    *Ly = (2*pow(y,2)+2*pow(x,2))*pow(h,-4);
    *Lz = 0.0;
  }
  if ( idx == 59 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (2*pow(y,2)+2*pow(x,2))*pow(h,-4);
  }
  if ( idx == 60 ) {
    *Lx = (2*y*z)*pow(h,-4);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 61 ) {
    *Lx = 0.0;
    *Ly = (2*y*z)*pow(h,-4);
    *Lz = 0.0;
  }
  if ( idx == 62 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (2*y*z)*pow(h,-4);
  }
  if ( idx == 63 ) {
    *Lx = (2*pow(x,2)+2*pow(z,2))*pow(h,-4);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 64 ) {
    *Lx = 0.0;
    *Ly = (2*pow(x,2)+2*pow(z,2))*pow(h,-4);
    *Lz = 0.0;
  }
  if ( idx == 65 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (2*pow(x,2)+2*pow(z,2))*pow(h,-4);
  }
  if ( idx == 66 ) {
    *Lx = (6*x*y)*pow(h,-4);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 67 ) {
    *Lx = 0.0;
    *Ly = (6*x*y)*pow(h,-4);
    *Lz = 0.0;
  }
  if ( idx == 68 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (6*x*y)*pow(h,-4);
  }
  if ( idx == 69 ) {
    *Lx = (2*x*z)*pow(h,-4);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 70 ) {
    *Lx = 0.0;
    *Ly = (2*x*z)*pow(h,-4);
    *Lz = 0.0;
  }
  if ( idx == 71 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (2*x*z)*pow(h,-4);
  }
  if ( idx == 72 ) {
    *Lx = (2*x*y)*pow(h,-4);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 73 ) {
    *Lx = 0.0;
    *Ly = (2*x*y)*pow(h,-4);
    *Lz = 0.0;
  }
  if ( idx == 74 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (2*x*y)*pow(h,-4);
  }
  if ( idx == 75 ) {
    *Lx = (6*x*z)*pow(h,-4);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 76 ) {
    *Lx = 0.0;
    *Ly = (6*x*z)*pow(h,-4);
    *Lz = 0.0;
  }
  if ( idx == 77 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (6*x*z)*pow(h,-4);
  }
  if ( idx == 78 ) {
    *Lx = (12*pow(y,2))*pow(h,-4);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 79 ) {
    *Lx = 0.0;
    *Ly = (12*pow(y,2))*pow(h,-4);
    *Lz = 0.0;
  }
  if ( idx == 80 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (12*pow(y,2))*pow(h,-4);
  }
  if ( idx == 81 ) {
    *Lx = (6*y*z)*pow(h,-4);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 82 ) {
    *Lx = 0.0;
    *Ly = (6*y*z)*pow(h,-4);
    *Lz = 0.0;
  }
  if ( idx == 83 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (6*y*z)*pow(h,-4);
  }
  if ( idx == 84 ) {
    *Lx = (2*pow(y,2)+2*pow(z,2))*pow(h,-4);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 85 ) {
    *Lx = 0.0;
    *Ly = (2*pow(y,2)+2*pow(z,2))*pow(h,-4);
    *Lz = 0.0;
  }
  if ( idx == 86 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (2*pow(y,2)+2*pow(z,2))*pow(h,-4);
  }
  if ( idx == 87 ) {
    *Lx = (6*y*z)*pow(h,-4);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 88 ) {
    *Lx = 0.0;
    *Ly = (6*y*z)*pow(h,-4);
    *Lz = 0.0;
  }
  if ( idx == 89 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (6*y*z)*pow(h,-4);
  }
  if ( idx == 90 ) {
    *Lx = (12*pow(z,2))*pow(h,-4);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 91 ) {
    *Lx = 0.0;
    *Ly = (12*pow(z,2))*pow(h,-4);
    *Lz = 0.0;
  }
  if ( idx == 92 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (12*pow(z,2))*pow(h,-4);
  }
  if ( idx == 93 ) {
    *Lx = (20*pow(x,3))*pow(h,-5);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 94 ) {
    *Lx = 0.0;
    *Ly = (20*pow(x,3))*pow(h,-5);
    *Lz = 0.0;
  }
  if ( idx == 95 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (20*pow(x,3))*pow(h,-5);
  }
  if ( idx == 96 ) {
    *Lx = (12*pow(x,2)*y)*pow(h,-5);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 97 ) {
    *Lx = 0.0;
    *Ly = (12*pow(x,2)*y)*pow(h,-5);
    *Lz = 0.0;
  }
  if ( idx == 98 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (12*pow(x,2)*y)*pow(h,-5);
  }
  if ( idx == 99 ) {
    *Lx = (12*pow(x,2)*z)*pow(h,-5);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 100 ) {
    *Lx = 0.0;
    *Ly = (12*pow(x,2)*z)*pow(h,-5);
    *Lz = 0.0;
  }
  if ( idx == 101 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (12*pow(x,2)*z)*pow(h,-5);
  }
  if ( idx == 102 ) {
    *Lx = (6*x*pow(y,2)+2*pow(x,3))*pow(h,-5);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 103 ) {
    *Lx = 0.0;
    *Ly = (6*x*pow(y,2)+2*pow(x,3))*pow(h,-5);
    *Lz = 0.0;
  }
  if ( idx == 104 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (6*x*pow(y,2)+2*pow(x,3))*pow(h,-5);
  }
  if ( idx == 105 ) {
    *Lx = (6*x*y*z)*pow(h,-5);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 106 ) {
    *Lx = 0.0;
    *Ly = (6*x*y*z)*pow(h,-5);
    *Lz = 0.0;
  }
  if ( idx == 107 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (6*x*y*z)*pow(h,-5);
  }
  if ( idx == 108 ) {
    *Lx = (2*pow(x,3)+6*x*pow(z,2))*pow(h,-5);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 109 ) {
    *Lx = 0.0;
    *Ly = (2*pow(x,3)+6*x*pow(z,2))*pow(h,-5);
    *Lz = 0.0;
  }
  if ( idx == 110 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (2*pow(x,3)+6*x*pow(z,2))*pow(h,-5);
  }
  if ( idx == 111 ) {
    *Lx = (2*pow(y,3)+6*pow(x,2)*y)*pow(h,-5);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 112 ) {
    *Lx = 0.0;
    *Ly = (2*pow(y,3)+6*pow(x,2)*y)*pow(h,-5);
    *Lz = 0.0;
  }
  if ( idx == 113 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (2*pow(y,3)+6*pow(x,2)*y)*pow(h,-5);
  }
  if ( idx == 114 ) {
    *Lx = (2*pow(y,2)*z+2*pow(x,2)*z)*pow(h,-5);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 115 ) {
    *Lx = 0.0;
    *Ly = (2*pow(y,2)*z+2*pow(x,2)*z)*pow(h,-5);
    *Lz = 0.0;
  }
  if ( idx == 116 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (2*pow(y,2)*z+2*pow(x,2)*z)*pow(h,-5);
  }
  if ( idx == 117 ) {
    *Lx = (2*y*pow(z,2)+2*pow(x,2)*y)*pow(h,-5);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 118 ) {
    *Lx = 0.0;
    *Ly = (2*y*pow(z,2)+2*pow(x,2)*y)*pow(h,-5);
    *Lz = 0.0;
  }
  if ( idx == 119 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (2*y*pow(z,2)+2*pow(x,2)*y)*pow(h,-5);
  }
  if ( idx == 120 ) {
    *Lx = (6*pow(x,2)*z+2*pow(z,3))*pow(h,-5);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 121 ) {
    *Lx = 0.0;
    *Ly = (6*pow(x,2)*z+2*pow(z,3))*pow(h,-5);
    *Lz = 0.0;
  }
  if ( idx == 122 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (6*pow(x,2)*z+2*pow(z,3))*pow(h,-5);
  }
  if ( idx == 123 ) {
    *Lx = (12*x*pow(y,2))*pow(h,-5);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 124 ) {
    *Lx = 0.0;
    *Ly = (12*x*pow(y,2))*pow(h,-5);
    *Lz = 0.0;
  }
  if ( idx == 125 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (12*x*pow(y,2))*pow(h,-5);
  }
  if ( idx == 126 ) {
    *Lx = (6*x*y*z)*pow(h,-5);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 127 ) {
    *Lx = 0.0;
    *Ly = (6*x*y*z)*pow(h,-5);
    *Lz = 0.0;
  }
  if ( idx == 128 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (6*x*y*z)*pow(h,-5);
  }
  if ( idx == 129 ) {
    *Lx = (2*x*pow(y,2)+2*x*pow(z,2))*pow(h,-5);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 130 ) {
    *Lx = 0.0;
    *Ly = (2*x*pow(y,2)+2*x*pow(z,2))*pow(h,-5);
    *Lz = 0.0;
  }
  if ( idx == 131 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (2*x*pow(y,2)+2*x*pow(z,2))*pow(h,-5);
  }
  if ( idx == 132 ) {
    *Lx = (6*x*y*z)*pow(h,-5);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 133 ) {
    *Lx = 0.0;
    *Ly = (6*x*y*z)*pow(h,-5);
    *Lz = 0.0;
  }
  if ( idx == 134 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (6*x*y*z)*pow(h,-5);
  }
  if ( idx == 135 ) {
    *Lx = (12*x*pow(z,2))*pow(h,-5);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 136 ) {
    *Lx = 0.0;
    *Ly = (12*x*pow(z,2))*pow(h,-5);
    *Lz = 0.0;
  }
  if ( idx == 137 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (12*x*pow(z,2))*pow(h,-5);
  }
  if ( idx == 138 ) {
    *Lx = (20*pow(y,3))*pow(h,-5);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 139 ) {
    *Lx = 0.0;
    *Ly = (20*pow(y,3))*pow(h,-5);
    *Lz = 0.0;
  }
  if ( idx == 140 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (20*pow(y,3))*pow(h,-5);
  }
  if ( idx == 141 ) {
    *Lx = (12*pow(y,2)*z)*pow(h,-5);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 142 ) {
    *Lx = 0.0;
    *Ly = (12*pow(y,2)*z)*pow(h,-5);
    *Lz = 0.0;
  }
  if ( idx == 143 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (12*pow(y,2)*z)*pow(h,-5);
  }
  if ( idx == 144 ) {
    *Lx = (2*pow(y,3)+6*y*pow(z,2))*pow(h,-5);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 145 ) {
    *Lx = 0.0;
    *Ly = (2*pow(y,3)+6*y*pow(z,2))*pow(h,-5);
    *Lz = 0.0;
  }
  if ( idx == 146 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (2*pow(y,3)+6*y*pow(z,2))*pow(h,-5);
  }
  if ( idx == 147 ) {
    *Lx = (6*pow(y,2)*z+2*pow(z,3))*pow(h,-5);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 148 ) {
    *Lx = 0.0;
    *Ly = (6*pow(y,2)*z+2*pow(z,3))*pow(h,-5);
    *Lz = 0.0;
  }
  if ( idx == 149 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (6*pow(y,2)*z+2*pow(z,3))*pow(h,-5);
  }
  if ( idx == 150 ) {
    *Lx = (12*y*pow(z,2))*pow(h,-5);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 151 ) {
    *Lx = 0.0;
    *Ly = (12*y*pow(z,2))*pow(h,-5);
    *Lz = 0.0;
  }
  if ( idx == 152 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (12*y*pow(z,2))*pow(h,-5);
  }
  if ( idx == 153 ) {
    *Lx = (20*pow(z,3))*pow(h,-5);
    *Ly = 0.0;
    *Lz = 0.0;
  }
  if ( idx == 154 ) {
    *Lx = 0.0;
    *Ly = (20*pow(z,3))*pow(h,-5);
    *Lz = 0.0;
  }
  if ( idx == 155 ) {
    *Lx = 0.0;
    *Ly = 0.0;
    *Lz = (20*pow(z,3))*pow(h,-5);
  }
}


void laplacian2d( const double x,
		  const double y,
		  const double h,
		  const int idx,
		  double * Lx,
		  double * Ly )
{
  if ( idx == 0 ) {
    *Lx = (2)*pow(h,-2);
    *Ly = 0.0;
  }
  if ( idx == 1 ) {
    *Lx = 0.0;
    *Ly = (2)*pow(h,-2);
  }
  if ( idx == 2 ) {
    *Lx = 0;
    *Ly = 0.0;
  }
  if ( idx == 3 ) {
    *Lx = 0.0;
    *Ly = 0;
  }
  if ( idx == 4 ) {
    *Lx = (2)*pow(h,-2);
    *Ly = 0.0;
  }
  if ( idx == 5 ) {
    *Lx = 0.0;
    *Ly = (2)*pow(h,-2);
  }
  if ( idx == 6 ) {
    *Lx = (6*x)*pow(h,-3);
    *Ly = 0.0;
  }
  if ( idx == 7 ) {
    *Lx = 0.0;
    *Ly = (6*x)*pow(h,-3);
  }
  if ( idx == 8 ) {
    *Lx = (2*y)*pow(h,-3);
    *Ly = 0.0;
  }
  if ( idx == 9 ) {
    *Lx = 0.0;
    *Ly = (2*y)*pow(h,-3);
  }
  if ( idx == 10 ) {
    *Lx = (2*x)*pow(h,-3);
    *Ly = 0.0;
  }
  if ( idx == 11 ) {
    *Lx = 0.0;
    *Ly = (2*x)*pow(h,-3);
  }
  if ( idx == 12 ) {
    *Lx = (6*y)*pow(h,-3);
    *Ly = 0.0;
  }
  if ( idx == 13 ) {
    *Lx = 0.0;
    *Ly = (6*y)*pow(h,-3);
  }
  if ( idx == 14 ) {
    *Lx = (12*pow(x,2))*pow(h,-4);
    *Ly = 0.0;
  }
  if ( idx == 15 ) {
    *Lx = 0.0;
    *Ly = (12*pow(x,2))*pow(h,-4);
  }
  if ( idx == 16 ) {
    *Lx = (6*x*y)*pow(h,-4);
    *Ly = 0.0;
  }
  if ( idx == 17 ) {
    *Lx = 0.0;
    *Ly = (6*x*y)*pow(h,-4);
  }
  if ( idx == 18 ) {
    *Lx = (2*pow(y,2)+2*pow(x,2))*pow(h,-4);
    *Ly = 0.0;
  }
  if ( idx == 19 ) {
    *Lx = 0.0;
    *Ly = (2*pow(y,2)+2*pow(x,2))*pow(h,-4);
  }
  if ( idx == 20 ) {
    *Lx = (6*x*y)*pow(h,-4);
    *Ly = 0.0;
  }
  if ( idx == 21 ) {
    *Lx = 0.0;
    *Ly = (6*x*y)*pow(h,-4);
  }
  if ( idx == 22 ) {
    *Lx = (12*pow(y,2))*pow(h,-4);
    *Ly = 0.0;
  }
  if ( idx == 23 ) {
    *Lx = 0.0;
    *Ly = (12*pow(y,2))*pow(h,-4);
  }
  if ( idx == 24 ) {
    *Lx = (20*pow(x,3))*pow(h,-5);
    *Ly = 0.0;
  }
  if ( idx == 25 ) {
    *Lx = 0.0;
    *Ly = (20*pow(x,3))*pow(h,-5);
  }
  if ( idx == 26 ) {
    *Lx = (12*pow(x,2)*y)*pow(h,-5);
    *Ly = 0.0;
  }
  if ( idx == 27 ) {
    *Lx = 0.0;
    *Ly = (12*pow(x,2)*y)*pow(h,-5);
  }
  if ( idx == 28 ) {
    *Lx = (6*x*pow(y,2)+2*pow(x,3))*pow(h,-5);
    *Ly = 0.0;
  }
  if ( idx == 29 ) {
    *Lx = 0.0;
    *Ly = (6*x*pow(y,2)+2*pow(x,3))*pow(h,-5);
  }
  if ( idx == 30 ) {
    *Lx = (2*pow(y,3)+6*pow(x,2)*y)*pow(h,-5);
    *Ly = 0.0;
  }
  if ( idx == 31 ) {
    *Lx = 0.0;
    *Ly = (2*pow(y,3)+6*pow(x,2)*y)*pow(h,-5);
  }
  if ( idx == 32 ) {
    *Lx = (12*x*pow(y,2))*pow(h,-5);
    *Ly = 0.0;
  }
  if ( idx == 33 ) {
    *Lx = 0.0;
    *Ly = (12*x*pow(y,2))*pow(h,-5);
  }
  if ( idx == 34 ) {
    *Lx = (20*pow(y,3))*pow(h,-5);
    *Ly = 0.0;
  }
  if ( idx == 35 ) {
    *Lx = 0.0;
    *Ly = (20*pow(y,3))*pow(h,-5);
  }
}


int main()
{
  int i, j, l;
  int k, d;
  double h;

  int dPkm2v, dGkm2, dGkm2o, dPk, dLap, N;

  printf("Enter k: ");
  scanf("%d", &k);

  printf("Enter dimension: ");
  scanf("%d", &d);

  printf("Enter number of points: ");
  scanf("%d", &N);

  if ( d == 2 ) {
    dPkm2v = k * (k-1);
    dGkm2  = ( (k+1) * k ) / 2 - 1;
    dGkm2o = dPkm2v - dGkm2;
    dPk    = ( (k+2) * (k+1) ) / 2;
    dLap   = 2*(dPk-3);

    h = sqrt(2.0);
  } else if ( d == 3 ) {
    dPkm2v = ( (k+1) * k * (k-1) ) / 2;
    dGkm2  = ( (k+2) * (k+1) * k ) / 6 - 1;
    dGkm2o = dPkm2v - dGkm2;
    dPk    = ( (k+3) * (k+2) * (k+1) ) / 6;
    dLap   = 3*(dPk-4);

    h = sqrt(3.0);
  }

  double xc = 0.5, yc = 0.5, zc = 0.5;

  double x, y, z, tmp1, tmp2, tmpx, tmpy, tmpz;
  double Lx, Ly, Lz;
  double Gx[dGkm2], Gy[dGkm2], Gz[dGkm2];
  double Gox[dGkm2o], Goy[dGkm2o], Goz[dGkm2o];
  double L[dLap*dPkm2v];

  tmp1 = 0.0;

  if ( d == 2 ) {
    get_laplacian_decomposition_2d(k, h, L);
    for (i = 0; i < N; ++i) {
      x = (double)(rand())/RAND_MAX;
      y = (double)(rand())/RAND_MAX;
      compute_Gk_2d(k-2, x, y, xc, yc, h, Gx, Gy);
      compute_Gk_orth_2d(k-2, x, y, xc, yc, h, Gox, Goy);
      for (l = 0; l < dLap; ++l) {
	laplacian2d(x-xc, y-yc, h, l, &Lx, &Ly);
	tmpx = 0.0;
	tmpy = 0.0;
	for (j = 0; j < dGkm2; ++j) {
	  tmpx += Gx[j]*L[l*dPkm2v+j];
	  tmpy += Gy[j]*L[l*dPkm2v+j];
	}
	for (j = dGkm2; j < dPkm2v; ++j) {
	  tmpx += Gox[j-dGkm2]*L[l*dPkm2v+j];
	  tmpy += Goy[j-dGkm2]*L[l*dPkm2v+j];
	}
	tmp2 = fabs(tmpx - Lx) + fabs(tmpy - Ly);
	tmp1 += tmp2;
      }
    }
  } else if ( d == 3 ) {
    get_laplacian_decomposition_3d(k, h, L);
    for (i = 0; i < N; ++i) {
      x = (double)(rand())/RAND_MAX;
      y = (double)(rand())/RAND_MAX;
      z = (double)(rand())/RAND_MAX;
      compute_Gk_3d(k-2, x, y, z, xc, yc, zc, h, Gx, Gy, Gz);
      compute_Gk_orth_3d(k-2, x, y, z, xc, yc, zc, h, Gox, Goy, Goz);
      for (l = 0; l < dLap; ++l) {
	laplacian3d(x-xc, y-yc, z-zc, h, l, &Lx, &Ly, &Lz);
	tmpx = 0.0;
	tmpy = 0.0;
	tmpz = 0.0;
	for (j = 0; j < dGkm2; ++j) {
	  tmpx += Gx[j]*L[l*dPkm2v+j];
	  tmpy += Gy[j]*L[l*dPkm2v+j];
	  tmpz += Gz[j]*L[l*dPkm2v+j];
	}
	for (j = dGkm2; j < dPkm2v; ++j) {
	  tmpx += Gox[j-dGkm2]*L[l*dPkm2v+j];
	  tmpy += Goy[j-dGkm2]*L[l*dPkm2v+j];
	  tmpz += Goz[j-dGkm2]*L[l*dPkm2v+j];
	}
	tmp2 = fabs(tmpx - Lx) + fabs(tmpy - Ly) + fabs(tmpz - Lz);
	tmp1 += tmp2;
      }
    }
  }

  printf("Error over all the points: %e\n", tmp1);

  return 0;
}
