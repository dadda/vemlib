#include "stdio.h"
#include "basisFunctions.h"

int main()
{
  int i, j, k, dPk, dPkm2, Nver;
  double *dab = NULL, *B = NULL;

  printf("Enter k: ");
  scanf("%i", &k);

  printf("Enter Nver: ");
  scanf("%i", &Nver);

  dPk   = ((k+2)*(k+1)) / 2;
  dPkm2 = (k*(k-1)) / 2;

  dab = (double *) calloc (dPk * dPkm2, sizeof(double));
  llcmb( k, 1.0, dab );

  printf("Decomposition:\n");
  for (i = 0; i < dPk; ++i) {
    for (j = 0; j < dPkm2; ++j) {
      printf("   %g", dab[dPk*j+i]);
    }
    printf("\n");
  }

  B = (double *) calloc (dPk * (k * Nver + dPkm2), sizeof(double));
  memset(B, 0.0, dPk * (k * Nver + dPkm2) * sizeof(double));
  memcpy(B+k*Nver*dPk, dab, dPk * dPkm2 * sizeof(double));

  printf("Matrix B:\n");
  for (i = 0; i < dPk; ++i) {
    for (j = 0; j < k*Nver+dPkm2; ++j) {
      printf("   %g", B[dPk*j+i]);
    }
    printf("\n");
  }

  free(dab);
  free(B);
  return 0;
}
