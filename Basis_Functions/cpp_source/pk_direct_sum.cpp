#include <iostream>
#include <ginac/ginac.h>

using namespace std;
using namespace GiNaC;

void compute_grad_poly_2d( int k )
{
  int dPk   = (k+1)*(k+2)/2;
  int dPkm1 = k*(k+1)/2;
  int dGkm1 = (k+1)*(k+2)/2-1;
  symbol x("x"), y("y"), h("h"), a("a");
  lst l;
  ex tmp, Poly;

  l.append(1);
  Poly = 1;
  for (int s = 0, m = 1; s < k; ++s) {
    tmp = l[ ((s+1)*s)/2 ] * x/h;
    l.append(tmp);
    Poly += tmp * pow(a,m);
    ++m;
    for (int q = 0; q <= s; ++q, ++m) {
      tmp = l[ ((s+1)*s)/2 + q ] * y/h;
      l.append(tmp);
      Poly += tmp * pow(a,m);
    }
  }

  cout << "Polynomial: " << Poly << endl;

  ex dxPoly = Poly.diff(x);
  ex dyPoly = Poly.diff(y);

  cout << "[],x      [],y\n" << "------------------------" << endl;
  for (int i = 1; i < dPk; ++i) {
    cout << 'a' << i << ':';
    cout << '\t' << dxPoly.coeff(a,i);
    cout << '\t' << dyPoly.coeff(a,i) << endl;
  }

  cout << "\nDimension of G^perp_[" << k-1 << "]: " << 2*dPkm1 - dGkm1 << endl;
}

void compute_grad_poly_3d( int k )
{
  int previous_level_offset;
  int dPk = (k+1)*(k+2)*(k+3)/6;
  int dPkm1 = k*(k+1)*(k+2)/6;
  int dGkm1 = (k+1)*(k+2)*(k+3)/6-1;
  symbol x("x"), y("y"), z("z"), h("h"), a("a");
  lst l;
  ex tmp, Poly;

  l.append(1);
  Poly += 1;
  for (int s = 0, m = 1; s < k; ++s) {
    tmp = l[ ((s+2)*(s+1)*s)/6 ] * x/h;
    l.append(tmp);
    Poly += tmp * pow(a,m);
    ++m;
    previous_level_offset = 0;
    for (int q = s; q >= 0; --q) {
      tmp = l[ ((s+2)*(s+1)*s)/6 + previous_level_offset ] * y/h;
      l.append(tmp);
      Poly += tmp * pow(a,m);
      ++m;
      for (int r = s-q; r >= 0; --r, ++previous_level_offset) {
	tmp = l[ ((s+2)*(s+1)*s)/6 + previous_level_offset ] * z/h;
	l.append(tmp);
	Poly += tmp * pow(a,m);
	++m;
      }
    }
  }

  // cout << "Polynomial: " << Poly << endl;

  ex dxPoly = Poly.diff(x);
  ex dyPoly = Poly.diff(y);
  ex dzPoly = Poly.diff(z);

  cout << "[],x      [],y      [],z\n" << "------------------------" << endl;
  for (int i = 1; i < dPk; ++i) {
    cout << 'a' << i << ':';
    cout << '\t' << dxPoly.coeff(a,i);
    cout << '\t' << dyPoly.coeff(a,i);
    cout << '\t' << dzPoly.coeff(a,i) << endl;
  }

  cout << "\nDimension of G^perp_[" << k-1 << "]: " << 3*dPkm1 - dGkm1 << endl;
}

int main()
{
  int k, dim;

  // symbol x("x"), y("y"), z("z"), h("h"), a("a");
  // ex Poly =
  //   a*x/h + pow(a,2)*y/h + pow(a,3)*z/h
  //   + pow(a,4)*x*x/(h*h) + pow(a,5)*x*y/(h*h) + pow(a,6)*x*z/(h*h) + pow(a,7)*y*y/(h*h) + pow(a,8)*y*z/(h*h) + pow(a,9)*z*z/(h*h);

  // ex dxPoly = Poly.diff(x);
  // ex dyPoly = Poly.diff(y);
  // ex dzPoly = Poly.diff(z);

  // cout << "Gradient, x-component: " << dxPoly << endl;
  // cout << "Gradient, y-component: " << dyPoly << endl;
  // cout << "Gradient, z-component: " << dzPoly << endl;

  cout << "Enter degree: ";
  cin >> k;

  cout << "Enter dimension: ";
  cin >> dim;

  switch(dim) {
  case 2:
    compute_grad_poly_2d(k);
    break;
  case 3:
    compute_grad_poly_3d(k);
    break;
  }

  return 0;
}
