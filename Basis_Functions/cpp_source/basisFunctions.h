#ifndef __BASIS_FUNCTIONS_H
#define __BASIS_FUNCTIONS_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void legendre01( const int k,
		 const double x,
		 double * const L );

void llcmb( const int k,
	    const double faceDiam,
	    double * const dab );

void compute_Pk_2d( const int k,
		    const double x,
		    const double y,
		    const double xc,
		    const double yc,
		    const double h,
		    double * Pk );

void compute_Pk_3d( const int k,
		    const double x,
		    const double y,
		    const double z,
		    const double xc,
		    const double yc,
		    const double zc,
		    const double h,
		    double * Pk );

void compute_Gk_2d( const int k,
		    const double x,
		    const double y,
		    const double xc,
		    const double yc,
		    const double h,
		    double * Gkx,
		    double * Gky );

void compute_Gk_3d( const int k,
		    const double x,
		    const double y,
		    const double z,
		    const double xc,
		    const double yc,
		    const double zc,
		    const double h,
		    double * Gkx,
		    double * Gky,
		    double * Gkz );

void compute_Gk_orth_2d( const int k,
			 const double x,
			 const double y,
			 const double xc,
			 const double yc,
			 const double h,
			 double * Gkox,
			 double * Gkoy );

void compute_Gk_orth_3d( const int k,
			 const double x,
			 const double y,
			 const double z,
			 const double xc,
			 const double yc,
			 const double zc,
			 const double h,
			 double * Gkox,
			 double * Gkoy,
			 double * Gkoz );

void compute_lapPk_2d( const int k,
		       const double x,
		       const double y,
		       const double xc,
		       const double yc,
		       const double h,
		       double * const L );

void get_laplacian_decomposition_2d( const int k,
				     const double h,
				     double *L );

void get_laplacian_decomposition_3d( const int k,
				     const double h,
				     double *L );

#endif
