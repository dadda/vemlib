function [mat]=intP(degree,G)
% ---------
% Computes quadrature rule on convex polygon
% ---------
% Inputs: degree of formula (not all are accepted, check quadrature.m)
% ... and matrix G (size mx2) with vertex coordinates  
% ---------
% Output: matrix with three columns, on each row one finds
% ... coordinates (x and y) of node followed by weight of node
% ---------

% choose quad rule on triangles
[NQh,xqh,yqh,wqh]=quadrature(degree);

m=size(G,1);    % number of vertezes
xx=sum(G)/m;    % average of vertexes
        
        xb = xx(1);  % mesh.polygon(ip).xb;
        yb = xx(2);  % mesh.polygon(ip).yb;
        
        NV = m;
        NE = m;
        
        NQ = NE*NQh;
        
        % nodi di quadratura totali
        
        xq = zeros(NQ,1);
        yq = zeros(NQ,1);
        wq = zeros(NQ,1);
        
        for ie=1:NE    % triangulate polygon anc cycle on triangles
            iep = ie+1;
            if iep==NE+1
                iep=1;
            end
            %
            % compute nodes on current triangle
            %
            x = zeros(3,1);
            y = zeros(3,1);
            %
            x(1) = xb;
            y(1) = yb;
            x(2) = G(ie,1);   % mesh.vertex(mesh.polygon(ip).vertices(ie)).x;
            y(2) = G(ie,2);   % mesh.vertex(mesh.polygon(ip).vertices(ie)).y;
            x(3) = G(iep,1);  % mesh.vertex(mesh.polygon(ip).vertices(iep)).x;
            y(3) = G(iep,2);  % mesh.vertex(mesh.polygon(ip).vertices(iep)).y;
            %
            % oriented area of triangle
            %
            areaT = 1/2*det([ 1    1    1
                x(1) x(2) x(3)
                y(1) y(2) y(3)]);
            %
            % vectors for triangle edges
            % e(i,:) = vector of edge i
            %
            e(1,1) = x(3)-x(2); e(1,2) = y(3)-y(2);
            e(2,1) = x(1)-x(3); e(2,2) = y(1)-y(3);
            e(3,1) = x(2)-x(1); e(3,2) = y(2)-y(1);
            %
            % trasform T : K^ ---> K
            %
            % node transform
            %
            % Jacobian matrix
            %
            J = [+e(3,1) -e(2,1)
                +e(3,2) -e(2,2)];
            %
            % nodes and weights on current triangle
            %
            tmp = [x(1)*ones(1,NQh); y(1)*ones(1,NQh)] + J * [xqh yqh]';
            %
            xq((ie-1)*NQh+1:(ie-1)*NQh+1+NQh-1) = tmp(1,:);
            yq((ie-1)*NQh+1:(ie-1)*NQh+1+NQh-1) = tmp(2,:);
            wq((ie-1)*NQh+1:(ie-1)*NQh+1+NQh-1) = wqh*2*areaT;
        end      
mat=[xq,yq,wq];
        