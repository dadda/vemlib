function [ x, w ] = lobatto ( n, use_mp )
%  LOBATTO   Lobatto Quadrature Rule.
%    [X, W] = LOBATTO(N, USE_MP) computes abscissas and weights of the
%    Gauss-Lobatto quadrature rule of order N+1 on the interval [-1,1].
%    USE_MP is a flag telling whether the Multiprecision Toolbox
%    (www.advanpix.com) should be used (USE_MP=1) or not (USE_MP=0).
%
%  Discussion:
%
%    The integral:
%
%      Integral ( -1 <= X <= 1 ) F(X) dX
%
%    The quadrature rule:
%
%      Sum ( 1 <= I <= N ) W(I) * F ( X(I) )
%
%    The quadrature rule will integrate exactly all polynomials up to
%    X^(2*N-3).
%
%    The Lobatto rule is distinguished by the fact that both endpoints
%    (-1 and 1) are always abscissas of the rule.
%
%  Licensing:
%
%    This code is distributed under the GNU LGPL license.
%
%  Modified:
%
%    09 June 2015
%    12 August 2016: added support for the Multiprecision Toolbox
%
%  Author:
%
%    Original MATLAB code by Greg von Winckel.
%    This MATLAB version by John Burkardt.
%
%  Reference:
%
%    Milton Abramowitz, Irene Stegun,
%    Handbook of Mathematical Functions,
%    National Bureau of Standards, 1964,
%    ISBN: 0-486-61272-4,
%    LC: QA47.A34.
%
%    Claudio Canuto, Yousuff Hussaini, Alfio Quarteroni, Thomas Zang,
%    Spectral Methods in Fluid Dynamics,
%    Springer, 1993,
%    ISNB13: 978-3540522058,
%    LC: QA377.S676.
%
%    Arthur Stroud, Don Secrest,
%    Gaussian Quadrature Formulas,
%    Prentice Hall, 1966,
%    LC: QA299.4G3S7.
%
%    Daniel Zwillinger, editor,
%    CRC Standard Mathematical Tables and Formulae,
%    30th Edition,
%    CRC Press, 1996,
%    ISBN: 0-8493-2479-3.

  if use_mp
    if ( n == 1 )
      x(1) = mp('-1');
      w(1) = mp('2');
      return
    end

    tolerance = 100.0 * mp('eps');
    mypi      = mp('pi');
    myone     = mp(1.0);
    mytwo     = mp(2.0);
  else
    if ( n == 1 )
      x(1) = -1.0;
      w(1) = 2.0;
      return
    end

    tolerance = 100.0 * eps;
    mypi      = pi;
    myone     = 1.0;
    mytwo     = 2.0;
  end

  x = zeros ( n, 1 );
  w = zeros ( n, 1 );

%
%  Initial estimate for the abscissas is the Chebyshev-Gauss-Lobatto nodes.
%
  x(1:n,1) = cos ( mypi * ( 0 : n - 1 ) / ( n - 1 ) )';
  xold(1:n,1) = mytwo;

  while ( tolerance < max ( abs ( x(1:n,1) - xold(1:n,1) ) ) )

    xold(1:n,1) = x(1:n,1);

    p(1:n,1) = myone;
    p(1:n,2) = x(1:n,1);

    for j = 2 : n-1
      p(1:n,j+1) = ( ( 2 * j - 1 ) * x(1:n,1) .* p(1:n,j)     ...
                   + (   - j + 1 ) *             p(1:n,j-1) ) ...
                   / (     j     );
    end

    x(1:n,1) = xold(1:n,1) - ( x(1:n,1) .* p(1:n,n) - p(1:n,n-1) ) ...
             ./ ( n * p(1:n,n) );
  end

  x(1:n) = x(n:-1:1);

  w(1:n,1) = mytwo ./ ( ( n - 1 ) * n * p(1:n,n).^2 );

  return
end
