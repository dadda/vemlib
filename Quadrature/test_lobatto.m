% Consider the approximate evaluation of the integral of
%
% f(x) = abs(x).^(alpha + 3./5.)
%
% over [−1, 1] for alpha = 0, 1, 2. The exact value of the integral is
% 10/(5*alpha+8).

close all

n = 5;
use_mp = 0;

figure()
hold on

for alpha = 0:2
    errors = zeros(5, 1);
    nnodes = errors;
    for i = 0:n-1
        nnodes(i+1) = 5*2^i;
        [x, w] = lobatto(nnodes(i+1), use_mp);
        y = abs(x).^(alpha+3./5);
        I = sum(w .* y);
        errors(i+1) = abs(I - 10./(5.*alpha+8.));
    end
    plot(nnodes, errors);
end

set(gca, 'xscale', 'log')
set(gca, 'yscale', 'log')
xlabel('Number of quadrature points')
ylabel('Quadrature error')
legend('\alpha = 0', '\alpha = 1', '\alpha = 2')