%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tests for elements_features()

addpath ../../Mesh_Handlers
addpath ../../Quadrature
addpath ../cpp_modules
addpath ../../Basis_Functions

%% Test calculation of volumes and centroids

meshfile = '../../Mesh_Handlers/meshfiles3d/voronoi/cube/cube_Nelt000051.mat';
meshfile_statistics = '../../Mesh_Handlers/meshfiles3d/voronoi/cube/cube_Nelt000051_statistics.txt';
load(meshfile)
drawOVMmesh
title('Voronoi mesh used used for tests')

elements_volumes   = zeros(mesh.Nelt,1);
elements_centroids = zeros(mesh.Nelt,3);

fprintf('Computing volumes and centroids of the Voronoi mesh...\n')
tic;
parfor E = 1:mesh.Nelt
    element = construct_element_connectivity(mesh, E);
    
    [~, ~, faces_normals_E, faces_algdist_E, ~, ~, faces_areas_E, ~, ~, faces_orders_E] = ...
        faces_features(mesh.coordinates, element, 1, 0, 0, 0);
    
    [ ~, elements_volumes(E), elements_centroids(E,:)] = ...
        elements_features( mesh.coordinates, {element}, {faces_areas_E}, ...
                           {faces_normals_E}, {faces_algdist_E}, {faces_orders_E}, -1 );
end
toc;

% Volumes and centroids computed with Voro++
fid = fopen(meshfile_statistics);
Nelt = str2double(fgetl(fid));

fromvoro = zeros(Nelt, 5);
for E = 1:Nelt
    fromvoro(E,:) = str2num(fgetl(fid)); %#ok<*ST2NM>
end
fclose(fid);

[~, isort] = sort(fromvoro(:,1));
fromvoro = fromvoro(isort,2:end);

fprintf('\nComparison between chinIntegrate4Matlab and Voro++:\n')
fprintf('---------------------------------------------------\n')
fprintf('Relative inf norm of difference for volumes: %f\n', max(abs(elements_volumes-fromvoro(:,1)))/max(abs(fromvoro(:,1))) );
fprintf('Max relative euclidean norm of difference for centroid: %f\n', max( max(abs(elements_centroids-fromvoro(:,2:end)),[],2)./max(abs(fromvoro(:,2:end)),[],2) ) );


%% Test construction of quadrature rules

% One polyhedron mesh
meshfile = '../../Mesh_Handlers/meshfiles3d/one_polyhedron/degenerate1.mat';

load(meshfile)

% Topological face index and face orientation
tfaces = floor( ( mesh.element2hfaces{1}+1 )/2 );
faces_orientation = mod(mesh.element2hfaces{1}, 2);

element = cell(mesh.Nfc, 1);
for f = 1:mesh.Nfc
    % Topological edge index and edge orientation
    tedges = floor( ( mesh.face2hedges{tfaces(f)}+1 )/2 );
    edges_orientation = mod(mesh.face2hedges{tfaces(f)}, 2);

    edges = mesh.edge2vertices(tedges,:)';
    element{f} = faces_orientation(f) * (edges_orientation .* edges(1,:) + (1-edges_orientation).*edges(2,:)) + ...
           (1-faces_orientation(f)) * ((1-edges_orientation(end:-1:1)) .* edges(1,end:-1:1) + edges_orientation(end:-1:1).*edges(2,end:-1:1));
end

[~, ~, faces_normals, faces_algdist, ~, ~, faces_areas, ~, ~, faces_orders] = ...
    faces_features(mesh.coordinates, element, 1, 0, 0, 0);

k = 6;

fprintf('\n\nGenerating a quadrature formula of order %d on a Voronoi cell...\n', k);
tic;
[ ~, element_volume, ~, element_qformula3D] = ...
        elements_features( mesh.coordinates, {element}, {faces_areas}, ...
                           {faces_normals}, {faces_algdist}, {faces_orders}, k );
toc;

xqd = element_qformula3D{1}(:,1);
yqd = element_qformula3D{1}(:,2);
zqd = element_qformula3D{1}(:,3);
wqd = element_qformula3D{1}(:,4);

drawOVMmesh
hold on
plot3(xqd, yqd, zqd, 'g*')
title('Quadrature nodes in a Voronoi cell')

% Function to integrate
gstr = 'pow(x,6) + pow(y,6) + pow(z,6) + pow(x,2)*pow(y,2)*pow(z,2) + x*pow(y,5)';

% Quadrature rule
Int = wqd'*(xqd.^6 + yqd.^6 + zqd.^6 + xqd.^2.*yqd.^2.*zqd.^2 + xqd.*yqd.^5);

% GiNaC
fprintf('\nIntegrating x^6 + y^6 + z^6 + x^2*y^2*z^2 + x*y^5 using GiNaC...\n')
tic;
Int2 = chinIntegrate4Matlab(gstr, mesh.coordinates, uint64(horzcat(element{:})), ...
                            uint64(mesh.Nfc), uint64([0; cumsum(faces_orders)]), faces_normals, faces_algdist);
toc;

% Comparison quadrature - GiNaC
fprintf('\nRelative difference quadrature rule - GiNaC: %f\n', abs(Int-Int2)/abs(Int2));



