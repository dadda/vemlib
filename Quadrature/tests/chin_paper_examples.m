% Polyhedra used for test cases in the paper by Chin et al.

addpath('../.')
addpath('../cpp_modules')
%addpath('../../AdvanpixMCT-4.0.0.11272')

% REMARK: observe that alg3 wants vertices as a d x Nver array, where d is
% the dimension and Nver is the number of vertices.

syms x y z

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2d test cases

% g = x*y;
% gstr = 'x*y';
% 
% %% Test 1
% 
% vertices = [0 0;
%             1 1;
%             0 2]';
% 
% polytope = [1 2;
%             2 3;
%             3 1];
% 
% a = [1 1 -2; -1 1 0];
% norm2a = sqrt( sum(a.^2, 1) );
% a = bsxfun(@rdivide, a, norm2a);
% b = sum(a.*vertices(:,polytope(:,1)),1);
% 
% polytope = mat2cell(polytope, [1,1,1], 2);
% polytope2 = uint64([1 2 2 3 3 1]);
% 
% % Time MATLAB Symbolic Toolbox
% tic;
% Int = alg3(g, vertices, polytope, a, b, [x;y]);
% toc;
% 
% % Time GiNaC
% tic;
% Int2 = chinIntegrate4Matlab(gstr, vertices', polytope2, uint64(3), ...
%                             uint64([0 2 4 6]), a', b);
% toc;
% 
% fprintf('Test 1. Matlab vs exact: %e\n', abs(1./3-eval(Int)) );
% fprintf('Test 1. Matlab vs GiNaC: %e\n', abs(Int2-eval(Int)) );
% 
% fprintf('\n');
% 
% %% Test 2
% 
% vertices = [0 0;
%             2 0;
%             1 1]';
% 
% polytope = [1 2;
%             2 3;
%             3 1];
% 
% a = [0 -1;
%      1  1;
%      -1 1]';
% norm2a = sqrt( sum(a.^2, 1) );
% a = bsxfun(@rdivide, a, norm2a);
% b = sum(a.*vertices(:,polytope(:,1)),1);
% 
% polytope = mat2cell(polytope, [1,1,1], 2);
% polytope2 = uint64([1 2 2 3 3 1]);
% 
% % Time MATLAB Symbolic Toolbox
% tic;
% Int = alg3(g, vertices, polytope, a, b, [x;y]);
% toc;
% 
% % Time GiNaC
% tic;
% Int2 = chinIntegrate4Matlab(gstr, vertices', polytope2, uint64(3), ...
%                             uint64([0 2 4 6]), a', b);
% toc;
% 
% fprintf('Test 2. Matlab vs exact: %e\n', abs(1./3-eval(Int)) );
% fprintf('Test 2. Matlab vs GiNaC: %e\n', abs(Int2-eval(Int)) );
% 
% fprintf('\n');

%% Test 3

g = x^2 + x*y + y^2;
gstr = 'pow(x,2) + x*y + pow(y,2)';

vertices = [1.220 -0.827;
            -1.490 -4.503;
            -3.766 -1.622;
            -4.240 -0.091;
            -3.160 4;
            -0.981 4.447;
            0.132 4.027]';
        
% Be careful: the paper gives the nodes in clockwise sense, whereas we need
% to go counterclockwise
polytope = [1 7;
            7 6;
            6 5;
            5 4;
            4 3;
            3 2;
            2 1];

% figure()
% grid on
% hold on
% for i = 1:size(polytope,1)
%     plot(vertices(1,polytope(i,:)), vertices(2,polytope(i,:)), 'b-o')
% end

% Outer normals
a = [vertices(2,polytope(:,2)) - vertices(2,polytope(:,1));
     vertices(1,polytope(:,1)) - vertices(1,polytope(:,2))];
norm2a = sqrt( sum(a.^2, 1) );
a = bsxfun(@rdivide, a, norm2a);
b = sum(a.*vertices(:,polytope(:,1)),1);

polytope  = mat2cell(polytope, ones(size(polytope,1),1), 2 );
polytope2 = uint64([1 7 7 6 6 5 5 4 4 3 3 2 2 1]);

Nfc = uint64(7);
facets_sizes = uint64(0:2:2*Nfc);

fprintf('Test (a) from Chin et al.\n')
    
% Time MATLAB Symbolic Toolbox
fprintf('MATLAB Symbolic Toolbox: ')
tic;
Int = alg3(g, vertices, polytope, a, b, [x;y]);
toc;

% Time GiNaC
fprintf('C++ module using GiNaC: ')
tic;
Int2 = chinIntegrate4Matlab(gstr, vertices', polytope2, Nfc, ...
                            facets_sizes, a', b);
toc;

% Evaluate discrepancy in double precision
fromPaper = 2031627344735367./8000000000000.;
fprintf('|Matlab - exact|: %e\n', abs(fromPaper-eval(Int)) );
fprintf('|Matlab - GiNaC|: %e\n', abs(Int2-eval(Int)) );

fprintf('\n');

% %% Test 4
% 
% vertices = [4.561 2.317;
%             1.491 -1.315;
%             -3.310 -3.164;
%             -4.845 -3.110;
%             -4.569 1.867]';
% 
% % Be careful: the paper gives the nodes in clockwise sense, whereas we need
% % to go counterclockwise
% polytope = [1 5;
%             5 4;
%             4 3;
%             3 2;
%             2 1];
% 
% figure()
% grid on
% hold on
% for i = 1:size(polytope,1)
%     plot(vertices(1,polytope(i,:)), vertices(2,polytope(i,:)), 'b-o')
% end
% 
% % Outer normals
% a = [vertices(2,polytope(:,2)) - vertices(2,polytope(:,1));
%      vertices(1,polytope(:,1)) - vertices(1,polytope(:,2))];
% norm2a = sqrt( sum(a.^2, 1) );
% a = bsxfun(@rdivide, a, norm2a);
% b = sum(a.*vertices(:,polytope(:,1)),1);
% 
% polytope = mat2cell(polytope, ones(size(polytope,1),1), 2 );
% 
% Int = alg3(g, vertices, polytope, a, b, [x;y]);
% 
% % Evaluate discrepancy in double precision
% fromPaper = 517091313866043./1600000000000.;
% fprintf('Test 4. Absolute difference in double precision: %e\n', abs(fromPaper-eval(Int)) );
% 
% %% Test 5
% 
% vertices = [-2.740 -1.888;
%             -3.292 4.233;
%             -2.723 -0.697;
%             -0.643 -3.151]';
% 
% % Be careful: the paper gives the nodes in clockwise sense, whereas we need
% % to go counterclockwise
% polytope = [1 4;
%             4 3;
%             3 2;
%             2 1];
% 
% figure()
% grid on
% hold on
% for i = 1:size(polytope,1)
%     plot(vertices(1,polytope(i,:)), vertices(2,polytope(i,:)), 'b-o')
% end
% 
% % Outer normals
% a = [vertices(2,polytope(:,2)) - vertices(2,polytope(:,1));
%      vertices(1,polytope(:,1)) - vertices(1,polytope(:,2))];
% norm2a = sqrt( sum(a.^2, 1) );
% a = bsxfun(@rdivide, a, norm2a);
% b = sum(a.*vertices(:,polytope(:,1)),1);
% 
% polytope = mat2cell(polytope, ones(size(polytope,1),1), 2 );
% 
% Int = alg3(g, vertices, polytope, a, b, [x;y]);
% 
% % Evaluate discrepancy in double precision
% fromPaper = 147449361647041./8000000000000.;
% fprintf('Test 5. Absolute difference in double precision: %e\n', abs(fromPaper-eval(Int)) );

%% Test 6

vertices = [0.211 -4.622;
            -2.684 3.851;
            0.468 4.879;
            4.630 -1.325;
            -0.411 -1.044]';

% Be careful: the paper gives the nodes in clockwise sense, whereas we need
% to go counterclockwise
polytope = [1 5;
            5 4;
            4 3;
            3 2;
            2 1];

% figure()
% grid on
% hold on
% for i = 1:size(polytope,1)
%     plot(vertices(1,polytope(i,:)), vertices(2,polytope(i,:)), 'b-o')
% end

% Outer normals
a = [vertices(2,polytope(:,2)) - vertices(2,polytope(:,1));
     vertices(1,polytope(:,1)) - vertices(1,polytope(:,2))];
norm2a = sqrt( sum(a.^2, 1) );
a = bsxfun(@rdivide, a, norm2a);
b = sum(a.*vertices(:,polytope(:,1)),1);

polytope  = mat2cell(polytope, ones(size(polytope,1),1), 2 );
polytope2 = uint64([1 5 5 4 4 3 3 2 2 1]);

Nfc = uint64(5);
facets_sizes = uint64(0:2:2*Nfc);

fprintf('Test (d) from Chin et al.\n')

% Time MATLAB Symbolic Toolbox
fprintf('MATLAB Symbolic Toolbox: ')
tic;
Int = alg3(g, vertices, polytope, a, b, [x;y]);
toc;

% Time GiNaC
fprintf('C++ module using GiNaC: ')
tic;
Int2 = chinIntegrate4Matlab(gstr, vertices', polytope2, Nfc, ...
                            facets_sizes, a', b);
toc;

% Evaluate discrepancy in double precision
fromPaper = 180742845225803./1000000000000.;
fprintf('|Matlab - exact|: %e\n', abs(fromPaper-eval(Int)) );
fprintf('|Matlab - GiNaC|: %e\n', abs(Int2-eval(Int)) );

fprintf('\n');

% %% Test 7
% 
% vertices = [-4.165 -0.832;
%             -3.668 1.568;
%             -3.266 1.279;
%             -1.090 -2.080;
%             3.313 -0.683;
%             3.033 -4.845;
%             -4.395 4.840;
%             -1.007 -3.328]';
% 
% % Be careful: the paper gives the nodes in clockwise sense, whereas we need
% % to go counterclockwise
% polytope = [1 8;
%             8 7;
%             7 6;  
%             6 5;
%             5 4;
%             4 3;
%             3 2;
%             2 1];
% 
% figure()
% grid on
% hold on
% for i = 1:size(polytope,1)
%     plot(vertices(1,polytope(i,:)), vertices(2,polytope(i,:)), 'b-o')
% end
% 
% % Outer normals
% a = [vertices(2,polytope(:,2)) - vertices(2,polytope(:,1));
%      vertices(1,polytope(:,1)) - vertices(1,polytope(:,2))];
% norm2a = sqrt( sum(a.^2, 1) );
% a = bsxfun(@rdivide, a, norm2a);
% b = sum(a.*vertices(:,polytope(:,1)),1);
% 
% polytope = mat2cell(polytope, ones(size(polytope,1),1), 2 );
% 
% Int = alg3(g, vertices, polytope, a, b, [x;y]);
% 
% % Evaluate discrepancy in double precision
% fromPaper = 1633405224899363./24000000000000.;
% fprintf('Test 7. Absolute difference in double precision: %e\n', abs(fromPaper-eval(Int)) );

%% Test 8

vertices = [-3.018 -4.473;
            -0.103 2.378;
            -1.605 -2.308;
            4.516 -0.771;
            4.203 0.478]';

% Be careful: the paper gives the nodes in clockwise sense, whereas we need
% to go counterclockwise
polytope = [1 5;
            5 4;
            4 3;
            3 2;
            2 1];

% figure()
% grid on
% hold on
% for i = 1:size(polytope,1)
%     plot(vertices(1,polytope(i,:)), vertices(2,polytope(i,:)), 'b-o')
% end

% Outer normals
a = [vertices(2,polytope(:,2)) - vertices(2,polytope(:,1));
     vertices(1,polytope(:,1)) - vertices(1,polytope(:,2))];
norm2a = sqrt( sum(a.^2, 1) );
a = bsxfun(@rdivide, a, norm2a);
b = sum(a.*vertices(:,polytope(:,1)),1);

polytope = mat2cell(polytope, ones(size(polytope,1),1), 2 );
polytope2 = uint64([1 5 5 4 4 3 3 2 2 1]);

Nfc = uint64(5);
facets_sizes = uint64(0:2:2*Nfc);

fprintf('Test (f) from Chin et al.\n')

% Time MATLAB Symbolic Toolbox
fprintf('MATLAB Symbolic Toolbox: ')
tic;
Int = alg3(g, vertices, polytope, a, b, [x;y]);
toc;

% Time GiNaC
fprintf('C++ module using GiNaC: ')
tic;
Int2 = chinIntegrate4Matlab(gstr, vertices', polytope2, Nfc, ...
                            facets_sizes, a', b);
toc;

% Evaluate discrepancy in double precision
fromPaper = 88161333955921./3000000000000.;
fprintf('|Matlab - exact|: %e\n', abs(fromPaper-eval(Int)) );
fprintf('|Matlab - GiNaC|: %e\n', abs(Int2-eval(Int)) );

fprintf('\n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 3d test cases

close all

g = x^2 + x*y + y^2 + z^2;
gstr = 'pow(x,2) + x*y + pow(y,2) + pow(z,2)';

%% Test 9

vertices = [0 0 0;
            5 0 0;
            5 5 0;
            0 5 0;
            0 0 5;
            5 0 5;
            5 5 5;
            0 5 5]';

Nfc = uint64(6);
polytope = cell(Nfc,1);
polytope{1} = [1 4 3 2];
polytope{2} = [1 2 6 5];
polytope{3} = [2 3 7 6];
polytope{4} = [3 4 8 7];
polytope{5} = [4 1 5 8];
polytope{6} = [5 6 7 8];
polytope2 = uint64(horzcat(polytope{:}));

facets_sizes = [4 4 4 4 4 4];
facets_sizes = uint64([0 cumsum(facets_sizes)]);

% figure()
% grid on
% hold on
% for i = 1:size(polytope,1)
%     l = length(polytope{i});
%     iterator = [2:l 1];
%     for j = 1:l
%         plot3(vertices(1,polytope{i}([j iterator(j)])), ...
%               vertices(2,polytope{i}([j iterator(j)])), ...
%               vertices(3,polytope{i}([j iterator(j)])), 'b-o')
%     end
% end

a = zeros(3, Nfc);
b = zeros(1, Nfc);
for i = 1:Nfc
    a(1,i) = ( vertices(2,polytope{i}(2)) - vertices(2,polytope{i}(1)) ) * ...
             ( vertices(3,polytope{i}(3)) - vertices(3,polytope{i}(1)) ) ...
           - ( vertices(3,polytope{i}(2)) - vertices(3,polytope{i}(1)) ) * ...
             ( vertices(2,polytope{i}(3)) - vertices(2,polytope{i}(1)) );
    a(2,i) = ( vertices(3,polytope{i}(2)) - vertices(3,polytope{i}(1)) ) * ...
             ( vertices(1,polytope{i}(3)) - vertices(1,polytope{i}(1)) ) ...
           - ( vertices(1,polytope{i}(2)) - vertices(1,polytope{i}(1)) ) * ...
             ( vertices(3,polytope{i}(3)) - vertices(3,polytope{i}(1)) );
    a(3,i) = ( vertices(1,polytope{i}(2)) - vertices(1,polytope{i}(1)) ) * ...
             ( vertices(2,polytope{i}(3)) - vertices(2,polytope{i}(1)) ) ...
           - ( vertices(2,polytope{i}(2)) - vertices(2,polytope{i}(1)) ) * ...
             ( vertices(1,polytope{i}(3)) - vertices(1,polytope{i}(1)) );
    a(:,i) = a(:,i)./norm(a(:,i));
    
    % b = normal dot x0, where x0 is ANY point on the plane
    b(i) = a(:,i)'*vertices(:,polytope{i}(1));
end

fprintf('Test (g) from Chin et al.\n')

% Time MATLAB Symbolic Toolbox
fprintf('MATLAB Symbolic Toolbox: ')
tic;
Int = alg3( g, vertices, polytope, a, b, [x;y;z] );
toc;

% Time GiNaC
fprintf('C++ module using GiNaC: ')
tic;
Int2 = chinIntegrate4Matlab(gstr, vertices', polytope2, Nfc, ...
                            facets_sizes, a', b);
toc;

fromPaper = 15625./4.;
fprintf('|Matlab - exact|: %e\n', abs(fromPaper-eval(Int)) );
fprintf('|Matlab - GiNaC|: %e\n', abs(Int2-eval(Int)) );

fprintf('\n');

%% Test 10

vertices = [0 0 0;
            5 0 0;
            5 4 0;
            3 2 0;
            3 5 0;
            0 5 0;
            0 0 5;
            5 0 5;
            5 4 5;
            3 2 5;
            3 5 5;
            0 5 5]';

Nfc = uint64(8);

polytope = cell(Nfc,1);
polytope{1} = [1 6 5 4 3 2];
polytope{2} = [1 2 8 7];
polytope{3} = [2 3 9 8];
polytope{4} = [3 4 10 9];
polytope{5} = [4 5 11 10];
polytope{6} = [5 6 12 11];
polytope{7} = [6 1 7 12];
polytope{8} = [7 8 9 10 11 12];
polytope2 = uint64(horzcat(polytope{:}));

% figure()
% grid on
% hold on
% for i = 1:size(polytope,1)
%     l = length(polytope{i});
%     iterator = [2:l 1];
%     for j = 1:l
%         plot3(vertices(1,polytope{i}([j iterator(j)])), ...
%               vertices(2,polytope{i}([j iterator(j)])), ...
%               vertices(3,polytope{i}([j iterator(j)])), 'b-o')
%     end
% end

facets_sizes = [6 4 4 4 4 4 4 6];
facets_sizes = uint64([0 cumsum(facets_sizes)]);

a = [0  0  1 -sqrt(2)/2 1 0 -1 0;
         0 -1  0  sqrt(2)/2 0 1  0 0;
        -1  0  0               0 0 0  0 1];
b = [0 0 5 -sqrt(2)/2 3 5 0 5];

fprintf('Test (h) from Chin et al.\n')

% Time MATLAB Symbolic Toolbox
fprintf('MATLAB Symbolic Toolbox: ')
tic;
Int = alg3(g, vertices, polytope, a, b, [x;y;z]);
toc;

% Time GiNaC
fprintf('C++ module using GiNaC: ')
tic;
Int2 = chinIntegrate4Matlab(gstr, vertices', polytope2, Nfc, ...
                            facets_sizes, a', b);
toc;

fromPaper = 33835./12.;
fprintf('|Matlab - exact|: %e\n', abs(fromPaper-eval(Int)) );
fprintf('|Matlab - GiNaC|: %e\n', abs(Int2-eval(Int)) );

fprintf('\n');

%% Test 11

vertices = [0 0 0;
            1 0 0;
            0 1 0;
            0 0 1;
            0.25 0.25 0.25]';

Nfc = uint64(6);

polytope = cell(6,1);
polytope{1} = [1 3 2];
polytope{2} = [1 2 4];
polytope{3} = [1 4 3];
polytope{4} = [5 3 4];
polytope{5} = [2 5 4];
polytope{6} = [2 3 5];
polytope2 = uint64(horzcat(polytope{:}));

% figure()
% grid on
% hold on
% for i = 1:size(polytope,1)
%     l = length(polytope{i});
%     iterator = [2:l 1];
%     for j = 1:l
%         plot3(vertices(1,polytope{i}([j iterator(j)])), ...
%               vertices(2,polytope{i}([j iterator(j)])), ...
%               vertices(3,polytope{i}([j iterator(j)])), 'b-o')
%     end
% end

facets_sizes = [3 3 3 3 3 3];
facets_sizes = uint64([0 cumsum(facets_sizes)]);

abar = sqrt(2/3);
a = [0 0 -1 abar abar/2 abar/2;
         0 -1 0 abar/2 abar abar/2;
         -1 0 0 abar/2 abar/2 abar];
b = [0 0 0 abar/2 abar/2 abar/2];

fprintf('Test (i) from Chin et al.\n')

% Time MATLAB Symbolic Toolbox
fprintf('MATLAB Symbolic Toolbox: ')
tic;
Int = alg3(g, vertices, polytope, a, b, [x;y;z]);
toc;

% Time GiNaC
fprintf('C++ module using GiNaC: ')
tic;
Int2 = chinIntegrate4Matlab(gstr, vertices', polytope2, Nfc, ...
                            facets_sizes, a', b);
toc;

fromPaper = 37./960.;
fprintf('|Matlab - exact|: %e\n', abs(fromPaper-eval(Int)) );
fprintf('|Matlab - GiNaC|: %e\n', abs(Int2-eval(Int)) );

fprintf('\n');










