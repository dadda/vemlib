function xyw = polygon_cubature_triangulation_dp(polygon_vertices,degree)
%--------------------------------------------------------------------------
% OBJECT:
%--------------------------------------------------------------------------
% This routine computes a quadrature rule over a polygon via triangulation,
% with a prescribed degree of precision.
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
% INPUT:
%--------------------------------------------------------------------------
%
% polygon_vertices: Polygon vertices written in cartesian coordinates,
%                   counterclockwise.
%                   It is a N x 2 matrix, where N is the number of nodes.
%
% degree          : degree of precision of the cubature rule.
%
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
% OUTPUT:
%--------------------------------------------------------------------------
%
% xyw             : Cubature rule \sum_{k=1}^M w_k f(x_k,y_k) stored as
%                   M x 3 matrix, in which the k-th row is [x_k y_k w_k].
% polygon_vertices_closed: Polygon vertices written in cartesian
%                   coordinates, counterclockwise.
%                   It is a N x 2 matrix, where N is the number of nodes.
%                   First and last row are equal.
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
% ROUTINES CALLED:
%--------------------------------------------------------------------------
% 1. minimal_triangulation
% 2. stroud_conical_rules
% 3. polygauss_2014
%--------------------------------------------------------------------------

N=length(polygon_vertices(:,1)); % NUMBER OF VERTICES.

% COMPUTING TRIANGULATION
XV=polygon_vertices(:,1); YV=polygon_vertices(:,2);
[TRI,err]=minimal_triangulation(XV,YV);
N_subtriangles=size(TRI,1);

% DETERMINING CUBATURE RULE.
xyw=[];
for ii=1:N_subtriangles
    TRI_ii=TRI(ii,:); TRI_ii=TRI_ii';
    XV_ii=XV(TRI_ii); YV_ii=YV(TRI_ii);
    xyw_ii=stroud_conical_rules(degree,[XV_ii YV_ii]);% VERTICES MUST NOT
    % BE REPEATED.
    xyw=[xyw; xyw_ii];
    
end
xyw = xyw(xyw(:,3) > 1e-10, :);

function [tri,err]=minimal_triangulation(x,y)

%--------------------------------------------------------------------------
% OBJECT.
%----------
%
% COMPUTE MINIMAL TRIANGULATION (BASIC ALGORITHM) ON SIMPLE DOMAINS,
% I.E. WITHOUT SELF-INTERSECTIONS.
%
%--------------------------------------------------------------------------
% INPUT.
%----------
%
% x,y: VERTICES OF THE POLYGONS. THE FIRST VERTEX IS NOT REPEATED.
%      THEY MUST BE COLUMN VECTORS.
%
%--------------------------------------------------------------------------
% OUTPUT.
%----------
%
% tri: MATRIX IN WHICH EACH ROW IS FORMED BY 3 POSITIVE INTEGERS IN WHICH
%      EACH ONE REPRESENTS A POINTER TO A VERTEX. EXAMPLE: [1 2 3; 1 3 4]
%      DESCRIBE TWO TRIANGLES T1=[x(1) y(1); x(2) y(2); x(3) y(3)] AND
%      T2=[x(1) y(1); x(3) y(3); x(4) y(4)].
%
%--------------------------------------------------------------------------
% FUNCTIONS USED IN THIS ROUTINE.
%---------------------------------
%
% 1. find_ear (USED BY "minimal_triangulation")
% 3. polyarea (MATLAB BUILT_IN FUNCTION, USED BY "minimal_triangulation").
% 4. setdiff (MATLAB BUILT_IN FUNCTION, USED BY "minimal_triangulation").
% 6. verLessThan (MATLAB BUILT_IN FUNCTION, USED BY "find_ear").
% 7. inpolygon (MATLAB BUILT_IN FUNCTION, USED BY "find_ear").
%
% THE FUNCTION "find_ear" IS ATTACHED TO THIS FILE.
%
% THE FUNCTIONS "polyarea", "setdiff", "verLessThan", "inpolygon" ARE
% MATLAB BUILT-IN.
%
%--------------------------------------------------------------------------
% TEST.
%-------
%
% THIS ROUTINE HAS BEEN TESTED IN MATLAB 7.6.0.324 (R2008a).
%
%--------------------------------------------------------------------------
% REFERENCES.
%-------------
%
% [1] Federica Basaglia, Un nuovo metodo di cubatura su poligoni (in
%     italian)
%
% [2] Chazelle and J. Incerpi, Triangulation and shape complexity,
%     ACM Trans. on Graphics, vol. 3, pp. 135-152, 1984.
%
% [3] B. Chazelle, Triangulating a simple polygon in linear time,
%     Discrete Comput. Geom., vol. 6, pp. 485-524, 1991.
%
% [4] David Eberly, Triangulation by Ear Clipping,
%           http://www.geometrictools.com/
%
% [5] R. Seidel, A simple and fast incremental randomized algorithm for
%     computing trapezoidal decompositions and for triangulating polygons,
%     Computational Geometry: Theory and Applications, vol. 1, no.1,
%     pp. 51-64, 1991.
%
% [6] Subhash Suri, Polygon Triangulation
%
% [7] http://cgm.cs.mcgill.ca/~godfried/teaching/cg-projects/97/
%            Ian/cutting_ears.html
%
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
%% Copyright (C) 2007-2009 Alvise Sommariva, Marco Vianello.
%%
%% This program is free software; you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 2 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program; if not, write to the Free Software
%% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
%%
%% Author:  Federica Basaglia
%%          Alvise Sommariva <alvise@euler.math.unipd.it>
%%          Marco Vianello   <marcov@euler.math.unipd.it>
%%
%% Date: November 04, 2010
%--------------------------------------------------------------------------

err=0;


pts=[x y];
number_pts=length(x);

% if norm(pts(1,:)-pts(end,:)) == 0
%     fprintf('\n \t reps');
%     x=x(1:end-1,:); y=y(1:end-1,:);
%     pts=[x y];
%     number_pts=length(x);
% end


pts_remaining=pts;
pointer_pts_remaining=(1:number_pts);
tri=[];

while length(pts_remaining(:,1)) > 3
    
    [tri_loc,err]=find_ear(pts_remaining);
    
    if (err == 1)
        tri=[];
        fprintf('\n \t [WARNING]: THE FUNCTION IS NOT ABLE');
        fprintf(' TO COMPUTE THE TRIANGULATION');
        return;
    end
    
    tri_add=pointer_pts_remaining(tri_loc);
    
    tri=[tri; tri_add];
    pointer_pt_remove=tri_add(2);
    pointer_pts_remaining=setdiff(pointer_pts_remaining,pointer_pt_remove);
    pts_remaining=pts(pointer_pts_remaining,:);
    
end

tri=[tri; pointer_pts_remaining];

% CUTTING DEGENERATE TRIANGLES.
vv=[1 number_pts+1];
index=[];
for ii=1:size(tri,1)
    
    ints=intersect(tri(ii,:),vv);
    if length(ints) <= 1
        index=[index; ii];
    end
end

tri=tri(index,:);




%--------------------------------
% find ear.
%--------------------------------
function [tri,err]=find_ear(vertices_pts)

%--------------------------------------------------------------------------
% OBJECT.
%----------
%
% FIND ONE EAR IN A POLYGON DESCRIBED BY THE ORDER SEQUENCE vertices_pts,
% BY A BASIC ALGORITHM, TO COMPUTE A MINIMAL TRIANGULATION (SEE CHAZELLE -
% SEIDEL ALGORITHM FOR A FASTER IMPLEMENTATION). IF THE ROUTINE IS NOT
% COMPLETED, THEN err=1.
%
% PS. WE USED A BASIC TRIANGULATION ALGORITHM FOR THIS PURPOSE. A FASTER
% ONE HAS BEEN STUDIED BY CHAZELLE AND SEIDEL. OUR ALGORITHM WORKS IN
% POLYGONS THAT ARE "SIMPLE", I.E. WITHOUT SELF-INTERSECTIONS. SEE THE
% REFERENCES ABOVE FOR MORE DETAILS.
%
%--------------------------------------------------------------------------
% INPUTS.
%----------
%
% vertices_pts: vertices of the polygon. Two column matrix.
%
%--------------------------------------------------------------------------
% OUTPUT.
%----------
%
% tri: vertices of the triangle that is "ear".
%
% err: "0" means "no warning", "1" means "the ear has not been found".
%
%--------------------------------------------------------------------------

number_pts=size(vertices_pts,1);
vertices_index=[ (1:number_pts)'; 1];
pts_remaining=vertices_pts;
curr_starting_index=1;
ear_found=0;
err=0;

while (ear_found == 0) && (curr_starting_index < number_pts)
    
    trial_triangle_vertices_pointer=...
        vertices_index(curr_starting_index:curr_starting_index+2);
    
    % TRIANGLE TO TEST.
    pts_trial_triangle=vertices_pts(trial_triangle_vertices_pointer,:);
    
    pts_remaining_pointer=...
        setdiff(1:number_pts,curr_starting_index:curr_starting_index+2);
    
    pts_remaining=vertices_pts(pts_remaining_pointer,:);
    
    xv=[vertices_pts(trial_triangle_vertices_pointer,1); ...
        vertices_pts(curr_starting_index,1)];
    yv=[vertices_pts(trial_triangle_vertices_pointer,2); ...
        vertices_pts(curr_starting_index,2)];
    
    % CHECKING POSITION OF BARYCENTER (TO SEE IF IT IS AN EAR).
    barycenter_triangle=...
        [sum(vertices_pts(trial_triangle_vertices_pointer,1)) ...
        sum(vertices_pts(trial_triangle_vertices_pointer,2))]/3;
    
    xt=pts_remaining(:,1);
    yt=pts_remaining(:,2);
    
    in = inpolygon(xt,yt,xv,yv);
    
    barycenter_triangle=[sum(vertices_pts...
        (trial_triangle_vertices_pointer,1)) ...
        sum(vertices_pts(trial_triangle_vertices_pointer,2))]/3;
    
    
    % inpolygon has been modified by MATLAB. We try to patch this problem.
    
    local_version=verLessThan('matlab', '7.6.1');
    
    if local_version == 0
        
        %******************************************************************
        %
        % IN = inpolygon(X,Y,xv,yv) returns a matrix IN the
        %      same size as X and Y.
        %      Each element of IN is assigned the value 1 or 0 depending on
        %      whether the point (X(p,q),Y(p,q)) is inside the polygonal
        %     region whose vertices are specified by the vectors xv and yv.
        %
        % In particular:
        %
        % IN(p,q) = 1: if (X(p,q),Y(p,q)) is inside the polygonal region
        %              or on the polygon boundary
        % IN(p,q) = 0: if (X(p,q),Y(p,q)) is outside the polygonal region
        %
        % [IN ON] = inpolygon(X,Y,xv,yv) returns a second matrix ON the
        %           same size as X and Y.
        %           Each element of ON is assigned the value 1 or 0
        %           depending on whether the point (X(p,q),Y(p,q)) is on
        %           the boundary of the polygonal region whose vertices are
        %           specified by the vectors xv and yv.
        %
        % In particular:
        %
        % ON(p,q) = 1: if (X(p,q),Y(p,q)) is on the polygon boundary
        % ON(p,q) = 0: if (X(p,q),Y(p,q)) is inside or outside the polygon
        %              boundary.
        %
        %******************************************************************
        
        % Working in Matlab 7.6.
        [bar_in,bar_on]= inpolygon(barycenter_triangle(1),...
            barycenter_triangle(2),vertices_pts(vertices_index,1),...
            vertices_pts(vertices_index,2));
        
        if (sum(in(1:end-1)) == 0) & (bar_in == 1) & (bar_on == 0)
            ear_found=1;
            tri=curr_starting_index:curr_starting_index+2;
            return;
        end
        
    else
        
        %******************************************************************
        %
        % IN = INPOLYGON(X, Y, XV, YV) returns a matrix IN the size of
        % X and Y.  IN(p,q) = 1 if the point (X(p,q), Y(p,q)) is
        % strictly inside the polygonal region whose vertices are
        % specified by the vectors XV and YV;  IN(p,q) is 0.5 if
        % the point is on the polygon; otherwise IN(p,q) = 0.
        %
        %******************************************************************
        
        % Working in Matlab 6.1.
        [bar_in]= inpolygon(barycenter_triangle(1),...
            barycenter_triangle(2),vertices_pts(vertices_index,1),...
            vertices_pts(vertices_index,2));
        
        if (sum(in(1:end-1)) == 0) & (bar_in > 0.5)
            ear_found=1;
            tri=curr_starting_index:curr_starting_index+2;
            return;
        end
        
    end
    
    curr_starting_index=curr_starting_index+1;
end

err=1;


function [xw]=stroud_conical_rules(ade,vertices)

% INPUT:
% ade: ALGEBRAIC DEGREE OF EXACTNESS.
% vertices: 3 x 2 MATRIX OF VERTICES OF THE SIMPLEX.

% OUTPUT:
% xw: NODES AND WEIGHTS OF STROUD CONICAL RULE TYPE OF ADE ade ON THE SIMPLEX
%     WITH VERTICES vertices.


if nargin < 2 % SEE LYNESS, COOLS,
    % "A survey on numerical cubature over triangles", p.4.
    vertices=[0 0; 1 0; 1 1];
end

[xw]=stroud_conical_rules_ref(ade+1);
bar_coord=[1-xw(:,1) xw(:,1)-xw(:,2) xw(:,2)];
xy=bar_coord*vertices;

A = polyarea(vertices(:,1),vertices(:,2));

w=xw(:,3)*A*2;
xw=[xy w];



function [xw]=stroud_conical_rules_ref(ade)

% SEE LYNESS, COOLS, "A survey on numerical cubature over triangles", p.4.

N=ceil((ade+1)/2);

[t,T]=gauss_jacobi(N,0,0);
t=(t+1)/2; T=T/2;

[x,w]=gauss_jacobi(N,0,1);
x=(x+1)/2; X=w/4;

[wx,wy]=meshgrid(T,X);
ww=wx.*wy;

[yt,xx]=meshgrid(x,x);

[yt,yx]=meshgrid(t,x);
yy=yt.*yx;

xw=[xx(:) yy(:) ww(:)];






function [x,w]=gauss_jacobi(N,a,b,gl)

% GAUSS-JACOBI (LOBATTO) RULE ON (-1,1).
% N IS ...
% a,b ARE THE GAUSS-JACOBI EXPONENTS.
% gl: 0: GAUSS POINTS. 1: GAUSS-LOBATTO POINTS.
% x, w ARE COLUMN VECTORS OF NODES AND WEIGHTS.
%      THE LENGTH OF x AND w IS "N" IF gl=0, "N+2" IF "gl=1".

if nargin < 4
    gl = 0;
end

if gl == 0
    ab=r_jacobi(N,a,b);
    xw=gauss(N,ab);
else
    xw=lobatto_jacobi(N,a,b);
end

x=xw(:,1);
w=xw(:,2);




%--------------------------------------------------------------------------
% ADDITIONAL FUNCTIONS BY D.LAURIE AND W.GAUTSCHI.
%--------------------------------------------------------------------------

function ab=r_jacobi(N,a,b)

nu=(b-a)/(a+b+2);
mu=2^(a+b+1)*gamma(a+1)*gamma(b+1)/gamma(a+b+2);
if N==1
    ab=[nu mu]; return
end

N=N-1;
n=1:N;
nab=2*n+a+b;
nuadd=(b^2-a^2)*ones(1,N)./(nab.*(nab+2));
A=[nu nuadd];
n=2:N;
nab=nab(n);
B1=4*(a+1)*(b+1)/((a+b+2)^2*(a+b+3));
B=4*(n+a).*(n+b).*n.*(n+a+b)./((nab.^2).*(nab+1).*(nab-1));
abadd=[mu; B1; B'];
ab=[A' abadd];


function xw=gauss(N,ab)
N0=size(ab,1); if N0<N, error('input array ab too short'), end
J=zeros(N);
for n=1:N, J(n,n)=ab(n,1); end
for n=2:N
    J(n,n-1)=sqrt(ab(n,2));
    J(n-1,n)=J(n,n-1);
end
[V,D]=eig(J);
[D,I]=sort(diag(D));
V=V(:,I);
xw=[D ab(1,2)*V(1,:)'.^2];


function xw=lobatto_jacobi(N,a,b)

if nargin<2, a=0; end;
if nargin<3, b=a; end
ab=r_jacobi(N+2,a,b);
ab(N+2,1)=(a-b)/(2*N+a+b+2);
ab(N+2,2)=4*(N+a+1)*(N+b+1)*(N+a+b+1)/((2*N+a+b+1)*(2*N+a+b+2)^2);
xw=gauss(N+2,ab);











