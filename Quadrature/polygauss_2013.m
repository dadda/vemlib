function [nodes_x, nodes_y, weights, max_distance] = polygauss_2013(N, boundary_pts, use_mp)
% POLYGAUSS_2013  Gauss-like and triangulation-free cubature over polygons.
%
% [NODES_X, NODES_Y, WEIGHTS, MAX_DISTANCE] = POLYGAUSS_2013(N, POLYGON_SIDES, USE_MP) 
% computes nodes (NODES_X, NODES_Y) and weights WEIGHTS of a cubature rule
% on a polygon. MAX_DISTANCE is the diameter of the polygon.
%
% INPUT:
%
% N            : degree of the 1 dimensional Gauss-Legendre rule.
%
% BOUNDARY_PTS : a matrix containing polygon vertices, ordered
%                counterclockwise. The last row must have the components of
%                the first vertex. In other words, the first row and the
%                last one are equal.
%
% USE_MP       : a flag telling whether the Multiprecision Toolbox
%                (www.advanpix.com) should be used (USE_MP=1) or not
%                (USE_MP=0).
%
% REFERENCE PAPER:
% [1] A. SOMMARIVA and M. VIANELLO
% "Gauss-like and triangulation-free cubature over polygons".

%--------------------------------------------------------------------------
% Copyright (C) 2007-2013 Marco Vianello and Alvise Sommariva
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
%
% Authors:
% Marco Vianello    <marcov@euler.math.unipd.it>
% Alvise Sommariva  <alvise@euler.math.unipd.it>
% Date: April 30, 2013.
%
% Current edited version: August 12, 2016.
%--------------------------------------------------------------------------

%----------------------------------------------------------------------
% BOUNDARY PTS.
%----------------------------------------------------------------------
% x_bd=boundary_pts(:,1);
% y_bd=boundary_pts(:,2);

%----------------------------------------------------------------------
% "MINIMUM" RECTANGLE CONTAINING POLYGON.
%----------------------------------------------------------------------
% x_min=min(x_bd); x_max=max(x_bd);
% y_min=min(y_bd); y_max=max(y_bd);

%----------------------------------------------------------------------
% SOME AUTOMATIC SETTINGS.
%----------------------------------------------------------------------
% if nargin < 3
%     rotation=1;
% end
cubature_type=4;

%--------------------------------------------------------------------------
% POLYGON ROTATION (IF NECESSARY).
%--------------------------------------------------------------------------
% switch rotation
%     case 0
% %         fprintf('\n \t [ROTATION]: NO.');
%         rot_matrix=eye(2);
%         axis_abscissa=[x_min y_max]-[x_min y_min];
%     case 1
% %         fprintf('\n \t [ROTATION]: AUTOMATIC');
%         [boundary_pts,rot_matrix,~,axis_abscissa,~,~,max_distance]=...
%             auto_rotation(boundary_pts,[],[]);
% %         fprintf(' [ANGLE CLOCKWISE (RESPECT Y, IN DEGREES)]: %5.5f',...
% %             rot_angle*180/pi);
%     case 2
% %         fprintf('\n \t [ROTATION]: PREFERRED DIRECTION');
%         nrm_vect=norm(Q-P);
%         if (nrm_vect > 0)
% %            direction_axis=(Q-P)/nrm_vect;
%             [boundary_pts,rot_matrix,~,axis_abscissa,~,~]=...
%                 auto_rotation(boundary_pts,P,Q);
%         else
% %             fprintf('\n \t [WARNING]: THE DIRECTION VECTOR IS NULL. ')
% %             fprintf('USING AUTOMATIC ROTATION.');
%             [boundary_pts,rot_matrix,~,axis_abscissa,~,~]=...
%                 auto_rotation(boundary_pts,P,Q);
%         end
% %         fprintf(' [ANGLE CLOCKWISE (RESPECT Y)]: %5.5f',rot_angle*180/pi);
% end

[boundary_pts,rot_matrix,~,axis_abscissa,~,~,max_distance] = ...
    auto_rotation(boundary_pts,[],[],use_mp);

%--------------------------------------------------------------------------
% COMPUTE NODES AND WEIGHTS OF 1D GAUSS-LEGENDRE RULE.
% TAKEN FROM TREFETHEN PAPER "Is ... Clenshaw-Curtis?".
%--------------------------------------------------------------------------

% DEGREE "N".
[s_N,w_N]=cubature_rules_1D((N-1),cubature_type);
N_length=length(s_N);

% DEGREE "M".
M=N+1;
[s_M,w_M]=cubature_rules_1D((M-1),cubature_type);

%----------------------------------------------------------------------
% L: NUMBER OF SIDES OF THE POLYGON.
% M: ORDER GAUSS INTEGRATION.
% N: ORDER GAUSS PRIMITIVE.
%----------------------------------------------------------------------
L=length(boundary_pts(:,1))-1;

%a=0.5;
a=axis_abscissa(1);

%----------------------------------------------------------------------
% COMPUTE 2D NODES (nodes_x,nodes_y) AND WEIGHTS "weights".
%----------------------------------------------------------------------

nodes_x=[];
nodes_y=[];
weights=[];

for index_side=1:L
    x1=boundary_pts(index_side,1); x2=boundary_pts(index_side+1,1);
    y1=boundary_pts(index_side,2); y2=boundary_pts(index_side+1,2);
    if ~(x1 == a && x2 == a)
        if (y2-y1) ~=0

            if (x2-x1) ~=0
                s_M_loc=s_M;
                w_M_loc=w_M;
            else
                s_M_loc=s_N;
                w_M_loc=w_N;
            end

            M_length=length(s_M_loc);

            half_pt_x=(x1+x2)/2; half_pt_y=(y1+y2)/2;
            half_length_x=(x2-x1)/2; half_length_y=(y2-y1)/2;


            % GAUSSIAN POINTS ON THE SIDE.
            x_gauss_side=half_pt_x+half_length_x*s_M_loc; %SIZE: (M_loc,1)
            y_gauss_side=half_pt_y+half_length_y*s_M_loc; %SIZE: (M_loc,1)

            scaling_fact_plus=(x_gauss_side+a)/2; %SIZE: (M_loc,1)
            scaling_fact_minus=(x_gauss_side-a)/2;%SIZE: (M_loc,1)

            local_weights=...
                (half_length_y*scaling_fact_minus).*w_M_loc;%SIZE:(M_loc,1)

            term_1=repmat(scaling_fact_plus,1,N_length); % SIZE: (M_loc,N)

            term_2=repmat(scaling_fact_minus,1,N_length); % SIZE: (M_loc,N)

            rep_s_N=repmat(s_N',M_length,1);

            % x, y ARE STORED IN MATRICES. A COUPLE WITH THE SAME INDEX
            % IS A POINT, i.e. "P_i=(x(k),y(k))" FOR SOME "k".
            x=term_1+term_2.*rep_s_N;
            y=repmat(y_gauss_side,1,N_length);

            number_rows=size(x,1);
            number_cols=size(x,2);

            x=x(:); x=x';
            y=y(:); y=y';

            rot_gauss_pts=rot_matrix'*[x;y]; % THE INVERSE OF A ROTATION
            % MATRIX IS ITS TRANSPOSE.

            x_rot=rot_gauss_pts(1,:); % GAUSS POINTS IN THE ORIGINAL SYSTEM.
            y_rot=rot_gauss_pts(2,:);

            x_rot=reshape(x_rot',number_rows,number_cols);
            y_rot=reshape(y_rot',number_rows,number_cols);

            nodes_x=[nodes_x; x_rot];
            nodes_y=[nodes_y; y_rot];
            weights=[weights; local_weights];


        end
    end
end

weights=weights*w_N';
weights=weights(:);

nodes_x=nodes_x(:);
nodes_y=nodes_y(:);


% method_used=3;
%
% switch method_used
%     case 1
%         % 2007 METHOD: FASTER BUT COMPLICATED.
%         f = fcnchk(intfcn);
%         f_xy = feval(f,nodes_x,nodes_y, varargin{:});
%         cubature_val=(weights'*f_xy)*w_N; % COMPUTING CUBATURE.
%     case 2
%         % MESHGRID LIKE METHOD
%         f = fcnchk(intfcn);
%         f_xy = feval(f,nodes_x,nodes_y, varargin{:});
%         cubature_val=sum(sum(weights.*f_xy));
%     case 3
%         % CLASSICAL VECTOR DESCRIPTION.
%         f = fcnchk(intfcn);
%         nodes_x=nodes_x(:);
%         nodes_y=nodes_y(:);
%         f_xy=feval(f,nodes_x,nodes_y, varargin{:});
%         cubature_val=weights'*f_xy;
% end





%----------------------------------------------------------------------
% FUNCTIONS USED IN THE ALGORITHM.
%----------------------------------------------------------------------


%----------------------------------------------------------------------
% 1. "auto_rotation"
%----------------------------------------------------------------------
function [polygon_bd_rot,rot_matrix,rot_angle,axis_abscissa,vertex_1,vertex_2,max_distance]=...
    auto_rotation(polygon_bd,vertex_1,vertex_2,use_mp)


% AUTOMATIC ROTATION OF A CONVEX POLYGON SO THAT "GAUSSIAN POINTS",
% AS IN THE PAPER THEY ARE ALL CONTAINED IN THE CONVEX POLYGON.
% SEE THE PAPER FOR DETAILS.


% FIND DIRECTION AND ROTATION ANGLE.
if isempty(vertex_1)
    % COMPUTING ALL THE DISTANCES BETWEEN POINTS.A LITTLE TIME CONSUMING
    % AS PROCEDURE.
    distances = points2distances(polygon_bd);
%    distances = squareform(pdist(polygon_bd(1:end-1,:)));
    [max_distances,max_col_comp]=max(distances,[],2);
    [max_distance,max_row_comp]=max(max_distances,[],1);
    vertex_1=polygon_bd(max_col_comp(max_row_comp),:);
    vertex_2=polygon_bd(max_row_comp,:);
    direction_axis=(vertex_2-vertex_1)/max_distance;
else
    direction_axis=(vertex_2-vertex_1)/norm(vertex_2-vertex_1);
end

% This line prevents direction_axis from being < -1 or > 1 due to
% numerical roundoff errors
direction_axis = min(max(direction_axis,-1.),1.);

rot_angle_x=acos(direction_axis(1));
rot_angle_y=acos(direction_axis(2));

if use_mp
    mypi = mp('pi');
else
    mypi = pi;
end

if rot_angle_y <= mypi/2.
    if rot_angle_x <= mypi/2.
        rot_angle=-rot_angle_y;
    else
        rot_angle=rot_angle_y;
    end
else
    if rot_angle_x <= mypi/2.
        rot_angle=mypi-rot_angle_y;
    else
        rot_angle=rot_angle_y;
    end
end


% CLOCKWISE ROTATION.
rot_matrix=[cos(rot_angle) sin(rot_angle);
    -sin(rot_angle) cos(rot_angle)];

%number_sides=size(polygon_bd,1)-1;

polygon_bd_rot=(rot_matrix*polygon_bd')';

axis_abscissa=rot_matrix*vertex_1';



%----------------------------------------------------------------------
% 3. "cubature_rules_1D"
%----------------------------------------------------------------------

function [nodes,weights]=cubature_rules_1D(n,cubature_type)

% SEE WALDVOGEL PAPER. ADDED NODES

% Weights of the Fejer2, Clenshaw-Curtis and Fejer1 quadrature by DFTs
% n>1. Nodes: x_k = cos(k*pi/n)

N=(1:2:n-1)'; l=length(N); m=n-l; K=(0:m-1)';

switch cubature_type

    case 1 % FEJER 1.
        v0=[2*exp(1i*pi*K/n)./(1-4*K.^2); zeros(l+1,1)];
        v1=v0(1:end-1)+conj(v0(end:-1:2));
        weights=ifft(v1);
        k=(1/2):(n-(1/2)); nodes=(cos(k*pi/n))';

    case 2 % FEJER 2.
        v0=[2./N./(N-2); 1/N(end); zeros(m,1)];
        v2=-v0(1:end-1)-v0(end:-1:2);
        wf2=ifft(v2); weights=[wf2;0];
        k=0:n; nodes=(cos(k*pi/n))';

    case 3 % CLENSHAW CURTIS.
        g0=-ones(n,1); g0(1+l)=g0(1+l)+n; g0(1+m)=g0(1+m)+n;
        g=g0/(n^2-1+mod(n,2));
        v0=[2./N./(N-2); 1/N(end); zeros(m,1)];
        v2=-v0(1:end-1)-v0(end:-1:2);
        wcc=ifft(v2+g); weights=[wcc;wcc(1,1)];
        k=0:n; nodes=(cos(k*pi/n))';

    case 4 % GAUSS LEGENDRE
        beta=0.5./sqrt(1-(2*(1:n)).^(-2));
        T=diag(beta,1)+diag(beta,-1);
        [V,D]=eig(T);
        x=diag(D); [x,index]=sort(x); x=x';
        w=2*V(1,index).^2;
        nodes=x';
        weights=w';

end








%----------------------------------------------------------------------
% 3. "points2distances"
%----------------------------------------------------------------------

function distances = points2distances(points)

% Create euclidean distance matrix from point matrix.

% Get dimensions.
[numpoints,~]=size(points);

% All inner products between points.
distances=points*points';

% Vector of squares of norms of points.
lsq=diag(distances);

% Distance matrix.
distances=sqrt(repmat(lsq,1,numpoints)+repmat(lsq,1,numpoints)'-2*distances);







