#include <cstdio>
#include <cmath>
#include <cblas.h>
#include <iostream>
#include "quadrature.h"
#include "tetgen.h"
#include "optimize.h"
//#include <ctime>

using namespace std;
using namespace GiNaC;

#define PI 3.14159265358979323846

class TetgenTimeout {};

/*******************************************************************************************
 * Function definitions for quadrature formulas on polyhedra
 *******************************************************************************************/

ex chinAlgorithm1D(
		   const ex & g,
		   const double * const vertices,
		   const double * const b,
		   const ex & sym_variable
		   )
{
  // Highest degree
  int p = g.degree(sym_variable);

  ex fp;
  ex Int = 0;
  for(int i = 0; i <= p; ++i)
    {
      fp = g.coeff(sym_variable, i);
      if (!fp.is_zero())
      	{
	  Int += ( b[0] * fp * std::pow(vertices[0],i) +
		   b[1] * fp * std::pow(vertices[1],i) ) / (1.+i);
    	}
    }

  return Int;
}


ex chinAlgorithm2D(
		   const ex & g,
		   const double * const vertices,
		   const size_t Nver, 
		   const size_t * const polytope,
		   const size_t polytope_size,
		   const size_t * const facets_cumsizes,
		   const double * const a,
		   const double * const b,
		   const lst & sym_variables
		   )
{
  symbol c("c");

  ex innerg = g.subs(sym_variables, lst{ c*sym_variables[0], c*sym_variables[1] });
  innerg = expand(innerg);

  // Highest degree
  int p = innerg.degree(c);

  // Rotation matrix
  double R[2][2];

  ex fp, gnew;
  ex Int = 0, h;
  for(int i = 0; i <= p; ++i)
    {
      fp = innerg.coeff(c, i);
      if (!fp.is_zero())
  	{
  	  h = 0;
  	  double new_vertices[2];
  	  double new_b[2];
	  
  	  for (size_t j = 0; j < polytope_size; ++j)
  	    {
  	      // Rotation matrix
  	      R[0][0] = a[polytope_size+j]; R[0][1] = -a[j];
  	      R[1][0] = a[j]; R[1][1] = a[polytope_size+j];

	      gnew = expand( fp.subs(sym_variables, lst{ R[0][0]*sym_variables[0] +
  	      		         			 R[1][0]*b[j],
							 R[0][1]*sym_variables[0] + 
		                                         R[1][1]*b[j] } ) );

  	      // Rotated vertices
  	      new_vertices[0] = R[0][0]*vertices[ polytope[facets_cumsizes[j]]-1 ] + 
  	      	                R[0][1]*vertices[ polytope[facets_cumsizes[j]]-1+Nver ];
  	      new_vertices[1] = R[0][0]*vertices[ polytope[facets_cumsizes[j]+1]-1 ] + 
  	      	                R[0][1]*vertices[ polytope[facets_cumsizes[j]+1]-1+Nver ];

  	      new_b[0] = new_vertices[0];
  	      new_b[1] = -new_vertices[1];

  	      h += b[j] * chinAlgorithm1D(gnew, new_vertices, new_b, sym_variables[0]);
  	    }
  	  h /= (2.0 + i);
  	  Int += h;
  	}
    }

  return Int;
}


ex chinAlgorithm3D(
		   const ex & g,
		   const double * const vertices,
		   const size_t Nver, 
		   const size_t * const polytope,
		   const size_t polytope_size,
		   const size_t * const facets_cumsizes,
		   const double * const a,
		   const double * const b,
		   const lst & sym_variables
		   )
{
  symbol c("c");

  ex innerg = g.subs(sym_variables[0] == c*sym_variables[0]);
  innerg = innerg.subs(sym_variables[1] == c*sym_variables[1]);
  innerg = expand(innerg.subs(sym_variables[2] == c*sym_variables[2]));

  // Highest degree
  int p = innerg.degree(c);

  // Rotation matrix and other useful variables
  double R[3][3];
  double omega[3];
  double ct, st, new_a_norm, tmp1, tmp2;
  size_t collapsed_variable, facet_size;

  ex fp, gnew;
  lst new_sym_variables;
  ex Int = 0, h;
  for(int i = 0; i <= p; ++i)
    {
      fp = innerg.coeff(c, i);
      // cout << fp << endl;
      if (!fp.is_zero())
  	{
  	  h = 0;
  	  double * new_vertices;
  	  double * new_a;
  	  double * new_b;
  	  size_t * new_polytope;
	  size_t * new_facets_cumsizes;

  	  for (size_t j = 0; j < polytope_size; ++j)
  	    {
  	      if (fabs(a[2*polytope_size+j]+1.) < 0.5 || fabs(a[2*polytope_size+j]-1.) < 0.5)
  		{
  		  // Rotation towards the x-axis
  		  ct = a[j];
		  st = sqrt( 1. - std::pow(ct,2) );
  		  omega[0] = 0.0;
  		  omega[1] = a[2*polytope_size+j]/st;
  		  omega[2] = -a[polytope_size+j]/st;

  		  collapsed_variable = 0;
		  new_sym_variables = lst{ sym_variables[1], sym_variables[2] };
  		}
  	      else
  		{
  		  // Rotation towards the z-axis
  		  ct = a[2*polytope_size+j];
		  st = sqrt( 1. - std::pow(ct,2) );
  		  omega[0] = a[polytope_size+j]/st;
  		  omega[1] = -a[j]/st;
  		  omega[2] = 0.0;

  		  collapsed_variable = 2;
		  new_sym_variables = lst{ sym_variables[0], sym_variables[1] };
  		}

  	      // Rodrigues's finite rotation formula
	      R[0][0] = ct + std::pow(omega[0],2)*(1-ct);
	      R[1][1] = ct + std::pow(omega[1],2)*(1-ct);
	      R[2][2] = ct + std::pow(omega[2],2)*(1-ct);

  	      R[0][1] = -omega[2]*st + omega[0]*omega[1]*(1-ct);
  	      R[0][2] = omega[1]*st + omega[0]*omega[2]*(1-ct);
  	      R[1][0] = omega[2]*st + omega[0]*omega[1]*(1-ct);
  	      R[1][2] = -omega[0]*st + omega[1]*omega[2]*(1-ct);
  	      R[2][0] = -omega[1]*st + omega[0]*omega[2]*(1-ct);
  	      R[2][1] = omega[0]*st + omega[1]*omega[2]*(1-ct);

	      // cout << R[0][0] << ' ' << R[0][1] << ' ' << R[0][2] << endl;
	      // cout << R[1][0] << ' ' << R[1][1] << ' ' << R[1][2] << endl;
	      // cout << R[2][0] << ' ' << R[2][1] << ' ' << R[2][2] << endl << endl;

  	      gnew = fp.subs(sym_variables, 
			     lst{ R[0][0]*sym_variables[0] +
  				  R[1][0]*sym_variables[1] +
  				  R[2][0]*sym_variables[2],
  				  R[0][1]*sym_variables[0] + 
  				  R[1][1]*sym_variables[1] +
  				  R[2][1]*sym_variables[2],
  				  R[0][2]*sym_variables[0] + 
  				  R[1][2]*sym_variables[1] +
				  R[2][2]*sym_variables[2] } );

  	      gnew = expand( gnew.subs(sym_variables[collapsed_variable] == b[j]) );

	      facet_size = facets_cumsizes[j+1]-facets_cumsizes[j];
  	      new_vertices = new double   [2*facet_size];
  	      new_polytope = new size_t [2*facet_size];
	      new_facets_cumsizes = new size_t [facet_size+1];
  	      new_a        = new double   [2*facet_size];
  	      new_b        = new double   [facet_size];

  	      if (collapsed_variable == 0)
  		{
  		  // 1st component: y
  		  // 2nd component: z
  		  for (size_t m = 0; m < facet_size; ++m)
  		    {
  		      new_vertices[m] =
			R[1][0]*vertices[ polytope[facets_cumsizes[j]+m]-1 ] +
  			R[1][1]*vertices[ polytope[facets_cumsizes[j]+m]-1+Nver ] +
  			R[1][2]*vertices[ polytope[facets_cumsizes[j]+m]-1+2*Nver ];
  		      new_vertices[facet_size+m] = 
			R[2][0]*vertices[ polytope[facets_cumsizes[j]+m]-1 ] +
  			R[2][1]*vertices[ polytope[facets_cumsizes[j]+m]-1+Nver ] +
  			R[2][2]*vertices[ polytope[facets_cumsizes[j]+m]-1+2*Nver ];
  		    }
  		}
  	      else
  		{
  		  // 1st component: x
  		  // 2nd component: y
  		  for (size_t m = 0; m < facet_size; ++m)
  		    {
  		      new_vertices[m] =
			R[0][0]*vertices[ polytope[facets_cumsizes[j]+m]-1 ] +
  			R[0][1]*vertices[ polytope[facets_cumsizes[j]+m]-1+Nver ] +
  			R[0][2]*vertices[ polytope[facets_cumsizes[j]+m]-1+2*Nver ];
  		      new_vertices[facet_size+m] =
			R[1][0]*vertices[ polytope[facets_cumsizes[j]+m]-1 ] +
  			R[1][1]*vertices[ polytope[facets_cumsizes[j]+m]-1+Nver ] +
  			R[1][2]*vertices[ polytope[facets_cumsizes[j]+m]-1+2*Nver ];
  		    }		  

  		}

	      new_facets_cumsizes[0] = 0;
  	      for (size_t m = 0; m < facet_size; ++m)
  		{
  		  new_polytope[2*m] = m+1;
  		  new_polytope[2*m+1] = fmax( int( (m+2) % (facet_size+1) ), 1 );
		  new_facets_cumsizes[m+1] = new_facets_cumsizes[m]+2;

  		  new_a[m] = new_vertices[ new_polytope[2*m+1]-1+facet_size ] -
		             new_vertices[ new_polytope[2*m]-1+facet_size ];
  		  new_a[facet_size+m] = new_vertices[ new_polytope[2*m]-1 ] -
		                        new_vertices[ new_polytope[2*m+1]-1 ];

  		  // Normalize outer normals
  		  // new_a_norm = sqrt( pow(new_a[m],2) + pow(new_a[facet_size+m],2) );
		  tmp1 = fabs(new_a[m]);
		  tmp2 = fabs(new_a[facet_size+m]);
		  if (tmp1 >= tmp2)
		    {
		      new_a_norm = sqrt(1.0 + std::pow(tmp2/tmp1,2)) * tmp1;
		    }
		  else
		    {
		      new_a_norm = sqrt(1.0 + std::pow(tmp1/tmp2,2)) * tmp2;
		    }

  		  new_a[m] /= new_a_norm;
  		  new_a[facet_size+m] /= new_a_norm;

  		  new_b[m] = new_a[m]*new_vertices[new_polytope[2*m]-1] +
  		             new_a[facet_size+m]*new_vertices[new_polytope[2*m]-1+facet_size];
  		}

  	      // Call Chin algorithm on the jth face
  	      h += b[j] * chinAlgorithm2D(gnew, new_vertices, facet_size,
  	      				  new_polytope, facet_size, new_facets_cumsizes,
	      				  new_a, new_b, new_sym_variables);

  	      // Deallocation
  	      delete[] new_vertices;
  	      delete[] new_polytope;
	      delete[] new_facets_cumsizes;
  	      delete[] new_a;
  	      delete[] new_b;
  	    }

  	  h /= (3. + i);
  	  Int += h;
  	}
    }

  return Int;
}


double orient3D(
		double ax, double ay, double az,
		double bx, double by, double bz,
		double cx, double cy, double cz,
		double dx, double dy, double dz
		)
{
  return (ax-dx) * ( (by-dy)*(cz-dz) - (bz-dz)*(cy-dy) )
    - (ay-dy) * ( (bx-dx)*(cz-dz) - (bz-dz)*(cx-dx) )
    + (az-dz) * ( (bx-dx)*(cy-dy) - (by-dy)*(cx-dx) );
}


void randomPtsInConvexPolyhedron(
				 const double * const vertices,
				 const size_t Nver,
				 const size_t * const vertexlist,
				 const size_t Nfc,
				 const size_t * const facets_cumsizes,
				 double * const xq,
				 double * const yq,
				 double * const zq,
				 const size_t Nqpts
				 )
{
  /*
   * Step 1: find boundary box
   */
  double xmin, xmax, ymin, ymax, zmin, zmax;

  xmin = vertices[0]; xmax = vertices[0];
  ymin = vertices[Nver]; ymax = vertices[Nver];
  zmin = vertices[2*Nver]; zmax = vertices[2*Nver];

  for (size_t i = 1; i < Nver; ++i)
    {
      if (vertices[i] < xmin) xmin = vertices[i];
      if (vertices[i] > xmax) xmax = vertices[i];
      if (vertices[Nver+i] < ymin) ymin = vertices[Nver+i];
      if (vertices[Nver+i] > ymax) ymax = vertices[Nver+i];
      if (vertices[2*Nver+i] < zmin) zmin = vertices[2*Nver+i];
      if (vertices[2*Nver+i] > zmax) zmax = vertices[2*Nver+i];
    }

  /*
   * Step 2: generate random points inside the polyhedron
   */
  bool isOutside;
  double x, y, z;
  double ax, ay, az, bx, by, bz, cx, cy, cz, test;
  for (size_t q = 0; q < Nqpts; ++q)
    {
      isOutside = true;
      while (isOutside)
	{
	  x = xmin + ( double(rand())/RAND_MAX ) * (xmax-xmin);
	  y = ymin + ( double(rand())/RAND_MAX ) * (ymax-ymin);
	  z = zmin + ( double(rand())/RAND_MAX ) * (zmax-zmin);

	  isOutside = false;
	  for (size_t f = 0; f < Nfc; ++f)
	    {
	      ax = vertices[ vertexlist[ facets_cumsizes[f] ] - 1 ];
	      ay = vertices[ vertexlist[ facets_cumsizes[f] ] + Nver - 1 ];
	      az = vertices[ vertexlist[ facets_cumsizes[f] ] + 2*Nver - 1 ];

	      bx = vertices[ vertexlist[ facets_cumsizes[f] + 1 ] - 1 ];
	      by = vertices[ vertexlist[ facets_cumsizes[f] + 1 ] + Nver - 1 ];
	      bz = vertices[ vertexlist[ facets_cumsizes[f] + 1 ] + 2*Nver - 1 ];

	      cx = vertices[ vertexlist[ facets_cumsizes[f] + 2 ] - 1 ];
	      cy = vertices[ vertexlist[ facets_cumsizes[f] + 2 ] + Nver - 1 ];
	      cz = vertices[ vertexlist[ facets_cumsizes[f] + 2 ] + 2*Nver - 1 ];

	      test = orient3D( ax, ay, az, bx, by, bz, cx, cy, cz, x, y, z );
	      if (test < 0.0)
		{
		  isOutside = true;
		  break;
		}
	    }
	}
      xq[q] = x; yq[q] = y; zq[q] = z;
    }
}


/*
 * Construct a quadrature rule on a polyhedron
 *
 * Inputs:
 *
 * - vertices: one dimensional array with the coordinates of the vertices making up JUST
 *             the polyhedron, NOT the whole mesh the polyhedron belongs to (unless the mesh
 *             contains just this polyhedron). Coordinates are stored as follows: first all x's,
 *             then all y's, and then all z's.
 *
 * - Nver:     number of vertices f the polyhedron.
 *
 * - vertexlist:  one dimensional array with connectivity information for the current polyhedron.
 *             Each face is represented by the indices of its vertices listed according
 *             to the order induced by the outer normal. Faces are listed one after the other.
 *             Entries point to nodes in array 'vertices'.
 *
 * - Nfc:      number of faces.
 *
 * - facets_cumsizes: one dimensional array with the cumulative sums of the orders of the faces.
 *                    The array starts with 0 and it has Nfc+1 entries.
 *
 * - deg:      degree of the quadrature rule to construct.
 *
 * - diameter: diameter of the polyhedron.
 *
 * - volume:   volume of the polyhedron.
 *
 * - centroid: one dimensional array with the polyhedron centroid.
 *
 * - a:        array with the outer normals to the polyhedron faces stored in column major order,
 *             i.e. first all x's, then all y's, then all z's.
 *
 * - b:        algebraic distances between the faces and the origin.
 *
 * - final_nodes: one dimensional array with the computed quadrature nodes, stored in column
 *                major order.
 *
 * - final_weights: computed quadrature weights.
 */
void constructQuadrature(
			 const double * const vertices, // first, all x, then all y, then all z
			 const size_t Nver,
			 const size_t * const vertexlist,
			 const size_t Nfc,
			 const size_t * const facets_cumsizes,
			 const int deg,
			 const double diameter,
			 const double volume,
			 const double * const centroid,
			 const double * const a,
			 const double * const b,
			 double * final_nodes,
			 double * final_weights
			 )
{
  double *xq, *yq, *zq;
  
  // Dimension of the polynomial space of degree deg
  int d3 = ((deg+3)*(deg+2)*(deg+1))/6;

  size_t nr = d3;
  size_t nc;
  
  /****************************************************************************************
   * Try to tetrahedralize the polyhedron
   ****************************************************************************************/

  tetgenio in, out;
  tetgenio::facet *f;
  tetgenio::polygon *p;

  // All indices start from 1
  in.firstnumber = 1;

  // Number of vertices of the polyhedron
  in.numberofpoints = Nver;

  in.pointlist = new REAL[in.numberofpoints * 3];

  // Remark: we assume vertices to be organized as in chinAlgorithm routines, i.e.,
  // first all the x values, then all the y values, and finally all the z values.
  for (size_t i = 0; i < in.numberofpoints; ++i)
    {
      in.pointlist[3*i]   = vertices[i];
      in.pointlist[3*i+1] = vertices[i+in.numberofpoints];
      in.pointlist[3*i+2] = vertices[i+in.numberofpoints*2];
    }

  in.numberoffacets  = Nfc;
  in.facetlist       = new tetgenio::facet[in.numberoffacets];
  //in.facetmarkerlist = NULL;
  //in.facetmarkerlist = new int[in.numberoffacets];

  // Load facets
  for (size_t i = 0; i < in.numberoffacets; ++i)
    {
      f = &in.facetlist[i];
      f -> numberofpolygons = 1;
      f -> polygonlist = new tetgenio::polygon[f->numberofpolygons];
      f -> numberofholes = 0;
      f -> holelist = NULL;
      p = &f->polygonlist[0];
      p -> numberofvertices = facets_cumsizes[i+1] - facets_cumsizes[i];
      p -> vertexlist = new int[p->numberofvertices];
      for (int j = 0; j < p->numberofvertices; ++j)
	p -> vertexlist[j] = vertexlist[facets_cumsizes[i]+j];
    }

  // time_t tm0 = time(0);
  // clock_t ck0 = clock();

  // Tetrahedralize the PLC
  char tetgen_switches[128];
  double toleranceCoplanarTest = 1e-8; // This is the default used by Tetgen
  double maxTolerance = 1e-2;
  bool problemWithTet = false;
  int tetgenError;

  while (toleranceCoplanarTest < maxTolerance)
    {
      tetgen_switches[0] = '\0';
      if (Nver == 4)
	{
	  // This is a tetrahedron
	  sprintf(tetgen_switches, "pQa%.16f", volume);
	}
      else
	{
	  // Other useful switches: -T, -V, -Y
	  sprintf(tetgen_switches, "pQa%.16fT%.16f", volume, toleranceCoplanarTest);
	}

      try
	{
	  tetrahedralize(tetgen_switches, &in, &out);
	  // boost::thread caller(myTetrahedralize, tetgen_switches, &in, &out);
	  // boost::this_thread::sleep_for(boost::chrono::seconds(2));
	  // if (!caller.try_join_for(boost::chrono::milliseconds(500)))
	  //   {
	  //     cerr << "Tetgen is stucked. Stopping it..." << endl;
	  //     throw TetgenTimeout();
	  //   }
	  break;
	}
      catch(...)
	{
	  cerr << "Tetgen failed" << endl;

	  if (Nver != 4)
	    {
	      toleranceCoplanarTest *= 10;
	    }
	  else
	    {
	      problemWithTet = true;
	      break;
	    }
	}
    }

  if ( (toleranceCoplanarTest >= maxTolerance) || problemWithTet )
    {
      cerr << "Tetgen failed meshing the current element." << endl
	   << "Generating random points inside the polyhedron." << endl
	   << "Beware: this approach only works for convex polyhedron." << endl;

      nc = d3 * d3 * d3;

      xq = new double[ nc ];
      yq = new double[ nc ];
      zq = new double[ nc ];

      randomPtsInConvexPolyhedron( vertices, Nver, vertexlist, Nfc, facets_cumsizes,
				   xq, yq, zq, nc );
    }
  else
    {
      /****************************************************************************************
       * Step 2: compute quadrature formulas on each tetrahedron
       ****************************************************************************************/

      int N = ceil((deg+1)/2.);

      double * nodes;
      double * weights;

      nodes   = new double[N*N*N*4];
      weights = new double[N*N*N];

      // Quadrature rule in barycentric coordinates
      StroudQuadrature(deg, 3, nodes, weights);

      double *xcoord, *ycoord, *zcoord;
      xcoord = new double[4 * out.numberoftetrahedra];
      ycoord = new double[4 * out.numberoftetrahedra];
      zcoord = new double[4 * out.numberoftetrahedra];
      for (size_t i = 0; i < out.numberoftetrahedra; ++i)
	{
	  // Remember that we made all the indices denoting geometrical objects
	  // in tetgen start from 1
	  xcoord[4*i]   = out.pointlist[ 3*(out.tetrahedronlist[4*i]-1) ];
	  xcoord[4*i+1] = out.pointlist[ 3*(out.tetrahedronlist[4*i+1]-1) ];
	  xcoord[4*i+2] = out.pointlist[ 3*(out.tetrahedronlist[4*i+2]-1) ];
	  xcoord[4*i+3] = out.pointlist[ 3*(out.tetrahedronlist[4*i+3]-1) ];

	  ycoord[4*i]   = out.pointlist[ 3*(out.tetrahedronlist[4*i]-1)+1 ];
	  ycoord[4*i+1] = out.pointlist[ 3*(out.tetrahedronlist[4*i+1]-1)+1 ];
	  ycoord[4*i+2] = out.pointlist[ 3*(out.tetrahedronlist[4*i+2]-1)+1 ];
	  ycoord[4*i+3] = out.pointlist[ 3*(out.tetrahedronlist[4*i+3]-1)+1 ];

	  zcoord[4*i]   = out.pointlist[ 3*(out.tetrahedronlist[4*i]-1)+2 ];
	  zcoord[4*i+1] = out.pointlist[ 3*(out.tetrahedronlist[4*i+1]-1)+2 ];
	  zcoord[4*i+2] = out.pointlist[ 3*(out.tetrahedronlist[4*i+2]-1)+2 ];
	  zcoord[4*i+3] = out.pointlist[ 3*(out.tetrahedronlist[4*i+3]-1)+2 ];
	}

      xq = new double[N*N*N*out.numberoftetrahedra];
      yq = new double[N*N*N*out.numberoftetrahedra];
      zq = new double[N*N*N*out.numberoftetrahedra];

      // Coordinates of quadrature nodes
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, N*N*N, out.numberoftetrahedra, 4,
		  1.0, nodes, N*N*N, xcoord, 4, 0.0, xq, N*N*N);
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, N*N*N, out.numberoftetrahedra, 4,
		  1.0, nodes, N*N*N, ycoord, 4, 0.0, yq, N*N*N);
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, N*N*N, out.numberoftetrahedra, 4,
		  1.0, nodes, N*N*N, zcoord, 4, 0.0, zq, N*N*N);

      nc = N*N*N*out.numberoftetrahedra;
      
      delete[] nodes;
      delete[] weights;
      delete[] xcoord;
      delete[] ycoord;
      delete[] zcoord;      
    }

  /**********************************************************************************************
   * Step 3: compute quadrature formula on the polyhedron using non negative least square method
   **********************************************************************************************/
  
  double * Amf = new double[nr * nc];

  // Set coefficient matrix Amf of the moment fitting method

  //double v = 0.0;
  size_t m, previous_level_offset;

  for (size_t j = 0; j < nc; ++j)
    {
      // First monomial basis function = 1
      Amf[nr*j] = 1.0;

      m = 1;
      for (int s = 1; s <= deg; ++s)
	{
	  Amf[nr*j + m] = Amf[nr*j + ((s+1)*s*(s-1))/6] * (xq[j] - centroid[0])/diameter;
	  // v = (A -> get( ((s+1)*s*(s-1))/6 , j )) * (xq[j] - centroid[0])/diameter;
	  // A -> set(m, j, v);
	  m += 1;
	  previous_level_offset = 0;
	  for (int q = s-1; q >= 0; --q)
	    {
	      Amf[nr*j + m] = Amf[nr*j + ((s+1)*s*(s-1))/6+previous_level_offset]
		* (yq[j] - centroid[1])/diameter;
	      // v = (A -> get( ((s+1)*s*(s-1))/6+previous_level_offset, j ))
	      // 	* (yq[j] - centroid[1])/diameter;
	      // A -> set(m, j, v);
	      m += 1;
	      for (int r = s-q-1; r >= 0; --r)
		{
		  Amf[nr*j + m] = Amf[nr*j + ((s+1)*s*(s-1))/6+previous_level_offset] *
		    (zq[j] - centroid[2])/diameter;
		  // v = (A -> get( ((s+1)*s*(s-1))/6+previous_level_offset, j ))
		  //   * (zq[j] - centroid[2])/diameter;
		  // A -> set(m, j, v);
		  m += 1;
		  previous_level_offset += 1;
		}
	    }
	}
    }

  // cerr << "Inputs for NNLS problem, Amf matrix: " << endl;

  // for (size_t i = 0; i < nr*nc; ++i)
  //   cerr << Amf[i] << endl;

  // cerr << endl;

  /*
   * Assemble right hand side for the moment fitting equation.
   * Use algorithm from Chin et al.
   */

  double * rhs = new double[nr];
  char gstr[256];

  // rhs[0] is the integral of the first basis function, which is = 1.
  rhs[0] = volume;

  // Assemble the remaining elements of the rhs
  size_t i = 1;

  int t;

  ex g, Int;
  symbol x("x"), y("y"), z("z");

  symtab table; 
  table["x"] = x; table["y"] = y; table["z"] = z;
  parser reader(table);

  lst l;
  l = {x, y, z};

  // cerr << "Inputs for NNLS problem, expressions for rhs: " << endl;

  for (int q = 1; q <= deg; ++q)
    {
      for (int r = q; r >= 0; --r)
	{
	  for (int s = q-r; s >= 0; --s)
	    {
	      t = q-r-s;
	      sprintf(gstr, "pow((x-(%.16f))/%.16f,%d) * pow((y-(%.16f))/%.16f,%d) * pow((z-(%.16f))/%.16f,%d)",
		      centroid[0], diameter, r,
		      centroid[1], diameter, s,
		      centroid[2], diameter, t);

	      g = reader(gstr);
	      Int = chinAlgorithm3D(g, vertices, Nver,
				    vertexlist, Nfc, facets_cumsizes, a, b, l);
	      
	      if (is_a<numeric>(Int))
		rhs[i] = ex_to<numeric>(Int).to_double();
	      else
		cerr << "Something went wrong.." << endl;

	      i += 1;
	    }
	}
    }

  // time_t tm3 = time(0);
  // clock_t ck3 = clock();
  // cout << "Step 3, assemblage. Wall time = " << difftime(tm3,tm2) << " seconds.\n";
  // cout << "Step 3, assemblage. CPU time  = " << double(ck3-ck2)/CLOCKS_PER_SEC << " seconds.\n";

  /*
   * Find the quadrature weights by solving the linear non negative least squares
   * problem, with the algorithm of Lawson and Hanson.
   */

  double * sol   = new double[nc];
  double * w     = new double[nc];
  double * zz    = new double[nr];
  size_t * index = new size_t[nc];
  double rnorm;
  size_t ip_end;

  // cerr << "Inputs for NNLS problem, rhs: " << endl;

  // for (size_t i = 0; i < nr; ++i)
  //   cerr << rhs[i] << endl;

  // cerr << endl;

  nnls(Amf, nr, nr, nc, rhs, sol, rnorm, w, zz, index, ip_end);

  // time_t tm4 = time(0);
  // clock_t ck4 = clock();
  // cout << "Step 4, NNLS. Wall time = " << difftime(tm4,tm3) << " seconds.\n";
  // cout << "Step 4, NNLS. CPU time  = " << double(ck4-ck3)/CLOCKS_PER_SEC << " seconds.\n";

  if (ip_end != d3)
    {
      /*
       * We expect the nnls solution to be of dimension d3. If that is not the case,
       * issue a warning.
       */
      cerr << "\nWarning! Size of NNLS solution " << ip_end
	   << " not matching the dimension of the polynomial basis " << d3 << endl;
    }

  // Construct final quadrature nodes and weights from NNLS solution

  if (ip_end < d3)
    {
      for (size_t i = 0; i < ip_end; ++i)
	{
	  final_nodes[i]      = xq[index[i]];
	  final_nodes[i+d3]   = yq[index[i]];
	  final_nodes[i+d3*2] = zq[index[i]];
	  final_weights[i]    = sol[index[i]];
	}
      for (size_t i = ip_end; i < d3; ++i)
	{
	  final_nodes[i]      = 0.0;
	  final_nodes[i+d3]   = 0.0;
	  final_nodes[i+d3*2] = 0.0;
	  final_weights[i]    = 0.0;
	}
    }
  else
    {
      for (size_t i = 0; i < d3; ++i)
	{
	  final_nodes[i]      = xq[index[i]];
	  final_nodes[i+d3]   = yq[index[i]];
	  final_nodes[i+d3*2] = zq[index[i]];
	  final_weights[i]    = sol[index[i]];
	}
    }

  delete[] sol;
  delete[] w;
  delete[] zz;
  delete[] index;
  delete[] Amf;
  delete[] rhs;
  delete[] xq;
  delete[] yq;
  delete[] zq;

  return;
}

/*
 * Construct a quadrature rule on a polygon
 */
template<class T>
int argmax(T *ptr, int n)
{
  T maxVal = ptr[0];
  int argMax = 0;

  for (int i = 1; i < n; ++i) {
    if (ptr[i] > maxVal) {
      maxVal = ptr[i];
      argMax = i;
    }
  }

  return argMax;
}


void auto_rotation(double * const polygon_bd,
		   const int m,
		   double * const max_distance_ptr,
		   double * const rot_matrix,
		   double * const axis_abscissa)
{
  double rot_angle;
  double * distances = new double[m*m];

  for (int i = 0; i < m*m; ++i)
    distances[i] = 0.0;

  int linearIdx = 0;
  for (int j = 0; j < m; ++j) {
    for (int i = j+1; i < m; ++i) {
      linearIdx = m*j+i;
      distances[linearIdx] = sqrt( (polygon_bd[j] - polygon_bd[i])*(polygon_bd[j] - polygon_bd[i]) +
				   (polygon_bd[j+m] - polygon_bd[i+m])*(polygon_bd[j+m] - polygon_bd[i+m]) );
    }
  }

  int maxIdx = argmax(distances, m*m);
  *max_distance_ptr = distances[maxIdx];

  int pos1   = maxIdx/m;
  int pos2   = maxIdx - m*pos1;

  double vertex_1[2];
  double vertex_2[2];

  vertex_1[0] = polygon_bd[pos2]; vertex_1[1] = polygon_bd[pos2+m];
  vertex_2[0] = polygon_bd[pos1]; vertex_2[1] = polygon_bd[pos1+m];

  double direction_axis[2];
  
  direction_axis[0] = min( max( (vertex_2[0]-vertex_1[0])/(*max_distance_ptr), -1. ), 1. );
  direction_axis[1] = min( max( (vertex_2[1]-vertex_1[1])/(*max_distance_ptr), -1. ), 1. );

  double rot_angle_x = acos( direction_axis[0] );
  double rot_angle_y = acos( direction_axis[1] );

  if (rot_angle_y <= PI/2.) {
    if (rot_angle_x <= PI/2.)
      rot_angle = -rot_angle_y;
    else
      rot_angle = rot_angle_y;
  } else {
    if (rot_angle_x <= PI/2.)
      rot_angle = PI-rot_angle_y;
    else
      rot_angle = rot_angle_y;
  }

  rot_matrix[0] = cos(rot_angle);
  rot_matrix[1] = -sin(rot_angle);
  rot_matrix[2] = sin(rot_angle);
  rot_matrix[3] = cos(rot_angle);

  double buffer[2];
  
  for (int i = 0; i < m; ++i) {
    buffer[0] = polygon_bd[i];
    buffer[1] = polygon_bd[i+m];
    polygon_bd[i]   = buffer[0] * rot_matrix[0] + buffer[1] * rot_matrix[2];
    polygon_bd[i+m] = buffer[0] * rot_matrix[1] + buffer[1] * rot_matrix[3];
  }

  axis_abscissa[0] = rot_matrix[0] * vertex_1[0] + rot_matrix[2] * vertex_1[1];
  axis_abscissa[1] = rot_matrix[1] * vertex_1[0] + rot_matrix[3] * vertex_1[1];

  delete[] distances;
}


void polygauss_2013(double * const polygon_bd,
		    const int m,
		    const int N,
		    double ** nodes_x,
		    double ** nodes_y,
		    double ** weights,
		    int * counter_ptr,
		    double * max_distance_ptr)
{
  double rot_matrix[4];
  double axis_abscissa[2];

  // --------------------------------------------------------------------------
  // Polygon rotation

  auto_rotation(polygon_bd, m, max_distance_ptr, rot_matrix, axis_abscissa);

  // --------------------------------------------------------------------------
  // Compute nodes and weights of 1D Gauss-Legendre rule

  double * s_N = new double[N];
  double * w_N = new double[N];

  legendre_compute_glr(N, s_N, w_N);
  rescale(-1.0, 1.0, N, s_N, w_N);

  int M = N+1;
  double * s_M = new double[M];
  double * w_M = new double[M];

  legendre_compute_glr(M, s_M, w_M);
  rescale(-1.0, 1.0, M, s_M, w_M);

  int L = m-1;
  double a = axis_abscissa[0];

  // The maximum number of 2D quadrature nodes should be L*(N^2+N);

  *counter_ptr = 0;
  double x1, x2, y1, y2;
  double *s_M_loc, *w_M_loc;
  int M_length;

  double half_pt_x, half_pt_y, half_length_x, half_length_y;

  double local_weights;
  double x_gauss_side, y_gauss_side, scaling_fact_plus, scaling_fact_minus;
  double term_1, term_2, rep_s_N, x, y;

  for (int index_side = 0; index_side < L; ++index_side) {
    x1 = polygon_bd[index_side];
    x2 = polygon_bd[index_side+1];
    y1 = polygon_bd[index_side+m];
    y2 = polygon_bd[index_side+1+m];

    if ( (fabs(x1-a) >= 1e-15) || (fabs(x2-a) >= 1e-15) ) {
      if ( fabs(y2-y1) > 1e-15 ) {
	if ( fabs(x2-x1) > 1e-15 ) {
	  s_M_loc = s_M;
	  w_M_loc = w_M;
	  M_length = M;
	} else {
	  s_M_loc = s_N;
	  w_M_loc = w_N;
	  M_length = N;
	}

	half_pt_x =(x1+x2)/2.0;
	half_pt_y = (y1+y2)/2.0;
        half_length_x = (x2-x1)/2.0;
	half_length_y = (y2-y1)/2.0;

	for (int i = 0; i < M_length; ++i) {
	  x_gauss_side = half_pt_x + half_length_x * s_M_loc[i];
	  y_gauss_side = half_pt_y + half_length_y * s_M_loc[i];

	  scaling_fact_plus  = ( x_gauss_side + a ) / 2.0;
	  scaling_fact_minus = ( x_gauss_side - a ) / 2.0;

	  local_weights = half_length_y * scaling_fact_minus * w_M_loc[i];

	  term_1  = scaling_fact_plus;
	  term_2  = scaling_fact_minus;

	  for (int j = 0; j < N; ++j) {
	    rep_s_N = s_N[j];

	    x = term_1 + term_2 * rep_s_N;
	    y = y_gauss_side;

	    nodes_x[(*counter_ptr)+i][j] = rot_matrix[0] * x + rot_matrix[1] * y;
	    nodes_y[(*counter_ptr)+i][j] = rot_matrix[2] * x + rot_matrix[3] * y;
	  }

	  weights[(*counter_ptr)+i][0] = local_weights;
	}

	*counter_ptr += M_length;
      }
    }
  }

  double aux_weight;

  for (int i = 0; i < *counter_ptr; ++i) {
    aux_weight = weights[i][0];
    for (int j = 0; j < N; ++j) {
      weights[i][j] = aux_weight * w_N[j];
    }
  }

  delete[] s_N;
  delete[] w_N;
  delete[] s_M;
  delete[] w_M;
}
