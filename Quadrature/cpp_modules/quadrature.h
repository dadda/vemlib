#ifndef _QUADRATURE_H
#define _QUADRATURE_H

#include <ginac/ginac.h>

/* Function declarations for quadrature rules on polygons/polyhedra */

GiNaC::ex chinAlgorithm1D(
			  const GiNaC::ex & g,
			  const double * const vertices,
			  const double * const b,
			  const GiNaC::ex & sym_variable
			  );

GiNaC::ex chinAlgorithm2D(
			  const GiNaC::ex & g,
			  const double * const vertices,
			  const size_t Nver, 
			  const size_t * const polytope,
			  const size_t polytope_size,
			  const size_t * const facets_cumsizes,
			  const double * const a,
			  const double * const b,
			  const GiNaC::lst & sym_variables
			  );

GiNaC::ex chinAlgorithm3D(
			  const GiNaC::ex & g,
			  const double * const vertices,
			  const size_t Nver, 
			  const size_t * const polytope,
			  const size_t polytope_size,
			  const size_t * const facets_cumsizes,
			  const double * const a,
			  const double * const b,
			  const GiNaC::lst & sym_variables
			  );

void constructQuadrature(
			 const double * const vertices,
			 const size_t Nver,
			 const size_t * const vertexlist,
			 const size_t Nfc,
			 const size_t * const facets_cumsizes,
			 const int deg,
			 const double diameter,
			 const double volume,
			 const double * const centroid,
			 const double * const a,
			 const double * const b,
			 double * final_nodes,
			 double * final_weights
			 );

/* Function declarations for Stroud quadrature rules */

void StroudQuadrature(
		      int deg,
		      int dim,
		      double * nodes,
		      double * weights
		      );

void jacobi(
	    int nt,
	    double alpha,
	    double beta,
	    double t[],
	    double wts[]
	    );

double class_matrix(
		    int m,
		    double alpha,
		    double beta,
		    double aj[], 
		    double bj[]
		    );

double r8_epsilon();
double r8_abs(double x);
double r8_gamma(double x);
double r8_sign (double x);
double r8_max ( double x, double y );
void r8vec_reverse ( int n, double a[] );

void lobatto_compute (
		      int n,
		      double x[],
		      double w[]
		      );

void sgqf (
	   int nt,
	   double aj[],
	   double bj[],
	   double zemu,
	   double t[], 
	   double wts[]
	   );

void imtqlx (
	     int n,
	     double d[],
	     double e[],
	     double z[]
	     );

void legendre_compute_glr (
			   int n,
			   double x[],
			   double w[]
			   );

void legendre_compute_glr0 (
			    int n,
			    double *p,
			    double *pp
			    );

void legendre_compute_glr1 (
			    int n,
			    double *roots,
			    double *ders
			    );

void legendre_compute_glr2 (
			    double p,
			    int n,
			    double *roots,
			    double *ders
			    );

void rescale (
	      double a,
	      double b,
	      int n,
	      double x[],
	      double w[]
	      );

double rk2_leg (
		double t,
		double tn,
		double x,
		int n
		);

double ts_mult (
		double *u,
		double h,
		int n
		);

template<class T> int argmax(T *ptr, int n);

void auto_rotation(
		   double * const polygon_bd,
		   const int m,
		   double * const max_distance_ptr,
		   double * const rot_matrix,
		   double * const axis_abscissa
		   );

void polygauss_2013(
		    double * const polygon_bd,
		    const int m,
		    const int N,
		    double ** nodes_x,
		    double ** nodes_y,
		    double ** weights,
		    int * counter_ptr,
		    double * max_distance_ptr
		    );

#endif
