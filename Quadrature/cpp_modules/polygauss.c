#include "polygauss.h"


void autoRotation( double * const polygon_bd,
		   const int Nver,
		   double * const diam,
		   double * const rot_matrix,
		   double * const axis_abscissa )
{
  int i, j, pos1, pos2;
  double rot_angle, rot_angle_x, rot_angle_y, tmp1, tmp2, tmp3;

  double vertex_1[2], vertex_2[2], direction_axis[2];

  *diam = 0.0;
  pos1  = 0;
  pos2  = 0;
  for (j = 0; j < Nver; ++j) {
    for (i = j+1; i < Nver; ++i) {
      tmp1 = polygon_bd[j] - polygon_bd[i];
      tmp2 = polygon_bd[j+Nver] - polygon_bd[i+Nver];
      tmp3 = tmp1*tmp1 + tmp2*tmp2;
      if ( tmp3 > *diam ) {
	*diam = tmp3;
	pos1  = j;
	pos2  = i;
      }
    }
  }
  *diam = sqrt( *diam );

  vertex_1[0] = polygon_bd[pos2]; vertex_1[1] = polygon_bd[pos2+Nver];
  vertex_2[0] = polygon_bd[pos1]; vertex_2[1] = polygon_bd[pos1+Nver];

  tmp1 = (vertex_2[0]-vertex_1[0])/(*diam);
  tmp1 = tmp1 > -1.0 ? tmp1 : -1.0;
  direction_axis[0] = tmp1 < 1.0 ? tmp1 : 1.0;

  tmp1 = (vertex_2[1]-vertex_1[1])/(*diam);
  tmp1 = tmp1 > -1.0 ? tmp1 : -1.0;
  direction_axis[1] = tmp1 < 1.0 ? tmp1 : 1.0;

  rot_angle_x = acos( direction_axis[0] );
  rot_angle_y = acos( direction_axis[1] );

  if (rot_angle_y <= PI/2.0) {
    rot_angle = rot_angle_x <= PI/2.0 ? -rot_angle_y : rot_angle_y;
  } else {
    rot_angle = rot_angle_x <= PI/2.0 ? PI-rot_angle_y : rot_angle_y;
  }

  rot_matrix[0] = cos(rot_angle);
  rot_matrix[1] = -sin(rot_angle);
  rot_matrix[2] = sin(rot_angle);
  rot_matrix[3] = cos(rot_angle);

  for (i = 0; i < Nver; ++i) {
    tmp1 = polygon_bd[i];
    tmp2 = polygon_bd[i+Nver];
    polygon_bd[i]      = tmp1 * rot_matrix[0] + tmp2 * rot_matrix[2];
    polygon_bd[i+Nver] = tmp1 * rot_matrix[1] + tmp2 * rot_matrix[3];
  }

  axis_abscissa[0] = rot_matrix[0] * vertex_1[0] + rot_matrix[2] * vertex_1[1];
  axis_abscissa[1] = rot_matrix[1] * vertex_1[0] + rot_matrix[3] * vertex_1[1];
}


void polygauss2013( const int deg,
		    const double * const polygon_bd,
		    const int Nver,
		    double ** xq,
		    double ** yq,
		    double ** wq,
		    int * const Nq,
		    double * const diam )
{
  int i, j, M, l, counter, counter2, itmp;
  int N = ceil((double)(deg+1)/2.0);

  double a;
  double rot_matrix[4], axis_abscissa[2];
  double *polygonCopy = NULL, *nodes = NULL, *weights = NULL;
  double *sN = NULL, *wN = NULL, *sM = NULL, *wM = NULL;

  double x1, x2, y1, y2;
  double *s_M_loc = NULL, *w_M_loc = NULL;
  int M_length;

  double half_pt_x, half_pt_y, half_length_x, half_length_y;

  double tol = 1e-15;
  double local_weights;
  double x_gauss_side, y_gauss_side, scaling_fact_plus, scaling_fact_minus;
  double term_1, term_2, rep_s_N, x, y, aux_weight;

  double **nodes_x = NULL, **nodes_y = NULL, **weights_tmp = NULL;

  polygonCopy = (double *) malloc (2 * Nver * sizeof(double));
  memcpy( polygonCopy, polygon_bd, 2 * Nver * sizeof(double) );
  
  /*
   * Polygon rotation
   */

  autoRotation(polygonCopy, Nver, diam, rot_matrix, axis_abscissa);

  /*
   * Compute nodes and weights of 1D Gauss-Legendre rule
   */

  StroudQuadrature( 2*N-1, 1, &itmp, &nodes, &weights );

  sN = (double *) malloc (N * sizeof(double));
  wN = (double *) malloc (N * sizeof(double));

  for (i = 0; i < N; ++i) {
    sN[i] = 2.0 * nodes[N+i] - 1.0;
    wN[i] = 2.0 * weights[i];
  }
  free(nodes);
  free(weights);

  M = N + 1;
  StroudQuadrature( 2*M-1, 1, &itmp, &nodes, &weights );

  sM = (double *) malloc (M * sizeof(double));
  wM = (double *) malloc (M * sizeof(double));
  for (i = 0; i < M; ++i) {
    sM[i] = 2.0 * nodes[M+i] - 1.0;
    wM[i] = 2.0 * weights[i];
  }
  free(nodes);
  free(weights);

  a = axis_abscissa[0];

  /* The maximum number of 2D quadrature nodes should be Nver*(N^2+N) */

  nodes_x     = (double **) malloc (Nver * (N+1) * sizeof(double *));
  nodes_y     = (double **) malloc (Nver * (N+1) * sizeof(double *));
  weights_tmp = (double **) malloc (Nver * (N+1) * sizeof(double *));
  for (i = 0; i < Nver*(N+1); ++i) {
    nodes_x[i]     = (double *) malloc (N * sizeof(double));
    nodes_y[i]     = (double *) malloc (N * sizeof(double));
    weights_tmp[i] = (double *) malloc (N * sizeof(double));
  }

  counter = 0;
  for (l = 0; l < Nver; ++l) {
    x1 = polygonCopy[l];
    x2 = polygonCopy[(l+1)%Nver];
    y1 = polygonCopy[l+Nver];
    y2 = polygonCopy[(l+1)%Nver+Nver];

    if ( (fabs(x1-a) >= tol) || (fabs(x2-a) >= tol) ) {
      if ( fabs(y2-y1) > tol ) {
	if ( fabs(x2-x1) > tol ) {
	  s_M_loc  = sM;
	  w_M_loc  = wM;
	  M_length = M;
	} else {
	  s_M_loc  = sN;
	  w_M_loc  = wN;
	  M_length = N;
	}

	half_pt_x     = (x1+x2)/2.0;
	half_pt_y     = (y1+y2)/2.0;
        half_length_x = (x2-x1)/2.0;
	half_length_y = (y2-y1)/2.0;

	for (i = 0; i < M_length; ++i) {
	  x_gauss_side = half_pt_x + half_length_x * s_M_loc[i];
	  y_gauss_side = half_pt_y + half_length_y * s_M_loc[i];

	  scaling_fact_plus  = ( x_gauss_side + a ) / 2.0;
	  scaling_fact_minus = ( x_gauss_side - a ) / 2.0;

	  local_weights = half_length_y * scaling_fact_minus * w_M_loc[i];

	  term_1  = scaling_fact_plus;
	  term_2  = scaling_fact_minus;

	  for (j = 0; j < N; ++j) {
	    rep_s_N = sN[j];

	    x = term_1 + term_2 * rep_s_N;
	    y = y_gauss_side;

	    nodes_x[counter+i][j] = rot_matrix[0] * x + rot_matrix[1] * y;
	    nodes_y[counter+i][j] = rot_matrix[2] * x + rot_matrix[3] * y;
	  }

	  weights_tmp[counter+i][0] = local_weights;
	}

	counter += M_length;
      }
    }
  }

  for (i = 0; i < counter; ++i) {
    aux_weight = weights_tmp[i][0];
    for (j = 0; j < N; ++j) {
      weights_tmp[i][j] = aux_weight * wN[j];
    }
  }

  *Nq = counter*N;
  *xq = (double *) malloc ((*Nq) * sizeof(double));
  *yq = (double *) malloc ((*Nq) * sizeof(double));
  *wq = (double *) malloc ((*Nq) * sizeof(double));

  counter2 = 0;
  for (j = 0; j < N; ++j) {
    for (i = 0; i < counter; ++i, ++counter2) {
      (*xq)[counter2] = nodes_x[i][j];
      (*yq)[counter2] = nodes_y[i][j];
      (*wq)[counter2] = weights_tmp[i][j];
    }
  }

  for (i = 0; i < Nver*(N+1); ++i) {
    free(nodes_x[i]);
    free(nodes_y[i]);
    free(weights_tmp[i]);
  }

  free(polygonCopy);
  free(sN);
  free(wN);
  free(sM);
  free(wM);
  free(nodes_x);
  free(nodes_y);
  free(weights_tmp);
}
