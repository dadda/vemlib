/*==========================================================
 * constructQuadrature4Matlab.cpp - MATLAB External Interfaces
 *
 * This file uses the extension .cpp.  Other common C++ extensions such
 * as .C, .cc, and .cxx are also supported.
 *
 * The calling syntax is:
 *
 *    [nodes, weights] = constructQuadrature4Matlab(
 *                                                  vertices,
 *                                                  vertexlist,
 *                                                  Nfc,
 *                                                  facets_cumsizes,
 *                                                  deg,
 *                                                  diameter,
 *                                                  volume,
 *                                                  centroid,
 *                                                  a,
 *                                                  b
 *                                                 )
 *
 * INPUTS:
 * 
 *
 * OUTPUT:
 *
 *
 *========================================================
 *
 * This is a MEX-file for MATLAB.
 *
 *========================================================*/
#include "quadrature.h"
#include "mex.h"

void _main();

/****************************/

void mexFunction(
		 int          nlhs,
		 mxArray      *plhs[],
		 int          nrhs,
		 const mxArray *prhs[]
		 )
{
  /* Check for proper number of arguments */

  if (nrhs != 10) {
    mexErrMsgIdAndTxt("MATLAB:constructQuadrature4Matlab:nargin", 
		      "CHININTEGRATE4MATLAB requires ten input arguments.");
  } else if (nlhs > 3) {
    mexErrMsgIdAndTxt("MATLAB:constructQuadrature4Matlab:nargout",
		      "CHININTEGRATE4MATLAB returns two output arguments.");
  }

  /* Validate input arguments */

  if ( !mxIsDouble(prhs[0]) )
    {
      mexErrMsgIdAndTxt("MATLAB:constructQuadrature4Matlab:notDouble", 
			"First argument must be an array of doubles.");
    }
  else if ( !mxIsUint32(prhs[1]) && !mxIsUint64(prhs[1]) )
    {
      mexErrMsgIdAndTxt("MATLAB:constructQuadrature4Matlab:notUint", 
  			"Second argument must be an array of unsigned 32-bit or 64-bit integers.");
    }
  else if ( !mxIsUint32(prhs[2]) && !mxIsUint64(prhs[2]) )
    {
      mexErrMsgIdAndTxt("MATLAB:constructQuadrature4Matlab:notUint", 
  			"Third argument must be either a 32-bit or 64-bit unsigned integer.");
    }
  else if ( !mxIsUint32(prhs[3]) && !mxIsUint64(prhs[3]) )
    {
      mexErrMsgIdAndTxt("MATLAB:constructQuadrature4Matlab:notUint", 
  			"Fourth argument must be an array of unsigned 32-bit or 64-bit integers.");
    }

  // Vertices array (in column-major order)
  double * vertices = mxGetPr(prhs[0]);

  // Number of vertices
  size_t Nver = mxGetM(prhs[0]);

  // Polytope connectivity
  size_t * vertexlist = (size_t *) mxGetData(prhs[1]);

  // Number of faces
  size_t Nfc = *((size_t *) mxGetData(prhs[2]));

  // Array with cumulative sum of facets sizes
  size_t * facets_cumsizes = (size_t *) mxGetData(prhs[3]);

  // Degree of the quadrature rule
  int deg = *((int *) mxGetData(prhs[4]));

  // Element diameter
  double diameter = *(mxGetPr(prhs[5]));

  // Element volume
  double volume = *(mxGetPr(prhs[6]));

  // Array with element centroid
  double * centroid = mxGetPr(prhs[7]);

  // Array with unit outer normals
  double * a = mxGetPr(prhs[8]);

  // Array with signed distances
  double * b = mxGetPr(prhs[9]);

  // Construct quadrature nodes and weights
  double * nodes;
  double * weights;

  /* Create the output matrices */

  mwSize d3 = ((deg+3)*(deg+2)*(deg+1))/6;

  // Quadrature nodes
  plhs[0] = mxCreateDoubleMatrix(d3,3,mxREAL);
  
  // Quadrature weights
  plhs[1] = mxCreateDoubleMatrix(d3,1,mxREAL);

  // Get pointers to the real data in the output matrices
  nodes   = mxGetPr(plhs[0]);
  weights = mxGetPr(plhs[1]);

  constructQuadrature(vertices, Nver, vertexlist, Nfc, facets_cumsizes,
                      deg, diameter, volume, centroid, a, b, nodes, weights);

  return;
}
