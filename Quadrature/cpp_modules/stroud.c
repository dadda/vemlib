#include "stroud.h"

/*******************************************************************************************
 * Function definitions for Stroud quadrature formulas
 *******************************************************************************************/

void StroudQuadrature(
		      const int deg,
		      const int dim,
		      int * Nq,
		      double ** nodes,
		      double ** weights
		      )
{
  int i, q1, q2, q3;
  int N = ceil((deg+1)/2.);
  
  double *t3 = NULL, *w3 = NULL;
  double *t2 = NULL, *w2 = NULL;
  double *t1 = NULL, *w1 = NULL;

  t3 = (double *) malloc (N * sizeof(double));
  w3 = (double *) malloc (N * sizeof(double));
  
  jacobi(N, 0.0, 0.0, t3, w3);
  
  for (i = 0; i < N; ++i)
    {
      t3[i] = 0.5 * (t3[i] + 1.0);
      w3[i] /= 2.0;
    }
  
  if (dim == 1)
    {
      *Nq = N;
      *nodes   = (double *) malloc (2 * (*Nq) * sizeof(double));
      *weights = (double *) malloc ((*Nq) * sizeof(double));
      for(i = 0; i < N; ++i)
	{
	  (*nodes)[i]   = 1.0 - t3[i];
	  (*nodes)[N+i] = t3[i];
	  (*weights)[i] = w3[i];
	}
    }
  else if (dim >= 2)
    {
      t2 = (double *) malloc (N * sizeof(double));
      w2 = (double *) malloc (N * sizeof(double));
      
      jacobi(N, 1.0, 0.0, t2, w2);
      for (i = 0; i < N; ++i)
	{
	  t2[i] = 0.5 * (t2[i] + 1.0);
	  w2[i] /= 4.0;
	}
      
      if (dim == 2)
	{
	  *Nq = N*N;
	  *nodes   = (double *) malloc (3 * (*Nq) * sizeof(double));
	  *weights = (double *) malloc ((*Nq) * sizeof(double));
	  for (q1 = 0; q1 < N; ++q1)
	    {
	      for (q2 = 0; q2 < N; ++q2)
		{
		  (*nodes)[q1 + q2*N]         = 1.0 - t2[q1] - (1.0 - t2[q1])*t3[q2];
		  (*nodes)[q1 + q2*N + N*N]   = t2[q1];
		  (*nodes)[q1 + q2*N + N*N*2] = (1.0 - t2[q1])*t3[q2];
		  (*weights)[q1 + q2*N]       = w2[q1]*w3[q2];
		}
	    }
	}
      else
	{
	  t1 = (double *) malloc (N * sizeof(double));
	  w1 = (double *) malloc (N * sizeof(double));
	  
	  jacobi(N, 2.0, 0.0, t1, w1);
	  for (i = 0; i < N; ++i)
	    {
	      t1[i] = 0.5 * (t1[i] + 1.0);
	      w1[i] /= 8.0;
	    }

	  *Nq = N*N*N;
	  *nodes   = (double *) malloc (4 * (*Nq) * sizeof(double));
	  *weights = (double *) malloc ((*Nq) * sizeof(double));

	  for (q1 = 0; q1 < N; ++q1)
	    {
	      for (q2 = 0; q2 < N; ++q2)
		{
		  for (q3 = 0; q3 < N; ++q3)
		    {
		      /* x */
		      (*nodes)[q1 + q2*N + q3*N*N + N*N*N]   = t1[q1];
		      /* y */
		      (*nodes)[q1 + q2*N + q3*N*N + N*N*N*2] = (1.0 - t1[q1])*t2[q2];
		      /* z */
		      (*nodes)[q1 + q2*N + q3*N*N + N*N*N*3] = (1.0 - t1[q1])*(1.0 - t2[q2])*t3[q3];
		      /* w */
		      (*weights)[q1 + q2*N + q3*N*N] = w1[q1] * w2[q2] * w3[q3];
		      /* 1-x-y-z */
		      (*nodes)[q1 + q2*N + q3*N*N] = 1.0
			- (*nodes)[q1 + q2*N + q3*N*N + N*N*N]
			- (*nodes)[q1 + q2*N + q3*N*N + N*N*N*2]
			- (*nodes)[q1 + q2*N + q3*N*N + N*N*N*3];
		    }
		}
	    }

	  free( t1 );
	  free( w1 );
	}

      free( t2 );
      free( w2 );
    }

  free( t3 );
  free( w3 );

  return;
}

      
void jacobi(
	    const int nt,
	    const double alpha,
	    const double beta,
	    double * const t,
	    double * const wts
	    )
{
  double *aj = NULL;
  double *bj = NULL;
  double zemu;
  /*
   *  Get the Jacobi matrix and zero-th moment.
   */

  aj = (double *) malloc (nt * sizeof(double));
  bj = (double *) malloc (nt * sizeof(double));

  zemu = class_matrix ( nt, alpha, beta, aj, bj );
  /*
   *  Compute the knots and weights.
   */
  sgqf ( nt, aj, bj, zemu, t, wts );

  free( aj );
  free( bj );

  return;
}


double class_matrix(
		    const int m,
		    const double alpha,
		    const double beta,
		    double * const aj, 
		    double * const bj
		    )
/****************************************************************************
*
*  Purpose:
*
*    CLASS_MATRIX computes the Jacobi matrix for a quadrature rule.
*
*  Discussion:
*
*    This routine computes the diagonal AJ and sub-diagonal BJ
*    elements of the order M tridiagonal symmetric Jacobi matrix
*    associated with the polynomials orthogonal with respect to
*    the weight function specified by KIND.
*
*    For weight functions 1-7, M elements are defined in BJ even
*    though only M-1 are needed.  For weight function 8, BJ(M) is
*    set to zero.
*
*    The zero-th moment of the weight function is returned in ZEMU.
*
*  Licensing:
*
*    This code is distributed under the GNU LGPL license. 
*
*  Modified:
*
*    16 February 2010
*
*  Author:
*
*    Original FORTRAN77 version by Sylvan Elhay, Jaroslav Kautsky.
*    C++ version by John Burkardt.
*
*  Reference:
*
*    Sylvan Elhay, Jaroslav Kautsky,
*    Algorithm 655: IQPACK, FORTRAN Subroutines for the Weights of 
*    Interpolatory Quadrature,
*    ACM Transactions on Mathematical Software,
*    Volume 13, Number 4, December 1987, pages 399-415.
*
*  Parameters:
*
*    Input, int M, the order of the Jacobi matrix.
*
*    Input, double ALPHA, the value of Alpha, if needed.
*
*    Input, double BETA, the value of Beta, if needed.
*
*    Output, double AJ[M], BJ[M], the diagonal and subdiagonal
*    of the Jacobi matrix.
*
*    Output, double CLASS_MATRIX, the zero-th moment.
*/
{
  double a2b2;
  double ab;
  double abi;
  int i;
  double pi = 3.14159265358979323846264338327950;
  double temp;
  double temp2;
  double zemu;

  temp = r8_epsilon ( );

  temp2 = 0.5;

  if ( 500.0 * temp < r8_abs ( pow ( r8_gamma ( temp2 ), 2 ) - pi ) )
    {
      fprintf( stderr, "\n" );
      fprintf( stderr, "CLASS_MATRIX - Fatal error!\n" );
      fprintf( stderr, "  Gamma function does not match machine parameters.\n" );
      exit ( 1 );
    }
  
  ab = alpha + beta;
  abi = 2.0 + ab;
  zemu = pow ( 2.0, ab + 1.0 ) * r8_gamma ( alpha + 1.0 ) 
    * r8_gamma ( beta + 1.0 ) / r8_gamma ( abi );
  aj[0] = ( beta - alpha ) / abi;
  bj[0] = sqrt ( 4.0 * ( 1.0 + alpha ) * ( 1.0 + beta ) 
		 / ( ( abi + 1.0 ) * abi * abi ) );
  a2b2 = beta * beta - alpha * alpha;
  
  for ( i = 2; i <= m; i++ )
    {
      abi = 2.0 * i + ab;
      aj[i-1] = a2b2 / ( ( abi - 2.0 ) * abi );
      abi = abi * abi;
      bj[i-1] = sqrt ( 4.0 * i * ( i + alpha ) * ( i + beta ) * ( i + ab ) 
		       / ( ( abi - 1.0 ) * abi ) );
    }

  return zemu;
}


double r8_epsilon ( )

/****************************************************************************
*
*  Purpose:
*
*    R8_EPSILON returns the R8 roundoff unit.
*
*  Discussion:
*
*    The roundoff unit is a number R which is a power of 2 with the
*    property that, to the precision of the computer's arithmetic,
*      1 < 1 + R
*    but
*      1 = ( 1 + R / 2 )
*
*  Licensing:
*
*    This code is distributed under the GNU LGPL license.
*
*  Modified:
*
*    01 September 2012
*
*  Author:
*
*    John Burkardt
*
*  Parameters:
*
*    Output, double R8_EPSILON, the R8 round-off unit.
*/
{
  const double value = 2.220446049250313E-016;

  return value;
}


double r8_abs ( const double x )

/****************************************************************************
*
*  Purpose:
*
*    R8_ABS returns the absolute value of an R8.
*
*  Licensing:
*
*    This code is distributed under the GNU LGPL license. 
*
*  Modified:
*
*    14 November 2006
*
*  Author:
*
*    John Burkardt
*
*  Parameters:
*
*    Input, double X, the quantity whose absolute value is desired.
*
*    Output, double R8_ABS, the absolute value of X.
*/
{
  double value;

  if ( 0.0 <= x )
  {
    value = x;
  } 
  else
  {
    value = -x;
  }
  return value;
}


double r8_gamma ( const double x )

/****************************************************************************
*
*  Purpose:
*
*    R8_GAMMA evaluates Gamma(X) for a real argument.
*
*  Discussion:
*
*    This routine calculates the gamma function for a real argument X.
*
*    Computation is based on an algorithm outlined in reference 1.
*    The program uses rational functions that approximate the gamma
*    function to at least 20 significant decimal digits.  Coefficients
*    for the approximation over the interval (1,2) are unpublished.
*    Those for the approximation for 12 <= X are from reference 2.
*
*  Licensing:
*
*    This code is distributed under the GNU LGPL license. 
*
*  Modified:
*
*    18 January 2008
*
*  Author:
*
*    Original FORTRAN77 version by William Cody, Laura Stoltz.
*    C++ version by John Burkardt.
*
*  Reference:
*
*    William Cody,
*    An Overview of Software Development for Special Functions,
*    in Numerical Analysis Dundee, 1975,
*    edited by GA Watson,
*    Lecture Notes in Mathematics 506,
*    Springer, 1976.
*
*    John Hart, Ward Cheney, Charles Lawson, Hans Maehly,
*    Charles Mesztenyi, John Rice, Henry Thatcher,
*    Christoph Witzgall,
*    Computer Approximations,
*    Wiley, 1968,
*    LC: QA297.C64.
*
*  Parameters:
*
*    Input, double X, the argument of the function.
*
*    Output, double R8_GAMMA, the value of the function.
*/
{
  /*
   *  Coefficients for minimax approximation over (12, INF).
   */
  double c[7] = {
   -1.910444077728E-03, 
    8.4171387781295E-04, 
   -5.952379913043012E-04, 
    7.93650793500350248E-04, 
   -2.777777777777681622553E-03, 
    8.333333333333333331554247E-02, 
    5.7083835261E-03 };
  double eps = 2.22E-16;
  double fact;
  double half = 0.5;
  int i;
  int n;
  double one = 1.0;
  double p[8] = {
  -1.71618513886549492533811E+00,
   2.47656508055759199108314E+01, 
  -3.79804256470945635097577E+02,
   6.29331155312818442661052E+02, 
   8.66966202790413211295064E+02,
  -3.14512729688483675254357E+04, 
  -3.61444134186911729807069E+04,
   6.64561438202405440627855E+04 };
  bool parity;
  double q[8] = {
  -3.08402300119738975254353E+01,
   3.15350626979604161529144E+02, 
  -1.01515636749021914166146E+03,
  -3.10777167157231109440444E+03, 
   2.25381184209801510330112E+04,
   4.75584627752788110767815E+03, 
  -1.34659959864969306392456E+05,
  -1.15132259675553483497211E+05 };
  const double r8_pi = 3.1415926535897932384626434;
  double res;
  double sqrtpi = 0.9189385332046727417803297;
  double sum;
  double twelve = 12.0;
  double two = 2.0;
  double value;
  double xbig = 171.624;
  double xden;
  double xinf = 1.79E+308;
  double xminin = 2.23E-308;
  double xnum;
  double y;
  double y1;
  double ysq;
  double z;
  double zero = 0.0;;

  parity = false;
  fact = one;
  n = 0;
  y = x;
  /*
   *  Argument is negative.
   */
  if ( y <= zero )
  {
    y = - x;
    y1 = ( double ) ( int ) ( y );
    res = y - y1;

    if ( res != zero )
    {
      if ( y1 != ( double ) ( int ) ( y1 * half ) * two )
      {
        parity = true;
      }

      fact = - r8_pi / sin ( r8_pi * res );
      y = y + one;
    }
    else
    {
      res = xinf;
      value = res;
      return value;
    }
  }
  /*
   *  Argument is positive.
   */
  if ( y < eps )
  {
    /*
     *  Argument < EPS.
     */
    if ( xminin <= y )
    {
      res = one / y;
    }
    else
    {
      res = xinf;
      value = res;
      return value;
    }
  }
  else if ( y < twelve )
  {
    y1 = y;
    /*
     *  0.0 < argument < 1.0.
     */
    if ( y < one )
    {
      z = y;
      y = y + one;
    }
    /*
     *  1.0 < argument < 12.0.
     *  Reduce argument if necessary.
     */
    else
    {
      n = ( int ) ( y ) - 1;
      y = y - ( double ) ( n );
      z = y - one;
    }
    /*
     *  Evaluate approximation for 1.0 < argument < 2.0.
     */
    xnum = zero;
    xden = one;
    for ( i = 0; i < 8; i++ )
    {
      xnum = ( xnum + p[i] ) * z;
      xden = xden * z + q[i];
    }
    res = xnum / xden + one;
    /*
     *  Adjust result for case  0.0 < argument < 1.0.
     */
    if ( y1 < y )
    {
      res = res / y1;
    }
    /*
     *  Adjust result for case 2.0 < argument < 12.0.
     */
    else if ( y < y1 )
    {
      for ( i = 1; i <= n; i++ )
      {
        res = res * y;
        y = y + one;
      }
    }
  }
  else
  {
    /*
     *  Evaluate for 12.0 <= argument.
     */
    if ( y <= xbig )
    {
      ysq = y * y;
      sum = c[6];
      for ( i = 0; i < 6; i++ )
      {
        sum = sum / ysq + c[i];
      }
      sum = sum / y - y + sqrtpi;
      sum = sum + ( y - half ) * log ( y );
      res = exp ( sum );
    }
    else
    {
      res = xinf;
      value = res;
      return value;
    }
  }
  /*
   *  Final adjustments and return.
   */
  if ( parity )
  {
    res = - res;
  }

  if ( fact != one )
  {
    res = fact / res;
  }

  value = res;

  return value;
}


void sgqf ( const int nt,
	    const double * const aj,
	    double * const bj,
	    const double zemu,
	    double * const t,
	    double * const wts )

/***************************************************************************
*
*  Purpose:
*
*    SGQF computes knots and weights of a Gauss Quadrature formula.
*
*  Discussion:
*
*    This routine computes all the knots and weights of a Gauss quadrature
*    formula with simple knots from the Jacobi matrix and the zero-th
*    moment of the weight function, using the Golub-Welsch technique.
*
*  Licensing:
*
*    This code is distributed under the GNU LGPL license. 
*
*  Modified:
*
*    08 January 2010
*
*  Author:
*
*    Original FORTRAN77 version by Sylvan Elhay, Jaroslav Kautsky.
*    C++ version by John Burkardt.
*
*  Reference:
*
*    Sylvan Elhay, Jaroslav Kautsky,
*    Algorithm 655: IQPACK, FORTRAN Subroutines for the Weights of 
*    Interpolatory Quadrature,
*    ACM Transactions on Mathematical Software,
*    Volume 13, Number 4, December 1987, pages 399-415.
*
*  Parameters:
*
*    Input, int NT, the number of knots.
*
*    Input, double AJ[NT], the diagonal of the Jacobi matrix.
*
*    Input/output, double BJ[NT], the subdiagonal of the Jacobi 
*    matrix, in entries 1 through NT-1.  On output, BJ has been overwritten.
*
*    Input, double ZEMU, the zero-th moment of the weight function.
*
*    Output, double T[NT], the knots.
*
*    Output, double WTS[NT], the weights.
*/
{
  int i;
  /*
   *  Exit if the zero-th moment is not positive.
   */
  if ( zemu <= 0.0 )
  {
    fprintf( stderr, "\n" );
    fprintf( stderr, "SGQF - Fatal error!\n" );
    fprintf( stderr, "  ZEMU <= 0.\n" );
    exit ( 1 );
  }
  /*
   *  Set up vectors for IMTQLX.
   */
  for ( i = 0; i < nt; i++ )
  {
    t[i] = aj[i];
  }
  wts[0] = sqrt ( zemu );
  for ( i = 1; i < nt; i++ )
  {
    wts[i] = 0.0;
  }
  /*
   *  Diagonalize the Jacobi matrix.
   */
  imtqlx ( nt, t, bj, wts );

  for ( i = 0; i < nt; i++ )
  {
    wts[i] = wts[i] * wts[i];
  }

  return;
}


void imtqlx ( const int n,
	      double * const d,
	      double * const e,
	      double * const z )

/****************************************************************************
*
*  Purpose:
*
*    IMTQLX diagonalizes a symmetric tridiagonal matrix.
*
*  Discussion:
*
*    This routine is a slightly modified version of the EISPACK routine to 
*    perform the implicit QL algorithm on a symmetric tridiagonal matrix. 
*
*    The authors thank the authors of EISPACK for permission to use this
*    routine. 
*
*    It has been modified to produce the product Q' * Z, where Z is an input 
*    vector and Q is the orthogonal matrix diagonalizing the input matrix.  
*    The changes consist (essentially) of applying the orthogonal transformations
*    directly to Z as they are generated.
*
*  Licensing:
*
*    This code is distributed under the GNU LGPL license. 
*
*  Modified:
*
*    08 January 2010
*
*  Author:
*
*    Original FORTRAN77 version by Sylvan Elhay, Jaroslav Kautsky.
*    C++ version by John Burkardt.
*
*  Reference:
*
*    Sylvan Elhay, Jaroslav Kautsky,
*    Algorithm 655: IQPACK, FORTRAN Subroutines for the Weights of 
*    Interpolatory Quadrature,
*    ACM Transactions on Mathematical Software,
*    Volume 13, Number 4, December 1987, pages 399-415.
*
*    Roger Martin, James Wilkinson,
*    The Implicit QL Algorithm,
*    Numerische Mathematik,
*    Volume 12, Number 5, December 1968, pages 377-383.
*
*  Parameters:
*
*    Input, int N, the order of the matrix.
*
*    Input/output, double D(N), the diagonal entries of the matrix.
*    On output, the information in D has been overwritten.
*
*    Input/output, double E(N), the subdiagonal entries of the 
*    matrix, in entries E(1) through E(N-1).  On output, the information in
*    E has been overwritten.
*
*    Input/output, double Z(N).  On input, a vector.  On output,
*    the value of Q' * Z, where Q is the matrix that diagonalizes the
*    input symmetric tridiagonal matrix.
*/
{
  double b;
  double c;
  double f;
  double g;
  int i;
  int ii;
  int itn = 30;
  int j;
  int k;
  int l;
  int m;
  int mml;
  double p;
  double prec;
  double r;
  double s;

  prec = r8_epsilon ( );

  if ( n == 1 )
  {
    return;
  }

  e[n-1] = 0.0;

  for ( l = 1; l <= n; l++ )
  {
    j = 0;
    for ( ; ; )
    {
      for ( m = l; m <= n; m++ )
      {
        if ( m == n )
        {
          break;
        }

        if ( fabs ( e[m-1] ) <= prec * ( fabs ( d[m-1] ) + fabs ( d[m] ) ) )
        {
          break;
        }
      }
      p = d[l-1];
      if ( m == l )
      {
        break;
      }
      if ( itn <= j )
      {
        fprintf( stderr, "\n" );
        fprintf( stderr, "IMTQLX - Fatal error!\n" );
        fprintf( stderr, "  Iteration limit exceeded\n" );
        exit ( 1 );
      }
      j = j + 1;
      g = ( d[l] - p ) / ( 2.0 * e[l-1] );
      r = sqrt ( g * g + 1.0 );
      g = d[m-1] - p + e[l-1] / ( g + fabs ( r ) * r8_sign ( g ) );
      s = 1.0;
      c = 1.0;
      p = 0.0;
      mml = m - l;

      for ( ii = 1; ii <= mml; ii++ )
      {
        i = m - ii;
        f = s * e[i-1];
        b = c * e[i-1];

        if ( fabs ( g ) <= fabs ( f ) )
        {
          c = g / f;
          r = sqrt ( c * c + 1.0 );
          e[i] = f * r;
          s = 1.0 / r;
          c = c * s;
        }
        else
        {
          s = f / g;
          r = sqrt ( s * s + 1.0 );
          e[i] = g * r;
          c = 1.0 / r;
          s = s * c;
        }
        g = d[i] - p;
        r = ( d[i-1] - g ) * s + 2.0 * c * b;
        p = s * r;
        d[i] = g + p;
        g = c * r - b;
        f = z[i];
        z[i] = s * z[i-1] + c * f;
        z[i-1] = c * z[i-1] - s * f;
      }
      d[l-1] = d[l-1] - p;
      e[l-1] = g;
      e[m-1] = 0.0;
    }
  }
  /*
   *  Sorting.
   */
  for ( ii = 2; ii <= m; ii++ )
  {
    i = ii - 1;
    k = i;
    p = d[i-1];

    for ( j = ii; j <= n; j++ )
    {
      if ( d[j-1] < p )
      {
         k = j;
         p = d[j-1];
      }
    }

    if ( k != i )
    {
      d[k-1] = d[i-1];
      d[i-1] = p;
      p = z[i-1];
      z[i-1] = z[k-1];
      z[k-1] = p;
    }
  }
  return;
}


double r8_sign ( const double x )

/****************************************************************************
*
*  Purpose:
*
*    R8_SIGN returns the sign of an R8.
*
*  Licensing:
*
*    This code is distributed under the GNU LGPL license. 
*
*  Modified:
*
*    18 October 2004
*
*  Author:
*
*    John Burkardt
*
*  Parameters:
*
*    Input, double X, the number whose sign is desired.
*
*    Output, double R8_SIGN, the sign of X.
*/
{
  double value;

  if ( x < 0.0 )
  {
    value = -1.0;
  } 
  else
  {
    value = 1.0;
  }
  return value;
}


void lobatto_compute ( const int n,
		       double * const x,
		       double * const w )

/******************************************************************************/
/*
  Purpose:
  
    LOBATTO_COMPUTE computes a Lobatto quadrature rule.
  
  Discussion:
    
    The integral:
  
      Integral ( -1 <= X <= 1 ) F(X) dX
  
    The quadrature rule:
  
      Sum ( 1 <= I <= N ) WEIGHT(I) * F ( XTAB(I) )
  
    The quadrature rule will integrate exactly all polynomials up to
    X^(2*N-3).
  
    The Lobatto rule is distinguished by the fact that both endpoints
    (-1 and 1) are always abscissas.
  
  Licensing:
  
    This code is distributed under the GNU LGPL license.
  
  Modified:
  
    28 May 2014
  
  Author:
  
    Original MATLAB version by Greg von Winckel.
    C version by John Burkardt.
  
  Reference:
  
    Milton Abramowitz, Irene Stegun,
    Handbook of Mathematical Functions,
    National Bureau of Standards, 1964,
    ISBN: 0-486-61272-4,
    LC: QA47.A34.
  
    Claudio Canuto, Yousuff Hussaini, Alfio Quarteroni, Thomas Zang,
    Spectral Methods in Fluid Dynamics,
    Springer, 1993,
    ISNB13: 978-3540522058,
    LC: QA377.S676.
  
    Arthur Stroud, Don Secrest,
    Gaussian Quadrature Formulas,
    Prentice Hall, 1966,
    LC: QA299.4G3S7.
  
    Daniel Zwillinger, editor,
    CRC Standard Mathematical Tables and Formulae,
    30th Edition,
    CRC Press, 1996,
    ISBN: 0-8493-2479-3.
  
  Parameters:
  
    Input, int N, the order.
  
    Output, double X[N], the abscissas.

    Output, double W[N], the weights.
*/
{
  int i;
  int j;
  double *p;
  const double r8_pi = 3.141592653589793;
  double test;
  double tolerance;
  double *xold;

  if ( n == 1 )
  {
    x[0] = -1.0;
    w[0] = 2.0;
    return;
  }
  tolerance = 100.0 * r8_epsilon ( );
/*
  Initial estimate for the abscissas is the Chebyshev-Gauss-Lobatto nodes.
*/
  for ( i = 0; i < n; i++ )
  {
    x[i] = cos ( r8_pi * ( double ) ( i ) / ( double ) ( n - 1 ) );
  }

  xold = ( double * ) malloc ( n * sizeof ( double ) );
  p = ( double * ) malloc ( n * n * sizeof ( double ) );

  do
  {
    for ( i = 0; i < n; i++ )
    {
      xold[i] = x[i];
    }
    for ( i = 0; i < n; i++ )
    {
      p[i+0*n] = 1.0;
    }
    for ( i = 0; i < n; i++ )
    {
      p[i+1*n] = x[i];
    }

    for ( j = 2; j <= n-1; j++ )
    {
      for ( i = 0; i < n; i++)
      {
        p[i+j*n] = ( ( double ) ( 2 * j - 1 ) * x[i] * p[i+(j-1)*n]
                   + ( double ) (   - j + 1 ) *        p[i+(j-2)*n] )
                   / ( double ) (     j     );
      }
    }

    for ( i = 0; i < n; i++ )
    {
      x[i] = xold[i] - ( x[i] * p[i+(n-1)*n] - p[i+(n-2)*n] )
           / ( ( double ) ( n ) * p[i+(n-1)*n] );
    }

    test = 0.0;
    for ( i = 0; i < n; i++ )
    {
      test = r8_max ( test, fabs ( x[i] - xold[i] ) );
    }

  } while ( tolerance < test );

  r8vec_reverse ( n, x );

  for ( i = 0; i < n; i++ )
  {
    w[i] = 2.0 / ( ( double ) ( ( n - 1 ) * n ) * pow ( p[i+(n-1)*n], 2 ) );
  }

  free ( p );
  free ( xold );

  return;
}


double r8_max ( const double x,
		const double y )

/******************************************************************************/
/*
  Purpose:
  
    R8_MAX returns the maximum of two R8's.
  
  Licensing:
  
    This code is distributed under the GNU LGPL license.
  
  Modified:
  
    18 August 2004
  
  Author:
  
    John Burkardt
  
  Parameters:
  
    Input, double X, Y, the quantities to compare.
  
    Output, double R8_MAX, the maximum of X and Y.
*/
{
  double value;

  if ( y < x )
  {
    value = x;
  }
  else
  {
    value = y;
  }
  return value;
}


void r8vec_reverse ( const int n,
		     double * const a )

/******************************************************************************/
/*
  Purpose:
  
    R8VEC_REVERSE reverses the elements of an R8VEC.
  
  Discussion:
  
    An R8VEC is a vector of double precision values.
  
    Input:
  
      N = 5,
      A = ( 11.0, 12.0, 13.0, 14.0, 15.0 )
  
    Output:
  
      A = ( 15.0, 14.0, 13.0, 12.0, 11.0 )
  
  Licensing:
  
    This code is distributed under the GNU LGPL license.
  
  Modified:
  
    30 April 2006
  
  Author:
  
    John Burkardt
  
  Parameters:
  
    Input, int N, the number of entries in the array.
  
    Input/output, double A[N], the array to be reversed.
*/
{
  int i;
  double temp;

  for ( i = 0; i < n/2; i++ )
  {
    temp     = a[i];
    a[i]     = a[n-1-i];
    a[n-1-i] = temp;
  }
  return;
}
