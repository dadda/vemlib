#ifndef _STROUD_H
#define _STROUD_H

#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <stdbool.h>

void StroudQuadrature(
		      const int deg,
		      const int dim,
		      int * Nq,
		      double ** nodes,
		      double ** weights
		      );

void jacobi(
	    const int nt,
	    const double alpha,
	    const double beta,
	    double * const t,
	    double * const wts
	    );

double class_matrix(
		    const int m,
		    const double alpha,
		    const double beta,
		    double * const aj, 
		    double * const bj
		    );

double r8_epsilon();
double r8_abs ( const double x );
double r8_gamma ( const double x );
double r8_sign ( const double x );
double r8_max ( const double x,
		const double y );
void r8vec_reverse ( const int n,
		     double * const a );

void sgqf ( const int nt,
	    const double * const aj,
	    double * const bj,
	    const double zemu,
	    double * const t,
	    double * const wts );

void imtqlx ( const int n,
	      double * const d,
	      double * const e,
	      double * const z );

void lobatto_compute ( const int n,
		       double * const x,
		       double * const w );
#endif
