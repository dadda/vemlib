#ifndef _POLYGAUSS_H
#define _POLYGAUSS_H

#include <math.h>
#include <string.h>
#include "stroud.h"

#define PI 3.14159265358979323846

void autoRotation( double * const polygon_bd,
		   const int Nver,
		   double * const diam,
		   double * const rot_matrix,
		   double * const axis_abscissa );

void polygauss2013( const int deg,
		    const double * const polygon_bd,
		    const int Nver,
		    double ** xq,
		    double ** yq,
		    double ** wq,
		    int * const Nq,
		    double * const diam );

#endif

