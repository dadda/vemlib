/*==========================================================
 * polygauss2013Mex.cpp - MATLAB External Interfaces
 *
 * This file uses the extension .cpp.  Other common C++ extensions such
 * as .C, .cc, and .cxx are also supported.
 *
 * The calling syntax is:
 *
 *    [nodes_x, nodes_y, weights, max_distance] = polygauss2013Mex(
 *                                                                 N,
 *                                                                 boundary_pts
 *                                                                )
 *
 * INPUT:
 *
 * N            : degree of the 1 dimensional Gauss-Legendre rule.
 *
 * BOUNDARY_PTS : a matrix containing polygon vertices, ordered
 *                counterclockwise. The last row must have the components of
 *                the first vertex. In other words, the first row and the
 *                last one are equal.
 *
 *
 *========================================================
 *
 * This is a MEX-file for MATLAB.
 *
 *========================================================*/
#include <iostream>
#include "quadrature.h"
#include "mex.h"

using namespace std;

void _main();

/****************************/

void mexFunction(
		 int          nlhs,
		 mxArray      *plhs[],
		 int          nrhs,
		 const mxArray *prhs[]
		 )
{
  /* Check for proper number of arguments */

  if (nrhs != 2) {
    mexErrMsgIdAndTxt("MATLAB:polygauss2013Mex:nargin", 
		      "POLYGAUSS2013MEX requires ten input arguments.");
  } else if (nlhs > 4) {
    mexErrMsgIdAndTxt("MATLAB:polygauss2013Mex:nargout",
		      "POLYGAUSS2013MEX returns at most four arguments.");
  }

  /* Validate input arguments */

  if ( (mxGetM(prhs[0]) > 1) || (mxGetN(prhs[0]) > 1) )
    {
      mexErrMsgIdAndTxt("MATLAB:polygauss2013Mex:notScalar", 
			"First argument must be a scalar.");
    }

  if ( (mxGetM(prhs[1]) < 3) || (mxGetN(prhs[1]) != 2) )
    {
      mexErrMsgIdAndTxt("MATLAB:polygauss2013Mex:mathdims", 
			"Second argument must be an array representing a closed polygon.");
    }

  // Degree of the 1 dimensional Gauss-Legendre rule.
  int N = *((int *) mxGetData(prhs[0]));

  // A matrix containing polygon vertices, ordered
  // counterclockwise. The last row must have the components of
  // the first vertex. In other words, the first row and the
  // last one are equal.
  double * polygon_bd = mxGetPr(prhs[1]);

  int m = mxGetM(prhs[1]);
  int L = m-1;

  double ** nodes_x = new double*[L*(N+1)];
  double ** nodes_y = new double*[L*(N+1)];
  double ** weights = new double*[L*(N+1)];

  for (int i = 0; i < L*(N+1); ++i) {
    nodes_x[i] = new double[N];
    nodes_y[i] = new double[N];
    weights[i] = new double[N];
  }

  int counter;
  double max_distance;

  // cout << "Checking the polygon boundary ..." << endl;
  // for (int i = 0; i < m; ++i)
  //   cout << polygon_bd[i] << ' ' << polygon_bd[i+m] << endl;

  polygauss_2013(polygon_bd, m, N, nodes_x, nodes_y, weights,
		 &counter, &max_distance);

  /* Create the output matrices */

  mwSize nrow = counter*N;

  // Quadrature nodes, x
  plhs[0] = mxCreateDoubleMatrix(nrow,1,mxREAL);
  
  // Quadrature nodes, y
  plhs[1] = mxCreateDoubleMatrix(nrow,1,mxREAL);

  // Quadrature weights
  plhs[2] = mxCreateDoubleMatrix(nrow,1,mxREAL);

  int counter2 = 0;
  double *final_nodes_x, *final_nodes_y, *final_weights;

  final_nodes_x = mxGetPr( plhs[0] );
  final_nodes_y = mxGetPr( plhs[1] );
  final_weights = mxGetPr( plhs[2] );

  for (int j = 0; j < N; ++j) {
    for (int i = 0; i < counter; ++i, ++counter2) {
      final_nodes_x[counter2] = nodes_x[i][j];
      final_nodes_y[counter2] = nodes_y[i][j];
      final_weights[counter2] = weights[i][j];
    }
  }

  if (nlhs == 4)
    plhs[3] = mxCreateDoubleScalar( max_distance );

  for (int i = 0; i < L*(N+1); ++i) {
    delete[] nodes_x[i];
    delete[] nodes_y[i];
    delete[] weights[i];
  }

  delete[] nodes_x;
  delete[] nodes_y;
  delete[] weights;

  return;
}
