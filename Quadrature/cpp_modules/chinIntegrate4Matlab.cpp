/*==========================================================
 * chinIntegrate4Matlab.cpp - MATLAB External Interfaces
 *
 * This file uses the extension .cpp.  Other common C++ extensions such
 * as .C, .cc, and .cxx are also supported.
 *
 * The calling syntax is:
 *
 *		chinIntegrate4Matlab(
 *                                   gstr,
 *                                   vertices,
 *                                   vertexlist,
 *                                   Nfc,
 *                                   facets_cumsizes,
 *                                   a,
 *                                   b
 *                                  )
 *
 * INPUTS:
 * 
 * - gstr: a string representing the polynomial to be integrated.
 *         C++ notation must be used.
 *
 * - vertices: Nver x 3 double array with vertices coordinates.
 *
 * - vertexlist: 1d unsigned array with element to node connectivity
 *             information stored sequentially one facet after the other.
 *
 * - Nfc: an unsigned integer denoting the number of facets.
 *
 * - facets_cumsizes: Nfc+1 1d unsigned array with cumulative sums of facets nodes cardinality,
 *                    starting with 0. It is useful to correctly read vertexlist array.
 *
 * - a: Nfc x 3 double array of unit outer normals.
 *
 * - b: Nfc x 1 double array of algebraic distances.
 *
 * OUTPUT:
 *
 * - The value of the integral.
 *
 *========================================================
 *
 * This is a MEX-file for MATLAB.
 *
 *========================================================*/

#include <ginac/ginac.h>
#include <iostream>
#include "quadrature.h"
#include "mex.h"

using namespace std;
using namespace GiNaC;

void _main();

/****************************/

void mexFunction(
		 int          nlhs,
		 mxArray      *plhs[],
		 int          nrhs,
		 const mxArray *prhs[]
		 )
{
  /* Check for proper number of arguments */

  if (nrhs != 7) {
    mexErrMsgIdAndTxt("MATLAB:chinIntegrate4Matlab:nargin", 
		      "CHININTEGRATE4MATLAB requires seven input arguments.");
  } else if (nlhs > 1) {
    mexErrMsgIdAndTxt("MATLAB:chinIntegrate4Matlab:nargout",
		      "CHININTEGRATE4MATLAB returns one output argument.");
  }

  /* Validate input arguments */

  if ( !mxIsChar(prhs[0]) )
    {
      mexErrMsgIdAndTxt("MATLAB:chinIntegrate4Matlab:notChar", 
			"First argument must be a string.");
    }
  else if ( !mxIsDouble(prhs[1]) )
    {
      mexErrMsgIdAndTxt("MATLAB:chinIntegrate4Matlab:notDouble", 
			"Second argument must be an array of doubles.");
    }
  else if ( !mxIsUint32(prhs[2]) && !mxIsUint64(prhs[2]) )
    {
      mexErrMsgIdAndTxt("MATLAB:chinIntegrate4Matlab:notUint", 
  			"Third argument must be an array of unsigned 32-bit or 64-bit integers.");
    }
  else if ( !mxIsUint32(prhs[4]) && !mxIsUint64(prhs[4]) )
    {
      mexErrMsgIdAndTxt("MATLAB:chinIntegrate4Matlab:notUint", 
  			"Fifth argument must be an array of unsigned 32-bit or 64-bit integers.");
    }
  else if ( !mxIsDouble(prhs[5]) )
    {
      mexErrMsgIdAndTxt("MATLAB:chinIntegrate4Matlab:notDouble", 
			"Sixth argument must be an array of doubles.");
    }
  else if ( !mxIsDouble(prhs[6]) )
    {
      mexErrMsgIdAndTxt("MATLAB:chinIntegrate4Matlab:notDouble", 
			"Seventh argument must be an array of doubles.");
    }

  // Vertices array (in column-major order)
  double * vertices = mxGetPr(prhs[1]);

  // Number of vertices
  size_t Nver = mxGetM(prhs[1]);

  // Dimension
  int d = mxGetN(prhs[1]);

  // Vertexlist connectivity
  size_t * vertexlist = (size_t *) mxGetData(prhs[2]);

  // Number of faces
  size_t Nfc = *((size_t *) mxGetData(prhs[3]));

  // Array with cumulative sum of facets sizes
  size_t * facets_cumsizes = (size_t *) mxGetData(prhs[4]);

  // Array with unit outer normals
  double * a = mxGetPr(prhs[5]);

  // Array with signed distances
  double * b = mxGetPr(prhs[6]);

  // List with symbolic variables
  lst l;

  /* Check consistency */
  if ( mxGetM(prhs[5]) != Nfc || mxGetN(prhs[5]) != d || mxGetNumberOfElements(prhs[6]) != Nfc )
    {
      mexErrMsgIdAndTxt("MATLAB:chinIntegrate4Matlab:mismatch", 
		      "Either the sixth and/or seventh arguments have inconsistent dimensions.");
    }
  if ( mxGetNumberOfElements(prhs[4]) != Nfc+1 || facets_cumsizes[0] != 0 )
    {
      mexErrMsgIdAndTxt("MATLAB:chinIntegrate4Matlab:mismatch", 
		      "The fifth argument must be an array of size Nfc+1 starting with 0.");
    }

  ex Int = 0;

  if (d == 2)
    {
      symbol x("x"), y("y");

      symtab table;
      table["x"] = x; table["y"] = y;
      parser reader(table);
      ex g = reader( mxArrayToString(prhs[0]) );

      l = {x, y};

      Int = chinAlgorithm2D(g, vertices, Nver, vertexlist, Nfc, facets_cumsizes, a, b, l);
    }
  else if (d == 3)
    {
      symbol x("x"), y("y"), z("z");

      symtab table;
      table["x"] = x; table["y"] = y; table["z"] = z;
      parser reader(table);
      ex g = reader( mxArrayToString(prhs[0]) );

      l = {x, y, z};

      Int = chinAlgorithm3D(g, vertices, Nver, vertexlist, Nfc, facets_cumsizes, a, b, l);
    }

  if (is_a<numeric>(Int))
    {
      plhs[0] = mxCreateDoubleScalar( ex_to<numeric>(Int).to_double() );
    }
  else
    {
      mexErrMsgIdAndTxt("MATLAB:chinIntegrate4Matlab:unknownError", 
		      "Something went wrong.");
    }

  return;
}
