#include <stdio.h>
#include "polygauss.h"

#define PI 3.14159265358979323846

double f2(double x, double y)
{
  return sqrt( pow(x-0.5,2) + pow(y-0.5,2) );
}

int main()
{
  int counter;

  double max_distance, myInt;
  double exact_result = 0.382597858232106;

  double *final_nodes_x = NULL;
  double *final_nodes_y = NULL;
  double *final_weights = NULL;

  double polygon_bd[8] = {0.0, 1.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0};
  polygauss2013( 39, polygon_bd, 4, &final_nodes_x, &final_nodes_y, &final_weights, &counter,
  		 &max_distance );

  myInt = 0.0;
  for (int i = 0; i < counter; ++i) {
    myInt += final_weights[i] * f2(final_nodes_x[i], final_nodes_y[i]);
  }
  
  printf("Test 1, relative error: %f\n", fabs(myInt-exact_result)/exact_result);
  
  free(final_nodes_x);
  free(final_nodes_y);
  free(final_weights);

  return 0;
}
