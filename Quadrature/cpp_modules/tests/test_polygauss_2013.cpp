#include <cmath>
#include <stdio.h>
#include <cblas.h>
#include <iostream>
#include "quadrature.h"

#define PI 3.14159265358979323846

using namespace std;

template<class T>
int argmax(T *ptr, int n)
{
  T maxVal = ptr[0];
  int argMax = 0;

  for (int i = 1; i < n; ++i) {
    if (ptr[i] > maxVal) {
      maxVal = ptr[i];
      argMax = i;
    }
  }

  return argMax;
}

double f2(double x, double y)
{
  return sqrt( pow(x-0.5,2) + pow(y-0.5,2) );
}


void auto_rotation(double * const polygon_bd,
		   const int m,
		   double * const max_distance_ptr,
		   double * const rot_matrix,
		   double * const axis_abscissa)
{
  double rot_angle;
  double * distances = new double[m*m];

  for (int i = 0; i < m*m; ++i)
    distances[i] = 0.0;

  int linearIdx = 0;
  for (int j = 0; j < m; ++j) {
    for (int i = j+1; i < m; ++i) {
      linearIdx = m*j+i;
      distances[linearIdx] = sqrt( (polygon_bd[j] - polygon_bd[i])*(polygon_bd[j] - polygon_bd[i]) +
				   (polygon_bd[j+m] - polygon_bd[i+m])*(polygon_bd[j+m] - polygon_bd[i+m]) );
      cout << linearIdx << ' ' << polygon_bd[j] << ' ' << polygon_bd[j+m] << ", "
	   << polygon_bd[i] << ' ' << polygon_bd[i+m] << ", " << distances[linearIdx] << endl;
    }
  }

  int maxIdx = argmax(distances, m*m);
  *max_distance_ptr = distances[maxIdx];

  int pos1   = maxIdx/m;
  int pos2   = maxIdx - m*pos1;

  cout << "Checking positions: " << pos1 << ' ' << pos2 << endl;

  double vertex_1[2];
  double vertex_2[2];

  vertex_1[0] = polygon_bd[pos2]; vertex_1[1] = polygon_bd[pos2+m];
  vertex_2[0] = polygon_bd[pos1]; vertex_2[1] = polygon_bd[pos1+m];

  double direction_axis[2];
  
  direction_axis[0] = min( max( (vertex_2[0]-vertex_1[0])/(*max_distance_ptr), -1. ), 1. );
  direction_axis[1] = min( max( (vertex_2[1]-vertex_1[1])/(*max_distance_ptr), -1. ), 1. );

  cout << "Direction axis: " << direction_axis[0] << ' ' << direction_axis[1] << endl;

  double rot_angle_x = acos( direction_axis[0] );
  double rot_angle_y = acos( direction_axis[1] );

  if (rot_angle_y <= PI/2.) {
    if (rot_angle_x <= PI/2.)
      rot_angle = -rot_angle_y;
    else
      rot_angle = rot_angle_y;
  } else {
    if (rot_angle_x <= PI/2.)
      rot_angle = PI-rot_angle_y;
    else
      rot_angle = rot_angle_y;
  }

  cout << "Rotation angle: " << rot_angle << endl;

  rot_matrix[0] = cos(rot_angle);
  rot_matrix[1] = -sin(rot_angle);
  rot_matrix[2] = sin(rot_angle);
  rot_matrix[3] = cos(rot_angle);

  double * polygon_bd_rot = new double[2*m];

  cblas_dgemm(CblasColMajor, CblasNoTrans, CblasTrans, m, 2, 2,
	      1.0, polygon_bd, m, rot_matrix, 2, 0.0, polygon_bd_rot, m);

  for (int i = 0; i < 2*m; ++i)
    polygon_bd[i] = polygon_bd_rot[i];

  axis_abscissa[0] = rot_matrix[0] * vertex_1[0] + rot_matrix[2] * vertex_1[1];
  axis_abscissa[1] = rot_matrix[1] * vertex_1[0] + rot_matrix[3] * vertex_1[1];

  cout << "Checking output of auto_rotation() ..." << endl;
  cout << "Vertex 1: " << vertex_1[0] << ", " << vertex_1[1] << endl;
  cout << "Vertex 2: " << vertex_2[0] << ", " << vertex_2[1] << endl;
  cout << "MaxIdx, max distance: " << maxIdx << ", " << *max_distance_ptr << endl;
  cout << "Rotation matrix (in column major order): "
       << rot_matrix[0] << ", " << rot_matrix[1] << ", "
       << rot_matrix[2] << ", " << rot_matrix[3] << endl;
  cout << "Axis abscissa: " << axis_abscissa[0] << ", " << axis_abscissa[1] << endl;
  cout << "Rotated polygon:" << endl;
  for (int i = 0; i < m; ++i) {
    cout << polygon_bd[i] << ' ' << polygon_bd[i+m] << endl;
  }

  delete[] distances;
  delete[] polygon_bd_rot;
}

void polygauss_2013(double * const polygon_bd,
		    const int m,
		    const int N,
		    double ** nodes_x,
		    double ** nodes_y,
		    double ** weights,
		    int * counter_ptr,
		    double * max_distance_ptr)
{
  double rot_matrix[4];
  double axis_abscissa[2];

  // --------------------------------------------------------------------------
  // Polygon rotation

  auto_rotation(polygon_bd, m, max_distance_ptr, rot_matrix, axis_abscissa);

  // --------------------------------------------------------------------------
  // Compute nodes and weights of 1D Gauss-Legendre rule

  double * s_N = new double[N];
  double * w_N = new double[N];

  legendre_compute_glr(N, s_N, w_N);
  rescale(-1.0, 1.0, N, s_N, w_N);

  cout << "Checking s_N, w_N ..." << endl;
  for (int i = 0; i < N; ++i) {
    cout << s_N[i] << ' ' << w_N[i] << endl;
  }

  int M = N+1;
  double * s_M = new double[M];
  double * w_M = new double[M];

  legendre_compute_glr(M, s_M, w_M);
  rescale(-1.0, 1.0, M, s_M, w_M);

  cout << "Checking s_M, w_M ..." << endl;
  for (int i = 0; i < M; ++i) {
    cout << s_M[i] << ' ' << w_M[i] << endl;
  }

  int L = m-1;
  double a = axis_abscissa[0];

  // The maximum number of 2D quadrature nodes should be L*(N^2+N);

  *counter_ptr = 0;
  double x1, x2, y1, y2;
  double *s_M_loc, *w_M_loc;
  int M_length;

  double half_pt_x, half_pt_y, half_length_x, half_length_y;

  double local_weights;
  double x_gauss_side, y_gauss_side, scaling_fact_plus, scaling_fact_minus;
  double term_1, term_2, rep_s_N, x, y;

  for (int index_side = 0; index_side < L; ++index_side) {
    x1 = polygon_bd[index_side];
    x2 = polygon_bd[index_side+1];
    y1 = polygon_bd[index_side+m];
    y2 = polygon_bd[index_side+1+m];

    cout << "x1,x2,y1,y2: " << x1 << ' ' << x2 << ' ' << y1 << ' ' << y2 << endl;

    if ( (fabs(x1-a) >= 1e-15) || (fabs(x2-a) >= 1e-15) ) {
      if ( fabs(y2-y1) > 1e-15 ) {
	if ( fabs(x2-x1) > 1e-15 ) {
	  s_M_loc = s_M;
	  w_M_loc = w_M;
	  M_length = M;
	  cout << "s_M_loc = s_M" << endl;
	} else {
	  s_M_loc = s_N;
	  w_M_loc = w_N;
	  M_length = N;
	  cout << "s_M_loc = s_N" << endl;
	}

	half_pt_x =(x1+x2)/2.0;
	half_pt_y = (y1+y2)/2.0;
        half_length_x = (x2-x1)/2.0;
	half_length_y = (y2-y1)/2.0;

	// cout << "x_gauss_side, y_gauss_side, local_weights:" << endl;
	cout << "x, y" << endl;

	for (int i = 0; i < M_length; ++i) {
	  x_gauss_side = half_pt_x + half_length_x * s_M_loc[i];
	  y_gauss_side = half_pt_y + half_length_y * s_M_loc[i];

	  scaling_fact_plus  = ( x_gauss_side + a ) / 2.0;
	  scaling_fact_minus = ( x_gauss_side - a ) / 2.0;

	  local_weights = half_length_y * scaling_fact_minus * w_M_loc[i];

	  // cout << x_gauss_side << ' ' << y_gauss_side << ' ' << local_weights << endl;

	  term_1  = scaling_fact_plus;
	  term_2  = scaling_fact_minus;

	  for (int j = 0; j < N; ++j) {
	    rep_s_N = s_N[j];

	    x = term_1 + term_2 * rep_s_N;
	    y = y_gauss_side;

	    cout << "x(" << i << ',' << j << ") = " << x << "    "
		 << "y(" << i << ',' << j << ") = " << y << endl;

	    nodes_x[(*counter_ptr)+i][j] = rot_matrix[0] * x + rot_matrix[1] * y;
	    nodes_y[(*counter_ptr)+i][j] = rot_matrix[2] * x + rot_matrix[3] * y;
	  }

	  weights[(*counter_ptr)+i][0] = local_weights;
	}

	*counter_ptr += M_length;
      }
    }
  }

  double aux_weight;
  // double * aux_weights2 = new double[counter_ptr*N];

  for (int i = 0; i < *counter_ptr; ++i) {
    aux_weight = weights[i][0];
    for (int j = 0; j < N; ++j) {
      weights[i][j] = aux_weight * w_N[j];
    }
  }

  // for (int i = 0; i < *counter_ptr; ++i) {
  //   aux_weights2[i] = 0.0;
  // }

  // for (int i = 0; i < counter4Nodes; ++i) {
  //   weights[i] = aux_weights2[i];
  // }

  delete[] s_N;
  delete[] w_N;
  delete[] s_M;
  delete[] w_M;
  // delete[] aux_weights2;
}

int main()
{
  int L, m, N, counter;

  double max_distance, exact_result;

  // Unit square
  double polygon_bd[10] = {0.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0};
  m = 5;
  cout << "Checking the polygon boundary ..." << endl;
  for (int i = 0; i < m; ++i)
    cout << polygon_bd[i] << ' ' << polygon_bd[i+m] << endl;

  L = m-1;

  N = 20;
  double ** nodes_x = new double*[L*(N+1)];
  double ** nodes_y = new double*[L*(N+1)];
  double ** weights = new double*[L*(N+1)];

  for (int i = 0; i < L*(N+1); ++i) {
    nodes_x[i] = new double[N];
    nodes_y[i] = new double[N];
    weights[i] = new double[N];
  }

  polygauss_2013(polygon_bd, m, N, nodes_x, nodes_y, weights,
		 &counter, &max_distance);

  double * final_nodes_x = new double[counter*N];
  double * final_nodes_y = new double[counter*N];
  double * final_weights = new double[counter*N];

  int counter2 = 0;
  for (int j = 0; j < N; ++j) {
    for (int i = 0; i < counter; ++i, ++counter2) {
      final_nodes_x[counter2] = nodes_x[i][j];
      final_nodes_y[counter2] = nodes_y[i][j];
      final_weights[counter2] = weights[i][j];
    }
  }

  // Test 1
  exact_result = 0.382597858232106;
  double myInt = 0.0;
  for (int i = 0; i < counter2; ++i) {
    myInt += final_weights[i] * f2(final_nodes_x[i], final_nodes_y[i]);
  }

  cout << "Test 1, relative error: " << fabs(myInt-exact_result)/exact_result << endl;

  for (int i = 0; i < L*(N+1); ++i) {
    delete[] nodes_x[i];
    delete[] nodes_y[i];
    delete[] weights[i];
  }

  delete[] nodes_x;
  delete[] nodes_y;
  delete[] weights;

  delete[] final_nodes_x;
  delete[] final_nodes_y;
  delete[] final_weights;

  return 0;
}
