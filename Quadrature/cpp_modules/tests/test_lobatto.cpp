#include <iostream>
#include <cmath>
#include "quadrature.h"

using namespace std;

int main()
{
  int maxdeg, N;
  double *nodes = NULL, *weights = NULL;
  double Int1, Int2, Int3;

  cout << "Enter max degree to check: ";
  cin >> maxdeg;

  for (int i = 0; i < maxdeg; ++i) {
    N = i+2;
    nodes   = (double *) malloc (N * sizeof(double));
    weights = (double *) malloc (N * sizeof(double));
    cout << "N: " << N << endl;

    /* Quadrature nodes and weights in [-1,1] */
    lobatto_compute(N, nodes, weights);
    Int1 = 0.0; Int2 = 0.0; Int3 = 0.0;
    for (int j = 0; j < N; ++j) {
      Int1 += 0.5*weights[j]*pow(nodes[j]/2.+0.5,i+1);
      Int2 += 0.5*weights[j]*pow(nodes[j]/2.+0.5,i+2);
      Int3 += 0.5*weights[j]*pow(nodes[j]/2.+0.5,i+3);
    }
    cout << endl;
    cout << "Error on degree " << i+1 << ": " << fabs(Int1 - 1.0/((double)i + 2.0)) << endl;
    cout << "Error on degree " << i+2 << ": " << fabs(Int2 - 1.0/((double)i + 3.0)) << endl;
    cout << "Error on degree " << i+3 << ": " << fabs(Int3 - 1.0/((double)i + 4.0)) << endl;
    free(nodes);
    free(weights);
  }

  return 0;
}
