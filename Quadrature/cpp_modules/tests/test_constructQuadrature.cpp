#include <cmath>
#include <iostream>
#include <ginac/ginac.h>
#include "quadrature.h"

using namespace std;
using namespace GiNaC;

int main()
{
  // FILE *fp;
  // int Nelt, tmp, counter;
  // size_t Nver, Nfc, Nver_fc;
  // double *vertices;
  // size_t *vertexlist, *facets_cumsizes;

  // fp = fopen("unitcube_voro_Nelt8.txt", "w");

  // fscanf(fp, "%i", &Nelt);

  // for (int i = 0; i < Nelt; ++i) {
  //   fscanf(fp, "%i", &tmp);
  //   fscanf(fp, "%lu", &Nver);

  //   vertices = new double [Nver*3];

  //   for (int j = 0; j < Nver; ++j) {
  //     fscanf(fp, "%lf", vertices+j);
  //     fscanf(fp, "%lf", vertices+j+Nver);
  //     fscanf(fp, "%lf", vertices+j+2*Nver);
  //   }

  //   fscanf(fp, "%lu", &Nfc);

  //   facets_cumsizes = new size_t[Nfc+1];
  //   facets_cumsizes[0] = 0;

  //   for (int k = 0; k < Nfc; ++k) {
  //     fscanf(fp, "%i", &tmp);
  //     facets_cumsizes[k+1] = facets_cumsizes[k] + (size_t)tmp;
  //   }

  //   vertexlist = new size_t[facets_cumsizes[Nfc]];

  //   counter = 0;
  //   for (int k = 0; k < Nfc; ++k) {
  //     Nver_fc = facets_cumsizes[k+1] - facets_cumsizes[k];
  //     for (int j = 0; j < Nver_fc; ++j, ++counter) {
  // 	fscanf(fp, "%lu", vertexlist+counter);
  //     }
  //   }

  //   delete[] vertices;
  //   delete[] facets_cumsizes;
  //   delete[] vertexlist;
  // }

  size_t Nver = 8;

  double * vertices = new double [Nver*3];

  vertices[0] = 0.; vertices[8] = 0.; vertices[16] = 0.;
  vertices[1] = 5.; vertices[9] = 0.; vertices[17] = 0.;
  vertices[2] = 5.; vertices[10] = 5.; vertices[18] = 0.;
  vertices[3] = 0.; vertices[11] = 5.; vertices[19] = 0.;
  vertices[4] = 0.; vertices[12] = 0.; vertices[20] = 5.;
  vertices[5] = 5.; vertices[13] = 0.; vertices[21] = 5.;
  vertices[6] = 5.; vertices[14] = 5.; vertices[22] = 5.;
  vertices[7] = 0.; vertices[15] = 5.; vertices[23] = 5.;

  // vertices[0] = 0.848135320952049; vertices[10] = 0.153010313909124;  vertices[20] = 0.456200044287923;
  // vertices[1] = 0.795173485444189;  vertices[11] = 0.158446442154946;  vertices[21] = 0.500000000000000;
  // vertices[2] = 0.791987438195075;  vertices[12] = 0.154821551514866;  vertices[22] = 0.457256421133336;
  // vertices[3] = 0.824082353705804;  vertices[13] = 0.122389947146594;  vertices[23] = 0.500000000000000;
  // vertices[4] = 0.859070661090112;  vertices[14] = 0.151539647334534;  vertices[24] = 0.500000000000000;
  // vertices[5] = 0.818187096341783;  vertices[15] = 0.124543654686276;  vertices[25] = 0.470754178771444;
  // vertices[6] = 0.826731345154710;  vertices[16] = 0.179433823184624;  vertices[26] = 0.444370684566051;
  // vertices[7] = 0.842986316794742;  vertices[17] = 0.192030632375043;  vertices[27] = 0.473083900070852;
  // vertices[8] = 0.853314143293326;  vertices[18] = 0.155358003202997;  vertices[28] = 0.464341972631795;
  // vertices[9] = 0.845586053339372;  vertices[19] = 0.194740513002635;  vertices[29] = 0.500000000000000;

  size_t Nfc = 6;

  size_t * vertexlist = new size_t[Nfc * 4];

  vertexlist[0] = 1; vertexlist[1] = 4; vertexlist[2] = 3; vertexlist[3] = 2;
  vertexlist[4] = 1; vertexlist[5] = 2; vertexlist[6] = 6; vertexlist[7] = 5;
  vertexlist[8] = 2; vertexlist[9] = 3; vertexlist[10] = 7; vertexlist[11] = 6;
  vertexlist[12] = 3; vertexlist[13] = 4; vertexlist[14] = 8; vertexlist[15] = 7;
  vertexlist[16] = 4; vertexlist[17] = 1; vertexlist[18] = 5; vertexlist[19] = 8;
  vertexlist[20] = 5; vertexlist[21] = 6; vertexlist[22] = 7; vertexlist[23] = 8;

  // vertexlist[0] = 2; vertexlist[1] = 10; vertexlist[2] = 8; vertexlist[3] = 7;
  // vertexlist[4] = 3; vertexlist[5] = 2; vertexlist[6] = 4; vertexlist[7] = 5;
  // vertexlist[8] = 10; vertexlist[9] = 2; vertexlist[10] = 3; vertexlist[11] = 6;
  // vertexlist[12] = 4; vertexlist[13] = 3; vertexlist[14] = 7; vertexlist[15] = 1;
  // vertexlist[16] = 6; vertexlist[17] = 4; vertexlist[18] = 6; vertexlist[19] = 1;
  // vertexlist[20] = 9; vertexlist[21] = 5; vertexlist[22] = 5; vertexlist[23] = 9;
  // vertexlist[24] = 8; vertexlist[25] = 10; vertexlist[26] = 7; vertexlist[27] = 8;
  // vertexlist[28] = 9; vertexlist[29] = 1;

  size_t * facets_cumsizes = new size_t[Nfc+1];

  facets_cumsizes[0] = 0;
  facets_cumsizes[1] = 4;
  facets_cumsizes[2] = 8;
  facets_cumsizes[3] = 12;
  facets_cumsizes[4] = 16;
  facets_cumsizes[5] = 20;
  facets_cumsizes[6] = 24;

  // facets_cumsizes[0] = 0;
  // facets_cumsizes[1] = 5;
  // facets_cumsizes[2] = 9;
  // facets_cumsizes[3] = 13;
  // facets_cumsizes[4] = 17;
  // facets_cumsizes[5] = 22;
  // facets_cumsizes[6] = 26;
  // facets_cumsizes[7] = 30;

  int deg = 2;

  double diameter = 5.0 * std::sqrt(3.0);
  double volume   = 125.0;
  double centroid[] = {2.5, 2.5, 2.5};
  double a[] = { 0.,  0., 1., 0., -1., 0.,
		 0., -1., 0., 1.,  0., 0.,
		 -1., 0., 0., 0.,  0., 1.};
  double b[] = {0., 0., 5., 5., 0., 5.};

  // double diameter = 0.080830809852279;
  // double volume   = 9.179236779761114e-05;
  // double centroid[] = {0.827439949767551,   0.158205193043412,   0.479182130218656};
  // double a[] = {-0.584086637610917, 0., -0.775416526853208,  -0.030826352256697,
  // 		0.629342321286121, 0.947530268631810, 0.743363570035683,
  // 		0.811298005087353, 0., -0.621702531471410,
  // 		-0.429106435060522, -0.755397909588911, 0.295759675341150,
  // 		0.345830365826410, -0.025265524052647,
  // 		1., 0.110522270357371, -0.902727757076405, -0.182489015633547,
  // 		-0.121295525344619, -0.572548653667246};
  // double b[] = {-0.348535687023277, 0.5, -0.659836081340576, -0.503627071252243,
  // 		0.334932283366132, 0.798167008501086, 0.422191791730692};

  symbol x("x"), y("y"), z("z");
  lst l;
  l = {x, y, z};

  int d3 = ((deg+3)*(deg+2)*(deg+1))/6;

  double * nodes = new double[3*d3];
  double * weights = new double[d3];

  constructQuadrature(vertices, Nver, vertexlist, Nfc, facets_cumsizes,
  		      deg, diameter, volume, centroid, a, b, nodes, weights);

  for (int i = 0; i < d3; ++i)
    {
      cout.width(20);
      cout << nodes[i];
      cout.width(20);
      cout << nodes[i+d3];
      cout.width(20);
      cout << nodes[i+2*d3];

      cout.width(20);
      cout << weights[i] << endl;
    }

  // Test integration of a polynomial
  double integral = 0.0;
  for (size_t i = 0; i < d3; ++i)
    integral += weights[i] * ( nodes[i]*nodes[i] + nodes[i]*nodes[i+d3]
			       + nodes[i+d3]*nodes[i+d3] + nodes[i+2*d3]*nodes[i+2*d3] );
    // integral += weights[i]
    //   * (std::pow(nodes[i],6) + std::pow(nodes[i+d3],6)
    // 	 + std::pow(nodes[i+2*d3],6)
    // 	 + std::pow(nodes[i],2)*std::pow(nodes[i+d3],2)*std::pow(nodes[i+2*d3],2)
    // 	 + nodes[i]*std::pow(nodes[i+d3],5));

  cout << "\nIntegral: " << integral << endl;

  delete[] nodes;
  delete[] weights;
  delete[] vertices;
  delete[] vertexlist;
  delete[] facets_cumsizes;

  // fclose(fp);

  return 0;
}
