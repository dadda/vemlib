function Int = alg3( g, vertices, polytope, a, b, sym_variables )
% Integration of polynomial over arbitrary polytope
%
% INPUTS:
%
% - g: a multivariate polyomial expressed in terms of symbolic variables
%      x, y, and z whose highest degree is p.
%
% - vertices: d x Nver array, with d = 1,2, or 3, with the coordinates of
%             the vertices describing the current polytope.
%
% - a: UNIT normals

syms c

% Dimension of the polytope
d = size(vertices, 1);

% Number of hyperplanes
m = length(polytope);

% % Norms of normals to hyperplanes
% norm2a = sqrt( sum(a.^2, 1) );
% unit_a = bsxfun(@rdivide, a, norm2a);

g = subs(g, sym_variables(1), c*sym_variables(1));

% Build d-dimensional rotation matrix
if d == 2
    g = subs(g, sym_variables(2), c*sym_variables(2));
    
    % Rotation is performed so that an edge lies normal to the y-axis
    ct = a(2,:);
    st = a(1,:);

    R = zeros(2, 2, m);
    R(1,1,:) = ct; R(1,2,:) = -st;
    R(2,1,:) = st; R(2,2,:) = ct;

elseif d == 3
    g = subs(g, sym_variables(2), c*sym_variables(2));
    g = subs(g, sym_variables(3), c*sym_variables(3));

    omega = zeros(3, m);
    R     = zeros(3, 3, m);
    collapsed_variable = 3*ones(1, m);

    % By default, rotation is performed so that a face lies normal to the z-axis
    ct = a(3,:);
    Ix = find( abs(ct+1) < 0.5 | abs(ct-1) < 0.5 );
    Iz = setdiff(1:m, Ix);

    ct(Ix) = a(1,Ix);

    st = sqrt( 1.-ct.^2 );
    
    omega(1,Iz) = a(2,Iz)./st(Iz);
    omega(2,Iz) = -a(1,Iz)./st(Iz);

    % By default rotation is performed to align the outer normal to a
    % face with the unit vector of the z axis, BOTH direction AND
    % orientation. This is done to get the correct outer normals when
    % we project onto 2d.
    % When a is close to -e_z, we arbitrarily use Rodrigues's formula
    % with e_x

    omega(2,Ix) = a(3,Ix)./st(Ix);
    omega(3,Ix) = -a(2,Ix)./st(Ix);
    collapsed_variable(Ix) = 1;
    
    % Remark: st is always positive as we consider an angle of rotation
    % between 0 and pi. The correct sense of rotation is taken
    % into account by the vector about which rotation occurs
    
    % Compute rotation matrix
    R(1,1,:) = ct + omega(1,:).^2.*(1-ct);
    R(2,2,:) = ct + omega(2,:).^2.*(1-ct);
    R(3,3,:) = ct + omega(3,:).^2.*(1-ct);

    R(1,2,:) = -omega(3,:).*st + omega(1,:).*omega(2,:).*(1-ct);
    R(1,3,:) = omega(2,:).*st + omega(1,:).*omega(3,:).*(1-ct);
    R(2,1,:) = omega(3,:).*st + omega(1,:).*omega(2,:).*(1-ct);
    R(2,3,:) = -omega(1,:).*st + omega(2,:).*omega(3,:).*(1-ct);
    R(3,1,:) = -omega(2,:).*st + omega(1,:).*omega(3,:).*(1-ct);
    R(3,2,:) = omega(1,:).*st + omega(2,:).*omega(3,:).*(1-ct);

%    R = simplify(R);
end

% Break g into (at most) (p+1) homogeneous polynomials
flist = coeffs(g, c);

% Trick to understand the order of each homogeneous term of g.
% Remark: coeffs and sym2poly have opposite orderings, i.e., coeffs sorts
% coefficients from the lowest order term to the highest one, whereas
% sym2poly does the opposite. flist_check is needed because coeffs does not
% returns 0 in position j if there is no term of order j, j = 0, ..., p.

% flist_check is a polynomial in a only
flist_check = sym2poly( subs(g, num2cell(sym_variables), num2cell(ones(size(sym_variables))) ) );

% Max order of the polynomial g
p = length(flist_check) - 1;

flist_iterator = 0;
Int = 0;

for j = 0:p
    if flist_check(end-j)
        flist_iterator = flist_iterator + 1;
        if d > 1
            h = 0;
            if d == 3
                for i = 1:m
                    new_coords   = R(:,:,i)'*sym_variables;
                    new_f        = subs(flist(flist_iterator), num2cell(sym_variables), num2cell(new_coords));
                    new_f        = simplify(subs(new_f, sym_variables(collapsed_variable(i)), b(i)));

                    new_vertices = R(:,:,i)*vertices(:,polytope{i});
                    % After rotation the last dimension has collapsed
                    new_vertices = new_vertices([1:collapsed_variable(i)-1 collapsed_variable(i)+1:end],:);

                    poly_size = length(polytope{i});

                    new_polytope = [1:poly_size; 
                                   [2:poly_size 1]]';

                    new_a = [new_vertices(2, new_polytope(:,2)) - ...
                             new_vertices(2, new_polytope(:,1));
                             new_vertices(1, new_polytope(:,1)) - ...
                             new_vertices(1, new_polytope(:,2))];
                    norm2a = sqrt( sum(new_a.^2, 1) );
                    for p = 1:poly_size
                        new_a(:,p) = new_a(:,p)/norm2a(p);
                    end
%                    new_a = bsxfun(@rdivide, new_a, norm2a);
                    
%                     new_b = new_vertices(1, new_polytope(1,:)) .* ...
%                             new_vertices(2, new_polytope(2,:)) - ...
%                             new_vertices(2, new_polytope(1,:)) .* ...
%                             new_vertices(1, new_polytope(2,:));
%                     new_b = simplify(new_b./norm2a);
                    new_b = sum( new_a.*new_vertices(:, new_polytope(:,1)), 1);

                    new_polytope = mat2cell(new_polytope, ones(poly_size,1), 2);

                    g = alg3( new_f, new_vertices, new_polytope, new_a, new_b, ...
                              sym_variables([1:collapsed_variable(i)-1 collapsed_variable(i)+1:end]) );
                    h = h + b(i)*g;
                end
            else
                for i = 1:m
                    new_coords   = R(:,:,i)'*sym_variables;
                    new_f        = subs(flist(flist_iterator), num2cell(sym_variables), num2cell(new_coords));
                    new_f        = simplify(subs(new_f, sym_variables(2), b(i)));

                    new_vertices = R(:,:,i)*vertices(:,polytope{i});
                    new_vertices = new_vertices(1,:); % After rotation the last dimension collapses

                    new_polytope = {[1,2]};
                    
                    % Due to our choice of taking outer normals in 2d as
                    % well as in 3d and to our definition of rotation
                    % matrix in 2d, the outer normal (in 1d) at the first vertex
                    % will always be 1, whereas it will be -1 at the second
                    % vertex
                    new_a        = [1 -1];
                    
                    % Due to our choice of new_a, new_b has to be taken as
                    % the signed distance
                    new_b        = new_a .* new_vertices;

                    g = alg3( new_f, new_vertices, new_polytope, new_a, new_b, sym_variables(1));
                    h = h + b(i)*g;
                end                
            end
            h = h / (d+j);
            Int = Int + h;
        else
            Int = Int + 1/(1+j)*( b(1)*subs(flist(flist_iterator),sym_variables(1),vertices(1)) + ...
                                  b(2)*subs(flist(flist_iterator),sym_variables(1),vertices(2)) );
        end
    end
end

end






