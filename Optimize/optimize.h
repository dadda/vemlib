#ifndef _OPTIMIZE_H
#define _OPTIMIZE_H

#include <cstddef>

int nnls(double * A, size_t mda, size_t m, size_t n, double * b, double * x,
	 double & rnorm, double * w, double * zz, size_t * index, size_t & ip_end);

void h12(int mode, size_t lpivot, size_t l1, size_t m,
	 double * u, double & up, double * C, size_t ice, size_t icv, size_t ncv);

void nnls_dtrsm(const size_t ip_end, double * A, const size_t m, double * zz,
		size_t & jj, const size_t * const index);

void g1(double a, double b, double & cterm, double & sterm, double & sig);

#endif
