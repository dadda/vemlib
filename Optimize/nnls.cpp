#include <iostream>
#include <cmath>
#include "optimize.h"

using namespace std;

/* C implementation of algorithm NNLS

   The original F77 version of this code was developed by
   Charles L. Lawson and Richard J. Hanson at Jet Propulsion Laboratory
   1973 JUN 15, and published in the book
   "SOLVING LEAST SQUARES PROBLEMS", Prentice-HalL, 1974.
*/

int nnls(
	 double * A, size_t mda, size_t m, size_t n, double * b, double * x, double & rnorm,
	 double * w, double * zz, size_t * index, size_t & ip_end
	 )
{
  int mode;
  size_t i, j, izmax, l, ip;
  size_t iter = 0;
  size_t itermax = 3*n, ii, jj;
  double sm, wmax, Asave, up, dummy;
  double unorm, ztest, alpha, t, cc, ss, temp;
  double factor = 0.01;
  bool KTcondition = false, flag;

  mode = 0;

  // Initialize the arrays index[], x[]
  for (i = 0; i < n; ++i)
    {
      x[i] = 0.0;
      index[i] = i;
    }

  /* Arrays P and Z are defined as follows:
   *
   * index[0] through index[ip_end-1] = set P.
   * index[ip_end] through index[n-1] = set Z.
   */
  size_t iz;

  ip_end = 0;

  /************************************* Outer loop **************************************/

  /*
   * Continue as long as not all coefficients are already in the solution or if less than
   * m cols of A have been triangulated. Also check iteration counter, as well as and Kuhn-Tucker
   * conditions.
   */
  while ( (ip_end < n) && (ip_end < m) && (iter <= itermax) && (!KTcondition) )
    {
      // Compute components of the dual (negative gradient)  vector w
      for (iz = ip_end; iz < n; ++iz)
  	{
      	  // Update w[j], j \in Z
      	  j = index[iz];
      	  sm = 0.0;
      	  for (l = ip_end; l < m; ++l)
      	    sm += A[j*m+l]*b[l];
      	  w[j] = sm;
  	}
      /*
       * Select a variable to be moved from set Z to set P.
       * Continue until a candidate is found (i.e., when ztest > 0.0).
       */
      ztest = 0.0;
      while(ztest <= 0.0)
  	{
  	  // Find largest positive w[j]
  	  wmax = 0.0;
  	  for (iz = ip_end; iz < n; ++iz)
  	    {
  	      j = index[iz];
  	      if (w[j] > wmax)
  		{
  		  wmax  = w[j];
  		  izmax = iz;
  		}
  	    }
  	  /*
  	   * If wmax <= 0.0, go to termination.
  	   * This indicates satisfaction of the Kuhn-Tucker conditions.
  	   */
  	  if (wmax <= 0.0)
	    {
	      KTcondition = true;
	      break;
	    }

  	  iz = izmax;
  	  j  = index[iz];

  	  /*
  	   * The sign of w[j] is ok for j to be moved to set P.
  	   * Begin the transformation and check new diagonal element to avoid near linear
  	   * dependence.
  	   */
  	  Asave = A[m*j+ip_end];
  	  h12(1, ip_end, ip_end+1, m, &A[m*j], up, &dummy, 1, 1, 0);
  	  unorm = 0.0;
  	  if (ip_end > 0)
  	    {
  	      for (l = 0; l < ip_end; ++l)
  		unorm += pow(A[m*j+l],2);
  	    }
  	  unorm = sqrt(unorm);
  	  if ( ( (unorm+abs(A[m*j+ip_end])*factor) - unorm ) > 0.0 )
  	    {
  	      /*
  	       * Col j is sufficiently independent. Copy b into zz, update zz
  	       * and solve for ztest ( = proposed new value for x[j]).
  	       */
  	      for (l = 0; l < m; ++l)
  		zz[l] = b[l];
  	      h12(2, ip_end, ip_end+1, m, &A[m*j], up, zz, 1, 1, 1);
  	      ztest = zz[ip_end]/A[m*j+ip_end];

  	      // See if ztest is positive
  	      if (ztest > 0.0)
  		{
  		  /*
  		   * The index j = index[iz] has been selected to be moved from
  		   * set Z to set P. Update b, update indices, apply Householder
  		   * transformations to cols in new set Z, zero subdiagonal elts
  		   * in col j, set w[j] = 0.0.
  		   */
  		  for (l = 0; l < m; ++l)
  		    b[l] = zz[l];
  		  index[iz] = index[ip_end];
  		  index[ip_end] = j;
  		  ip_end += 1;

  		  if (ip_end < n)
  		    {
  		      for (size_t jz = ip_end; jz < n; ++jz)
  			{
  			  jj = index[jz];
  			  h12(2, ip_end-1, ip_end, m, &A[m*j], up, &A[m*jj], 1, mda, 1);
  			}
  		    }
  		  if (ip_end < m)
  		    {
  		      for (l = ip_end; l < m; ++l)
  			A[m*j+l] = 0.0;
  		    }

  		  w[j] = 0.0;

  		  /*
  		   * Solve the triangular system.
  		   * Store the solution temporarily in zz.
  		   */
  		  nnls_dtrsm(ip_end, A, m, zz, jj, index);

  		  /************************* Secondary loop begins here ***********************/
		  
  		  while (true)
  		    {
  		      // Iteration counter
  		      iter += 1;
  		      if (iter > itermax)
  			{
  			  mode = 2;
  			  break;
  			}
  		      /*
  		       * See if all new constrained coeffs are feasible.
  		       * If not, compute alpha.
  		       */
  		      alpha = 2.0;
  		      for (ip = 0; ip < ip_end; ++ip)
  			{
  			  l = index[ip];
  			  if (zz[ip] <= 0.0)
  			    {
  			      t = -x[l]/(zz[ip]-x[l]);
  			      if (alpha > t)
  				{
  				  alpha = t;
  				  jj = ip;
  				}
  			    }
  			}
  		      /*
  		       * If all new constrained coeffs are feasible then alpha will still = 2.
  		       * If so, exit from secondary loop to main loop (remember that
  		       * ztest != 0.0 here, so we will get out from the intermediate loop).
  		       */
  		      if (alpha == 2.0)
  			{
  			  for (ip = 0; ip < ip_end; ++ip)
  			    {
  			      i = index[ip];
  			      x[i] = zz[ip];
  			    }
  			  break;
  			}
  		      else
  			{
  			  /*
  			   * Otherwise use alpha which will be between 0. and 1.
  			   * to interpolate between the old x and the new zz.
  			   */
  			  for (ip = 0; ip < ip_end; ++ip)
  			    {
  			      l = index[ip];
  			      x[l] += alpha*(zz[ip]-x[l]);
  			    }
  			  /*
  			   * Modify A and b and the index arrays to move coefficient i
  			   * from set P to set Z.
  			   */
  			  i = index[jj];
  			  flag = true;
  			  while(flag)
  			    {
  			      x[i] = 0.0;

  			      if (jj != (ip_end-1))
  				{
  				  jj += 1;
  				  for (j = jj; j < ip_end; ++j)
  				    {
  				      ii = index[j];
  				      index[j-1] = ii;
  				      g1(A[m*ii+j-1], A[m*ii+j], cc, ss, A[m*ii+j-1]);
  				      A[m*ii+j] = 0.0;
  				      for (l = 0; l < n; ++l)
  					{
  					  if (l != ii)
  					    {
  					      // Apply procedure G2
  					      temp = A[m*l+j-1];
  					      A[m*l+j-1] = cc*temp + ss*A[m*l+j];
  					      A[m*l+j]   = -ss*temp + cc*A[m*l+j];
  					    }
  					}
  				      // Apply procedure G2
  				      temp = b[j-1];
  				      b[j-1] = cc*temp + ss*b[j];
  				      b[j]   = -ss*temp + cc*b[j];
  				    }
  				}
  			      ip_end -= 1;
  			      index[ip_end] = i;
  			      /*
  			       * See if the remaining coeffs in set P are feasible. They
  			       * should, because of the way alpha was determined.
  			       * In any are infeasible, it is due to roud-off error.
  			       * Any that are nonpositive will be set to zero and moved
  			       * from set P to set Z.
  			       */
  			      flag = false;
  			      for (jj = 0; jj < ip_end; ++jj)
  				{
  				  i = index[jj];
  				  if (x[i] <= 0.0)
  				    {
  				      flag = true;
  				      break;
  				    }
  				}
  			    }
  			  // Copy b into zz. Then solve again and loop back
  			  for (i = 0; i < m; ++i)
  			    zz[i] = b[i];
  			  nnls_dtrsm(ip_end, A, m, zz, jj, index);
  			}
  		    }

  		  /************************* Secondary loop ends here ***********************/

  		}
  	      else
  		{
  		  // Reject j as a candidate to be moved from set Z to set P.
  		  A[m*j+ip_end] = Asave;
  		  w[j] = 0.0;
  		}
  	    }
  	  else
  	    {
  	      // Reject j as a candidate to be moved from set Z to set P.
  	      A[m*j+ip_end] = Asave;
  	      w[j] = 0.0;
  	    }
  	}
    }

  // Compute the norm of the final residual vector
  sm = 0.0;
  if (ip_end < m)
    for (i = ip_end; i < m; ++i) sm += b[i]*b[i];
  else
    for (i = 0; i < n; ++i) w[i] = 0.0;
  rnorm = sqrt(sm);

  return mode;
}


void h12(int mode, size_t lpivot, size_t l1, size_t m,
	 double * u, double & up, double * C, size_t ice, size_t icv, size_t ncv)
{
  double sm, b, cl, clinv;
  size_t i2, i3, i4, incr;

  if ((lpivot < l1) && (l1 < m))
    {
      cl = fabs( *(u+lpivot) );
      if (mode == 1)
	{
	  // Construct the transformation

	  /*
	   * Perform Step 1 of (10.22) by making the computation of the square root
	   * of the sum of squares resistant to underflow.
	   */
	  for (size_t j = l1; j < m; ++j)
	    cl = fmax( abs(*(u+j)), cl );

	  if (cl > 0)
	    {
	      clinv = 1.0/cl;
	      sm    = pow((*(u+lpivot))*clinv,2);
	      for (size_t j = l1; j < m; ++j)
		sm += pow((*(u+j))*clinv,2);
	      cl *= sqrt(sm);
	      if (*(u+lpivot) > 0)
		cl = -cl;
	      up = *(u+lpivot) - cl;
	      *(u+lpivot) = cl;
	    }
	}
      else if (cl <= 0.0)
	{
	  return;
	}

      if (ncv > 0)
	{
	  b = up * (*(u+lpivot));
	  if (b < 0.0)
	    {
	      b = 1.0/b;
	      //i2 = ice*lpivot-icv;
	      incr = ice*(l1-lpivot);
	      for (size_t j = 0; j < ncv; ++j)
		{
		  i2 = ice*lpivot+j*icv; // C[i2] is now cpj of (10.22)
		  i3 = i2+incr;
		  i4 = i3;
		  sm = C[i2]*up;
		  for (size_t i = l1; i < m; ++i)
		    {
		      sm += C[i3] * (*(u+i));
		      i3 += ice;
		    }
		  if (sm != 0.0)
		    {
		      sm *= b;
		      C[i2] += sm*up;
		      for (size_t i = l1; i < m; ++i)
			{
			  // (10.22), step 10
			  C[i4] += sm * (*(u+i));
			  i4 += ice;
			}
		    }
		}
	    }
	}
    }

  return;
}

// Solve upper triangular system for nnls routine
void nnls_dtrsm(
		const size_t ip_end, double * A, const size_t m, double * zz,
		size_t & jj, const size_t * const index
		)
{
  size_t ip;

  for (size_t l = 0; l < ip_end; ++l)
    {
      // Start from the bottom
      ip = ip_end-1-l;
      if (l != 0)
	{
	  // Update RHS
	  for (size_t ii = 0; ii <= ip; ++ii)
	    zz[ii] -= A[m*jj+ii]*zz[ip+1];
	}
      jj = index[ip];
      zz[ip] /= A[m*jj+ip];
    }
}

/*
 * Compute orthogonal rotation matrix
 *
 * The original version of this code was developed by
 * Charles L. Lawson and Richard J. Hanson at Jet Propulsion Laboratory
 * 1973 JUN 12, and published in the book
 * "SOLVING LEAST SQUARES PROBLEMS", Prentice-HalL, 1974.
 * Revised FEB 1995 to accompany reprinting of the book by SIAM.
 *   
 *     COMPUTE.. MATRIX   (C, S) SO THAT (C, S)(A) = (SQRT(A**2+B**2))   
 *                        (-S,C)         (-S,C)(B)   (   0          )    
 *     COMPUTE SIG = SQRT(A**2+B**2) 
 *        SIG IS COMPUTED LAST TO ALLOW FOR THE POSSIBILITY THAT 
 *        SIG MAY BE IN THE SAME LOCATION AS A OR B .
 *     ------------------------------------------------------------------
 */
void g1(double a, double b, double & cterm, double & sterm, double & sig)
{
  double xr, yr;
  if (abs(a) > abs(b))
    {
      xr    = b/a;
      yr    = sqrt(1.0+pow(xr,2));
      cterm = a > 0 ? 1.0/yr : -1.0/yr;
      sterm = cterm * xr;
      sig   = abs(a)*yr;
    }
  else if (b != 0.0)
    {
      xr    = a/b;
      yr    = sqrt(1.0+pow(xr,2));
      sterm = b > 0 ? 1.0/yr : -1.0/yr;
      cterm = sterm * xr;
      sig   = abs(b)*yr;      
    }
  else
    {
      sig   = 0.0;
      cterm = 0.0;
      sterm = 1.0;
    }

  return;
}
