% Script for comparing NNLS and Matlab lsqnonneg

format long

m = 50; n = 100;

A = rand(m,n);
b = rand(m,1);

save('A.txt', '-ascii', '-double', 'A');
save('b.txt', '-ascii', '-double', 'b');

tic;
x = lsqnonneg(A,b);
[~,ix] = sort(x, 'descend');
toc;

disp('Complete lsqnonneg solution:');
disp(x);
disp('Selected lsqnonneg solution:');
disp(x(ix(1:m)));