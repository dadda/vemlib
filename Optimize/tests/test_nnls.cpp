#include <iostream>
#include <time.h>
#include <stdio.h>
#include "optimize.h"

using namespace std;

double* load_vector(const char * file, size_t sz);
double* load_mat(const char * file, size_t & m, size_t & n);

/* Main program */

int main(int argc, char** argv)
{
  size_t m, n;
  double * A;
  double * v;

  A = load_mat(argv[1], m, n);
  v = load_vector(argv[2], m);

  // /* Print matrix and vector to screen */

  // if ( A != NULL)
  //   {
  //     cout << "MATRIX A:" << endl;
  //     for (size_t i = 0; i < m; ++i)
  // 	{
  // 	  for (size_t j = 0; j < n; ++j)
  // 	    cout << ' ' << A[m*j+i];
  // 	  cout << endl;
  // 	}
  //     cout << endl;
  //   }

  // if ( v != NULL)
  //   {
  //     cout << "VECTOR b:" << endl;
  //     for (size_t i = 0; i < m; ++i)
  // 	cout << v[i] << endl;
  //   }
  cout << m << ' ' << n << endl;

  if ( A != NULL && v != NULL)
    {
      cout << endl << "Solving the NNLS problem...\n\n";

      double * x     = new double[n];
      double * w     = new double[n];
      double * zz    = new double[m];
      size_t * index = new size_t[n];
      double rnorm;
      size_t ip_end;

      time_t tm0 = time(0);
      clock_t ck0 = clock();
      nnls(A, m, m, n, v, x, rnorm, w, zz, index, ip_end);
      time_t tm1 = time(0);
      clock_t ck1 = clock();
      cout << "wall time = " << difftime(tm1,tm0) << " seconds.\n";
      cout << "CPU time = " << double(ck1-ck0)/CLOCKS_PER_SEC << " seconds.\n";

      cout << "\nNNLS solution, {x, w, x[sorted]}:\n";
      for (size_t j = 0; j < n; ++j)
	cout << x[j] << ' ' << w[j] << ' ' << x[index[j]] << endl;
      
      cout << "ip_end: " << ip_end << endl;

      delete[] x;
      delete[] w;
      delete[] zz;
      delete[] index;
      delete[] A;
      delete[] v;
    }

  return 0;
}

/* Routine definitions */

double* load_mat(const char* fn, size_t & m, size_t & n)
{
  FILE* fp = fopen(fn, "r");
  double * A;

  if (!fp) {
    cerr << "could not load matrix from " << fn << endl;
    return NULL;
  }

  fscanf(fp, "%zu %zu\n", &m, &n);

  A = new double[m*n];
  for (size_t i = 0; i < m; ++i)
    for (size_t j = 0; j < n; ++j)
      fscanf(fp, "%lf", &A[m*j+i]);

  return A;
}

double* load_vector(const char * fn, size_t sz)
{
  double* v = new double[sz];
  FILE* fp;

  fp = fopen(fn, "r");
  if (!fp) {
    cerr << "could not load vector from " << fn << endl;
    delete v;
    return NULL;
  }

  size_t i = 0;

  size_t m;
  
  // read size info, and skip
  fscanf(fp, "%zu", &m);
  if (m != sz) {
    cerr << "could not load vector from " << fn << endl;
    delete v;
    return NULL;
  }
  while (i < sz) {
    fscanf(fp, "%lf", &v[i]);
    i++;
  }

  fclose(fp);

  return v;
}
