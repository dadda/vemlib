function G = compute_G( k, vertices, element, element_diameter, element_centroid )

unique_nodes = unique(horzcat(element{:}));
Nver = length(unique_nodes);
Nfc  = uint64(length(element));

d3 = ((k+3)*(k+2)*(k+1))/6;
G  = zeros(d3, d3);

% Create strings corresponding to monomial basis functions
basis_template  = '(%d / %.16f) * pow((x-(%.16f))/%.16f,%d) * pow((y-(%.16f))/%.16f,%d) * pow((z-(%.16f))/%.16f,%d)';
basis_strings_x = cell(d3,1);
basis_strings_y = cell(d3,1);
basis_strings_z = cell(d3,1);

i = 2;
for q = 1:k
    for r = q:-1:0
        for s = q-r:-1:0
            t = q-r-s;
            basis_strings_x{i} = sprintf(basis_template, r, element_diameter, ...
                element_centroid(1), element_diameter, r-1, ...
                element_centroid(2), element_diameter, s, ...
                element_centroid(3), element_diameter, t);
            basis_strings_y{i} = sprintf(basis_template, s, element_diameter, ...
                element_centroid(1), element_diameter, r, ...
                element_centroid(2), element_diameter, s-1, ...
                element_centroid(3), element_diameter, t);
            basis_strings_z{i} = sprintf(basis_template, t, element_diameter, ...
                element_centroid(1), element_diameter, r, ...
                element_centroid(2), element_diameter, s, ...
                element_centroid(3), element_diameter, t-1);

            i = i + 1;
        end
    end
end

[~, ~, faces_normals, faces_algdist, ~, ~, ~, ~, ~, faces_orders] = ...
    faces_features(vertices, element, k, 0, 0, 0);
facets_cumsizes = uint64([0; cumsum(faces_orders)]);
    
if k == 1
    basis_on_vertices = monomial_basis_3d(vertices(unique_nodes,1), vertices(unique_nodes,2), vertices(unique_nodes,3), ...
                                          k, element_centroid(1), element_centroid(2), element_centroid(3), ...
                                          element_diameter, ones(Nver,1));
    basis_on_vertices = permute(basis_on_vertices, [1,3,2]);
    G(1,:) = sum(basis_on_vertices, 1)/Nver;
    for i = 2:d3
        for j = i:d3
            gij = ['(' basis_strings_x{i} ') * (' basis_strings_x{j} ')' ...
                ' + (' basis_strings_y{i} ') * (' basis_strings_y{j} ')' ...
                ' + (' basis_strings_z{i} ') * (' basis_strings_z{j} ')'];
            G(i,j) = chinIntegrate4Matlab(gij, vertices, uint64(horzcat(element{:})), Nfc, ...
                facets_cumsizes, faces_normals, faces_algdist);
            G(j,i) = G(i,j);
        end
    end
else
    error('Not implemented yet!')
end

end

