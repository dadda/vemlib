addpath ../../Mesh_Handlers
addpath ../../Quadrature
addpath ../../Quadrature/cpp_modules
addpath ../../Basis_Functions
addpath ../../VEM_diffusion
addpath ../

%meshfile = '../../Mesh_Handlers/meshfiles3d/one_polyhedron/degenerate3.mat';
meshfile = '../../Mesh_Handlers/meshfiles3d/voronoi/cube/cube_Nelt000051.mat';
load(meshfile);

% Polynomial degree
k = 1;
% Extra order of accuracy
eoa = 0;
% Compress face quadrature rules
compressquad = 0;
% Use multi precision toolbox
use_mp = 0;

elements_diameters = zeros(mesh.Nelt, 1);

for E = 1:mesh.Nelt
    element = construct_element_connectivity(mesh, E);

    % Compute face-related geometrical quantities and quadrature formula
    [faces_bnd_nodes, faces_qformula2D, faces_normals, faces_algdist, ...
     faces_edges_normals2D, faces_diameters, faces_areas, faces_centroids2D, ...
     ~, faces_orders] = ...
        faces_features(mesh.coordinates, element, k, eoa, compressquad, use_mp);

    % Compute element-related geometrical quantities and quadrature formula
    [ elements_diameters(E), ~, element_centroid, element_qformula3D ] = ...
        elements_features(mesh.coordinates, {element}, {faces_areas}, {faces_normals}, ...
                          {faces_algdist}, {faces_orders}, 2*k+eoa);

    % Evaluate monomial basis on 3d quadrature nodes for computing
    % integral of load term
    basis_on_qd_3d = monomial_basis_3d( ...
        element_qformula3D{1}(:,1), element_qformula3D{1}(:,2), element_qformula3D{1}(:,3), k, ...
        element_centroid(1), element_centroid(2), element_centroid(3), ...
        elements_diameters(E), 1.0);
    basis_on_qd_3d = permute(basis_on_qd_3d, [1,3,2]);

    % Evaluate load term
    f = loadcal_3d( ...
        element_qformula3D{1}(:,1), element_qformula3D{1}(:,2), element_qformula3D{1}(:,3), use_mp);

    % Assemble local stiffness matrix and right hand side
    [ ~, ~, ~, ...
      ~, ~, ~, Gp] = ...
        local_assembler_3d_k1(mesh.coordinates, element, faces_bnd_nodes, faces_qformula2D, ...
                              faces_normals, faces_edges_normals2D, faces_diameters, faces_centroids2D, ...
                              elements_diameters(E), element_centroid, ...
                              element_qformula3D{1}(:,4), basis_on_qd_3d, f);

    Gp_check = compute_G(k, mesh.coordinates, element, elements_diameters(E), element_centroid);

    relerr = norm(Gp-Gp_check, inf)/norm(Gp_check, inf);
    fprintf('\nElement %d. Relative error in the inf norm: %e\n', E, relerr);
end


% 
% % Compute face-related quantities
% [faces_bnd_nodes, faces_qformula2D, faces_normals, faces_algdist, ...
%     faces_edges_normals2D, faces_diameters, faces_areas, faces_centroids2D, ...
%     ~, faces_orders] = ...
%     faces_features(mesh.coordinates, element, k, 0, compressquad, use_mp);
% 
% % Compute element diameter and centroid
% [ element_diameter, ~, element_centroid, elements_qformula3D ] = ...
%     elements_features(mesh.coordinates, {element}, {faces_areas}, {faces_normals}, ...
%     {faces_algdist}, {faces_orders}, 2*k);
% 
% basis_on_qd_3d = monomial_basis_3d(elements_qformula3D{1}(:,1), ...
%                                 elements_qformula3D{1}(:,2), ...
%                                 elements_qformula3D{1}(:,3), ...
%                                 k, element_centroid(1), element_centroid(2), element_centroid(3), ...
%                                 element_diameter, 1.0);
% basis_on_qd_3d = permute(basis_on_qd_3d, [1,3,2]);

% % Matrix G
% tmp = ones(size(elements_qformula3D{1}(:,1)));
% 
% [~,~,~,~,~,~,Gp] = local_assembler_3d_k1(mesh.coordinates, element, faces_bnd_nodes, faces_qformula2D, ...
%     faces_normals, faces_edges_normals2D, faces_diameters, faces_centroids2D, ...
%     element_diameter, element_centroid, elements_qformula3D{1}(:,4), basis_on_qd_3d, tmp );
