function sol = compute_VEM_sol_3d( mesh, k, use_mp )

if k == 1
    sol = exactsol_3d(mesh.coordinates(:,1), mesh.coordinates(:,2), mesh.coordinates(:,3), use_mp);
end

end

