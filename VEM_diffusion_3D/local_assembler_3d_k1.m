function [Sp, row_dofs_for_Sp, col_dofs_for_Sp, rhs, dofs_for_rhs, P0s, Gp] = local_assembler_3d_k1(vertices, element, faces_bnd_nodes, faces_qformula2D, ...
    faces_normals, faces_edges_normals2D, faces_diameters, faces_centroids2D, ...
    element_diameter, element_centroid, wqd, basis_on_qd_3d, load)
% Assemble local stiffness matrix

% element: cell array with the vertices for each face of the element

Nfc = length(element);

% Build local mapping (from global vertex numbering to local vertex
% numbering)

[unique_nodes, ~, iunique_nodes] = unique(horzcat(element{:}));

% Number of local vertices
Nver = length(unique_nodes);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Step 1: construct projection Pi Nabla star

%%%%%%%%%%%%%%%%%%%
% Assemble matrix B

Bp = zeros(4,Nver);

% First row of B (see equation (7.4a))
Bp(1,:) = 1./Nver;

% Loop to assemble the remaining part of matrix B, i.e. rows 2-4.
% As shown in the notes, in the case 

node_counter = 0;
for f = 1:Nfc
    % Construct Pi Nabla projector for each face - this is a cut and paste
    % from the 2D code (see local_assembler_k1().
    
    Nver_f = length(element{f});

    % In 2D, matrix D coincides with the evaluations of the 2D basis
    % functions on the face vertices
    D_f = monomial_basis(faces_bnd_nodes{f}(:,1), faces_bnd_nodes{f}(:,2), 1, ...
        faces_centroids2D(f,1), faces_centroids2D(f,2), faces_diameters(f), 1.0);
    D_f = permute(D_f, [1,3,2]);
    
    B_f = [ones(1,Nver_f)/Nver_f;
          ( faces_edges_normals2D{f}(1:Nver_f,1) + faces_edges_normals2D{f}(2:Nver_f+1,1))' / (2.*faces_diameters(f));
          ( faces_edges_normals2D{f}(1:Nver_f,2) + faces_edges_normals2D{f}(2:Nver_f+1,2))' / (2.*faces_diameters(f))];
%     B_f = [ones(1,Nver_f)/Nver_f;
%           ( (faces_edges_normals2D{f}(1:Nver_f,1)/faces_diameters(f)) ...
%           + (faces_edges_normals2D{f}(2:Nver_f+1,1)/faces_diameters(f)) )' / 2.;
%           ( (faces_edges_normals2D{f}(1:Nver_f,2)/faces_diameters(f)) ...
%           + (faces_edges_normals2D{f}(2:Nver_f+1,2)/faces_diameters(f)) )' / 2.];

    G_f   = B_f*D_f;

    PNs_f = G_f\B_f;
    
    % Use Pi Nabla projector and monomial basis functions evaluated on 2D
    % quadrature nodes to compute face integrals

    %    Nqd_f = size(faces_qformula2D{f},1);
    basis_on_qd = monomial_basis(faces_qformula2D{f}(:,1), faces_qformula2D{f}(:,2), 1, ...
        faces_centroids2D(f,1), faces_centroids2D(f,2), faces_diameters(f), 1.0);
    basis_on_qd = permute(basis_on_qd, [1,3,2]);
    
    tmp = faces_qformula2D{f}(:,3)' * basis_on_qd;
    tmp = tmp * PNs_f;

    %   N_x^F * int_F \phi_j
    Bp(2, iunique_nodes( node_counter+1:node_counter+Nver_f ) ) = ...
        Bp(2, iunique_nodes( node_counter+1:node_counter+Nver_f ) ) + faces_normals(f,1) * tmp;
    %   N_y^F * int_F \phi_j
    Bp(3, iunique_nodes( node_counter+1:node_counter+Nver_f ) ) = ...
        Bp(3, iunique_nodes( node_counter+1:node_counter+Nver_f ) ) + faces_normals(f,2) * tmp;
    %   N_z^F * int_F \phi_j
    Bp(4, iunique_nodes( node_counter+1:node_counter+Nver_f ) ) = ...
        Bp(4, iunique_nodes( node_counter+1:node_counter+Nver_f ) ) + faces_normals(f,3) * tmp;    
    
    node_counter = node_counter + Nver_f;
end

Bp(2:4,:) = Bp(2:4,:)/element_diameter;

%%%%%%%%%%%%%%%%%%%
% Assemble matrix D

Dp = monomial_basis_3d(vertices(unique_nodes,1), vertices(unique_nodes,2), vertices(unique_nodes,3), ...
                       1, element_centroid(1), element_centroid(2), element_centroid(3), ...
                       element_diameter, 1.0);
Dp = permute(Dp, [1,3,2]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pi Nabla and L^2 projectors

Gp  = Bp*Dp;
PNs = Gp\Bp;
PN  = Dp*PNs;

% Also in 3D, the L^2 projector coincides with the Pi Nabla projector for k
% = 1
P0s = PNs;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Local stiffness matrix and right hand side

Gt      = Gp;
Gt(1,:) = 0.;

Sp = PNs'*Gt*PNs + element_diameter*((eye(Nver) - PN)'*(eye(Nver) - PN));

% % Diagonal stabilization
% Sp      = PNs'*Gt*PNs;
% scaling = diag( max( element_diameter, diag(Sp) ) );
% Sp      = Sp + ((eye(Nver) - PN)' * scaling * (eye(Nver) - PN));

Sp = Sp(:);

rhs = ((wqd.*load)'*basis_on_qd_3d)*P0s;
rhs = rhs(:);
dofs_for_rhs = unique_nodes(:);

tmp = repmat(dofs_for_rhs, 1, Nver);
row_dofs_for_Sp = tmp(:);
tmp = tmp';
col_dofs_for_Sp = tmp(:);

end





