function out = exactsol_3d( x, y, z, use_mp )

% % Constant
% out = ones(size(x));

% % Linear
% out = x + y + z;

% Cinf
if use_mp
    mypi = mp('pi');
else
    mypi = pi;
end
n = 2;
C = 1.0/(3.0 * n^2 * mypi^2);
out = C * sin(n*mypi*x) .* sin(n*mypi*y) .* sin(n*mypi*z);

end

