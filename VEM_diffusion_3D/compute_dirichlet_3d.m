function [ Udir, diridofs ] = compute_dirichlet_3d( mesh, k, use_mp )

if k == 1
    % Dirichlet degrees of freedom are given by the value of the solution
    % at the boundary nodes
    
    % Identify boundary nodes by first finding boundary faces, and then
    % selecting corresponding nodes
    
    tedges = floor( ( horzcat( mesh.face2hedges{ mesh.face2elements(:,2) == -1 } ) + 1 )/2 );
    vertices = mesh.edge2vertices(tedges,:);
    diridofs = unique(vertices(:));
    
    xdir = mesh.coordinates(diridofs,1);
    ydir = mesh.coordinates(diridofs,2);
    zdir = mesh.coordinates(diridofs,3);
    
    Udir = dirdata_3d(xdir, ydir, zdir, use_mp);
else
    
end

end
