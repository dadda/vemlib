function [ Slocs, row_dofs_for_Slocs, col_dofs_for_Slocs, ...
           rhs_locs, dofs_for_rhs_locs, P0s_by_ele, ...
           elements_diameters, elements_centroids, elements_qformulas3D ] = ...
                assemble_local_matrices_and_rhs_3d( mesh, i_F, ...
                                                    k, eoa, compressquad, use_mp, ...
                                                    elements_diameters, ...
                                                    elements_centroids, ...
                                                    elements_qformulas3D )

Slocs              = cell(mesh.Nelt,1);
row_dofs_for_Slocs = cell(mesh.Nelt,1);
col_dofs_for_Slocs = cell(mesh.Nelt,1);
rhs_locs           = cell(mesh.Nelt,1);
dofs_for_rhs_locs  = cell(mesh.Nelt,1);
P0s_by_ele         = cell(mesh.Nelt,1);

numInputs = nargin; % Prevent warning about nargin in parfor loop
if numInputs <= 6
    elements_diameters   = zeros(mesh.Nelt,1);
    elements_centroids   = zeros(mesh.Nelt,3);
    elements_qformulas3D = cell(mesh.Nelt,1);
end

if k == 1
    parfor ( E = 1:mesh.Nelt, getParforArg() )

        % Read OVM mesh and construct element to nodes connectivity
        % information
        element = construct_element_connectivity(mesh, E);
        
        % Compute face-related geometrical quantities and quadrature formula
        [faces_bnd_nodes, faces_qformula2D, faces_normals, faces_algdist, ...
         faces_edges_normals2D, faces_diameters, faces_areas, faces_centroids2D, ...
         ~, faces_orders] = ...
            faces_features(mesh.coordinates, element, k, eoa, compressquad, use_mp);

        % Compute element-related geometrical quantities and quadrature formula
        if numInputs <= 6
            [ elements_diameters(E), ~, element_centroid, element_qformula3D ] = ...
                elements_features(mesh.coordinates, {element}, {faces_areas}, {faces_normals}, ...
                                  {faces_algdist}, {faces_orders}, 2*k+eoa);
            elements_centroids(E,:) = element_centroid;
            elements_qformulas3D{E} = element_qformula3D{1};
        else
            element_centroid = elements_centroids(E,:);
            element_qformula3D{1} = elements_qformulas3D{E};
        end

        % Evaluate monomial basis on 3d quadrature nodes for computing
        % integral of load term
        basis_on_qd_3d = monomial_basis_3d( ...
            element_qformula3D{1}(:,1), element_qformula3D{1}(:,2), element_qformula3D{1}(:,3), k, ...
            element_centroid(1), element_centroid(2), element_centroid(3), ...
            elements_diameters(E), 1.0);
        basis_on_qd_3d = permute(basis_on_qd_3d, [1,3,2]);
        
        % Evaluate load term
        
        f = []; % Initialization to prevent parfor from issuing a warning about f
        
        switch i_F
            case 1 % Compute exact load term
                f = loadcal_3d( element_qformula3D{1}(:,1), ...
                                element_qformula3D{1}(:,2), ...
                                element_qformula3D{1}(:,3), use_mp);
            case 2 % Generate load term from the uniform distribution on [-1,1]
                if use_mp
                    f = -1 + 2*rand( size(element_qformula3D{1}(:,1)), 'mp' );
                else
                    f = -1 + 2*rand( size(element_qformula3D{1}(:,1)) );
                end
        end

        % Assemble local stiffness matrix and right hand side
        [ Slocs{E}, row_dofs_for_Slocs{E}, col_dofs_for_Slocs{E}, ...
          rhs_locs{E} , dofs_for_rhs_locs{E}, P0s_by_ele{E}] = ...
            local_assembler_3d_k1(mesh.coordinates, element, faces_bnd_nodes, faces_qformula2D, ...
                                  faces_normals, faces_edges_normals2D, faces_diameters, faces_centroids2D, ...
                                  elements_diameters(E), element_centroid, ...
                                  element_qformula3D{1}(:,4), basis_on_qd_3d, f);
                              
    end
    
elseif k == 2
    
else
        
end

Slocs              = vertcat(Slocs{:});
row_dofs_for_Slocs = vertcat(row_dofs_for_Slocs{:});
col_dofs_for_Slocs = vertcat(col_dofs_for_Slocs{:});
rhs_locs           = vertcat(rhs_locs{:});
dofs_for_rhs_locs  = vertcat(dofs_for_rhs_locs{:});

end



