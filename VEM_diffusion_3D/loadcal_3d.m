function out = loadcal_3d(x, y, z, use_mp)
% - nabla u = out

% out = zeros(size(x));

% Cinf
if use_mp
    mypi = mp('pi');
else
    mypi = pi;
end
n = 2;
out = sin(n*mypi*x) .* sin(n*mypi*y) .* sin(n*mypi*z);

end
