function errL2 = compute_errL2_3d( mesh, k, P0s_by_ele, U, ...
                                   elements_qformulas3D, elements_diameters, elements_centroids, ...
                                   use_mp )

errL2 = 0.;

if k == 1
    parfor ( E = 1:mesh.Nelt, getParforArg() )
        sol = exactsol_3d( ...
            elements_qformulas3D{E}(:,1), elements_qformulas3D{E}(:,2), elements_qformulas3D{E}(:,3), use_mp);

        % This call might be removed if we are willing to store extra
        % connectivity information computed when assembling local solvers.
        % TO DO: chekc the faster trade off.
        element    = construct_element_connectivity(mesh, E);
        local_dofs = unique(horzcat(element{:}));
        Uloc       = U(local_dofs);

        element_centroid = elements_centroids(E,:);
        basis_on_qd_3d = monomial_basis_3d( ...
            elements_qformulas3D{E}(:,1), elements_qformulas3D{E}(:,2), elements_qformulas3D{E}(:,3), k, ...
            element_centroid(1), element_centroid(2), element_centroid(3), ...
            elements_diameters(E), 1.0);
        basis_on_qd_3d = permute(basis_on_qd_3d, [1,3,2]);
        
        errL2 = errL2 + elements_qformulas3D{E}(:,4)' * (sol - basis_on_qd_3d*(P0s_by_ele{E}*Uloc)).^2;
    end
end
errL2 = sqrt(errL2);

end
