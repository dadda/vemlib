function [ U, errS, errinf, errL2, ...
           elements_diameters, elements_centroids, elements_qformulas3D ] = ...
                VEM_diffusion_3d(mesh, k, eoa, compressquad, basis_type, use_mp )
% VEM_DIFFUSION_3D    Virtual Element MEthod for the Poisson equation in 3D
%
%    [U, ERRS, ERRINF, ERRL2, MESH] = VEM_DIFFUSION_3d(MESH, K, EOA, COMPRESSQUAD, USE_MP)
%    solves the Poisson equation in 3D with the Virtual Element Method.
%
%    Inputs:
%
%   - MESH : polyhedral mesh in OVM format.
%
%   - K : degree of the VEM space.
%
%   - EOA : extra order of accuracy for quadrature formulas.
%
%   - COMPRESSQUAD : compress 2D quadrature rule to have a rule with less
%        nodes/weights (equal to the dimension of the desidered polynomial
%        space) but the same algebraic degree of exactness.
%
%   - BASIS_TYPE : type of monomial basis functions to use:
%        * BASIS_TYPE == 1, monomial basis functions of type ( (x-xd)/hd )^alpha;
%        * BASIS_TYPE == 2, monomial basis functions scaled by their L^2 norm.
%
%   - USE_MP : flag telling whether the Multiprecision Toolbox
%        (www.advanpix.com) should be used (USE_MP=1) or not
%        (USE_MP=0).
%
%   The load is given as a function "loadcal_3d" from the domain into Reals^3;
%   non-hom dirichlet BCs are given through function "dirdata_3d".
%
%   Outputs:
%
%   - U : column vector with the degrees of freedom of the VEM approximation
%        to the exact solution.
%
%   - ERRS, ERRINF, ERRL2 : energy norm, inf norm, and L^2 norm of the
%        approximation error.
%
%   - ELEMENTS_DIAMETERS   : [Nelt x 1] array with the elements diameters.
%   - ELEMENTS_CENTROIDS   : [Nelt x 3] array with the elements centroids.
%   - ELEMENTS_QFORMULAS3D : [Nelt x 1] cell array with 3D quadrature rules on the elements.
%
%   Nelt is the number of elements of the mesh.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Assemble local stiffness matrices and right hand sides

fprintf('Assembling local stiffness matrices and right hand sides...');

[ Slocs, row_dofs_for_Slocs, col_dofs_for_Slocs, ...
  rhs_locs, dofs_for_rhs_locs, P0s_by_ele, ...
  elements_diameters, elements_centroids, elements_qformulas3D ] = ...
      assemble_local_matrices_and_rhs_3d(mesh, 1, k, eoa, compressquad, use_mp);

% Assemble global stiffness matrix
S = sparse(row_dofs_for_Slocs, col_dofs_for_Slocs, Slocs);

% Assemble global right hand side
F = sparse(dofs_for_rhs_locs, ones([length(dofs_for_rhs_locs) 1]), rhs_locs);

fprintf('done\n')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute Dirichlet boundary conditions

[ Udir, diridofs ] = compute_dirichlet_3d( mesh, k, use_mp );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% d2m2 = (k*(k-1))/2;
% d3m2 = ((k+1)*k*(k-1))/6;
% 
% % Dimension of the global system
% N = mesh.Nver + mesh.Ned*(k-1) + mesh.Nfc*d2m2 + mesh.Nelt*d3m2;
% 
% % 
% tmp = false(N,1);
% tmp(diridofs) = true;
% 
% [~, ~, rjx] = unique(row_dofs_for_Slocs);
% 
% diri_ii = tmp(rjx) & (row_dofs_for_Slocs == col_dofs_for_Slocs);
% diri_ij = tmp(rjx) & ~diri_ii;
% 
% counts = histc(row_dofs_for_Slocs(diri_ii), 1:N);
% 
% Slocs(diri_ii) = 1.0 ./ counts(rjx(diri_ii));
% Slocs(diri_ij) = 0.0;
% 
% % Assemble global stiffness matrix
% S = sparse(row_dofs_for_Slocs, col_dofs_for_Slocs, Slocs);
% 
% % Assemble global right hand side
% F = sparse(dofs_for_rhs_locs, ones([length(dofs_for_rhs_locs) 1]), rhs_locs);
% 
% F(diridofs) = Udir;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Solve the linear system

fprintf('Solving the linear system...');

d2m2 = (k*(k-1))/2;
d3m2 = ((k+1)*k*(k-1))/6;

N = mesh.Nver + mesh.Ned*(k-1) + mesh.Nfc*d2m2 + mesh.Nelt*d3m2;
U = zeros(N,1);
dofs = setdiff(1:N, diridofs);

% Resolution
U(dofs) = S(dofs, dofs) \ (F(dofs) - S(dofs, diridofs) * Udir(:));

U(diridofs) = Udir;

fprintf('done\n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Error computation

if nargout > 1
    
    fprintf('Computing errors...');
    
    sol = compute_VEM_sol_3d(mesh, k, use_mp);
    tmp = U-sol;
    
    %-----------------------------------
    % ERROR in discrete energy norm
    %
    % Since we eliminate Dirichlet degrees of freedom, they do not
    % contribute to the energy norm of the error
    %-----------------------------------

%     errS = sqrt( abs( [tmp(dofs); tmp(diridofs)]' * [ S11*tmp(dofs) + S12*tmp(diridofs);
%                                                       S12'*tmp(dofs) + tmp(diridofs)      ] ) ...
%         / abs( [sol(dofs); sol(diridofs)]' * [ S11*sol(dofs) + S12*sol(diridofs);
%                                                S12'*sol(dofs) + sol(diridofs)      ] ) );

    errS = sqrt( abs( tmp(dofs)' * S(dofs, dofs) * tmp(dofs) ) ...
        / abs( sol(dofs)' * S(dofs, dofs) * sol(dofs) ...
               + sol(diridofs)' * S(diridofs, diridofs) * sol(diridofs) ) );
    errS = full(errS);

%    errS = sqrt( abs( tmp' * S * tmp ) / abs(sol' * S * sol) );
    
    %---------------------------------
    %  ERROR IN L-INFINITY
    %---------------------------------

    errinf = norm(tmp, inf)/norm(sol, inf);

    %---------------------------------
    %  ERROR IN L2
    %---------------------------------

    errL2 = compute_errL2_3d( mesh, k, P0s_by_ele, U, ...
                              elements_qformulas3D, ...
                              elements_diameters, ...
                              elements_centroids, ...
                              use_mp );
    
    fprintf('done\n');
    
end

end
