function [u, mu] = KBBsolver( mesh, k, use_mp, ...
                              sortedInterfaceEdges, sortedInterfaceFaces, ...
                              isEdgePrimal, isFacePrimal, ...
                              K, rhs, ...
                              internalDofs, boundaryDofs, ...
                              vertexPrimalDofs, ...
                              Qedge, Qface )
% Solve a system with a local block from KBB.
%
% Remark: rhs is allowed to be a matrix in the case we want to solve
% several systems with the same matrix but different right hand sides.

% Impose zero edge and face averages by means of extra Lagrange
% multipliers
dim = size(K,1);
numRHS = size(rhs,2);

% Eliminate non homogeneous Dirichlet dofs.
% Note that Dirichlet dofs have already been eliminated from K.
if k == 1
    uDir = dirdata_3d( mesh.coordinates(mesh.diridofs, 1), ...
                       mesh.coordinates(mesh.diridofs, 2), ...
                       mesh.coordinates(mesh.diridofs, 3), use_mp );
end
rhs(mesh.diridofs, :) = repmat(uDir, 1, numRHS);

% Eliminate primal vertices if there is any of them. Primal vertices
% must be eliminated from both K and rhs. Note that, according to the
% Lagrange multiplier approach, homogeneous conditions are associated
% with primal vertices.

if ~isempty(vertexPrimalDofs)
    rhs(vertexPrimalDofs, :) = 0;

    K( vertexPrimalDofs, : ) = 0;
    K( :, vertexPrimalDofs ) = 0;
    K( vertexPrimalDofs + dim*(vertexPrimalDofs-1) ) = 1;

    % Auxiliary dofs introduced by Klawonn

    % In the presence of one primal vertex at least, the leading two by
    % two block of K_Q defined on page 22 by Klawonn is invertible
    % (observe that in the scalar case we need no more than one
    % auxiliary degree of freedom)
    dofA = [];
    dofIA = internalDofs;

else
    % If choosing the auxiliary variable among the internal ones, no
    % pivoting is necessary to maintain stability. We arbitrarily
    % select the first internal dof
    dofA = internalDofs(1);
    dofIA = internalDofs(2:end);
end

%
% Assemble matrix Q - we assume that the boundary is read in the
% following way: vertices first, from 1 to 8, then edges, from 1 to 12,
% and finally faces, from 1 to 6
%

% Safety sorting
sortedInterfaceEdges = sort(sortedInterfaceEdges);
sortedInterfaceFaces = sort(sortedInterfaceFaces);

numInterfaceDofsByEdge = zeros(12, 1);
numInterfaceDofsByFace = zeros(6, 1);

for i = 1:12
    numInterfaceDofsByEdge(i) = length(mesh.edgeInternalDofs{i});
end

for i = 1:6
    numInterfaceDofsByFace(i) = length(mesh.faceInternalDofs{i});
end

numRows = isEdgePrimal * length(sortedInterfaceEdges) + isFacePrimal * length(sortedInterfaceFaces);
numCols = length(boundaryDofs);

len = isEdgePrimal * sum(numInterfaceDofsByEdge(sortedInterfaceEdges)) ...
    + isFacePrimal * sum(numInterfaceDofsByFace(sortedInterfaceFaces));

qRowDofs = zeros(len, 1);
qColDofs = zeros(len, 1);
qValues  = zeros(len, 1);

numConstraint = 0;
shift         = 0;

if isEdgePrimal
    for i = 1:length(sortedInterfaceEdges)
        e = sortedInterfaceEdges(i);
        numConstraint = numConstraint + 1;

        qRowDofs(shift+1:shift+numInterfaceDofsByEdge(e)) = ...
            numConstraint * ones(numInterfaceDofsByEdge(e),1);

        % Skip vertices
        qColDofs(shift+1:shift+numInterfaceDofsByEdge(e)) = ...
            8+sum(numInterfaceDofsByEdge(1:e-1))+1:8+sum(numInterfaceDofsByEdge(1:e));

        qValues(shift+1:shift+numInterfaceDofsByEdge(e))  = Qedge{i};

        shift = shift + numInterfaceDofsByEdge(e);
    end
end

if isFacePrimal
    for i = 1:length(sortedInterfaceFaces)
        f = sortedInterfaceFaces(i);
        numConstraint = numConstraint + 1;

        qRowDofs(shift+1:shift+numInterfaceDofsByFace(f)) = ...
            numConstraint * ones(numInterfaceDofsByFace(f),1);

        % Skip vertices and edges
        qColDofs(shift+1:shift+numInterfaceDofsByFace(f)) = ...
            8+sum(numInterfaceDofsByEdge)+sum(numInterfaceDofsByFace(1:f-1))+1:8+sum(numInterfaceDofsByEdge)+sum(numInterfaceDofsByFace(1:f));

        qValues(shift+1:shift+numInterfaceDofsByFace(f))  = Qface{i};

        shift = shift + numInterfaceDofsByFace(f);
    end
end

Q = sparse(qRowDofs, qColDofs, qValues, numRows, numCols);

K_IA = sparse( [ zeros(length(dofIA), numRows)   K(dofIA, dofA);
                 Q'                              K(boundaryDofs, dofA)] );

% Carry out one step of block Gaussian elimination
tmpSol = K( [dofIA; boundaryDofs], [dofIA; boundaryDofs] ) \ ...
    ( [K_IA rhs([dofIA; boundaryDofs], :)] );

u_A = ( [zeros(numRows, numRows+length(dofA));
         zeros(length(dofA), numRows)   K(dofA, dofA)] - K_IA'*tmpSol(:,1:numRows+length(dofA)) ) \ ...
         ( [ zeros(numRows,numRHS); rhs(dofA,:) ] - K_IA'*tmpSol(:, numRows+length(dofA)+1:end) );

u_I = tmpSol(:, numRows+length(dofA)+1:end) - tmpSol(:,1:numRows+length(dofA)) * u_A;

u = zeros(dim, numRHS);
u( [dofIA; boundaryDofs], : ) = u_I;
u( dofA, : ) = u_A(numRows+1:end, :);
mu = u_A(1:numRows, :);

end

