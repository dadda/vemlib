function elementFacesByBoundary = groupFacesByBoundary( refMesh )
% It only works for the unit cube

bndFaces = find( refMesh.face2elements(:,2) == -1 );

numBndFaces = length(bndFaces);
dummy = zeros(numBndFaces,1);

for f = 1:numBndFaces
    % Topological edge indices
    tedges = floor( ( refMesh.face2hedges{bndFaces(f)}+1 )/2 );
    edges  = refMesh.edge2vertices(tedges,:);
    faceNodes = unique(edges(:));
    
    if all( abs( refMesh.coordinates(faceNodes,1) ) < 1e-15 )
        dummy(f) = 1;
    elseif all( abs( refMesh.coordinates(faceNodes,1)-1 ) < 1e-15 )
        dummy(f) = 2;
    elseif all( abs( refMesh.coordinates(faceNodes,2) ) < 1e-15 )
        dummy(f) = 3;
    elseif all( abs( refMesh.coordinates(faceNodes,2)-1 ) < 1e-15 )
        dummy(f) = 4;
    elseif all( abs( refMesh.coordinates(faceNodes,3) ) < 1e-15 )
        dummy(f) = 5;
    elseif all( abs( refMesh.coordinates(faceNodes,3)-1 ) < 1e-15 )
        dummy(f) = 6;
    else
        error('There must be something wrong. Are you using a unit cube as reference mesh?')
    end
end

elementFacesByBoundary = cell(6,1);
elementFacesByBoundary{1} = bndFaces( dummy == 1 );
elementFacesByBoundary{2} = bndFaces( dummy == 2 );
elementFacesByBoundary{3} = bndFaces( dummy == 3 );
elementFacesByBoundary{4} = bndFaces( dummy == 4 );
elementFacesByBoundary{5} = bndFaces( dummy == 5 );
elementFacesByBoundary{6} = bndFaces( dummy == 6 );

end




