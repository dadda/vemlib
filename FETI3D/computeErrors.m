function [ relerrEnergy, relerrInf ] = computeErrors( Nvect, sol, u, K, runAsFor )

energyNorm   = 0;
relerrEnergy = 0;

numSubdomains = prod(Nvect);

numWorkers = getParforArg(runAsFor);

parfor ( s = 1:numSubdomains, numWorkers )
    energyNorm = energyNorm + sol{s}' * K{s} * sol{s};
    
    tmp = sol{s} - u{s};
    relerrEnergy = relerrEnergy + tmp' * K{s} * tmp;
end

relerrEnergy = sqrt( relerrEnergy / energyNorm );

sol = vertcat( sol{:} );
u   = vertcat( u{:} );

relerrInf = norm( u - sol, inf ) / norm( sol, inf );

end

