function [ edgeAverageDofs, edgeAverageDofsLocal, primalEdgeDofs, Tedge ] = ...
                processPrimalEdges( refPrimalEdges, mesh, k )

refEdge2endpoints = [1 3;
                     2 4;
                     1 2;
                     3 4;
                     1 5;
                     2 6;
                     3 7;
                     4 8;
                     5 7;
                     6 8;
                     5 6;
                     7 8];
                 
numPrimalEdges = length(refPrimalEdges);

edgeAverageDofs      = zeros(numPrimalEdges, 1);
edgeAverageDofsLocal = zeros(numPrimalEdges, 1);
primalEdgeDofs       = cell(numPrimalEdges, 1);
Tedge                = cell(numPrimalEdges, 1);

for e = 1:numPrimalEdges
    [ Tedge{e}, edgeAverageDofsLocal(e) ] = ...
        constructEdgeTransform( mesh, k, ...
                                [ mesh.vertexDofs( refEdge2endpoints( refPrimalEdges(e), 1 ) );
                                  mesh.edgeInternalDofs{refPrimalEdges(e)};
                                  mesh.vertexDofs( refEdge2endpoints( refPrimalEdges(e), 2 ) )] );
    
    edgeAverageDofs(e) = mesh.edgeInternalDofs{ refPrimalEdges(e) }(edgeAverageDofsLocal(e));
    primalEdgeDofs{e}  = mesh.edgeInternalDofs{ refPrimalEdges(e) };
end

end

