function sol = exactSolution( meshArray, Nx, Ny, Nz, k, use_mp, ...
                              isVertexPrimal, isEdgePrimal, isFacePrimal, ...
                              approach4constraints, ...
                              Tvertex, Tedge, Tface, ...
                              runAsFor )

sol = cell(Nx, Ny, Nz);

numSubdomains = Nx*Ny*Nz;

if k == 1

    numWorkers = getParforArg(runAsFor);

    parfor ( s = 1:numSubdomains, numWorkers )
        sol{s} = exactsol_3d( meshArray{s}.coordinates(:,1), ...
                              meshArray{s}.coordinates(:,2), ...
                              meshArray{s}.coordinates(:,3), use_mp );
    end

    if approach4constraints == 1
        parfor ( s = 1:numSubdomains, numWorkers )
%         for s = 1:numSubdomains
            ks = floor( (s-1) / (Nx*Ny) ) + 1;
            js = floor( (s - Nx*Ny*(ks-1) - 1) / Nx ) + 1;
            is = s - Nx*Ny*(ks-1) - Nx*(js-1);

            [interfaceVertices, interfaceEdges, interfaceFaces] = ...
                getInterfaceVEF( Nx, Ny, Nz, is, js, ks );

            transformedDofs = cell(3,1);
            if ~isVertexPrimal
                transformedDofs{1} = meshArray{s}.vertexDofs( interfaceVertices );
            end
            if isEdgePrimal || isFacePrimal
                transformedDofs{2} = vertcat( meshArray{s}.edgeInternalDofs{interfaceEdges} );
            end
            if isFacePrimal
                transformedDofs{3} = vertcat( meshArray{s}.faceInternalDofs{interfaceFaces} );
            end
            transformedDofs = vertcat( transformedDofs{:} );

            if ~isempty(transformedDofs)
                tmp = vertcat( Tvertex{s}{:}, Tedge{s}{:}, Tface{s}{:} );

                dimension = meshArray{s}.Nver;
                notTransformedDofs = setdiff((1:dimension)', transformedDofs);

                T = sparse( [tmp(:,1); notTransformedDofs], ...
                            [tmp(:,2); notTransformedDofs], ...
                            [tmp(:,3); ones(length(notTransformedDofs),1)] );
                sol{s} = T \ sol{s};
            end
        end
    end
end
