function [ vertexPrimalDofs, vertexDualDofs, Tvertex, ...
           globalPrimalDofByVertex, globalPrimalDofs, globalPrimalDofsEnd, ...
           globalLambdaDofsByVertex, globalLambdaDofs, globalLambdaDofsEnd, ...
           localLambdaDofs, localLambdaDofsEnd, ...
           constraintsSign, constraintsSignScaled ] = ...
                subdomainOperators4Vertices( ...
                    mesh, k, ...
                    Nx, Ny, Nz, is, js, ks, ...
                    sortedInterfaceVertices, ...
                    isVertexPrimal, isEdgePrimal, isFacePrimal, ...
                    faceIntegrals, ...
                    Tvertex, ...
                    globalPrimalDofByVertex, globalPrimalDofs, globalPrimalDofsEnd, ...
                    globalLambdaDofsByVertex, globalLambdaDofs, globalLambdaDofsEnd, ...
                    localLambdaDofs, localLambdaDofsEnd, ...
                    constraintsSign, rho, constraintsSignScaled )

%
% Build maps from an interface vertex to its neighbors
%

% Map used whether interface vertices are primal
primalPatch = [-1 -1 -1;  % reference vertex 1
                0 -1 -1;  % reference vertex 2
               -1  0 -1;  % ...
                0  0 -1;
               -1 -1  0;
                0 -1  0;
               -1  0  0;
	        nan nan nan]; % reference vertex 8 introduces a new primal dof.
                              % This row is never used, but is stored to be consistent with
                              % the same map for primal edges, where having as many rows as
                              % reference edges is mandatory

% Map used whether interface vertices are dual
dualPatch = zeros(7, 3, 8);

dualPatch(:,:,1) = [-1 -1 -1;
                     0 -1 -1;
                    -1  0 -1;
                     0  0 -1;
                    -1 -1  0;
                     0 -1  0;
                    -1  0  0];

dualPatch(:,:,2) = [ 0 -1 -1;
                     1 -1 -1;
                     0  0 -1;
                     1  0 -1;
                     0 -1  0;
                     1 -1  0;
                     1  0  0];

dualPatch(:,:,3) = [-1  0 -1;
                     0  0 -1;
                    -1  1 -1;
                     0  1 -1;
                    -1  0  0;
                    -1  1  0;
                     0  1  0];

dualPatch(:,:,4) = [ 0  0 -1;
                     1  0 -1;
                     0  1 -1;
                     1  1 -1;
                     1  0  0;
                     0  1  0;
                     1  1  0];

dualPatch(:,:,5) = [-1 -1  0;
                     0 -1  0;
                    -1  0  0;
                    -1 -1  1;
                     0 -1  1;
                    -1  0  1;
                     0  0  1];

dualPatch(:,:,6) = [ 0 -1  0;
                     1 -1  0;
                     1  0  0;
                     0 -1  1;
                     1 -1  1;
                     0  0  1;
                     1  0  1];

dualPatch(:,:,7) = [-1  0  0;
                    -1  1  0;
                     0  1  0;
                    -1  0  1;
                     0  0  1;
                    -1  1  1;
                     0  1  1];

dualPatch(:,:,8) = [ 1  0  0;
                     0  1  0;
                     1  1  0;
                     0  0  1;
                     1  0  1;
                     0  1  1;
                     1  1  1];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Linearized index of current subdomain
linearSubdomainInd = is + Nx*(js-1) + Nx*Ny*(ks-1);

% Safety sorting
sortedInterfaceVertices = sort(sortedInterfaceVertices);
numInterfaceVertices    = length(sortedInterfaceVertices);

if (is == Nx) || (js == Ny) || (ks == Nz)
    if sortedInterfaceVertices(end) == 8
        error( ['Error in subdomain(%i,%i,%i): reference vertex 8 appears ' ...
                'in the list of interface vertices'], is, js, ks );
    end
    
    % This subdomain does not add any new primal dof
    globalPrimalDofsEnd( linearSubdomainInd ) = globalPrimalDofsEnd( max( linearSubdomainInd - 1, 1 ) );
end

Tvertex{is, js, ks} = cell(8,1);

if isVertexPrimal
    vertexPrimalDofs = mesh.vertexDofs( sortedInterfaceVertices );
    vertexDualDofs   = [];

    % Select primal dofs that have already been added by previous
    % subdomains
    globalPrimalDofs = [ globalPrimalDofs;
                         globalPrimalDofByVertex( ...
                            sub2ind( [Nx, Ny, Nz], ...
                                     is + primalPatch( sortedInterfaceVertices( sortedInterfaceVertices<8 ), 1 ), ...
                                     js + primalPatch( sortedInterfaceVertices( sortedInterfaceVertices<8 ), 2 ), ...
                                     ks + primalPatch( sortedInterfaceVertices( sortedInterfaceVertices<8 ), 3 ) ) ) ];
    
    if sortedInterfaceVertices(end) == 8
        % Add reference vertex 8 to the vector of global primal dofs
        globalPrimalDofByVertex( linearSubdomainInd ) = ...
            globalPrimalDofsEnd( max( linearSubdomainInd - 1, 1 ) ) + 1;
        
        globalPrimalDofs = [ globalPrimalDofs;
                             globalPrimalDofByVertex( linearSubdomainInd ) ];

        globalPrimalDofsEnd( linearSubdomainInd ) = globalPrimalDofByVertex( linearSubdomainInd );
    end
else
    vertexPrimalDofs = [];
    vertexDualDofs   = mesh.vertexDofs( sortedInterfaceVertices );

    % This subdomain does not add any new primal dof
    globalPrimalDofsEnd( linearSubdomainInd ) = globalPrimalDofsEnd( max( linearSubdomainInd - 1, 1 ) );

    % Initialization
    globalLambdaDofsEnd( linearSubdomainInd ) = globalLambdaDofsEnd( max( linearSubdomainInd - 1, 1 ) );
    
    for i = 1:numInterfaceVertices
        v = sortedInterfaceVertices(i);

        if v == 8
            % Compute the vertex transformation from scratch
            Tvertex{is, js, ks}{8} = constructVertexTransform( mesh, k, 8, ...
                                                               isEdgePrimal, isFacePrimal, ...
                                                               faceIntegrals );
        else
            Tvertex{is,js,ks}{v} = ...
                Tvertex{ is + primalPatch(v,1), js + primalPatch(v,2), ks + primalPatch(v,3) }{8};
        end

        % For each vertex v, add old lambda dofs first, then new ones

        % Old dofs
        numOldDofs = 8 - v;
        globalLambdaDofsByVertex( sub2ind( [Nx, Ny, Nz, 8, 7], ...
                                               is*ones(numOldDofs,1), ...
                                               js*ones(numOldDofs,1), ...
                                               ks*ones(numOldDofs,1), ...
                                               v*ones(numOldDofs,1), (1:numOldDofs)' ) ) = ...
        globalLambdaDofsByVertex( ...
                                  sub2ind( [Nx, Ny, Nz, 8, 7], ...
                                           is + dualPatch(1:numOldDofs, 1, v), ...
                                           js + dualPatch(1:numOldDofs, 2, v), ... 
                                           ks + dualPatch(1:numOldDofs, 3, v), ...
                                           (8:-1:(v+1))', ...
                                           (8-v)*ones(numOldDofs,1) ) );

        newConstraints = ones(7,1);
        newConstraints(1:numOldDofs) = -1;
        
        % New dofs
        globalLambdaDofsByVertex( sub2ind( [Nx, Ny, Nz, 8, 7], ...
                                           is*ones(7-numOldDofs,1), ...
                                           js*ones(7-numOldDofs,1), ...
                                           ks*ones(7-numOldDofs,1), ...
                                           v*ones(7-numOldDofs,1), (numOldDofs+1:7)' ) ) = ...
        globalLambdaDofsEnd( linearSubdomainInd ) + (1:(7-numOldDofs))';
            
        globalLambdaDofs = [ globalLambdaDofs;
                             globalLambdaDofsByVertex( sub2ind( [Nx, Ny, Nz, 8, 7], ...
                                                       is*ones(7,1), ...
                                                       js*ones(7,1), ...
                                                       ks*ones(7,1), ...
                                                       v*ones(7,1), (1:7)' ) ) ]; %#ok<AGROW>
                                                       
        globalLambdaDofsEnd( linearSubdomainInd ) = globalLambdaDofsEnd( linearSubdomainInd ) ...
                                                    + (7 - numOldDofs);
                                                    
        rhoSum = sum( rho( sub2ind( [Nx, Ny, Nz], ...
                                    [is; is + dualPatch(:, 1, v)], ...
                                    [js; js + dualPatch(:, 2, v)], ...
                                    [ks; ks + dualPatch(:, 3, v)] ) ) );

        pseudoInverse = rho( sub2ind( [Nx, Ny, Nz], ...
                                      is + dualPatch(:, 1, v), ...
                                      js + dualPatch(:, 2, v), ...
                                      ks + dualPatch(:, 3, v) ) ) / rhoSum;
                                  
        constraintsSign = [ constraintsSign;
                            newConstraints ]; %#ok<AGROW>
        constraintsSignScaled = [ constraintsSignScaled;
                                  pseudoInverse .* newConstraints ]; %#ok<AGROW>
    end

    numInterfaceVertices = length(sortedInterfaceVertices);

    localLambdaDofs = [ localLambdaDofs;
                        kron( (1:numInterfaceVertices)', ones(7,1) ) ];
    localLambdaDofsEnd = localLambdaDofsEnd + numInterfaceVertices;
end


end
