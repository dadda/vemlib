function [ faceAverageDofs, faceAverageDofsLocal, primalFaceDofs, Tface, ...
           globalPrimalDofs, globalPrimalDofsEnd, ...
           globalLambdaDofs, globalLambdaDofsEnd, ...
           localLambdaDofs, localLambdaDofsEnd, ...
           constraintsSign, constraintsSignScaled, ...
           notTransformedDualDofs ] = ...
                subdomain111Operators4Faces( ...
                    mesh, k, eoa, compressquad, use_mp, ...
                    Nx, Ny, ...
                    isFacePrimal, ...
                    globalPrimalDofs, globalPrimalDofsEnd, ...
                    globalLambdaDofs, globalLambdaDofsEnd, ...
                    localLambdaDofs, localLambdaDofsEnd, ...
                    constraintsSign, rho, constraintsSignScaled, ...
                    notTransformedDualDofs )

refFaces = [2 4 6];

numLambdaDofsFace = 0;
for i = 1:length(refFaces)
    numLambdaDofsFace = numLambdaDofsFace + length(mesh.faceInternalDofs{refFaces(i)});
end

if isFacePrimal
    [ faceAverageDofs, faceAverageDofsLocal, primalFaceDofs, Tface ] = ...
               processPrimalFaces( refFaces, mesh, k, eoa, compressquad, use_mp );
           
    numLambdaDofsFace = numLambdaDofsFace - length(refFaces);
    
    globalPrimalDofs = [ globalPrimalDofs;
                         (globalPrimalDofsEnd(1,1,1)+(1:length(refFaces)))' ];
    globalPrimalDofsEnd(1,1,1) = globalPrimalDofsEnd(1,1,1) + length(refFaces);
else
    faceAverageDofs = [];
    faceAverageDofsLocal = [];
    primalFaceDofs = [];
    Tface = [];
    
    notTransformedDualDofs = [ notTransformedDualDofs;
                               vertcat( mesh.faceInternalDofs{refFaces} ) ];    
end

globalLambdaDofs = [ globalLambdaDofs;
                     (globalLambdaDofsEnd(1,1,1)+1:globalLambdaDofsEnd(1,1,1)+numLambdaDofsFace)' ];
localLambdaDofs  = [ localLambdaDofs;
                     (localLambdaDofsEnd+1:localLambdaDofsEnd+numLambdaDofsFace)' ];
constraintsSign  = [ constraintsSign;
                     ones(numLambdaDofsFace,1) ];

patchI = [1 2; % face2
          1 1; % face4
          1 1]; % face6
patchJ = [1 1;
          1 2;
          1 1];
patchK = [1 1;
          1 1;
          1 2];
pseudoInverse = ...
    rho( patchI(:,2) + Nx*(patchJ(:,2)-1) + Nx*Ny*(patchK(:,2)-1) ) ...
    ./ sum( rho( patchI + Nx*(patchJ-1) + Nx*Ny*(patchK-1) ), 2 ) ;

for i = 1:length(refFaces)
    constraintsSignScaled = [ constraintsSignScaled;
                              ones( length(mesh.faceInternalDofs{refFaces(i)})-isFacePrimal, 1 ) ...
                              * pseudoInverse(i) ]; %#ok<AGROW>
end

globalLambdaDofsEnd(1,1,1) = globalLambdaDofsEnd(1,1,1) + numLambdaDofsFace;
localLambdaDofsEnd         = localLambdaDofsEnd + numLambdaDofsFace;

end

