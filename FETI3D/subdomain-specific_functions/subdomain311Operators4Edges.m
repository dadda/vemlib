function [ edgeAverageDofs, edgeAverageDofsLocal, primalEdgeDofs, Tedge, ...
           globalPrimalDofs, globalPrimalDofsEnd, ...
           globalLambdaDofs, globalLambdaDofsEnd, ...
           localLambdaDofs, localLambdaDofsEnd, ...
           constraintsSign, constraintsSignScaled, ...
           notTransformedDualDofs ] = ...
                subdomain311Operators4Edges( ...
                    mesh, k, ...
                    Nx, Ny, ...
                    isVertexPrimal, isEdgePrimal, ...
                    globalPrimalDofs, globalPrimalDofsStart, globalPrimalDofsEnd, ...
                    globalLambdaDofs, globalLambdaDofsStart, globalLambdaDofsEnd, ...
                    localLambdaDofs, localLambdaDofsEnd, ...
                    constraintsSign, rho, constraintsSignScaled, ...
                    notTransformedDualDofs )

refEdges = [7 9 12];

numLambdaDofsEdge = zeros(length(refEdges),1);
for i = 1:length(refEdges)
    numLambdaDofsEdge(i) = length(mesh.edgeInternalDofs{refEdges(i)}) - isEdgePrimal;
end

if isEdgePrimal
    [ edgeAverageDofs, edgeAverageDofsLocal, primalEdgeDofs, Tedge ] = ...
        processPrimalEdges( refEdges, mesh, k );
    
    % REMARK: refEdge 7 and refEdge 9 are old, whereas refEdge 12 is new
    
    globalPrimalDofs = [ globalPrimalDofs;
                         globalPrimalDofsStart(Nx-1, 1, 1) + isVertexPrimal;
                         globalPrimalDofsStart(Nx-1, 1, 1) + isVertexPrimal + 1;
                         globalPrimalDofsEnd(Nx, 1, 1) + 1 ];
    globalPrimalDofsEnd(Nx, 1, 1) = globalPrimalDofsEnd(Nx, 1, 1) + 1;
else
    edgeAverageDofs = [];
    edgeAverageDofsLocal = [];
    primalEdgeDofs = [];
    Tedge = [];

    notTransformedDualDofs = [ notTransformedDualDofs;
                               vertcat( mesh.edgeInternalDofs{refEdges} ) ];    
end

numConstraintEq = 3;

startingDof = globalLambdaDofsStart(Nx-1,1,1) ...
              ... % Lambda dofs from interface vertices
              + (1-isVertexPrimal) * 7 ...         % subdomain (1,1,1) only contributes 7 lambda dofs
                                       ...         % due to Dirichlet boundary conditions
              + (1-isVertexPrimal) * (Nx > 2) * 6;

patchI = [Nx Nx-1 Nx-1 Nx;
          Nx Nx-1 Nx-1 Nx;
          Nx Nx   Nx   Nx];
patchJ = [1 1 2 2;
          1 1 1 1;
          1 2 1 2];
patchK = [1 1 1 1;
          1 1 2 2;
          1 1 2 2];
rhoSums = sum( rho( patchI + Nx*(patchJ-1) + Nx*Ny*(patchK-1) ), 2 );
pseudoInverse = bsxfun(@rdivide, ...
    rho( patchI(:, 2:end) + Nx*(patchJ(:, 2:end)-1) + Nx*Ny*(patchK(:, 2:end)-1) ), ...
    rhoSums );

% Add refEdge 7 - this is a shared refEdge
dofs = zeros(numConstraintEq, numLambdaDofsEdge(1));
dofs(1,:) = ...
    startingDof:numConstraintEq:startingDof+numConstraintEq*(numLambdaDofsEdge(1)-1);
dofs(2:end, :) = ...
    reshape( globalLambdaDofsEnd(Nx,1,1)+(1:(numConstraintEq-1)*numLambdaDofsEdge(1)), ...
             2, numLambdaDofsEdge(1) );
dofs = dofs(:);
globalLambdaDofs = [ globalLambdaDofs; dofs ];
globalLambdaDofsEnd(Nx, 1, 1) = globalLambdaDofsEnd(Nx, 1, 1) + ...
                                (numConstraintEq-1)*numLambdaDofsEdge(1);
newConstraints = ones(numConstraintEq, numLambdaDofsEdge(1));
newConstraints(1,:) = -1.;
newConstraintsScaled = bsxfun(@times, pseudoInverse(1,:)', newConstraints);

constraintsSign       = [ constraintsSign; newConstraints(:) ];
constraintsSignScaled = [ constraintsSignScaled; newConstraintsScaled(:) ];

% Add refEdge 9 - this is a shared refEdge
startingDof = startingDof + numConstraintEq*numLambdaDofsEdge(1);

dofs = zeros(numConstraintEq, numLambdaDofsEdge(2));
dofs(1,:) = ...
    startingDof:numConstraintEq:startingDof+numConstraintEq*(numLambdaDofsEdge(2)-1);
dofs(2:end, :) = ...
    reshape( globalLambdaDofsEnd(Nx,1,1)+(1:(numConstraintEq-1)*numLambdaDofsEdge(2)), ...
             2, numLambdaDofsEdge(2) );
dofs = dofs(:);
globalLambdaDofs = [ globalLambdaDofs; dofs ];
globalLambdaDofsEnd(Nx, 1, 1) = globalLambdaDofsEnd(Nx, 1, 1) + ...
                                (numConstraintEq-1)*numLambdaDofsEdge(2);
newConstraints = ones(numConstraintEq, numLambdaDofsEdge(2));
newConstraints(1,:) = -1.;
newConstraintsScaled = bsxfun(@times, pseudoInverse(2,:)', newConstraints);

constraintsSign       = [ constraintsSign; newConstraints(:) ];
constraintsSignScaled = [ constraintsSignScaled; newConstraintsScaled(:) ];

% Add refEdge 12 - this is brand new
globalLambdaDofs = [ globalLambdaDofs;
                     globalLambdaDofsEnd(Nx, 1, 1)+(1:numConstraintEq*numLambdaDofsEdge(3))' ];
globalLambdaDofsEnd(Nx, 1, 1) = globalLambdaDofsEnd(Nx, 1, 1) + numConstraintEq*numLambdaDofsEdge(3);

constraintsSign = [ constraintsSign;
                    ones(numConstraintEq*numLambdaDofsEdge(3),1) ];
constraintsSignScaled = [ constraintsSignScaled;
                          kron( ones( numLambdaDofsEdge(3), 1 ), pseudoInverse(3,:)' ) ];

localLambdaDofs  = [ localLambdaDofs;
                     kron( (localLambdaDofsEnd+1:localLambdaDofsEnd+sum(numLambdaDofsEdge))', ...
                           ones(numConstraintEq,1) ) ];

localLambdaDofsEnd = localLambdaDofsEnd + sum(numLambdaDofsEdge);


end
