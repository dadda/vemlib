function [ faceAverageDofs, faceAverageDofsLocal, primalFaceDofs, Tface, ...
           globalPrimalDofs, globalPrimalDofsEnd, ...
           globalLambdaDofs, globalLambdaDofsEnd, ...
           localLambdaDofs, localLambdaDofsEnd, ...
           constraintsSign, constraintsSignScaled, ...
           notTransformedDualDofs ] = ...
                subdomain311Operators4Faces( ...
                    mesh, k, eoa, compressquad, use_mp, ...
                    Nx, Ny, ...
                    isVertexPrimal, isEdgePrimal, isFacePrimal, ...
                    globalPrimalDofs, globalPrimalDofsStart, globalPrimalDofsEnd, ...
                    globalLambdaDofs, globalLambdaDofsStart, globalLambdaDofsEnd, ...
                    localLambdaDofs, localLambdaDofsEnd, ...
                    constraintsSign, rho, constraintsSignScaled, ...
                    notTransformedDualDofs )

refFaces = [1 4 6];

numLambdaDofsFace = zeros(length(refFaces), 1);
for i = 1:length(refFaces)
    numLambdaDofsFace(i) = length(mesh.faceInternalDofs{refFaces(i)}) - isFacePrimal;
end

if isFacePrimal
    [ faceAverageDofs, faceAverageDofsLocal, primalFaceDofs, Tface ] = ...
        processPrimalFaces( refFaces, mesh, k, eoa, compressquad, use_mp );
    
    % REMARK: refFace 1 is old, refFaces 4 and 6 are new
    
    globalPrimalDofs = [ globalPrimalDofs;
                         globalPrimalDofsStart(Nx-1, 1, 1) + isVertexPrimal + 3*isEdgePrimal;
                         globalPrimalDofsEnd(Nx, 1, 1) + [1; 2] ];
                     
    globalPrimalDofsEnd(Nx, 1, 1) = globalPrimalDofsEnd(Nx, 1, 1) + 2;
else
    faceAverageDofs = [];
    faceAverageDofsLocal = [];
    primalFaceDofs = [];
    Tface = [];

    notTransformedDualDofs = [ notTransformedDualDofs;
                               vertcat( mesh.faceInternalDofs{refFaces} ) ];    
end

numConstraintEqEdge = 3;
startingDof = globalLambdaDofsStart(Nx-1, 1, 1) ...
              ... % Add lambda dofs from interface vertices
              + (1-isVertexPrimal)*(7+6*(Nx>2)) ...               
              ... % First part of the lambda dofs added by the previous subdomain.
              + numConstraintEqEdge * ( length(mesh.edgeInternalDofs{7}) ...     
                                        + length(mesh.edgeInternalDofs{9}) ...
                                        + length(mesh.edgeInternalDofs{12}) ...
                                        - 3*isEdgePrimal ) ...
              ... % Second part of the lambda dofs added by the previous subdomain.
              + (Nx>2) * ( (numConstraintEqEdge-1) * ( length(mesh.edgeInternalDofs{8}) ...
                                                       + length(mesh.edgeInternalDofs{10}) ...
                                                       - 2*isEdgePrimal ) );

globalLambdaDofs = [ globalLambdaDofs;
                     ... % Old dofs
                     (startingDof:startingDof+numLambdaDofsFace(1)-1)';
                     ... % New dofs
                     globalLambdaDofsEnd(Nx,1,1)+(1:sum(numLambdaDofsFace(2:end)))' ];

globalLambdaDofsEnd(Nx,1,1) = globalLambdaDofsEnd(Nx,1,1) + sum(numLambdaDofsFace(2:end));

localLambdaDofs = [ localLambdaDofs;
                    (localLambdaDofsEnd+1:localLambdaDofsEnd+sum(numLambdaDofsFace))' ];
localLambdaDofsEnd = localLambdaDofsEnd + sum(numLambdaDofsFace);

patchI = [Nx Nx-1; % refFace 1
          Nx Nx;   % refFace 4
          Nx Nx];   % refFace 6
patchJ = [1 1;
          1 2;
          1 1];
patchK = [1 1;
          1 1;
          1 2];
pseudoInverse = ...
    rho( patchI(:,2) + Nx*(patchJ(:,2)-1) + Nx*Ny*(patchK(:,2)-1) ) ...
    ./ sum( rho( patchI + Nx*(patchJ-1) + Nx*Ny*(patchK-1) ), 2 ) ;

constraintsSign = [ constraintsSign;
                    -ones(numLambdaDofsFace(1), 1);
                    ones(sum(numLambdaDofsFace(2:end)), 1) ];

constraintsSignScaled = [ constraintsSignScaled;
                          -ones(numLambdaDofsFace(1), 1) * pseudoInverse(1);
                          ones(numLambdaDofsFace(2), 1) * pseudoInverse(2);
                          ones(numLambdaDofsFace(3), 1) * pseudoInverse(3) ];

end

