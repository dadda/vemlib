function [ edgeAverageDofs, edgeAverageDofsLocal, primalEdgeDofs, Tedge, ...
           globalPrimalDofs, globalPrimalDofsEnd, ...
           globalLambdaDofs, globalLambdaDofsEnd, ...
           localLambdaDofs, localLambdaDofsEnd, ...
           constraintsSign, constraintsSignScaled, ...
           notTransformedDualDofs ] = ...
                subdomain111Operators4Edges( mesh, k, ...
                                             Nx, Ny, ...
                                             isEdgePrimal, ...
                                             globalPrimalDofs, globalPrimalDofsEnd, ...
                                             globalLambdaDofs, globalLambdaDofsEnd, ...
                                             localLambdaDofs, localLambdaDofsEnd, ...
                                             constraintsSign, rho, constraintsSignScaled, ...
                                             notTransformedDualDofs )

refEdges = [8 10 12];

numLambdaDofsEdge = 0;
for i = 1:length(refEdges)
    numLambdaDofsEdge = numLambdaDofsEdge + length(mesh.edgeInternalDofs{refEdges(i)});
end

if isEdgePrimal
    [ edgeAverageDofs, edgeAverageDofsLocal, primalEdgeDofs, Tedge ] = ...
               processPrimalEdges( refEdges, mesh, k );
           
    numLambdaDofsEdge = numLambdaDofsEdge - length(refEdges);
    
    globalPrimalDofs = [ globalPrimalDofs;
                         (globalPrimalDofsEnd(1,1,1)+(1:length(refEdges)))' ];
    globalPrimalDofsEnd(1,1,1) = globalPrimalDofsEnd(1,1,1) + length(refEdges);
else
    edgeAverageDofs = [];
    edgeAverageDofsLocal = [];
    primalEdgeDofs = [];
    Tedge = [];
    
    notTransformedDualDofs = [ notTransformedDualDofs;
                               vertcat( mesh.edgeInternalDofs{refEdges} ) ];    
end

% Number of contraint equations an edge dual variable appears into in the
% fully redundant case
numConstraintEq = 3;

globalLambdaDofs = [ globalLambdaDofs;
                     (globalLambdaDofsEnd(1,1,1)+1:globalLambdaDofsEnd(1,1,1)+numConstraintEq*numLambdaDofsEdge)' ];
localLambdaDofs  = [ localLambdaDofs;
                     kron( (localLambdaDofsEnd+1:localLambdaDofsEnd+numLambdaDofsEdge)', ...
                           ones(numConstraintEq,1) ) ];
constraintsSign  = [ constraintsSign;
                     ones(numConstraintEq*numLambdaDofsEdge,1) ];

patchI = [1 2 1 2; % refEdge{8}
          1 2 1 2; % refEdge{10}
          1 1 1 1]; % refEdge{12}
patchJ = [1 1 2 2;
          1 1 1 1;
          1 2 1 2];
patchK = [1 1 1 1;
          1 1 2 2;
          1 1 2 2];
rhoSums = sum( rho( patchI + Nx*(patchJ-1) + Nx*Ny*(patchK-1) ), 2 );
pseudoInverse = bsxfun(@rdivide, ...
    rho( patchI(:, 2:end) + Nx*(patchJ(:, 2:end)-1) + Nx*Ny*(patchK(:, 2:end)-1) ), ...
    rhoSums );

for i = 1:length(refEdges)
    constraintsSignScaled = [ constraintsSignScaled;
                              kron( ones( length(mesh.edgeInternalDofs{refEdges(i)})-isEdgePrimal, 1 ), ...
                                    pseudoInverse(i,:)' ) ]; %#ok<AGROW>
end

globalLambdaDofsEnd(1,1,1) = globalLambdaDofsEnd(1,1,1) + numConstraintEq*numLambdaDofsEdge;
localLambdaDofsEnd         = localLambdaDofsEnd + numLambdaDofsEdge;

end

