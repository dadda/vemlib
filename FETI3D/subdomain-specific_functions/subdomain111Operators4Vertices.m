function [ primalVerticesDofs, ...
           globalPrimalDofs, globalPrimalDofsEnd, ...
           globalLambdaDofs, globalLambdaDofsEnd, ...
           localLambdaDofs, localLambdaDofsEnd, ...
           constraintsSign, constraintsSignScaled, ...
           notTransformedDualDofs ] = ...
                subdomain111Operators4Vertices( ...
                    mesh, ...
                    Nx, Ny, ...
                    isVertexPrimal, ...
                    globalPrimalDofs, globalPrimalDofsEnd, ...
                    globalLambdaDofs, globalLambdaDofsEnd, ...
                    localLambdaDofs, localLambdaDofsEnd, ...
                    constraintsSign, rho, constraintsSignScaled, ...
                    notTransformedDualDofs )

refVertices = 8;

if isVertexPrimal
    primalVerticesDofs = mesh.vertexDofs(refVertices);
    
    % Column indices for primal boolean operator
    globalPrimalDofs = [ globalPrimalDofs; 1 ];
    
    globalPrimalDofsEnd(1,1,1) = 1;
else
    primalVerticesDofs = [];
    
    % Remember that a dual vertex appears in 7 equations
    % in the fully redundant case
    
    % Vector of row indices for assembling dual boolean operators
    globalLambdaDofs = [ globalLambdaDofs;
                         (1:7)'];
                     
    % Vector of column indices for assembling dual boolean operators
    localLambdaDofs  = [ localLambdaDofs;
                         ones(7,1) ];
                     
    % Signs in equations imposing continuity of dual variables are chosen
    % as follows: for an equation involving a dual dof from a subdomain i
    % and the corrisponding dual dof from the subdomain j, we assign +1 to
    % the dof associated with the subdomain with smaller index according to the patch
    % numbering, and -1 otherwise. Since here we are in the first
    % subdomain, we have +1 everywhere.
    constraintsSign  = [ constraintsSign;
                         ones(7,1) ];

    % i-th index of the neighboring subdomains (including myself, in first position)
    patchI  = [1,2,1,2,1,2,1,2];
    % i-th index of the neighboring subdomains (including myself, in first position)
    patchJ  = [1,1,2,2,1,1,2,2];
    % i-th index of the neighboring subdomains (including myself, in first position)
    patchK  = [1,1,1,1,2,2,2,2];
    
    pseudoInverse = ...
        rho( patchI(2:end) + Nx*(patchJ(2:end)-1) + Nx*Ny*(patchK(2:end)-1) ) ...
        / sum( rho( patchI + Nx*(patchJ-1) + Nx*Ny*(patchK-1) ) ) ;
    constraintsSignScaled = [ constraintsSignScaled;
                              pseudoInverse' ];
                          
    globalLambdaDofsEnd(1,1,1) = globalLambdaDofsEnd(1,1,1) + 7;
    localLambdaDofsEnd  = localLambdaDofsEnd + 1;
    
    notTransformedDualDofs = [ notTransformedDualDofs;
                               mesh.vertexDofs(refVertices) ];
end


end

