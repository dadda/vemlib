function [ edgeAverageDofs, edgeAverageDofsLocal, primalEdgeDofs, Tedge, ...
           globalPrimalDofs, globalPrimalDofsEnd, ...
           globalLambdaDofs, globalLambdaDofsEnd, ...
           localLambdaDofs, localLambdaDofsEnd, ...
           constraintsSign, constraintsSignScaled, ...
           notTransformedDualDofs ] = ...
                subdomain211Operators4Edges( ...
                    mesh, k, ...
                    Nx, Ny, is, js, ks, ...
                    isVertexPrimal, isEdgePrimal, ...
                    globalPrimalDofs, globalPrimalDofsStart, globalPrimalDofsEnd, ...
                    globalLambdaDofs, globalLambdaDofsStart, globalLambdaDofsEnd, ...
                    localLambdaDofs, localLambdaDofsEnd, ...
                    constraintsSign, rho, constraintsSignScaled, ...
                    notTransformedDualDofs )

refEdges = [7 8 9 10 12];

numLambdaDofsEdge = zeros(length(refEdges),1);
for i = 1:length(refEdges)
    numLambdaDofsEdge(i) = length(mesh.edgeInternalDofs{refEdges(i)}) - isEdgePrimal;
end

if isEdgePrimal
    [ edgeAverageDofs, edgeAverageDofsLocal, primalEdgeDofs, Tedge ] = ...
        processPrimalEdges( refEdges, mesh, k );
    
    % REMARK: out of the five edges to be considered, three are new (8, 10,
    % 12), whereas two are not (7 and 9)
    
    globalPrimalDofs = [ globalPrimalDofs;
                         % If there are no primal vertices, reference edge
                         % 7 is the first NEW primal dof added by the
                         % PREVIOUS subdomain; otherwise, it was the second
                         % one to be added.
                         globalPrimalDofsStart(is-1, js, ks) + isVertexPrimal;
                         globalPrimalDofsEnd(is, js, ks) + 1;
                         globalPrimalDofsStart(is-1, js, ks) + isVertexPrimal + 1;
                         globalPrimalDofsEnd(is, js, ks) + 2;
                         globalPrimalDofsEnd(is, js, ks) + 3 ];
                     
    globalPrimalDofsEnd(is, js, ks) = globalPrimalDofsEnd(is, js, ks) + 3;
else
    edgeAverageDofs = [];
    edgeAverageDofsLocal = [];
    primalEdgeDofs = [];
    Tedge = [];

    notTransformedDualDofs = [ notTransformedDualDofs;
                               vertcat( mesh.edgeInternalDofs{refEdges} ) ];    
end

numConstraintEq = 3;

startingDof = globalLambdaDofsStart(is-1,js,ks) + (1-isVertexPrimal)*(7+6*(is>2));

patchI = [is is-1 is-1 is;   % refEdge 7
          is is+1 is   is+1; % refEdge 8
          is is-1 is-1 is;   % refEdge 9
          is is+1 is   is+1; % refEdge 10
          is is   is   is];  % refEdge 12
patchJ = [js js   js+1 js+1;
          js js   js+1 js+1;
          js js   js   js;
          js js   js   js;
          js js+1 js   js+1];
patchK = [ks ks   ks   ks;
          ks ks   ks   ks;
          ks ks   ks+1 ks+1;
          ks ks   ks+1 ks+1;
          ks ks   ks+1 ks+1];
rhoSums = sum( rho( patchI + Nx*(patchJ-1) + Nx*Ny*(patchK-1) ), 2 );
pseudoInverse = bsxfun(@rdivide, ...
    rho( patchI(:, 2:end) + Nx*(patchJ(:, 2:end)-1) + Nx*Ny*(patchK(:, 2:end)-1) ), ...
    rhoSums );

% REMARK: when processing an edge to assemble dual boolean operators, we
% consider, for each single node on the edge, ALL the equations where the
% node appears (i.e. all its connections with the other subdomains), THEN
% we move to the next node in the edge. This makes the assemblage harder,
% but it should be beneficial for the sparsity of boolean operators.

% Add refEdge 7
%
% refEdge 7 is shared with the previous subdomain. Therefore, for each node
% on refEdge 7, its first lambda dof, which is linking the current
% subdomain with the previous one, has already been processed, WHEREAS, the
% other two lambda dofs are associated with contraints with the FOLLOWING
% subdomains, therefore, they are NEW.
dofs = zeros(numConstraintEq, numLambdaDofsEdge(1));
dofs(1,:) = ...
    startingDof:numConstraintEq:startingDof+numConstraintEq*(numLambdaDofsEdge(1)-1);
dofs(2:end, :) = ...
    reshape( globalLambdaDofsEnd(is,js,ks)+(1:(numConstraintEq-1)*numLambdaDofsEdge(1)), ...
             2, numLambdaDofsEdge(1) );
dofs = dofs(:);
globalLambdaDofs = [ globalLambdaDofs; dofs ];
globalLambdaDofsEnd(is,js,ks) = globalLambdaDofsEnd(is,js,ks) + ...
                                (numConstraintEq-1)*numLambdaDofsEdge(1);
newConstraints = ones(numConstraintEq, numLambdaDofsEdge(1));
newConstraints(1,:) = -1.;
newConstraintsScaled = bsxfun(@times, pseudoInverse(1,:)', newConstraints);

constraintsSign       = [ constraintsSign; newConstraints(:) ];
constraintsSignScaled = [ constraintsSignScaled; newConstraintsScaled(:) ];

% Add redEdge 8 - this edge is brand new
globalLambdaDofs = [ globalLambdaDofs;
                     globalLambdaDofsEnd(is,js,ks)+(1:numConstraintEq*numLambdaDofsEdge(2))' ];
globalLambdaDofsEnd(is,js,ks) = globalLambdaDofsEnd(is,js,ks) + numConstraintEq*numLambdaDofsEdge(2);

constraintsSign = [ constraintsSign;
                    ones(numConstraintEq*numLambdaDofsEdge(2),1) ];
constraintsSignScaled = [ constraintsSignScaled;
                          kron( ones( numLambdaDofsEdge(2), 1 ), pseudoInverse(2,:)' ) ];

% Add refEdge 9 - the FIRST equation for each node is shared with the FIRST
% equation in the previous subdomain
startingDof = startingDof + numConstraintEq*numLambdaDofsEdge(1);

dofs = zeros(numConstraintEq, numLambdaDofsEdge(3));
dofs(1,:) = ...
    startingDof:numConstraintEq:startingDof+numConstraintEq*(numLambdaDofsEdge(3)-1);
dofs(2:end, :) = ...
    reshape( globalLambdaDofsEnd(is,js,ks)+(1:(numConstraintEq-1)*numLambdaDofsEdge(3)), ...
             2, numLambdaDofsEdge(3) );
dofs = dofs(:);
globalLambdaDofs = [ globalLambdaDofs; dofs ];
globalLambdaDofsEnd(is,js,ks) = globalLambdaDofsEnd(is,js,ks) + ...
                                (numConstraintEq-1)*numLambdaDofsEdge(3);
newConstraints = ones(numConstraintEq, numLambdaDofsEdge(3));
newConstraints(1,:) = -1.;
newConstraintsScaled = bsxfun(@times, pseudoInverse(3,:)', newConstraints);

constraintsSign       = [ constraintsSign; newConstraints(:) ];
constraintsSignScaled = [ constraintsSignScaled; newConstraintsScaled(:) ];

% Add redEdge 10 - this edge is brand new
globalLambdaDofs = [ globalLambdaDofs;
                     globalLambdaDofsEnd(is,js,ks)+(1:numConstraintEq*numLambdaDofsEdge(4))' ];
globalLambdaDofsEnd(is,js,ks) = globalLambdaDofsEnd(is,js,ks) + numConstraintEq*numLambdaDofsEdge(4);

constraintsSign = [ constraintsSign;
                    ones(numConstraintEq*numLambdaDofsEdge(4),1) ];
constraintsSignScaled = [ constraintsSignScaled;
                          kron( ones( numLambdaDofsEdge(4), 1 ), pseudoInverse(4,:)' ) ];

% Add redEdge 12 - this edge is brand new
globalLambdaDofs = [ globalLambdaDofs;
                     globalLambdaDofsEnd(is,js,ks)+(1:numConstraintEq*numLambdaDofsEdge(5))' ];
globalLambdaDofsEnd(is,js,ks) = globalLambdaDofsEnd(is,js,ks) + numConstraintEq*numLambdaDofsEdge(5);

constraintsSign = [ constraintsSign;
                    ones(numConstraintEq*numLambdaDofsEdge(5),1) ];
constraintsSignScaled = [ constraintsSignScaled;
                          kron( ones( numLambdaDofsEdge(5), 1 ), pseudoInverse(5,:)' ) ];

localLambdaDofs  = [ localLambdaDofs;
                     kron( (localLambdaDofsEnd+1:localLambdaDofsEnd+sum(numLambdaDofsEdge))', ...
                           ones(numConstraintEq,1) ) ];

localLambdaDofsEnd = localLambdaDofsEnd + sum(numLambdaDofsEdge);
                       
end













