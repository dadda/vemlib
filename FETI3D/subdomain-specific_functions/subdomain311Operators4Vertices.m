function [ primalVerticesDofs, ...
           globalPrimalDofs, globalPrimalDofsEnd, ...
           globalLambdaDofs, globalLambdaDofsEnd, ...
           localLambdaDofs, localLambdaDofsEnd, ...
           constraintsSign, constraintsSignScaled, ...
           notTransformedDualDofs ] = ...
                subdomain311Operators4Vertices( ...
                    mesh, ...
                    Nx, Ny, ...
                    isVertexPrimal, ...
                    globalPrimalDofs, globalPrimalDofsStart, globalPrimalDofsEnd, ...
                    globalLambdaDofs, globalLambdaDofsStart, globalLambdaDofsEnd, ...
                    localLambdaDofs, localLambdaDofsEnd, ...
                    constraintsSign, rho, constraintsSignScaled, ...
                    notTransformedDualDofs )

refVertices = 7;

% This subdomain does not add any new primal vertex
globalPrimalDofsEnd(Nx, 1, 1) = globalPrimalDofsEnd(Nx-1, 1, 1);

if isVertexPrimal
    primalVerticesDofs = mesh.vertexDofs(refVertices);
    
    globalPrimalDofs = [ globalPrimalDofs;
                         globalPrimalDofsStart(Nx-1, 1, 1) ];
    globalLambdaDofsEnd(Nx, 1, 1) = globalLambdaDofsEnd(Nx-1, 1, 1);
else
    primalVerticesDofs = [];
    
    % If the vertex is dual, we do add new lambda dofs
    globalLambdaDofs = [ globalLambdaDofs;
                         % Retrieve old lambda dof
                         globalLambdaDofsStart(Nx-1, 1, 1) + 6*(Nx>2);
                         % Add new lambda dofs
                         globalLambdaDofsEnd(Nx-1, 1, 1) + (1:6)' ];
                     
    globalLambdaDofsEnd(Nx, 1, 1) = globalLambdaDofsEnd(Nx-1, 1, 1) + 6;
    
    localLambdaDofs = [ localLambdaDofs;
                        ones(7,1) ];
    localLambdaDofsEnd  = localLambdaDofsEnd + 1;
    
    constraintsSign = [ constraintsSign;
                        -1;
                        ones(6,1) ];
                    
    % i-th index of the neighboring subdomains (including myself, in first position)
    patchI  = [Nx Nx-1 Nx-1 Nx Nx-1 Nx Nx-1 Nx];
    % i-th index of the neighboring subdomains (including myself, in first position)
    patchJ  = [1 1 2 2 1 1 2 2];
    % i-th index of the neighboring subdomains (including myself, in first position)
    patchK  = [1 1 1 1 2 2 2 2];
    
    pseudoInverse = ...
        rho( patchI(2:end) + Nx*(patchJ(2:end)-1) + Nx*Ny*(patchK(2:end)-1) ) ...
        / sum( rho( patchI + Nx*(patchJ-1) + Nx*Ny*(patchK-1) ) ) ;
    constraintsSignScaled = [ constraintsSignScaled;
                              -pseudoInverse(1);
                              pseudoInverse(2:end)' ];
                    
    notTransformedDualDofs = [ notTransformedDualDofs;
                               mesh.vertexDofs(refVertices) ];
end


end

