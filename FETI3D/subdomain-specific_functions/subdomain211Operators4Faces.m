function [ faceAverageDofs, faceAverageDofsLocal, primalFaceDofs, Tface, ...
           globalPrimalDofs, globalPrimalDofsEnd, ...
           globalLambdaDofs, globalLambdaDofsEnd, ...
           localLambdaDofs, localLambdaDofsEnd, ...
           constraintsSign, constraintsSignScaled, ...
           notTransformedDualDofs ] = ...
                subdomain211Operators4Faces( ...
                    mesh, k, eoa, compressquad, use_mp, ...
                    Nx, Ny, is, js, ks, ...
                    isVertexPrimal, isEdgePrimal, isFacePrimal, ...
                    globalPrimalDofs, globalPrimalDofsStart, globalPrimalDofsEnd, ...
                    globalLambdaDofs, globalLambdaDofsStart, globalLambdaDofsEnd, ...
                    localLambdaDofs, localLambdaDofsEnd, ...
                    constraintsSign, rho, constraintsSignScaled, ...
                    notTransformedDualDofs )

refFaces = [1 2 4 6];

numLambdaDofsFace = zeros(length(refFaces), 1);
for i = 1:length(refFaces)
    numLambdaDofsFace(i) = length(mesh.faceInternalDofs{refFaces(i)}) - isFacePrimal;
end

if isFacePrimal
    [ faceAverageDofs, faceAverageDofsLocal, primalFaceDofs, Tface ] = ...
        processPrimalFaces( refFaces, mesh, k, eoa, compressquad, use_mp );

    % REMARK: refFace 1 has already been processed by the previous
    % subdomain, whereas refFace 2, 4, and 6 have not.
    
    globalPrimalDofs = [ globalPrimalDofs;
                         globalPrimalDofsStart(is-1, js, ks) + isVertexPrimal + 3*isEdgePrimal;
                         globalPrimalDofsEnd(is, js, ks) + (1:3)' ];
                     
    globalPrimalDofsEnd(is, js, ks) = globalPrimalDofsEnd(is, js, ks) + 3;
else
    faceAverageDofs = [];
    faceAverageDofsLocal = [];
    primalFaceDofs = [];
    Tface = [];
    
    notTransformedDualDofs = [ notTransformedDualDofs;
                               vertcat( mesh.faceInternalDofs{refFaces} ) ];    

end

numConstraintEqEdge = 3;
startingDof = globalLambdaDofsStart(is-1, js, ks) ...
              ... % Add lambda dofs from interface vertices
              + (1-isVertexPrimal)*(7+6*(is>2)) ...               
              ... % First part of the lambda dofs added by the previous subdomain.
              ... % These are actually all the lambda dofs to be added in the case
              ... % the previous subdomain is the very first one.
              ... % REMARK: we should add dofs on refEdges 8, 10, and 12 of the
              ... % previous subdomain; however, these edges map to the refEdges of the
              ... % current subdomain as follows:
              ... % 7 <-> 8, 9 <-> 10, 12 unaltered.
              + numConstraintEqEdge * ( length(mesh.edgeInternalDofs{7}) ...     
                                        + length(mesh.edgeInternalDofs{9}) ...
                                        + length(mesh.edgeInternalDofs{12}) ...
                                        - 3*isEdgePrimal ) ...
              ... % Second part of the lambda dofs added by the previous subdomain.
              ... % These are added only if is > 2
              + (is>2) * ( (numConstraintEqEdge-1) * ( length(mesh.edgeInternalDofs{8}) ...
                                                       + length(mesh.edgeInternalDofs{10}) ...
                                                       - 2*isEdgePrimal ) );

globalLambdaDofs = [ globalLambdaDofs;
                     ... % Old dofs
                     (startingDof:startingDof+numLambdaDofsFace(1)-1)';
                     ... % New dofs
                     globalLambdaDofsEnd(is,js,ks)+(1:sum(numLambdaDofsFace(2:end)))' ];

globalLambdaDofsEnd(is,js,ks) = globalLambdaDofsEnd(is,js,ks) + sum(numLambdaDofsFace(2:end));

localLambdaDofs = [ localLambdaDofs;
                    (localLambdaDofsEnd+1:localLambdaDofsEnd+sum(numLambdaDofsFace))' ];
localLambdaDofsEnd = localLambdaDofsEnd + sum(numLambdaDofsFace);

patchI = [is is-1; % refFace 1
          is is+1; % refFace 2
          is is;   % refFace 4
          is is];  % refFace 6
patchJ = [js js;
          js js;
          js js+1;
          js js];
patchK = [ks ks;
          ks ks;
          ks ks;
          ks ks+1];
pseudoInverse = ...
    rho( patchI(:,2) + Nx*(patchJ(:,2)-1) + Nx*Ny*(patchK(:,2)-1) ) ...
    ./ sum( rho( patchI + Nx*(patchJ-1) + Nx*Ny*(patchK-1) ), 2 ) ;

constraintsSign = [ constraintsSign;
                    -ones(numLambdaDofsFace(1), 1);
                    ones(sum(numLambdaDofsFace(2:end)), 1) ];

constraintsSignScaled = [ constraintsSignScaled;
                          -ones(numLambdaDofsFace(1), 1) * pseudoInverse(1);
                          ones(numLambdaDofsFace(2), 1) * pseudoInverse(2);
                          ones(numLambdaDofsFace(3), 1) * pseudoInverse(3);
                          ones(numLambdaDofsFace(4), 1) * pseudoInverse(4) ];
                
end











