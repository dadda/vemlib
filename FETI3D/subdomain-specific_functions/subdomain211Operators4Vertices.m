function [ primalVerticesDofs, ...
           globalPrimalDofs, globalPrimalDofsEnd, ...
           globalLambdaDofs, globalLambdaDofsEnd, ...
           localLambdaDofs, localLambdaDofsEnd, ...
           constraintsSign, constraintsSignScaled, ...
           notTransformedDualDofs ] = ...
                subdomain211Operators4Vertices( ...
                    mesh, ...
                    Nx, Ny, is, js, ks, ...
                    isVertexPrimal, ...
                    globalPrimalDofs, globalPrimalDofsStart, globalPrimalDofsEnd, ...
                    globalLambdaDofs, globalLambdaDofsStart, globalLambdaDofsEnd, ...
                    localLambdaDofs, localLambdaDofsEnd, ...
                    constraintsSign, rho, constraintsSignScaled, ...
                    notTransformedDualDofs )

refVertices = [7 8];

if isVertexPrimal
    primalVerticesDofs = mesh.vertexDofs(refVertices);

    % Add one new primal vertex dof
    globalPrimalDofsEnd(is, js, ks) = globalPrimalDofsEnd(is-1, js, ks) + 1;

    globalPrimalDofs = [ globalPrimalDofs;
                         % Reference vertex 7 is the first NEW global
                         % primal dof that has been added by
                         % the previous subdomain
                         globalPrimalDofsStart(is-1, js, ks);
                         % Add reference vertex 8 to the vector of global
                         % primal dofs
                         globalPrimalDofsEnd(is, js, ks) ];
                     
    globalLambdaDofsEnd(is, js, ks) = globalLambdaDofsEnd(is-1, js, ks);
else
    primalVerticesDofs = [];
    
    globalPrimalDofsEnd(is, js, ks) = globalPrimalDofsEnd(is-1, js, ks);
    
    globalLambdaDofs = [ globalLambdaDofs;
                         % Retrieve old lambda dof
                         globalLambdaDofsStart(is-1, js, ks)+6*(is>2);
                         % Add new lambda dofs
                         globalLambdaDofsEnd(is-1, js, ks)+(1:13)' ];
                     
    % Add only the number of NEW lambda dofs
    globalLambdaDofsEnd(is, js, ks) = globalLambdaDofsEnd(is-1, js, ks) + 13;
                     
    localLambdaDofs = [ localLambdaDofs;
                        ones(7,1);
                        2*ones(7,1) ];
    localLambdaDofsEnd = localLambdaDofsEnd + length(refVertices);

    newConstraints  = [ -1;           % Constraint involving the "previous" subdomain
                        ones(13,1) ]; % Constraint involving the "next" subdomain
    constraintsSign = [ constraintsSign; newConstraints ];
                    
    patchI = [ is is-1 is-1 is   is-1 is   is-1 is;
               is is+1 is   is+1 is   is+1 is   is+1 ];
    patchJ = [ js js   js+1 js+1 js   js   js+1 js+1;
               js js   js+1 js+1 js   js   js+1 js+1 ];
    patchK = [ ks ks   ks   ks   ks+1 ks+1 ks+1 ks+1;
               ks ks   ks   ks   ks+1 ks+1 ks+1 ks+1 ];

    rhoSums = sum( rho( patchI + Nx*(patchJ-1) + Nx*Ny*(patchK-1) ), 2 );
    pseudoInverse = bsxfun(@rdivide, ...
        rho( patchI(:, 2:end) + Nx*(patchJ(:, 2:end)-1) + Nx*Ny*(patchK(:, 2:end)-1) ), ...
        rhoSums );
    
    pseudoInverse = pseudoInverse';
    pseudoInverse = pseudoInverse(:);
    
    constraintsSignScaled = [ constraintsSignScaled;
                              pseudoInverse.*newConstraints ];
    
    notTransformedDualDofs = [ notTransformedDualDofs;
                               mesh.vertexDofs(refVertices) ];
end


end

