
maxdim = 20;
vars = cell(maxdim, 1);

for dim = 1:maxdim
    vars{dim} = sym(sprintf('x%i', dim));
end

for dim = 1:maxdim
    fprintf('\nDimension: %i\n', dim)
    for i = 1:dim
        T = sym(eye(dim));
        T(i,:) = - vertcat(vars{1:dim});
        T(:,i) = 1;

        if i == 1
            tstart = tic;
            gold = permute( inv(T), [2 1] );
            telapsed_gold = toc(tstart);

            tstart = tic;
            TinvTransp = invTransposeT( T, i );
            telapsed_invTransposeT = toc(tstart);

            errinf = norm(gold-TinvTransp, inf);
            fprintf('i = 1, elapsed time for gold: %f\n', telapsed_gold)
            fprintf('i = 1, elapsed time for invTransposeT: %f\n', telapsed_invTransposeT)
        else
            gold = permute( inv(T), [2 1] );
            TinvTransp = invTransposeT( T, i );
            errinf = norm(gold-TinvTransp, inf);
        end
        
        try
            errinf = double(errinf);
            if errinf > 0
               error('Wrong TinvTransp for dim = %i, i = %i', dim, i) 
            end
        catch
            error('Wrong TinvTransp for dim = %i, i = %i', dim, i)
        end
    end
end

fprintf('Everything went fine!\n')
