% Test different options for solving linear systems

n = 10000;
density = 0.1;

% denseA = rand(n, n);
% denseA = 0.5 * (denseA + denseA');
% denseA = denseA + n*eye(n);
% denseB = zeros(n,1);

sparseA = sprandsym(n, density) + n*speye(n);
sparseB = sparse(n,1);

maxit = 100;
xVect1 = zeros(n,maxit);
xVect2 = zeros(n,maxit);

% Strategy 1: keep using A
tic;
for i = 1:maxit
    x = sparseA\sparseB;
    xVect1(:,i) = x;
end
toc;

whos;

% Strategy 2: replace A with its lower triangular Cholesky factor ans solve
% triangular systems
tic;

sparseA = chol(sparseA, 'lower');
for i = 1:maxit
    y = sparseA\sparseB;
    x = sparseA\y;
    xVect2(:,i) = x;
end

toc;

whos

disp(norm(xVect1-xVect2, 'fro'))

