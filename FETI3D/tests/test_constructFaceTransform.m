
addpath ../../Mesh_Handlers
addpath ../../Quadrature
addpath ../../Quadrature/cpp_modules
addpath ../../Basis_Functions
addpath ../../VEM_diffusion
addpath ../../VEM_diffusion_3D
addpath ../


meshroot = '../../Mesh_Handlers/meshfiles3d';
meshname = chooseMesh3d( 'Note: make sure to select a mesh of the unit cube' );
meshname = [meshroot '/' meshname];
load([meshname '.mat'])
fprintf('Loaded mesh %s\n', meshname);

facesByBoundary = groupFacesByBoundary( mesh );

k = 1;
eoa = 0;
compressquad = 0;
use_mp = 0;

[~, ~, faceInternalDofs] = ...
    createPreliminaryDofPartition( mesh, k );

for i = 1:6
    [ T, primalDof, dualDofs ] = constructFaceTransform( mesh, k, facesByBoundary{i}, ...
                                                                  faceInternalDofs{i}, ...
                                                                  eoa, compressquad, use_mp );

    numFacesOnBoundary = length(facesByBoundary{i});
    faceIntegrals = zeros( mesh.Nver, 1 );
    
    for l = 1:numFacesOnBoundary
        f = facesByBoundary{i}(l);
        element = mesh.face2elements(f, 1);        
        tfaces  = floor( ( mesh.element2hfaces{element}+1 )/2 );
        faces_orientation = mod(mesh.element2hfaces{element}, 2);

        % Locate the face inside the element by comparing topological
        % indices
        fLocal = find(tfaces == f);
        
        % Topological edge index and edge orientation
        tedges = floor( ( mesh.face2hedges{f}+1 )/2 );
        edges_orientation = mod(mesh.face2hedges{f}, 2);
        edges = mesh.edge2vertices(tedges,:)';

        face = faces_orientation(fLocal) * (edges_orientation .* edges(1,:) + (1-edges_orientation).*edges(2,:)) + ...
               (1-faces_orientation(fLocal)) * ((1-edges_orientation(end:-1:1)) .* edges(1,end:-1:1) + edges_orientation(end:-1:1).*edges(2,end:-1:1));
        
        [face_bnd_nodes, face_qformula2D, ~, ~, ...
         face_edges_normals2D, face_diameter, face_area, face_centroid2D] = ...
            faces_features(mesh.coordinates, {face}, k, eoa, compressquad, use_mp);
        
        % Assemble \Pi^{\Nabla^*}
        Nver_f = length(face);

        D_f = monomial_basis(face_bnd_nodes{1}(:,1), face_bnd_nodes{1}(:,2), 1, ...
            face_centroid2D(1), face_centroid2D(2), face_diameter, 1.0);
        D_f = permute(D_f, [1,3,2]);

        B_f = [ones(1,Nver_f)/Nver_f;
              ( face_edges_normals2D{1}(1:Nver_f,1) + face_edges_normals2D{1}(2:Nver_f+1,1))' / (2.*face_diameter);
              ( face_edges_normals2D{1}(1:Nver_f,2) + face_edges_normals2D{1}(2:Nver_f+1,2))' / (2.*face_diameter)];
        
        G_f   = B_f*D_f;
        
        PNs_f = G_f\B_f;
        
        % Compute moment of order 0 for each face VEM function
        basis_on_qd = monomial_basis(face_qformula2D{1}(:,1), face_qformula2D{1}(:,2), 1, ...
            face_centroid2D(1), face_centroid2D(2), face_diameter, 1.0);
        basis_on_qd = permute(basis_on_qd, [1,3,2]);

        tmp = face_qformula2D{1}(:,3)' * basis_on_qd;
        tmp = tmp * PNs_f;
        
        faceIntegrals(face) = faceIntegrals(face) + tmp';
    end

    faceIntegrals = faceIntegrals( faceInternalDofs{i} );
    faceIntegrals = T'*faceIntegrals;

    disp('Checking integrals')
    disp(faceIntegrals)
end


