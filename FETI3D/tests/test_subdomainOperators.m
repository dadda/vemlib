%
% Solving 3D Poisson equation on the unit cube with FETI-DP and VEM on
%

addpath ../../Mesh_Handlers
addpath ../../Quadrature
addpath ../../Quadrature/cpp_modules
addpath ../../Basis_Functions
addpath ../../VEM_diffusion
addpath ../../VEM_diffusion_3D
addpath ../
addpath ../subdomain-specific_functions

fprintf('\nScript for solving Poisson equation in 3D with VEM (k = 1) and FETI-DP\n\n');

if license('test','Distrib_Computing_Toolbox')
    if isempty(gcp)
        parpool
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Inputs

%
% Polynomial degree
%
k = 1;

eoa = input('Extra order of accuracy for quadrature formulas [0]: ');
if isempty(eoa)
   eoa = 0; 
end
compressquad = input('Compress 2D quadrature rules? 0/1 [0]: ');
if isempty(compressquad)
    compressquad = 0;
end

%
% Type of basis functions
%
basis_type = 1;

%
% Multi-precision toolbox
%
use_mp = 0;

%
% Mesh
%

meshroot = './../../Mesh_Handlers/meshfiles3d';
meshname = chooseMesh3d( 'Note: make sure to select a mesh of the unit cube' );
meshname = [meshroot '/' meshname];
load([meshname '.mat'])
fprintf('Loaded mesh %s\n', meshname);

%
% FETI-DP  options
%

Nx = input('Number of subdomains in x (>= 2) [2]: ');
if isempty(Nx) || (Nx < 2)
    Nx = 2;
end
Ny = input('Number of subdomains in y (>= 2) [2]: ');
if isempty(Ny) || (Ny < 2)
    Ny = 2;
end
Nz = input('Number of subdomains in z (>= 2) [2]: ');
if isempty(Nz) || (Nz < 2)
    Nz = 2;
end

% Diffusion coefficient
i_rho = input('Diffusion coefficient? (1) ro = 1; (2) rand; (3) rand exp. Choice [1]: ');
if isempty(i_rho)
    i_rho = 1;
end
switch i_rho
    case 1
        rho = ones(Nx, Ny, Nz);
    case 2
        rho = rand(Nx, Ny, Nz);
    case 3
        alpha = randi([-4 4], Nx, Ny, Nz);
        rho = 10.^alpha;
    otherwise
        disp('value not valid for ro ')
end

isVertexPrimal = input('Use vertices in primal space? 0/1 [1]: ');
if isempty(isVertexPrimal)
    isVertexPrimal = 1;
end

isEdgePrimal = input('Use edge averages in primal space? 0/1 [1]: ');
if isempty(isEdgePrimal)
    isEdgePrimal = 1;
end

isFacePrimal = input('Use face averages in primal space? 0/1 [0]: ');
if isempty(isFacePrimal)
    isFacePrimal = 0;
end

if (isVertexPrimal + isEdgePrimal + isFacePrimal) == 0
    error('The primal space cannot be empty')
end

% Choose how to implement the change of basis
approach2constraints = input(['Use explicit change of basis or local Lagrange multipliers ' ...
                              'to edge and face constraints? 1/2 [2]: ']);
if isempty(approach2constraints)
    approach2constraints = 2;
end

% Dirichlet preconditioner for the FETI-DP interface problem
use_pc = input('Use FETI-DP Dirichlet preconditioner? 0/1 [1]: ');
if isempty(use_pc)
    use_pc = 1;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create domain decomposition data structures

% Identify internal and boundary nodes on the reference mesh
[ refVertexDofs, refEdgeInternalDofs, refFaceInternalDofs, ...
  refInternalDofs, refBoundaryDofs ] = createPreliminaryDofPartition( mesh, k );

% Group element faces by interface face
refElementFacesByBoundary = groupFacesByBoundary( mesh );

% Replicate reference mesh to create a domain decomposition
meshArray = repMesh( mesh, Nx, Ny, Nz, ...
                     refVertexDofs, refEdgeInternalDofs, refFaceInternalDofs, ...
                     refElementFacesByBoundary );

% Identify degrees of freedom on the Dirichlet boundary
meshArray = ...
    findDirichletDofs( meshArray, Nx, Ny, Nz );

% Compute dimension of the global primal space and of the space of Lagrange
% multipliers \lambda
[ dimPrimal, dimLambda ] = ...
    spacesDim( meshArray, Nx, Ny, Nz, isVertexPrimal, isEdgePrimal, isFacePrimal );

% Construct primal and dual boolean operators and transformation matrices
% and matrices for LAgrange multiplier appraoch for primal averages
[ vertexPrimalDofs, vertexDualDofs, ...
  edgeAverageDofs, edgeAverageDofsLocal, edgeDualDofs, ...
  faceAverageDofs, faceAverageDofsLocal, faceDualDofs, ...
  Qedge, Tedge, ...
  Qface, Tface, ...
  Bprimal, Bdual, BdualScaled ] = ...
    subdomainOperators( meshArray, k, eoa, compressquad, use_mp, ...
                        Nx, Ny, Nz, ...
                        isVertexPrimal, isEdgePrimal, isFacePrimal, ...
                        dimPrimal, dimLambda, ...
                        rho );
                    
xcheck = linspace(0, 1, Nx+1);
ycheck = linspace(0, 1, Ny+1);
zcheck = linspace(0, 1, Nz+1);

if 0
    %
    % Test 1 for boolean: in this test case we are assuming that the only
    % primal variables are vertices
    %
    x = xcheck(2:end-1); %#ok<UNRCH>
    y = ycheck(2:end-1);
    z = zcheck(2:end-1);

    [xcheck, ycheck, zcheck] = meshgrid(xcheck, ycheck, zcheck);

    [X, Y, Z] = meshgrid(x, y, z);
    X = permute(X, [2,1,3]); X = X(:);
    Y = permute(Y, [2,1,3]); Y = Y(:);
    Z = permute(Z, [2,1,3]); Z = Z(:);

    checkBdual = 0;
    checkBdualScaled = 0;
    figure()
    for k = 1:Nz
        for j = 1:Ny
            for i = 1:Nx
                myX = Bprimal{i,j,k}*X;
                myY = Bprimal{i,j,k}*Y;
                myZ = Bprimal{i,j,k}*Z;

                checkBdual = checkBdual + sum( Bdual{i,j,k}, 2 );
                checkBdualScaled = checkBdualScaled + sum( BdualScaled{i,j,k}, 2 );
                
                hold on;
                plot3(xcheck(:), ycheck(:), zcheck(:), 'g*')
                plot3(myX, myY, myZ, 'r*')
                pause
                clf;
            end
        end
    end
    
    fprintf('Inf norm of error for Bdual: %f\n', norm(checkBdual, inf));
    fprintf('Inf norm of error for BdualScaled: %f\n', norm(checkBdualScaled, inf));
end

if 0
    %
    % Test 2 for BPRIMAL: manually assemble auxiliary vectors for testing 3 x 3 x 3
    % subdomains in the presence of both primal vertices and edges

    [xcheck, ycheck, zcheck] = meshgrid(xcheck, ycheck, zcheck);
    
    if (Nx ~= 3) || (Ny ~= 3) || (Nz ~= 3)
        error('Test case only working for Nx = Ny = Nz = 3')
    end

    x1 = [1/3 1/3 1/3 1/6 2/3 2/3 2/3 1/2 5/6];
    x2 = [x1 x1 1/3 2/3];
    x = [x2 x2 1/3 2/3 1/3 2/3]';

    y1 = [1/3 1/3 1/6 1/3 1/3 1/3 1/6 1/3 1/3];
    y2 = [y1 y1+1/3 5/6 5/6];
    y = [y2 y2 1/3 1/3 2/3 2/3]';

    z1 = [1/3 1/6 1/3 1/3 1/3 1/6 1/3 1/3 1/3];
    z2 = [z1 z1 1/3 1/3];
    z = [z2 z2+1/3 5/6 5/6 5/6 5/6]';

    checkBdual = 0;
    checkBdualScaled = 0;

    figure()
    for k = 1:Nz
        for j = 1:Ny
            for i = 1:Nx
                myX = Bprimal{i,j,k}*x;
                myY = Bprimal{i,j,k}*y;
                myZ = Bprimal{i,j,k}*z;

                checkBdual = checkBdual + sum( Bdual{i,j,k}, 2 );
                checkBdualScaled = checkBdualScaled + sum( BdualScaled{i,j,k}, 2 );

                numPrimalVertices = length(vertexPrimalDofs{i,j,k});

                hold on;
                plot3(xcheck(:), ycheck(:), zcheck(:), 'g*')
                plot3( myX(numPrimalVertices+1:end), ...
                       myY(numPrimalVertices+1:end), ...
                       myZ(numPrimalVertices+1:end), 'r*')
                plot3( myX(1:numPrimalVertices), ...
                       myY(1:numPrimalVertices), ...
                       myZ(1:numPrimalVertices), 'b*')
                pause
                clf;
            end
        end
    end
    
    fprintf('Inf norm of error for Bdual: %f\n', norm(checkBdual, inf));
    fprintf('Inf norm of error for BdualScaled: %f\n', norm(checkBdualScaled, inf));
end

if 1
    % Test 3 for vertexPrimalDofs, edgeAverageDofs, faceAverageDofs
    
    primalPatchV = [-1 -1 -1;  % reference vertex 1
                     0 -1 -1;  % reference vertex 2
                    -1  0 -1;  % ...
                     0  0 -1;
                    -1 -1  0;
                     0 -1  0;
                    -1  0  0;
                     nan nan nan];

    primalPatchE = [-1  0 -1  2;
                     0  0 -1  2;
                     0 -1 -1  3;
                     0  0 -1  3;
                    -1 -1  0  1;
                     0 -1  0  1;
                    -1  0  0  1;
                     nan nan nan nan;    % reference edge 8 introduces a new primal dof
                    -1  0  0  2;
                     nan nan nan nan;    % reference edge 10 introduces a new primal dof
                     0 -1  0  3;
                     nan nan nan nan];   % reference edge 12 introduces a new primal dof

    primalPatchF = [-1  0  0;
                     1  0  0;
                     0 -1  0;
                     0  1  0;
                     0  0 -1;
                     0  0  1];

%     figure()
%     grid on; axis equal; hold on; view(45, 15);
%     xlim([0,1]); ylim([0,1]); zlim([0,1]);
    checkBdual = 0;
    checkBdualScaled = 0;

    for k = 1:Nz
        for j = 1:Ny
            for i = 1:Nx
                linearSubdomainInd = i + Nx*(j-1) + Nx*Ny*(k-1);
                [interfaceVertices, interfaceEdges, interfaceFaces] = ...
                    getInterfaceVEF( Nx, Ny, Nz, i, j, k );

                checkBdual = checkBdual + sum( Bdual{i,j,k}, 2 );
                checkBdualScaled = checkBdualScaled + sum( BdualScaled{i,j,k}, 2 );
                
                if linearSubdomainInd > 1
                    % Compare primal vertices of current subdomain with its
                    % primal parent
                    for p = 1:length(interfaceVertices)
                        v = interfaceVertices(p);
                        if isVertexPrimal
                            if v < 8
                                y = meshArray{i,j,k}.coordinates(vertexPrimalDofs{i,j,k}(p),:) ...
                                    - meshArray{ i+primalPatchV(v,1), ...
                                                 j+primalPatchV(v,2), ...
                                                 k+primalPatchV(v,3)}.coordinates(vertexPrimalDofs{ i+primalPatchV(v,1), ...
                                                                                                      j+primalPatchV(v,2), ...
                                                                                                      k+primalPatchV(v,3) }(end),:);
                                err = norm(y);
                                if err > 1e-12
                                    error('We have got a problem for subdomain %i,%i,%i.', i, j, k);
                                end
                            end
                        else
                            
                        end
                    end
                    
                    for q = 1:length(interfaceEdges)
                        e = interfaceEdges(q);
                        if isEdgePrimal
                            if (e ~= 8) && (e ~= 10) && (e ~= 12)
                                [~, interfaceEdgesParent] = ...
                                    getInterfaceVEF( Nx, Ny, Nz, i + primalPatchE(e,1), ...
                                                                 j + primalPatchE(e,2), ...
                                                                 k + primalPatchE(e,3) );
                                test = interfaceEdgesParent == (2*primalPatchE(e,4)+6);
                                test = find(test);

                                y = meshArray{i,j,k}.coordinates(edgeAverageDofs{i,j,k}(q),:) ...
                                        - meshArray{ i + primalPatchE(e,1), ...
                                                     j + primalPatchE(e,2), ...
                                                     k + primalPatchE(e,3)}.coordinates(edgeAverageDofs{ ...
                                                          i + primalPatchE(e,1), ...
                                                          j + primalPatchE(e,2), ...
                                                          k + primalPatchE(e,3)}(test),:);

                                y2 = meshArray{i,j,k}.coordinates(meshArray{i,j,k}.edgeInternalDofs{e},:) ...
                                        - meshArray{ i + primalPatchE(e,1), ...
                                                     j + primalPatchE(e,2), ...
                                                     k + primalPatchE(e,3)}.coordinates( ...
                                                        meshArray{ i + primalPatchE(e,1), ...
                                                                   j + primalPatchE(e,2), ...
                                                                   k + primalPatchE(e,3) }.edgeInternalDofs{2*primalPatchE(e,4)+6},:);
                                err = norm(y);
                                err2 = norm(y2);
                                if err > 1e-12 || err2 > 1e-12
                                    error('We have got a problem for subdomain %i,%i,%i.', i, j, k);
                                end
                            end
                        end
                    end
                    
                    for r = 1:length(interfaceFaces)
                        f = interfaceFaces(r);
                        if isFacePrimal
                            if mod(f,2)
                                [~, ~, interfaceFacesParent] = ...
                                    getInterfaceVEF( Nx, Ny, Nz, i + primalPatchF(f,1), ...
                                                                 j + primalPatchF(f,2), ...
                                                                 k + primalPatchF(f,3) );
                                test = interfaceFacesParent == (f+1);
                                test = find(test);

                                y = meshArray{i,j,k}.coordinates(faceAverageDofs{i,j,k}(r),:) ...
                                        - meshArray{ i + primalPatchF(f,1), ...
                                                     j + primalPatchF(f,2), ...
                                                     k + primalPatchF(f,3)}.coordinates(faceAverageDofs{ ...
                                                          i + primalPatchF(f,1), ...
                                                          j + primalPatchF(f,2), ...
                                                          k + primalPatchF(f,3)}(test),:);

                                y2 = meshArray{i,j,k}.coordinates(meshArray{i,j,k}.faceInternalDofs{f},:) ...
                                        - meshArray{ i + primalPatchF(f,1), ...
                                                     j + primalPatchF(f,2), ...
                                                     k + primalPatchF(f,3)}.coordinates( ...
                                                        meshArray{i + primalPatchF(f,1), ...
                                                                  j + primalPatchF(f,2), ...
                                                                  k + primalPatchF(f,3)}.faceInternalDofs{f+1},:);
                                err = norm(y);
                                err2 = norm(y2);
                                if err > 1e-12 || err2 > 1e-12
                                    error('We have got a problem for subdomain %i,%i,%i.', i, j, k);
                                end
                            end
                        end
                    end
                end
            end
        end
    end
    fprintf('Inf norm of error for Bdual: %f\n', norm(checkBdual, inf));
    fprintf('Inf norm of error for BdualScaled: %f\n', norm(checkBdualScaled, inf));
end







