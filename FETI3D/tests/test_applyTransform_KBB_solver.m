%
% Solving 3D Poisson equation on the unit cube with FETI-DP and VEM on
%

addpath ../../Mesh_Handlers
addpath ../../Quadrature
addpath ../../Quadrature/cpp_modules
addpath ../../Basis_Functions
addpath ../../VEM_diffusion
addpath ../../VEM_diffusion_3D
addpath ../

fprintf('\nScript for solving Poisson equation in 3D with VEM (k = 1) and FETI-DP\n\n');

if license('test','Distrib_Computing_Toolbox')
    if isempty(gcp)
        parpool
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Inputs

%
% Polynomial degree
%
k = 1;

eoa = input('Extra order of accuracy for quadrature formulas [0]: ');
if isempty(eoa)
   eoa = 0; 
end
compressquad = input('Compress 2D quadrature rules? 0/1 [0]: ');
if isempty(compressquad)
    compressquad = 0;
end

%
% Type of basis functions
%
basis_type = 1;

%
% Multi-precision toolbox
%
use_mp = 0;

%
% Mesh
%

meshroot = './../../Mesh_Handlers/meshfiles3d';
meshname = chooseMesh3d( 'Note: make sure to select a mesh of the unit cube' );
meshname = [meshroot '/' meshname];
load([meshname '.mat'])
fprintf('Loaded mesh %s\n', meshname);

%
% FETI-DP  options
%

Nx = input('Number of subdomains in x (>= 2) [2]: ');
if isempty(Nx) || (Nx < 2)
    Nx = 2;
end
Ny = input('Number of subdomains in y (>= 2) [2]: ');
if isempty(Ny) || (Ny < 2)
    Ny = 2;
end
Nz = input('Number of subdomains in z (>= 2) [2]: ');
if isempty(Nz) || (Nz < 2)
    Nz = 2;
end

% Diffusion coefficient
i_rho = input('Diffusion coefficient? (1) ro = 1; (2) rand; (3) rand exp. Choice [1]: ');
if isempty(i_rho)
    i_rho = 1;
end
switch i_rho
    case 1
        rho = ones(Nx, Ny, Nz);
    case 2
        rho = rand(Nx, Ny, Nz);
    case 3
        alpha = randi([-4 4], Nx, Ny, Nz);
        rho = 10.^alpha;
    otherwise
        disp('value not valid for ro ')
end

isVertexPrimal = input('Use vertices in primal space? 0/1 [1]: ');
if isempty(isVertexPrimal)
    isVertexPrimal = 1;
end

isEdgePrimal = input('Use edge averages in primal space? 0/1 [1]: ');
if isempty(isEdgePrimal)
    isEdgePrimal = 1;
end

isFacePrimal = input('Use face averages in primal space? 0/1 [0]: ');
if isempty(isFacePrimal)
    isFacePrimal = 0;
end

if (isVertexPrimal + isEdgePrimal + isFacePrimal) == 0
    error('The primal space cannot be empty')
end

% Choose how to implement the change of basis
approach4constraints = input(['Use explicit change of basis or local Lagrange multipliers ' ...
                              'to edge and face constraints? 1/2 [2]: ']);
if isempty(approach4constraints)
    approach4constraints = 2;
end

% Dirichlet preconditioner for the FETI-DP interface problem
use_pc = input('Use FETI-DP Dirichlet preconditioner? 0/1 [1]: ');
if isempty(use_pc)
    use_pc = 1;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create domain decomposition data structures

load_data = input(['Do you want to load mesh data structures, ' ...
                   'stiffness matrices and load terms from memory? 0/1 [0]: ']);
if isempty(load_data)
    load_data = 0;
end
if load_data == 0
    % Identify internal and boundary nodes on the reference mesh
    [ refVertexDofs, refEdgeInternalDofs, refFaceInternalDofs, ...
      refInternalDofs ] = createPreliminaryDofPartition( mesh, k );

    % Group element faces by interface face
    refElementFacesByBoundary = groupFacesByBoundary( mesh );

    % Replicate reference mesh to create a domain decomposition
    meshArray = repMesh( mesh, Nx, Ny, Nz, ...
                         refVertexDofs, refEdgeInternalDofs, refFaceInternalDofs, ...
                         refElementFacesByBoundary );

    % Identify degrees of freedom on the Dirichlet boundary
    meshArray = ...
        findDirichletDofs( meshArray, Nx, Ny, Nz );

    % Assemble subdomain stiffness matrices and right hand sides
    [ K, F ] = ...
        subdomainAssembler( meshArray, Nx, Ny, Nz, rho, ...
                            k, eoa, compressquad, use_mp );

    save_data = input(['Do you want to save mesh data structures, ' ...
                       'stiffness matrices and load terms? 0/1 [0]: ']);
    if isempty(save_data)
        save_data = 0;
    end
    if save_data ~= 0
        mshName = strsplit(meshname, {'./../../Mesh_Handlers/meshfiles3d/', '/'});
        mshName = strjoin(mshName(2:end), '_');
        filename = sprintf(['../FETI3D/workspaces/%s__k_%i__Nx_%i_Ny_%i_Nz_%i__' ...
                            'i_rho_%i__eoa_%i__compressquad_%i__use_mp_%i.mat'], ...
                            mshName, k, Nx, Ny, Nz, i_rho, eoa, compressquad, use_mp );
        save(filename, 'refVertexDofs', 'refEdgeInternalDofs', ...
                       'refFaceInternalDofs', 'refInternalDofs', ...
                       'refElementFacesByBoundary', ...
                       'meshArray', 'K', 'F');
    end
else
    filename = input('Enter filename of the workspace to be loaded: ', 's');
    load(filename)
end
                       
% Compute dimension of the global primal space and of the space of Lagrange
% multipliers \lambda
[ dimPrimal, dimLambda ] = ...
    spacesDim( meshArray, Nx, Ny, Nz, isVertexPrimal, isEdgePrimal, isFacePrimal );

% Construct primal and dual boolean operators and transformation matrices
[ vertexPrimalDofs, vertexDualDofs,  ...
  edgeAverageDofs, edgeAverageDofsLocal, edgeDualDofs, ...
  faceAverageDofs, faceAverageDofsLocal, faceDualDofs, ...
  Qedge, Tedge, ...
  Qface, Tface, ...
  Bprimal, Bdual, BdualScaled ] = ...
    subdomainOperators( meshArray, k, eoa, compressquad, use_mp, ...
                        Nx, Ny, Nz, ...
                        isVertexPrimal, isEdgePrimal, isFacePrimal, ...
                        dimPrimal, dimLambda, ...
                        rho );

% Explicitly apply the change of basis if needed
if approach4constraints == 1
    [ K, F ] = applyTransform( meshArray, Nx, Ny, Nz, ...
                               isEdgePrimal, isFacePrimal, ...
                               refInternalDofs, vertexPrimalDofs, vertexDualDofs, ...
                               edgeDualDofs, faceDualDofs, Tedge, Tface, K, F );
end

% Check that K\F provides a solution with zero averages on primal
% edges/faces.
% In the explicit change of basis approach, the idea is as follows: 
% let the exact solution be the constant one. Then,
% after the change of basis, K{is,js,ks}\F{is,js,ks} provides, for those
% subdomain which are not floating, for example those on the domain
% boundary, a solution that, in correspondence at primal edges/faces, must
% be all zero (because the basis functions with zero average do not
% contribute to the solution) except for a single value which should be
% equal to the constant value of the solution. To check this out, inspect
% sol in debug mode.
% In the Lagrange multiplier approach, you have to check that the degree
% of freedom corresponding to a primal edge/face average is zero, because
% this is exactly what we are imposing with the additional Lagrange
% multipliers. For example, for subdomain 1,1,1, you may want to do something like this:
%
% a = inv(Tface{1,1,1}{2})*U(mesh.faceInternalDofs{4}), a(faceAverageDofsLocal{1,1,1}(2))

for ks = 1:Nz
    for js = 1:Ny
        for is = 1:Nx
            if approach4constraints == 1
                sol = K{is,js,ks}\F{is,js,ks};
            else
                [~, interfaceEdges, interfaceFaces] = ...
                    getInterfaceVEF( Nx, Ny, Nz, is, js, ks );
                
                mesh = meshArray{is,js,ks};
                
                boundaryDofs = [ mesh.vertexDofs; vertcat( mesh.edgeInternalDofs{:} ); vertcat( mesh.faceInternalDofs{:} ) ];
                
                U = KBBsolver( mesh, k, use_mp, ...
                        interfaceEdges, interfaceFaces, ...
                        isEdgePrimal, isFacePrimal, ...
                        K{is,js,ks}, F{is,js,ks}, ...
                        refInternalDofs, boundaryDofs, ...
                        vertexPrimalDofs{is,js,ks}, ...
                        Qedge{is,js,ks}, Qface{is,js,ks} );
            end
        end
    end
end

