
addpath ../../Mesh_Handlers
addpath ../../Quadrature
addpath ../../Quadrature/cpp_modules
addpath ../../Basis_Functions
addpath ../../VEM_diffusion
addpath ../../VEM_diffusion_3D
addpath ../


meshroot = '../../Mesh_Handlers/meshfiles3d';
meshname = chooseMesh3d( 'Note: make sure to select a mesh of the unit cube' );
meshname = [meshroot '/' meshname];
load([meshname '.mat'])
fprintf('Loaded mesh %s\n', meshname);

k = 1;

[vertexDofs, edgeInternalDofs] = ...
    createPreliminaryDofPartition( mesh, k );

refEdge2endpoints = [1 3;
                     2 4;
                     1 2;
                     3 4;
                     1 5;
                     2 6;
                     3 7;
                     4 8;
                     5 7;
                     6 8;
                     5 6;
                     7 8];

numEdges = 12;
for i = 1:numEdges
    extendedEdgeDofs = [vertexDofs(refEdge2endpoints(i,1));
                        edgeInternalDofs{i};
                        vertexDofs(refEdge2endpoints(i,2))];

    [ ~, T, primalDof, dualDofs ] = constructEdgeTransform( mesh, k, extendedEdgeDofs );
    v = [mesh.coordinates(extendedEdgeDofs(3:end),1)-mesh.coordinates(extendedEdgeDofs(1:end-2),1) ...
         mesh.coordinates(extendedEdgeDofs(3:end),2)-mesh.coordinates(extendedEdgeDofs(1:end-2),2) ...
         mesh.coordinates(extendedEdgeDofs(3:end),3)-mesh.coordinates(extendedEdgeDofs(1:end-2),3)];

    edgeIntegrals = sqrt(v(:,1).^2 + v(:,2).^2 + v(:,3).^2) / 2.0;
    edgeIntegrals = T'*edgeIntegrals;

    fprintf('Checking integrals on edge %i:\n', i)
    disp(edgeIntegrals)
end






