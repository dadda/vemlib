addpath ../../Mesh_Handlers
addpath ../../Quadrature
addpath ../../Quadrature/cpp_modules
addpath ../../Basis_Functions
addpath ../../VEM_diffusion
addpath ../../VEM_diffusion_3D
addpath ..

load ../../Mesh_Handlers/meshfiles3d/voronoi/cube/unitcube_voro_Nelt8.mat

tic;
facesByBoundary = groupFacesByBoundary( mesh );
toc;

figure()
hold on

color = {'r', 'g', 'b', 'c', 'y', 'm'};

for i = 1:6
    for f = 1:length(facesByBoundary{i})
        tedges = floor( ( mesh.face2hedges{facesByBoundary{i}(f)}+1 )/2 );
        edges  = mesh.edge2vertices(tedges,:)';
        edges_orientation = mod(mesh.face2hedges{facesByBoundary{i}(f)}, 2);

        ned = length(tedges);
        faceNodes = edges_orientation .* edges(1,:) + (1-edges_orientation).*edges(2,:);
        patch( mesh.coordinates(faceNodes,1), ...
               mesh.coordinates(faceNodes,2), ...
               mesh.coordinates(faceNodes,3), ...
               color{i} );
    end
end

