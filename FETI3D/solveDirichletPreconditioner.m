function z = solveDirichletPreconditioner( r, ...
                                           Nx, Ny, Nz, ...
                                           dimLambda, ...
                                           internalDofs, ...
                                           vertexDualDofs, ...
                                           edgeInternalDualDofs, ...
                                           faceInternalDualDofs, ...
                                           BdualScaled, K, runAsFor )
% Solve for the preconditioned residual, i.e. given a residual r and the
% FETI-DP preconditioner M, we want to compute the solution z of the system
%
% M * z = r  ==>  z = M^-1*r
%
% Note: Klawonn and Farhat use the term preconditioner to refer to M^-1.

z = zeros(dimLambda, 1);

numSubdomains = Nx*Ny*Nz;

numWorkers = getParforArg(runAsFor);

parfor ( s = 1:numSubdomains, numWorkers )
    dualDofs = [ vertexDualDofs{s};
                 edgeInternalDualDofs{s};
                 faceInternalDualDofs{s} ];

	tmp = K{s}(internalDofs, internalDofs) \ ...
          ( K{s}(internalDofs, dualDofs) * (BdualScaled{s}' * r) );
    
    z = z + BdualScaled{s} * ( K{s}(dualDofs, dualDofs) * (BdualScaled{s}' * r) ...
                               - K{s}(dualDofs, internalDofs) * tmp );
end

end

