function [ Kstar, fstar, dr ] = assembleDualPrimalOperators( meshArray, k, use_mp, ...
                                                             Nx, Ny, Nz, ...
                                                             isEdgePrimal, isFacePrimal, ...
                                                             dimPrimal, dimLambda, ...
                                                             approach4constraints, ...
                                                             internalDofs, ...
                                                             vertexPrimalDofs, vertexDualDofs, ...
                                                             edgeAverageDofs, edgeAverageDofsLocal, edgeInternalDualDofs, ...
                                                             faceAverageDofs, faceAverageDofsLocal, faceInternalDualDofs, ...
                                                             Qedge, Tedge, ...
                                                             Qface, Tface, ...
                                                             Bprimal, Bdual, K, F, ...
                                                             runAsFor )
% Assemble K^*_{cc}, f^*_c and d_r

Kstar = sparse(dimPrimal, dimPrimal);
fstar = zeros(dimPrimal, 1);
dr    = zeros(dimLambda, 1);

numSubdomains = Nx*Ny*Nz;

numWorkers = getParforArg(runAsFor);

parfor ( s = 1:numSubdomains, numWorkers )
    primalDofs = [ vertexPrimalDofs{s};
                   edgeAverageDofs{s};
                   faceAverageDofs{s} ];

    dualDofs = [ vertexDualDofs{s};
                 edgeInternalDualDofs{s};
                 faceInternalDualDofs{s} ];

    numInternalDofs = length(internalDofs);
    
    % Farhat uses 'r' to denote the set of both internal and dual (both
    % transformed and not) degrees of freedom (\Delta in Klawonn), whereas 'c'
    % is used for primal dofs (\Pi in Klawonn). It follows, for example, that:
    %
    % Farhat: K_{rc} <------> Klawonn: [K_{internal, primal}; K_{dual, primal}]
    
    if approach4constraints == 1

        % Using the notation of Farhat, the first length(primalDofs)
        % columns of tmp contain K_{rr}^{-1}*K_{rc}, whereas the last
        % column contain K_{rr}^{-1}*f_r.
        tmp = K{s}( [ internalDofs; dualDofs ], [ internalDofs; dualDofs ] ) \ ...
            [ K{s}( [ internalDofs; dualDofs ], primalDofs ) ...
              F{s}( [ internalDofs; dualDofs ] ) ];

        Kstar = Kstar + Bprimal{s}' * ( K{s}( primalDofs, primalDofs ) ...
                                        - K{s}( primalDofs, [ internalDofs; dualDofs ] ) * tmp(:, 1:end-1) ) ...
                                    * Bprimal{s};

        fstar = fstar + Bprimal{s}' * ( F{s}( primalDofs ) ...
                                        - K{s}( primalDofs, [ internalDofs; dualDofs ] ) * tmp(:, end) );

        % Remark: Bdual{s} acts only on dual dofs
        dr    = dr + Bdual{s} * tmp(numInternalDofs+1:end, end);
    else
        
%         % Remark: dual dofs on primal edges/faces do not need to be
%         % transformed because KBBsolver works on the original, i.e. not
%         % transformed, variables (and it does not act on transformed primal,
%         % as it impose them to be 0!) and returns a solution expressed in
%         % terms of the original dofs. So, we will only be transforming
%         % primal (edge and/or face) dofs.
%         
%         dim = size(K{s},1);
%         dimPrimalLocal = length(primalDofs);
%         
%         Krc = zeros(dim, dimPrimalLocal);
%         Kcc = zeros(dimPrimalLocal, dimPrimalLocal);
%         fc  = zeros(dimPrimalLocal, 1);
% 
%         numPrimalVertices = length(vertexPrimalDofs{s});
%         
%         averageDofsLocal = [ edgeAverageDofsLocal{s}; faceAverageDofsLocal{s} ];
%         numTransformations = length(averageDofsLocal);
%         
%         ks = floor( (s-1) / (Nx*Ny) ) + 1;
%         js = floor( (s - Nx*Ny*(ks-1) - 1) / Nx ) + 1;
%         is = s - Nx*Ny*(ks-1) - Nx*(js-1);
%         
%         [~, interfaceEdges, interfaceFaces] = ...
%             getInterfaceVEF( Nx, Ny, Nz, is, js, ks );
% 
%         interfaceDofs = [ vertexDualDofs{s};
%                           vertcat( meshArray{s}.edgeInternalDofs{interfaceEdges} );
%                           vertcat( meshArray{s}.faceInternalDofs{interfaceFaces} ) ];
% 
%         transformedDofs = {};
% 
%         if isEdgePrimal
%             transformedDofs = vertcat( transformedDofs, meshArray{s}.edgeInternalDofs(interfaceEdges) );
%         end
%         if isFacePrimal
%             transformedDofs = vertcat( transformedDofs, meshArray{s}.faceInternalDofs(interfaceFaces) );
%         end
%     
%         T = vertcat( Tedge{s}(:), Tface{s}(:) );
% 
%         % Primal vertices do not need to be transformed
%         Krc( [ internalDofs; interfaceDofs ], 1:numPrimalVertices ) = ...
%             K{s}( [ internalDofs; interfaceDofs ], vertexPrimalDofs{s} );
%         
%         Kcc(1:numPrimalVertices, 1:numPrimalVertices) = ...
%             K{s}( vertexPrimalDofs{s}, vertexPrimalDofs{s} );
%         
%         fc(1:numPrimalVertices) = F{s}( vertexPrimalDofs{s} );
%         
%         for j = 1:numTransformations
%             Krc( [ internalDofs; interfaceDofs ], numPrimalVertices+j ) = ...
%                 K{s}( [ internalDofs; interfaceDofs ], transformedDofs{j} ) ...
%                     * T{j}(:, averageDofsLocal(j));
%             
%             Kcc( 1:numPrimalVertices, numPrimalVertices+j ) = ...
%                 K{s}( vertexPrimalDofs{s}, transformedDofs{j} ) ...
%                     * T{j}(:, averageDofsLocal(j));
% 
%             Kcc( numPrimalVertices+j, 1:numPrimalVertices ) = ...
%                 Kcc( 1:numPrimalVertices, numPrimalVertices+j )';
% 
%             Kcc( numPrimalVertices+j, numPrimalVertices+j ) = T{j}(:,averageDofsLocal(j))' * ...
%                        K{s}( transformedDofs{j}, transformedDofs{j} ) ...
%                        * T{j}(:,averageDofsLocal(j));
%                    
%             for i = 1:j-1
%                 Kcc( numPrimalVertices+i, numPrimalVertices+j ) = T{i}(:,averageDofsLocal(i))' * ...
%                            K{s}( transformedDofs{i}, transformedDofs{j} ) ...
%                            * T{j}(:,averageDofsLocal(j));
%                        
%                 Kcc( numPrimalVertices+j, numPrimalVertices+i ) = ...
%                     Kcc( numPrimalVertices+i, numPrimalVertices+j );
%             end
%             
%             fc( numPrimalVertices+j ) = T{j}(:,averageDofsLocal(j))' * F{s}( transformedDofs{j} );
%         end
%         
%         boundaryDofs = ...
%             [ meshArray{s}.vertexDofs;
%               vertcat( meshArray{s}.edgeInternalDofs{:} );
%               vertcat( meshArray{s}.faceInternalDofs{:} ) ];
% 
%         tmp = KBBsolver( meshArray{s}, k, use_mp, ...
%                          interfaceEdges, interfaceFaces, ...
%                          isEdgePrimal, isFacePrimal, ...
%                          K{s}, [Krc F{s}], ...
%                          internalDofs, boundaryDofs, ...
%                          vertexPrimalDofs{s}, ...
%                          Qedge{s}, Qface{s} );
% 
%         Kstar = Kstar + Bprimal{s}' * ( Kcc - Krc' * tmp(:, 1:end-1) ) ...
%                                     * Bprimal{s};
% 
%         fstar = fstar + Bprimal{s}' * ( fc - Krc' * tmp(:, end) );
%         
%         dr    = dr + Bdual{s} * tmp(dualDofs, end);
    end
end


end

