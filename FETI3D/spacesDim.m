function [ dP, dL ] = spacesDim( meshArray, Nx, Ny, Nz, isVertexPrimal, isEdgePrimal, isFacePrimal )

% Dimension of the primal space \hat W_\Pi
if isVertexPrimal
    dPv = (Nx-1)*(Ny-1)*(Nz-1);
else
    dPv = 0;
end

if isEdgePrimal
    dPe = (Nx-1) * (Ny-1) * Nz ...                % "pillar" edges
          + ((Nx-1) * Ny + (Ny-1) * Nx) * (Nz-1);   % "floor" edges
else
    dPe = 0;
end

if isFacePrimal
    dPf = ((Nx-1) * Ny + (Ny-1) * Nx) * Nz ... % "pillar" faces
          + Nx * Ny * (Nz-1);                    % "floor" faces
else
    dPf = 0;
end

dP = dPv + dPe +dPf;

% Dimension of the space of Lagrange multipliers for continuity
% constraints. We are considering the fully redundant case.

% Edge dofs
dLe = 0;

tmp = 0;
for k = 1:Nz-1
    for j = 1:Ny-1
        tmp = tmp + length( meshArray{1,j,k}.edgeInternalDofs{12} );
    end
end
tmp = tmp * Nx;
dLe = dLe + tmp;

tmp = 0;
for k = 1:Nz-1
    for i = 1:Nx-1
        tmp = tmp + length( meshArray{i,1,k}.edgeInternalDofs{10} );
    end
end
tmp = tmp * Ny;
dLe = dLe + tmp;

tmp = 0;
for j = 1:Ny-1
    for i = 1:Nx-1
        tmp = tmp + length( meshArray{i,j,1}.edgeInternalDofs{8} );
    end
end
tmp = tmp * Nz;
dLe = dLe + tmp;

% Face dofs
dLf = 0;

tmp = 0;
for k = 1:Nz-1
    tmp = tmp + length( meshArray{1,1,k}.faceInternalDofs{6} );
end
tmp = tmp * Nx * Ny;
dLf = dLf + tmp;

tmp = 0;
for j = 1:Ny-1
    tmp = tmp + length( meshArray{1,j,1}.faceInternalDofs{4} );
end
tmp = tmp * Nx * Nz;
dLf = dLf + tmp;

tmp = 0;
for i = 1:Nx-1
    tmp = tmp + length( meshArray{i,1,1}.faceInternalDofs{2} );
end
tmp = tmp * Ny * Nz;
dLf = dLf + tmp;

% Update dofs according to the choice of primal space
if isVertexPrimal
    dLv = 0;
else
    dLv = (Nx-1)*(Ny-1)*(Nz-1); 
end

if isEdgePrimal
    dLe = dLe - dPe;
end

if isFacePrimal
    dLf = dLf - dPf;
end

% In the fully redundant case a vertex is shared by m = 8 subdomains, which
% gives m*(m-1)/2 = 28 constraints. Similarly, an edge is shared by m = 4
% domains, providing 6 constraints for each dual dof on an interface edge.
% A dual dof on an interface face, instead, only provides 1 constraint.

dL = 28 * dLv + 6 * dLe + dLf;

end









