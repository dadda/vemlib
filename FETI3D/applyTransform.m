function [ K, F ] = applyTransform( meshArray, Nx, Ny, Nz, ...
                                    isVertexPrimal, isEdgePrimal, isFacePrimal, ...
                                    Tvertex, Tedge, Tface, ...
                                    K, F, runAsFor )

numSubdomains = Nx*Ny*Nz;

% numWorkers    = getParforArg(runAsFor);
% parfor ( s = 1:numSubdomains, numWorkers )
for s = 1:numSubdomains

    ks = floor( (s-1) / (Nx*Ny) ) + 1;
    js = floor( (s - Nx*Ny*(ks-1) - 1) / Nx ) + 1;
    is = s - Nx*Ny*(ks-1) - Nx*(js-1);

    [interfaceVertices, interfaceEdges, interfaceFaces] = ...
        getInterfaceVEF( Nx, Ny, Nz, is, js, ks );

    transformedDofs = cell(3,1);
    if ~isVertexPrimal
        transformedDofs{1} = meshArray{s}.vertexDofs( interfaceVertices );
    end
    if isEdgePrimal || isFacePrimal
        transformedDofs{2} = vertcat( meshArray{s}.edgeInternalDofs{interfaceEdges} );
    end
    if isFacePrimal
        transformedDofs{3} = vertcat( meshArray{s}.faceInternalDofs{interfaceFaces} );
    end
    transformedDofs = vertcat( transformedDofs{:} );

    if ~isempty(transformedDofs)
        tmp = vertcat( Tvertex{s}{:}, Tedge{s}{:}, Tface{s}{:} );

        dimension          = size(K{s},1);
        notTransformedDofs = setdiff((1:dimension)', transformedDofs);

        T = sparse( [tmp(:,1); notTransformedDofs], ...
                    [tmp(:,2); notTransformedDofs], ...
                    [tmp(:,3); ones(length(notTransformedDofs),1)] );

        tmp  = K{s} * T;
        K{s} = T'*tmp;
        tmp  = T'*F{s};
        F{s} = tmp;
    end
end

end
