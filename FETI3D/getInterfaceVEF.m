function [ vertices, edges, faces ] = ...
                getInterfaceVEF( Nx, Ny, Nz, is, js, ks )
% Get interface vertices, edges, and faces for each subdomain

iMap = (is == 1) + 2 * ((is > 1) && (is < Nx)) + 3 * (is == Nx);
jMap = (js == 1) + 2 * ((js > 1) && (js < Ny)) + 3 * (js == Ny);
kMap = (ks == 1) + 2 * ((ks > 1) && (ks < Nz)) + 3 * (ks == Nz);

subdomainMap = iMap + 3 * (jMap-1) + 9 * (kMap-1);

switch subdomainMap
    case 1
        % is == 1, js == 1, ks == 1
        vertices = 8;
        edges    = [8 10 12];
        faces    = [2 4 6];
    case 2
        % 1 < is < Nx, js == 1, ks == 1
        vertices = [7 8];
        edges    = [7 8 9 10 12];
        faces    = [1 2 4 6];
    case 3
        % is == Nx, js == 1, ks == 1
        vertices = 7;
        edges    = [7 9 12];
        faces    = [1 4 6];
    case 4
        % is == 1, 1 < js < Ny, ks == 1
        vertices = [6 8];
        edges    = [6 8 10 11 12];
        faces    = [2 3 4 6];
    case 5
        vertices = 5:8;
        edges    = 5:12;
        faces    = [1:4 6];
    case 6
        vertices = [5 7];
        edges    = [5 7 9 11 12];
        faces    = [1 3 4 6];
    case 7
        vertices = 6;
        edges    = [6 10 11];
        faces    = [2 3 6];
    case 8
        vertices = [5 6];
        edges    = [5 6 9:11];
        faces    = [1:3 6];
    case 9
        vertices = 5;
        edges    = [5 9 11];
        faces    = [1 3 6];
    case 10
        vertices = [4 8];
        edges    = [2 4 8 10 12];
        faces    = [2 4 5 6];
    case 11
        vertices = [3 4 7 8];
        edges    = [1 2 4 7 8 9 10 12];
        faces    = [1 2 4:6];
    case 12
        vertices = [3 7];
        edges    = [1 4 7 9 12];
        faces    = [1 4:6];
    case 13
        vertices = [2 4 6 8];
        edges    = [2:4 6 8 10:12];
        faces    = 2:6;
    case 14
        vertices = 1:8;
        edges    = 1:12;
        faces    = 1:6;
    case 15
        vertices = [1 3 5 7];
        edges    = [1 3 4 5 7 9 11 12];
        faces    = [1 3:6];
    case 16
        vertices = [2 6];
        edges    = [2 3 6 10 11];
        faces    = [2 3 5 6];
    case 17
        vertices = [1 2 5 6];
        edges    = [1:3 5 6 9:11];
        faces    = [1:3 5 6];
    case 18
        vertices = [1 5];
        edges    = [1 3 5 9 11];
        faces    = [1 3 5 6];
    case 19
        vertices = 4;
        edges    = [2 4 8];
        faces    = [2 4 5];
    case 20
        vertices = [3 4];
        edges    = [1 2 4 7 8];
        faces    = [1 2 4 5];
    case 21
        vertices = 3;
        edges    = [1 4 7];
        faces    = [1 4 5];
    case 22
        vertices = [2 4];
        edges    = [2 3 4 6 8];
        faces    = [2 3:5];
    case 23
        vertices = 1:4;
        edges    = 1:8;
        faces    = 1:5;
    case 24
        vertices = [1 3];
        edges    = [1 3 4 5 7];
        faces    = [1 3:5];
    case 25
        vertices = 2;
        edges    = [2 3 6];
        faces    = [2 3 5];
    case 26
        vertices = [1 2];
        edges    = [1:3 5 6];
        faces    = [1:3 5];
    case 27
        vertices = 1;
        edges    = [1 3 5];
        faces    = [1 3 5];
end

end

