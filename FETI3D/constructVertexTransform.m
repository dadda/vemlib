function T = constructVertexTransform( mesh, k, interfaceVertexIdx, ...
                                       isEdgePrimal, isFacePrimal, ...
                                       faceIntegrals )

refVertex2refAdjacentEdges = [ 1 3 5;
                               2 3 6;
                               1 4 7;
                               2 4 8;
                               5 9 11;
                               6 10 11;
                               7 9 12;
                               8 10 12 ];

% Follow the reference edges as indicated by refVertex2refAdjacentEdges
refVertex2refAdjacentVertices = [ 3 2 5;
                                  4 1 6;
                                  1 4 7;
                                  2 3 8;
                                  1 7 6;
                                  2 8 5;
                                  3 5 8;
                                  4 6 7 ];


% This matrix tells whether the adjacent reference edges are entering or exiting
% a reference vertex. Convention: -1, entering; 1, exiting.
adjacentRefEdgesOrientation = [ 1 1 1;
                                1 -1 1;
                                -1 1 1;
                               -1 -1 1;
                               -1 1 1;
                               -1 1 -1;
                               -1 -1 1;
                              -1 -1 -1 ];

% For each i,j pair, where i denotes the i-th interface vertex,
% opposingFaces[i,j] represents the interface face bounded by the interface
% edges emanating from i, excluded the reference edge given by
% refVertex2refAdjacentEdges[i,j]
opposingFaces = [ 3 1 5;
                  3 2 5;
                  4 1 5;
                  4 2 5;
                  6 3 1;
                  6 3 2;
                  6 4 1;
                  6 4 2 ];

if k == 1
    primalFlag = isEdgePrimal + 2*isFacePrimal;
    
    switch primalFlag

        case 1

            %
            % Only subdomain edges are primal
            %

            me     = mesh.vertexDofs(interfaceVertexIdx);
            T      = zeros(4, 3); % I, J, V triplets
            T(1,:) = [ me me 1 ];
            for i = 1:3
                if adjacentRefEdgesOrientation(interfaceVertexIdx,i) == 1
                    T(i+1,1)  = mesh.edgeInternalDofs{refVertex2refAdjacentEdges(interfaceVertexIdx,i)}(1);
                    if length(mesh.edgeInternalDofs{ refVertex2refAdjacentEdges(interfaceVertexIdx,i) }) >= 2
                        neighbors = mesh.edgeInternalDofs{ refVertex2refAdjacentEdges(interfaceVertexIdx,i) }(1:2);
                    else
                        % Handle the special case of a subdomain having a
                        % single node on an interface edge
                        neighbors = [ T(i+1,1), mesh.vertexDofs( refVertex2refAdjacentVertices(interfaceVertexIdx,i) ) ];
                    end
                else
                    T(i+1,1)  = mesh.edgeInternalDofs{refVertex2refAdjacentEdges(interfaceVertexIdx,i)}(end);
                    if length(mesh.edgeInternalDofs{ refVertex2refAdjacentEdges(interfaceVertexIdx,i) }) >= 2
                        neighbors = mesh.edgeInternalDofs{ refVertex2refAdjacentEdges(interfaceVertexIdx,i) }(end:-1:end-1);
                    else
                        % Handle the special case of a subdomain having a
                        % single node on an interface edge
                        neighbors = [ T(i+1,1), mesh.vertexDofs( refVertex2refAdjacentVertices(interfaceVertexIdx,i) ) ];
                    end
                end
                T(i+1,2) = me;

                L = sqrt( (mesh.coordinates(neighbors,1) - mesh.coordinates(me,1)).^2 ...
                          + (mesh.coordinates(neighbors,2) - mesh.coordinates(me,2)).^2 ...
                          + (mesh.coordinates(neighbors,3) - mesh.coordinates(me,3)).^2 );

                T(i+1,3) = -L(1)/L(2);
            end

        case 2

            %
            % Only subdomain faces are primal
            %

            me   = mesh.vertexDofs(interfaceVertexIdx);
            T    = cell(4, 1);
            T{1} = [ me me 1 ];

            for i = 1:3
                interfaceFaceIdx = opposingFaces(interfaceVertexIdx,i);
                connections      = mesh.faceInternalDofs{interfaceFaceIdx};
                Nconn            = length(connections);
                T{1+i}           = [ connections ...
                                     me*ones(Nconn,1) ...
                                     -1/Nconn*faceIntegrals( me, interfaceFaceIdx )./faceIntegrals( connections, interfaceFaceIdx ) ];
            end

            T = vertcat( T{:} );

        case 3

            %
            % Both subdomain edges and faces are primal
            %

            me   = mesh.vertexDofs(interfaceVertexIdx);
            T    = cell(7, 1);
            T{1} = [ me me 1 ];

            % First set zero edge averages
            for i = 1:3
                if adjacentRefEdgesOrientation(interfaceVertexIdx,i) == 1
                    if length(mesh.edgeInternalDofs{ refVertex2refAdjacentEdges(interfaceVertexIdx,i) }) >= 2
                        neighbors = mesh.edgeInternalDofs{ refVertex2refAdjacentEdges(interfaceVertexIdx,i) }(1:2);
                    else
                        % Handle the special case of a subdomain having a
                        % single node on an interface edge
                        neighbors = [ mesh.edgeInternalDofs{ refVertex2refAdjacentEdges(interfaceVertexIdx,i) }(1), ...
                                      mesh.vertexDofs( refVertex2refAdjacentVertices(interfaceVertexIdx,i) ) ];
                    end
                else
                    if length(mesh.edgeInternalDofs{ refVertex2refAdjacentEdges(interfaceVertexIdx,i) }) >= 2
                        neighbors = mesh.edgeInternalDofs{ refVertex2refAdjacentEdges(interfaceVertexIdx,i) }(end:-1:end-1);
                    else
                        % Handle the special case of a subdomain having a
                        % single node on an interface edge
                        neighbors = [ mesh.edgeInternalDofs{ refVertex2refAdjacentEdges(interfaceVertexIdx,i) }(end), ...
                                      mesh.vertexDofs( refVertex2refAdjacentVertices(interfaceVertexIdx,i) ) ];
                    end
                end

                L = sqrt( (mesh.coordinates(neighbors,1) - mesh.coordinates(me,1)).^2 ...
                          + (mesh.coordinates(neighbors,2) - mesh.coordinates(me,2)).^2 ...
                          + (mesh.coordinates(neighbors,3) - mesh.coordinates(me,3)).^2 );

                T{1+i} = [ neighbors(1) me -L(1)/L(2) ];
            end

            % Now set zero face average
            for i = 1:3
                interfaceFaceIdx = opposingFaces(interfaceVertexIdx,i);
                connections      = mesh.faceInternalDofs{interfaceFaceIdx};
                Nconn            = length(connections);
                notI             = [1:i-1 i+1:3];
                T{4+i}           = [ connections ...
                                     me*ones(Nconn,1) ...
                                     -1/Nconn*( faceIntegrals( me, interfaceFaceIdx ) ...
                                                + T{notI(1)+1}(3) * faceIntegrals(T{notI(1)+1}(1), interfaceFaceIdx) ...
                                                + T{notI(2)+1}(3) * faceIntegrals(T{notI(2)+1}(1), interfaceFaceIdx) )./faceIntegrals( connections, interfaceFaceIdx ) ];
            end

            T = vertcat( T{:} );

    end

else
    error('FETI-DP is not supported for k > 1, yet')
end

end
