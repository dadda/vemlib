function [ faceAverageDofs, faceAverageDofsLocal, primalFaceDofs, Tface ] = ...
                processPrimalFaces( refPrimalFaces, mesh, k, eoa, compressquad, use_mp )

numPrimalFaces = length(refPrimalFaces);

faceAverageDofs      = zeros(numPrimalFaces, 1);
faceAverageDofsLocal = zeros(numPrimalFaces, 1);
primalFaceDofs       = cell(numPrimalFaces, 1);
Tface                = cell(numPrimalFaces, 1);

for f = 1:numPrimalFaces
    [ Tface{f}, faceAverageDofsLocal(f) ] = ...
        constructFaceTransform( mesh, k, mesh.elementFacesByBoundary{refPrimalFaces(f)}, ...
                                mesh.faceInternalDofs{refPrimalFaces(f)}, ...
                                eoa, compressquad, use_mp );
    faceAverageDofs(f) = mesh.faceInternalDofs{ refPrimalFaces(f) }( faceAverageDofsLocal(f) );                            
end

end

