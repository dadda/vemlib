function faceIntegrals = computeSubdomainFacesIntegrals( mesh, k, eoa, compressquad, use_mp, interfaceFaces )

faceIntegrals     = zeros(mesh.Nver, 6);
numInterfaceFaces = length(interfaceFaces);

for i = 1:numInterfaceFaces
    interfaceFaceIdx = interfaceFaces(i);
    
    faceByBoundary = mesh.elementFacesByBoundary{interfaceFaceIdx};
    faceByBoundary = reshape(faceByBoundary, 1, length(faceByBoundary));
    for f = faceByBoundary
        element = mesh.face2elements(f, 1);        
        tfaces  = floor( ( mesh.element2hfaces{element}+1 )/2 );
        faces_orientation = mod(mesh.element2hfaces{element}, 2);

        % Locate the face inside the element by comparing topological
        % indices
        fLocal = find(tfaces == f);
        
        % Topological edge index and edge orientation
        tedges = floor( ( mesh.face2hedges{f}+1 )/2 );
        edges_orientation = mod(mesh.face2hedges{f}, 2);
        edges = mesh.edge2vertices(tedges,:)';

        face = faces_orientation(fLocal) * (edges_orientation .* edges(1,:) + (1-edges_orientation).*edges(2,:)) + ...
               (1-faces_orientation(fLocal)) * ((1-edges_orientation(end:-1:1)) .* edges(1,end:-1:1) + edges_orientation(end:-1:1).*edges(2,end:-1:1));

       % Compute integrals over face
       [face_bnd_nodes, ~, ~, ~, faces_edges_normals2D, ~, face_area, face_centroid2D] = ...
           faces_features(mesh.coordinates, {face}, k, eoa, compressquad, use_mp);

       Nver_f = length(face);

       dOrth      = faces_edges_normals2D{1}(1:end-1,:) + faces_edges_normals2D{1}(2:end,:);
       faceCenter = sum(face_bnd_nodes{1},1) / Nver_f;

       faceIntegrals(face, interfaceFaceIdx) = faceIntegrals(face, interfaceFaceIdx) + ...
           0.5 * ( dOrth * (face_centroid2D - faceCenter)' ) + face_area/Nver_f;
    end
 
end

end
