function [ faceAverageDofs, faceAverageDofsLocal, faceInternalDualDofs, Qface, Tface, ...
    	   globalPrimalDofsByFace, globalPrimalDofs, globalPrimalDofsEnd, ...
    	   globalLambdaDofsByFace, globalLambdaDofs, globalLambdaDofsEnd, ...
    	   localLambdaDofs, localLambdaDofsEnd, ...
           constraintsSign, constraintsSignScaled ] = ...
                subdomainOperators4Faces( ...
                   mesh, k, ...
        		   Nx, Ny, Nz, is, js, ks, ...
                   sortedInterfaceFaces, ...
                   isFacePrimal, faceIntegrals, ...
                   faceAverageDofs, faceAverageDofsLocal, Qface, Tface, ...
        		   globalPrimalDofsByFace, globalPrimalDofs, globalPrimalDofsEnd, ...
        		   globalLambdaDofsByFace, globalLambdaDofs, globalLambdaDofsEnd, ...
        		   localLambdaDofs, localLambdaDofsEnd, ...
                   constraintsSign, rho, constraintsSignScaled, K )

% Map used for both the case in which faces are primal and in which they are not
patch = [-1  0  0;
          1  0  0;
    	  0 -1  0;
    	  0  1  0;
    	  0  0 -1;
    	  0  0  1];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Linearized index of current subdomain
linearSubdomainInd = is + Nx*(js-1) + Nx*Ny*(ks-1);

% Safety sorting
sortedInterfaceFaces = sort(sortedInterfaceFaces);

numInterfaceFaces = length(sortedInterfaceFaces);
numDualDofsByFace = zeros(numInterfaceFaces, 1);
for i = 1:numInterfaceFaces
    numDualDofsByFace(i) = length(mesh.faceInternalDofs{sortedInterfaceFaces(i)}) - isFacePrimal;
end

Qface{is, js, ks} = cell(numInterfaceFaces, 1);
Tface{is, js, ks} = cell(numInterfaceFaces, 1);

if isFacePrimal
    faceAverageDofsLocal{is, js, ks} = zeros(numInterfaceFaces, 1);
    faceAverageDofs{is, js, ks}      = zeros(numInterfaceFaces, 1);
    faceInternalDualDofs             = cell(numInterfaceFaces, 1);
    
    for i = 1:numInterfaceFaces
        f = sortedInterfaceFaces(i);
        
        if mod(f,2) == 0
            [ Qface{is,js,ks}{i}, Tface{is,js,ks}{i}, faceAverageDofsLocal{is,js,ks}(i) ] = ...
                constructFaceTransform( mesh, k, f, faceIntegrals, K );

            globalPrimalDofsByFace( is, js, ks, f/2 ) = ...
                globalPrimalDofsEnd( linearSubdomainInd ) + 1;

            globalPrimalDofs = [ globalPrimalDofs;
                                 globalPrimalDofsByFace( is, js, ks, f/2 ) ];

            globalPrimalDofsEnd( linearSubdomainInd ) = ...
                globalPrimalDofsByFace( is, js, ks, f/2 );
        else
            % Faces 1, 3, 5, which do not introduce new lambda dofs.
            % Remark: (f+1)/2 maps the current face f into the correct primal dof of
            % the neighboring subdomain.
            [~, ~, interfaceFacesParent] = ...
                getInterfaceVEF( Nx, Ny, Nz, is + patch(f,1), ...
                	                         js + patch(f,2), ...
                                             ks + patch(f,3) );
            test = interfaceFacesParent == (f+1);

            Qface{is,js,ks}{i} = ...
                Qface{ is + patch(f,1), js + patch(f,2), ks + patch(f,3) }{ test };
            Tface{is,js,ks}{i} = ...
                Tface{ is + patch(f,1), js + patch(f,2), ks + patch(f,3) }{ test };
            faceAverageDofsLocal{is,js,ks}(i) = ...
                faceAverageDofsLocal{ is + patch(f,1), ...
                                      js + patch(f,2), ...
                                      ks + patch(f,3) }( test );
                                  
            globalPrimalDofs = [ globalPrimalDofs;
                                 globalPrimalDofsByFace( ...
                                    is + patch(f, 1), ...
                                    js + patch(f, 2), ...
                                    ks + patch(f, 3), ...
                                    (f+1)/2 ) ];
        end

        faceAverageDofs{is,js,ks}(i) = mesh.faceInternalDofs{f}( faceAverageDofsLocal{is,js,ks}(i) );
        faceInternalDualDofs{i}      = mesh.faceInternalDofs{f}( [ 1:faceAverageDofsLocal{is,js,ks}(i)-1 ...
                                                                   faceAverageDofsLocal{is,js,ks}(i)+1:numDualDofsByFace(i)+1 ] );

        if isempty(faceInternalDualDofs{i})
            % This can happen if an interface face has a single internal node.
            % Therefore, there will be a single primal dof and no dual dof
            % on such face.
            faceInternalDualDofs{i} = [];
        end
    end
    
    faceInternalDualDofs = vertcat( faceInternalDualDofs{:} );
else
    faceAverageDofs{is,js,ks}      = [];
    faceAverageDofsLocal{is,js,ks} = [];
    faceInternalDualDofs           = vertcat( mesh.faceInternalDofs{ sortedInterfaceFaces } );
end

for i = 1:numInterfaceFaces
    f = sortedInterfaceFaces(i);

    if mod(f,2)
        globalLambdaDofsByFace{ is, js, ks, f } = ...
            globalLambdaDofsByFace{ is + patch(f, 1), js + patch(f, 2), ks + patch(f, 3), f+1 };
    else
        globalLambdaDofsByFace{ is, js, ks, f } = ...
            globalLambdaDofsEnd( linearSubdomainInd ) + (1:numDualDofsByFace(i));
        globalLambdaDofsEnd( linearSubdomainInd ) = ...
            globalLambdaDofsEnd( linearSubdomainInd ) + numDualDofsByFace(i);
    end

    globalLambdaDofs = [ globalLambdaDofs;
        		         globalLambdaDofsByFace{ is, js, ks, f }(:) ];

    rhoSum = rho( is, js, ks ) + rho( is+patch(f,1), js+patch(f,2), ks+patch(f,3) );
    pseudoInverse = rho( is+patch(f,1), js+patch(f,2), ks+patch(f,3) ) / rhoSum;

    constraintsSign = [ constraintsSign;
                        (1-2*mod(f,2))*ones(numDualDofsByFace(i),1) ];

    constraintsSignScaled = [ constraintsSignScaled;
                              (1-2*mod(f,2))*ones(numDualDofsByFace(i),1)*pseudoInverse ];

end

localLambdaDofs = [ localLambdaDofs;
                    (localLambdaDofsEnd+1:localLambdaDofsEnd+sum(numDualDofsByFace))' ];

localLambdaDofsEnd = localLambdaDofsEnd + sum(numDualDofsByFace);

end
