function [ meshArray ] = findDirichletDofs( meshArray, Nx, Ny, Nz )
%
% Set Dirichlet degrees of freedom
%
% We are assuming that meshArray has been created with repMesh() function,
% therefore we have to take care of the possible reflections.

for ks = 1:Nz
    for js = 1:Ny
        for is = 1:Nx
            [ interfaceVertices, interfaceEdges, interfaceFaces] = ...
                        getInterfaceVEF( Nx, Ny, Nz, is, js, ks );
            vDiri = setdiff(1:8, interfaceVertices);
            eDiri = setdiff(1:12, interfaceEdges);
            fDiri = setdiff(1:6, interfaceFaces);
            meshArray{is, js, ks}.diridofs = [ meshArray{is, js, ks}.vertexDofs(vDiri);
                                               vertcat( meshArray{is, js, ks}.edgeInternalDofs{eDiri} );
                                               vertcat( meshArray{is, js, ks}.faceInternalDofs{fDiri} ) ];
        end
    end
end

end
