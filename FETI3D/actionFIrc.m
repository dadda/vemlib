function y = actionFIrc( x, ...
                         meshArray, k, use_mp, ...
                         Nx, Ny, Nz, ...
                         isEdgePrimal, isFacePrimal, ...
                         dimLambda, ...
                         approach4constraints, ...
                         internalDofs, ...
                         vertexPrimalDofs, vertexDualDofs, ...
                         edgeAverageDofs, edgeAverageDofsLocal, edgeInternalDualDofs, ...
                         faceAverageDofs, faceAverageDofsLocal, faceInternalDualDofs, ...
                         Qedge, Tedge, ...
                         Qface, Tface, ...
                         Bprimal, Bdual, K, runAsFor )

y = zeros(dimLambda, 1);

numSubdomains = Nx*Ny*Nz;

numWorkers = getParforArg(runAsFor);

parfor ( s = 1:numSubdomains, numWorkers )
    primalDofs = [ vertexPrimalDofs{s};
                   edgeAverageDofs{s};
                   faceAverageDofs{s} ];

    dualDofs = [ vertexDualDofs{s};
                 edgeInternalDualDofs{s};
                 faceInternalDualDofs{s} ];

    numInternalDofs = length(internalDofs);
    
    if approach4constraints == 1
        
        tmp = K{s}( [ internalDofs; dualDofs ], [ internalDofs; dualDofs ] ) \ ...
            ( K{s}( [ internalDofs; dualDofs ], primalDofs ) * (Bprimal{s} * x) );
        
        y = y + Bdual{s} * tmp(numInternalDofs+1:end);
    else
%         dim = size(K{s},1);
%         dimPrimalLocal = length(primalDofs);
%         
%         Krc = zeros(dim, dimPrimalLocal);
%         
%         numPrimalVertices = length(vertexPrimalDofs{s});
%         
%         averageDofsLocal = [ edgeAverageDofsLocal{s}; faceAverageDofsLocal{s} ];
%         numTransformations = length(averageDofsLocal);
%         
%         ks = floor( (s-1) / (Nx*Ny) ) + 1;
%         js = floor( (s - Nx*Ny*(ks-1) - 1) / Nx ) + 1;
%         is = s - Nx*Ny*(ks-1) - Nx*(js-1);
%         
%         [~, interfaceEdges, interfaceFaces] = ...
%             getInterfaceVEF( Nx, Ny, Nz, is, js, ks );
% 
%         interfaceDofs = [ vertexDualDofs{s};
%                           vertcat( meshArray{s}.edgeInternalDofs{interfaceEdges} );
%                           vertcat( meshArray{s}.faceInternalDofs{interfaceFaces} ) ];
% 
%         transformedDofs = {};
% 
%         if isEdgePrimal
%             transformedDofs = vertcat( transformedDofs, meshArray{s}.edgeInternalDofs(interfaceEdges) );
%         end
%         if isFacePrimal
%             transformedDofs = vertcat( transformedDofs, meshArray{s}.faceInternalDofs(interfaceFaces) );
%         end
%     
%         T = vertcat( Tedge{s}(:), Tface{s}(:) );
% 
%         % Primal vertices do not need to be transformed
%         Krc( [ internalDofs; interfaceDofs ], 1:numPrimalVertices ) = ...
%             K{s}( [ internalDofs; interfaceDofs ], vertexPrimalDofs{s} );
%         
%         for j = 1:numTransformations
%             Krc( [ internalDofs; interfaceDofs ], numPrimalVertices+j ) = ...
%                 K{s}( [ internalDofs; interfaceDofs ], transformedDofs{j} ) ...
%                     * T{j}(:, averageDofsLocal(j));
%         end
%         
%         boundaryDofs = ...
%             [ meshArray{s}.vertexDofs;
%               vertcat( meshArray{s}.edgeInternalDofs{:} );
%               vertcat( meshArray{s}.faceInternalDofs{:} ) ];
% 
%         tmp = KBBsolver( meshArray{s}, k, use_mp, ...
%                          interfaceEdges, interfaceFaces, ...
%                          isEdgePrimal, isFacePrimal, ...
%                          K{s}, Krc * (Bprimal{s} * x), ...
%                          internalDofs, boundaryDofs, ...
%                          vertexPrimalDofs{s}, ...
%                          Qedge{s}, Qface{s} );
% 
%         y = y + Bdual{s} * tmp(dualDofs);
    end
end

end

