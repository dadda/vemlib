function [ Q, T, primalDof ] = ...
    constructEdgeTransform( mesh, k, interfaceEdgeIdx, ...
                            isEdgePrimal, isFacePrimal, ...
                            faceIntegrals )

refEdge2refVertices = [1 3;
                       2 4;
                       1 2;
                       3 4;
                       1 5;
                       2 6;
                       3 7;
                       4 8;
                       5 7;
                       6 8;
                       5 6;
                       7 8];

refEdge2refFaces = [ 1 5;
                     2 5;
                     3 5;
                     4 5;
                     1 3;
                     2 3;
                     1 4;
                     2 4;
                     1 6;
                     2 6;
                     3 6;
                     4 6 ];

if k == 1
    primalFlag = isEdgePrimal + 2*isFacePrimal;

    switch primalFlag

        case 1

            %
            % Only subdomain edges are primal
            %

            numDualDofs = length( mesh.edgeInternalDofs{ interfaceEdgeIdx } ) - 1;

            edgeDofs = [ mesh.vertexDofs( refEdge2refVertices(interfaceEdgeIdx,1) );
                         mesh.edgeInternalDofs{ interfaceEdgeIdx };
                         mesh.vertexDofs( refEdge2refVertices(interfaceEdgeIdx,2) ) ];

            L = sqrt( (mesh.coordinates(edgeDofs(2:end),1) - mesh.coordinates(edgeDofs(1:end-1),1)).^2 ...
                    + (mesh.coordinates(edgeDofs(2:end),2) - mesh.coordinates(edgeDofs(1:end-1),2)).^2 ...
                    + (mesh.coordinates(edgeDofs(2:end),3) - mesh.coordinates(edgeDofs(1:end-1),3)).^2 );
            L = L(1:end-1) + L(2:end);

            T = [ mesh.edgeInternalDofs{interfaceEdgeIdx}(1:end-1) ...
                  mesh.edgeInternalDofs{interfaceEdgeIdx}(1:end-1) ...
                  ones(numDualDofs,1);
                  mesh.edgeInternalDofs{interfaceEdgeIdx}(2:end) ...
                  mesh.edgeInternalDofs{interfaceEdgeIdx}(1:end-1) ...
                  -L(1:end-1)./L(2:end);
                  mesh.edgeInternalDofs{interfaceEdgeIdx} ...
                  mesh.edgeInternalDofs{interfaceEdgeIdx}(end)*ones(numDualDofs+1,1) ...
                  ones(numDualDofs+1,1) ];

            primalDof = numDualDofs + 1;

        case 2

            %
            % Only subdomain faces are primal
            %

            numDualDofs = length( mesh.edgeInternalDofs{ interfaceEdgeIdx } );

            T = cell(3*numDualDofs, 1);

            for i = 1:numDualDofs
                me = mesh.edgeInternalDofs{interfaceEdgeIdx}(i);

                T{3*i-2} = [ me me 1 ];

                for j = 1:2
                    interfaceFaceIdx = refEdge2refFaces(interfaceEdgeIdx,j);
                    connections      = mesh.faceInternalDofs{interfaceFaceIdx};
                    Nconn            = length(connections);
                    T{3*i-2+j}       = [ connections ...
                                         me*ones(Nconn,1) ...
                                         -1/Nconn*faceIntegrals( me, interfaceFaceIdx )./faceIntegrals( connections, interfaceFaceIdx ) ];
                end
            end

            T = vertcat( T{:} );

            primalDof = []; % There is no primal dof coming from edges

        case 3

            %
            % Both subdomain edges and faces are primal
            %

            numDualDofs = length( mesh.edgeInternalDofs{ interfaceEdgeIdx } ) - 1;

            T = cell(4*numDualDofs, 1);

            edgeDofs = [ mesh.vertexDofs( refEdge2refVertices(interfaceEdgeIdx,1) );
                         mesh.edgeInternalDofs{ interfaceEdgeIdx };
                         mesh.vertexDofs( refEdge2refVertices(interfaceEdgeIdx,2) ) ];

            L = sqrt( (mesh.coordinates(edgeDofs(2:end),1) - mesh.coordinates(edgeDofs(1:end-1),1)).^2 ...
                    + (mesh.coordinates(edgeDofs(2:end),2) - mesh.coordinates(edgeDofs(1:end-1),2)).^2 ...
                    + (mesh.coordinates(edgeDofs(2:end),3) - mesh.coordinates(edgeDofs(1:end-1),3)).^2 );
            L = L(1:end-1) + L(2:end);

            for i = 1:numDualDofs
                me       = mesh.edgeInternalDofs{interfaceEdgeIdx}(i);
                T{4*i-3} = [ me me 1 ];

                % Impose zero edge average
                eNeighbor = mesh.edgeInternalDofs{interfaceEdgeIdx}(i+1);
                intRatio  = -L(i)/L(i+1);
                T{4*i-2}  = [ eNeighbor me intRatio ];

                for j = 1:2
                    interfaceFaceIdx = refEdge2refFaces(interfaceEdgeIdx,j);
                    connections      = mesh.faceInternalDofs{interfaceFaceIdx};
                    Nconn            = length(connections);
                    T{4*i-2+j}       = [ connections ...
                                         me*ones(Nconn,1) ...
                                         -1/Nconn*(faceIntegrals( me, interfaceFaceIdx )+intRatio*faceIntegrals(eNeighbor,interfaceFaceIdx))./faceIntegrals( connections, interfaceFaceIdx ) ];
                end
            end

            % Add primal constraint
            T = vertcat( T{:}, ...
                         [ mesh.edgeInternalDofs{interfaceEdgeIdx} ...
                           mesh.edgeInternalDofs{interfaceEdgeIdx}(end)*ones(numDualDofs+1,1) ...
                           ones(numDualDofs+1,1) ] );

            primalDof = numDualDofs + 1;
    end

    Q = [];

else
    error('FETI-DP is not supported for k > 1, yet')
end

end
