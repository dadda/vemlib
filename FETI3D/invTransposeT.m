function TinvTransp = invTransposeT( T, primalDof )
% Compute the inverse transpose of a transformation matrix defined as in Li
% and Widlund (2004)

numRows  = size(T, 1);

A = T(primalDof, :);
detT = 2 - sum(A);

if (numRows <= 2)
    TinvTransp = eye(numRows) - triu(T, 1) - tril(T, -1);
else
    TinvTransp = repmat(A, numRows, 1);
    TinvTransp( primalDof, : ) = -TinvTransp( primalDof, : );
    TinvTransp( numRows*(0:numRows-1) + (1:numRows) ) = detT + A;
    TinvTransp( :, primalDof ) = -1;
    TinvTransp( primalDof, primalDof ) = 1.;
end

TinvTransp = permute(TinvTransp/detT, [2 1]);

end

