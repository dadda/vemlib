function [ lambda, relRes, iter, relResVec, eigEst ] = ...
    pcgFETIDP( d, tol, maxit, use_pc, ...
               meshArray, k, use_mp, ...
               Nx, Ny, Nz, ...
               isVertexPrimal, isEdgePrimal, isFacePrimal, ...
               dimPrimal, dimLambda, ...
               approach4constraints, ...
               internalDofs, ...
               vertexPrimalDofs, vertexDualDofs, ...
               edgeAverageDofs, edgeAverageDofsLocal, edgeInternalDualDofs, ...
               faceAverageDofs, faceAverageDofsLocal, faceInternalDualDofs, ...
               Qedge, Tedge, ...
               Qface, Tface, ...
               Bprimal, Bdual, BdualScaled, K, Kstar, fid, runAsFor, ...
               savePCGiter, mshName, iR, i_rho, eoa, compressquad )
% Solve the dual interface problem by the preconditioned conjugate gradient
% algorithm.

iter = 0;

try
    filename = sprintf( ['../FETI3D/workspaces/%s__k_%i__Nx_%i_Ny_%i_Nz_%i__' ...
                         'rho_idx_%i__rho_type_%i__eoa_%i__compressquad_%i__use_mp_%i__' ...
                         'VEF_%i%i%i__approach_%i__maxit_%i__use_pc_%i_lastIt.mat'], ...
                         mshName, k, Nx, Ny, Nz, iR, i_rho, eoa, compressquad, use_mp, ...
                         isVertexPrimal, isEdgePrimal, isFacePrimal, ...
                         approach4constraints, maxit, use_pc );
    load(filename);

    if ~isempty(fid)
        fprintf(fid, 'Resuming PCG from iteration %i\n', iter);
    else
        fprintf('Resuming PCG from iteration %i\n', iter);
    end
catch
    dim    = length(d);
    lambda = zeros(dim, 1);
    r      = d;
    
    relRes    = 1;
    relResVec = ones(maxit, 1);

    alpha = 1;
    T = zeros(maxit);
end

% Initial residual norm
rNorm0 = norm(d, 2);
if rNorm0 < 1e-12
    if ~isempty(fid)
        % fid provided
        fprintf(fid, ['WARNING: the initial guess lambda = 0 is close to the ' ...
                      'exact solution. Tolerance changed to 0.01\n']);
    else
        warning( ['The initial guess lambda = 0 is close to the exact solution.' ...
                  ' Tolerance changed to 0.01']);
    end
    tol = 0.01;
end

while ( (relRes > tol) && (iter < maxit) )
    iter = iter + 1;
    
    if i_rho ~= 1
        if ~isempty(fid)
            fprintf(fid, 'Starting PCG iteration %i ... ', iter);
        else
            fprintf('Starting PCG iteration %i ... ', iter);
        end
    end
    
    if use_pc
        z = solveDirichletPreconditioner( r, Nx, Ny, Nz, dimLambda, internalDofs, ...
                                          vertexDualDofs, edgeInternalDualDofs, faceInternalDualDofs, ...
                                          BdualScaled, K, runAsFor );
    else
        z = r;
    end
    
    gamma = z' * r;
    
    if iter == 1
        p = z;
    else
        beta = gamma / gammaOld;
        p    = z + beta*p;
    end
    
    FIp = actionFI( p, meshArray, k, use_mp, Nx, Ny, Nz, isEdgePrimal, isFacePrimal, ...
                    dimPrimal, dimLambda, approach4constraints, internalDofs, ...
                    vertexPrimalDofs, vertexDualDofs, ...
                    edgeAverageDofs, edgeAverageDofsLocal, edgeInternalDualDofs, ...
                    faceAverageDofs, faceAverageDofsLocal, faceInternalDualDofs, ...
                    Qedge, Tedge, Qface, Tface, Bprimal, Bdual, K, Kstar, runAsFor );

    alphaOld = alpha;
    alpha    = gamma / (p' * FIp);
    lambda   = lambda + alpha*p;
    r        = r - alpha*FIp;
        
    % Compute approximate condition number of M^-1 * F_I
    % from the tridiagonal Lanczos matrix generated during the PCG
    % iteration as the ratio between the maximum and the minimum
    % eigenvalues
    if iter > 1
        T(iter-1:iter, iter-1:iter) = T(iter-1:iter, iter-1:iter) + ...
            [1 sqrt(beta); sqrt(beta) beta] / alphaOld;
    end
    
    gammaOld = gamma;
    relRes   = norm(r, 2) / rNorm0;
    relResVec(iter) = relRes;

    if abs( alpha ) < 1e-15
        if ~isempty(fid)
        % fid provided
            fprintf(fid, ['WARNING: pcgFETIDP stagnated (two consecutive iterates were the same). ' ...
                          'Stopping pcgFETIDP ...\n']);
        else
            warning('pcgFETIDP stagnated (two consecutive iterates were the same). Stopping pcgFETIDP ...\n');
        end
        break
    end

    if alpha < 0
        if ~isempty(fid)
            fprintf(fid, ['WARNING: the coefficient matrix of the dual interface system is negative. ' ...
                          'Stopping pcgFETIDP ...\n']);
        else
            warning('The coefficient matrix of the dual interface system is negative. Stopping pcgFETIDP ...\n');
        end
        break
    end

    if savePCGiter ~= 0
        filename = sprintf( ['../FETI3D/workspaces/%s__k_%i__Nx_%i_Ny_%i_Nz_%i__' ...
                             'rho_idx_%i__rho_type_%i__eoa_%i__compressquad_%i__use_mp_%i__' ...
                             'VEF_%i%i%i__approach_%i__maxit_%i__use_pc_%i_lastIt.mat'], ...
                             mshName, k, Nx, Ny, Nz, iR, i_rho, eoa, compressquad, use_mp, ...
                             isVertexPrimal, isEdgePrimal, isFacePrimal, ...
                             approach4constraints, maxit, use_pc );
        save(filename, 'iter', 'lambda', 'r', 'relRes', 'relResVec', ...
                       'alpha', 'T', 'gammaOld', 'p', '-v7.3');
    end
    
    if i_rho ~= 1
        if ~isempty(fid)
            fprintf(fid, 'done\n');
        else
            fprintf('done\n');
        end
    end
end

relResVec = relResVec(1:iter);

if iter > 3
    T = T(2:iter-2,2:iter-2);
    l = eig(T);
    eigEst = [min(l), max(l)];
else
    eigEst = [1 1];
    if ~isempty(fid)
        fprintf(fid, 'WARNING: eigenvalue estimate failed because iterates converged too fast.\n');
    else
        warning('Eigenvalue estimate failed: iterates converged too fast.');
    end
end

if relRes <= tol
    if ~isempty(fid)
        fprintf(fid, 'PCG converged to the desired tolerance %g within %i iterations.\n', tol, iter);
    else
        fprintf('PCG converged to the desired tolerance %g within %i iterations.\n', tol, iter);
    end
else
    if ~isempty(fid)
        fprintf(fid, 'PCG iterated %i times but did not converge.\n', iter);
    else
        fprintf('PCG iterated %i times but did not converge.\n', iter);
    end
end

end
