function [ edgeAverageDofs, edgeAverageDofsLocal, edgeInternalDualDofs, Qedge, Tedge, ...
    	   globalPrimalDofsByEdge, globalPrimalDofs, globalPrimalDofsEnd, ...
           globalLambdaDofsByEdge, globalLambdaDofs, globalLambdaDofsEnd, ...
           localLambdaDofs, localLambdaDofsEnd, ...
           constraintsSign, constraintsSignScaled ] = ...
                subdomainOperators4Edges( ...
                    mesh, k, ...
                    Nx, Ny, Nz, is, js, ks, ...
                    sortedInterfaceEdges, ...
                    isVertexPrimal, isEdgePrimal, isFacePrimal, ...
                    faceIntegrals, ...
                    edgeAverageDofs, edgeAverageDofsLocal, Qedge, Tedge, ...
                    globalPrimalDofsByEdge, globalPrimalDofs, globalPrimalDofsEnd, ...
                    globalLambdaDofsByEdge, globalLambdaDofs, globalLambdaDofsEnd, ...
                    localLambdaDofs, localLambdaDofsEnd, ...
                    constraintsSign, rho, constraintsSignScaled )
	 
%
% Build maps from an interface edge to its neighbors
%

% Map used whether interface edges are primal. For edges we extend the map, such that, for
% each i = 1, ..., 12, primalPatch(i,4) denotes the element of
% globalPrimalDofsByEdge(is, js, js, :) we have to select.
primalPatch = [
	-1  0 -1  2;
	 0  0 -1  2;
	 0 -1 -1  3;
	 0  0 -1  3;
	-1 -1  0  1;
	 0 -1  0  1;
	-1  0  0  1;
	 nan nan nan nan;    % reference edge 8 introduces a new primal dof
	-1  0  0  2;
	 nan nan nan nan;    % reference edge 10 introduces a new primal dof
     0 -1  0  3;
	 nan nan nan nan ];   % reference edge 12 introduces a new primal dof

% Map used for dual dofs on interface edges
dualPatch = zeros(3, 4, 12);

dualPatch(:,:,1) = [-1  0 -1 10;
                     0  0 -1  9;
                    -1  0  0  2];

dualPatch(:,:,2) = [ 0  0 -1 10;
        		     1  0 -1  9;
                     1  0  0 nan];

dualPatch(:,:,3) = [ 0 -1 -1 12;
                     0  0 -1 11;
                     0 -1  0  4];

dualPatch(:,:,4) = [ 0  0 -1 12;
                     0  1 -1 11;
                     0  1  0 nan];

dualPatch(:,:,5) = [-1 -1  0  8;
                     0 -1  0  7;
                    -1  0  0  6];

dualPatch(:,:,6) = [ 0 -1  0  8;
                     1 -1  0  7;
                     1  0  0  nan];

dualPatch(:,:,7) = [-1  0  0  8;
                    -1  1  0  nan;
                     0  1  0  nan];

dualPatch(:,:,8) = [ 1  0  0  nan;
                     0  1  0  nan;
                     1  1  0  nan];

dualPatch(:,:,9) = [-1  0  0 10;
                    -1  0  1  nan;
                     0  0  1  nan];

dualPatch(:,:,10) = [ 1  0  0  nan;
                      0  0  1  nan;
                      1  0  1  nan];

dualPatch(:,:,11) = [ 0 -1  0 12;
                      0 -1  1  nan;
                      0  0  1  nan];

dualPatch(:,:,12) = [ 0  1  0  nan;
                      0  0  1  nan;
                      0  1  1  nan];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Linearized index of current subdomain
linearSubdomainInd = is + Nx*(js-1) + Nx*Ny*(ks-1);

% Safety sorting
sortedInterfaceEdges = sort(sortedInterfaceEdges);

numInterfaceEdges = length(sortedInterfaceEdges);
numDualDofsByEdge = zeros(numInterfaceEdges, 1);
for i = 1:numInterfaceEdges
    numDualDofsByEdge(i) = length(mesh.edgeInternalDofs{sortedInterfaceEdges(i)}) - isEdgePrimal;
end

Qedge{is, js, ks} = cell(numInterfaceEdges, 1);
Tedge{is, js, ks} = cell(numInterfaceEdges, 1);

if isEdgePrimal
    edgeAverageDofsLocal{is, js, ks} = zeros(numInterfaceEdges, 1);
    edgeAverageDofs{is, js, ks}      = zeros(numInterfaceEdges, 1);
    edgeInternalDualDofs             = cell(numInterfaceEdges, 1);

    for i = 1:numInterfaceEdges
        e = sortedInterfaceEdges(i);
        if ((e == 8) || (e == 10) || (e == 12))
            % Remark: (e-6)/2, for e = 8,10,12, gives 1,2,3, i.e. the correct position
            % in the last dimension of globalPrimalDofsByEdge.
            
            globalPrimalDofsByEdge( is, js, ks, (e-6)/2 ) = ...
               globalPrimalDofsEnd( linearSubdomainInd ) + 1;

            globalPrimalDofs = [ globalPrimalDofs;
                                 globalPrimalDofsByEdge( is, js, ks, (e-6)/2 ) ];

            globalPrimalDofsEnd( linearSubdomainInd ) = ...
               globalPrimalDofsByEdge( is, js, ks, (e-6)/2 );
        else
            % e = 1, ..., 7, 9, 11
            
            [~, interfaceEdgesParent] = ...
                getInterfaceVEF( Nx, Ny, Nz, is + primalPatch(e,1), ...
                                             js + primalPatch(e,2), ...
                                             ks + primalPatch(e,3) );
            test = interfaceEdgesParent == (2*primalPatch(e,4)+6);

            edgeAverageDofsLocal{is,js,ks}(i) = ...
                edgeAverageDofsLocal{ is + primalPatch(e,1), ...
                                      js + primalPatch(e,2), ...
                                      ks + primalPatch(e,3) }( test );
                                      
            globalPrimalDofs = [ globalPrimalDofs;
                                 globalPrimalDofsByEdge( ...
                                    sub2ind( [Nx, Ny, Nz, 3], ...
                                        is + primalPatch(e,1), ...
                                        js + primalPatch(e,2), ...
                                        ks + primalPatch(e,3), ...
                                        primalPatch(e,4) ) ) ];
        end

        edgeAverageDofs{is, js, ks}(i) = mesh.edgeInternalDofs{e}(end);
        edgeInternalDualDofs{i}        = mesh.edgeInternalDofs{e}(1:end-1);

        if isempty(edgeInternalDualDofs{i})
            % This can happen if an interface edge has a single internal node.
            % Therefore, there will be a single primal dof and no dual dof
            % on such edge.
            edgeInternalDualDofs{i} = [];
        end
    end

    edgeInternalDualDofs = vertcat( edgeInternalDualDofs{:} );
else
    edgeAverageDofs{is, js, ks}      = [];
    edgeAverageDofsLocal{is, js, ks} = [];
    edgeInternalDualDofs             = vertcat( mesh.edgeInternalDofs{ sortedInterfaceEdges } );
end

if isVertexPrimal
    % globalLambdaDofsEnd requires initialization
    globalLambdaDofsEnd( linearSubdomainInd ) = globalLambdaDofsEnd( max( linearSubdomainInd - 1, 1 ) );
end

numConstraintEq = 3;

for i = 1:numInterfaceEdges
    e = sortedInterfaceEdges(i);

    if isEdgePrimal || isFacePrimal
        if ((e == 8) || (e == 10) || (e == 12))
            % Remark: (e-6)/2, for e = 8,10,12, gives 1,2,3, i.e. the correct position
            % in the last dimension of globalPrimalDofsByEdge.

            [ Qedge{is,js,ks}{i}, Tedge{is,js,ks}{i} ] = ...
                constructEdgeTransform( mesh, k, e, ...
                                        isEdgePrimal, isFacePrimal, ...
                                        faceIntegrals );
        else
            % e = 1, ..., 7, 9, 11

            [~, interfaceEdgesParent] = ...
                getInterfaceVEF( Nx, Ny, Nz, is + primalPatch(e,1), ...
                                             js + primalPatch(e,2), ...
                                             ks + primalPatch(e,3) );
            test = interfaceEdgesParent == (2*primalPatch(e,4)+6);

            Qedge{is,js,ks}{i} = ...
                Qedge{ is + primalPatch(e,1), js + primalPatch(e,2), ks + primalPatch(e,3) }{ test };

            Tedge{is,js,ks}{i} = ...
                Tedge{ is + primalPatch(e,1), js + primalPatch(e,2), ks + primalPatch(e,3) }{ test };
        end
    end

    globalLambdaDofsByEdge{ is, js, ks, e } = zeros(numConstraintEq, numDualDofsByEdge(i));

    % numOldDofs corresponds to the number of preceding subdomains
    numOldDofs = mod(e,2) + 2*(e <= 6);

    for j = 1:numOldDofs
        globalLambdaDofsByEdge{ is, js, ks, e }(j, :) = ...
	    globalLambdaDofsByEdge{ is + dualPatch(j, 1, e), ...
				    js + dualPatch(j, 2, e), ...
				    ks + dualPatch(j, 3, e), ...
				    dualPatch(j, 4, e) }(numOldDofs,:);
    end

    globalLambdaDofsByEdge{ is, js, ks, e }(numOldDofs+1:numConstraintEq, :) = ...
        reshape( globalLambdaDofsEnd( linearSubdomainInd ) + ...
		 (1:(numConstraintEq-numOldDofs)*numDualDofsByEdge(i)), ...
		 numConstraintEq-numOldDofs, numDualDofsByEdge(i) );

    globalLambdaDofs = [ globalLambdaDofs;
		                 globalLambdaDofsByEdge{ is, js, ks, e }(:) ];

    globalLambdaDofsEnd( linearSubdomainInd ) = ...
        globalLambdaDofsEnd( linearSubdomainInd ) + ...
        (numConstraintEq-numOldDofs)*numDualDofsByEdge(i);

    rhoSum = sum( rho( sub2ind( [Nx, Ny, Nz], ...
                                [is; is + dualPatch(:, 1, e)], ...
                                [js; js + dualPatch(:, 2, e)], ...
                                [ks; ks + dualPatch(:, 3, e)] ) ) );

    pseudoInverse = rho( sub2ind( [Nx, Ny, Nz], ...
                                  is + dualPatch(:, 1, e), ...
                                  js + dualPatch(:, 2, e), ...
                                  ks + dualPatch(:, 3, e) ) ) / rhoSum;

    newConstraints = ones(numConstraintEq, numDualDofsByEdge(i));
    newConstraints(1:numOldDofs,:) = -1.;
    newConstraintsScaled = bsxfun(@times, pseudoInverse, newConstraints);

    constraintsSign       = [ constraintsSign; newConstraints(:) ];
    constraintsSignScaled = [ constraintsSignScaled; newConstraintsScaled(:) ];

end

localLambdaDofs  = [ localLambdaDofs;
                     kron( (localLambdaDofsEnd+1:localLambdaDofsEnd+sum(numDualDofsByEdge))', ...
                           ones(numConstraintEq,1) ) ];

localLambdaDofsEnd = localLambdaDofsEnd + sum(numDualDofsByEdge);

end
