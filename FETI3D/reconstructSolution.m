function [ u ] = reconstructSolution( lambda, uPrimal, ...
                                      meshArray, k, use_mp, ...
                                      Nx, Ny, Nz, ...
                                      isEdgePrimal, isFacePrimal, ...
                                      approach4constraints, ...
                                      internalDofs, ...
                                      vertexPrimalDofs, vertexDualDofs, ...
                                      edgeAverageDofs, edgeAverageDofsLocal, edgeInternalDualDofs, ...
                                      faceAverageDofs, faceAverageDofsLocal, faceInternalDualDofs, ...
                                      Qedge, Tedge, ...
                                      Qface, Tface, ...
                                      Bprimal, Bdual, K, F, runAsFor )
% reconstructSolution   Reconstruct solution in each subdomain.
%
% If the explicit change of basis approach has been chosen, u will be
% expressed in the transformed basis.

u = cell(Nx, Ny, Nz);

numSubdomains = Nx*Ny*Nz;

numWorkers = getParforArg(runAsFor);

parfor ( s = 1:numSubdomains, numWorkers )
    primalDofs = [ vertexPrimalDofs{s};
                   edgeAverageDofs{s};
                   faceAverageDofs{s} ];

    dualDofs = [ vertexDualDofs{s};
                 edgeInternalDualDofs{s};
                 faceInternalDualDofs{s} ];

    numInternalDofs = length(internalDofs);
    
    dim  = size(K{s}, 1);
    
    if approach4constraints == 1
        u{s} = zeros(dim, 1);

        % Set u at primal dofs
        u{s}( primalDofs ) = Bprimal{s} * uPrimal;
        
        % Set u at internal and dual dofs
        u{s}( [ internalDofs; dualDofs ] ) = ...
            K{s}( [ internalDofs; dualDofs ], [ internalDofs; dualDofs ] ) \ ...
            ( F{s}( [ internalDofs; dualDofs ] )  ...
              - [ sparse(numInternalDofs, 1); Bdual{s}' * lambda ] ...
              - K{s}( [ internalDofs; dualDofs ], primalDofs ) * u{s}( primalDofs ) );

        % Set u at Dirichlet dofs
        u{s}( meshArray{s}.diridofs ) = F{s}( meshArray{s}.diridofs );

    else
%         averageDofsLocal   = [ edgeAverageDofsLocal{s}; faceAverageDofsLocal{s} ];
%         numTransformations = length(averageDofsLocal);
% 
%         ks = floor( (s-1) / (Nx*Ny) ) + 1;
%         js = floor( (s - Nx*Ny*(ks-1) - 1) / Nx ) + 1;
%         is = s - Nx*Ny*(ks-1) - Nx*(js-1);
% 
%         [~, interfaceEdges, interfaceFaces] = ...
%             getInterfaceVEF( Nx, Ny, Nz, is, js, ks );
% 
%         transformedDofs = {};
% 
%         if isEdgePrimal
%             transformedDofs = vertcat( transformedDofs, meshArray{s}.edgeInternalDofs(interfaceEdges) );
%         end
%         if isFacePrimal
%             transformedDofs = vertcat( transformedDofs, meshArray{s}.faceInternalDofs(interfaceFaces) );
%         end
% 
%         T = vertcat( Tedge{s}(:), Tface{s}(:) );
% 
%         numPrimalVertices = length(vertexPrimalDofs{s});
%         dimPrimalLocal = length(primalDofs);
% 
%         interfaceDofs = [ vertexDualDofs{s};
%                           vertcat( meshArray{s}.edgeInternalDofs{interfaceEdges} );
%                           vertcat( meshArray{s}.faceInternalDofs{interfaceFaces} ) ];
% 
%         Krc = zeros(dim, dimPrimalLocal);
%         
%         % Primal vertices do not need to be transformed
%         Krc( [ internalDofs; interfaceDofs ], 1:numPrimalVertices ) = ...
%             K{s}( [ internalDofs; interfaceDofs ], vertexPrimalDofs{s} );
%         
%         for j = 1:numTransformations
%             Krc( [ internalDofs; interfaceDofs ], numPrimalVertices+j ) = ...
%                 K{s}( [ internalDofs; interfaceDofs ], transformedDofs{j} ) ...
%                     * T{j}(:, averageDofsLocal(j));
%         end
%         
%         boundaryDofs = ...
%             [ meshArray{s}.vertexDofs;
%               vertcat( meshArray{s}.edgeInternalDofs{:} );
%               vertcat( meshArray{s}.faceInternalDofs{:} ) ];
% 
%         tmp = zeros(dim, 1);
%         tmp(dualDofs) = -Bdual{s}' * lambda;
%         tmp = tmp + F{s} - Krc * (Bprimal{s} * uPrimal);
%         
%         tmp = KBBsolver( meshArray{s}, k, use_mp, ...
%                          interfaceEdges, interfaceFaces, ...
%                          isEdgePrimal, isFacePrimal, ...
%                          K{s}, tmp, ...
%                          internalDofs, boundaryDofs, ...
%                          vertexPrimalDofs{s}, ...
%                          Qedge{s}, Qface{s} );
% 
%         % Note: tmp lives in the original space of variables. Let us
%         % express it in the transformed basis, and combine it with uPrimal,
%         % thereby obtaining the final solution in the transformed basis.
%         % Finally, transform this last vector back to the space of original
%         % variables. Observe that Dirichlet boundary conditions are already
%         % imposed by KBBsolver().
%         for j = 1:numTransformations
%             tmp( transformedDofs{j} ) = T{j} \ tmp( transformedDofs{j} );
%         end
% 
%         if norm( tmp( primalDofs ) ) > 1e-12
%             error('The solution returned by KBBsolver is not zero at primal dofs.')
%         end
%         
%         tmp( primalDofs ) = Bprimal{s} * uPrimal;
%         % Transform tmp back to the original basis function space
%         for j = 1:numTransformations
%             tmp( transformedDofs{j} ) = T{j} * tmp( transformedDofs{j} );
%         end
%         
%         u{s} = tmp;
    end
end

end










