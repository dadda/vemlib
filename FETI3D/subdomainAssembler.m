function [ K, F, hmax ] = ...
    subdomainAssembler( meshArray, Nx, Ny, Nz, rho, i_F, ...
                        k, eoa, compressquad, use_mp, runAsFor )

numSubdomains = Nx*Ny*Nz;

K    = cell( Nx, Ny, Nz );
F    = cell( Nx, Ny, Nz );
hmax = zeros(Nx, Ny, Nz);

% Remark: given a 3 dimensional cell like meshArray, the third index, say k, runs
% the slowest, the second one, say 'j', runs faster, and the first one (say 'i') the fastest.
% Thus, the linearized index s follows the same ordering: for a given value
% of j and k, it runs through all the values of i first, and so on.

%
% Process the first subdomain separately to compute elements_diameters,
% elements_centroids, and elements-qformulas3D once for all.
%

getParforArg(runAsFor);

[ K{1}, row_dofs_for_K, col_dofs_for_K, ...
  F{1}, dofs_for_F, ~, ...
  ref_elements_diameters, ref_elements_centroids, ref_elements_qformulas3D ] = ...
    assemble_local_matrices_and_rhs_3d( meshArray{1}, i_F, ...
                                        k, eoa, compressquad, use_mp );

hmax(1) = max(ref_elements_diameters);

K{1} = rho(1) * K{1};

if i_F == 1
    % We assume that the load term provided in loadcal_3d is equal to -\Delta u_exact
    % and that we want the method to compute u_exact (in view of an error analysis,
    % for example). Therefore, since the stiffness matrix has been multiplied by rho(1),
    % the right hand side must be multiplied by rho(1) as well
    F{1} = rho(1) * full( sparse(dofs_for_F, ones([length(dofs_for_F) 1]), F{1}) );
elseif i_F == 2
    % We assume that we want to solve the equation -\nabla \cdot \rho \nabla u = f,
    % being f a random right hand side. In this case, we do not have to scale
    % the right hand side by \rho. Clearly, no error analysis can be performed in this case.
    F{1} = full( sparse(dofs_for_F, ones([length(dofs_for_F) 1]), F{1}) );
end

if ~isempty( meshArray{1}.diridofs )
    d2m2 = (k*(k-1))/2;
    d3m2 = ((k+1)*k*(k-1))/6;

    dim = meshArray{1}.Nver + meshArray{1}.Ned*(k-1) ...
        + meshArray{1}.Nfc*d2m2 + meshArray{1}.Nelt*d3m2;
    dofs = setdiff(1:dim, meshArray{1}.diridofs);

    % REMARK: k = 1 is assumed here
    F{1}(meshArray{1}.diridofs) = ...
        dirdata_3d( meshArray{1}.coordinates(meshArray{1}.diridofs, 1), ...
                    meshArray{1}.coordinates(meshArray{1}.diridofs, 2), ...
                    meshArray{1}.coordinates(meshArray{1}.diridofs, 3), use_mp );

    % Assemble K{s} temporarily to eliminate Dirichlet dofs
    dummy = sparse(row_dofs_for_K, col_dofs_for_K, K{1});
    dummy = dummy(dofs, meshArray{1}.diridofs);

    F{1}(dofs) = F{1}(dofs) - dummy * F{1}(meshArray{1}.diridofs);

    dummy = false(dim, 1);
    dummy(meshArray{1}.diridofs) = true;

    % Find mapping from vector of domain row degrees of freedom to
    % Dirichlet rows
    [~, ~, rjx] = unique(row_dofs_for_K);
    [~, ~, cjx] = unique(col_dofs_for_K);

    % Entries of K to be set = 1
    diri_ii = dummy(rjx) & (row_dofs_for_K == col_dofs_for_K);

    % Entries of K to be zeroed
    diri_ij = (dummy(rjx) | dummy(cjx)) & (~diri_ii);

    % Count the number of occurences of Dirichlet (i,i) entries
    counts = histc(row_dofs_for_K(diri_ii), 1:dim);

    K{1}(diri_ii) = 1.0 ./ counts(rjx(diri_ii));
    K{1}(diri_ij) = 0.0;

    % Final assemblage
    K{1} = sparse( row_dofs_for_K, col_dofs_for_K, K{1} );
else
    K{1} = sparse( row_dofs_for_K, col_dofs_for_K, K{1} );
end

%
% Process the remaining subdomains
%

Hx = 1/Nx;
Hy = 1/Ny;
Hz = 1/Nz;

numWorkers = getParforArg(runAsFor);

parfor ( s = 2:numSubdomains, numWorkers )
    ks = floor( (s-1) / (Nx*Ny) ) + 1;
    js = floor( (s - Nx*Ny*(ks-1) - 1) / Nx ) + 1;
    is = s - Nx*Ny*(ks-1) - Nx*(js-1);

    elements_diameters = ref_elements_diameters;

    elements_centroids = ref_elements_centroids;
    elements_centroids(:,1) = elements_centroids(:,1) * (2*mod(is,2)-1) + Hx * (is-mod(is,2));
    elements_centroids(:,2) = elements_centroids(:,2) * (2*mod(js,2)-1) + Hy * (js-mod(js,2));
    elements_centroids(:,3) = elements_centroids(:,3) * (2*mod(ks,2)-1) + Hz * (ks-mod(ks,2));

    elements_qformulas3D = ref_elements_qformulas3D;
    for E = 1:meshArray{s}.Nelt
        elements_qformulas3D{E}(:,1) = elements_qformulas3D{E}(:,1) * (2*mod(is,2)-1) + Hx * (is-mod(is,2));
        elements_qformulas3D{E}(:,2) = elements_qformulas3D{E}(:,2) * (2*mod(js,2)-1) + Hy * (js-mod(js,2));
        elements_qformulas3D{E}(:,3) = elements_qformulas3D{E}(:,3) * (2*mod(ks,2)-1) + Hz * (ks-mod(ks,2));
    end

    [ K{s}, row_dofs_for_K, col_dofs_for_K, F{s}, dofs_for_F ] = ...
        assemble_local_matrices_and_rhs_3d( meshArray{s}, i_F, ...
                                            k, eoa, compressquad, use_mp, ...
                                            elements_diameters, ...
                                            elements_centroids, ...
                                            elements_qformulas3D );

    hmax(s) = max(elements_diameters);

    K{s} = rho(s) * K{s};

    if i_F == 1
        F{s} = rho(s) * full( sparse(dofs_for_F, ones([length(dofs_for_F) 1]), F{s}) );
    elseif i_F == 2
        F{s} = full( sparse(dofs_for_F, ones([length(dofs_for_F) 1]), F{s}) );
    end

    if ~isempty( meshArray{s}.diridofs )
        d2m2 = (k*(k-1))/2;
        d3m2 = ((k+1)*k*(k-1))/6;

        dim = meshArray{s}.Nver + meshArray{s}.Ned*(k-1) ...
            + meshArray{s}.Nfc*d2m2 + meshArray{s}.Nelt*d3m2;
        dofs = setdiff(1:dim, meshArray{s}.diridofs);
        
        % REMARK: k = 1 is assumed here
        F{s}(meshArray{s}.diridofs) = ...
            dirdata_3d( meshArray{s}.coordinates(meshArray{s}.diridofs, 1), ...
                        meshArray{s}.coordinates(meshArray{s}.diridofs, 2), ...
                        meshArray{s}.coordinates(meshArray{s}.diridofs, 3), use_mp );
                    
        % Assemble K{s} temporarily to eliminate Dirichlet dofs
        dummy = sparse(row_dofs_for_K, col_dofs_for_K, K{s});
        dummy = dummy(dofs, meshArray{s}.diridofs);
        
        F{s}(dofs) = F{s}(dofs) - dummy * F{s}(meshArray{s}.diridofs);
        
        dummy = false(dim, 1);
        dummy(meshArray{s}.diridofs) = true;

        % Find mapping from vector of domain row degrees of freedom to
        % Dirichlet rows
        [~, ~, rjx] = unique(row_dofs_for_K);
        [~, ~, cjx] = unique(col_dofs_for_K);

        % Entries of K to be set = 1
        diri_ii = dummy(rjx) & (row_dofs_for_K == col_dofs_for_K);

        % Entries of K to be zeroed
        diri_ij = (dummy(rjx) | dummy(cjx)) & (~diri_ii);

        % Count the number of occurences of Dirichlet (i,i) entries
        counts = histc(row_dofs_for_K(diri_ii), 1:dim);

        K{s}(diri_ii) = 1.0 ./ counts(rjx(diri_ii));
        K{s}(diri_ij) = 0.0;
        
        % Final assemblage
        K{s} = sparse( row_dofs_for_K, col_dofs_for_K, K{s} );
    else
        K{s} = sparse( row_dofs_for_K, col_dofs_for_K, K{s} );
    end
end

end

