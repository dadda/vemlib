function [ fig1, fig2 ] = plotSolution( meshArray, Nx, Ny, Nz, u, k )

% Draw a 3D scatterplot where the color of each point is determined by
% the value of the solution at that point.

fig1 = figure();

% Compute dimension of global arrays
numSubdomains = Nx*Ny*Nz;
sizes = zeros(numSubdomains, 1);

for s = 1:numSubdomains
    sizes(s) = meshArray{s}.Nver;
end

cumSizes = [0; cumsum(sizes)];
dim = cumSizes(end);

X = zeros(dim,1);
Y = zeros(dim,1);
Z = zeros(dim,1);

for s = 1:numSubdomains
    X(cumSizes(s)+1:cumSizes(s+1)) = meshArray{s}.coordinates(:,1);
    Y(cumSizes(s)+1:cumSizes(s+1)) = meshArray{s}.coordinates(:,2);
    Z(cumSizes(s)+1:cumSizes(s+1)) = meshArray{s}.coordinates(:,3);
end

if k == 1
    data = vertcat( u{:} );
end

scatter3(X, Y, Z, [], data, 'filled' );
view(-45, 45)

% Draw a patch plot
fig2 = figure();
hold on;

for s = 1:numSubdomains
    for E = 1:meshArray{s}.Nelt
        element = construct_element_connectivity(meshArray{s}, E);
        for f = 1:length(element)
            if k == 1
                patch( 'XData', meshArray{s}.coordinates(element{f}, 1), ...
                       'YData', meshArray{s}.coordinates(element{f}, 2), ...
                       'ZData', meshArray{s}.coordinates(element{f}, 3), ...
                       'CData', u{s}(element{f}), 'FaceColor', 'interp' );
            end
        end
    end
end

view(-45, 45);
hold off;

end

