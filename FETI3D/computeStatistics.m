function [ numSubdomains, hmaxInv, Hoh, totNumDofs, numDofsBySubdomain ] = ...
                computeStatistics( meshArray, Nvect, hmaxBySubdomain, u )

numSubdomains = prod( Nvect );
hmaxInv       = 1./max(hmaxBySubdomain(:));

Hvect = 1 ./ Nvect;
H   = sqrt( sum ( Hvect.^2 ) );
Hoh = H*hmaxInv;

totNumDofs = 0;
numDofsBySubdomain = zeros(numSubdomains,1);

for s = 1:numSubdomains
    numDofsBySubdomain(s) = numel(u{s}) - numel(meshArray{s}.diridofs);
    totNumDofs = totNumDofs + numDofsBySubdomain(s);
end

end

