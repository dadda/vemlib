function [vertexDofs, edgeInternalDofs, faceInternalDofs, ...
          internalDofs, boundaryDofs ] = createPreliminaryDofPartition( mesh, k )

% Identify internal and boundary dofs for a mesh of the unit cube

if k == 1
    % For k = 1, we dofs only at the mesh nodes

    vertexDofs = zeros(8, 1);       % Eight vertices in a cube
    edgeInternalDofs = cell(12, 1); % Twelve edges
    faceInternalDofs = cell(6,1);   % Six faces
    
    %%% x = 0
    ixmin = find( abs( mesh.coordinates(:,1) ) < 1e-15 );

    % edge1: x = 0 \cap z = 0
    iedge1 = ixmin( abs( mesh.coordinates(ixmin,3) ) < 1e-15 );
    [~, tmp] = sort( mesh.coordinates(iedge1,2) );
    edgeInternalDofs{1} = iedge1(tmp(2:end-1));
    vertexDofs(1) = iedge1(tmp(1));
    vertexDofs(3) = iedge1(tmp(end));
    
    % edge5: x = 0 \cap y = 0
    iedge5 = ixmin( abs( mesh.coordinates(ixmin,2) ) < 1e-15 );
    [~, tmp] = sort( mesh.coordinates(iedge5,3) );
    edgeInternalDofs{5} = iedge5(tmp(2:end-1));
    vertexDofs(5) = iedge5(tmp(end));

    % edge7: x = 0 \cap y = 1
    iedge7 = ixmin( abs( 1-mesh.coordinates(ixmin,2) ) < 1e-15 );
    [~, tmp] = sort( mesh.coordinates(iedge7,3) );
    edgeInternalDofs{7} = iedge7(tmp(2:end-1));
    vertexDofs(7) = iedge7(tmp(end));

    % edge9: x = 0 \cap z = 1
    iedge9 = ixmin( abs( 1-mesh.coordinates(ixmin,3) ) < 1e-15 );
    [~, tmp] = sort( mesh.coordinates(iedge9,2) );
    edgeInternalDofs{9} = iedge9(tmp(2:end-1));

    % Internal nodes on x = 0
    faceInternalDofs{1} = setdiff( ixmin, [ vertexDofs([1 3 5 7]);
                                            vertcat( edgeInternalDofs{[1,5,7,9]} ) ] );

    %%% x = 1
    ixmax = find( abs( 1-mesh.coordinates(:,1) ) < 1e-15 );

    % edge2: x = 1 \cap z = 0
    iedge2 = ixmax( abs( mesh.coordinates(ixmax,3) ) < 1e-15 );
    [~, tmp] = sort( mesh.coordinates(iedge2,2) );
    edgeInternalDofs{2} = iedge2(tmp(2:end-1));
    vertexDofs(2) = iedge2(tmp(1));
    vertexDofs(4) = iedge2(tmp(end));

    % edge6: x = 1 \cap y = 0
    iedge6 = ixmax( abs( mesh.coordinates(ixmax,2) ) < 1e-15 );
    [~, tmp] = sort( mesh.coordinates(iedge6,3) );
    edgeInternalDofs{6} = iedge6(tmp(2:end-1));
    vertexDofs(6) = iedge6(tmp(end));

    % edge8: x = 1 \cap y = 1
    iedge8 = ixmax( abs( 1-mesh.coordinates(ixmax,2) ) < 1e-15 );
    [~, tmp] = sort( mesh.coordinates(iedge8,3) );
    edgeInternalDofs{8} = iedge8(tmp(2:end-1));
    vertexDofs(8) = iedge8(tmp(end));

    % edge10: x = 1 \cap z = 1
    iedge10 = ixmax( abs( 1-mesh.coordinates(ixmax,3) ) < 1e-15 );
    [~, tmp] = sort( mesh.coordinates(iedge10,2) );
    edgeInternalDofs{10} = iedge10(tmp(2:end-1));

    % Internal nodes on x = 1
    faceInternalDofs{2} = setdiff( ixmax, [ vertexDofs([2,4,6,8]);
                                            vertcat( edgeInternalDofs{[2,6,8,10]} ) ] );

    %%% y = 0
    iymin = find( abs( mesh.coordinates(:,2) ) < 1e-15 );

    % edge3: y = 0 \cap z = 0
    iedge3 = iymin( abs( mesh.coordinates(iymin,3) ) < 1e-15 );
    [~, tmp] = sort( mesh.coordinates(iedge3,1) );
    edgeInternalDofs{3} = iedge3(tmp(2:end-1));

    % edge11: y = 0 \cap z = 1
    iedge11 = iymin( abs( 1-mesh.coordinates(iymin,3) ) < 1e-15 );
    [~, tmp] = sort( mesh.coordinates(iedge11,1) );
    edgeInternalDofs{11} = iedge11(tmp(2:end-1));

    % Internal nodes on y = 0
    faceInternalDofs{3} = setdiff( iymin, [ vertexDofs([1,2,5,6]);
                                            vertcat( edgeInternalDofs{[3,5,6,11]} ) ] );

    %%% y = 1
    iymax = find( abs( 1-mesh.coordinates(:,2) ) < 1e-15 );

    % edge4: y = 1 \cap z = 0
    iedge4 = iymax( abs( mesh.coordinates(iymax,3) ) < 1e-15 );
    [~, tmp] = sort( mesh.coordinates(iedge4,1) );
    edgeInternalDofs{4} = iedge4(tmp(2:end-1));

    % edge12: y = 1 \cap z = 1
    iedge12 = iymax( abs( 1-mesh.coordinates(iymax,3) ) < 1e-15 );
    [~, tmp] = sort( mesh.coordinates(iedge12,1) );
    edgeInternalDofs{12} = iedge12(tmp(2:end-1));

    % Internal nodes on y = 1
    faceInternalDofs{4} = setdiff( iymax, [ vertexDofs([3,4,7,8]);
                                            vertcat( edgeInternalDofs{[4,7,8,12]} ) ] );

    %%% z = 0
    izmin = find( abs( mesh.coordinates(:,3) ) < 1e-15 );

    % Internal nodes on z = 0
    faceInternalDofs{5} = setdiff( izmin, [ vertexDofs(1:4);
                                            vertcat( edgeInternalDofs{1:4} ) ] );

    %%% z = 1
    izmax = find( abs( 1-mesh.coordinates(:,3) ) < 1e-15 );

    % Internal nodes on z = 1
    faceInternalDofs{6} = setdiff( izmax, [ vertexDofs(5:8);
                                            vertcat( edgeInternalDofs{9:12} ) ] );

    boundaryDofs = [ vertexDofs; vertcat( edgeInternalDofs{:} ); vertcat( faceInternalDofs{:} ) ];
    internalDofs = setdiff((1:mesh.Nver)', boundaryDofs);
                                        
    if 0
        plot3(mesh.coordinates(vertexDofs,1), mesh.coordinates(vertexDofs,2), mesh.coordinates(vertexDofs,3), 'r*') %#ok<UNRCH>
        hold on
        pause
        
        for i = 1:12
            plot3(mesh.coordinates(edgeInternalDofs{i},1), mesh.coordinates(edgeInternalDofs{i},2), mesh.coordinates(edgeInternalDofs{i},3), 'g*')
            pause
        end
        
        for i = 1:6
            plot3(mesh.coordinates(faceInternalDofs{i},1), mesh.coordinates(faceInternalDofs{i},2), mesh.coordinates(faceInternalDofs{i},3), 'c*')
            pause
        end
        
        plot3(mesh.coordinates(internalDofs,1), mesh.coordinates(internalDofs,2), mesh.coordinates(internalDofs,3), 'b*')
        pause
        % close(gcf)
        
        checkEdges = vertcat( edgeInternalDofs{:} );
        checkFaces = vertcat( faceInternalDofs{:} );
                   
        fprintf('\nChecking intersections...\n')
        intersect( vertexDofs, checkEdges )
        intersect( vertexDofs, checkFaces )
        intersect( vertexDofs, internalDofs )
        intersect( checkEdges, checkFaces )
        intersect( checkEdges, internalDofs )
        intersect( checkFaces, internalDofs )
    end

end




end