function delta = actionFI( lambda, ...
                           meshArray, k, use_mp, ...
                           Nx, Ny, Nz, ...
                           isEdgePrimal, isFacePrimal, ...
                           dimPrimal, dimLambda, ...
                           approach4constraints, ...
                           internalDofs, ...
                           vertexPrimalDofs, vertexDualDofs, ...
                           edgeAverageDofs, edgeAverageDofsLocal, edgeInternalDualDofs, ...
                           faceAverageDofs, faceAverageDofsLocal, faceInternalDualDofs, ...
                           Qedge, Tedge, ...
                           Qface, Tface, ...
                           Bprimal, Bdual, K, Kstar, runAsFor )

delta = zeros(dimLambda, 1);
y     = zeros(dimPrimal, 1);

numSubdomains = Nx*Ny*Nz;

numWorkers = getParforArg(runAsFor);

parfor ( s = 1:numSubdomains, numWorkers )
    primalDofs = [ vertexPrimalDofs{s};
                   edgeAverageDofs{s};
                   faceAverageDofs{s} ];

    dualDofs = [ vertexDualDofs{s};
                 edgeInternalDualDofs{s};
                 faceInternalDualDofs{s} ];

    numInternalDofs = length(internalDofs);
    
    if approach4constraints == 1

        % Step 1
        tmp = K{s}( [ internalDofs; dualDofs ], [ internalDofs; dualDofs ] ) \ ...
            [ sparse(numInternalDofs, 1); Bdual{s}' * lambda ];
        
        delta = delta + Bdual{s} * tmp(numInternalDofs+1:end);
        
        % Step 2-1
        y = y + Bprimal{s}' * ( K{s}( primalDofs, [ internalDofs; dualDofs ] ) * tmp );
    else
%         dim = size(K{s},1);
%         dimPrimalLocal = length(primalDofs);
%         
%         Kcr = zeros(dimPrimalLocal, dim);
% 
%         numPrimalVertices = length(vertexPrimalDofs{s});
% 
%         averageDofsLocal = [ edgeAverageDofsLocal{s}; faceAverageDofsLocal{s} ];
%         numTransformations = length(averageDofsLocal);
% 
%         ks = floor( (s-1) / (Nx*Ny) ) + 1;
%         js = floor( (s - Nx*Ny*(ks-1) - 1) / Nx ) + 1;
%         is = s - Nx*Ny*(ks-1) - Nx*(js-1);
%         
%         [~, interfaceEdges, interfaceFaces] = ...
%             getInterfaceVEF( Nx, Ny, Nz, is, js, ks );
% 
%         interfaceDofs = [ vertexDualDofs{s};
%                           vertcat( meshArray{s}.edgeInternalDofs{interfaceEdges} );
%                           vertcat( meshArray{s}.faceInternalDofs{interfaceFaces} ) ];
% 
%         transformedDofs = {};
% 
%         if isEdgePrimal
%             transformedDofs = vertcat( transformedDofs, meshArray{s}.edgeInternalDofs(interfaceEdges) );
%         end
%         if isFacePrimal
%             transformedDofs = vertcat( transformedDofs, meshArray{s}.faceInternalDofs(interfaceFaces) );
%         end
% 
%         T = vertcat( Tedge{s}(:), Tface{s}(:) );
% 
%         % Primal vertices do not need to be transformed
%         Kcr( 1:numPrimalVertices, [ internalDofs; interfaceDofs ] ) = ...
%             K{s}(vertexPrimalDofs{s},  [ internalDofs; interfaceDofs ] );
% 
%         for j = 1:numTransformations
%             Kcr( numPrimalVertices+j, [ internalDofs; interfaceDofs ] ) = ...
%                 T{j}(:, averageDofsLocal(j))' * K{s}( transformedDofs{j}, [ internalDofs; interfaceDofs ] );
%         end
% 
%         boundaryDofs = ...
%             [ meshArray{s}.vertexDofs;
%               vertcat( meshArray{s}.edgeInternalDofs{:} );
%               vertcat( meshArray{s}.faceInternalDofs{:} ) ];
% 
%         tmp = zeros(dim, 1);
%         tmp(dualDofs) = Bdual{s}' * lambda;
%         
%         tmp = KBBsolver( meshArray{s}, k, use_mp, ...
%                          interfaceEdges, interfaceFaces, ...
%                          isEdgePrimal, isFacePrimal, ...
%                          K{s}, tmp, ...
%                          internalDofs, boundaryDofs, ...
%                          vertexPrimalDofs{s}, ...
%                          Qedge{s}, Qface{s} );
% 
%         % Step 1
%         delta = delta + Bdual{s} * tmp(dualDofs);
% 
%         % Step 2-1
%         y = y + Bprimal{s}' * (Kcr * tmp);
    end
end

% Step 2-2
y = Kstar \ y;

% Step 2-3
z = actionFIrc( y, meshArray, k, use_mp, Nx, Ny, Nz, isEdgePrimal, isFacePrimal, ...
                dimLambda, approach4constraints, internalDofs, ...
                vertexPrimalDofs, vertexDualDofs, ...
                edgeAverageDofs, edgeAverageDofsLocal, edgeInternalDualDofs, ...
                faceAverageDofs, faceAverageDofsLocal, faceInternalDualDofs, ...
                Qedge, Tedge, Qface, Tface, Bprimal, Bdual, K, runAsFor );

% Combine Step 1 and Step 2
delta = delta + z;

end
