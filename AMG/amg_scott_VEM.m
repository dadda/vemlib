%---------------
% AMG hsl_mi20
% Dati S e f risolve il sistema
% Sx = f
% con gradiente coniugato e diversi precondizionatori
%------

% AMG
addpath ('/home/daniele/Documents/codes/hsl_mi20-2.0.0/matlab/');
javaaddpath ('/home/daniele/Documents/codes/hsl_mi20-2.0.0/matlab/');

% AGMG
addpath ('/home/daniele/Documents/codes/AGMG_3.3.0-aca/Matlab');

addpath('./../Basis_Functions');
addpath('./../Mesh_Handlers');
addpath('./../Quadrature');
addpath('./../Quadrature/cpp_modules');
addpath('./../VEM_diffusion');

fprintf('\nScript for profiling different preconditioning strategies for VEM\n\n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% User-required inputs

filename = input('Enter name of report file: ', 's');
fid = fopen(filename, 'a');

i_F = input('Enter type of load term: (1) exact, (2) uniform distribution in [-1,1]. Choice [1]: ');
if isempty(i_F)
    i_F = 1;
end

meshname     = input('Enter name of meshfile: ', 's');
isOFFmesh    = input('Is this a mesh read from an .off file ? ');

k            = input('Polynomial degree (>= 1) [1]: ');
if isempty(k)
    k = 1;
end

eoa = input('Extra order of accuracy for quadrature formulas [0]: ');
if isempty(eoa)
   eoa = 0; 
end

compressquad = input('Compress 2D quadrature rules? 0/1 [0]: ');
if isempty(compressquad)
    compressquad = 0;
end

basis_type = input('Type of basis functions: 1) Hitchhicker   2) Hitchhicker scaled. Choice [1]: ');
if isempty(basis_type)
    basis_type = 1;
end

use_mp = input('Use multiprecision toolbox (0/1) [0]? ');
if isempty(use_mp)
    use_mp = 0;
end

if use_mp
    addpath('./../../AdvanpixMCT-4.3.6.12354')
    precision = input('Set default precision: ');
    mp.Digits(precision);
    k = mp(k);
end

info.stoppingCriterion = input('Stopping criterion: (1) tolerance on relative residual norm (2) direct method errors. Choice [1]: ');
if isempty(info.stoppingCriterion)
    info.stoppingCriterion = 1;
end

if info.stoppingCriterion == 1
    tol = input('Enter tolerance on the relative residual norm [1e-8]: ');
    if isempty(tol)
        tol = 1e-8;
    end
else
    tol = inf;
end

maxit = input('Enter maximum number of iterations for PCG [1000]: ');
if isempty(maxit)
    maxit = 1000;
end

niter4time = input('Enter the number of times an algorithm should be run to estimate elapsed time [10]: ');
if isempty(niter4time)
    niter4time = 10;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Solve the problem

dimension = input('Do you want to run a 2D or a 3D test case? (2/3) [2]: ');
if isempty(dimension)
    dimension = 2;
end

if dimension == 2
    fprintf('Loading mesh %s...\n', meshname);
    if isOFFmesh
        mesh = readOFFmesh(meshname);
        mesh = loadmesh(mesh, use_mp);
    else
        mesh = loadmesh(meshname, use_mp);
    end
    fprintf('...mesh %s loaded\n', meshname);

    %
    % Assemble global stiffness matrix and load term
    %

    [~, ~, ~, ~, mesh, S, F, diridofs, sol] = ...
        VEM_d_parallel(mesh, k, eoa, compressquad, basis_type, use_mp, i_F);

%     %
%     % Plot mesh and compute relevant geometrical parameters
%     %
%
%     figure(); hold on; axis equal;
    
    he_min = Inf;
    for K = 1:mesh.Nelt
        Nver_loc = mesh.elements(K, 1);
        element = [ mesh.coordinates( mesh.elements(K, [2:Nver_loc+1 2]), 1 ) ...
                    mesh.coordinates( mesh.elements(K, [2:Nver_loc+1 2]), 2 ) ];
        he_tmp = min( sqrt( sum( ( element(2:end,:) - element(1:end-1,:) ).^2, 2 ) ) );
        if abs(he_tmp) < 1e-10
            disp('Check me')
        end
        he_min = min( he_tmp, he_min );
%         % These lines are meant to be used for Koch meshes
%         if mod(K,2)
%             patch(element(:,1), element(:,2), 'g')
%         else
%             patch(element(:,1), element(:,2), 'b')
%         end
%         % For other meshes, just plot element boundaries
%         plot( [element(:,1); element(1,1)], [element(:,2); element(1,2)], 'b-' );
    end    
end

info.sol = sol;
info.solEnergyNorm = sqrt( sol' * S * sol );
info.solInfNorm = max( abs( sol ) );

fprintf(fid, 'Data\n');
fprintf(fid, '*********************\n\n');
fprintf(fid, 'Minimum diameter: %.2e\n', he_min);
fprintf(fid, 'Degrees of freedom: %i\n\n', length(F));

fprintf('Data\n');
fprintf('*********************\n\n');
fprintf('Minimum diameter: %.2e\n', he_min);
fprintf('Degrees of freedom: %i\n\n', length(F));

%-------------------------------
% Run direct solver
%-------------------------------

elapsedTime = 0;

for i = 1:niter4time
    tic;
    x = S\F;
    elapsedTime = elapsedTime + toc;
end

elapsedTime = elapsedTime / niter4time;

% Compute eigenvalues
lambdaMin = eigs(S, 1, 'SM');
lambdaMax = eigs(S, 1, 'LM');

% Discrete energy norm of the relative error
errene = sqrt( (x-sol)' * S * (x-sol) ) / info.solEnergyNorm  ;
info.directErrorEnergyNorm = errene;

% Discrete inf norm of the relative error
errinf = max( abs( sol - x ) ) / info.solInfNorm;
info.directErrorInfNorm = errinf;

fprintf(fid, 'MATLAB direct solver\n');
fprintf(fid, '*****************************\n\n');

% fprintf(fid, 'Minimum eigenvalue: %.2e\n', lambdaMin);
% fprintf(fid, 'Maximum eigenvalue: %.2e\n\n', lambdaMax);
% fprintf(fid, 'Condition number: %.2e\n', lambdaMax/lambdaMin);
% fprintf(fid, 'Relative error energy norm: %.2e\n', errene);
% fprintf(fid, 'Relative error inf norm: %.2e\n\n', errinf);

fprintf(fid, '%i & %.2e & %.2e & %.2e & %.2e & %.2e & %.2e & %.1e\n', ...
             length(F), he_min, lambdaMin, lambdaMax, lambdaMax/lambdaMin, errene, errinf, elapsedTime);

fprintf('\nMATLAB direct solver\n');
fprintf('*****************************\n\n');

fprintf('Minimum eigenvalue: %.2e\n', lambdaMin);
fprintf('Maximum eigenvalue: %.2e\n\n', lambdaMax);
fprintf('Condition number: %.2e\n', lambdaMax/lambdaMin);
fprintf('Relative error energy norm: %.2e\n', errene);
fprintf('Relative error inf norm: %.2e\n', errinf);
fprintf('Elapsed time: %.1e\n\n', elapsedTime);

fprintf('%i & %.2e & %.2e & %.2e & %.2e & %.2e & %.2e & %.1e\n', ...
        length(F), he_min, lambdaMin, lambdaMax, lambdaMax/lambdaMin, errene, errinf, elapsedTime);

%-------------------------------
% Run CG without preconditioning
%-------------------------------

use_pc = 0;

elapsedTime = 0;

for i = 1:niter4time
    tic;
    [ x, iter, resf, ~, eigest ] = pcgmio_cond( S, F, maxit, tol, use_pc, info );
    elapsedTime = elapsedTime + toc;
end

elapsedTime = elapsedTime / niter4time;

% Discrete energy norm of the relative error
errene = sqrt( (x-sol)' * S * (x-sol) ) / info.solEnergyNorm ;

% Discrete inf norm of the relative error
errinf = max( abs( sol - x ) ) / info.solInfNorm;

fprintf(fid, '\nCG without preconditioning\n');
fprintf(fid, '*****************************\n\n');

% fprintf(fid, 'Final iteration number: %i\n', iter);
% fprintf(fid, 'Final residual: %.2e\n', resf);
% fprintf(fid, 'Minimum eigenvalue: %.2e\n', eigest(1));
% fprintf(fid, 'Maximum eigenvalue: %.2e\n\n', eigest(2));
% fprintf(fid, 'Estimated condition number: %.2e\n', eigest(2)/eigest(1));
% fprintf(fid, 'Relative error energy norm: %.2e\n', errene);
% fprintf(fid, 'Relative error inf norm: %.2e\n\n', errinf);

fprintf(fid, '%i & %.2e & %i & %.2e & %.2e & %.2e & %.2e & %.2e & %.1e\n', ...
             length(F), he_min, iter, eigest(1), eigest(2), eigest(2)/eigest(1), errene, errinf, elapsedTime);

fprintf('CG without preconditioning\n');
fprintf('*****************************\n\n');

fprintf('Final iteration number: %i\n', iter);
fprintf('Final residual: %.2e\n', resf);
fprintf('Minimum eigenvalue: %.2e\n', eigest(1));
fprintf('Maximum eigenvalue: %.2e\n\n', eigest(2));
fprintf('Estimated condition number: %.2e\n', eigest(2)/eigest(1));
fprintf('Relative error energy norm: %.2e\n', errene);
fprintf('Relative error inf norm: %.2e\n', errinf);
fprintf('Elapsed time: %.1e\n\n', elapsedTime);

fprintf('%i & %.2e & %i & %.2e & %.2e & %.2e & %.2e & %.2e & %.1e\n', ...
        length(F), he_min, iter, eigest(1), eigest(2), eigest(2)/eigest(1), errene, errinf, elapsedTime);

%--------------------------------
% Run CG with AMG preconditioning
%--------------------------------

use_pc = 1;

elapsedTime = 0;

for i = 1:niter4time
    tic;

    % Initialize AMG
    hsl_mi20_startup
    control = hsl_mi20_control;
    inform = hsl_mi20_setup(S,control);

    % Run PCG
    [ x, iter, resf, ~, eigest ] = pcgmio_cond( S, F, maxit, tol, use_pc, info );

    % Destroy AMG
    hsl_mi20_finalize;

    elapsedTime = elapsedTime + toc;
end

elapsedTime = elapsedTime / niter4time;

% Discrete energy norm of the relative error
errene = sqrt( (x-sol)' * S * (x-sol) ) / info.solEnergyNorm ;

% Discrete inf norm of the relative error
errinf = max( abs( sol - x ) ) / info.solInfNorm;

fprintf(fid, 'PCG with AMG preconditioning\n');
fprintf(fid, '****************************\n\n');

% fprintf(fid, 'Final iteration number: %i\n', iter);
% fprintf(fid, 'Final residual: %.2e\n', resf);
% fprintf(fid, 'Minimum eigenvalue: %.2e\n', min(eigest));
% fprintf(fid, 'Maximum eigenvalue: %.2e\n', max(eigest));
% fprintf(fid, 'Estimated condition number: %.2e\n', max(eigest)/min(eigest));
% fprintf(fid, 'Relative error energy norm: %.2e\n', errene);
% fprintf(fid, 'Relative error inf norm: %.2e\n\n', errinf);

fprintf(fid, '%i & %.2e & %i & %.2e & %.2e & %.2e & %.2e & %.2e & %.1e\n', ...
             length(F), he_min, iter, eigest(1), eigest(2), eigest(2)/eigest(1), errene, errinf, elapsedTime);

fprintf('\nPCG with AMG preconditioning\n');
fprintf('****************************\n\n');

fprintf('Final iteration number: %i\n', iter);
fprintf('Final residual: %.2e\n', resf);
fprintf('Minimum eigenvalue: %.2e\n', min(eigest));
fprintf('Maximum eigenvalue: %.2e\n', max(eigest));
fprintf('Estimated condition number: %.2e\n\n', max(eigest)/min(eigest));
fprintf('Relative error energy norm: %.2e\n', errene);
fprintf('Relative error inf norm: %.2e\n', errinf);
fprintf('Elapsed time: %.1e\n\n', elapsedTime);

fprintf('%i & %.2e & %i & %.2e & %.2e & %.2e & %.2e & %.2e & %.1e\n', ...
        length(F), he_min, iter, eigest(1), eigest(2), eigest(2)/eigest(1), errene, errinf, elapsedTime);


%----------------------------------
% Run PCG with AGMG preconditioning
%----------------------------------

use_pc = 2;

elapsedTime = 0;

for i = 1:niter4time
    tic;

    % Initialize AGMG
    agmg(S, [], [], [], [], [], [], 1);

    % Run PCG
    [ x, iter, resf, ~, eigest ] = pcgmio_cond( S, F, maxit, tol, use_pc, info );

    % Destroy AGMG
    agmg(S, [], [], [], [], [], [], -1);

    elapsedTime = elapsedTime + toc;
end

elapsedTime = elapsedTime / niter4time;

% Discrete energy norm of the relative error
errene = sqrt( (x-sol)' * S * (x-sol) ) / info.solEnergyNorm ;

% Discrete inf norm of the relative error
errinf = max( abs( sol - x ) ) / info.solInfNorm;

fprintf(fid, 'PCG with AGMG preconditioning\n');
fprintf(fid, '*****************************\n\n');

% fprintf(fid, 'Final iteration number: %i\n', iter);
% fprintf(fid, 'Final residual: %.2e\n', resf);
% fprintf(fid, 'Minimum eigenvalue: %.2e\n', min(eigest));
% fprintf(fid, 'Maximum eigenvalue: %.2e\n', max(eigest));
% fprintf(fid, 'Estimated condition number: %.2e\n', max(eigest)/min(eigest));
% fprintf(fid, 'Relative error energy norm: %.2e\n', errene);
% fprintf(fid, 'Relative error inf norm: %.2e\n\n', errinf);

fprintf(fid, '%i & %.2e & %i & %.2e & %.2e & %.2e & %.2e & %.2e & %.1e\n', ...
             length(F), he_min, iter, eigest(1), eigest(2), eigest(2)/eigest(1), errene, errinf, elapsedTime);

fprintf('\nPCG with AGMG preconditioning\n');
fprintf('*****************************\n\n');

fprintf('Final iteration number: %i\n', iter);
fprintf('Final residual: %.2e\n', resf);
fprintf('Minimum eigenvalue: %.2e\n', min(eigest));
fprintf('Maximum eigenvalue: %.2e\n', max(eigest));
fprintf('Estimated condition number: %.2e\n\n', max(eigest)/min(eigest));
fprintf('Relative error energy norm: %.2e\n', errene);
fprintf('Relative error inf norm: %.2e\n', errinf);
fprintf('Elapsed time: %.1e\n\n', elapsedTime);

fprintf('%i & %.2e & %i & %.2e & %.2e & %.2e & %.2e & %.2e & %.1e\n', ...
        length(F), he_min, iter, eigest(1), eigest(2), eigest(2)/eigest(1), errene, errinf, elapsedTime);


%-----------------------------------
% Run PCG with ILU preconditioner
%-----------------------------------

use_pc = 3;

elapsedTime = 0;

for i = 1:niter4time
    tic;

    % Initialize ILU preconditioner
    [info.ilu.L, info.ilu.U] = ilu(S);

    % Run PCG
    [ x, iter, resf, ~, eigest ] = pcgmio_cond( S, F, maxit, tol, use_pc, info );

    % Destroy ILU preconditioner data
    info.ilu.L = [];
    info.ilu.U = [];

    elapsedTime = elapsedTime + toc;
end

elapsedTime = elapsedTime / niter4time;

% Discrete energy norm of the relative error
errene = sqrt( (x-sol)' * S * (x-sol) ) / info.solEnergyNorm ;

% Discrete inf norm of the relative error
errinf = max( abs( sol - x ) ) / info.solInfNorm;

fprintf(fid, 'PCG with ILU preconditioning\n');
fprintf(fid, '****************************\n\n');

% fprintf(fid, 'Final iteration number: %i\n', iter);
% fprintf(fid, 'Final residual: %.2e\n', resf);
% fprintf(fid, 'Minimum eigenvalue: %.2e\n', min(eigest));
% fprintf(fid, 'Maximum eigenvalue: %.2e\n', max(eigest));
% fprintf(fid, 'Estimated condition number: %.2e\n', max(eigest)/min(eigest));
% fprintf(fid, 'Relative error energy norm: %.2e\n', errene);
% fprintf(fid, 'Relative error inf norm: %.2e\n\n', errinf);

fprintf(fid, '%i & %.2e & %i & %.2e & %.2e & %.2e & %.2e & %.2e & %.1e\n', ...
             length(F), he_min, iter, eigest(1), eigest(2), eigest(2)/eigest(1), errene, errinf, elapsedTime);

fprintf('\nPCG with ILU preconditioning\n');
fprintf('****************************\n\n');

fprintf('Final iteration number: %i\n', iter);
fprintf('Final residual: %.2e\n', resf);
fprintf('Minimum eigenvalue: %.2e\n', min(eigest));
fprintf('Maximum eigenvalue: %.2e\n', max(eigest));
fprintf('Estimated condition number: %.2e\n\n', max(eigest)/min(eigest));
fprintf('Relative error energy norm: %.2e\n', errene);
fprintf('Relative error inf norm: %.2e\n', errinf);
fprintf('Elapsed time: %.1e\n\n', elapsedTime);

fprintf('%i & %.2e & %i & %.2e & %.2e & %.2e & %.2e & %.2e & %.1e\n', ...
        length(F), he_min, iter, eigest(1), eigest(2), eigest(2)/eigest(1), errene, errinf, elapsedTime);

fclose(fid);
