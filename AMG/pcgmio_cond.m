% preconditioned pcg
function [ x, k, resf, mem, eigest ] = pcgmio_cond( A, b, maxit, tol, ip, info )
%
%
n=length(b);
beta=0; x=zeros(n,1);
r=b; ap=x;
gamma=r'*r;
res0=sqrt(gamma);
res=res0;
mem=1;
alfa=1;

k=0;
while k < maxit
    if info.stoppingCriterion == 1
        if res/res0 <= tol
            break;
        end
    else
        errene = sqrt( (x-info.sol)' * A * (x-info.sol) ) / info.solEnergyNorm ;
        errinf = max( abs( info.sol - x ) ) / info.solInfNorm;
        if ( errene <= info.directErrorEnergyNorm ) && ...
                ( errinf <= info.directErrorInfNorm )
            break;
        end
    end
 
  if ip == 0
      % no preconditioner
      z = r;
  elseif ip == 1
      % Standard AMG preconditioner
      z = hsl_mi20_precondition(r);
  elseif ip == 2
      % AGMG preconditioner
      z = agmg([], full(r), [], [], [], [], [], 3);
  elseif ip == 3
      % ILU preconditioner
      z = info.ilu.U \ ( info.ilu.L \ r );
  end
  
  k=k+1;
  gamma=r'*z;
  if (k==1), p=z;
  else
      beta=gamma/gamma0;
      p=z+beta*p;
  end
  
  ap = A*p;  
  delta=p'*ap;
  
  % only for eigest.
  oldalfa = alfa;
    
  alfa = gamma/delta;
  x = x + alfa*p;
  r = r - alfa*ap;
  
  if(alfa==0)
      disp('stagnation')
  end
  
  if(alfa <= 0.0)
      disp('Negative matrix')
  end
  
% approximate condition number of M^-1*A
% matrice tridiagonal 
   if ( k > 1 )                       % 
       tri(k,k)=(1/alfa)+(beta/oldalfa);
       tri(k,k-1)=-sqrt(beta)/oldalfa;
       tri(k-1,k)=tri(k,k-1);
     else
       tri(k,k)=(1/alfa);
   end 
       
  gamma0=gamma;
  res = norm(r);
  
  %disp([k,res/res0])   %, norm(b-a*x)])
  mem=[mem,res/res0];

end	

esti   = eig(tri);
eigest = [min(esti), max(esti)];

resf=res/res0;
% figure(5)
% semilogy(mem,'--b')
end
