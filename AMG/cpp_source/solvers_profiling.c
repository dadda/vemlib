
static char help[] = "Reads a matrix, rhs and exact solution from a file \
and solves a linear system.\n\
This version first preloads and solves a small system, then loads \n\
another (larger) system and solves it as well.  This example illustrates\n\
preloading of instructions with the smaller system so that more accurate\n\
performance monitoring can be done with the larger one (that actually\n\
is the system of interest).  See the 'Performance Hints' chapter of the\n\
users manual for a discussion of preloading. Input parameters include:\n\
  -f <input_file> : file with matrix, rhs, and exact solution\n\
  -check_symmetry : check whether the system matrix is symmetric\n\n";


#include <petscksp.h>
#include <math.h>

typedef struct convergenceTestData {
  PetscInt  criterion;
  PetscReal xstarNormInf;
  Vec*      xstarPtr;
  Vec       x;
  PetscReal relerrNormInfRef;
} convergenceTestData;


/*
 * User defined convergence test
 */
PetscErrorCode convergenceTest( KSP ksp,
				PetscInt it,
				PetscReal rnorm,
				KSPConvergedReason* reason,
				void* data )
{
  PetscBool      flg;
  PetscInt       maxits;
  PetscReal      relerrNormInf;
  PetscErrorCode ierr = 0;

  *reason = KSP_CONVERGED_ITERATING; /* tell the program KSP is still running */

  /*
   * Check for convergence first
   */

  switch( ((convergenceTestData *)data)->criterion ) {

  case 1:
    /*
     * Compare the relative error inf-norm with that of a direct method
     */
    ierr = KSPBuildSolution(ksp, ((convergenceTestData *)data)->x, NULL); CHKERRQ(ierr);
    ierr = VecAXPY(((convergenceTestData *)data)->x,
		   -1.0, *(((convergenceTestData *)data)->xstarPtr)); CHKERRQ(ierr);
    ierr = VecNorm(((convergenceTestData *)data)->x, NORM_INFINITY, &relerrNormInf); CHKERRQ(ierr);
    relerrNormInf /= ((convergenceTestData *)data)->xstarNormInf;

    if ( relerrNormInf <= ((convergenceTestData *)data)->relerrNormInfRef ) {
      *reason = 1;
      return ierr;
    }
    break;

  default:
    SETERRQ(PETSC_COMM_WORLD, 1, "Convergence criterion not recognized");

  }

  /*
   * Now check for divergence
   */

  ierr = KSPGetTolerances(ksp, NULL, NULL, NULL, &maxits); CHKERRQ(ierr);
  if ( it >= maxits ) { *reason = KSP_DIVERGED_ITS; return ierr; }

  flg = PetscIsNanReal(rnorm) || PetscIsInfReal(rnorm);
  if ( flg ) { *reason = KSP_DIVERGED_NANORINF; return ierr; }

  return ierr;
}


PetscErrorCode destroyConvergenceTestData( void * data )
{
  PetscErrorCode ierr;

  ierr = VecDestroy( &(((convergenceTestData *)data)->x) ); CHKERRQ(ierr);
  return ierr;
}


int main(int argc, char **argv)
{
  KSP                 ksp;                       /* linear solver context */
  Mat                 A;                         /* VEM matrix */
  Vec                 b, xstar, x, u;            /* rhs, exact solution, approx solution, tmp */
  char                file[4][PETSC_MAX_PATH_LEN];  /* input file name */
  char                kspinfo[120];
  PetscInt            m, M, its, itmp[2], criterion;
  PetscViewer         fd, viewer;
  PetscBool           flg, preload = PETSC_TRUE;
  PetscBool           load_reference = PETSC_FALSE, save_reference = PETSC_FALSE;
  PetscBool           custom_convergence_test = PETSC_TRUE;
  PetscBool           solution_is_available = PETSC_TRUE;
  convergenceTestData testData;
  PetscReal           absresRef, relerrNormInfRef;
  PetscReal           bNorm2, absres;
  PetscReal           xstarNormInf, xstarNormEnergy;
  PetscReal           relerrNormInf, relerrNormEnergy;
  PetscReal           rtol, abstol, dtol;
  PetscErrorCode      ierr;
  KSPConvergedReason  reason;

  ierr = PetscInitialize(&argc, &argv, (char*)0, help); if ( ierr ) return ierr;

  /*
   * Determine files from which we read the two linear systems
   * (matrix and right-hand-side vector).
   */

  /* Determine the system of interest first */
  ierr = PetscOptionsGetString(NULL, NULL, "-f", file[1], PETSC_MAX_PATH_LEN, &flg); CHKERRQ(ierr);
  if ( !flg ) SETERRQ(PETSC_COMM_WORLD, 1, "Must indicate binary file with the -f option");

  /* Determine the system for preloading */
  ierr = PetscOptionsGetString(NULL, NULL, "-fpreload", file[0], PETSC_MAX_PATH_LEN, &preload);
  CHKERRQ(ierr);
  if ( !preload ) {
    /* don't bother with preloading */
    ierr = PetscStrcpy(file[0], file[1]); CHKERRQ(ierr);
  }

  /*
   * Determine whether the solution has been stored or not
   */

  ierr = PetscOptionsGetBool(NULL, NULL, "-solution_is_available", NULL, &solution_is_available);
  CHKERRQ(ierr);

  /*
   * Determine whether the relative residual should be saved and/or loaded
   */

  ierr = PetscOptionsGetString(NULL, NULL, "-fload_reference",
			       file[2], PETSC_MAX_PATH_LEN, &load_reference); CHKERRQ(ierr);
  if ( load_reference ) {
    PetscViewer viewRef;

    ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD, file[2], FILE_MODE_READ, &viewRef);
    CHKERRQ(ierr);

    /* No need for reading leading integers because it is this
     * script that saves reference values, not MATLAB */
    ierr = PetscViewerBinaryRead(viewRef, &absresRef, 1, NULL, PETSC_DOUBLE); CHKERRQ(ierr);

    if ( solution_is_available ) {
      ierr = PetscViewerBinaryRead(viewRef, &relerrNormInfRef, 1, NULL, PETSC_DOUBLE);
      CHKERRQ(ierr);
    }

    ierr = PetscViewerDestroy(&viewRef); CHKERRQ(ierr);
  }

  ierr = PetscOptionsGetString(NULL, NULL, "-fsave_reference", file[3], PETSC_MAX_PATH_LEN,
			       &save_reference); CHKERRQ(ierr);

  ierr = PetscOptionsGetInt(NULL, NULL, "-custom_convergence_test",
			    &criterion, &custom_convergence_test); CHKERRQ(ierr);

  /* -----------------------------------------------------------
                  Beginning of linear solver loop
     ----------------------------------------------------------- */

  PetscPreLoadBegin(preload, "Load system");

  /* ------------------------ New stage ------------------------
                             Load system
     ----------------------------------------------------------- */

  ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD, file[PetscPreLoadIt], FILE_MODE_READ, &fd);
  CHKERRQ(ierr);

  /*
   * Load the matrix and vector; then destroy the viewer.
   */

  ierr = MatCreate(PETSC_COMM_WORLD, &A); CHKERRQ(ierr);
  ierr = MatSetFromOptions(A); CHKERRQ(ierr);
  ierr = MatLoad(A, fd); CHKERRQ(ierr);
  ierr = MatSetOption(A, MAT_SYMMETRIC, PETSC_TRUE); CHKERRQ(ierr);

  ierr = VecCreate(PETSC_COMM_WORLD, &b); CHKERRQ(ierr);
  ierr = VecSetFromOptions(b); CHKERRQ(ierr);
  ierr = VecLoad(b, fd); CHKERRQ(ierr);

  if ( solution_is_available || ( preload && (PetscPreLoadIt == 0) ) ) {
    ierr = VecCreate(PETSC_COMM_WORLD, &xstar); CHKERRQ(ierr);
    ierr = VecSetFromOptions(xstar); CHKERRQ(ierr);
    ierr = VecLoad(xstar, fd); CHKERRQ(ierr);
  }

  ierr = PetscViewerBinaryRead(fd, itmp, 2, NULL, PETSC_INT); CHKERRQ(ierr);
  ierr = PetscViewerBinaryRead(fd, &bNorm2, 1, NULL, PETSC_DOUBLE); CHKERRQ(ierr);

  if ( solution_is_available || ( preload && (PetscPreLoadIt == 0) ) ) {
    ierr = PetscViewerBinaryRead(fd, itmp, 2, NULL, PETSC_INT); CHKERRQ(ierr);
    ierr = PetscViewerBinaryRead(fd, &xstarNormInf, 1, NULL, PETSC_DOUBLE); CHKERRQ(ierr);

    ierr = PetscViewerBinaryRead(fd, itmp, 2, NULL, PETSC_INT); CHKERRQ(ierr);  
    ierr = PetscViewerBinaryRead(fd, &xstarNormEnergy, 1, NULL, PETSC_DOUBLE); CHKERRQ(ierr);
  }

  ierr = PetscViewerDestroy(&fd); CHKERRQ(ierr);

  /* Check whether A is symmetric */
  ierr = PetscOptionsGetBool(NULL, NULL, "-check_symmetry", NULL, &flg); CHKERRQ(ierr);
  if ( flg ) {
    Mat       Atmp;
    PetscReal err;

    ierr = MatTranspose(A, MAT_INITIAL_MATRIX, &Atmp); CHKERRQ(ierr);
    ierr = MatAXPY(Atmp, -1.0, A, SAME_NONZERO_PATTERN); CHKERRQ(ierr);
    ierr = MatNorm(Atmp, NORM_1, &err); CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_WORLD, "\n\n*** Simmetry check ***\n\n"); CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_WORLD, "||A - A^T||_1: %e\n", err); CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_WORLD, "\n*** End of Simmetry check ***\n\n"); CHKERRQ(ierr);
    ierr = MatDestroy(&Atmp); CHKERRQ(ierr);
  }

  /*
   * If the loaded matrix is larger than the rhs and/or the exact solution (due to being padded
   * to match the block size of the system), then create new padded vectors.
   */

  ierr = MatGetSize(A, &M, NULL); CHKERRQ(ierr);
  ierr = VecGetSize(b, &m); CHKERRQ(ierr);

  if ( M != m ) {   /* Create a new vector b by padding the old one */
    PetscInt    j, mvec, start, end, indx;
    Vec         tmp;
    PetscScalar *bold;

    ierr = VecCreate(PETSC_COMM_WORLD, &tmp); CHKERRQ(ierr);
    ierr = VecSetSizes(tmp, M, PETSC_DECIDE); CHKERRQ(ierr);
    ierr = VecSetFromOptions(tmp); CHKERRQ(ierr);
    ierr = VecGetOwnershipRange(b, &start, &end); CHKERRQ(ierr);
    ierr = VecGetLocalSize(b, &mvec); CHKERRQ(ierr);
    ierr = VecGetArray(b, &bold); CHKERRQ(ierr);
    for (j = 0; j < mvec; ++j) {
      indx = start+j;
      ierr = VecSetValues(tmp, 1, &indx, bold+j, INSERT_VALUES); CHKERRQ(ierr);
    }
    ierr = VecRestoreArray(b, &bold); CHKERRQ(ierr);
    ierr = VecDestroy(&b); CHKERRQ(ierr);
    ierr = VecAssemblyBegin(tmp); CHKERRQ(ierr);
    ierr = VecAssemblyEnd(tmp); CHKERRQ(ierr);
    b    = tmp;
  }

  ierr = MatCreateVecs(A, &x, NULL); CHKERRQ(ierr);
  ierr = VecDuplicate(b, &u); CHKERRQ(ierr);

  /*
   * Set 0 initial guess for conjugate gradient
   */
  ierr = VecSet(x, 0.0); CHKERRQ(ierr);

  /* ierr = MatView(A, PETSC_VIEWER_STDOUT_WORLD); CHKERRQ(ierr); */
  /* ierr = VecView(b, PETSC_VIEWER_STDOUT_WORLD); CHKERRQ(ierr); */

  /* ------------------------ New stage ------------------------
                      Setup and solve system
     ----------------------------------------------------------- */

  /*
   * Conclude profiling last stage; begin profiling next stage.
   */
  PetscPreLoadStage("KSPSetUpAndSolve");

  /*
   * Create linear solver; set operators; set runtime options.
   */
  ierr = KSPCreate(PETSC_COMM_WORLD, &ksp); CHKERRQ(ierr);
  ierr = KSPSetInitialGuessNonzero(ksp, PETSC_FALSE); CHKERRQ(ierr);

  ierr = KSPSetOperators(ksp, A, A); CHKERRQ(ierr);

  if ( custom_convergence_test && ( !preload || (PetscPreLoadIt == 1) ) ) {

    /*
     * Update the convergence test only for the system of interest
     */

    if ( !load_reference )
      SETERRQ(PETSC_COMM_WORLD, 1,
	      "Must load binary file with reference data with the -fload_reference option");

    if ( criterion == 0 ) {
      ierr = KSPSetTolerances(ksp, 0.0, absresRef, PETSC_DEFAULT, PETSC_DEFAULT); CHKERRQ(ierr);
    } else if ( criterion == 1 ) {
      if ( !solution_is_available )
	SETERRQ(PETSC_COMM_WORLD, 1, "Must indicate binary file where solution is stored");

      testData.criterion        = criterion;
      testData.xstarNormInf     = xstarNormInf;
      testData.xstarPtr         = &xstar;
      testData.relerrNormInfRef = relerrNormInfRef;

      ierr = VecDuplicate( xstar, &(testData.x) ); CHKERRQ(ierr);
      ierr = KSPSetConvergenceTest(ksp, convergenceTest, &testData, destroyConvergenceTestData);
      CHKERRQ(ierr);
    }
  }
  
  ierr = KSPSetFromOptions(ksp); CHKERRQ(ierr);

  /*
   * Here we explicitly call KSPSetUp() and KSPSetUpOnBlocks() to
   * enable more precise profiling of setting up the preconditioner.
   * These calls are optional, since both will be called within
   * KSPSolve() if they haven't been called already.
   */
  ierr = KSPSetUp(ksp); CHKERRQ(ierr);
  ierr = KSPSetUpOnBlocks(ksp); CHKERRQ(ierr);

  /*
   * Solve system
   */
  ierr = KSPSolve(ksp, b, x); CHKERRQ(ierr);
  ierr = KSPGetIterationNumber(ksp, &its); CHKERRQ(ierr);

  /* ------------------------ New stage ------------------------
          Check error, print output, free data structures.
     ----------------------------------------------------------- */

  /*
   * Conclude profiling last stage; begin profiling next stage.
   */
  PetscPreLoadStage("PostProc");

  /*
   * Write output and check error. KSPView() prints information about the linear solver.
   */

  ierr = PetscViewerStringOpen(PETSC_COMM_WORLD, kspinfo, 120, &viewer); CHKERRQ(ierr);
  ierr = KSPView(ksp, viewer); CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer); CHKERRQ(ierr);

  /*
   * Check error
   */

  /* Compute relative residual norm and relative error inf norm */
  ierr   = MatMult(A, x, u); CHKERRQ(ierr);
  ierr   = VecAXPY(u, -1.0, b); CHKERRQ(ierr);
  ierr   = VecNorm(u, NORM_2, &absres); CHKERRQ(ierr);

  if ( solution_is_available || ( preload && (PetscPreLoadIt == 0) ) ) {
    ierr = VecAXPY(xstar, -1.0, x); CHKERRQ(ierr); /* Now xstar = x - solution */
    ierr = VecNorm(xstar, NORM_INFINITY, &relerrNormInf); CHKERRQ(ierr);
    relerrNormInf /= xstarNormInf;

    /* Compute relative error energy norm */
    ierr = MatMult(A, xstar, u); CHKERRQ(ierr);
    ierr = VecDot(xstar, u, &relerrNormEnergy); CHKERRQ(ierr);
    relerrNormEnergy = sqrt( relerrNormEnergy ) / xstarNormEnergy;
  }

  if ( save_reference && ( !preload || (PetscPreLoadIt == 1) ) ) {
    PetscViewer writeRef;

    ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD, file[3], FILE_MODE_WRITE, &writeRef);
    CHKERRQ(ierr);

    ierr = PetscViewerBinaryWrite(writeRef, &absres, 1, PETSC_DOUBLE, PETSC_FALSE);
    CHKERRQ(ierr);

    if ( solution_is_available ) {
      ierr = PetscViewerBinaryWrite(writeRef, &relerrNormInf, 1, PETSC_DOUBLE, PETSC_FALSE);
      CHKERRQ(ierr);
    }

    ierr = PetscViewerDestroy(&writeRef); CHKERRQ(ierr);
  }

  /*
   * Print results
   */

  ierr = PetscPrintf(PETSC_COMM_WORLD, "\n\n*** Results ***\n\n"); CHKERRQ(ierr);

  ierr = PetscPrintf(PETSC_COMM_WORLD, "kspinfo: %s\n", kspinfo); CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "Dof: %i\n", M); CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "Iterations: %i\n", its); CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "Absolute residual 2-norm: %e\n", absres); CHKERRQ(ierr);

  if ( solution_is_available || ( preload && (PetscPreLoadIt == 0) ) ) {
    ierr = PetscPrintf(PETSC_COMM_WORLD, "Relative error inf-norm: %e\n", relerrNormInf);
    CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_WORLD, "Relative error energy norm: %e\n", relerrNormEnergy);
    CHKERRQ(ierr);
  }

  /* Get tolerances for printing purposes */
  ierr = KSPGetTolerances(ksp, &rtol, &abstol, &dtol, NULL); CHKERRQ(ierr);

  ierr = KSPGetConvergedReason(ksp, &reason); CHKERRQ(ierr);
  switch (reason) {
  case 1:
    ierr = PetscPrintf(PETSC_COMM_WORLD,
		       "Reason: relative error inf-norm smaller than or equal to "
		       "reference ( %e )\n", relerrNormInfRef); CHKERRQ(ierr);
    break;
  case KSP_CONVERGED_RTOL:
    ierr = PetscPrintf(PETSC_COMM_WORLD,
		       "Reason: (preconditioned) residual 2-norm decreased by a factor of rtol = %e, from 2-norm of right hand side\n",
		       rtol); CHKERRQ(ierr);
    break;
  case KSP_CONVERGED_ATOL:
    if ( custom_convergence_test && ( criterion == 0 ) ) {
      ierr = PetscPrintf(PETSC_COMM_WORLD,
			 "Reason: (preconditioned) residual 2-norm smaller than or equal to reference "
			 "( %e )\n", absresRef); CHKERRQ(ierr);
    } else {
      ierr = PetscPrintf(PETSC_COMM_WORLD,
			 "Reason: (preconditioned) residual 2-norm smaller than abstol = %e\n",
			 abstol); CHKERRQ(ierr);
    }
    break;
  case KSP_CONVERGED_ITS:
    ierr = PetscPrintf(PETSC_COMM_WORLD,
  		       "Reason: used by the preonly preconditioner "
  		       "that always uses ONE iteration\n"); CHKERRQ(ierr);
    break;
  case KSP_CONVERGED_CG_NEG_CURVE:
    ierr = PetscPrintf(PETSC_COMM_WORLD,
  		       "Reason: convergence is reached along "
  		       "a negative curvature direction\n"); CHKERRQ(ierr);
    break;
  case KSP_DIVERGED_ITS:
    ierr = PetscPrintf(PETSC_COMM_WORLD,
  		       "Reason: required more than %i iterations "
  		       "to reach convergence\n", its); CHKERRQ(ierr);
    break;
  case KSP_DIVERGED_DTOL:
    ierr = PetscPrintf(PETSC_COMM_WORLD,
  		       "Reason: residual norm increased by a factor of dtol ( %e )\n", dtol);
    CHKERRQ(ierr);
    break;
  case KSP_DIVERGED_NANORINF:
    ierr = PetscPrintf(PETSC_COMM_WORLD,
  		       "Reason: residual norm became Not-a-number or Inf "
  		       "likely due to 0/0\n"); CHKERRQ(ierr);
    break;
  case KSP_DIVERGED_BREAKDOWN:
    ierr = PetscPrintf(PETSC_COMM_WORLD, "Reason: generic breakdown in method\n"); CHKERRQ(ierr);
    break;
  default:
    ierr = PetscPrintf(PETSC_COMM_WORLD, ">>> WARNING, UNEXPECTED BREAKDOWN <<<\n"); CHKERRQ(ierr);
  }

  ierr = PetscPrintf(PETSC_COMM_WORLD, "\n*** End of Results ***\n\n"); CHKERRQ(ierr);

  /*
   * Free work space
   */
  ierr = MatDestroy(&A); CHKERRQ(ierr);
  ierr = VecDestroy(&b); CHKERRQ(ierr);
  if ( solution_is_available || ( preload && (PetscPreLoadIt == 0) ) ) {
    ierr = VecDestroy(&xstar); CHKERRQ(ierr);
  }
  ierr = VecDestroy(&u); CHKERRQ(ierr);
  ierr = VecDestroy(&x); CHKERRQ(ierr);
  ierr = KSPDestroy(&ksp); CHKERRQ(ierr);

  PetscPreLoadEnd();

  /* -----------------------------------------------------------
                      End of linear solver loop
     ----------------------------------------------------------- */
  
  ierr = PetscFinalize();
  return ierr;
}
