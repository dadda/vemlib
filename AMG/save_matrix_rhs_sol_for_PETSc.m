
addpath ../Basis_Functions
addpath ../Mesh_Handlers
addpath ../Quadrature
addpath ../Quadrature/cpp_modules
addpath ../Scripts
addpath ../VEM_diffusion
addpath /home/daniele/Documents/codes/petsc/share/petsc/matlab

i_F = input('Enter type of load term: (1) exact, (2) uniform distribution in [-1,1]. Choice [1]: ');
if isempty(i_F)
    i_F = 1;
end

kvalues = input('Blank-separated list of polynomial degrees (>= 1) [1]: ', 's');
if isempty(kvalues)
    kvalues = 1;
else
    kvalues = str2num(kvalues); %#ok<ST2NM>
end

eoa = input('Extra order of accuracy for quadrature formulas [0]: ');
if isempty(eoa)
   eoa = 0; 
end

compressquad = input('Compress 2D quadrature rules? 0/1 [0]: ');
if isempty(compressquad)
    compressquad = 0;
end

basis_type = input('Type of basis functions: 1) Hitchhicker   2) Hitchhicker scaled. Choice [1]: ');
if isempty(basis_type)
    basis_type = 1;
end

dofsLayout = input('Layout of degrees of freedom: 1) standard; 2) clusterized [1]: ');
if isempty(dofsLayout)
    dofsLayout = 1;
end

use_mp = input('Use multiprecision toolbox (0/1) [0]? ');
if isempty(use_mp)
    use_mp = 0;
end

if use_mp
    addpath('./../../AdvanpixMCT-4.3.6.12354')
    precision = input('Set default precision: ');
    mp.Digits(precision);
    k = mp(k);
end

if isempty(gcp('nocreate'))
    parpool;
end

%%%%%%%%%%%%%%
% OFF meshes %
%%%%%%%%%%%%%%

meshRoot = '../Mesh_Handlers/meshfiles2d/voronoi-notnested';

meshNames = {
%     'randVoro__Nelt_000100__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'randVoro__Nelt_002500__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'randVoro__Nelt_005000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
    'randVoro__Nelt_010000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
    'randVoro__Nelt_020000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
    'randVoro__Nelt_040000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
    'randVoro__Nelt_080000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
    'randVoro__Nelt_160000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
    'randVoro__Nelt_320000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
    'randVoro__Nelt_640000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
    'randVoro__Nelt_01280000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
    };

meshExt = '.metis';
  
% meshRoot = '/home/daniele/Documents/codes/triangle/meshes/constrained_delaunay';
% 
% meshNames = {
%     'unitsquare_a1em6';
%     'unitsquare_a1em6_random_refine';
%     };

% meshRoot = '../Mesh_Handlers/meshfiles2d/voronoi-notnested/enumath2017';
% 
% meshNames = {
%     'randVoro__Nelt_010000__hmin_3e-05__minArea_1e-15__tolTotArea_1e-8__cut%02ix%02i';
%     'randVoro__Nelt_020000__hmin_2e-05__minArea_1e-15__tolTotArea_1e-8__cut%02ix%02i';
%     'randVoro__Nelt_040000__hmin_2e-05__minArea_1e-15__tolTotArea_1e-6__cut%02ix%02i';
%     'randVoro__Nelt_080000__hmin_2e-05__minArea_1e-15__tolTotArea_1e-6__cut%02ix%02i';
%     'randVoro__Nelt_160000__hmin_2e-05__minArea_1e-07__tolTotArea_1e-7__cut%02ix%02i';
%     };
% 
% meshExt = '.off';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% .mat files with mesh.Node, mesh.Element %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% meshRoot = '/home/daniele/Documents/codes/triangle/meshes/conforming_delaunay';
%
% meshNames = {
%     'unitsquare_a1em7.off.metis_nparts_00000064';
%     'unitsquare_a1em7.off.metis_nparts_00000128';
%     'unitsquare_a1em7.off.metis_nparts_00000256';
%     'unitsquare_a1em7.off.metis_nparts_00000512';
%     'unitsquare_a1em7.off.metis_nparts_00001024';
%     'unitsquare_a1em7.off.metis_nparts_00002048';
%     'unitsquare_a1em7.off.metis_nparts_00004096';
%     'unitsquare_a1em7.off.metis_nparts_00008192';
%     'unitsquare_a1em7.off.metis_nparts_00016384';
%     'unitsquare_a1em7.off.metis_nparts_00032768';
%     'unitsquare_a1em7.off.metis_nparts_00065536';
%     'unitsquare_a1em7.off.metis_nparts_00131072';
%     'unitsquare_a1em7.off.metis_nparts_00262144';
%     'unitsquare_a1em7.off.metis_nparts_00524288';
%     'unitsquare_a1em7.off.metis_nparts_01048576';
%     'unitsquare_a1em7.off.metis_nparts_02097152';
%     };

% meshRoot = '../Mesh_Handlers/meshfiles2d/voronoi-notnested';

% meshNames = {
%     'randVoro__Nelt_005000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off.metis_nparts_00000512';
%     'randVoro__Nelt_010000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off.metis_nparts_00001024';
%     'randVoro__Nelt_020000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off.metis_nparts_00002048';
%     'randVoro__Nelt_040000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off.metis_nparts_00004096';
%     'randVoro__Nelt_080000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off.metis_nparts_00008192';
%     'randVoro__Nelt_160000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off.metis_nparts_00016384';
%     'randVoro__Nelt_320000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off.metis_nparts_00032768';
%     'randVoro__Nelt_640000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off.metis_nparts_00065536';
%     'randVoro__Nelt_01280000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off.metis_nparts_00131072';
%     };

% meshRoot = '../Mesh_Handlers/meshfiles_AleRusso/escher';
%
% meshNames = {
%     'horse_002500';
%     'horse_003600';
%     'horse_004900';
%     'horse_006400';
%     'horse_008100';
%     'horse_010000';
%     'horse_012100';
%     'horse_014400';
%     'horse_016900';
%     'horse_019600';
%     'horse_002500.mat.metis_nparts_00000250';
%     'horse_003600.mat.metis_nparts_00000360';
%     'horse_004900.mat.metis_nparts_00000490';
%     'horse_006400.mat.metis_nparts_00000640';
%     'horse_008100.mat.metis_nparts_00000810';
%     'horse_010000.mat.metis_nparts_00001000';
%     'horse_012100.mat.metis_nparts_00001210';
%     'horse_014400.mat.metis_nparts_00001440';
%     'horse_016900.mat.metis_nparts_00001690';
%     'horse_019600.mat.metis_nparts_00001960';
%     };

% meshRoot = '../Mesh_Handlers/meshfiles2d/koch';
% 
% meshNames = {
%     'koch_iter_3_04x04_bndRefined_0';
%     'koch_iter_3_12x12_bndRefined_0';
%     'koch_iter_3_16x16_bndRefined_0';
%     'koch_iter_3_20x20_bndRefined_0';
%     'koch_iter_3_24x24_bndRefined_0';
%     'koch_iter_3_28x28_bndRefined_0';
%     'koch_iter_3_32x32_bndRefined_0';
%     'koch_iter_3_36x36_bndRefined_0';
%     'koch_iter_3_40x40_bndRefined_0';
% };

% meshExt = '.metis';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% meshRoot = '../Mesh_Handlers/meshfiles_AleRusso/esagoni-regolari';
% 
% meshNames = {
%     'esagoni0100x0100';
%     'esagoni0200x0200';
%     'esagoni0300x0300';
%     'esagoni0400x0400';
%     'esagoni0500x0500';
%     'esagoni0600x0600';
%     'esagoni0700x0700';
%     'esagoni0800x0800';
%     'esagoni0900x0900';
%     'esagoni1000x1000';
%     };

% meshExt = '.mat';
% meshExt = '.metis';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

NxVect    = [64 128 256 512 1024];
NyVect    = ones(size(NxVect));
saveData  = 1;
i_rho     = 3;
isCutMesh = 1;

for k = kvalues
    for iN = 1:length(NxVect)
        try
            filename = sprintf('./workspaces/rho_type_%i_%i_%i.mat', i_rho, NxVect(iN), NyVect(iN));
            load(filename);
        catch
            % Diffusion coefficient
            %
            % Beware that if stiffness matrices are loaded, rho will be
            % loaded too later. This is useful in the case of random
            % coefficients in order to ensure repeatability of results.
            switch i_rho
                case 1
                    rho = [];
                case 2
                    rho = rand(NxVect(iN), NyVect(iN));
                case 3
                    alpha = randi([-5 5], NxVect(iN), NyVect(iN));
                    rho = 10.^alpha;
                case 4
                    rho = 1e-5*ones(NxVect(iN), NyVect(iN));
                    for is = 1:NxVect(iN)
                        for js = 1:NyVect(iN)
                            if mod(is+js,2) == 1
                                rho(is,js) = 1e5;
                            end
                        end
                    end
                otherwise
                    error('Unknown option')
            end

            if saveData ~= 0
                filename = sprintf('./workspaces/rho_type_%i_%i_%i.mat', i_rho, NxVect(iN), NyVect(iN));
                save(filename, 'rho', '-v7.3');
            end
        end

        for i = 1:length(meshNames)

            meshname = [ meshRoot '/' meshNames{i} ];

            fprintf('Loading mesh %s ... ', meshname);
            if strcmp(meshExt, '.off')
                if isCutMesh
                    meshname = sprintf(meshname, NxVect(iN), NyVect(iN));
                    mesh = readOFFmesh([meshname '.off']);
                    mesh = loadmesh(mesh, use_mp);

                    %
                    % Need to read subdomain flags in the proper order
                    %

                    Hx = 1.0/NxVect(iN); Hy = 1.0/NyVect(iN);

                    numSubdomains    = NxVect(iN)*NyVect(iN);
                    NeltArray        = zeros(numSubdomains,1);

                    fid = fopen([meshname '.off.domains.txt']);
                    for E = 1:mesh.Nelt
                        subdomainIdx = fscanf(fid, '%d', 1) + 1;
                        NeltArray(subdomainIdx) = NeltArray(subdomainIdx) + 1;
                    end
                    fclose(fid);

                    elementArray   = cell( numSubdomains, 1 );
                    elementIdArray = cell( numSubdomains, 1 );
                    for s = 1:numSubdomains
                        elementArray{s}   = cell( NeltArray(s), 1 );
                        elementIdArray{s} = zeros( NeltArray(s), 1 );
                    end

                    NeltArray(:) = 0;

                    fid = fopen([meshname '.off.domains.txt']);
                    for E = 1:mesh.Nelt
                        subdomainIdx = fscanf(fid, '%d', 1) + 1; % Indices in Livesu's files start from 0
                        NeltArray(subdomainIdx) = NeltArray(subdomainIdx) + 1;

                        NvLoc = mesh.elements(E,1);
                        elementArray{subdomainIdx}{NeltArray(subdomainIdx)} = ...
                            mesh.elements(E,2:NvLoc+1);
                        elementIdArray{subdomainIdx}(NeltArray(subdomainIdx)) = E;
                    end
                    fclose(fid);

                    % Create subdomain mesh data structures with local (subdomain) numbering
                    mesh.subdomainId = zeros(mesh.Nelt,1);
                    for s = 1:numSubdomains
                        [C, ~, IC]  = unique( horzcat( elementArray{s}{:} ) );
                        Node = mesh.coordinates( C, : );

                        % Set the correct subdomain index
                        center = sum( Node, 1 ) / size(Node, 1);
                        iUB = []; jUB = [];
                        for is = 1:NxVect(iN)
                            if is*Hx > center(1)
                                iUB = is;
                                break;
                            end
                        end
                        for js = 1:NyVect(iN)
                            if js*Hy > center(2)
                                jUB = js;
                                break;
                            end
                        end
                        subdomainIdx            = ( jUB - 1 )*NxVect(iN) + iUB;
                        mesh.subdomainId(elementIdArray{subdomainIdx}) = subdomainIdx;
                    end
                else
                    mesh = readOFFmesh([meshname '.off']); %#ok<UNRCH>
                    mesh = loadmesh(mesh, use_mp);
                end
            elseif strcmp(meshExt, '.metis')
%                 mesh = loadmesh([meshname '.mat'], use_mp);
                mesh = readOFFmesh([meshname '.off']);
                mesh = loadmesh(mesh, use_mp);
                mesh.subdomainId = load([meshname '.off.metis.epart.' num2str(NxVect(iN))]) + 1;
            else
                mesh = loadmesh([meshname '.mat'], use_mp);
            end
            fprintf('done\n');

            fprintf('Assembling matrix and rhs ... ');
            [~, ~, ~, ~, S, F] = ...
                VEM_d_parallel(mesh, k, eoa, compressquad, basis_type, use_mp, i_F, rho, dofsLayout);
            fprintf('done\n');

            fprintf('Saving data to binary file ... ');

            filename = sprintf('%s__i_rho_%i__i_F_%i__k_%02i__basis_%i__L_%i__S_rhs.bin', ...
                               meshname, i_rho, i_F, k, basis_type, NxVect(iN)*NyVect(iN));

            F     = full(F);
            normF = norm(F);

            PetscBinaryWrite(filename, S, F, normF);

            fprintf('done\n');
        end
    end
end
