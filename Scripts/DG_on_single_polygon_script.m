%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

addpath('../Basis_Functions');
addpath('../Mesh_Handlers');
addpath('../Quadrature');
addpath('../Quadrature/cpp_modules');
addpath('../VEM_diffusion');
addpath('../DG_polygons');

%
% Inputs from the user
%
use_mp = input('Use multiprecision toolbox (0/1)? ');
if use_mp
    addpath('../../AdvanpixMCT-4.3.6.12354')
    precision = input('Set default precision: '); mp.Digits(precision);

    k = mp(input('Polynomial degree: '));
    basis_type = input('Type of 2D basis functions: 1) Hitchhicker 2) Hitchhicker scaled: ');
    t = mp(input('Type of stabilization (-1, 0, 1): '));
    if t == 0
        alpha_K = 0;
        Stype  = 2; PNs_K = []; P0s_K = []; S_K = []; ref_area_K = []; alpha1 = 0; alpha2 = 0;
        ftype  = 1;
    else
        alpha_K = mp( input('Scaling factor for the stabilization term, alpha_K: ', 's') );
        Stype  = input('Stabilization matrix: (1) load a precomputed matrix; (2) compute it locally. Choice: ');
        if Stype == 1
            filename = input('MAT file with precomputed stabilization: ', 's');
            data     = load(filename);
            PNs_K    = mp( data.PNs ); P0s_K = mp( data.P0s );
            S_K      = mp( data.S ); ref_area_K = mp( data.ref_area_K);
            alpha1 = 0; alpha2 = 0;
        elseif Stype == 2
            PNs_K  = []; P0s_K = []; S_K = []; ref_area_K = [];
            alpha1 = mp( input('First multiplying factor for the stabilization term: ', 's') );
            alpha2 = mp( input('Second multiplying factor for the stabilization term: ', 's') );
        end

        ftype = input(['Type of stabilization for the load term: ' ...
                       '(1) \Pi^0_{k+2}; ' ...
                       '(2) \Pi^0_{k+1}; ' ...
                       '(3) \Pi^0_{k}; '...
                       '(4) \Pi^\\nabla_{k+2}. Choice: ']);
    end
else
    k = input('Polynomial degree: ');
    basis_type = input('Type of 2D basis functions: 1) Hitchhicker 2) Hitchhicker scaled: ');
    t = input('Type of stabilization (-1, 0, 1): ');
    if t == 0
        alpha_K = 0;
        Stype  = 2; PNs_K = []; P0s_K = []; S_K = []; ref_area_K = []; alpha1 = 0; alpha2 = 0;
        ftype  = 1;
    else
        alpha_K = input('Scaling factor for the stabilization term, alphaK: ');
        Stype  = input('Stabilization matrix: (1) load a precomputed matrix; (2) compute it locally. Choice: ');
        if Stype == 1
            filename = input('MAT file with precomputed stabilization: ', 's');
            data     = load(filename);
            PNs_K = data.PNs;
            P0s_K = data.P0s;
            S_K   = data.S;
            ref_area_K = data.ref_area_K;
            alpha1 = 0; alpha2 = 0;
        elseif Stype == 2
            PNs_K  = []; P0s_K = []; S_K = []; ref_area_K = [];
            alpha1 = input('First multiplying factor for the stabilization term: ');
            alpha2 = input('Second multiplying factor for the stabilization term: ');
        end

        ftype = input(['Type of stabilization for the load term: ' ...
                       '(1) \Pi^0_{k+2}; ' ...
                       '(2) \Pi^0_{k+1}; ' ...
                       '(3) \Pi^0_{k}; '...
                       '(4) \Pi^\\nabla_{k+2}. Choice: ']);
    end    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Assemble the DG system on a single polygon

meshroot = '../Mesh_Handlers/meshfiles_AleRusso';
% meshname = 'poligono/quadrato';
meshname = 'poligono/horse';
meshname = [meshroot '/' meshname];

fprintf('Loading mesh %s...', meshname);
mesh = loadmesh(meshname, use_mp);
fprintf('loaded\n');

%
% Gauss-Lobatto rule with k+3 nodes
%
[xqd1, wqd1] = lobatto(k+3, use_mp);
formula1d    = [(1.-xqd1)/2. (xqd1+1.)/2. wqd1/2.];

basis1d = legendre_on_unit_interval(formula1d(:,2), k);

%
% Compute geometrical parameters
%
% We need to use enough 2d quadrature points to compute the VEM stiffness
% matrix of degree k+2 later.
eoa          = 0;
compressquad = 0;
[ ~, ~, xqd_K, yqd_K, wqd_K, Nx_K, Ny_K, h_K, area_K, xb, yb ] = ...
    build_coordinate_matrices_parallel( k+2, eoa, compressquad, ...
                                        mesh.elements, mesh.coordinates, formula1d, use_mp );

[basis_on_quad_pts2d, grad_basis_on_quad_pts2d] = ...
    monomial_basis(xqd_K, yqd_K, k+2, xb, yb, h_K, ones(size(xqd_K)));
basis_on_quad_pts2d = permute(basis_on_quad_pts2d, [1, 3, 2]);
grad_basis_on_quad_pts2d = permute(grad_basis_on_quad_pts2d, [1, 3, 4, 2]);

if basis_type == 2
    basisL2norm         = sqrt( wqd_K' * basis_on_quad_pts2d.^2 );
    basis_on_quad_pts2d = bsxfun(@times, 1./basisL2norm, basis_on_quad_pts2d);
    grad_basis_on_quad_pts2d = bsxfun(@times, 1./basisL2norm, grad_basis_on_quad_pts2d);
end

%
% Load term - call function from VEM_diffusion folder
%
f_K = loadcal_DG(xqd_K, yqd_K, use_mp);

%
% Run the local solver
%
if basis_type == 1
    [ A, B, F, ~, Atilde, Btilde, Ftilde, Z, G, xbnd, ybnd, edges_lengths ] = ...
        local_solver_DG( k, mesh.coordinates, mesh.elements, mesh.perm, ...
                         xb, yb, h_K, area_K, Nx_K, Ny_K, ...
                         formula1d, basis1d, ...
                         wqd_K, basis_on_quad_pts2d, grad_basis_on_quad_pts2d, ...
                         t, alpha_K, Stype, PNs_K, P0s_K, S_K, ref_area_K, ...
                         alpha1, alpha2, ftype, f_K );
elseif basis_type == 2
    [ A, B, F, ~, Atilde, Btilde, Ftilde, Z, G, xbnd, ybnd, edges_lengths ] = ...
        local_solver_DG( k, mesh.coordinates, mesh.elements, mesh.perm, ...
                         xb, yb, h_K, area_K, Nx_K, Ny_K, ...
                         formula1d, basis1d, ...
                         wqd_K, basis_on_quad_pts2d, grad_basis_on_quad_pts2d, ...
                         t, alpha_K, Stype, PNs_K, P0s_K, S_K, ref_area_K, ...
                         alpha1, alpha2, ftype, f_K, ...
                         basisL2norm );
end

%
% Dirichlet boundary conditions
%
Udir = dirdata_DG(xbnd, ybnd, use_mp);
tmp  = bsxfun(@times, formula1d(:,3), basis1d);
Udir = bsxfun(@times, edges_lengths(2:end)', tmp' * Udir);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Solve the DG system

d2  = ((k+1)*(k+2))/2;
rhs = G + [ zeros(d2,1); Udir(:) ];

U  = Z\rhs;

u_K = U(1:d2);
lambda_K = U(d2+1:end);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Error computation

sol = exactsol_DG(xqd_K, yqd_K, use_mp);

errL2_u = sqrt( wqd_K' * (sol - basis_on_quad_pts2d(:,1:d2)*u_K).^2 );

fprintf('L^2 norm of the error for u: %e\n', errL2_u);






