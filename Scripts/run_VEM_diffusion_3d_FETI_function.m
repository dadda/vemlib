
addpath ../Mesh_Handlers
addpath ../Quadrature
addpath ../Quadrature/cpp_modules
addpath ../Basis_Functions
addpath ../VEM_diffusion
addpath ../VEM_diffusion_3D
addpath ../FETI3D

%
% Test 1
%
% We keep the dimension of the local problems, and H/h, fixed, and increase
% the number of subdomains and thus the overall problem size. Thus, we can
% use the same reference mesh and increase Nx, Ny, Nz.
%
%
% Test 2
%
% We fix the number of subdomains and increase the dimension of the local
% subdomain problems to see the polylogarithmic growth in the condition
% number of the preconditioned system.
%

testName = 'Test 2 With Octahedral Meshes And Checkerboard Data';

% refMeshNames = {
%     './../Mesh_Handlers/meshfiles3d/hexahedra/cubes_Nelt000064.mat';
%     './../Mesh_Handlers/meshfiles3d/hexahedra/cubes_Nelt000216.mat';
%     './../Mesh_Handlers/meshfiles3d/hexahedra/cubes_Nelt000512.mat';
%     './../Mesh_Handlers/meshfiles3d/hexahedra/cubes_Nelt001728.mat';
%     './../Mesh_Handlers/meshfiles3d/hexahedra/cubes_Nelt004096.mat';
%     './../Mesh_Handlers/meshfiles3d/hexahedra/cubes_Nelt008000.mat';
%     './../Mesh_Handlers/meshfiles3d/hexahedra/cubes_Nelt013824.mat';
%     './../Mesh_Handlers/meshfiles3d/hexahedra/cubes_Nelt021952.mat';
%     './../Mesh_Handlers/meshfiles3d/hexahedra/cubes_Nelt027000.mat';
%     './../Mesh_Handlers/meshfiles3d/hexahedra/cubes_Nelt032768.mat';    
%     };

refMeshNames = {
    './../Mesh_Handlers/meshfiles3d/lattices/bcc_lattice_04.mat';
    './../Mesh_Handlers/meshfiles3d/lattices/bcc_lattice_06.mat';
    './../Mesh_Handlers/meshfiles3d/lattices/bcc_lattice_08.mat';
    './../Mesh_Handlers/meshfiles3d/lattices/bcc_lattice_10.mat';
    './../Mesh_Handlers/meshfiles3d/lattices/bcc_lattice_12.mat';
    './../Mesh_Handlers/meshfiles3d/lattices/bcc_lattice_14.mat';
    './../Mesh_Handlers/meshfiles3d/lattices/bcc_lattice_16.mat';
    './../Mesh_Handlers/meshfiles3d/lattices/bcc_lattice_18.mat';
    };

% refMeshNames = {
% %     './../Mesh_Handlers/meshfiles3d/voronoi/cube/unitcube_voro_v02_Nelt000032.mat';
%     './../Mesh_Handlers/meshfiles3d/voronoi/cube/unitcube_voro_v02_Nelt000064.mat';
%     './../Mesh_Handlers/meshfiles3d/voronoi/cube/unitcube_voro_v02_Nelt000128.mat';
%     './../Mesh_Handlers/meshfiles3d/voronoi/cube/unitcube_voro_v02_Nelt000256.mat';
%     './../Mesh_Handlers/meshfiles3d/voronoi/cube/unitcube_voro_v02_Nelt000512.mat';
%     './../Mesh_Handlers/meshfiles3d/voronoi/cube/unitcube_voro_v02_Nelt001024.mat';
%     './../Mesh_Handlers/meshfiles3d/voronoi/cube/unitcube_voro_v02_Nelt002048.mat';
%     './../Mesh_Handlers/meshfiles3d/voronoi/cube/unitcube_voro_v02_Nelt004096.mat';
%     './../Mesh_Handlers/meshfiles3d/voronoi/cube/unitcube_voro_v02_Nelt008192.mat';
%     };

eoa          = 0;
compressquad = 0;
NxVect       = 6; %[2 4 6 8 10 12];
NyVect       = 6; %[2 4 6 8 10 12];
NzVect       = 6; %[2 4 6 8 10 12];
i_rho        = 4;
% alphaVect    = 2:2:12;
i_F          = 2;
isVertexPrimalVect   = [1 0 0 1 1 0 1];
isEdgePrimalVect     = [0 1 0 1 0 1 1];
isFacePrimalVect     = [0 0 1 0 1 1 1];
approach4constraints = 1;
use_pc      = 1;
saveData    = 1;
savePCGiter = 1;
tol         = 1e-12;
maxit       = 500;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%
% Run the test
%

filename = input('Enter name of log file: ', 's');
fid = fopen(filename, 'w');

logFilePath = strsplit( filename, '/' );
filenameLastTest = strjoin( horzcat( logFilePath(1:end-1), ...
                                     strjoin( horzcat( strsplit( testName ), ...
                                                       '_numLastTest.txt' ), ...
                                              '' ) ), '/' );

runAsFor = input('Run parfor as for loop? 0/1 [0]: ');
if isempty(runAsFor)
    runAsFor = 0;
end

if runAsFor == 0
    if isempty(gcp('nocreate'))
        parpool;
    end
else
    delete(gcp('nocreate'));
end

fprintf(fid, '********************************************************\n');
fprintf(fid, '* FETI-DP for solving the 3D Poisson equation with VEM *\n');
fprintf(fid, '********************************************************\n\n');

fprintf(fid, '*********************\n');
fprintf(fid, '* %s\n', testName);
fprintf(fid, '*********************\n\n');

fprintf('%s\n', testName);
fprintf('------\n');

testCounter = 0;

for iA = 1:length(isVertexPrimalVect)
    for iN = 1:length(NxVect)
        for iR = 1:length(i_rho)

            try
                filename = sprintf('../FETI3D/workspaces/rho_idx_%i_rho_type_%i_Nx_%i_Ny_%i_Nz_%i.mat', ...
                    iR, i_rho(iR), NxVect(iN), NyVect(iN), NzVect(iN));
                load(filename);
                fprintf(fid, '\nDiffusion coefficient loaded\n');
            catch
                % Diffusion coefficient
                %
                % Beware that if loadKF == 1, loading the corresponding file
                % will cause the variable rho to be overwritten,
                % in order to ensure repeatability of the results in the case
                % of random coefficients.
                switch i_rho(iR)
                    case 1
                        rho = ones(NxVect(iN), NyVect(iN), NzVect(iN));
                    case 2
                        rho = rand(NxVect(iN), NyVect(iN), NzVect(iN));
                    case 3
                        alpha = randi([-5 5], NxVect(iN), NyVect(iN), NzVect(iN));
                        rho = 10.^alpha;
                    case 4
                        rho = 1e-5*ones(NxVect(iN), NyVect(iN), NzVect(iN));
                        for is = 1:NxVect(iN)
                            for js = 1:NyVect(iN)
                                for ks = 1:NzVect(iN)
                                    if mod(is+js+ks,2) == 1
                                        rho(is, js, ks) = 1e5;
                                    end
                                end
                            end
                        end
                    case 5
                        rho = ones(NxVect(iN), NyVect(iN), NzVect(iN));
                        rho(3:end-2,3:end-2,3:end-2) = 10^alphaVect(iR);
                    case 6
                        alpha = randi([-alphaVect(iR) alphaVect(iR)], ...
                            NxVect(iN), NyVect(iN), NzVect(iN));
                        rho = 10.^alpha;
                end
                fprintf(fid, '\nDiffusion coefficient created\n');

                if saveData ~= 0
                    filename = sprintf('../FETI3D/workspaces/rho_idx_%i_rho_type_%i_Nx_%i_Ny_%i_Nz_%i.mat', ...
                        iR, i_rho(iR), NxVect(iN), NyVect(iN), NzVect(iN));
                    save(filename, 'rho', '-v7.3');
                end
            end

            for iM = 1:length(refMeshNames)
                testCounter = testCounter + 1;

                fprintf(fid, '******************** Start of %s, iM = %d, iR = %d, iN = %d, iA = %d\n\n', testName, iM, iR, iN, iA);

                fprintf(['Running %s with mesh %d, diffusion coefficient %d, Nx/Ny/Nz = %d/%d/%d, ' ...
                         'primal vertices/edges/faces = %d/%d/%d\n'], ...
                         testName, iM, iR, NxVect(iN), NyVect(iN), NzVect(iN), ...
                         isVertexPrimalVect(iA), isEdgePrimalVect(iA), isFacePrimalVect(iA) );

                doTest = false;

                try
                    fidLastTest = fopen(filenameLastTest);
                    numLastTest = fscanf(fidLastTest, '%d', 1);
                    fclose(fidLastTest);

                    if numLastTest >= testCounter
                        % Do not do test
                        fprintf(fid, 'Test already performed\n\n');
                    else
                        doTest = true;
                    end
                catch
                    doTest = true;
                end

                if doTest
                    % Do test and rewrite last test file
                    VEM_diffusion_3d_FETI_function( fid, refMeshNames{iM}, eoa, compressquad, runAsFor, ...
                                                    NxVect(iN), NyVect(iN), NzVect(iN), ...
                                                    iR, i_rho(iR), rho, i_F, ...
                                                    isVertexPrimalVect(iA), isEdgePrimalVect(iA), isFacePrimalVect(iA), ...
                                                    approach4constraints, use_pc, ...
                                                    saveData, savePCGiter, ...
                                                    tol, maxit );

                    % Update last test performed
                    fidLastTest = fopen( filenameLastTest, 'w' );
                    fprintf(fidLastTest, '%d\n', testCounter);
                    fclose(fidLastTest);
                end

                fprintf(fid, '******************** End of %s, iM = %d, iR = %d, iN = %d, iA = %d\n\n', testName, iM, iR, iN, iA);
            end
        end
    end
end

fprintf(fid, '\nEnd of %s\n', testName);
fprintf(fid, '*************\n\n');

fclose(fid);
disp('Done')
