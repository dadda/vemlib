%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Convergence test for DG-VEM for Poisson equation

addpath('../Basis_Functions');
addpath('../Mesh_Handlers');
addpath('../Quadrature');
addpath('../Quadrature/cpp_modules');
addpath('../VEM_diffusion');
addpath('../DG_polygons');

fprintf('\nConvergence test for DG-VEM for Poisson equation\n\n');

%
% Inputs from the user
%
k          = input('Polynomial degree: ');
basis_type = input('Type of 2D basis functions: 1) Hitchhicker 2) Hitchhicker scaled: ');
eoa = input('Extra order of accuracy for quadrature formulas: ');
compressquad = input('Compress 2D quadrature rules (0/1)? ');
t = input('Type of stabilization (-1, 0, 1): ');
if t == 0
    alpha  = 0;
    SType  = 2;
    alpha1 = 0;
    alpha2 = 0;
    fType  = 1;
else
    alpha  = input('Scaling factor alpha for the (-1)-stab term: ');
    SType  = input(['Stabilization matrix: ' ...
                    '(1) compute it on few reference elements; ' ...
                    '(2) compute it element by element. Choice: ']);
    alpha1 = input('First multiplying factor for the stabilization term: ');
    alpha2 = input('Second multiplying factor for the stabilization term: ');
    fType      = input(['Stabilization for the load term: ' ...
                       '(1) \Pi^0_{k+2}; ' ...
                       '(2) \Pi^0_{k+1}; ' ...
                       '(3) \Pi^0_{k}; '...
                       '(4) \Pi^\\nabla_{k+2}. Choice: ']);
end

use_mp = input('Use multiprecision toolbox (0/1)? ');
if use_mp
    warning('Quadrature formula compression will be disabled');    
    addpath('../../AdvanpixMCT-4.3.6.12354')
    precision = input('Set default precision: ');
    mp.Digits(precision);
    k = mp(k); t = mp(t);
    alpha = mp(alpha); alpha1 = mp(alpha1); alpha2 = mp(alpha2);
    compressquad = 0;
end

if license('test','Distrib_Computing_Toolbox')
    if isempty(gcp)
        parpool
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% meshroot = './../Mesh_Handlers/meshfiles_AleRusso';

% % QUADRATI REGOLARI
% 
% meshnames = {'quadrati-regolari/quadrati1x1r';
%              'quadrati-regolari/quadrati1x1';
%              'quadrati-regolari/quadrati2x2';
%              'quadrati-regolari/quadrati4x4';
%              'quadrati-regolari/quadrati8x8';
%              'quadrati-regolari/quadrati10x10';
%              'quadrati-regolari/quadrati16x16';
%              'quadrati-regolari/quadrati20x20';
%              'quadrati-regolari/quadrati30x30';
%              'quadrati-regolari/quadrati32x32';
%              'quadrati-regolari/quadrati40x40';
%              'quadrati-regolari/quadrati50x50';
%              'quadrati-regolari/quadrati64x64';
%              'quadrati-regolari/quadrati80x80';
%              'quadrati-regolari/quadrati100x100'};

% % ESAGONI (QUASI) REGOLARI
% 
% meshnames = {'esagoni-regolari/esagoni8x10';
% 	         'esagoni-regolari/esagoni18x20';
%              'esagoni-regolari/esagoni26x30';
%              'esagoni-regolari/esagoni34x40';
%              'esagoni-regolari/esagoni44x50';
%              'esagoni-regolari/esagoni52x60';
%              'esagoni-regolari/esagoni60x70';
%              'esagoni-regolari/esagoni70x80'};

% % ESAGONI DEFORMATI
% 
% meshnames = {'esagoni-deformati/esagoni8x10d';
%              'esagoni-deformati/esagoni18x20d';
%              'esagoni-deformati/esagoni26x30d';
%              'esagoni-deformati/esagoni34x40d';
%              'esagoni-deformati/esagoni44x50d';
%              'esagoni-deformati/esagoni52x60d';
%              'esagoni-deformati/esagoni60x70d';
%              'esagoni-deformati/esagoni70x80d'};

% % POLIGONI DI VORONOI CON CONTROL POINTS RANDOM
% 
% meshnames = {'poligoni-random/poligoni10';
%             'poligoni-random/poligoni100';
%             'poligoni-random/poligoni200';
%             'poligoni-random/poligoni300';
%             'poligoni-random/poligoni400';
%             'poligoni-random/poligoni500';
%             'poligoni-random/poligoni600';
%             'poligoni-random/poligoni700';
%             'poligoni-random/poligoni800';
%             'poligoni-random/poligoni900';
%             'poligoni-random/poligoni1000'};
%             'poligoni-random/poligoni1200';
%             'poligoni-random/poligoni1400';
%             'poligoni-random/poligoni2000'};
%             'poligoni-random/poligoni3000';
%             'poligoni-random/poligoni4000';
%             'poligoni-random/poligoni5000';
%             'poligoni-random/poligoni10000'};
%           'poligoni-random/poligoni20000'};

% NESTED VORONOI MESHES GENERATED WITH POLYMESHER

meshroot = './../Mesh_Handlers/meshfiles2d/';

% meshnames = {'voronoi-nested/regularized_MaxIter10_hmin_controlled_000016';
%              'voronoi-nested/regularized_MaxIter10_hmin_controlled_000064';
%              'voronoi-nested/regularized_MaxIter10_hmin_controlled_000256';
%              'voronoi-nested/regularized_MaxIter10_hmin_controlled_001024';
%              'voronoi-nested/regularized_MaxIter10_hmin_controlled_004093'};

% meshnames = {'voronoi-nested/regularized_MaxIter10_hmin_controlled_v2_000016';
%              'voronoi-nested/regularized_MaxIter10_hmin_controlled_v2_000064';
%              'voronoi-nested/regularized_MaxIter10_hmin_controlled_v2_000256';
%              'voronoi-nested/regularized_MaxIter10_hmin_controlled_v2_000994';
%              'voronoi-nested/regularized_MaxIter10_hmin_controlled_v2_003922';
%              'voronoi-nested/regularized_MaxIter10_hmin_controlled_v2_014938'};

% meshnames = {'voronoi-nested/regularized_MaxIter10_hmin_controlled_v3_000016';
%              'voronoi-nested/regularized_MaxIter10_hmin_controlled_v3_000064';
%              'voronoi-nested/regularized_MaxIter10_hmin_controlled_v3_000256';
%              'voronoi-nested/regularized_MaxIter10_hmin_controlled_v3_001024';
%              'voronoi-nested/regularized_MaxIter10_hmin_controlled_v3_004096'};

meshnames = {'voronoi-nested/regularized_MaxIter10_hmin_controlled_v4_000016';
             'voronoi-nested/regularized_MaxIter10_hmin_controlled_v4_000064';
             'voronoi-nested/regularized_MaxIter10_hmin_controlled_v4_000256';
             'voronoi-nested/regularized_MaxIter10_hmin_controlled_v4_001024';
             'voronoi-nested/regularized_MaxIter10_hmin_controlled_v4_004078';
             'voronoi-nested/regularized_MaxIter10_hmin_controlled_v4_015976'};

nbmeshes   = length(meshnames);
relerrL2_u = zeros([1 nbmeshes]);
relerrH1_u = zeros([1 nbmeshes]);
hmax       = zeros([1 nbmeshes]);

for i = 1:nbmeshes
    meshname = [meshroot '/' meshnames{i}];
    
    fprintf('Loading mesh %s...', meshname);
    mesh = loadmesh(meshname, use_mp);
    fprintf('loaded\n');

    [ ~, ~, ~, relerrL2_u(i), ~, relerrH1_u(i), h] = ...
        DG_on_polygons( mesh, k, basis_type, eoa, compressquad, ...
                        t, alpha, SType, alpha1, alpha2, fType, use_mp );
    
    hmax(i) = max(h);
end

fprintf('\nRelative error for u in the L^2 norm\n');

fprintf('------------------------------------\n');
format shorte
disp(relerrL2_u)

fprintf('Order of convergence:\n')
orderL2_u = log(relerrL2_u(1:end-1)./relerrL2_u(2:end)) ./ log(hmax(1:end-1)./hmax(2:end));
format bank
disp(orderL2_u)

fprintf('\nRelative error for u in the H^1 norm\n');

fprintf('------------------------------------\n');
format shorte
disp(relerrH1_u)

fprintf('Order of convergence:\n')
orderH1_u = log(relerrH1_u(1:end-1)./relerrH1_u(2:end)) ./ log(hmax(1:end-1)./hmax(2:end));
format bank
disp(orderH1_u)

