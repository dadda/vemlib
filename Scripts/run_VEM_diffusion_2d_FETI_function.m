
addpath ../Basis_Functions
addpath ../Mesh_Handlers
addpath ../Quadrature
addpath ../Quadrature/cpp_modules
addpath ../VEM_diffusion
addpath ../FETI
addpath ../FETI3D

%
% Specify test inputs
%

testName = 'Test 1';

meshNames = {
%     '../Mesh_Handlers/meshfiles2d/voronoi-notnested/enumath2017/randVoro__Nelt_010000__hmin_3e-05__minArea_1e-15__tolTotArea_1e-8__cut%02dx%02d.off';
    '../Mesh_Handlers/meshfiles2d/voronoi-notnested/enumath2017/randVoro__Nelt_020000__hmin_2e-05__minArea_1e-15__tolTotArea_1e-8__cut%02dx%02d.off';
%     '../Mesh_Handlers/meshfiles2d/voronoi-notnested/enumath2017/randVoro__Nelt_040000__hmin_2e-05__minArea_1e-15__tolTotArea_1e-6__cut%02dx%02d.off';
%     '../Mesh_Handlers/meshfiles2d/voronoi-notnested/enumath2017/randVoro__Nelt_080000__hmin_2e-05__minArea_1e-15__tolTotArea_1e-6__cut%02dx%02d.off';
%     '../Mesh_Handlers/meshfiles2d/voronoi-notnested/enumath2017/randVoro__Nelt_160000__hmin_2e-05__minArea_1e-07__tolTotArea_1e-7__cut%02dx%02d.off'
    };
% meshNames = { '../Mesh_Handlers/meshfiles_AleRusso/esagoni-regolari/esagoni8x10.mat' };
% meshNames = { '../Mesh_Handlers/meshfiles_AleRusso/poligoni-random/poligoni100.mat' };

% Specify how subdomain meshes are created:
% - 0: create subdomain meshes from a reference mesh
% - 1: load subdomain meshes from OFF file generated via Livesu's package
subdomainMeshApproach = 1;

kVect        = 1;
eoa          = 0;
compressquad = 0;
basis_type   = 2;
use_mp       = 0;
mp_precision = 20;
NxVect       = [4 8 12 16];
NyVect       = [4 8 12 16];
i_rho        = 1;
gamma        = 1;
i_F          = 1;
use_pc       = 1;
saveData     = 1;
savePCGiter  = 1;
tol          = 1e-6;
maxit        = 1000;

%
% Run the test
%

filename = input('Enter name of log file: ', 's');
fid = fopen(filename, 'w');

logFilePath = strsplit( filename, '/' );
filenameLastTest = strjoin( horzcat( logFilePath(1:end-1), ...
                                     strjoin( horzcat( strsplit( testName ), ...
                                                       '_numLastTest.txt' ), ...
                                              '' ) ), '/' );

runAsFor = input('Run parfor as for loop? 0/1 [0]: ');
if isempty(runAsFor)
    runAsFor = 0;
end

if runAsFor == 0
    if isempty(gcp('nocreate'))
        parpool;
    end
else
    delete(gcp('nocreate'));
end

fprintf(fid, '********************************************************\n');
fprintf(fid, '* FETI-DP for solving the 2D Poisson equation with VEM *\n');
fprintf(fid, '********************************************************\n\n');

fprintf(fid, '**********\n');
fprintf(fid, '* %s *\n', testName);
fprintf(fid, '**********\n\n');

fprintf('%s\n', testName);
fprintf('------\n');

testCounter = 0;

for k = kVect
    for iN = 1:length(NxVect)
        
        try
            filename = sprintf('../FETI/workspaces/rho_type_%i_%i_%i.mat', i_rho, NxVect(iN), NyVect(iN));
            load(filename);
            fprintf(fid, '\nDiffusion coefficient loaded\n');
        catch
            % Diffusion coefficient
            %
            % Beware that if stiffness matrices are loaded, rho will be
            % loaded too later. This is useful in the case of random
            % coefficients in order to ensure repeatability of results.
            switch i_rho
                case 1
                    rho = ones(NxVect(iN), NyVect(iN));
                case 2
                    rho = rand(NxVect(iN), NyVect(iN));
                case 3
                    alpha = randi([-5 5], NxVect(iN), NyVect(iN));
                    rho = 10.^alpha;
                otherwise
                    error('Unknown option')
            end
            fprintf(fid, '\nDiffusion coefficient created\n');
            
            if saveData ~= 0
                filename = sprintf('../FETI/workspaces/rho_type_%i_%i_%i.mat', i_rho, NxVect(iN), NyVect(iN));
                save(filename, 'rho', '-v7.3');
            end
        end

        for iM = 1:length(meshNames)
            testCounter = testCounter + 1;

            fprintf(fid, '******************** Start of %s, iM = %d, iN = %d, k = %d\n\n', testName, iM, iN, k);

            fprintf(['Running %s with mesh %d, Nx/Ny = %d/%d, ' ...
                     'degree of VEM basis = %d\n'], ...
                     testName, iM, NxVect(iN), NyVect(iN), k );

            doTest = false;

            try
                fidLastTest = fopen(filenameLastTest);
                numLastTest = fscanf(fidLastTest, '%d', 1);
                fclose(fidLastTest);
                
                if numLastTest >= testCounter
                    % Do not do test
                    fprintf(fid, 'Test already performed\n\n');
                else
                    doTest = true;
                end
            catch
                doTest = true;
            end
            
            if doTest
                if subdomainMeshApproach == 1
                    meshName = sprintf( meshNames{iM}, NxVect(iN), NyVect(iN) );
                else
                    meshName = meshNames{iM};
                end
                
                % Do test and update last test file
                VEM_diffusion_2d_FETI_function( fid, meshName, subdomainMeshApproach, ...
                                                k, eoa, compressquad, basis_type, use_mp, mp_precision, ...
                                                NxVect(iN), NyVect(iN), ...
                                                i_rho, rho, gamma, i_F, ...
                                                use_pc, ...
                                                saveData, savePCGiter, ...
                                                tol, maxit, runAsFor );
                
                % Update last test performed
                fidLastTest = fopen( filenameLastTest, 'w' );
                fprintf(fidLastTest, '%d\n', testCounter);
                fclose(fidLastTest);
            end

            fprintf(fid, '******************** End of %s, iM = %d, iN = %d, k = %d\n\n', testName, iM, iN, k);
        end
    end
end

fprintf(fid, '\nEnd of %s\n', testName);
fprintf(fid, '*************\n\n');

fclose(fid);
disp('Done')
