function arg = getParforArg( varargin )
% Helper function to choose the NumWorkers argument to PARFOR

p = gcp('nocreate');

if nargin == 1
    runAsFor = varargin{1};
    if runAsFor == 0 && isempty(p)
        parpool;
    end
    p = gcp('nocreate');
end

if isempty(p)
  arg = 0;
else
  arg = p.NumWorkers;
end

end
