function [ ] = VEM_diffusion_3d_FETI_function( fid, ...
                                               refMeshName, ...
                                               eoa, compressquad, ...
                                               runAsFor, ...
                                               Nx, Ny, Nz, ...
                                               iR, i_rho, rho, i_F, ...
                                               isVertexPrimal, isEdgePrimal, isFacePrimal, ...
                                               approach4constraints, use_pc, ...
                                               saveData, savePCGiter, ...
                                               tol, maxit )

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check inputs

% Polynomial degree
k = 1;

if isempty(eoa)
   eoa = 0;
end

if isempty(compressquad)
    compressquad = 0;
end

% Type of basis functions
basis_type = 1;

% Multi-precision toolbox
use_mp = 0;

%
% FETI-DP  options
%

if isempty(Nx) || (Nx < 2)
    Nx = 2;
end

if isempty(Ny) || (Ny < 2)
    Ny = 2;
end

if isempty(Nz) || (Nz < 2)
    Nz = 2;
end

if isempty(isVertexPrimal)
    isVertexPrimal = 1;
end

if isempty(isEdgePrimal)
    isEdgePrimal = 1;
end

if isempty(isFacePrimal)
    isFacePrimal = 0;
end

if (isVertexPrimal + isEdgePrimal + isFacePrimal) == 0
    error('The primal space cannot be empty')
end

if isempty(approach4constraints)
    approach4constraints = 2;
end

if isempty(use_pc)
    use_pc = 1;
end

if isempty(saveData)
    saveData = 0;
end

if isempty(savePCGiter)
    savePCGiter = 0;
end

%
%
%

fprintf(fid, 'Inputs\n');
fprintf(fid, '******\n\n');
fprintf(fid, 'Reference mesh file: %s\n', refMeshName);
fprintf(fid, 'Degree of VEM space: %i\n', k);
fprintf(fid, 'Extra order of accuracy for quadrature formulas: %i\n', eoa);
fprintf(fid, 'Compress 2D quadrature rules: %i\n', compressquad);
fprintf(fid, 'Type of basis functions: %i\n', basis_type);
fprintf(fid, 'Use multiprecision toolbox: %i\n', use_mp);

fprintf(fid, '\n');

fprintf(fid, 'Number of subdomains in x, y, and z directions: %i %i %i\n', Nx, Ny, Nz);
fprintf(fid, 'Diffusion coefficient type: %i\n', i_rho);

fprintf(fid, 'Use primal vertices: %i\n', isVertexPrimal);
fprintf(fid, 'Use primal edges: %i\n', isEdgePrimal);
fprintf(fid, 'Use primal faces: %i\n', isFacePrimal);
fprintf(fid, 'Approach for implementing primal edge/face constraints: %i\n', approach4constraints);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create domain decomposition data structures

fprintf(fid, '\nCreating domain decomposition data structures...\n');

% Reference mesh
load(refMeshName, 'mesh')

mshName = strsplit(refMeshName, {'./../Mesh_Handlers/meshfiles3d/', '/', '.mat'});
mshName = strjoin(mshName(2:end), '_');

try
    % Try to load subdomains stiffness matrices and right hand sides
    filename = sprintf(['../FETI3D/workspaces/%s__k_%i__Nx_%i_Ny_%i_Nz_%i__' ...
                        'rho_idx_%i__rho_type_%i__eoa_%i__compressquad_%i__use_mp_%i.mat'], ...
                        mshName, k, Nx, Ny, Nz, iR, i_rho, eoa, compressquad, use_mp );
    load(filename);
    fprintf(fid, 'Mesh array loaded\n');
    fprintf(fid, 'Subdomain stiffness matrices and load terms loaded\n');
catch
    % Identify internal and boundary nodes on the reference mesh
    [ refVertexDofs, refEdgeInternalDofs, refFaceInternalDofs, ...
      refInternalDofs ] = createPreliminaryDofPartition( mesh, k );

    % Group element faces by interface face
    refElementFacesByBoundary = groupFacesByBoundary( mesh );

    % Replicate reference mesh to create a domain decomposition
    fprintf(fid, 'Replicating reference mesh ... ');
    meshArray = repMesh( mesh, Nx, Ny, Nz, ...
                         refVertexDofs, refEdgeInternalDofs, refFaceInternalDofs, ...
                         refElementFacesByBoundary );
    fprintf(fid, 'done\n');

    % Identify degrees of freedom on the Dirichlet boundary
    meshArray = ...
        findDirichletDofs( meshArray, Nx, Ny, Nz );

    % Assemble subdomain stiffness matrices and right hand sides
    fprintf(fid, 'Assembling subdomain stiffness matrices and load terms ... ');
    [ K, F, hmaxBySubdomain ] = ...
        subdomainAssembler( meshArray, Nx, Ny, Nz, rho, i_F, ...
                            k, eoa, compressquad, use_mp, runAsFor );
    fprintf(fid, 'done\n');
 
    infoK = whos('K');
    
    % Do not save variables if K is larger than 16 GB
    if ( saveData ~= 0 ) && ( infoK.bytes <= 16*8*1024^3)
        filename = sprintf(['../FETI3D/workspaces/%s__k_%i__Nx_%i_Ny_%i_Nz_%i__' ...
                            'rho_idx_%i__rho_type_%i__eoa_%i__compressquad_%i__use_mp_%i.mat'], ...
                            mshName, k, Nx, Ny, Nz, iR, i_rho, eoa, compressquad, use_mp );
        save(filename, 'refVertexDofs', 'refEdgeInternalDofs', ...
                       'refFaceInternalDofs', 'refInternalDofs', ...
                       'refElementFacesByBoundary', ...
                       'meshArray', 'K', 'F', 'hmaxBySubdomain', '-v7.3');
    end
end

% Compute dimension of the global primal space and of the space of Lagrange
% multipliers \lambda
[ dimPrimal, dimLambda ] = ...
    spacesDim( meshArray, Nx, Ny, Nz, isVertexPrimal, isEdgePrimal, isFacePrimal );

try
    % Try to load primal and dual boolean operators and transformation
    % matrices
    filename = sprintf( ['../FETI3D/workspaces/%s__k_%i__Nx_%i_Ny_%i_Nz_%i__' ...
                         'rho_idx_%i__rho_type_%i__eoa_%i__compressquad_%i__use_mp_%i__' ...
                         'VEF_%i%i%i.mat'], ...
                         mshName, k, Nx, Ny, Nz, iR, i_rho, eoa, compressquad, use_mp, ...
                         isVertexPrimal, isEdgePrimal, isFacePrimal );
    load(filename);
    fprintf(fid, 'Boolean operators and transformation matrices loaded\n');
catch
    % Construct primal and dual boolean operators and transformation matrices
    fprintf(fid, 'Constructing primal and dual boolean operators, and transformation matrices ... ');
    [ vertexPrimalDofs, vertexDualDofs, ...
      edgeAverageDofs, edgeAverageDofsLocal, edgeInternalDualDofs, ...
      faceAverageDofs, faceAverageDofsLocal, faceInternalDualDofs, ...
      Tvertex, ...
      Qedge, Tedge, ...
      Qface, Tface, ...
      Bprimal, Bdual, BdualScaled ] = ...
        subdomainOperators( meshArray, k, eoa, compressquad, use_mp, ...
                            Nx, Ny, Nz, ...
                            isVertexPrimal, isEdgePrimal, isFacePrimal, ...
                            dimPrimal, dimLambda, ...
                            rho, K );
    fprintf(fid, 'done\n');

    if saveData ~= 0
        filename = sprintf( ['../FETI3D/workspaces/%s__k_%i__Nx_%i_Ny_%i_Nz_%i__' ...
                             'rho_idx_%i__rho_type_%i__eoa_%i__compressquad_%i__use_mp_%i__' ...
                             'VEF_%i%i%i.mat'], ...
                             mshName, k, Nx, Ny, Nz, iR, i_rho, eoa, compressquad, use_mp, ...
                             isVertexPrimal, isEdgePrimal, isFacePrimal );
        save(filename, 'vertexPrimalDofs', 'vertexDualDofs', ...
                       'edgeAverageDofs', 'edgeAverageDofsLocal', 'edgeInternalDualDofs', ...
                       'faceAverageDofs', 'faceAverageDofsLocal', 'faceInternalDualDofs', ...
                       'Tvertex', 'Qedge', 'Tedge', 'Qface', 'Tface', ...
                       'Bprimal', 'Bdual', 'BdualScaled', '-v7.3');
    end
end

% Explicitly apply the change of basis if needed
if approach4constraints == 1
    fprintf(fid, 'Applying explicit change of basis ... ');
    [ K, F ] = applyTransform( meshArray, Nx, Ny, Nz, ...
                               isVertexPrimal, isEdgePrimal, isFacePrimal, ...
                               Tvertex, Tedge, Tface, K, F, runAsFor );
    fprintf(fid, 'done\n');
end

try
    % Try to load dual primal operators
    filename = sprintf( ['../FETI3D/workspaces/%s__k_%i__Nx_%i_Ny_%i_Nz_%i__' ...
                         'rho_idx_%i__rho_type_%i__eoa_%i__compressquad_%i__use_mp_%i__' ...
                         'VEF_%i%i%i__approach_%i.mat'], ...
                         mshName, k, Nx, Ny, Nz, iR, i_rho, eoa, compressquad, use_mp, ...
                         isVertexPrimal, isEdgePrimal, isFacePrimal, ...
                         approach4constraints );
    load(filename);
    fprintf(fid, 'Kstar, fstar, and d loaded\n');
catch
    % Assemble some terms of the dual-primal system (i.e. with \lambda and u_c
    % as unknowns)
    fprintf(fid, 'Assembling Kstar, fstar, dr ... ');
    [ Kstar, fstar, dr ] = assembleDualPrimalOperators( meshArray, k, use_mp, ...
                                                        Nx, Ny, Nz, ...
                                                        isEdgePrimal, isFacePrimal, ...
                                                        dimPrimal, dimLambda, ...
                                                        approach4constraints, ...
                                                        refInternalDofs, ...
                                                        vertexPrimalDofs, vertexDualDofs, ...
                                                        edgeAverageDofs, edgeAverageDofsLocal, edgeInternalDualDofs, ...
                                                        faceAverageDofs, faceAverageDofsLocal, faceInternalDualDofs, ...
                                                        Qedge, Tedge, ...
                                                        Qface, Tface, ...
                                                        Bprimal, Bdual, K, F, runAsFor );
    fprintf(fid, 'done\n');

    % Compute right hand side for the dual interface problem
    tmp = Kstar\fstar;
    d = dr - actionFIrc( tmp, meshArray, k, use_mp, Nx, Ny, Nz, ...
                         isEdgePrimal, isFacePrimal, dimLambda, approach4constraints, ...
                         refInternalDofs, vertexPrimalDofs, vertexDualDofs, ...
                         edgeAverageDofs, edgeAverageDofsLocal, edgeInternalDualDofs, ...
                         faceAverageDofs, faceAverageDofsLocal, faceInternalDualDofs, ...
                         Qedge, Tedge, Qface, Tface, Bprimal, Bdual, K, runAsFor );

    if saveData ~= 0
        filename = sprintf( ['../FETI3D/workspaces/%s__k_%i__Nx_%i_Ny_%i_Nz_%i__' ...
                             'rho_idx_%i__rho_type_%i__eoa_%i__compressquad_%i__use_mp_%i__' ...
                             'VEF_%i%i%i__approach_%i.mat'], ...
                             mshName, k, Nx, Ny, Nz, iR, i_rho, eoa, compressquad, use_mp, ...
                             isVertexPrimal, isEdgePrimal, isFacePrimal, ...
                             approach4constraints );
        save(filename, 'Kstar', 'fstar', 'd', '-v7.3');
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Solution with FETI-DP

if isempty(tol)
    tol = 1e-6;
end

if isempty(maxit)
    maxit = min(dimLambda, 100);
end

try
    % Try to load PCG solution
    filename = sprintf( ['../FETI3D/workspaces/%s__k_%i__Nx_%i_Ny_%i_Nz_%i__' ...
                         'rho_idx_%i__rho_type_%i__eoa_%i__compressquad_%i__use_mp_%i__' ...
                         'VEF_%i%i%i__approach_%i__tol_%e__maxit_%i__use_pc_%i.mat'], ...
                         mshName, k, Nx, Ny, Nz, iR, i_rho, eoa, compressquad, use_mp, ...
                         isVertexPrimal, isEdgePrimal, isFacePrimal, ...
                         approach4constraints, tol, maxit, use_pc );
    load(filename);
    fprintf(fid, 'PCG solution loaded\n');
catch
    fprintf(fid, ['Solving the dual interface problem with PCG ' ...
                  '(tolerance = %e, maximum number of iterations = %i)...\n'], tol, maxit);
    [ lambda, ~, iter, relResVec, eigEst ] = ...
        pcgFETIDP( d, tol, maxit, use_pc, meshArray, k, use_mp, Nx, Ny, Nz, ...
                   isVertexPrimal, isEdgePrimal, isFacePrimal, dimPrimal, dimLambda, ...
                   approach4constraints, ...
                   refInternalDofs, vertexPrimalDofs, vertexDualDofs, ...
                   edgeAverageDofs, edgeAverageDofsLocal, edgeInternalDualDofs, ...
                   faceAverageDofs, faceAverageDofsLocal, faceInternalDualDofs, ...
                   Qedge, Tedge, Qface, Tface, ...
                   Bprimal, Bdual, BdualScaled, K, Kstar, fid, runAsFor, ...
                   savePCGiter, mshName, iR, i_rho, eoa, compressquad );

    if saveData ~= 0
        filename = sprintf( ['../FETI3D/workspaces/%s__k_%i__Nx_%i_Ny_%i_Nz_%i__' ...
                             'rho_idx_%i__rho_type_%i__eoa_%i__compressquad_%i__use_mp_%i__' ...
                             'VEF_%i%i%i__approach_%i__tol_%e__maxit_%i__use_pc_%i.mat'], ...
                             mshName, k, Nx, Ny, Nz, iR, i_rho, eoa, compressquad, use_mp, ...
                             isVertexPrimal, isEdgePrimal, isFacePrimal, ...
                             approach4constraints, tol, maxit, use_pc );
        save(filename, 'lambda', 'iter', 'relResVec', 'eigEst', '-v7.3');
    end
end

%
% Compute vector of the primal dofs
%
uPrimal = Kstar \ ( fstar + actionFIrcT( lambda, ...
                                         meshArray, k, use_mp, ...
                                         Nx, Ny, Nz, isEdgePrimal, isFacePrimal, ...
                                         dimPrimal, approach4constraints, ...
                                         refInternalDofs, vertexPrimalDofs, vertexDualDofs, ...
                                         edgeAverageDofs, edgeAverageDofsLocal, edgeInternalDualDofs, ...
                                         faceAverageDofs, faceAverageDofsLocal, faceInternalDualDofs, ...
                                         Qedge, Tedge, Qface, Tface, Bprimal, Bdual, K, runAsFor ) );

%
% Reconstruct the solution in each subdomain
%
fprintf(fid, 'Reconstructing the solution in each subdomain ... ');
u = reconstructSolution( lambda, uPrimal, meshArray, k, use_mp, Nx, Ny, Nz, ...
                         isEdgePrimal, isFacePrimal, approach4constraints, ...
                         refInternalDofs, vertexPrimalDofs, vertexDualDofs, ...
                         edgeAverageDofs, edgeAverageDofsLocal, edgeInternalDualDofs, ...
                         faceAverageDofs, faceAverageDofsLocal, faceInternalDualDofs, ...
                         Qedge, Tedge, Qface, Tface, Bprimal, Bdual, K, F, runAsFor );
fprintf(fid, 'done\n');

%
% Error computation
%
% Compute errors only if the load term has been computed analytically.
%
if i_F == 1 && i_rho == 1
    sol = exactSolution( meshArray, Nx, Ny, Nz, k, use_mp, ...
                         isVertexPrimal, isEdgePrimal, isFacePrimal, ...
                         approach4constraints, Tvertex, Tedge, Tface, runAsFor );

    [ relerrEnergy, relerrInf ] = computeErrors( [Nx, Ny, Nz], sol, u, K, runAsFor );
end

%
% Report
%
[ numSubdomains, hmaxInv, Hoh, numDofs ] = ...
    computeStatistics( meshArray, [Nx, Ny, Nz], hmaxBySubdomain, u );
fprintf(fid, '\nResults\n');
fprintf(fid, '*******\n\n');
fprintf(fid, 'Number of subdomains: %i\n', numSubdomains);
fprintf(fid, '1/h: %e\n', hmaxInv);
fprintf(fid, 'H/h: %e\n', Hoh);
fprintf(fid, 'D.o.f.: %i\n', numDofs);
fprintf(fid, 'Dimension of coarse space: %i\n', length(uPrimal) );
fprintf(fid, 'Minimum eigenvalue: %e\n', eigEst(1));
fprintf(fid, 'Maximum eigenvalue: %e\n', eigEst(2));
fprintf(fid, 'Number of PCG iterations: %i\n', iter);
if i_F == 1 && i_rho == 1
    fprintf(fid, 'Relative error energy norm: %e\n', relerrEnergy);
    fprintf(fid, 'Relative error inf norm: %e\n', relerrInf);
end
fprintf(fid, 'Vector of the estimated residual norms:\n');
for i = 1:iter
    fprintf(fid, '%e\n', relResVec(i));
end

fprintf(fid, '\n\n');

end
