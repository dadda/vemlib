%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VEM for the Poisson equation in 3D - Convergence test

addpath ../Mesh_Handlers
addpath ../Quadrature
addpath ../Quadrature/cpp_modules
addpath ../Basis_Functions
addpath ../VEM_diffusion
addpath ../VEM_diffusion_3D

fprintf('\nConvergence test for the Poisson equation\n\n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% User-required inputs

k            = input('Polynomial degree (>= 1): ');
eoa          = input('Extra order of accuracy for quadrature formulas: ');
compressquad = input('Compress 2D quadrature rules (0/1)? ');
basis_type   = input('Type of basis functions:\n(1) mu_alpha = ((x-xd)/hd)^alpha\n(2) mu_alpha/||mu_alpha||_L2\nChoose a basis: ');
if basis_type == 2
    error('Still need to implement scaled monomial functions in 3D')
end
use_mp       = input('Use multiprecision toolbox (0/1)? ');
if use_mp == 1
    error('Still need to implement Advanpix support')
end

if license('test','Distrib_Computing_Toolbox')
    if isempty(gcp)
        parpool
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Choose a mesh

meshroot = './../Mesh_Handlers/meshfiles3d';

% Cubes
meshnames = {'hexahedra/cubes_Nelt000008';
             'hexahedra/cubes_Nelt000064';
             'hexahedra/cubes_Nelt000512';
             'hexahedra/cubes_Nelt004096'};
%              'hexahedra/cubes_Nelt032768'};
% 
% % Hexahedra
% meshnames = {'hexahedra/hexa_Nelt000008';
%              'hexahedra/hexa_Nelt000064';
%              'hexahedra/hexa_Nelt000512';
%              'hexahedra/hexa_Nelt004096';
%              'hexahedra/hexa_Nelt032768'};
% 
% % Tetrahedra
% meshnames = {'tetrahedra/tetra_Nelt000006';
%              'tetrahedra/tetra_Nelt000048';
%              'tetrahedra/tetra_Nelt000384';
%              'tetrahedra/tetra_Nelt003072';
%              'tetrahedra/tetra_Nelt024576';
%              'tetrahedra/tetra_Nelt196608'};
% 
% % Voronoi meshes of the cube [-1,1] x [-1,1] x [-1,1]
% meshnames = {'voronoi/cube/cube_Nelt000051';
%              'voronoi/cube/cube_Nelt000284';
%              'voronoi/cube/cube_Nelt001859';
%              'voronoi/cube/cube_Nelt013365'};
%         
% % Voronoi from particles randomly inserted in [0,1]^3
% meshnames = {'voronoi/cube/unitcube_voro_Nelt8';
%              'voronoi/cube/unitcube_voro_Nelt64';
%              'voronoi/cube/unitcube_voro_Nelt512';
%              'voronoi/cube/unitcube_voro_Nelt4096'};
%             'voronoi/cube/unitcube_voro_Nelt32768'}; -> CAUSES OUT OF
%             MEMORY IN THE LINEAR SOLVER

nbmeshes = length(meshnames);
errS     = zeros(nbmeshes, 1);
errinf   = errS;
errL2    = errS;
hEmax    = errS;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Solve the problem and collect errors

% Uncomment when using the Parallel toolbox
%parpool

for i = 1:nbmeshes
    meshname = [meshroot '/' meshnames{i}];
    load([meshname '.mat'])
    fprintf('Loaded mesh %s\n', meshname);

    [~, errS(i), errinf(i), errL2(i), elements_diameters] = VEM_diffusion_3d(mesh, k, eoa, compressquad, basis_type, use_mp);
    hEmax(i) = max(elements_diameters);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Print errors

fprintf('\nEnergy norm of the errors:\n');
fprintf('--------------------------\n');
format shorte
disp(errS)

fprintf('Order of convergence:\n')
orderS = log(errS(1:end-1)./errS(2:end))./log(hEmax(1:end-1)./hEmax(2:end));
format bank
disp(orderS)

fprintf('\nInf norm of the errors:\n');
fprintf('-------------------------\n');
format shorte
disp(errinf)

fprintf('Order of convergence:\n')
orderinf = log(errinf(1:end-1)./errinf(2:end))./log(hEmax(1:end-1)./hEmax(2:end));
format bank
disp(orderinf)

fprintf('\nL2 norm of the errors:\n');
fprintf('------------------------\n');
format shorte
disp(errL2)

fprintf('Order of convergence:\n')
orderL2 = log(errL2(1:end-1)./errL2(2:end))./log(hEmax(1:end-1)./hEmax(2:end));
format bank
disp(orderL2)
