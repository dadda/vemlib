%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Solve the Poisson equation with the VEM

% clear
% close all
% clc

addpath('./../Basis_Functions');
addpath('./../Mesh_Handlers');
addpath('./../Quadrature');
addpath('../Quadrature/cpp_modules');
addpath('./../VEM_diffusion');

fprintf('\nScript for solving the Poisson equation in 2D with the VEM\n\n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% User-required inputs

i_F = input('Enter type of load term: (1) exact, (2) uniform distribution in [-1,1]. Choice [1]: ');
if isempty(i_F)
    i_F = 1;
end

dofsLayout = input('Layout of degrees of freedom: 1) standard; 2) clusterized [1]: ');
if isempty(dofsLayout)
    dofsLayout = 1;
end

k = input('Polynomial degree (>= 1) [1]: ');
if isempty(k)
    k = 1;
end

eoa = input('Extra order of accuracy for quadrature formulas [0]: ');
if isempty(eoa)
   eoa = 0;
end

compressquad = input('Compress 2D quadrature rules? 0/1 [0]: ');
if isempty(compressquad)
    compressquad = 0;
end

basis_type = input('Type of basis functions: 1) Hitchhicker   2) Hitchhicker scaled. Choice [1]: ');
if isempty(basis_type)
    basis_type = 1;
end

use_mp = input('Use multiprecision toolbox (0/1) [0]? ');
if isempty(use_mp)
    use_mp = 0;
end

if use_mp
    addpath('./../../AdvanpixMCT-4.4.4.12666')
    precision = input('Set default precision: ');
    mp.Digits(precision);
    k = mp(k);
end

if isempty(gcp('nocreate'))
    parpool;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Problem solving

%%% Choose a mesh

meshroot = '../Mesh_Handlers';

% meshname = 'meshfiles_AleRusso/poligono/quadrato';
% meshname = 'meshfiles_AleRusso/quadrati-regolari/quadrati10x10';
% meshname = 'meshfiles_AleRusso/esagoni-regolari/esagoni04x05';
% meshname = 'meshfiles_AleRusso/esagoni-regolari/esagoni8x10';
% meshname = 'meshfiles_AleRusso/esagoni-deformati/esagoni52x60d';
% meshname = 'meshfiles_AleRusso/poligoni-random/poligoni1000';

% meshname = 'meshfiles2d/voronoi-mvtnorm/mu_025_025__B_1_m1_1_1__D_0p30_0p15_000800';
% meshname = 'meshfiles_AleRusso/escher/horse_000400';
% meshname = 'meshfiles2d/koch/koch_iter3_12x12';
% meshname = 'meshfiles2d/koch/koch_iter3_12x12_bndRefined';

% meshname = 'meshfiles2d/genova/98_with_canvas';
% meshname = 'meshfiles2d/genova/100_with_canvas';
% meshname = 'meshfiles2d/genova/76_with_canvas';
% meshname = 'meshfiles2d/genova/u_like_0.000000_with_canvas';

% meshname = 'meshfiles2d/genova/maze_1.000000_with_canvas';
% meshname = 'meshfiles2d/genova/u_like_1.000000_with_canvas';
% meshname = 'meshfiles2d/genova/zeta_1.000000_with_canvas';
meshname = 'meshfiles2d/genova/CANVAS/1_with_canvas';

% meshExt = '.off';
% meshExt = '.mat';
meshExt = ''; % .node, .ele mesh

meshname = [meshroot '/' meshname meshExt];
fprintf('Loading mesh %s...\n', meshname);
if strcmp(meshExt, '.off')
    mesh = readOFFmesh(meshname);
    mesh = loadmesh(mesh, 0);
elseif strcmp(meshExt, '')
    mesh = readNodeEle(meshname);
    mesh = loadmesh(mesh, 0);
elseif strcmp(meshExt, '.mat')
    mesh = loadmesh(meshname, 0);
end
fprintf('...mesh %s loaded\n', meshname);

%
%
%

[U, errH1, errL2, ~, ~, ~, ~, ~, kappa] = ...
    VEM_d_parallel(mesh, k, eoa, compressquad, basis_type, use_mp, i_F, [], dofsLayout);
      
fprintf('H^1 norm of the error: %e\n', errH1);
fprintf('L^2 norm of the error: %e\n', errL2);
fprintf('1-norm condition number estimate: %e\n', kappa);

%
% 
%

figure()
hold on;
set(gcf, 'color', [1 1 1]);
for E = 1:mesh.Nelt
    NvLoc = mesh.elements(E,1);
    x = mesh.coordinates(mesh.elements(E,2:NvLoc+1), 1);
    y = mesh.coordinates(mesh.elements(E,2:NvLoc+1), 2);
    patch('XData', x, 'YData', y, 'ZData', U(mesh.elements(E,2:NvLoc+1)), 'CData', U(mesh.elements(E,2:NvLoc+1)), 'FaceColor', 'interp');
end
