%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VEM for the minimal surface problem - convergence test

addpath ../Basis_Functions
addpath ../Mesh_Handlers
addpath ../Quadrature
addpath ../Quadrature/cpp_modules
addpath ../VEM_diffusion
addpath ../VEM_minimal_surface

fprintf('\nVEM for the minimal surface problem - convergence test\n\n');

bc = input(['Boundary conditions:\n' ...
            '0) x+y;\n' ...
            '1) Schwarz D surface from [0,1]^2;\n' ...
            '2) Cantor function on the four sides of [0,1]^2;\n' ...
            '3) Minimal surface on the unit disk (see https://it.mathworks.com/help/pde/examples/minimal-surface-problem-on-the-unit-disk.html);\n' ...
            '4) Example on [0.25, 0.75]^2 from P.Concus paper;\n' ...
            '5) Catenoid on a sector;\n' ...
            '6) Scherk fifth surface on [-0.8, 0.8]^2.\n\n' ...
            'Choice: ']);

rtol = input('Relative tolerance on increments [1e-9]: ');
if isempty(rtol)
    rtol = 1e-9;
end

atol = input('Absolute tolerance on increments [1e-16]: ');
if isempty(atol)
    atol = 1e-16;
end

maxit = input('Maximum number of fixed-point iterations [10000]: ');
if isempty(maxit)
    maxit = 10000;
end

solver = input(['Choose solver for VEM meshes:\n' ...
                '1) UMFPACK;\n' ...
                '2) AGMG.\n\n' ...
                'Choice [1]: ']);
if isempty(solver)
    solver = 1;
end
if solver == 2
    % AGMG
    addpath '/home/daniele/Documents/codes/AGMG_3.3.0-aca/Matlab'
end

plotSol = input('Plot solution (0/1) [0]: ');
if isempty(plotSol)
    plotSol = 0;
end

if isempty(gcp('nocreate'))
    parpool;
end

%
% Meshes
%

% meshRoot = '../Mesh_Handlers/meshfiles2d/voronoi-notnested';

% meshNames = {
% %     'randVoro__Nelt_000100__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'randVoro__Nelt_002500__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'randVoro__Nelt_005000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'randVoro__Nelt_010000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'randVoro__Nelt_020000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'randVoro__Nelt_040000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'randVoro__Nelt_080000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'randVoro__Nelt_160000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'randVoro__Nelt_320000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
% %     'randVoro__Nelt_640000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     };
% 
% meshNameShort = 'square';

% meshNames = {
%     'notnested_MaxIter100_002048';
%     'notnested_MaxIter100_004096';
%     'notnested_MaxIter100_008192';
%     'notnested_MaxIter100_016384';
%     'notnested_MaxIter100_032768';
%     'notnested_MaxIter100_065536';
%     'notnested_MaxIter100_131072';
%     'notnested_MaxIter100_262144';
% %     'notnested_MaxIter100_524288';
%     };
% 
% meshNameShort = 'u-square';

% meshNames = {
%     'unitsquare025075_002048_hmin_1e-10_MaxIter_000';
%     'unitsquare025075_004096_hmin_1e-10_MaxIter_000';
%     'unitsquare025075_008192_hmin_1e-10_MaxIter_000';
%     'unitsquare025075_016384_hmin_1e-10_MaxIter_000';
%     'unitsquare025075_032768_hmin_1e-10_MaxIter_000';
%     'unitsquare025075_065536_hmin_1e-10_MaxIter_000';
%     'unitsquare025075_131072_hmin_1e-10_MaxIter_000';
%     'unitsquare025075_262144_hmin_1e-10_MaxIter_000';
%     };
% 
% meshNameShort = 'concus';

% meshNames = {
%     'unitsquare025075_002048_hmin_1e-10_MaxIter_100';
%     'unitsquare025075_004096_hmin_1e-10_MaxIter_100';
%     'unitsquare025075_008192_hmin_1e-10_MaxIter_100';
%     'unitsquare025075_016384_hmin_1e-10_MaxIter_100';
%     'unitsquare025075_032768_hmin_1e-10_MaxIter_100';
%     'unitsquare025075_065536_hmin_1e-10_MaxIter_100';
%     'unitsquare025075_131072_hmin_1e-10_MaxIter_100';
%     'unitsquare025075_262144_hmin_1e-10_MaxIter_100';
%     };
% 
% meshNameShort = 'u-concus';

% meshNames = {
%     'unitsquare0808__Nelt_002048__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'unitsquare0808__Nelt_004096__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'unitsquare0808__Nelt_008192__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'unitsquare0808__Nelt_016384__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'unitsquare0808__Nelt_032768__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'unitsquare0808__Nelt_065536__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'unitsquare0808__Nelt_131072__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'unitsquare0808__Nelt_262144__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     };
% 
% meshNameShort = 'scherk';

% meshNames = {
%     'unitsquare0808__Nelt_002048__hmin_1e-10__MaxIter_100';
%     'unitsquare0808__Nelt_004096__hmin_1e-10__MaxIter_100';
%     'unitsquare0808__Nelt_008192__hmin_1e-10__MaxIter_100';
%     'unitsquare0808__Nelt_016384__hmin_1e-10__MaxIter_100';
%     'unitsquare0808__Nelt_032768__hmin_1e-10__MaxIter_100';
%     'unitsquare0808__Nelt_065536__hmin_1e-10__MaxIter_100';
%     'unitsquare0808__Nelt_131072__hmin_1e-10__MaxIter_100';
%     'unitsquare0808__Nelt_262144__hmin_1e-10__MaxIter_100';
%     };
% 
% meshNameShort = 'u-scherk';

% meshRoot = '../Mesh_Handlers/meshfiles2d/voronoi-notnested/cantor';
% 
% meshNames = {
%     'cantor_level_04_a1em3_marked';
%     'cantor_level_04_a5em4_marked';
%     'cantor_level_04_a1em4_marked';
%     'cantor_level_04_a5em5_marked';
%     'cantor_level_04_a1em5_marked';
%     };
% 
% meshNameShort = 'd-cantor';

% meshRoot = '../Mesh_Handlers/meshfiles_AleRusso/esagoni-deformati';
% 
% meshNames = {
%     'esagoni18x20d';
%     'esagoni26x30d';
%     'esagoni34x40d';
%     'esagoni44x50d';
%     'esagoni52x60d';
%     'esagoni60x70d';
%     'esagoni70x80d'
%     };
% 
% meshNameShort = 'hexa';

% meshRoot = '../Mesh_Handlers/meshfiles2d/unitcircle';
% 
% meshNames = {
%     'ucirc__Nelt_002048__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'ucirc__Nelt_004096__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'ucirc__Nelt_008192__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'ucirc__Nelt_016384__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'ucirc__Nelt_032768__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'ucirc__Nelt_065536__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'ucirc__Nelt_131072__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'ucirc__Nelt_262144__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     };
% 
% meshNameShort = 'circ';

% meshNames = {
%     'ucirc_002048_hmin_1e-10_MaxIter_100_corrected';
%     'ucirc_004096_hmin_1e-10_MaxIter_100_corrected';
%     'ucirc_008192_hmin_1e-10_MaxIter_100_corrected';
%     'ucirc_016384_hmin_1e-10_MaxIter_100_corrected';
%     'ucirc_032768_hmin_1e-10_MaxIter_100_corrected';
%     'ucirc_065536_hmin_1e-10_MaxIter_100_corrected';
%     'ucirc_131072_hmin_1e-10_MaxIter_100_corrected';
%     'ucirc_262144_hmin_1e-10_MaxIter_100_corrected';
%     };
% 
% meshNameShort = 'u-circ';

% meshRoot = '../Mesh_Handlers/meshfiles2d/unitcircle/dual';
% 
% meshNames = {
%     'unitcircle_a0p01.1_marked_dual_corrected';
%     'unitcircle_a0p0025.1_marked_dual_corrected';
%     'unitcircle_a0p000625.1_marked_dual_corrected';
%     'unitcircle_a0p00015625.1_marked_dual_corrected';
%     'unitcircle_a0p0000390625.1_marked_dual_corrected';
%     'unitcircle_a0p000009765625.1_marked_dual_corrected';
%     'unitcircle_a0p00000244140625.1_marked_dual_corrected';
%     };
% 
% meshNameShort = 'd-circ';

% meshRoot = '../Mesh_Handlers/meshfiles2d/sector';
% 
% meshNames = {
%    'sector__Nelt_002048__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%    'sector__Nelt_004096__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%    'sector__Nelt_008192__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%    'sector__Nelt_016384__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%    'sector__Nelt_032768__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%    'sector__Nelt_065536__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%    'sector__Nelt_131072__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%    'sector__Nelt_262144__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     };
% 
% meshNameShort = 'sector';

% meshNames = {
%     'sector_002048_hmin_1e-10_MaxIter_100_corrected';
%     'sector_004096_hmin_1e-10_MaxIter_100_corrected';
%     'sector_008192_hmin_1e-10_MaxIter_100_corrected';
%     'sector_016384_hmin_1e-10_MaxIter_100_corrected';
%     'sector_032768_hmin_1e-10_MaxIter_100_corrected';
%     'sector_065536_hmin_1e-10_MaxIter_100_corrected';
%     'sector_131072_hmin_1e-10_MaxIter_100_corrected';
%     'sector_262144_hmin_1e-10_MaxIter_100_corrected';
%     };
% 
% meshNameShort = 'u-sector';

% meshRoot = '../Mesh_Handlers/meshfiles2d/sector/dual';
% 
% meshNames = {
%     'sector_a0p05.1_marked_dual_corrected';
%     'sector_a0p0125.1_marked_dual_corrected';
%     'sector_a0p003125.1_marked_dual_corrected';
%     'sector_a0p00078125.1_marked_dual_corrected';
%     'sector_a0p0001953125.1_marked_dual_corrected';
%     'sector_a0p000048828125.1_marked_dual_corrected';
%     'sector_a0p00001220703125.1_marked_dual_corrected';
%     };
% 
% meshNameShort = 'd-sector';

% meshExt = '.off';
% meshExt = '.mat';

nbMeshes = length(meshNames);
dofsVect = zeros(nbMeshes, 1);
errH1    = zeros(nbMeshes, 1);
errL2    = zeros(nbMeshes, 1);
hmaxVect = zeros(nbMeshes, 1);
itVect   = zeros(nbMeshes, 1);

hasAnalyticSolution = 0;

%
% Compute the FEM reference solution once for all
%

% meshRootSol     = '/home/daniele/Documents/codes/triangle/meshes/conforming_delaunay';
% meshBasenameSol = 'unitsquare_a1em7';
% meshNameSol     = [ meshRootSol '/' meshBasenameSol '.off' ];

% meshRootSol     = '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/voronoi-notnested/cantor';
% meshBasenameSol = 'cantor_level_04_a1em7';
% meshNameSol     = [ meshRootSol '/' meshBasenameSol '.off' ];

% meshRootSol     = '/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/meshfiles2d/unitcircle';
% meshBasenameSol = 'unitdisk_Vmax_5em7.1';
% meshNameSol     = [ meshRootSol '/' meshBasenameSol '.off' ];

if ~hasAnalyticSolution
    fprintf('Loading reference mesh %s ... ', meshNameSol);
    FEMmesh = readOFFmesh(meshNameSol);
    FEMmesh = loadmesh(FEMmesh, 0);
    FEMtr   = triangulation(FEMmesh.elements(:,2:4), FEMmesh.coordinates);
    fprintf('done\n');

    try
        % Try to load FEM reference solution
        filename = sprintf(['/home/daniele/Documents/codes/vem_matlab/' ...
                            'VEM_minimal_surface/workspaces/uFEM_bc_%i_reference_mesh_%s.mat'], ...
                             bc, meshBasenameSol);
        load(filename);
        fprintf('FEM reference solution loaded\n');
    catch
        fprintf('Solving the minimal surface problem with FEM to compute the reference solution ...\n');
        uFEM   = VEM_minsurf(FEMmesh, bc, rtol, atol, maxit, [], 2); % solve with AGMG
        fprintf('\n');

        filename = sprintf(['/home/daniele/Documents/codes/vem_matlab/' ...
                            'VEM_minimal_surface/workspaces/uFEM_bc_%i_reference_mesh_%s.mat'], ...
                             bc, meshBasenameSol);
        save(filename, 'uFEM', '-v7.3');
    end
else
    FEMtr = [];
    uFEM  = [];
end

for m = 1:nbMeshes

    %
    % VEM
    %

    meshName = [ meshRoot '/' meshNames{m} meshExt ];

    fprintf('Loading mesh %s ... ', meshName);
    if strcmp(meshExt, '.off')
        VEMmesh = readOFFmesh(meshName);
        VEMmesh = loadmesh(VEMmesh, 0);
    else
        VEMmesh = loadmesh(meshName, 0);
    end
    fprintf('done\n');

    Nver = VEMmesh.Nver;

    try
        % Try to load VEM solution
        filename = sprintf(['/home/daniele/Documents/codes/vem_matlab/' ...
                            'VEM_minimal_surface/workspaces/uVEM_bc_%i_mesh_%s.mat'], ...
                             bc, meshNames{m});
        load(filename);
        fprintf('VEM solution loaded\n');
    catch
        fprintf('Solving the minimal surface problem ...\n');
        [uVEM, ~, hmax, it, xqd, yqd, wqd, basis_on_qd, areas, P0s_by_ele] = ...
            VEM_minsurf(VEMmesh, bc, rtol, atol, maxit, [], solver);
        fprintf('\n');

        filename = sprintf(['/home/daniele/Documents/codes/vem_matlab/' ...
                            'VEM_minimal_surface/workspaces/uVEM_bc_%i_mesh_%s.mat'], ...
                             bc, meshNames{m});
        save(filename, 'uVEM', 'hmax', 'it', ...
                       'xqd', 'yqd', 'wqd', 'basis_on_qd', 'areas', 'P0s_by_ele', '-v7.3');
    end
    hmaxVect(m) = hmax;
    itVect(m)   = it;
    dofsVect(m) = VEMmesh.Nver;

	fprintf('Computing errors ... ');

    [ errH1(m), errL2(m) ] = compute_errors_minsurf( bc, FEMtr, uFEM, uVEM, ...
                                                     VEMmesh.Nelt, VEMmesh.coordinates, VEMmesh.elements, ...
                                                     hasAnalyticSolution, ...
                                                     xqd, yqd, wqd, basis_on_qd, areas, P0s_by_ele );

    fprintf('done\n');

    if plotSol
        figure()
        axis equal;
        set(gcf, 'color', [1 1 1]);
        plot3(VEMmesh.coordinates(:,1), VEMmesh.coordinates(:,2), uVEM, 'c.')

        figure()
        hold on;
        axis equal;
        set(gcf, 'color', [1 1 1]);
        for E = 1:VEMmesh.Nelt
            NvLoc = VEMmesh.elements(E,1);
            x = VEMmesh.coordinates(VEMmesh.elements(E,2:NvLoc+1), 1);
            y = VEMmesh.coordinates(VEMmesh.elements(E,2:NvLoc+1), 2);
%             patch('XData', x, 'YData', y, 'ZData', uVEM(VEMmesh.elements(E,2:NvLoc+1)), 'FaceColor', [1, 165/255, 0]);
            patch('XData', x, 'YData', y, 'ZData', uVEM(VEMmesh.elements(E,2:NvLoc+1)), 'CData', uVEM(VEMmesh.elements(E,2:NvLoc+1)), 'FaceColor', 'interp');
        end
        pause;
    end
end

fprintf('\nRelative error in the H^1 norm\n');
fprintf('----------------------------------\n');
format shorte
disp(errH1)

fprintf('Order of convergence:\n')
orderH1    = log(errH1(1:end-1)./errH1(2:end)) ./ log(hmaxVect(1:end-1)./hmaxVect(2:end));
orderH1bis = 2 * log(errH1(1:end-1)./errH1(2:end)) ./ log(dofsVect(2:end)./dofsVect(1:end-1));
format bank
disp(orderH1)
disp(orderH1bis)

fprintf('\nRelative error in the L^2 norm\n');
fprintf('------------------------------\n');
format shorte
disp(errL2)

fprintf('Order of convergence:\n')
orderL2    = log(errL2(1:end-1)./errL2(2:end)) ./ log(hmaxVect(1:end-1)./hmaxVect(2:end));
orderL2bis = 2 * log(errL2(1:end-1)./errL2(2:end)) ./ log(dofsVect(2:end)./dofsVect(1:end-1));
format bank
disp(orderL2)
disp(orderL2bis)

%
% Print results to a Latex table
%

for m = 1:nbMeshes
    fprintf('%s$_{%i}$', meshNameShort, m);
    fprintf('   &   %e', hmaxVect(m));
    fprintf('   &   %i', dofsVect(m));
    fprintf('   &   %i', itVect(m));
    fprintf('   &   %e', errH1(m));
    if m == 1
        fprintf('   &   {-}');
    else
        fprintf('   &   %f', orderH1bis(m-1));
    end
    fprintf('   &   %e', errL2(m));
    if m == 1
        fprintf('   &   {-}\\\\\n');
    else
        fprintf('   &   %f\\\\\n', orderL2bis(m-1));
    end
end

fprintf('\n');

%
% Print results to a figure
%

fprintf('errS\n');
fprintf('----\n');

for m = 1:nbMeshes
    fprintf('(%i, %g)\n', dofsVect(m), errH1(m));
end

fprintf('\n\n');

fprintf('errL2\n');
fprintf('-----\n');

for m = 1:nbMeshes
    fprintf('(%i, %g)\n', dofsVect(m), errL2(m));
end

fprintf('\n\n');

fprintf('N_dofs^(-1/2)\n');
fprintf('-------------\n');

for m = 1:nbMeshes
    fprintf('(%i, %g)\n', dofsVect(m), 1 / sqrt( dofsVect(m) ));
end

fprintf('\n\n');

fprintf('N_dofs^(-1)\n');
fprintf('-----------\n');

for m = 1:nbMeshes
    fprintf('(%i, %g)\n', dofsVect(m), 1 / dofsVect(m));
end

fprintf('\n\n');
