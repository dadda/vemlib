addpath ../../PolyMesher/
addpath ../Mesh_Handlers/PolyMesher_Addons/
addpath ../Quadrature

global myElementNodes BdBox

% rng('shuffle')

warning('error', 'MATLAB:voronoin:circumcenter:ColinearCoplanarPoints');
warning('error', 'MATLAB:qhullmx:InternalWarning');

num = 16;
filename = sprintf('regularized_MaxIter05_%06i.mat', num);
data = load(filename);

Element        = data.mesh.Element;
Node           = data.mesh.Node;
numOldElements = length(Element);

hmax = zeros(numOldElements, 1);
hmin = zeros(numOldElements, 1);
area = zeros(numOldElements, 1);

% Compute diameter for each element in the mesh
for K = 1:numOldElements
    vertices = [ Node(Element{K},1) Node(Element{K},2) ];
    hmax(K) = max(pdist(vertices));
    hmin(K) = min(pdist(vertices));
    area(K) = polyarea( vertices(:,1), vertices(:,2) );
end

[hmaxSorted, ix] = sort(hmax);
hmax          = hmaxSorted(end);
hmin             = min(hmin);

% Index of the last element in the sorted list that does not have to be
% refined
keepUntilMe      = find(hmaxSorted <= hmax/2);

% [sortedArea, ix] = sort(area);
% maxArea          = sortedArea(end);
% % Index of the last element in the sorted list that does not have to be
% % refined
% keepUntilMe      = find(sortedArea < maxArea/4);

if isempty(keepUntilMe)
    keepUntilMe = 0;
else
    keepUntilMe = keepUntilMe(end);
end

NElem          = input('Enter the number of subdivisions for each element to be refined: ');
numNewElements = keepUntilMe + (numOldElements-keepUntilMe) * NElem;

% Array for storing newly generated nodes and elements before
% post-processing
tmpNodeList    = cell(numOldElements, 1);
tmpElementList = cell(numOldElements, 1);

%
% First process elements that do not need to be refined
%
toBeKept = unique( horzcat( Element{ix(1:keepUntilMe)} ) );

% Array of nodes of new conforming mesh
newNodes = Node(toBeKept, :);

for K = 1:keepUntilMe
    tmpNodeList{K}    = Node;
    tmpElementList{K} = cell(1,1); tmpElementList{K}{1} = Element{ix(K)};
end

%
% Now process element that need to be refined
%
MaxIterDefault = 5;
MaxIterAfterRestart = 5;
tolerance4NodeSelection = 1e-1;
tolerance4Connectivity  = 1e-3;
scalingFactor = 1;
reltolerance4Areas = 1e-6;
maxNumRestarts = 500;

for K = keepUntilMe+1:numOldElements
    fprintf('Running the mesher on element %i...\n', K)
    myElementNodes = [ Node(Element{ix(K)},1) Node(Element{ix(K)},2) ];
    BdBox = [ min(myElementNodes(:,1)) max(myElementNodes(:,1)) ...
              min(myElementNodes(:,2)) max(myElementNodes(:,2)) ];
%     trueBdBox = [ min(myElementNodes(:,1)) max(myElementNodes(:,1)) ...
%                   min(myElementNodes(:,2)) max(myElementNodes(:,2)) ];
% 
%     myElementNodes(:,1) = (myElementNodes(:,1) - trueBdBox(1)) / (trueBdBox(2) - trueBdBox(1));
%     myElementNodes(:,2) = (myElementNodes(:,2) - trueBdBox(3)) / (trueBdBox(4) - trueBdBox(3));
% 
%     BdBox = [0 1 0 1];
    
%     Nver = length(Element{K});
% 
%     % Find mid point and use that as reference point
%     C = sum(myElementNodes,1)/Nver;
% 
%     v1 = [(myElementNodes(:,1)-C(1)) ...
%           (myElementNodes(:,2)-C(2))];
%     v2 = [(myElementNodes([2:Nver 1],1)-C(1)) ...
%           (myElementNodes([2:Nver 1],2)-C(2))];
% 
%     normals = sum( v1(:,1).*v2(:,2) - v1(:,2).*v2(:,1), 1);
%     refArea = norm(normals)/2.0;
    refArea = polyarea( myElementNodes(:,1), myElementNodes(:,2) );
    
    MaxIter = MaxIterDefault;
    numRestarts = 0;
    while true
        try
            [localNodes, localElement] = PolyMesher_modified(@voroDomain,NElem,MaxIter);
        catch error
            if strcmp(error.identifier, 'MATLAB:voronoin:circumcenter:ColinearCoplanarPoints') || ...
                strcmp(error.identifier, 'MATLAB:qhullmx:InternalWarning')
                warning('Warning issued by qhull or voronoin. Restarting PolyMesher...')
                MaxIter = MaxIterAfterRestart;
                numRestarts = numRestarts + 1;
                continue;
            end
        end
        checkArea = 0;
        for s = 1:NElem
%             Nver = length(localElement{s});
            myLocalElement = [ localNodes(localElement{s},1), ...
                               localNodes(localElement{s},2) ];

%             % Find mid point and use that as reference point
%             C = sum(myLocalElement,1)/Nver;
% 
%             v1 = [(myLocalElement(:,1)-C(1)) ...
%                   (myLocalElement(:,2)-C(2))];
%             v2 = [(myLocalElement([2:Nver 1],1)-C(1)) ...
%                   (myLocalElement([2:Nver 1],2)-C(2))];
% 
%             normals = sum( v1(:,1).*v2(:,2) - v1(:,2).*v2(:,1), 1);
%             checkArea = checkArea + norm(normals)/2.0;
            checkArea = checkArea + polyarea( myLocalElement(:,1), myLocalElement(:,2) );

%             plot([myLocalElement(:,1); myLocalElement(1,1)], ...
%                  [myLocalElement(:,2); myLocalElement(1,2)], 'g', 'Linewidth', 2)
        end
        relError = abs(refArea - checkArea)/refArea;
        if relError < reltolerance4Areas*10^(floor(numRestarts/maxNumRestarts))
            break
        else
            warning('Relative error: %f. Restarting PolyMesher...', relError)
        end
        MaxIter = MaxIterAfterRestart;
        numRestarts = numRestarts + 1;
%         else
%             Nver = length(Element{K});
%             C = sum(myElementNodes,1)/Nver;
%             localNodes = [myElementNodes; C];
%             localElement = [1:Nver;
%                             [2:Nver 1];
%                             (Nver+1)*ones(1,Nver)];
%             localElement = localElement';
%             localElement = mat2cell( localElement, ones(Nver,1), 3);
%             break
%         end
    end
    
%     localNodes(:,1) = localNodes(:,1) * (trueBdBox(2) - trueBdBox(1)) + trueBdBox(1);
%     localNodes(:,2) = localNodes(:,2) * (trueBdBox(4) - trueBdBox(3)) + trueBdBox(3);

    tmpNodeList{K} = localNodes;
    tmpElementList{K} = localElement;
    
    % Remove nodes that have already been generated
    if K > 1
        % minDlocal = min( pdist(localNodes) );
        D = pdist2(localNodes*scalingFactor, newNodes*scalingFactor);
        % toBeEliminated = find( any( D < sqrt(refArea/NElem)*tolerance4NodeSelection, 2 ) );
        % toBeEliminated = find( any( D < minDlocal * tolerance4NodeSelection, 2 ) );
        toBeEliminated = find( any( D < tolerance4NodeSelection * hmin, 2 ) );
        localNodes(toBeEliminated,:) = [];
    end
    newNodes = [ newNodes; localNodes ];
end

%
% Construct connectivity information for new conforming mesh
%

% Connectivity array of new conforming mesh
newElements = cell(numNewElements, 1);
counter = 0;

hmin = min( pdist(newNodes) );

for K = 1:numOldElements
    % Find nodes associated with element K
    for k = 1:length(tmpElementList{K})
        counter = counter + 1;
        myOldBoundary = [ tmpNodeList{K}(tmpElementList{K}{k},1) ...
                          tmpNodeList{K}(tmpElementList{K}{k},2) ];
        myOldBoundary = myOldBoundary * scalingFactor;
                      
        numNodes = length(tmpElementList{K}{k});
        iterator = [2:numNodes 1];
        Dist = -Inf;

        for v = 1:numNodes
            newDist = dLine(scalingFactor*newNodes, myOldBoundary(v,1), myOldBoundary(v,2), ...
                                      myOldBoundary(iterator(v),1), myOldBoundary(iterator(v),2));
            newDist = newDist(:,2);
            Dist = max( Dist, newDist );
        end
        
%         % Find mid point and use that as reference point
%         C = sum(myOldBoundary,1)/numNodes;
% 
%         v1 = [(myOldBoundary(:,1)-C(1)) ...
%               (myOldBoundary(:,2)-C(2))];
%         v2 = [(myOldBoundary([2:numNodes 1],1)-C(1)) ...
%               (myOldBoundary([2:numNodes 1],2)-C(2))];
% 
%         normals = sum( v1(:,1).*v2(:,2) - v1(:,2).*v2(:,1), 1);
%         refArea = norm(normals)/2.0;
        refArea = polyarea( myOldBoundary(:,1), myOldBoundary(:,2) );
        
        % Preliminar connectivity data for current element can be obtained
        % by finding the points lying on the polygon's sides
%        preliminarElement = find( abs(Dist) < sqrt(refArea/NElem)*tolerance4Connectivity );
        preliminarElement = find( abs(Dist) < hmin*tolerance4Connectivity );
        
%         [~, onpoly] = inpolygon( NewNodes(:,1), NewNodes(:,2), ...
%                                  [myOldBoundary(:,1); myOldBoundary(1,1)], ...
%                                  [myOldBoundary(:,2); myOldBoundary(1,2)] );
%         preliminarElement = find( onpoly );
                
        % To select polygon vertices in counterclockwise fashion, we
        % compute the phase angles of the segments connecting the polygon
        % barycenter with the vertices and sort them in ascending order
        myNewBoundary = newNodes(preliminarElement, :);
        barycenter = sum(myNewBoundary,1)/length(preliminarElement);
        myNewBoundary(:,1) = myNewBoundary(:,1) - barycenter(1);
        myNewBoundary(:,2) = myNewBoundary(:,2) - barycenter(2);
        myTheta = atan2(myNewBoundary(:,2), myNewBoundary(:,1));
        [~, I] = sort(myTheta);
        newElements{counter} = preliminarElement(I)';        
        myNewBoundary = myNewBoundary(I,:);

        % Check whether there is an old boundary point that should have been included
        Dist = -Inf;
        numNodes = length(newElements{counter});
        iterator = [2:numNodes 1];
        for v = 1:numNodes
            newDist = dLine(myOldBoundary, myNewBoundary(v,1), myNewBoundary(v,2), ...
                                           myNewBoundary(iterator(v),1), myNewBoundary(iterator(v),2));
            newDist = newDist(:,2);
            Dist = max( Dist, newDist );
        end
        ix = find(Dist > 0);
        
        preliminarElement = newElements{counter};
        for node = 1:length(ix)
            Dist = pdist2(newNodes, myOldBoundary(ix(node),:));
            [~, jx] = min(Dist);
            preliminarElement = [preliminarElement jx];
        end
        preliminarElement = unique(preliminarElement);

        myNewBoundary = newNodes(preliminarElement, :);
        barycenter = sum(myNewBoundary,1)/length(preliminarElement);
        myNewBoundary(:,1) = myNewBoundary(:,1) - barycenter(1);
        myNewBoundary(:,2) = myNewBoundary(:,2) - barycenter(2);
        myTheta = atan2(myNewBoundary(:,2), myNewBoundary(:,1));
        [~, I] = sort(myTheta);
        newElements{counter} = preliminarElement(I);        
    end
end

% Check the newly generated mesh
figure()
axis equal; hold on;

% Plot initial mesh
for K = 1:size(Element,1)
    plot(Node([Element{K} Element{K}(1)],1), ...
         Node([Element{K} Element{K}(1)],2), 'g', 'Linewidth', 2)    
end

pause

% Overlay finer mesh
maxSideNum = 0;
for K = 1:size(newElements,1)
    plot(newNodes([newElements{K} newElements{K}(1)],1), ...
         newNodes([newElements{K} newElements{K}(1)],2), 'b')
    maxSideNum = max( maxSideNum, length(newElements{K}) );
end

pause

for K = 1:size(newElements,1)
    plot(newNodes(newElements{K},1), ...
         newNodes(newElements{K},2), 'r*')
    if length(newElements{K}) == maxSideNum
        patch(newNodes(newElements{K},1), ...
              newNodes(newElements{K},2), 'y')
    else
        patch(newNodes(newElements{K},1), ...
              newNodes(newElements{K},2), 'b')
    end
    myNewBoundary = newNodes(newElements{K}, :);
    barycenter = sum(myNewBoundary,1)/length(newElements{K});
    text(barycenter(1), barycenter(2), num2str(length(newElements{K})), 'HorizontalAlignment', 'center')
end

mesh.Node = max(min(newNodes, 1), 0);
mesh.Element = newElements;

filename = sprintf('regularized_MaxIter05_%06i.mat', numNewElements);
save(filename, 'mesh')

disp(numNewElements)