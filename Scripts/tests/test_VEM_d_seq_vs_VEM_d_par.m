%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compare the sequential and parallel versions of the code

% clear
% close all
% clc

addpath('./../Basis_Functions');
addpath('./../Mesh_Handlers');
addpath('./../Quadrature');
addpath('./../VEM_diffusion');

k = input('Polynomial degree (>= 1): ');
eoa = input('Extra order of accuracy for quadrature formulas: ');

% REMARK: when using the multiprecision toolbox, the parallel code is
% faster than the sequential one even with 1 processor (probably,
% this is due to the fact that manipulating multidimensional arrays as it
% is done in the sequential code is costly when using mp numbers.

use_mp = input('Use multiprecision toolbox (0/1)? ');
if use_mp
    addpath('./../AdvanpixMCT-4.0.0.11272')
    precision = input('Set default precision: ');
    mp.Digits(precision);
    k = mp(k);
end

%%% Choose a mesh

meshroot = './../Mesh_Handlers/meshfiles_AleRusso';
meshname = 'poligoni-random/poligoni500';
meshname = [meshroot '/' meshname];

fprintf('Loading mesh %s...\n', meshname);
mesh = loadmesh(meshname, use_mp);
fprintf('...mesh %s loaded\n', meshname);

%%% Run and time the sequential code

fprintf('\nSequential code:\n')
tic;
[~, errS_s, errinf_s, errL2_s] = VEM_d_sequential(mesh, k, eoa, use_mp);
toc;

%%% Run and time the parallel code

%parpool

fprintf('\nParallel code:\n')
compressquad = input('Compress 2D quadrature rules (0/1)?');
basis_type = input('Type of basis functions: 1) Hitchhicker   2) Hitchhicker scaled. Choice: ');

tic;
[~, errS_p, errinf_p, errL2_p] = VEM_d_parallel(mesh, k, eoa, compressquad, basis_type, use_mp);
toc;

fprintf('\n| errS_s - errS_p |     = %f\n', abs(errS_s - errS_p));
fprintf('\n| errinf_s - errinf_p | = %f\n', abs(errinf_s - errinf_p));
fprintf('\n| errL2_s - errL2_p |   = %f\n\n', abs(errL2_s - errL2_p));


