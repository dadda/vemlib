%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Converge test for DG on polygons with minus 1 stabilization

addpath ../Basis_Functions
addpath ../Mesh_Handlers
addpath ../Quadrature
addpath ../Quadrature/cpp_modules
addpath ../VEM_diffusion
addpath ../DG_polygons

fprintf('\nConverge test for DG on polygons with minus 1 stabilization\n\n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% meshRoot = '../Mesh_Handlers/meshfiles_AleRusso/esagoni-regolari';
% 
% meshNames = {
%     'esagoni18x20';
%     'esagoni26x30';
%     'esagoni34x40';
%     'esagoni44x50';
%     'esagoni52x60';
%     'esagoni60x70';
%     'esagoni70x80';
%     };
% 
% meshNameShort = 'hexa';

% meshRoot = '../Mesh_Handlers/meshfiles_AleRusso/esagoni-deformati';

% meshNames = {
%     'esagoni18x20d';
%     'esagoni26x30d';
%     'esagoni34x40d';
%     'esagoni44x50d';
%     'esagoni52x60d';
%     'esagoni60x70d';
%     'esagoni70x80d'
%     };
%
% meshNameShort = 'd-hexa';

% meshRoot = '../Mesh_Handlers/meshfiles_AleRusso/escher';

% meshNames = {
%     'horse_000100';
%     'horse_000225';
%     'horse_000400';
%     'horse_000625';
%     'horse_000900';
%     'horse_001225';
%     'horse_001600';
%     'horse_002025';
%     'horse_002500'
%     };
%
% meshNameShort = 'horse';

meshRoot = '../Mesh_Handlers/meshfiles2d/voronoi-notnested';

meshNames = {
    'notnested_MaxIter100_002048';
    'notnested_MaxIter100_004096';
    'notnested_MaxIter100_008192';
    'notnested_MaxIter100_016384';
    'notnested_MaxIter100_032768';
%     'notnested_MaxIter100_065536';
%     'notnested_MaxIter100_131072';
%     'notnested_MaxIter100_262144';
    };

meshNameShort = 'lloyd';

% meshNames = {
%     'randVoro__Nelt_002500__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'randVoro__Nelt_005000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'randVoro__Nelt_010000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'randVoro__Nelt_020000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'randVoro__Nelt_040000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'randVoro__Nelt_080000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'randVoro__Nelt_160000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'randVoro__Nelt_320000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     };
% 
% meshNameShort = 'voro';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

meshExt = '.mat';
% meshExt = '.off';

kVect            = 1;
basis_type       = 1;
eoa              = 0;
compressquad     = 0;
use_mp           = 0;
mp_precision     = 20;

refStab          = 0;
tVect            = 1;
alphaVect        = 1;
sConsistencyVect = 1;
sStabilityVect   = 1;
sProjOnConstVect = 0;
fTypeVect        = 1;

plotSol = input('Plot solution (0/1) [0]: ');
if isempty(plotSol)
    plotSol = 0;
end

if isempty(gcp('nocreate'))
    parpool;
end

% warning('off', 'MATLAB:sqrtm:SingularMatrix');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Run the test

if use_mp == 1
   warning('Quadrature formulas compression will be disabled');
   addpath ../../AdvanpixMCT-4.4.4.12666
   mp.Digits(mp_precision);
   kVect = mp(kVect); tVect = mp(tVect); alphaVect = mp(alphaVect);
   compressquad = 0;
end

nbMeshes    = length(meshNames);
errL2       = zeros(nbMeshes, 1);
errH1       = zeros(nbMeshes, 1);
hmax        = zeros(nbMeshes, 1);
testCounter = 0;

fprintf('\n\n');

for i = 1:length(tVect)

    for k = kVect

        for m = 1:nbMeshes

            testCounter = testCounter + 1;

            fprintf('Test %i:\n', testCounter);
            fprintf('--------\n\n');
            fprintf('Degree of local spaces: %i\n', k);
            fprintf('Type of 2D basis functions: %i\n', basis_type);
            fprintf('Extra order of accuracy for quadrature formulas: %i\n', eoa);
            fprintf('Compress 2D quadrature rules: %i\n', compressquad);
            fprintf('Stabilization parameter t: %i\n', tVect(i));
            fprintf('Scaling factor alpha of the stabilization term: %f\n', alphaVect(i));
            fprintf('Stabilization matrices computed on reference elements: %i\n', refStab);
            fprintf('Strategy for the consistency part of the stabilization: %i\n', sConsistencyVect(i));
            fprintf('Strategy for the stability part of the stabilization: %i\n', sStabilityVect(i));
            fprintf('Added contribution from projection on constants: %i\n', sProjOnConstVect(i));
            fprintf('Strategy for the stabilization of the load term: %i\n\n', fTypeVect(i));

            meshName = [ meshRoot '/' meshNames{m} meshExt ];

            fprintf('Loading mesh %s ... ', meshName);
            if strcmp(meshExt, '.off')
                mesh = readOFFmesh(meshName);
                mesh = loadmesh(mesh, 0);
            else
                mesh = loadmesh(meshName, 0);
            end
            fprintf('done. ');

            try
                % Try to load solution
                filename = sprintf(['/home/daniele/Documents/codes/vem_matlab/DG_polygons/workspaces/' ...
                                    'output__mesh_%s__k_%i__t_%i__alpha_%g__refStab_%i' ...
                                    '__sC_%i__sS_%i__sP_%i__fT_%i.mat'], ...
                                    meshNames{m}, k, tVect(i), alphaVect(i), refStab, ...
                                    sConsistencyVect(i), sStabilityVect(i), sProjOnConstVect(i), ...
                                    fTypeVect(i));
                load(filename);
                fprintf('Solution loaded\n');
            catch
                fprintf('Solving the problem ... ');
                [ u_h, ~, ~, errL2_m, ~, errH1_m, h, xb, yb, xqd, yqd, wqd, basis_on_quad_pts2d ] = ...
                    DG_on_polygons( mesh, k, basis_type, eoa, compressquad, ...
                                    tVect(i), alphaVect(i), refStab, ...
                                    sConsistencyVect(i), sStabilityVect(i), sProjOnConstVect(i), ...
                                    fTypeVect(i), use_mp );
                fprintf('done\n');

                filename = sprintf(['/home/daniele/Documents/codes/vem_matlab/DG_polygons/workspaces/' ...
                                    'output__mesh_%s__k_%i__t_%i__alpha_%g__refStab_%i' ...
                                    '__sC_%i__sS_%i__sP_%i__fT_%i.mat'], ...
                                    meshNames{m}, k, tVect(i), alphaVect(i), refStab, ...
                                    sConsistencyVect(i), sStabilityVect(i), sProjOnConstVect(i), ...
                                    fTypeVect(i));
                save(filename, 'u_h', 'errL2_m', 'errH1_m', 'h', ...
                               'xb', 'yb', 'xqd', 'yqd', 'wqd', 'basis_on_quad_pts2d', '-v7.3');
            end

            errL2(m) = errL2_m;
            errH1(m) = errH1_m;
            hmax(m)  = max(h);

            if plotSol
                dPk = ((k+1)*(k+2)) / 2;

                figure()
%                 axis equal;
                hold on;
                set(gcf, 'color', [1 1 1]);
                for K = 1:mesh.Nelt
                    NvLoc = mesh.elements(K,1);
                    x = mesh.coordinates(mesh.elements(K,2:NvLoc+1), 1);
                    y = mesh.coordinates(mesh.elements(K,2:NvLoc+1), 2);
                    basis_on_bnd = monomial_basis(x, y, k, xb(K), yb(K), h(K), ones(size(x)));
                    basis_on_bnd = permute(basis_on_bnd, [1, 3, 2]);
                    if basis_type == 2
                        basisL2norm = sqrt( wqd{K}' * basis_on_quad_pts2d{K}.^2 );
                        basis_on_bnd = bsxfun(@times, 1./basisL2norm(1:dPk), basis_on_bnd);
                    end

                    u = basis_on_bnd * u_h(:, K);

                    patch('XData', x, 'YData', y, 'ZData', u, 'CData', u, 'FaceColor', 'interp');
                end
                pause;
            end

        end

        fprintf('\nRelative error in the H^1 norm\n');
        fprintf('----------------------------------\n');
        format shorte
        disp(errH1)

        fprintf('Order of convergence:\n')
        orderH1    = log(errH1(1:end-1)./errH1(2:end)) ./ log(hmax(1:end-1)./hmax(2:end));
        format bank
        disp(orderH1)

        fprintf('\nRelative error in the L^2 norm\n');
        fprintf('------------------------------\n');
        format shorte
        disp(errL2)

        fprintf('Order of convergence:\n')
        orderL2    = log(errL2(1:end-1)./errL2(2:end)) ./ log(hmax(1:end-1)./hmax(2:end));
        format bank
        disp(orderL2)

        %
        % Print results to a Latex table
        %

        for m = 1:nbMeshes
            fprintf('%s$_{%i}$', meshNameShort, m);
            fprintf('   &   %e', hmax(m));
            fprintf('   &   %e', errH1(m));
            if m == 1
                fprintf('   &   {-}');
            else
                fprintf('   &   %f', orderH1(m-1));
            end
            fprintf('   &   %e', errL2(m));
            if m == 1
                fprintf('   &   {-}\\\\\n');
            else
                fprintf('   &   %f\\\\\n', orderL2(m-1));
            end
        end

        fprintf('\n');

        %
        % Print results to a figure
        %

        fprintf('errH1, k = %i\n', k);
        fprintf('-----------\n');

        for m = 1:nbMeshes
            fprintf('(%i, %g)\n', hmax(m), errH1(m));
        end

        fprintf('\n\n');

        fprintf('errL2, k = %i\n', k);
        fprintf('-----------\n');

        for m = 1:nbMeshes
            fprintf('(%i, %g)\n', hmax(m), errL2(m));
        end

        fprintf('\n\n');

        fprintf('h^%i\n', k);
        fprintf('----\n');

        for m = 1:nbMeshes
            fprintf('(%i, %g)\n', hmax(m), hmax(m)^k);
        end

        fprintf('\n\n');

        fprintf('h^%i\n', k+1);
        fprintf('----\n');

        for m = 1:nbMeshes
            fprintf('(%i, %g)\n', hmax(m), hmax(m)^(k+1));
        end

        fprintf('\n\n');

    end

end
