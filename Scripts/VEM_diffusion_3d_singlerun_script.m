%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VEM for the Poisson equation in 3D

addpath ../Mesh_Handlers
addpath ../Quadrature
addpath ../Quadrature/cpp_modules
addpath ../Basis_Functions
addpath ../VEM_diffusion
addpath ../VEM_diffusion_3D

fprintf('\nScript for solving the Poisson equation in 3D with the VEM\n\n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% User-required inputs

k            = input('Polynomial degree (>= 1): ');
eoa          = input('Extra order of accuracy for quadrature formulas: ');
compressquad = input('Compress 2D quadrature rules (0/1)? ');
basis_type   = input('Type of basis functions:\n(1) mu_alpha = ((x-xd)/hd)^alpha\n(2) mu_alpha/||mu_alpha||_L2\nChoose a basis: ');
if basis_type == 2
    error('Still need to implement scaled monomial functions in 3D')
end
use_mp       = input('Use multiprecision toolbox (0/1)? ');
if use_mp == 1
    error('Still need to implement Advanpix support')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Choose a mesh

meshroot = './../Mesh_Handlers/meshfiles3d';

% % Regular polyhedra
% meshname = 'one_polyhedron/cube';
% meshname = 'one_polyhedron/cube-octahedron';
% meshname = 'one_polyhedron/dodecahedron';
% meshname = 'one_polyhedron/icosahedron';
% meshname = 'one_polyhedron/octahedron';
% meshname = 'one_polyhedron/soccer_ball';
% meshname = 'one_polyhedron/tetrahedron';
% meshname = 'one_polyhedron/tetrakaidecahedron';
% 
% % Voronoi cells
% meshname = 'one_polyhedron/single_cell';
% meshname = 'one_polyhedron/degenerate1';
% meshname = 'one_polyhedron/degenerate2';
% meshname = 'one_polyhedron/degenerate3';
% 
% % Cubes
% meshname = 'hexahedra/cubes_Nelt000008';
% meshname = 'hexahedra/cubes_Nelt000064';
% meshname = 'hexahedra/cubes_Nelt000512';
% meshname = 'hexahedra/cubes_Nelt004096';
% meshname = 'hexahedra/cubes_Nelt032768';
% 
% % Hexahedra
% meshname = 'hexahedra/hexa_Nelt000008';
% meshname = 'hexahedra/hexa_Nelt000064';
% meshname = 'hexahedra/hexa_Nelt000512';
% meshname = 'hexahedra/hexa_Nelt004096';
% meshname = 'hexahedra/hexa_Nelt032768';
% 
% % Tetrahedra
% meshname = 'tetrahedra/tetra_Nelt000006';
% meshname = 'tetrahedra/tetra_Nelt000048';
% meshname = 'tetrahedra/tetra_Nelt000384';
% meshname = 'tetrahedra/tetra_Nelt003072';
% meshname = 'tetrahedra/tetra_Nelt024576';
% meshname = 'tetrahedra/tetra_Nelt196608';
% 
% % Voronoi meshes of the cube [-1.1,1.1] x [-1.1,1.1] x [-1.1,1.1] as dual
% % of Delaunay triangulation of [-1,1]^3.
% meshname = 'voronoi/cube/cube_Nelt000051';
% meshname = 'voronoi/cube/cube_Nelt000284';
% meshname = 'voronoi/cube/cube_Nelt001859';
% meshname = 'voronoi/cube/cube_Nelt013365';
%
% % Voronoi from particle randomly inserted in [0,1]^3
% meshname = 'voronoi/cube/unitcube_voro_Nelt8';
% meshname = 'voronoi/cube/unitcube_voro_Nelt64';
meshname = 'voronoi/cube/unitcube_voro_Nelt512';
% meshname = 'voronoi/cube/unitcube_voro_Nelt4096';
% meshname = 'voronoi/cube/unitcube_voro_Nelt32768'; -> CAUSES OUT OF MEMORY IN THE LINEAR SOLVER


meshname = [meshroot '/' meshname];
load([meshname '.mat'])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Solve the problem

% Uncomment when using the Parallel toolbox
%parpool
[U, errS, errinf, errL2] = VEM_diffusion_3d(mesh, k, eoa, compressquad, basis_type, use_mp);

fprintf('\nEnergy norm of the error: %e\n', errS);
fprintf('Inf norm of the error: %e\n', errinf);
fprintf('L^2 norm of the error: %e\n', errL2);
