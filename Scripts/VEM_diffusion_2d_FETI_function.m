function [ ] = VEM_diffusion_2d_FETI_function( fid, meshName, subdomainMeshApproach, ...
                                               k, eoa, compressquad, basis_type, use_mp, mp_precision, ...
                                               Nx, Ny, ...
                                               i_rho, rho, gamma, i_F, ...
                                               use_pc, ...
                                               saveData, savePCGiter, ...
                                               tol, maxit, runAsFor )

%
% Preprocess inputs
%

if use_mp
    addpath './../../AdvanpixMCT-4.4.4.12666'
    mp.Digits(mp_precision);
    k = mp(k);
end

% 1d quadrature rule
[xqd1, wqd1] = lobatto(k+1, use_mp);
formula1d    = [(1.-xqd1)/2. (xqd1+1.)/2. wqd1/2.];

%
% Create domain decomposition data structures
%

fprintf(fid, '\nCreating domain decomposition data structures ...\n');

meshBasename = strsplit(meshName, {'/', '.mat', '.off'});
meshBasename = meshBasename{end-1};

try
    % Try to load subdomains stiffness matrices and right hand sides
    filename = sprintf(['../FETI/workspaces/%s__k_%i__eoa_%i__compressquad_%i__' ...
                        'basis_type_%i__use_mp_%i__Nx_%i__Ny_%i__i_rho_%i.mat'], ...
                        meshBasename, k, eoa, compressquad, basis_type, use_mp, Nx, Ny, i_rho );
    load(filename);
    fprintf(fid, 'Mesh array loaded\n');
    fprintf(fid, 'Subdomain stiffness matrices and load terms loaded\n');
catch
    fprintf(fid, 'Creating mesh array ... ');
    [ meshArray, iwest, ieast, isouth, inorth ] = ...
        createMeshArray2d( meshName, subdomainMeshApproach, Nx, Ny, k, formula1d, use_mp );
    fprintf(fid, 'done\n');
    
    fprintf(fid, 'Assembling subdomain stiffness matrices and load terms ... ');
    [ K, F, hmaxBySubdomain ] = ...
        subdomainAssembler2d( meshArray, Nx, Ny, rho, i_F, ...
                              k, eoa, compressquad, formula1d, basis_type, use_mp, ...
                              runAsFor );
    fprintf(fid, 'done\n');

    infoK = whos('K');
    
    % Do not save variables if K is larger than 16 GB
    if ( saveData ~= 0 ) && ( infoK.bytes <= 16*8*1024^3)
        filename = sprintf(['../FETI/workspaces/%s__k_%i__eoa_%i__compressquad_%i__' ...
                            'basis_type_%i__use_mp_%i__Nx_%i__Ny_%i__i_rho_%i.mat'], ...
                            meshBasename, k, eoa, compressquad, basis_type, use_mp, Nx, Ny, i_rho );
        save(filename, 'meshArray', 'iwest', 'ieast', 'isouth', 'inorth', ...
                       'K', 'F', 'hmaxBySubdomain', '-v7.3');
    end
end


try
    % Try to load primal and dual boolean operators
    filename = sprintf(['../FETI/workspaces/%s__k_%i__eoa_%i__compressquad_%i__' ...
                        'basis_type_%i__use_mp_%i__Nx_%i__Ny_%i__i_rho_%i__gamma_%g.mat'], ...
                        meshBasename, k, eoa, compressquad, basis_type, use_mp, Nx, Ny, ...
                        i_rho, gamma );
    load(filename);
    fprintf(fid, 'Boolean operators loaded\n');
catch
    % Construct primal and dual boolean operators
    fprintf(fid, 'Constructing primal and dual boolean operators ... ');
    [ dimGlobalPrimal, dimGlobalLambda, localPrimalDofs, localDualDofs, localInternalDofs, ...
      Bprimal, Bdual, BdualScaled ] = ...
        booleanOperators2d( meshArray, Nx, Ny, iwest, ieast, isouth, inorth, rho, gamma );
    fprintf(fid, 'done\n');

    if saveData ~= 0
        filename = sprintf(['../FETI/workspaces/%s__k_%i__eoa_%i__compressquad_%i__' ...
                            'basis_type_%i__use_mp_%i__Nx_%i__Ny_%i__i_rho_%i__gamma_%g.mat'], ...
                            meshBasename, k, eoa, compressquad, basis_type, use_mp, Nx, Ny, ...
                            i_rho, gamma );
        save(filename, 'dimGlobalPrimal', 'dimGlobalLambda', 'localPrimalDofs', 'localDualDofs', ...
                       'localInternalDofs', 'Bprimal', 'Bdual', 'BdualScaled', '-v7.3');
    end
end


try
    % Try to load dual primal operators
    filename = sprintf(['../FETI/workspaces/%s__k_%i__eoa_%i__compressquad_%i__' ...
                        'basis_type_%i__use_mp_%i__Nx_%i__Ny_%i__i_rho_%i__gamma_%g__DP.mat'], ...
                        meshBasename, k, eoa, compressquad, basis_type, use_mp, Nx, Ny, ...
                        i_rho, gamma );
    load(filename);
    fprintf(fid, 'Kstar, fstar, and d loaded\n');
catch
    % Assemble some terms of the dual-primal system
    fprintf(fid, 'Assembling Kstar, fstar, d ... ');

    [ Kstar, fstar, dr ] = ...
        assembleDualPrimalOperators2d( Nx, Ny, K, F, dimGlobalPrimal, dimGlobalLambda, ...
                                       localPrimalDofs, localDualDofs, localInternalDofs, ...
                                       Bprimal, Bdual, runAsFor );

	tmp = Kstar\fstar;
    d   = dr - actionFIrc2d( tmp, Nx, Ny, K, dimGlobalLambda, ...
                             localPrimalDofs, localDualDofs, localInternalDofs, ...
                             Bprimal, Bdual, runAsFor );

    fprintf(fid, 'done\n');
    
    if saveData ~= 0
        filename = sprintf(['../FETI/workspaces/%s__k_%i__eoa_%i__compressquad_%i__' ...
                            'basis_type_%i__use_mp_%i__Nx_%i__Ny_%i__i_rho_%i__gamma_%g__DP.mat'], ...
                            meshBasename, k, eoa, compressquad, basis_type, use_mp, Nx, Ny, ...
                            i_rho, gamma );
        save(filename, 'Kstar', 'fstar', 'd', '-v7.3');
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Solution with FETI-DP

try
    % Try to load PCG solution
    filename = sprintf(['../FETI/workspaces/%s__k_%i__eoa_%i__compressquad_%i__' ...
                        'basis_type_%i__use_mp_%i__Nx_%i__Ny_%i__i_rho_%i__gamma_%g__' ...
                        'tol_%g__maxit_%i__use_pc_%i.mat'], ...
                        meshBasename, k, eoa, compressquad, basis_type, use_mp, Nx, Ny, ...
                        i_rho, gamma, tol, maxit, use_pc );
    load(filename);
    fprintf(fid, 'PCG solution loaded\n');    
catch
    fprintf(fid, ['Solving the dual interface problem with PCG ' ...
                  '(tolerance = %g, maximum number of iterations = %i)...\n'], tol, maxit);
    [ lambda, ~, iter, relResVec, eigEst ] = ...
        pcgFETIDP2d( meshBasename, k, eoa, compressquad, basis_type, use_mp, ...
                     Nx, Ny, i_rho, K, gamma, ...
                     dimGlobalPrimal, dimGlobalLambda, ...
                     localPrimalDofs, localDualDofs, localInternalDofs, ...
                     Bprimal, Bdual, BdualScaled, ...
                     Kstar, d, tol, maxit, use_pc, fid, savePCGiter, runAsFor );

    if saveData ~= 0
        filename = sprintf(['../FETI/workspaces/%s__k_%i__eoa_%i__compressquad_%i__' ...
                            'basis_type_%i__use_mp_%i__Nx_%i__Ny_%i__i_rho_%i__gamma_%g__' ...
                            'tol_%g__maxit_%i__use_pc_%i.mat'], ...
                            meshBasename, k, eoa, compressquad, basis_type, use_mp, Nx, Ny, ...
                            i_rho, gamma, tol, maxit, use_pc );
        save(filename, 'lambda', 'iter', 'relResVec', 'eigEst', '-v7.3');
    end
end

%
% Compute vector of the primal dofs
%
uPrimal = Kstar \ ( fstar + actionFIrcT2d( lambda, Nx, Ny, K, dimGlobalPrimal, ...
                                           localPrimalDofs, localDualDofs, localInternalDofs, ...
                                           Bprimal, Bdual, runAsFor ) );

%
% Reconstruct the solution in each subdomain
%
fprintf(fid, 'Reconstructing the solution in each subdomain ... ');
u = reconstructSolution2d( lambda, uPrimal, meshArray, Nx, Ny, K, F, ...
                           localPrimalDofs, localDualDofs, localInternalDofs, ...
                           Bprimal, Bdual, runAsFor );
fprintf(fid, 'done\n');

%
% Error computation
%
% Compute errors only if the load term has been computed analytically.
%
if i_F == 1
    sol = exactSolution2d( meshArray, Nx, Ny, ...
                           k, eoa, compressquad, basis_type, formula1d, use_mp, ...
                           runAsFor );

    % We can use the function in the 3D module ;)
    [ relerrEnergy, relerrInf ] = computeErrors( [Nx, Ny], sol, u, K, runAsFor );
end

%
% Report
%
[ numSubdomains, hmaxInv, Hoh, totNumDofs, numDofsBySubdomain ] = ...
    computeStatistics( meshArray, [Nx, Ny], hmaxBySubdomain, u ); %#ok<ASGLU>

% [ hmin, gamma0, gamma1 ] = computationalIntensiveStatistics( meshArray, [Nx, Ny], runAsFor );
%
% if saveData ~= 0
%     filename = sprintf(['../FETI/workspaces/%s__k_%i__eoa_%i__compressquad_%i__' ...
%                         'basis_type_%i__use_mp_%i__Nx_%i__Ny_%i__i_rho_%i__gamma_%g__' ...
%                         'tol_%g__maxit_%i__use_pc_%i_statistics.mat'], ...
%                         meshBasename, k, eoa, compressquad, basis_type, use_mp, Nx, Ny, ...
%                         i_rho, gamma, tol, maxit, use_pc );
%     if i_F == 1
%         save(filename, 'relerrEnergy', 'relerrInf', 'hmaxInv', 'Hoh', 'totNumDofs', ...
%                        'numDofsBySubdomain', 'hmin', 'gamma0', 'gamma1', '-v7.3');
%     else
%         save(filename, 'hmaxInv', 'Hoh', 'totNumDofs', ...
%                        'numDofsBySubdomain', 'hmin', 'gamma0', 'gamma1', '-v7.3');
%     end
% end


fprintf(fid, '\nResults\n');
fprintf(fid, '*******\n\n');
fprintf(fid, 'Number of subdomains: %i\n', numSubdomains);
fprintf(fid, '1/h: %e\n', hmaxInv);
fprintf(fid, 'H/h: %e\n', Hoh);
% fprintf(fid, 'h_min: %e\n', hmin);
% fprintf(fid, 'gamma_0: %e\n', gamma0);
% fprintf(fid, 'gamma_1: %e\n', gamma1);
fprintf(fid, 'D.o.f.: %i\n', totNumDofs);
fprintf(fid, 'Dimension of coarse space: %i\n', length(uPrimal) );
fprintf(fid, 'Minimum eigenvalue: %e\n', eigEst(1));
fprintf(fid, 'Maximum eigenvalue: %e\n', eigEst(2));
fprintf(fid, 'Number of PCG iterations: %i\n', iter);
if i_F == 1
    fprintf(fid, 'Relative error energy norm: %e\n', relerrEnergy);
    fprintf(fid, 'Relative error inf norm: %e\n', relerrInf);
end
fprintf(fid, 'Vector of the estimated residual norms:\n');
for i = 1:iter
    fprintf(fid, '%e\n', relResVec(i));
end

fprintf(fid, '\n\n');

end


