function [] = DG_convtest_function( fid, ...
					                meshNames, k, basis_type, eoa, compressquad, use_mp, ...
					                t, alpha, ...
                                    refStab, sConsistency, sStability, sProjOnConst, fType )

fprintf(fid, '\nInputs\n');
fprintf(fid, '******\n\n');
fprintf(fid, 'Degree of local spaces: %i\n', k);
fprintf(fid, 'Type of 2D basis functions: %i\n', basis_type);
fprintf(fid, 'Extra order of accuracy for quadrature formulas: %i\n', eoa);
fprintf(fid, 'Compress 2D quadrature rules: %i\n', compressquad);
fprintf(fid, 'Stabilization parameter t: %i\n', t);
fprintf(fid, 'Scaling factor alpha of the stabilization term: %f\n', alpha);
fprintf(fid, 'Stabilization matrices computed on reference elements: %i\n', refStab);
fprintf(fid, 'Strategy for the consistency part of the stabilization: %i\n', sConsistency);
fprintf(fid, 'Strategy for the stability part of the stabilization: %i\n', sStability);
fprintf(fid, 'Added contribution from projection on constants: %i\n', sProjOnConst);
fprintf(fid, 'Strategy for the stabilization of the load term: %i\n\n', fType);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

numMeshes  = length(meshNames);
relerrL2_u = zeros([1 numMeshes]);
relerrH1_u = zeros([1 numMeshes]);
hmax       = zeros([1 numMeshes]);

for i = 1:numMeshes
	fprintf(fid, 'Loading mesh %s ... ', meshNames{i});
    mesh = loadmesh(meshNames{i}, use_mp);
    fprintf(fid, 'done. ');

    fprintf(fid, 'Running DG method ... ');
    [ ~, ~, ~, relerrL2_u(i), ~, relerrH1_u(i), h] = ...
        DG_on_polygons( mesh, k, basis_type, eoa, compressquad, t, alpha, ...
                        refStab, sConsistency, sStability, sProjOnConst, fType, use_mp );
    fprintf(fid, 'done\n');
                    
    hmax(i) = max(h);
end
orderL2_u = log(relerrL2_u(1:end-1)./relerrL2_u(2:end)) ./ log(hmax(1:end-1)./hmax(2:end));
orderH1_u = log(relerrH1_u(1:end-1)./relerrH1_u(2:end)) ./ log(hmax(1:end-1)./hmax(2:end));

fprintf(fid, '\nResults\n');
fprintf(fid, '*******\n\n');
fprintf(fid, 'Relative error for u in the L^2 norm:');
for i = 1:numMeshes
	fprintf(fid, '   %.4e', relerrL2_u(i));
end
fprintf(fid, '\n');
fprintf(fid, 'Order of convergence:');
for i = 1:numMeshes-1
    fprintf(fid, '   %1.2f', orderL2_u(i));
end
fprintf(fid, '\n');
fprintf(fid, 'Relative error for u in the H^1 norm:');
for i = 1:numMeshes
    fprintf(fid, '   %.4e', relerrH1_u(i));
end
fprintf(fid, '\n');
fprintf(fid, 'Order of convergence:');
for i = 1:numMeshes-1
    fprintf(fid, '   %1.2f', orderH1_u(i));
end

fprintf(fid, '\n\n\n');

end
