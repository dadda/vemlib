%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Solve the Poisson equation with DG-VEM

addpath('../Basis_Functions');
addpath('../Mesh_Handlers');
addpath('../Quadrature');
addpath('../Quadrature/cpp_modules');
addpath('../VEM_diffusion');
addpath('../DG_polygons');

fprintf('\nScript for solving the Poisson equation in 2D with DG-VEM\n\n');

%
% Inputs from the user
%
k            = input('Polynomial degree: ');
basis_type   = input('Type of 2D basis functions: 1) Hitchhicker 2) Hitchhicker scaled: ');
eoa          = input('Extra order of accuracy for quadrature formulas: ');
compressquad = input('Compress 2D quadrature rules (0/1)? ');
t            = input('Type of stabilization (-1, 0, 1): ');
if t == 0
    alpha        = 0;
    refStab      = 1;
    sConsistency = 0;
    sStability   = 0;
    sProjOnConst = 0;
    fType        = 1;
else
    alpha   = input('Scaling factor alpha for the (-1)-stab term: ');
    refStab = input('Compute stabilization matrices on reference elements (0/1)?: ');
    sConsistency = input(['Consistency term in the VEM stabilization:\n' ...
                          '(0) none;\n' ...
                          '(1) classical VEM consistency term;\n' ...
                          '(2) mortar method recipe.\n\n' ...
                          'Choice: ']);
    sStability   = input(['Stability term in the VEM stabilization:\n' ...
                          '(0) none;\n' ...
                          '(1) classical VEM stability term;\n' ...
                          '(2) mortar method recipe.\n\n' ...
                          'Choice: ']);
    sProjOnConst = input(['Add contribution to the VEM stabilization from projection ' ...
                          'on constants (0/1)?: ']);
    fType        = input(['Stabilization for the load term:\n' ...
                          '(1) \Pi^0_{k+2};\n' ...
                          '(2) \Pi^0_{k+1};\n' ...
                          '(3) \Pi^0_{k};\n'...
                          '(4) \Pi^\\nabla_{k+2}.\n\nChoice: ']);
end

use_mp = input('Use multiprecision toolbox (0/1)? ');
if use_mp
    warning('Quadrature formula compression will be disabled');    
    addpath('../../AdvanpixMCT-4.4.4.12666')
    precision = input('Set default precision: ');
    mp.Digits(precision);
    k = mp(k); t = mp(t); alpha = mp(alpha);
    compressquad = 0;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

warning('off', 'MATLAB:sqrtm:SingularMatrix');

%
% Choose a mesh
%
meshroot = '../Mesh_Handlers/meshfiles_AleRusso';

% meshname = 'poligono/quadrato';
% meshname = 'poligono/horse';
% meshname = 'quadrati-regolari/quadrati10x10';
% meshname = 'esagoni-regolari/esagoni04x05';
% meshname = 'esagoni-regolari/esagoni8x10';
meshname = 'esagoni-deformati/esagoni8x10d';
% meshname = 'esagoni-deformati/esagoni52x60d';
% meshname = 'poligoni-random/poligoni1000';
meshname = [meshroot '/' meshname];

fprintf('Loading mesh %s...\n', meshname);
mesh = loadmesh(meshname, use_mp);
fprintf('...mesh %s loaded\n', meshname);

%
% Run DG-VEM
%
[ u_h, lambda_h, errL2_u, relerrL2_u, errH1_u, relerrH1_u, h, xb, yb, xqd, yqd, wqd, basis_on_quad_pts2d ] = ...
    DG_on_polygons( mesh, k, basis_type, eoa, compressquad, t, alpha, ...
                    refStab, sConsistency, sStability, sProjOnConst, fType, use_mp );

fprintf('L^2 absolute error for u_h: %e\n', errL2_u);
fprintf('L^2 relative error for u_h: %e\n\n', relerrL2_u);

fprintf('H^1 absolute error for u_h: %e\n', errH1_u);
fprintf('H^1 relative error for u_h: %e\n\n', relerrH1_u);

% %
% % Plot u_h
% %
% fig1 = figure();
% fig2 = figure();
% 
% d2 = ((k+1)*(k+2)) / 2;
% for K = 1:mesh.Nelt
%     Nver_loc = mesh.elements(K,1);
%     x = mesh.coordinates(mesh.elements(K,2:Nver_loc+1), 1);
%     y = mesh.coordinates(mesh.elements(K,2:Nver_loc+1), 2);
%     basis_on_bnd = monomial_basis(x, y, k, xb(K), yb(K), h(K), ones(size(x)));
%     basis_on_bnd = permute(basis_on_bnd, [1, 3, 2]);
%     if basis_type == 2
%         basisL2norm = sqrt( wqd{K}' * basis_on_quad_pts2d{K}.^2 );
%         basis_on_bnd = bsxfun(@times, 1./basisL2norm(1:d2), basis_on_bnd);
%     end
%    
%     u = basis_on_bnd * u_h(:, K);
% 
%     figure(fig1)
%     hold on
%     patch(x, y, u, 'FaceColor', 'interp');
%    
%     figure(fig2)
%     hold on
%     patch('XData', x, 'YData', y, 'ZData', u, 'CData', u, 'FaceColor', 'interp');
% end
