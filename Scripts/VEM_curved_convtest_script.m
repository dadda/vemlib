%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Converge test for curved VEM

addpath ../Basis_Functions
addpath ../Mesh_Handlers
addpath ../Quadrature
addpath ../Quadrature/cpp_modules
addpath ../VEM_diffusion
addpath ../VEM_curved

fprintf('\nHigh Order VEM on Curved Domains - Convergence Test\n\n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% User-required inputs

bnd = input(['Domain:\n' ...
             '1) astroid;\n' ...
             '2) flower;\n' ...
             '3) quarter;\n' ...
             '4) sector;\n' ...
             '5) spiral;\n' ...
             '6) unit circle.\n\n' ...
             'Choice: ']);
switch bnd
    case 1
        bnd = 'astroid';
    case 2
        bnd = 'flower';
    case 3
        bnd = 'quarter';
    case 4
        bnd = 'sector';
    case 5
        bnd = 'spiral';
    case 6
        bnd = 'unitcircle';
end

mVect = str2num( input('List of polynomial degrees (>= 1), comma or white-space separated [1]: ', 's') ); %#ok<ST2NM>
if isempty(mVect)
    mVect = 1;
end

eoa = input('Extra order of accuracy for quadrature formulas [0]: ');
if isempty(eoa)
   eoa = 0;
end

compressquad = input('Compress 2D quadrature rules? 0/1 [0]: ');
if isempty(compressquad)
    compressquad = 0;
end

gamma = input('Stabilization parameter gamma [1]: ');
if isempty(gamma)
    gamma = 1;
end

use_mp = input('Use multiprecision toolbox (0/1) [0]? ');
if isempty(use_mp)
    use_mp = 0;
end

if use_mp
    addpath('./../../AdvanpixMCT-4.4.4.12666')
    precision = input('Set default precision: ');
    mp.Digits(precision);
    mVect = mp(mVect);
end

dofsLayout = 1;

plotSol = input('Plot solution (0/1) [0]: ');
if isempty(plotSol)
    plotSol = 0;
end

if isempty(gcp('nocreate'))
    parpool;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% meshRoot = '../Mesh_Handlers/meshfiles2d/astroid/dual';

% meshNames = {
%     'astroid_a0p01.1_marked_dual_corrected_optimized';
%     'astroid_a0p0025.1_marked_dual_corrected_optimized';
%     'astroid_a0p000625.1_marked_dual_corrected_optimized';
%     'astroid_a0p00015625.1_marked_dual_corrected_optimized';
%     'astroid_a0p0000390625.1_marked_dual_corrected_optimized';
%     'astroid_a0p000009765625.1_marked_dual_corrected_optimized';
%     'astroid_a0p00000244140625.1_marked_dual_corrected_optimized';
%     };
% 
% meshNameShort = 'astroid';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% meshRoot = '../Mesh_Handlers/meshfiles2d/flower/dual';

% meshNames = {
%     'flower_a0p02.1_marked_dual_corrected_optimized';
%     'flower_a0p005.1_marked_dual_corrected_optimized';
%     'flower_a0p0025.1_marked_dual_corrected_optimized';
%     'flower_a0p00125.1_marked_dual_corrected_optimized';
%     'flower_a0p000625.1_marked_dual_corrected_optimized';
%     'flower_a0p0003125.1_marked_dual_corrected_optimized';
%     'flower_a0p00015625.1_marked_dual_corrected_optimized';
%     'flower_a0p000078125.1_marked_dual_corrected_optimized';
%     };
% 
% meshNameShort = 'flower';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% meshRoot = '../Mesh_Handlers/meshfiles2d/quarter';

% meshNames = {
%     'quarter_000256_hmin_1e-10_MaxIter_100_corrected';
%     'quarter_000512_hmin_1e-10_MaxIter_100_corrected';
%     'quarter_001024_hmin_1e-10_MaxIter_100_corrected';
%     'quarter_002048_hmin_1e-10_MaxIter_100_corrected';
%     'quarter_004096_hmin_1e-10_MaxIter_100_corrected';
%     'quarter_008192_hmin_1e-10_MaxIter_100_corrected';
%     'quarter_016384_hmin_1e-10_MaxIter_100_corrected';
%     'quarter_032768_hmin_1e-10_MaxIter_100_corrected';
%     'quarter_065536_hmin_1e-10_MaxIter_100_corrected';
%     'quarter_131072_hmin_1e-10_MaxIter_100_corrected';
%     'quarter_262144_hmin_1e-10_MaxIter_100_corrected';
%     };
% 
% meshNameShort = 'u-quarter';

% meshNames = {
%     'quarter__Nelt_000256__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'quarter__Nelt_000512__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'quarter__Nelt_001024__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'quarter__Nelt_002048__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'quarter__Nelt_004096__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'quarter__Nelt_008192__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'quarter__Nelt_016384__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'quarter__Nelt_032768__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'quarter__Nelt_065536__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'quarter__Nelt_131072__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'quarter__Nelt_262144__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     };
% 
% meshNameShort = 'quarter';

% meshRoot = '../Mesh_Handlers/meshfiles2d/quarter/dual';

% meshNames = {
%     'quarter_a0p01.1_marked_dual_corrected';
%     'quarter_a0p0025.1_marked_dual_corrected';
%     'quarter_a0p000625.1_marked_dual_corrected';
%     'quarter_a0p00015625.1_marked_dual_corrected';
%     'quarter_a0p0000390625.1_marked_dual_corrected';
%     'quarter_a0p000009765625.1_marked_dual_corrected';
%     'quarter_a0p00000244140625.1_marked_dual_corrected';
%     };
% 
% meshNameShort = 'd-quarter';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% meshRoot = '../Mesh_Handlers/meshfiles2d/sector';

% meshNames = {
%     'sector_000256_hmin_1e-10_MaxIter_100_corrected';
%     'sector_000512_hmin_1e-10_MaxIter_100_corrected';
%     'sector_001024_hmin_1e-10_MaxIter_100_corrected';
%     'sector_002048_hmin_1e-10_MaxIter_100_corrected';
%     'sector_004096_hmin_1e-10_MaxIter_100_corrected';
%     'sector_008192_hmin_1e-10_MaxIter_100_corrected';
%     'sector_016384_hmin_1e-10_MaxIter_100_corrected';
%     'sector_032768_hmin_1e-10_MaxIter_100_corrected';
%     'sector_065536_hmin_1e-10_MaxIter_100_corrected';
%     'sector_131072_hmin_1e-10_MaxIter_100_corrected';
%     'sector_262144_hmin_1e-10_MaxIter_100_corrected';
%     };
% 
% meshNameShort = 'u-sector';

% meshNames = {
%     'sector__Nelt_000256__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'sector__Nelt_000512__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'sector__Nelt_001024__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'sector__Nelt_002048__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'sector__Nelt_004096__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'sector__Nelt_008192__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'sector__Nelt_016384__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'sector__Nelt_032768__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'sector__Nelt_065536__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'sector__Nelt_131072__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'sector__Nelt_262144__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     };
% 
% meshNameShort = 'sector';

% meshRoot = '../Mesh_Handlers/meshfiles2d/sector/dual';

% meshNames = {
%     'sector_a0p05.1_marked_dual_corrected';
%     'sector_a0p0125.1_marked_dual_corrected';
%     'sector_a0p003125.1_marked_dual_corrected';
%     'sector_a0p00078125.1_marked_dual_corrected';
%     'sector_a0p0001953125.1_marked_dual_corrected';
%     'sector_a0p000048828125.1_marked_dual_corrected';
%     'sector_a0p00001220703125.1_marked_dual_corrected';
%     };
% 
% meshNameShort = 'd-sector';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% meshRoot = '../Mesh_Handlers/meshfiles2d/spiral/dual';

% meshNames = {
%     'spiral_fat_a0p01.1_marked_dual_corrected_optimized';
%     'spiral_fat_a0p005.1_marked_dual_corrected_optimized';
%     'spiral_fat_a0p0025.1_marked_dual_corrected_optimized';
%     'spiral_fat_a0p00125.1_marked_dual_corrected_optimized';
%     'spiral_fat_a0p000625.1_marked_dual_corrected_optimized';
%     'spiral_fat_a0p0003125.1_marked_dual_corrected_optimized';
%     'spiral_fat_a0p00015625.1_marked_dual_corrected_optimized';
% %     'spiral_fat_a0p000078125.1_marked_dual_corrected_optimized';
%     };
% 
% meshNameShort = 'spiral';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

meshRoot = '../Mesh_Handlers/meshfiles2d/unitcircle';

meshNames = {
    'ucirc_000256_hmin_1e-10_MaxIter_100_corrected';
    'ucirc_000512_hmin_1e-10_MaxIter_100_corrected';
    'ucirc_001024_hmin_1e-10_MaxIter_100_corrected';
    'ucirc_002048_hmin_1e-10_MaxIter_100_corrected';
    'ucirc_004096_hmin_1e-10_MaxIter_100_corrected';
    'ucirc_008192_hmin_1e-10_MaxIter_100_corrected';
    'ucirc_016384_hmin_1e-10_MaxIter_100_corrected';
    'ucirc_032768_hmin_1e-10_MaxIter_100_corrected';
    'ucirc_065536_hmin_1e-10_MaxIter_100_corrected';
    'ucirc_131072_hmin_1e-10_MaxIter_100_corrected';
    'ucirc_262144_hmin_1e-10_MaxIter_100_corrected';
};

meshNameShort = 'u-circle';

% meshNames = {
%     'ucirc__Nelt_000256__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'ucirc__Nelt_000512__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'ucirc__Nelt_001024__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'ucirc__Nelt_002048__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'ucirc__Nelt_004096__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'ucirc__Nelt_008192__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'ucirc__Nelt_016384__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'ucirc__Nelt_032768__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'ucirc__Nelt_065536__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'ucirc__Nelt_131072__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     'ucirc__Nelt_262144__hmin_1e-10__minArea_1e-20__tolTotArea_1e+00_corrected';
%     };
% 
% meshNameShort = 'circle';

% meshRoot = '../Mesh_Handlers/meshfiles2d/unitcircle/dual';

% meshNames = {
%     'unitcircle_a0p01.1_marked_dual_corrected';
%     'unitcircle_a0p0025.1_marked_dual_corrected';
%     'unitcircle_a0p000625.1_marked_dual_corrected';
%     'unitcircle_a0p00015625.1_marked_dual_corrected';
%     'unitcircle_a0p0000390625.1_marked_dual_corrected';
%     'unitcircle_a0p000009765625.1_marked_dual_corrected';
%     'unitcircle_a0p00000244140625.1_marked_dual_corrected';
%     };
% 
% meshNameShort = 'd-circ';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

meshExt = '.mat';
% meshExt = '.off';

solType = 'cos4';

nbMeshes = length(meshNames);
dofs     = zeros(nbMeshes, 1);
errA     = zeros(nbMeshes, 1);
errL2    = zeros(nbMeshes, 1);
hmax     = zeros(nbMeshes, 1);
deltamax = zeros(nbMeshes, 1);

fprintf('\n\n');

for m = mVect

    fprintf('Experiments for m = %i\n', m);
    fprintf('---------------------\n\n');

    for i = 1:nbMeshes
        meshName = [ meshRoot '/' meshNames{i} meshExt ];

        fprintf('Loading mesh %s ... ', meshName);
        if strcmp(meshExt, '.off')
            mesh = readOFFmesh(meshName);
            mesh = loadmesh(mesh, 0);
        else
            mesh = loadmesh(meshName, 0);
        end
        fprintf('done. ');

        try
            % Try to load solution
            filename = sprintf(['/home/daniele/Documents/codes/vem_matlab/' ...
                                'VEM_curved/workspaces/output_bnd_%s_mesh_%s_m_%i_gamma_%g_sol_%s.mat'], ...
                                 bnd, meshNames{i}, m, gamma, solType);
            load(filename);
            fprintf('VEM solution loaded\n');
        catch
            fprintf('Solving the problem ... ');
            [ u, errA_i, errL2_i, hmax_i, deltamax_i ] = ...
                VEM_curved( bnd, mesh, m, eoa, compressquad, gamma, use_mp, dofsLayout );
            fprintf('done\n');

            filename = sprintf(['/home/daniele/Documents/codes/vem_matlab/' ...
                                'VEM_curved/workspaces/output_bnd_%s_mesh_%s_m_%i_gamma_%g_sol_%s.mat'], ...
                                 bnd, meshNames{i}, m, gamma, solType);
            save(filename, 'u', 'errA_i', 'errL2_i', 'hmax_i', 'deltamax_i', '-v7.3');
        end

        errA(i)     = errA_i;
        errL2(i)    = errL2_i;
        hmax(i)     = hmax_i;
        deltamax(i) = deltamax_i;
        dofs(i)     = length(u);

        if plotSol
            if dofsLayout == 1
                figure()
                axis equal;
                set(gcf, 'color', [1 1 1]);
                plot3( mesh.coordinates(:,1), mesh.coordinates(:,2), u(1:mesh.Nver), ...
                       '.', 'MarkerEdgeColor', [1, 165/255, 0] )

                figure()
%                 axis equal;
                hold on;
                set(gcf, 'color', [1 1 1]);
                for E = 1:mesh.Nelt
                    NvLoc = mesh.elements(E,1);
                    x = mesh.coordinates( mesh.elements(E,2:NvLoc+1), 1 );
                    y = mesh.coordinates( mesh.elements(E,2:NvLoc+1), 2 );
                    patch('XData', x, 'YData', y, 'ZData', u(mesh.elements(E,2:NvLoc+1)), 'CData', u(mesh.elements(E,2:NvLoc+1)), 'FaceColor', 'interp');
                end
                pause;
            end
        end
    end

    fprintf('\nRelative error in the energy norm\n');
    fprintf('----------------------------------\n');
    format shorte
    disp(errA)

    fprintf('Order of convergence:\n')
    orderA    = log(errA(1:end-1)./errA(2:end)) ./ log(hmax(1:end-1)./hmax(2:end));
    orderAbis = 2 * log(errA(1:end-1)./errA(2:end)) ./ log(dofs(2:end)./dofs(1:end-1));
    format bank
    disp(orderA)
    disp(orderAbis)

    fprintf('\nRelative error in the L^2 norm\n');
    fprintf('------------------------------\n');
    format shorte
    disp(errL2)

    fprintf('Order of convergence:\n')
    orderL2    = log(errL2(1:end-1)./errL2(2:end)) ./ log(hmax(1:end-1)./hmax(2:end));
    orderL2bis = 2 * log(errL2(1:end-1)./errL2(2:end)) ./ log(dofs(2:end)./dofs(1:end-1));
    format bank
    disp(orderL2)
    disp(orderL2bis)

    %
    % Print results to a Latex table
    %

    for i = 1:nbMeshes
        fprintf('%s$_{%i}$', meshNameShort, i);
        fprintf('   &   %e', hmax(i));
        fprintf('   &   %i', dofs(i));
        fprintf('   &   %e', errA(i));
        if i == 1
            fprintf('   &   {-}');
        else
            fprintf('   &   %f', orderAbis(i-1));
        end
        fprintf('   &   %e', errL2(i));
        if i == 1
            fprintf('   &   {-}\\\\\n');
        else
            fprintf('   &   %f\\\\\n', orderL2bis(i-1));
        end
    end

    fprintf('\n');

    %
    % Print results to a figure
    %

    fprintf('errA, m = %i\n', m);
    fprintf('-----------\n');

    for i = 1:nbMeshes
        fprintf('(%i, %g)\n', dofs(i), errA(i));
    end

    fprintf('\n\n');

    fprintf('errL2, m = %i\n', m);
    fprintf('-----------\n');

    for i = 1:nbMeshes
        fprintf('(%i, %g)\n', dofs(i), errL2(i));
    end

    fprintf('\n\n');

    fprintf('N_dofs^(-%i/2)\n', m);
    fprintf('-------------\n');

    for i = 1:nbMeshes
        fprintf('(%i, %g)\n', dofs(i), 1 / sqrt( dofs(i) )^m);
    end

    fprintf('\n\n');

    fprintf('N_dofs^(-%i/2)\n', m+1);
    fprintf('-------------\n');

    for i = 1:nbMeshes
        fprintf('(%i, %g)\n', dofs(i), 1 / sqrt( dofs(i) )^(m+1));
    end

    fprintf('\n\n');

end
