%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Convergence test for the VEM for the Poisson equation

% clear
% close all
% clc

addpath('./../Basis_Functions');
addpath('./../Mesh_Handlers');
addpath('./../Quadrature');
addpath('../Quadrature/cpp_modules');
addpath('./../VEM_diffusion');

fprintf('\nConvergence test for the VEM for the Poisson equation\n\n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% User-required inputs

i_F = input('Enter type of load term: (1) exact, (2) uniform distribution in [-1,1]. Choice [1]: ');
if isempty(i_F)
    i_F = 1;
end

dofsLayout = input('Layout of degrees of freedom: 1) standard; 2) clusterized [1]: ');
if isempty(dofsLayout)
    dofsLayout = 1;
end

k = input('Polynomial degree (>= 1) [1]: ');
if isempty(k)
    k = 1;
end

eoa = input('Extra order of accuracy for quadrature formulas [0]: ');
if isempty(eoa)
   eoa = 0;
end

compressquad = input('Compress 2D quadrature rules? 0/1 [0]: ');
if isempty(compressquad)
    compressquad = 0;
end

basis_type = input('Type of basis functions: 1) Hitchhicker   2) Hitchhicker scaled. Choice [1]: ');
if isempty(basis_type)
    basis_type = 1;
end

use_mp = input('Use multiprecision toolbox (0/1) [0]? ');
if isempty(use_mp)
    use_mp = 0;
end

if use_mp
    addpath('./../../AdvanpixMCT-4.4.4.12666')
    precision = input('Set default precision: ');
    mp.Digits(precision);
    k = mp(k);
end

if isempty(gcp('nocreate'))
    parpool;
end

%
% Meshes
%

% meshRoot = '../Mesh_Handlers/meshfiles2d/voronoi-notnested';
% 
% meshNames = {
%     'randVoro__Nelt_002500__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'randVoro__Nelt_005000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'randVoro__Nelt_010000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'randVoro__Nelt_020000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'randVoro__Nelt_040000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'randVoro__Nelt_080000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'randVoro__Nelt_160000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'randVoro__Nelt_320000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     'randVoro__Nelt_640000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08';
%     };

% meshNames = {
%     'notnested_MaxIter100_002048';
%     'notnested_MaxIter100_004096';
%     'notnested_MaxIter100_008192';
%     'notnested_MaxIter100_016384';
%     'notnested_MaxIter100_032768';
%     'notnested_MaxIter100_065536';
%     'notnested_MaxIter100_131072';
%     'notnested_MaxIter100_262144';
% %     'notnested_MaxIter100_524288';
%     };

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% meshRoot = '../Mesh_Handlers/meshfiles2d/voronoi-notnested';
% 
% meshNames = {
%     'randVoro__Nelt_005000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off.metis_nparts_00000512';
% 	'randVoro__Nelt_010000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off.metis_nparts_00001024';
% 	'randVoro__Nelt_020000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off.metis_nparts_00002048';
% 	'randVoro__Nelt_040000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off.metis_nparts_00004096';
% 	'randVoro__Nelt_080000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off.metis_nparts_00008192';
% 	'randVoro__Nelt_160000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off.metis_nparts_00016384';
% 	'randVoro__Nelt_320000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off.metis_nparts_00032768';
%     'randVoro__Nelt_640000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off.metis_nparts_00065536';
% 	'randVoro__Nelt_01280000__hmin_1e-10__minArea_1e-20__tolTotArea_1e-08.off.metis_nparts_00131072';
%     };

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% meshRoot = '../Mesh_Handlers/meshfiles_AleRusso/esagoni-deformati';
% 
% meshNames = {
%     'esagoni18x20d';
%     'esagoni26x30d';
%     'esagoni34x40d';
%     'esagoni44x50d';
%     'esagoni52x60d';
%     'esagoni60x70d';
%     'esagoni70x80d'
%     };

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% meshRoot = '../Mesh_Handlers/meshfiles_AleRusso/escher';
% 
% meshNames = {
% %     'horse_000100';
% %     'horse_000225';
% %     'horse_000400';
% %     'horse_000625';
% %     'horse_000900';
% %     'horse_001225';
% %     'horse_001600';
% %     'horse_002025';
%     'horse_002500';
%     'horse_003600';
%     'horse_004900';
%     'horse_006400';
%     'horse_008100';
%     'horse_010000';
%     'horse_012100';
%     'horse_014400';
%     'horse_016900';
%     'horse_019600';
%     };

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% meshRoot = '../Mesh_Handlers/meshfiles_AleRusso/escher';
% 
% meshNames = {
%     'horse_002500.mat.metis_nparts_00000250';
%     'horse_003600.mat.metis_nparts_00000360';
%     'horse_004900.mat.metis_nparts_00000490';
%     'horse_006400.mat.metis_nparts_00000640';
%     'horse_008100.mat.metis_nparts_00000810';
%     'horse_010000.mat.metis_nparts_00001000';
%     };

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% meshRoot = '../Mesh_Handlers/meshfiles2d/koch';
% 
% meshNames = {
%     'koch_iter_3_12x12_bndRefined_0';
%     'koch_iter_3_16x16_bndRefined_0';
%     'koch_iter_3_20x20_bndRefined_0';
%     'koch_iter_3_24x24_bndRefined_0';
%     'koch_iter_3_28x28_bndRefined_0';
%     'koch_iter_3_32x32_bndRefined_0';
%     'koch_iter_3_36x36_bndRefined_0';
%     'koch_iter_3_40x40_bndRefined_0';
%     'koch_iter_3_44x44_bndRefined_0';
%     'koch_iter_3_48x48_bndRefined_0';
%     'koch_iter_3_52x52_bndRefined_0';
%     'koch_iter_3_56x56_bndRefined_0';
%     'koch_iter_3_60x60_bndRefined_0';
%     };

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% meshExt = '.off';
meshExt = '.mat';

nbMeshes = length(meshNames);
hmax     = zeros(nbMeshes, 1);
dofs     = zeros(nbMeshes, 1);
errH1    = zeros(nbMeshes, 1);
errL2    = zeros(nbMeshes, 1);
kappa    = zeros(nbMeshes, 1);

dPkm2 = (k*(k-1)) / 2;

for i = 1:nbMeshes
    meshName = [ meshRoot '/' meshNames{i} meshExt ];

    fprintf('Loading mesh %s ... ', meshName);
    if strcmp(meshExt, '.off')
        mesh = readOFFmesh(meshName);
        mesh = loadmesh(mesh, 0);
    else
        mesh = loadmesh(meshName, 0);
    end
    fprintf('done. ');

    [~, errH1(i), errL2(i), hE, ~, ~, ~, ~, kappa(i)] = ...
        VEM_d_parallel(mesh, k, eoa, compressquad, basis_type, use_mp, i_F, [], dofsLayout);

    hmax(i) = max(hE);
    dofs(i) = mesh.Nver + (k-1) * mesh.Nfc + dPkm2 * mesh.Nelt;
end

fprintf('\nRelative error in the H^1 norm\n');
fprintf('--------------------------------\n');
format shorte
disp(errH1)

fprintf('Order of convergence:\n')
orderH1    = log(errH1(1:end-1)./errH1(2:end)) ./ log(hmax(1:end-1)./hmax(2:end));
orderH1bis = 2 * log(errH1(1:end-1)./errH1(2:end)) ./ log(dofs(2:end)./dofs(1:end-1));
format bank
disp(orderH1)
disp(orderH1bis)

fprintf('\nRelative error in the L^2 norm\n');
fprintf('------------------------------\n');
format shorte
disp(errL2)

fprintf('Order of convergence:\n')
orderL2    = log(errL2(1:end-1)./errL2(2:end)) ./ log(hmax(1:end-1)./hmax(2:end));
orderL2bis = 2 * log(errL2(1:end-1)./errL2(2:end)) ./ log(dofs(2:end)./dofs(1:end-1));
format bank
disp(orderL2)
disp(orderL2bis)

fprintf('\n1-norm condition number estimate\n');
fprintf('----------------------------------\n');
format shorte
disp(kappa)
