%
% Solving 3D Poisson equation on the unit cube with FETI-DP and VEM on
%

addpath ../Mesh_Handlers
addpath ../Quadrature
addpath ../Quadrature/cpp_modules
addpath ../Basis_Functions
addpath ../VEM_diffusion
addpath ../VEM_diffusion_3D
addpath ../FETI3D

fprintf('\nScript for solving Poisson equation in 3D with VEM (k = 1) and FETI-DP\n\n');

if license('test','Distrib_Computing_Toolbox')
    if isempty(gcp)
        parpool
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Inputs

%
% Polynomial degree
%
k = 1;

eoa = input('Extra order of accuracy for quadrature formulas [0]: ');
if isempty(eoa)
   eoa = 0; 
end
compressquad = input('Compress 2D quadrature rules? 0/1 [0]: ');
if isempty(compressquad)
    compressquad = 0;
end

%
% Type of basis functions
%
basis_type = 1;

%
% Multi-precision toolbox
%
use_mp = 0;

%
% Mesh
%

meshroot = './../Mesh_Handlers/meshfiles3d';
meshname = chooseMesh3d( 'Note: make sure to select a mesh of the unit cube' );
meshname = [meshroot '/' meshname];
load([meshname '.mat'])
fprintf('Loaded mesh %s\n', meshname);

%
% FETI-DP  options
%

Nx = input('Number of subdomains in x (>= 2) [2]: ');
if isempty(Nx) || (Nx < 2)
    Nx = 2;
end
Ny = input('Number of subdomains in y (>= 2) [2]: ');
if isempty(Ny) || (Ny < 2)
    Ny = 2;
end
Nz = input('Number of subdomains in z (>= 2) [2]: ');
if isempty(Nz) || (Nz < 2)
    Nz = 2;
end

% Diffusion coefficient
i_rho = input('Diffusion coefficient? (1) ro = 1; (2) rand; (3) rand exp. Choice [1]: ');
if isempty(i_rho)
    i_rho = 1;
end
switch i_rho
    case 1
        rho = ones(Nx, Ny, Nz);
    case 2
        rho = rand(Nx, Ny, Nz);
    case 3
        alpha = randi([-4 4], Nx, Ny, Nz);
        rho = 10.^alpha;
    otherwise
        disp('value not valid for ro ')
end

isVertexPrimal = input('Use vertices in primal space? 0/1 [1]: ');
if isempty(isVertexPrimal)
    isVertexPrimal = 1;
end

isEdgePrimal = input('Use edge averages in primal space? 0/1 [1]: ');
if isempty(isEdgePrimal)
    isEdgePrimal = 1;
end

isFacePrimal = input('Use face averages in primal space? 0/1 [0]: ');
if isempty(isFacePrimal)
    isFacePrimal = 0;
end

if (isVertexPrimal + isEdgePrimal + isFacePrimal) == 0
    error('The primal space cannot be empty')
end

% Choose how to implement the change of basis
approach4constraints = input(['Use explicit change of basis or local Lagrange multipliers ' ...
                              'to edge and face constraints? 1/2 [2]: ']);
if isempty(approach4constraints)
    approach4constraints = 2;
end

% Dirichlet preconditioner for the FETI-DP interface problem
use_pc = input('Use FETI-DP Dirichlet preconditioner? 0/1 [1]: ');
if isempty(use_pc)
    use_pc = 1;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create domain decomposition data structures

load_data = input(['Do you want to load mesh data structures, ' ...
                   'stiffness matrices and load terms from memory? 0/1 [0]: ']);
if isempty(load_data)
    load_data = 0;
end
if load_data == 0
    % Identify internal and boundary nodes on the reference mesh
    [ refVertexDofs, refEdgeInternalDofs, refFaceInternalDofs, ...
      refInternalDofs ] = createPreliminaryDofPartition( mesh, k );

    % Group element faces by interface face
    refElementFacesByBoundary = groupFacesByBoundary( mesh );

    % Replicate reference mesh to create a domain decomposition
    meshArray = repMesh( mesh, Nx, Ny, Nz, ...
                         refVertexDofs, refEdgeInternalDofs, refFaceInternalDofs, ...
                         refElementFacesByBoundary );

    % Identify degrees of freedom on the Dirichlet boundary
    meshArray = ...
        findDirichletDofs( meshArray, Nx, Ny, Nz );

    % Assemble subdomain stiffness matrices and right hand sides
    [ K, F ] = ...
        subdomainAssembler( meshArray, Nx, Ny, Nz, rho, ...
                            k, eoa, compressquad, use_mp );
                        
    save_data = input(['Do you want to save mesh data structures, ' ...
                       'stiffness matrices and load terms? 0/1 [0]: ']);
    if isempty(save_data)
        save_data = 0;
    end
    if save_data ~= 0
        mshName = strsplit(meshname, {'./../Mesh_Handlers/meshfiles3d/', '/'});
        mshName = strjoin(mshName(2:end), '_');
        filename = sprintf(['../FETI3D/workspaces/%s__k_%i__Nx_%i_Ny_%i_Nz_%i__' ...
                            'rho_type_%i__eoa_%i__compressquad_%i__use_mp_%i.mat'], ...
                            mshName, k, Nx, Ny, Nz, i_rho, eoa, compressquad, use_mp );
        save(filename, 'refVertexDofs', 'refEdgeInternalDofs', ...
                       'refFaceInternalDofs', 'refInternalDofs', ...
                       'refElementFacesByBoundary', ...
                       'meshArray', 'K', 'F');
    end
else
    filename = input('Enter filename of the workspace to be loaded: ', 's');
    load(filename)
end

% Compute dimension of the global primal space and of the space of Lagrange
% multipliers \lambda
[ dimPrimal, dimLambda ] = ...
    spacesDim( meshArray, Nx, Ny, Nz, isVertexPrimal, isEdgePrimal, isFacePrimal );

% Construct primal and dual boolean operators and transformation matrices
[ vertexPrimalDofs, vertexDualDofs, ...
  edgeAverageDofs, edgeAverageDofsLocal, edgeDualDofs, ...
  faceAverageDofs, faceAverageDofsLocal, faceDualDofs, ...
  Qedge, Tedge, ...
  Qface, Tface, ...
  Bprimal, Bdual, BdualScaled ] = ...
    subdomainOperators( meshArray, k, eoa, compressquad, use_mp, ...
                        Nx, Ny, Nz, ...
                        isVertexPrimal, isEdgePrimal, isFacePrimal, ...
                        dimPrimal, dimLambda, ...
                        rho );

% Explicitly apply the change of basis if needed
if approach4constraints == 1
    [ K, F ] = applyTransform( meshArray, Nx, Ny, Nz, ...
                               isEdgePrimal, isFacePrimal, ...
                               refInternalDofs, ...
                               vertexPrimalDofs, vertexDualDofs, ...
                               edgeDualDofs, faceDualDofs, Tedge, Tface, K, F );
end

% Assemble some terms of the dual-primal system (i.e. with \lambda and u_c
% as unknowns)
[ Kstar, fstar, dr ] = assembleDualPrimalOperators( meshArray, k, use_mp, ...
                                                    Nx, Ny, Nz, ...
                                                    isEdgePrimal, isFacePrimal, ...
                                                    dimPrimal, dimLambda, ...
                                                    approach4constraints, ...
                                                    refInternalDofs, ...
                                                    vertexPrimalDofs, vertexDualDofs, ...
                                                    edgeAverageDofs, edgeAverageDofsLocal, edgeDualDofs, ...
                                                    faceAverageDofs, faceAverageDofsLocal, faceDualDofs, ...
                                                    Qedge, Tedge, ...
                                                    Qface, Tface, ...
                                                    Bprimal, Bdual, K, F );

% Compute right hand side for the dual interface problem
tmp = Kstar\fstar;
d = dr - actionFIrc( tmp, meshArray, k, use_mp, Nx, Ny, Nz, ...
                     isEdgePrimal, isFacePrimal, dimLambda, approach4constraints, ...
                     refInternalDofs, vertexPrimalDofs, vertexDualDofs, ...
                     edgeAverageDofs, edgeAverageDofsLocal, edgeDualDofs, ...
                     faceAverageDofs, faceAverageDofsLocal, faceDualDofs, ...
                     Qedge, Tedge, Qface, Tface, Bprimal, Bdual, K );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Solution with FETI-DP

tol = input('Enter the tolerance on the relative residual norm [1e-6]: ');
if isempty(tol)
    tol = 1e-6;
end

maxit = input('Enter maximum number of iterations for PCG [ min(dimLambda, 100) ]: ');
if isempty(maxit)
    maxit = min(dimLambda, 100);
end

fprintf('Solving the dual interface problem with PCG ...\n')
[ lambda, relRes, iter, relResVec, eigEst ] = ...
    pcgFETIDP( d, tol, maxit, use_pc, meshArray, k, use_mp, Nx, Ny, Nz, ...
               isEdgePrimal, isFacePrimal, dimPrimal, dimLambda, ...
               approach4constraints, ...
               refInternalDofs, vertexPrimalDofs, vertexDualDofs, ...
               edgeAverageDofs, edgeAverageDofsLocal, edgeDualDofs, ...
               faceAverageDofs, faceAverageDofsLocal, faceDualDofs, ...
               Qedge, Tedge, Qface, Tface, ...
               Bprimal, Bdual, BdualScaled, K, Kstar );

figure()
semilogy(relResVec, '--b')

%
% Eigenvalues estimates
%
fprintf('min(eig) max(eig): %g %g\n', eigEst(1), eigEst(2));
fprintf('pcgFETIDP condest: %g\n', eigEst(2)/eigEst(1));

%
% Compute vector of the primal dofs
%
uPrimal = Kstar \ (fstar + actionFIrcT( lambda, ...
                                        meshArray, k, use_mp, ...
                                        Nx, Ny, Nz, isEdgePrimal, isFacePrimal, ...
                                        dimPrimal, approach4constraints, ...
                                        refInternalDofs, vertexPrimalDofs, vertexDualDofs, ...
                                        edgeAverageDofs, edgeAverageDofsLocal, edgeDualDofs, ...
                                        faceAverageDofs, faceAverageDofsLocal, faceDualDofs, ...
                                        Qedge, Tedge, Qface, Tface, Bprimal, Bdual, K ) );

%
% Reconstruct the solution in each subdomain
%
u = reconstructSolution( lambda, uPrimal, meshArray, k, use_mp, Nx, Ny, Nz, ...
                         isEdgePrimal, isFacePrimal, approach4constraints, ...
                         refInternalDofs, vertexPrimalDofs, vertexDualDofs, ...
                         edgeAverageDofs, edgeAverageDofsLocal, edgeDualDofs, ...
                         faceAverageDofs, faceAverageDofsLocal, faceDualDofs, ...
                         Qedge, Tedge, Qface, Tface, Bprimal, Bdual, K, F );

%
% Error computation
%
sol = exactSolution( meshArray, Nx, Ny, Nz, k, use_mp );

[ relerrEnergy, relerrInf ] = computeErrors( Nx, Ny, Nz, sol, u, K );

fprintf('Relative error energy norm: %f\n', relerrEnergy);
fprintf('Relative error inf norm: %f\n', relerrInf);

%
% Plot solution
%
[ fig1, fig2 ] = plotSolution( meshArray, Nx, Ny, Nz, u, k );



