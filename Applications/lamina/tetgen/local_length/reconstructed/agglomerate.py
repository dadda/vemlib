# -*- coding: utf-8 -*-

###
### This file is generated automatically by SALOME v8.3.0 with dump python functionality
###

import sys
import salome

salome.salome_init()
theStudy = salome.myStudy

import salome_notebook
notebook = salome_notebook.NoteBook(theStudy)
# sys.path.insert( 0, r'/home/daniele/Documents/codes/vem_matlab/Applications/lamina/tetgen/local_length/reconstructed')
sys.path.insert( 0, r'/home/daniele/Documents/codes/vem_matlab/Applications/lamina/salome_old')

###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS
import numpy as np

geompy = geomBuilder.New(theStudy)

###
### SMESH component
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

smesh = smeshBuilder.New(theStudy)

#([Mesh], status) = smesh.CreateMeshesFromMED(r'/home/daniele/Documents/codes/vem_matlab/Applications/lamina/tetgen/local_length/reconstructed/lamina_cell.1.med')
# Mesh_mirrored = Mesh.MirrorObjectMakeMesh( Mesh, SMESH.AxisStruct( 0.147626, 0.104684, 0.103558, 1, 0, 0 ), SMESH.SMESH_MeshEditor.PLANE, 0, 'Mesh_mirrored' )
# Compound_Mesh_1 = smesh.Concatenate([Mesh.GetMesh(), Mesh_mirrored.GetMesh()], 1, 1, 1e-05)
# Compound_Mesh_1_mirrored = Compound_Mesh_1.MirrorObjectMakeMesh( Compound_Mesh_1, SMESH.AxisStruct( 0.104686, 0.147626, 0.0981094, 0, 1, 0 ), SMESH.SMESH_MeshEditor.PLANE, 0, 'Compound_Mesh_1_mirrored' )
# Compound_Mesh_2 = smesh.Concatenate([Compound_Mesh_1.GetMesh(), Compound_Mesh_1_mirrored.GetMesh()], 1, 1, 1e-05)
# Compound_Mesh_2_mirrored = Compound_Mesh_2.MirrorObjectMakeMesh( Compound_Mesh_2, SMESH.AxisStruct( 0.194422, 0.0969442, 0.147626, 0, 0, 1 ), SMESH.SMESH_MeshEditor.PLANE, 0, 'Compound_Mesh_2_mirrored' )
# Compound_Mesh_3 = smesh.Concatenate([Compound_Mesh_2.GetMesh(), Compound_Mesh_2_mirrored.GetMesh()], 1, 1, 1e-05)
# smesh.SetName(Compound_Mesh_3, 'Compound_Mesh_3')

# numGroups = 12

([Mesh], status) = smesh.CreateMeshesFromMED(r'/home/daniele/Documents/codes/vem_matlab/Applications/lamina/salome_old/lamina_cell.1.med')

numGroups = 8
groupList = [0] * numGroups

# metisOutput = np.loadtxt('Compound_Mesh_3_extracted.metis.epart.12')
metisOutput = np.loadtxt(r'/home/daniele/Documents/codes/vem_matlab/Applications/lamina/salome_old/lamina_cell.metis.epart.8')
for i in range(numGroups):
    idx, = np.nonzero( metisOutput == i )
    idx  = idx + 1
    idx  = idx.tolist()
    idx  = [ str(j) for j in idx ]
    idx  = ','.join(idx)
    aCriteria = []
    aCriterion = smesh.GetCriterion(SMESH.VOLUME,SMESH.FT_RangeOfIds,SMESH.FT_Undefined,idx)
    aCriteria.append(aCriterion)
    aFilter_1 = smesh.GetFilterFromCriteria(aCriteria)
#     aFilter_1.SetMesh(Compound_Mesh_3.GetMesh())
#     groupList[i] = Compound_Mesh_3.GroupOnFilter( SMESH.VOLUME, 'Group_%i' % i, aFilter_1 )
    aFilter_1.SetMesh(Mesh.GetMesh())
    groupList[i] = Mesh.GroupOnFilter( SMESH.VOLUME, 'Group_%i' % i, aFilter_1 )


# Group_1 = Compound_Mesh_3.CreateEmptyGroup( SMESH.VOLUME, 'Group_1' )
# nbAdd = Group_1.AddFrom( Compound_Mesh_3.GetMesh() )
# aCriteria = []
# aCriterion = smesh.GetCriterion(SMESH.VOLUME,SMESH.FT_RangeOfIds,SMESH.FT_Undefined,'1, 3, 5')
# aCriteria.append(aCriterion)
# aFilter_1 = smesh.GetFilterFromCriteria(aCriteria)
# aFilter_1.SetMesh(Compound_Mesh_3.GetMesh())
# Group_2 = Compound_Mesh_3.GroupOnFilter( SMESH.VOLUME, 'Group_2', aFilter_1 )


## Set names of Mesh objects
# smesh.SetName(Group_2, 'Group_2')
# smesh.SetName(Compound_Mesh_3.GetMesh(), 'Compound_Mesh_3')
smesh.SetName(Mesh.GetMesh(), 'Mesh')
# smesh.SetName(Compound_Mesh_2_mirrored.GetMesh(), 'Compound_Mesh_2_mirrored')
# smesh.SetName(Mesh.GetMesh(), 'Mesh')
# smesh.SetName(Compound_Mesh_1.GetMesh(), 'Compound_Mesh_1')
# smesh.SetName(Mesh_mirrored.GetMesh(), 'Mesh_mirrored')
# smesh.SetName(Compound_Mesh_2.GetMesh(), 'Compound_Mesh_2')
# smesh.SetName(Compound_Mesh_1_mirrored.GetMesh(), 'Compound_Mesh_1_mirrored')
# smesh.SetName(Group_1, 'Group_1')


if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser(True)
