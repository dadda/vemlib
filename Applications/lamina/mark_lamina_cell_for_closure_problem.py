
import sys
sys.path.insert( 0, r'/home/daniele/Documents/codes/SALOME-8.3.0-UB16.04/')
sys.path.insert( 0, r'/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/')

import readPolyhedralMesh as rPM
import salome

salome.salome_init()
theStudy = salome.myStudy

import salome_notebook
notebook = salome_notebook.NoteBook(theStudy)

import GEOM
from salome.geom import geomBuilder
import SALOMEDS
import numpy as np
import math

###
### Geometrical parameters
###

# Average area of a pore which axon bundles pass through [mm^2] (Jonas 1991)
Ap = 4e-3

# VASCULAR porosity (Balaratnasingam 2014)
phiGoal = 0.1567

# Laminar beam length (Sanders 2006)
L = math.sqrt( 4.0 * Ap / ( phiGoal * ( 6.0 + 4.0 * math.sqrt(2) ) ) )

theta  = math.pi/4.0
varphi = math.pi/4.0

sinTheta = math.sin(theta)
cosTheta = math.sqrt( 1.0 - sinTheta*sinTheta )

sinVarPhi = math.sqrt( 2.0 / 3.0 ) # math.sin(varphi)
cosVarPhi = math.sqrt( 1.0 - sinVarPhi*sinVarPhi )

halfHx = L * (1.0 + sinVarPhi * cosTheta)
halfHy = L * (1.0 + sinVarPhi * sinTheta)
halfHz = L * (1.0 + cosVarPhi)

###
### GEOM component
###

geompy = geomBuilder.New(theStudy)

O  = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)

macroVerticesList = []
for k in range(3):
    for j in range(3):
        for i in range(3):
            macroVerticesList.append( geompy.MakeVertex( i*halfHx, j*halfHy, k*halfHz) )

xFaces = []
for k in [0, 9]:
    for j in [0, 3]:
        for i in [0, 2]:
            xFaces.append( geompy.MakeFaceWires( [ geompy.MakePolyline( \
                                                            [ macroVerticesList[0+i+j+k], \
                                                              macroVerticesList[3+i+j+k], \
                                                              macroVerticesList[12+i+j+k], \
                                                              macroVerticesList[9+i+j+k] ], \
                                                            True ) ], 1) )

yFaces = []
for k in [0, 9]:
    for i in [0, 1]:
        for j in [0, 6]:
            yFaces.append( geompy.MakeFaceWires( [ geompy.MakePolyline( \
                                                            [ macroVerticesList[0+i+j+k], \
                                                              macroVerticesList[1+i+j+k], \
                                                              macroVerticesList[10+i+j+k], \
                                                              macroVerticesList[9+i+j+k] ], \
                                                            True ) ], 1) )

zFaces = []
for j in [0, 3]:
    for i in [0, 1]:
        for k in [0, 18]:
            zFaces.append( geompy.MakeFaceWires( [ geompy.MakePolyline( \
                                                            [ macroVerticesList[0+i+j+k], \
                                                              macroVerticesList[1+i+j+k], \
                                                              macroVerticesList[4+i+j+k], \
                                                              macroVerticesList[3+i+j+k] ], \
                                                            True ) ], 1) )

xCompound00 = geompy.MakeCompound( [ xFaces[0], xFaces[1] ])
xCompound10 = geompy.MakeCompound( [ xFaces[2], xFaces[3] ])
xCompound01 = geompy.MakeCompound( [ xFaces[4], xFaces[5] ])
xCompound11 = geompy.MakeCompound( [ xFaces[6], xFaces[7] ])

yCompound00 = geompy.MakeCompound( [ yFaces[0], yFaces[1] ])
yCompound10 = geompy.MakeCompound( [ yFaces[2], yFaces[3] ])
yCompound01 = geompy.MakeCompound( [ yFaces[4], yFaces[5] ])
yCompound11 = geompy.MakeCompound( [ yFaces[6], yFaces[7] ])

zCompound00 = geompy.MakeCompound( [ zFaces[0], zFaces[1] ])
zCompound10 = geompy.MakeCompound( [ zFaces[2], zFaces[3] ])
zCompound01 = geompy.MakeCompound( [ zFaces[4], zFaces[5] ])
zCompound11 = geompy.MakeCompound( [ zFaces[6], zFaces[7] ])

geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( xCompound00, 'xCompound00' )
geompy.addToStudy( xCompound10, 'xCompound10' )
geompy.addToStudy( xCompound01, 'xCompound01' )
geompy.addToStudy( xCompound11, 'xCompound11' )
geompy.addToStudy( yCompound00, 'yCompound00' )
geompy.addToStudy( yCompound10, 'yCompound10' )
geompy.addToStudy( yCompound01, 'yCompound01' )
geompy.addToStudy( yCompound11, 'yCompound11' )
geompy.addToStudy( zCompound00, 'zCompound00' )
geompy.addToStudy( zCompound10, 'zCompound10' )
geompy.addToStudy( zCompound01, 'zCompound01' )
geompy.addToStudy( zCompound11, 'zCompound11' )

###
### SMESH component
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

#
# Read dual mesh
#

basename = r''

filename = r'/home/daniele/Documents/codes/vem_matlab/Applications/lamina/tetgen/local_length/' \
           r'reconstructed/' \
           r'lamina_cell.1.ncells_3086.ovm_replicated_x.ovm_replicated_y.ovm_replicated_z.ovm'

# [ node, edges, f2he, p2hf ] = rPM.readPMGOmesh( basename )
[ node, edges, f2he, p2hf ] = rPM.readOVMmesh( filename )

smesh  = smeshBuilder.New(theStudy)
Mesh_1 = smesh.Mesh()

Nver = node.shape[0]
Ned  = edges.shape[0]
Nfc  = len(f2he)
Npol = len(p2hf)

for i in range(Nver):
    Mesh_1.AddNode( node[i,0], node[i,1], node[i,2] )

for i in range(Ned):
    Mesh_1.AddEdge( [ j+1 for j in edges[i,:] ] )

for i in range(Npol):
    hFaces = p2hf[i]
    tFaces = hFaces/2
    NfcLoc = len(hFaces)
    polyhedron = [0] * NfcLoc
    NedByFace  = [0] * NfcLoc
    for j in range(NfcLoc):
        hEdges = f2he[tFaces[j]]
        tEdges = hEdges/2
        edgesOrientation = hEdges%2
        edgesLoc = edges[tEdges,:].T
        polyhedron[j] = (1 - hFaces[j]%2) * ( (1-edgesOrientation) * edgesLoc[0,:] \
                                              + edgesOrientation * edgesLoc[1,:] ) \
                        + (hFaces[j]%2) * ( (1-edgesOrientation[-1::-1]) * edgesLoc[1,-1::-1] \
                                            + edgesOrientation[-1::-1] * edgesLoc[0,-1::-1] )
        polyhedron[j] = polyhedron[j] + 1
        NedByFace[j] = len(polyhedron[j])
    Mesh_1.AddPolyhedralVolume( np.hstack(polyhedron).tolist(), NedByFace )

for i in range(Nfc):
    hEdges = f2he[i]
    tEdges = hEdges/2
    edgesOrientation = hEdges%2
    edgesLoc = edges[tEdges,:].T
    face = (1-edgesOrientation) * edgesLoc[0,:] + edgesOrientation * edgesLoc[1,:]
    face = face + 1
    Mesh_1.AddPolygonalFace( face.tolist() )


#
# x groups
#

aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.FACE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,xCompound00)
aCriteria.append(aCriterion)
aFilter_1 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_1.SetMesh(Mesh_1.GetMesh())
xFaces00 = Mesh_1.GroupOnFilter( SMESH.FACE, 'xFaces00', aFilter_1 )

aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.FACE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,xCompound10)
aCriteria.append(aCriterion)
aFilter_1 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_1.SetMesh(Mesh_1.GetMesh())
xFaces10 = Mesh_1.GroupOnFilter( SMESH.FACE, 'xFaces10', aFilter_1 )

aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.FACE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,xCompound01)
aCriteria.append(aCriterion)
aFilter_1 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_1.SetMesh(Mesh_1.GetMesh())
xFaces01 = Mesh_1.GroupOnFilter( SMESH.FACE, 'xFaces01', aFilter_1 )

aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.FACE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,xCompound11)
aCriteria.append(aCriterion)
aFilter_1 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_1.SetMesh(Mesh_1.GetMesh())
xFaces11 = Mesh_1.GroupOnFilter( SMESH.FACE, 'xFaces11', aFilter_1 )

#
# y groups
#

aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.FACE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,yCompound00)
aCriteria.append(aCriterion)
aFilter_1 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_1.SetMesh(Mesh_1.GetMesh())
yFaces00 = Mesh_1.GroupOnFilter( SMESH.FACE, 'yFaces00', aFilter_1 )

aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.FACE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,yCompound10)
aCriteria.append(aCriterion)
aFilter_1 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_1.SetMesh(Mesh_1.GetMesh())
yFaces10 = Mesh_1.GroupOnFilter( SMESH.FACE, 'yFaces10', aFilter_1 )

aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.FACE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,yCompound01)
aCriteria.append(aCriterion)
aFilter_1 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_1.SetMesh(Mesh_1.GetMesh())
yFaces01 = Mesh_1.GroupOnFilter( SMESH.FACE, 'yFaces01', aFilter_1 )

aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.FACE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,yCompound11)
aCriteria.append(aCriterion)
aFilter_1 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_1.SetMesh(Mesh_1.GetMesh())
yFaces11 = Mesh_1.GroupOnFilter( SMESH.FACE, 'yFaces11', aFilter_1 )

#
# z groups
#

aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.FACE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,zCompound00)
aCriteria.append(aCriterion)
aFilter_1 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_1.SetMesh(Mesh_1.GetMesh())
zFaces00 = Mesh_1.GroupOnFilter( SMESH.FACE, 'zFaces00', aFilter_1 )

aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.FACE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,zCompound10)
aCriteria.append(aCriterion)
aFilter_1 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_1.SetMesh(Mesh_1.GetMesh())
zFaces10 = Mesh_1.GroupOnFilter( SMESH.FACE, 'zFaces10', aFilter_1 )

aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.FACE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,zCompound01)
aCriteria.append(aCriterion)
aFilter_1 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_1.SetMesh(Mesh_1.GetMesh())
zFaces01 = Mesh_1.GroupOnFilter( SMESH.FACE, 'zFaces01', aFilter_1 )

aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.FACE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,zCompound11)
aCriteria.append(aCriterion)
aFilter_1 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_1.SetMesh(Mesh_1.GetMesh())
zFaces11 = Mesh_1.GroupOnFilter( SMESH.FACE, 'zFaces11', aFilter_1 )

## Set names of Mesh objects
smesh.SetName(xFaces00, 'xFaces00')
smesh.SetName(xFaces10, 'xFaces10')
smesh.SetName(xFaces01, 'xFaces01')
smesh.SetName(xFaces11, 'xFaces11')
smesh.SetName(yFaces00, 'yFaces00')
smesh.SetName(yFaces10, 'yFaces10')
smesh.SetName(yFaces01, 'yFaces01')
smesh.SetName(yFaces11, 'yFaces11')
smesh.SetName(zFaces00, 'zFaces00')
smesh.SetName(zFaces10, 'zFaces10')
smesh.SetName(zFaces01, 'zFaces01')
smesh.SetName(zFaces11, 'zFaces11')
smesh.SetName(Mesh_1.GetMesh(), 'Mesh_1')

if salome.sg.hasDesktop():
    salome.sg.updateObjBrowser(True)
