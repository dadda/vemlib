# -*- coding: utf-8 -*-

###
### This file is generated automatically by SALOME v8.3.0 with dump python functionality
###

import sys
import salome

salome.salome_init()
theStudy = salome.myStudy

import salome_notebook
notebook = salome_notebook.NoteBook(theStudy)
sys.path.insert( 0, r'/home/daniele/Documents/codes/vem_matlab/Applications/lamina')

###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS


geompy = geomBuilder.New(theStudy)

O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)
P1 = geompy.MakeVertex(0, 0.5, 0.5)
P2 = geompy.MakeVertex(0.5, 0, 0.5)
P3 = geompy.MakeVertex(0.5, 0.5, 0)
P4 = geompy.MakeVertex(0.5, 0.5, 0.5)
P5 = geompy.MakeVertex(1.07735, 1.07735, 1.07735)
P6 = geompy.MakeVertex(1.57735, 1.07735, 1.07735)
P7 = geompy.MakeVertex(1.07735, 1.57735, 1.07735)
P8 = geompy.MakeVertex(1.07735, 1.07735, 1.57735)
Line_1 = geompy.MakeLineTwoPnt(P1, P4)
Line_2 = geompy.MakeLineTwoPnt(P2, P4)
Line_3 = geompy.MakeLineTwoPnt(P3, P4)
Line_4 = geompy.MakeLineTwoPnt(P4, P5)
Line_5 = geompy.MakeLineTwoPnt(P5, P6)
Line_6 = geompy.MakeLineTwoPnt(P5, P7)
Line_7 = geompy.MakeLineTwoPnt(P5, P8)
Circle_1 = geompy.MakeCircle(P1, Line_1, 0.1)
Circle_2 = geompy.MakeCircle(P2, Line_2, 0.1)
Circle_3 = geompy.MakeCircle(P3, Line_3, 0.1)
Circle_4 = geompy.MakeCircle(P4, Line_4, 0.1)
Circle_5 = geompy.MakeCircle(P5, Line_5, 0.1)
Circle_6 = geompy.MakeCircle(P5, Line_6, 0.1)
Circle_7 = geompy.MakeCircle(P5, Line_7, 0.1)
Face_1 = geompy.MakeFaceWires([Circle_1], 1)
Face_2 = geompy.MakeFaceWires([Circle_2], 1)
Face_3 = geompy.MakeFaceWires([Circle_3], 1)
Face_4 = geompy.MakeFaceWires([Circle_4], 1)
Face_5 = geompy.MakeFaceWires([Circle_5], 1)
Face_6 = geompy.MakeFaceWires([Circle_6], 1)
Face_7 = geompy.MakeFaceWires([Circle_7], 1)
Pipe_1 = geompy.MakePipe(Face_1, Line_1)
Pipe_2 = geompy.MakePipe(Face_2, Line_2)
Pipe_3 = geompy.MakePipe(Face_3, Line_3)
Pipe_4 = geompy.MakePipe(Face_4, Line_4)
Pipe_5 = geompy.MakePipe(Face_5, Line_5)
Pipe_6 = geompy.MakePipe(Face_6, Line_6)
Pipe_7 = geompy.MakePipe(Face_7, Line_7)
Fuse_1 = geompy.MakeFuseList([Pipe_1, Pipe_2, Pipe_3, Pipe_4, Pipe_5, Pipe_6, Pipe_7], True, True)
Fuse_1_face_78 = geompy.GetSubShape(Fuse_1, [78])
geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( P1, 'P1' )
geompy.addToStudy( P2, 'P2' )
geompy.addToStudy( P3, 'P3' )
geompy.addToStudy( P4, 'P4' )
geompy.addToStudy( Line_3, 'Line_3' )
geompy.addToStudy( Circle_3, 'Circle_3' )
geompy.addToStudy( P5, 'P5' )
geompy.addToStudy( P6, 'P6' )
geompy.addToStudy( Line_5, 'Line_5' )
geompy.addToStudy( Circle_5, 'Circle_5' )
geompy.addToStudy( Line_4, 'Line_4' )
geompy.addToStudy( P8, 'P8' )
geompy.addToStudy( Line_7, 'Line_7' )
geompy.addToStudy( P7, 'P7' )
geompy.addToStudy( Line_6, 'Line_6' )
geompy.addToStudy( Circle_6, 'Circle_6' )
geompy.addToStudy( Line_2, 'Line_2' )
geompy.addToStudy( Circle_2, 'Circle_2' )
geompy.addToStudy( Circle_7, 'Circle_7' )
geompy.addToStudy( Line_1, 'Line_1' )
geompy.addToStudy( Circle_1, 'Circle_1' )
geompy.addToStudy( Face_1, 'Face_1' )
geompy.addToStudy( Circle_4, 'Circle_4' )
geompy.addToStudy( Face_2, 'Face_2' )
geompy.addToStudy( Face_3, 'Face_3' )
geompy.addToStudy( Face_4, 'Face_4' )
geompy.addToStudy( Face_5, 'Face_5' )
geompy.addToStudy( Face_6, 'Face_6' )
geompy.addToStudy( Face_7, 'Face_7' )
geompy.addToStudy( Pipe_1, 'Pipe_1' )
geompy.addToStudy( Pipe_2, 'Pipe_2' )
geompy.addToStudy( Pipe_3, 'Pipe_3' )
geompy.addToStudy( Pipe_4, 'Pipe_4' )
geompy.addToStudy( Pipe_5, 'Pipe_5' )
geompy.addToStudy( Pipe_6, 'Pipe_6' )
geompy.addToStudy( Pipe_7, 'Pipe_7' )
geompy.addToStudy( Fuse_1, 'Fuse_1' )
geompy.addToStudyInFather( Fuse_1, Fuse_1_face_78, 'Fuse_1:face_78' )

###
### SMESH component
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

smesh = smeshBuilder.New(theStudy)




if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser(True)
