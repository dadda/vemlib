# -*- coding: utf-8 -*-

###
### This file is generated automatically by SALOME v8.3.0 with dump python functionality
###

import sys
import salome

salome.salome_init()
theStudy = salome.myStudy

import salome_notebook
notebook = salome_notebook.NoteBook(theStudy)
sys.path.insert( 0, r'/home/daniele/Documents/codes/SALOME-8.3.0-UB16.04/lamina_cribrosa')

import math

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

import GEOM
from salome.geom import geomBuilder

import SALOMEDS

###
### Geometrical parameters
###

# Average area of a pore which axon bundles pass through [micron^2] (Jonas 1991)
Ap = 4e3

# VASCULAR porosity (Balaratnasingam 2014)
phiGoal = 0.1567

# Laminar beam length (Sanders 2006)
L = math.sqrt( 4.0 * Ap / ( phiGoal * ( 6.0 + 4.0 * math.sqrt(2) ) ) )

theta  = math.pi/4.0
varphi = math.pi/4.0

sinTheta = math.sin(theta)
cosTheta = math.sqrt( 1.0 - sinTheta*sinTheta )

sinVarPhi = math.sqrt( 2.0 / 3.0 ) # math.sin(varphi)
cosVarPhi = math.sqrt( 1.0 - sinVarPhi*sinVarPhi )

halfHx = L * (1.0 + sinVarPhi * cosTheta)
halfHy = L * (1.0 + sinVarPhi * sinTheta)
halfHz = L * (1.0 + cosVarPhi)

# Initial value for the laminar beam thickness [micron]
thickness  = 0.2486 * L

###
### Mesh parameters
###

# The closer this value to 1, the finer the mesh
finess1d = 0.1

###
### GEOM component
###

geompy = geomBuilder.New(theStudy)

O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)
P1 = geompy.MakeVertex(0, L/2.0, L/2.0)
P2 = geompy.MakeVertex(L/2.0, 0, L/2.0)
P3 = geompy.MakeVertex(L/2.0, L/2.0, 0)
P4 = geompy.MakeVertex(L/2.0, L/2.0, L/2.0)
P5 = geompy.MakeVertex(halfHx-L/2.0, halfHy-L/2.0, halfHz-L/2.0)
P6 = geompy.MakeVertex(halfHx,       halfHy-L/2.0, halfHz-L/2.0)
P7 = geompy.MakeVertex(halfHx-L/2.0, halfHy,       halfHz-L/2.0)
P8 = geompy.MakeVertex(halfHx-L/2.0, halfHy-L/2.0, halfHz)
Line_1 = geompy.MakeLineTwoPnt(P1, P4)
Line_2 = geompy.MakeLineTwoPnt(P2, P4)
Line_3 = geompy.MakeLineTwoPnt(P3, P4)
Line_4 = geompy.MakeLineTwoPnt(P4, P5)
Line_5 = geompy.MakeLineTwoPnt(P5, P6)
Line_6 = geompy.MakeLineTwoPnt(P5, P7)
Line_7 = geompy.MakeLineTwoPnt(P5, P8)
Circle_1 = geompy.MakeCircle(P1, Line_1, thickness)
Circle_2 = geompy.MakeCircle(P2, Line_2, thickness)
Circle_3 = geompy.MakeCircle(P3, Line_3, thickness)
Circle_4 = geompy.MakeCircle(P4, Line_4, thickness)
Circle_5 = geompy.MakeCircle(P5, Line_5, thickness)
Circle_6 = geompy.MakeCircle(P5, Line_6, thickness)
Circle_7 = geompy.MakeCircle(P5, Line_7, thickness)
Face_1 = geompy.MakeFaceWires([Circle_1], 1)
Face_2 = geompy.MakeFaceWires([Circle_2], 1)
Face_3 = geompy.MakeFaceWires([Circle_3], 1)
Face_4 = geompy.MakeFaceWires([Circle_4], 1)
Face_5 = geompy.MakeFaceWires([Circle_5], 1)
Face_6 = geompy.MakeFaceWires([Circle_6], 1)
Face_7 = geompy.MakeFaceWires([Circle_7], 1)
Pipe_1 = geompy.MakePipe(Face_1, Line_1)
Pipe_2 = geompy.MakePipe(Face_2, Line_2)
Pipe_3 = geompy.MakePipe(Face_3, Line_3)
Pipe_4 = geompy.MakePipe(Face_4, Line_4)
Pipe_5 = geompy.MakePipe(Face_5, Line_5)
Pipe_6 = geompy.MakePipe(Face_6, Line_6)
Pipe_7 = geompy.MakePipe(Face_7, Line_7)
Fuse_1 = geompy.MakeFuseList([Pipe_1, Pipe_2, Pipe_3, Pipe_4, Pipe_5, Pipe_6, Pipe_7], True, True)
Plane_1 = geompy.MakePlane(P6, OX, 2000)
Plane_2 = geompy.MakePlane(P7, OY, 2000)
Plane_3 = geompy.MakePlane(P8, OZ, 2000)
Mirror_1 = geompy.MakeMirrorByPlane(Fuse_1, Plane_1)
Mirror_2_1 = geompy.MakeMirrorByPlane(Fuse_1, Plane_2)
Mirror_2_2 = geompy.MakeMirrorByPlane(Mirror_1, Plane_2)
Mirror_2_3 = geompy.MakeMirrorByPlane(Fuse_1, Plane_3)
Mirror_2_4 = geompy.MakeMirrorByPlane(Mirror_1, Plane_3)
Mirror_2_5 = geompy.MakeMirrorByPlane(Mirror_2_1, Plane_3)
Mirror_2_6 = geompy.MakeMirrorByPlane(Mirror_2_2, Plane_3)
Fuse_2 = geompy.MakeFuseList([Fuse_1, Mirror_1, Mirror_2_1, Mirror_2_2, Mirror_2_3, Mirror_2_4, Mirror_2_5, Mirror_2_6], True, True)
Fuse_2_Faces    = geompy.ExtractShapes(Fuse_2, geompy.ShapeType["FACE"], True)
Fuse_2_Edges    = geompy.ExtractShapes(Fuse_2, geompy.ShapeType["EDGE"], True)
Fuse_2_Vertices = geompy.ExtractShapes(Fuse_2, geompy.ShapeType["VERTEX"], True)
Face_compound   = geompy.MakeCompound(Fuse_2_Faces)
Edge_compound   = geompy.MakeCompound(Fuse_2_Edges)
Vertex_compound = geompy.MakeCompound(Fuse_2_Vertices)
geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( P1, 'P1' )
geompy.addToStudy( P2, 'P2' )
geompy.addToStudy( P3, 'P3' )
geompy.addToStudy( P4, 'P4' )
geompy.addToStudy( Line_3, 'Line_3' )
geompy.addToStudy( Circle_3, 'Circle_3' )
geompy.addToStudy( P5, 'P5' )
geompy.addToStudy( P6, 'P6' )
geompy.addToStudy( Line_5, 'Line_5' )
geompy.addToStudy( Circle_5, 'Circle_5' )
geompy.addToStudy( Line_4, 'Line_4' )
geompy.addToStudy( P8, 'P8' )
geompy.addToStudy( Line_7, 'Line_7' )
geompy.addToStudy( P7, 'P7' )
geompy.addToStudy( Line_6, 'Line_6' )
geompy.addToStudy( Circle_6, 'Circle_6' )
geompy.addToStudy( Line_2, 'Line_2' )
geompy.addToStudy( Circle_2, 'Circle_2' )
geompy.addToStudy( Circle_7, 'Circle_7' )
geompy.addToStudy( Line_1, 'Line_1' )
geompy.addToStudy( Circle_1, 'Circle_1' )
geompy.addToStudy( Face_1, 'Face_1' )
geompy.addToStudy( Circle_4, 'Circle_4' )
geompy.addToStudy( Face_2, 'Face_2' )
geompy.addToStudy( Face_3, 'Face_3' )
geompy.addToStudy( Face_4, 'Face_4' )
geompy.addToStudy( Face_5, 'Face_5' )
geompy.addToStudy( Face_6, 'Face_6' )
geompy.addToStudy( Face_7, 'Face_7' )
geompy.addToStudy( Pipe_1, 'Pipe_1' )
geompy.addToStudy( Pipe_2, 'Pipe_2' )
geompy.addToStudy( Pipe_3, 'Pipe_3' )
geompy.addToStudy( Pipe_4, 'Pipe_4' )
geompy.addToStudy( Pipe_5, 'Pipe_5' )
geompy.addToStudy( Pipe_6, 'Pipe_6' )
geompy.addToStudy( Pipe_7, 'Pipe_7' )
geompy.addToStudy( Fuse_1, 'Fuse_1' )
geompy.addToStudy( Plane_1, 'Plane_1' )
geompy.addToStudy( Plane_2, 'Plane_2' )
geompy.addToStudy( Plane_3, 'Plane_3' )
geompy.addToStudy( Mirror_1, 'Mirror_1' )
geompy.addToStudy( Mirror_2_1, 'Mirror_2_1' )
geompy.addToStudy( Mirror_2_2, 'Mirror_2_2' )
geompy.addToStudy( Mirror_2_3, 'Mirror_2_3' )
geompy.addToStudy( Mirror_2_4, 'Mirror_2_4' )
geompy.addToStudy( Mirror_2_5, 'Mirror_2_5' )
geompy.addToStudy( Mirror_2_6, 'Mirror_2_6' )
geompy.addToStudy( Fuse_2, 'Fuse_2' )
for i in range(len(Fuse_2_Faces)):
  geompy.addToStudyInFather( Fuse_2, Fuse_2_Faces[i], 'Face_' + str(i+1) )
for i in range(len(Fuse_2_Edges)):
  geompy.addToStudyInFather( Fuse_2, Fuse_2_Edges[i], 'Edge_' + str(i+1) )
for i in range(len(Fuse_2_Vertices)):
  geompy.addToStudyInFather( Fuse_2, Fuse_2_Vertices[i], 'Vertex_' + str(i+1) )
geompy.addToStudy( Face_compound, 'Face_compound' )
geompy.addToStudy( Edge_compound, 'Edge_compound' )
geompy.addToStudy( Vertex_compound, 'Vertex_compound' )

###
### SMESH component
###

smesh = smeshBuilder.New(theStudy)
Mesh_1 = smesh.Mesh(Fuse_2)
Regular_1D = Mesh_1.Segment()
Automatic_Length_1 = Regular_1D.AutomaticLength(finess1d)
MEFISTO_2D = Mesh_1.Triangle(algo=smeshBuilder.MEFISTO)
NETGEN_3D = Mesh_1.Tetrahedron()
isDone = Mesh_1.Compute()

# Check porosity
BoundingBox    = Mesh_1.BoundingBox()
BoundingVolume = ( BoundingBox[3] - BoundingBox[0] ) * ( BoundingBox[4] - BoundingBox[1] ) * ( BoundingBox[5] - BoundingBox[2] )
currentPhi     = Mesh_1.GetVolume() / BoundingVolume
print 'Current value of phi is', currentPhi

aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_LyingOnGeom,SMESH.FT_Undefined,Face_compound)
aCriteria.append(aCriterion)
aFilter_1 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_1.SetMesh(Mesh_1.GetMesh())
nodes_on_model_faces = Mesh_1.GroupOnFilter( SMESH.NODE, 'nodes_on_model_faces', aFilter_1 )
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_LyingOnGeom,SMESH.FT_Undefined,Edge_compound)
aCriteria.append(aCriterion)
aFilter_2 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_2.SetMesh(Mesh_1.GetMesh())
nodes_on_model_edges = Mesh_1.GroupOnFilter( SMESH.NODE, 'nodes_on_model_edges', aFilter_2 )
all_nodes = Mesh_1.CreateEmptyGroup( SMESH.NODE, 'all_nodes' )
nbAdd = all_nodes.AddFrom( Mesh_1.GetMesh() )
internal_nodes = Mesh_1.GetMesh().CutListOfGroups( [ all_nodes ], [ nodes_on_model_faces ], 'internal_nodes' )


## Set names of Mesh objects
smesh.SetName(Regular_1D.GetAlgorithm(), 'Regular_1D')
smesh.SetName(NETGEN_3D.GetAlgorithm(), 'NETGEN 3D')
smesh.SetName(MEFISTO_2D.GetAlgorithm(), 'MEFISTO_2D')
smesh.SetName(Automatic_Length_1, 'Automatic Length_1')
smesh.SetName(Mesh_1.GetMesh(), 'Mesh_1')
smesh.SetName(nodes_on_model_edges, 'nodes_on_model_edges')
smesh.SetName(all_nodes, 'all_nodes')
smesh.SetName(nodes_on_model_faces, 'nodes_on_model_faces')
smesh.SetName(internal_nodes, 'internal_nodes')


if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser(True)

# smesh = smeshBuilder.New(theStudy)
# smeshObj_1 = smesh.CreateHypothesis('MaxLength')
# smeshObj_2 = smesh.CreateHypothesis('AutomaticLength')
# smeshObj_2.SetFineness( 0.2 )
# smeshObj_3 = smesh.CreateHypothesis('Regular_1D')
# smeshObj_4 = smesh.CreateHypothesis('MEFISTO_2D')
# smeshObj_5 = smesh.CreateHypothesis('NETGEN_3D', 'NETGENEngine')
# smeshObj_6 = smesh.CreateHypothesis('MaxLength')
# Mesh_1 = smesh.Mesh(Fuse_2)
# Regular_1D = Mesh_1.Segment()
# Automatic_Length_1 = Regular_1D.AutomaticLength(0.1)
# MEFISTO_2D = Mesh_1.Triangle(algo=smeshBuilder.MEFISTO)
# NETGEN_3D = Mesh_1.Tetrahedron()
# isDone = Mesh_1.Compute()
# aCriteria = []
# aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,Face_compound)
# aCriteria.append(aCriterion)
# aFilter_1 = smesh.GetFilterFromCriteria(aCriteria)
# aFilter_1.SetMesh(Mesh_1.GetMesh())
# nodes_on_model_faces = Mesh_1.GroupOnFilter( SMESH.NODE, 'nodes_model_faces', aFilter_1 )
# aCriteria = []
# aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,Edge_compound)
# aCriteria.append(aCriterion)
# aFilter_2 = smesh.GetFilterFromCriteria(aCriteria)
# aFilter_2.SetMesh(Mesh_1.GetMesh())
# nodes_on_model_edges = Mesh_1.GroupOnFilter( SMESH.NODE, 'nodes_model_edges', aFilter_2 )
# aCriteria = []
# aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,Vertex_compound)
# aCriteria.append(aCriterion)
# aFilter_3 = smesh.GetFilterFromCriteria(aCriteria)
# aFilter_3.SetMesh(Mesh_1.GetMesh())
# smeshObj_7 = Mesh_1.GroupOnFilter( SMESH.NODE, 'nodes_model_vertices', aFilter_3 )
# smeshObj_8 = Mesh_1.GetMesh().CutListOfGroups( [ nodes_on_model_edges ], [ smeshObj_7 ], 'internal_nodes_model_edges' )
# smeshObj_9 = Mesh_1.GetMesh().CutListOfGroups( [ nodes_on_model_faces ], [ nodes_on_model_edges ], 'nodes_internal_to_model_faces' )
# smeshObj_8.SetName( 'nodes_internal_to_model_edges' )
# try:
#   Mesh_1.ExportDAT( r'/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/drawings/Mesh_1.dat' )
#   pass
# except:
#   print 'ExportDAT() failed. Invalid file name?'
# [ nodes_on_model_faces, nodes_on_model_edges, smeshObj_7, smeshObj_8, smeshObj_9 ] = Mesh_1.GetGroups()
# smesh.SetName(Mesh_1, 'Mesh_1')
# try:
#   Mesh_1.ExportMED( r'/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/drawings/Mesh_1.med', 0, SMESH.MED_V2_2, 1, None ,1)
#   pass
# except:
#   print 'ExportToMEDX() failed. Invalid file name?'
# smesh.SetName(Mesh_1, 'Mesh_1')
# try:
#   Mesh_1.ExportMED( r'/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/drawings/Mesh_1.med', 1, SMESH.MED_V2_2, 1, Mesh_1, 1, [], 'efsv' )
#   pass
# except:
#   print 'ExportPartToMED() failed. Invalid file name?'
# try:
#   Mesh_1.ExportDAT( r'/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/drawings/Mesh_1.dat' )
#   pass
# except:
#   print 'ExportDAT() failed. Invalid file name?'
# [ nodes_on_model_faces, nodes_on_model_edges, smeshObj_7, smeshObj_8, smeshObj_9 ] = Mesh_1.GetGroups()
# smeshObj_9.SetName( '-100000' )
# try:
#   Mesh_1.ExportDAT( r'/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/drawings/Mesh_1.dat' )
#   pass
# except:
#   print 'ExportDAT() failed. Invalid file name?'
# smeshObj_9.SetName( '100000' )
# smesh.SetName(Mesh_1, 'Mesh_1')
# try:
#   Mesh_1.ExportMED( r'/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/drawings/Mesh_1.med', 1, SMESH.MED_V2_2, 1, Mesh_1, 1, [], 'efsv' )
#   pass
# except:
#   print 'ExportPartToMED() failed. Invalid file name?'
# [ nodes_on_model_faces, nodes_on_model_edges, smeshObj_7, smeshObj_8, smeshObj_9 ] = Mesh_1.GetGroups()
# smeshObj_9.SetName( 'nodes_internal_to_model_faces' )
# nodes_on_model_faces.SetName( 'nodes_on_model_faces' )
# nodes_on_model_edges.SetName( 'nodes_on_model_edges' )
# smeshObj_7.SetName( 'nodes_on_model_vertices' )
# Mesh_1.RemoveGroup( smeshObj_9 )
# smeshObj_10 = Mesh_1.GetMesh().CutListOfGroups( [ nodes_on_model_faces ], [ smeshObj_7 ], 'nodes_internal_to_model_faces' )
# Mesh_1.RemoveGroup( smeshObj_10 )
# Mesh_1.RemoveGroup( smeshObj_8 )
# smeshObj_11 = Mesh_1.GetMesh().CutListOfGroups( [ nodes_on_model_edges ], [ smeshObj_7 ], 'nodes_internal_to_model_edges' )
# smeshObj_12 = Mesh_1.GetMesh().CutListOfGroups( [ nodes_on_model_faces ], [ nodes_on_model_edges ], 'nodes_internal_to_model_faces' )
# all_nodes = Mesh_1.CreateEmptyGroup( SMESH.NODE, 'all_nodes' )
# nbAdd = all_nodes.AddFrom( Mesh_1.GetMesh() )
# interior_nodes = Mesh_1.GetMesh().CutListOfGroups( [ all_nodes ], [ nodes_on_model_faces ], 'interior_nodes' )
# smeshObj_13 = Mesh_1.CreateEmptyGroup( SMESH.EDGE, 'all_edges' )
# nbAdd = smeshObj_13.AddFrom( Mesh_1.GetMesh() )
# Mesh_1.RemoveGroupWithContents( smeshObj_13 )
# smeshObj_14 = Mesh_1.CreateEmptyGroup( SMESH.FACE, 'all_faces' )
# nbAdd = smeshObj_14.AddFrom( Mesh_1.GetMesh() )
# aCriteria = []
# aCriterion = smesh.GetCriterion(SMESH.FACE,SMESH.FT_LyingOnGeom,SMESH.FT_Undefined,Face_compound)
# aCriteria.append(aCriterion)
# aFilter_4 = smesh.GetFilterFromCriteria(aCriteria)
# aFilter_4.SetMesh(Mesh_1.GetMesh())
# smeshObj_15 = Mesh_1.GroupOnFilter( SMESH.FACE, 'boundary_faces', aFilter_4 )
# smeshObj_16 = Mesh_1.GetMesh().CutListOfGroups( [ smeshObj_14 ], [ smeshObj_15 ], 'internal_faces' )
# Mesh_1.RemoveGroup( smeshObj_16 )
# Mesh_1.RemoveGroup( smeshObj_15 )
# [ nodes_on_model_faces, nodes_on_model_edges, smeshObj_7, smeshObj_11, smeshObj_12, all_nodes, interior_nodes, smeshObj_14 ] = Mesh_1.GetGroups()
# aCriteria = []
# aCriterion = smesh.GetCriterion(SMESH.FACE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,Face_compound)
# aCriteria.append(aCriterion)
# aFilter_5 = smesh.GetFilterFromCriteria(aCriteria)
# aFilter_5.SetMesh(Mesh_1.GetMesh())
# smeshObj_17 = Mesh_1.GroupOnFilter( SMESH.FACE, 'boundary_faces', aFilter_5 )
# Mesh_1.RemoveGroup( smeshObj_17 )
# aCriteria = []
# aCriterion = smesh.GetCriterion(SMESH.FACE,SMESH.FT_EqualFaces,SMESH.FT_Undefined,0)
# aCriteria.append(aCriterion)
# aFilter_6 = smesh.GetFilterFromCriteria(aCriteria)
# aFilter_6.SetMesh(Mesh_1.GetMesh())
# smeshObj_18 = Mesh_1.GroupOnFilter( SMESH.FACE, 'internal_faces', aFilter_6 )
# try:
#   Mesh_1.ExportDAT( r'/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/drawings/Mesh_1.dat' )
#   pass
# except:
#   print 'ExportDAT() failed. Invalid file name?'
# [ nodes_on_model_faces, nodes_on_model_edges, smeshObj_7, smeshObj_11, smeshObj_12, all_nodes, interior_nodes, smeshObj_14, smeshObj_18 ] = Mesh_1.GetGroups()
# Mesh_1.RemoveGroup( smeshObj_18 )
# Mesh_1.RemoveGroup( smeshObj_14 )
# theNbElems = Mesh_1.Evaluate(Fuse_2)
# smeshObj_19 = smesh.CreateHypothesis('MaxLength')
# Automatic_Length_2 = smesh.CreateHypothesis('AutomaticLength')
# Automatic_Length_2.SetFineness( 0.1 )
# aCriteria = []
# aCriterion = smesh.GetCriterion(SMESH.FACE,SMESH.FT_EntityType,SMESH.FT_Undefined,SMESH.Entity_Triangle)
# aCriteria.append(aCriterion)
# aFilter_7 = smesh.GetFilterFromCriteria(aCriteria)
# aFilter_7.SetMesh(Mesh_1.GetMesh())
# smeshObj_20 = Mesh_1.GroupOnFilter( SMESH.FACE, 'Group_1', aFilter_7 )
# Mesh_1.RemoveGroup( smeshObj_20 )
# smeshObj_21 = Mesh_1.CreateEmptyGroup( SMESH.NODE, 'other_vertices_on_edges' )
# nbAdd = smeshObj_21.Add( [ 87, 29, 46, 98 ] )
# Mesh_1.RemoveGroup( smeshObj_21 )
# smeshObj_22 = Mesh_1.CreateEmptyGroup( SMESH.NODE, 'nodes_on_model_vertices_4_edges_in' )
# nbAdd = smeshObj_22.Add( [ 57, 56, 100, 70, 31, 18, 37, 19 ] )
# smeshObj_23 = Mesh_1.CreateEmptyGroup( SMESH.NODE, 'other_nodes_on_edges' )
# nbAdd = smeshObj_23.Add( [ 87, 29, 98, 46, 73, 74, 21, 17 ] )
# smeshObj_24 = Mesh_1.GetMesh().CutListOfGroups( [ smeshObj_7 ], [ smeshObj_22, smeshObj_23 ], 'nodes_on_model_vertices_3_edges_in' )
# Mesh_1.RemoveGroup( smeshObj_11 )
# smeshObj_25 = Mesh_1.GetMesh().CutListOfGroups( [ nodes_on_model_edges ], [ smeshObj_22, smeshObj_24 ], 'nodes_internal_to_model_edges' )
# Mesh_1.RemoveGroup( smeshObj_12 )
# smeshObj_26 = Mesh_1.GetMesh().CutListOfGroups( [ nodes_on_model_faces ], [ nodes_on_model_edges ], 'nodes_internal_to_model_faces' )
# Mesh_1.RemoveGroup( smeshObj_7 )
# smeshObj_27 = Mesh_1.GetMesh().UnionListOfGroups([ smeshObj_22, smeshObj_24 ], 'nodes_on_model_vertices' )
# Mesh_1.RemoveGroup( smeshObj_27 )
# Mesh_1.RemoveGroup( smeshObj_26 )
# Mesh_1.RemoveGroup( smeshObj_25 )
# Mesh_1.RemoveGroup( smeshObj_24 )
# Mesh_1.RemoveGroup( smeshObj_23 )
# Mesh_1.RemoveGroup( smeshObj_22 )

# ## some objects were removed
# aStudyBuilder = theStudy.NewBuilder()
# SO = theStudy.FindObjectIOR(theStudy.ConvertObjectToIOR(smeshObj_4))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = theStudy.FindObjectIOR(theStudy.ConvertObjectToIOR(smeshObj_19))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = theStudy.FindObjectIOR(theStudy.ConvertObjectToIOR(smeshObj_3))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = theStudy.FindObjectIOR(theStudy.ConvertObjectToIOR(smeshObj_23))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = theStudy.FindObjectIOR(theStudy.ConvertObjectToIOR(smeshObj_6))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = theStudy.FindObjectIOR(theStudy.ConvertObjectToIOR(smeshObj_5))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = theStudy.FindObjectIOR(theStudy.ConvertObjectToIOR(smeshObj_22))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = theStudy.FindObjectIOR(theStudy.ConvertObjectToIOR(smeshObj_2))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = theStudy.FindObjectIOR(theStudy.ConvertObjectToIOR(smeshObj_21))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = theStudy.FindObjectIOR(theStudy.ConvertObjectToIOR(smeshObj_14))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = theStudy.FindObjectIOR(theStudy.ConvertObjectToIOR(smeshObj_27))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = theStudy.FindObjectIOR(theStudy.ConvertObjectToIOR(smeshObj_15))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = theStudy.FindObjectIOR(theStudy.ConvertObjectToIOR(smeshObj_1))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = theStudy.FindObjectIOR(theStudy.ConvertObjectToIOR(smeshObj_26))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = theStudy.FindObjectIOR(theStudy.ConvertObjectToIOR(smeshObj_16))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = theStudy.FindObjectIOR(theStudy.ConvertObjectToIOR(smeshObj_25))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = theStudy.FindObjectIOR(theStudy.ConvertObjectToIOR(smeshObj_17))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = theStudy.FindObjectIOR(theStudy.ConvertObjectToIOR(smeshObj_24))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = theStudy.FindObjectIOR(theStudy.ConvertObjectToIOR(smeshObj_18))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = theStudy.FindObjectIOR(theStudy.ConvertObjectToIOR(smeshObj_20))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = theStudy.FindObjectIOR(theStudy.ConvertObjectToIOR(smeshObj_12))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = theStudy.FindObjectIOR(theStudy.ConvertObjectToIOR(smeshObj_13))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = theStudy.FindObjectIOR(theStudy.ConvertObjectToIOR(smeshObj_7))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = theStudy.FindObjectIOR(theStudy.ConvertObjectToIOR(smeshObj_10))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = theStudy.FindObjectIOR(theStudy.ConvertObjectToIOR(smeshObj_11))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = theStudy.FindObjectIOR(theStudy.ConvertObjectToIOR(smeshObj_8))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = theStudy.FindObjectIOR(theStudy.ConvertObjectToIOR(smeshObj_9))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)

# ## Set names of Mesh objects
# smesh.SetName(Regular_1D.GetAlgorithm(), 'Regular_1D')
# smesh.SetName(NETGEN_3D.GetAlgorithm(), 'NETGEN 3D')
# smesh.SetName(MEFISTO_2D.GetAlgorithm(), 'MEFISTO_2D')
# smesh.SetName(Automatic_Length_1, 'Automatic Length_1')
# smesh.SetName(Automatic_Length_2, 'Automatic Length_2')
# smesh.SetName(interior_nodes, 'interior_nodes')
# smesh.SetName(Mesh_1.GetMesh(), 'Mesh_1')
# smesh.SetName(all_nodes, 'all_nodes')
# smesh.SetName(nodes_on_model_edges, 'nodes_on_model_edges')
# smesh.SetName(nodes_on_model_faces, 'nodes_on_model_faces')


# if salome.sg.hasDesktop():
#   salome.sg.updateObjBrowser(True)
