# -*- coding: utf-8 -*-

###
### This file is generated automatically by SALOME v8.3.0 with dump python functionality
###

import sys
import salome

salome.salome_init()
theStudy = salome.myStudy

import salome_notebook
notebook = salome_notebook.NoteBook(theStudy)
sys.path.insert( 0, r'/home/daniele/Documents/codes/vem_matlab/Applications/lamina')

import math
import numpy as np

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

import GEOM
from salome.geom import geomBuilder

import SALOMEDS

###
### Geometrical parameters
###

# Average area of a pore which axon bundles pass through [micron^2] (Jonas 1991)
Ap = 4e3

# VASCULAR porosity (Balaratnasingam 2014)
phiGoal = 0.1567

# Laminar beam length (Sanders 2006)
L = math.sqrt( 4.0 * Ap / ( phiGoal * ( 6.0 + 4.0 * math.sqrt(2) ) ) )

theta  = math.pi/4.0
varphi = math.pi/4.0

sinTheta = math.sin(theta)
cosTheta = math.sqrt( 1.0 - sinTheta*sinTheta )

sinVarPhi = math.sqrt( 2.0 / 3.0 ) # math.sin(varphi)
cosVarPhi = math.sqrt( 1.0 - sinVarPhi*sinVarPhi )

halfHx = L * (1.0 + sinVarPhi * cosTheta)
halfHy = L * (1.0 + sinVarPhi * sinTheta)
halfHz = L * (1.0 + cosVarPhi)

# Initial value for the laminar beam thickness [micron]
thickness  = 0.2486 * L

###
### Mesh parameters
###

# The closer this value to 1, the finer the mesh
finess1d = 0.1

###
### GEOM component
###

geompy = geomBuilder.New(theStudy)

O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)
P1 = geompy.MakeVertex(0, L/2.0, L/2.0)
P2 = geompy.MakeVertex(L/2.0, 0, L/2.0)
P3 = geompy.MakeVertex(L/2.0, L/2.0, 0)
P4 = geompy.MakeVertex(L/2.0, L/2.0, L/2.0)
P5 = geompy.MakeVertex(halfHx-L/2.0, halfHy-L/2.0, halfHz-L/2.0)
P6 = geompy.MakeVertex(halfHx,       halfHy-L/2.0, halfHz-L/2.0)
P7 = geompy.MakeVertex(halfHx-L/2.0, halfHy,       halfHz-L/2.0)
P8 = geompy.MakeVertex(halfHx-L/2.0, halfHy-L/2.0, halfHz)
Line_1 = geompy.MakeLineTwoPnt(P1, P4)
Line_2 = geompy.MakeLineTwoPnt(P2, P4)
Line_3 = geompy.MakeLineTwoPnt(P3, P4)
Line_4 = geompy.MakeLineTwoPnt(P4, P5)
Line_5 = geompy.MakeLineTwoPnt(P5, P6)
Line_6 = geompy.MakeLineTwoPnt(P5, P7)
Line_7 = geompy.MakeLineTwoPnt(P5, P8)
Circle_1 = geompy.MakeCircle(P1, Line_1, thickness)
Circle_2 = geompy.MakeCircle(P2, Line_2, thickness)
Circle_3 = geompy.MakeCircle(P3, Line_3, thickness)
Circle_4 = geompy.MakeCircle(P4, Line_4, thickness)
Circle_5 = geompy.MakeCircle(P5, Line_5, thickness)
Circle_6 = geompy.MakeCircle(P5, Line_6, thickness)
Circle_7 = geompy.MakeCircle(P5, Line_7, thickness)
Face_1 = geompy.MakeFaceWires([Circle_1], 1)
Face_2 = geompy.MakeFaceWires([Circle_2], 1)
Face_3 = geompy.MakeFaceWires([Circle_3], 1)
Face_4 = geompy.MakeFaceWires([Circle_4], 1)
Face_5 = geompy.MakeFaceWires([Circle_5], 1)
Face_6 = geompy.MakeFaceWires([Circle_6], 1)
Face_7 = geompy.MakeFaceWires([Circle_7], 1)
Pipe_1 = geompy.MakePipe(Face_1, Line_1)
Pipe_2 = geompy.MakePipe(Face_2, Line_2)
Pipe_3 = geompy.MakePipe(Face_3, Line_3)
Pipe_4 = geompy.MakePipe(Face_4, Line_4)
Pipe_5 = geompy.MakePipe(Face_5, Line_5)
Pipe_6 = geompy.MakePipe(Face_6, Line_6)
Pipe_7 = geompy.MakePipe(Face_7, Line_7)
Fuse_1 = geompy.MakeFuseList([Pipe_1, Pipe_2, Pipe_3, Pipe_4, Pipe_5, Pipe_6, Pipe_7], True, True)
Plane_1 = geompy.MakePlane(P6, OX, 2000)
Plane_2 = geompy.MakePlane(P7, OY, 2000)
Plane_3 = geompy.MakePlane(P8, OZ, 2000)
Mirror_1 = geompy.MakeMirrorByPlane(Fuse_1, Plane_1)
Mirror_2_1 = geompy.MakeMirrorByPlane(Fuse_1, Plane_2)
Mirror_2_2 = geompy.MakeMirrorByPlane(Mirror_1, Plane_2)
Mirror_2_3 = geompy.MakeMirrorByPlane(Fuse_1, Plane_3)
Mirror_2_4 = geompy.MakeMirrorByPlane(Mirror_1, Plane_3)
Mirror_2_5 = geompy.MakeMirrorByPlane(Mirror_2_1, Plane_3)
Mirror_2_6 = geompy.MakeMirrorByPlane(Mirror_2_2, Plane_3)
Fuse_2 = geompy.MakeFuseList([Fuse_1, Mirror_1, Mirror_2_1, Mirror_2_2, Mirror_2_3, Mirror_2_4, Mirror_2_5, Mirror_2_6], True, True)
Fuse_2_Faces    = geompy.ExtractShapes(Fuse_2, geompy.ShapeType["FACE"], True)
Fuse_2_Edges    = geompy.ExtractShapes(Fuse_2, geompy.ShapeType["EDGE"], True)
Fuse_2_Vertices = geompy.ExtractShapes(Fuse_2, geompy.ShapeType["VERTEX"], True)
Face_compound   = geompy.MakeCompound(Fuse_2_Faces)
geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( P1, 'P1' )
geompy.addToStudy( P2, 'P2' )
geompy.addToStudy( P3, 'P3' )
geompy.addToStudy( P4, 'P4' )
geompy.addToStudy( Line_3, 'Line_3' )
geompy.addToStudy( Circle_3, 'Circle_3' )
geompy.addToStudy( P5, 'P5' )
geompy.addToStudy( P6, 'P6' )
geompy.addToStudy( Line_5, 'Line_5' )
geompy.addToStudy( Circle_5, 'Circle_5' )
geompy.addToStudy( Line_4, 'Line_4' )
geompy.addToStudy( P8, 'P8' )
geompy.addToStudy( Line_7, 'Line_7' )
geompy.addToStudy( P7, 'P7' )
geompy.addToStudy( Line_6, 'Line_6' )
geompy.addToStudy( Circle_6, 'Circle_6' )
geompy.addToStudy( Line_2, 'Line_2' )
geompy.addToStudy( Circle_2, 'Circle_2' )
geompy.addToStudy( Circle_7, 'Circle_7' )
geompy.addToStudy( Line_1, 'Line_1' )
geompy.addToStudy( Circle_1, 'Circle_1' )
geompy.addToStudy( Face_1, 'Face_1' )
geompy.addToStudy( Circle_4, 'Circle_4' )
geompy.addToStudy( Face_2, 'Face_2' )
geompy.addToStudy( Face_3, 'Face_3' )
geompy.addToStudy( Face_4, 'Face_4' )
geompy.addToStudy( Face_5, 'Face_5' )
geompy.addToStudy( Face_6, 'Face_6' )
geompy.addToStudy( Face_7, 'Face_7' )
geompy.addToStudy( Pipe_1, 'Pipe_1' )
geompy.addToStudy( Pipe_2, 'Pipe_2' )
geompy.addToStudy( Pipe_3, 'Pipe_3' )
geompy.addToStudy( Pipe_4, 'Pipe_4' )
geompy.addToStudy( Pipe_5, 'Pipe_5' )
geompy.addToStudy( Pipe_6, 'Pipe_6' )
geompy.addToStudy( Pipe_7, 'Pipe_7' )
geompy.addToStudy( Fuse_1, 'Fuse_1' )
geompy.addToStudy( Plane_1, 'Plane_1' )
geompy.addToStudy( Plane_2, 'Plane_2' )
geompy.addToStudy( Plane_3, 'Plane_3' )
geompy.addToStudy( Mirror_1, 'Mirror_1' )
geompy.addToStudy( Mirror_2_1, 'Mirror_2_1' )
geompy.addToStudy( Mirror_2_2, 'Mirror_2_2' )
geompy.addToStudy( Mirror_2_3, 'Mirror_2_3' )
geompy.addToStudy( Mirror_2_4, 'Mirror_2_4' )
geompy.addToStudy( Mirror_2_5, 'Mirror_2_5' )
geompy.addToStudy( Mirror_2_6, 'Mirror_2_6' )
geompy.addToStudy( Fuse_2, 'Fuse_2' )
for i in range(len(Fuse_2_Faces)):
  geompy.addToStudyInFather( Fuse_2, Fuse_2_Faces[i], 'Face_' + str(i+1) )
for i in range(len(Fuse_2_Edges)):
  geompy.addToStudyInFather( Fuse_2, Fuse_2_Edges[i], 'Edge_' + str(i+1) )
for i in range(len(Fuse_2_Vertices)):
  geompy.addToStudyInFather( Fuse_2, Fuse_2_Vertices[i], 'Vertex_' + str(i+1) )
geompy.addToStudy( Face_compound, 'Face_compound' )

###
### SMESH component
###

smesh = smeshBuilder.New(theStudy)
Mesh_1 = smesh.Mesh(Fuse_2)
Regular_1D = Mesh_1.Segment()
Automatic_Length_1 = Regular_1D.AutomaticLength(finess1d)
MEFISTO_2D = Mesh_1.Triangle(algo=smeshBuilder.MEFISTO)
NETGEN_3D = Mesh_1.Tetrahedron()
isDone = Mesh_1.Compute()
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_LyingOnGeom,SMESH.FT_Undefined,Face_compound)
aCriteria.append(aCriterion)
aFilter_1 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_1.SetMesh(Mesh_1.GetMesh())
nodes_on_model_faces = Mesh_1.GroupOnFilter( SMESH.NODE, 'nodes_on_model_faces', aFilter_1 )

# Check porosity
BoundingBox    = Mesh_1.BoundingBox()
BoundingVolume = ( BoundingBox[3] - BoundingBox[0] ) * ( BoundingBox[4] - BoundingBox[1] ) * ( BoundingBox[5] - BoundingBox[2] )
currentPhi     = Mesh_1.GetVolume() / BoundingVolume
print 'Current value of phi is', currentPhi

# Save data to text files
all_nodes_flags = [0] * Mesh_1.NbNodes()
for i in range(nodes_on_model_faces.GetNumberOfNodes()):
  nodeID = nodes_on_model_faces.GetID(i+1)
  all_nodes_flags[nodeID-1] = 1

fid = open(r'/home/daniele/Documents/codes/vem_matlab/Applications/lamina/lamina_cell.node', 'w')
fid.write(str(Mesh_1.NbNodes()) + ' 3 0 1\n')
for i in range( Mesh_1.NbNodes() ):
  fid.write(str(i+1) + ' ' + ' '.join('%.16f' % coord for coord in Mesh_1.GetNodeXYZ(i+1)) + ' ' + str(all_nodes_flags[i]) + '\n')
fid.close()

counterT = 0
fidT = open(r'/home/daniele/Documents/codes/vem_matlab/Applications/lamina/lamina_cell.ele', 'w')
fidE = open(r'/home/daniele/Documents/codes/vem_matlab/Applications/lamina/lamina_cell.eONme', 'w')
bndEList = []
fidT.write(str(Mesh_1.NbTetras()) + ' 4 0\n')
fidE.write(str(Mesh_1.NbEdges()) + '\n')
for i in range(Mesh_1.NbElements()):
  numLocalNodes = Mesh_1.GetElemNbNodes(i+1)
  if numLocalNodes == 2:
    fidE.write(' '.join(str(id) for id in sorted(Mesh_1.GetElemNodes(i+1))) + '\n')
  elif numLocalNodes == 3:
    localNodes = Mesh_1.GetElemNodes(i+1)
    bndEList.append( sorted([localNodes[0],localNodes[1]]) )
    bndEList.append( sorted([localNodes[1],localNodes[2]]) )
    bndEList.append( sorted([localNodes[2],localNodes[0]]) )
  elif numLocalNodes == 4:
    counterT = counterT+1
    fidT.write(str(counterT) + ' ' + ' '.join(str(id) for id in Mesh_1.GetElemNodes(i+1)) + '\n')
fidT.close()
fidE.close()

# Remove duplicate rows
bndEList = np.vstack({tuple(row) for row in bndEList})
fid = open(r'/home/daniele/Documents/codes/vem_matlab/Applications/lamina/lamina_cell.bndE', 'w')
fid.write(str(bndEList.shape[0]) + '\n')
for i in range(bndEList.shape[0]):
  fid.write(' '.join(str(id) for id in bndEList[i,:]) + '\n')
fid.close()

## Set names of Mesh objects
smesh.SetName(Regular_1D.GetAlgorithm(), 'Regular_1D')
smesh.SetName(NETGEN_3D.GetAlgorithm(), 'NETGEN 3D')
smesh.SetName(MEFISTO_2D.GetAlgorithm(), 'MEFISTO_2D')
smesh.SetName(Automatic_Length_1, 'Automatic Length_1')
smesh.SetName(Mesh_1.GetMesh(), 'Mesh_1')
smesh.SetName(nodes_on_model_faces, 'nodes_on_model_faces')

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser(True)
