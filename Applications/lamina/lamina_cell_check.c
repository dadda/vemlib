#include "pmgo.h"

int main()
{
  const char * filenameOVM =
    "/home/daniele/Documents/codes/vem_matlab/Applications/lamina/tetgen/"
    "local_length/reconstructed/"
    "lamina_cell.1.ncells_3086.ovm_replicated_x.ovm_replicated_y.ovm_replicated_z.ovm";

  dualMesh dMesh;

  double LSsum, tmp, buffer[3];
  int i;

  dualMeshInitialize( &dMesh );
  dualMeshReadOVM( filenameOVM, &dMesh );
  dualMeshConstructConnectivity( &dMesh );

  /* Check whether all the faces are planar */
  LSsum = 0.0;
#pragma omp parallel for default(none) shared(dMesh)			\
  private(i, buffer, tmp)						\
  schedule(dynamic) reduction(+:LSsum)
  for (i = 0; i < dMesh.Nfc; ++i) {
    leastSquaresFace( &dMesh, dMesh.vertices, i, buffer, &tmp );
    LSsum += tmp;
  }
  printf( "\nLeast squares of entire mesh is %e\n", LSsum);

  dualMeshDestroy( &dMesh );

  return 0;
}

