
import sys
sys.path.insert( 0, r'/home/daniele/Documents/codes/SALOME-8.3.0-UB16.04/')
sys.path.insert( 0, r'/home/daniele/Documents/codes/vem_matlab/Mesh_Handlers/')

import readPolyhedralMesh as rPM
import writePolyhedralMesh as wPM
import salome

salome.salome_init()
theStudy = salome.myStudy

import salome_notebook
notebook = salome_notebook.NoteBook(theStudy)

import GEOM
from salome.geom import geomBuilder
import SALOMEDS
import numpy as np
import math

###
### Geometrical parameters
###

# Average area of a pore which axon bundles pass through [mm^2] (Jonas 1991)
Ap = 4e-3

# VASCULAR porosity (Balaratnasingam 2014)
phiGoal = 0.1567

# Laminar beam length (Sanders 2006)
L = math.sqrt( 4.0 * Ap / ( phiGoal * ( 6.0 + 4.0 * math.sqrt(2) ) ) )

theta  = math.pi/4.0
varphi = math.pi/4.0

sinTheta = math.sin(theta)
cosTheta = math.sqrt( 1.0 - sinTheta*sinTheta )

sinVarPhi = math.sqrt( 2.0 / 3.0 ) # math.sin(varphi)
cosVarPhi = math.sqrt( 1.0 - sinVarPhi*sinVarPhi )

halfHx = L * (1.0 + sinVarPhi * cosTheta)
halfHy = L * (1.0 + sinVarPhi * sinTheta)
halfHz = L * (1.0 + cosVarPhi)

###
### GEOM component
###

geompy = geomBuilder.New(theStudy)

O  = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)

PX = geompy.MakeVertex(halfHx, 0, 0)
PY = geompy.MakeVertex(0, halfHy, 0)
PZ = geompy.MakeVertex(0, 0, halfHz)

PlaneX = geompy.MakePlane(PX, OX, 2000)
PlaneY = geompy.MakePlane(PY, OY, 2000)
PlaneZ = geompy.MakePlane(PZ, OZ, 2000)

geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( PX, 'PX' )
geompy.addToStudy( PY, 'PY' )
geompy.addToStudy( PZ, 'PZ' )
geompy.addToStudy( PlaneX, 'PlaneX' )
geompy.addToStudy( PlaneY, 'PlaneY' )
geompy.addToStudy( PlaneZ, 'PlaneZ' )

###
### SMESH component
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

#
# Read starting dual mesh
#

filename = r'/home/daniele/Documents/codes/vem_matlab/Applications/lamina/tetgen/local_length/' \
           r'reconstructed/' \
           r'lamina_cell.1.ncells_3086.ovm_replicated_x.ovm_replicated_y.ovm'

# Reflection direction
direction = 'z'

[ node, edges, f2he, p2hf ] = rPM.readOVMmesh( filename )

smesh  = smeshBuilder.New(theStudy)
Mesh_1 = smesh.Mesh()

Nver = node.shape[0]
Ned  = edges.shape[0]
Nfc  = len(f2he)
Npol = len(p2hf)

for i in range(Nver):
    Mesh_1.AddNode( node[i,0], node[i,1], node[i,2] )

for i in range(Ned):
    Mesh_1.AddEdge( [ j+1 for j in edges[i,:] ] )

for i in range(Npol):
    hFaces = p2hf[i]
    tFaces = hFaces/2
    NfcLoc = len(hFaces)
    polyhedron = [0] * NfcLoc
    NedByFace  = [0] * NfcLoc
    for j in range(NfcLoc):
        hEdges = f2he[tFaces[j]]
        tEdges = hEdges/2
        edgesOrientation = hEdges%2
        edgesLoc = edges[tEdges,:].T
        polyhedron[j] = (1 - hFaces[j]%2) * ( (1-edgesOrientation) * edgesLoc[0,:] \
                                              + edgesOrientation * edgesLoc[1,:] ) \
                        + (hFaces[j]%2) * ( (1-edgesOrientation[-1::-1]) * edgesLoc[1,-1::-1] \
                                            + edgesOrientation[-1::-1] * edgesLoc[0,-1::-1] )
        polyhedron[j] = polyhedron[j] + 1
        NedByFace[j] = len(polyhedron[j])
    Mesh_1.AddPolyhedralVolume( np.hstack(polyhedron).tolist(), NedByFace )

for i in range(Nfc):
    hEdges = f2he[i]
    tEdges = hEdges/2
    edgesOrientation = hEdges%2
    edgesLoc = edges[tEdges,:].T
    face = (1-edgesOrientation) * edgesLoc[0,:] + edgesOrientation * edgesLoc[1,:]
    face = face + 1
    Mesh_1.AddPolygonalFace( face.tolist() )

all_nodes = Mesh_1.CreateEmptyGroup( SMESH.NODE, 'all_nodes' )
nbAdd = all_nodes.AddFrom( Mesh_1.GetMesh() )
all_edges = Mesh_1.CreateEmptyGroup( SMESH.EDGE, 'all_edges' )
nbAdd = all_edges.AddFrom( Mesh_1.GetMesh() )
all_faces = Mesh_1.CreateEmptyGroup( SMESH.FACE, 'all_faces' )
nbAdd = all_faces.AddFrom( Mesh_1.GetMesh() )

if direction == 'x':
    chosenPlane = PlaneX
elif direction == 'y':
    chosenPlane = PlaneY
elif direction == 'z':
    chosenPlane = PlaneZ

aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToPlane,SMESH.FT_Undefined,chosenPlane)
aCriteria.append(aCriterion)
aFilter_1 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_1.SetMesh(Mesh_1.GetMesh())
fixed_nodes = Mesh_1.GroupOnFilter( SMESH.NODE, 'fixed_nodes', aFilter_1 )
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.EDGE,SMESH.FT_BelongToPlane,SMESH.FT_Undefined,chosenPlane)
aCriteria.append(aCriterion)
aFilter_2 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_2.SetMesh(Mesh_1.GetMesh())
fixed_edges = Mesh_1.GroupOnFilter( SMESH.EDGE, 'fixed_edges', aFilter_2 )
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.FACE,SMESH.FT_BelongToPlane,SMESH.FT_Undefined,chosenPlane)
aCriteria.append(aCriterion)
aFilter_3 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_3.SetMesh(Mesh_1.GetMesh())
fixed_faces = Mesh_1.GroupOnFilter( SMESH.FACE, 'fixed_faces', aFilter_3 )

#
#
#

numNodes      = Mesh_1.NbNodes()
numFixedNodes = fixed_nodes.GetNumberOfNodes()
numAddedNodes = numNodes - numFixedNodes
numNewNodes   = numNodes + numAddedNodes

newNodes = np.zeros([numNewNodes, 3])
newNodes[:numNodes,:] = node

iamfixed  = np.zeros([numNodes,1])
for i in range(numFixedNodes):
    nodeID = fixed_nodes.GetID(i+1)
    iamfixed[nodeID-1] = 1

nodeFlags    = np.array(range(numNodes))
counterAdded = 0
for i in range(numNodes):
    if iamfixed[i] != 1:
        nodeFlags[i] = numNodes + counterAdded
        counterAdded = counterAdded + 1

if direction == 'x':
    addedNodes = [ [ 2*halfHx-node[i,0], node[i,1], node[i,2] ] for i in range(numNodes) if iamfixed[i] != 1 ]
elif direction == 'y':
    addedNodes = [ [ node[i,0], 2*halfHy-node[i,1], node[i,2] ] for i in range(numNodes) if iamfixed[i] != 1 ]
elif direction == 'z':
    addedNodes = [ [ node[i,0], node[i,1], 2*halfHz-node[i,2] ] for i in range(numNodes) if iamfixed[i] != 1 ]
addedNodes = np.array( addedNodes )
newNodes[numNodes:,:] = addedNodes

#
#
#

numEdges      = all_edges.Size()
numFixedEdges = fixed_edges.Size()
numAddedEdges = numEdges - numFixedEdges
numNewEdges   = numEdges + numAddedEdges

newEdges = np.zeros([numNewEdges,2])
newEdges[:numEdges,:] = edges

iamfixed  = np.zeros([numEdges,1])
for i in range(numFixedEdges):
    ID = fixed_edges.GetID(i+1)
    iamfixed[ID-1] = 1

edgeFlags    = np.array(range(numEdges))
counterAdded = 0
for i in range(numEdges):
    if iamfixed[i] != 1:
        edgeFlags[i] = numEdges + counterAdded
        counterAdded = counterAdded + 1

addedEdges = np.array([ [ nodeFlags[edges[i,0]], nodeFlags[edges[i,1]] ] for i in range(numEdges) if iamfixed[i] != 1 ])
newEdges[numEdges:,:] = addedEdges

#
#
#

numFaces      = all_faces.Size()
numFixedFaces = fixed_faces.Size()
numAddedFaces = numFaces - numFixedFaces
numNewFaces   = numFaces + numAddedFaces

newf2he = [0] * numNewFaces
newf2he[:numFaces] = f2he

iamfixed = np.zeros([numFaces,1])
shift    = min(all_faces.GetIDs())
for i in range(numFixedFaces):
    ID = fixed_faces.GetID(i+1) - shift + 1
    iamfixed[ID-1] = 1

faceFlags    = np.array(range(numFaces))
counterAdded = 0
for i in range(numFaces):
    if iamfixed[i] != 1:
        faceFlags[i] = numFaces + counterAdded
        counterAdded = counterAdded + 1

addedf2he = []
for i in range(numFaces):
    if iamfixed[i] != 1:
        hEdges = f2he[i]
        tEdges = hEdges/2
        numLocEdges = len(hEdges)
        lochEdges = []
        for j in range(numLocEdges):
            if hEdges[j]%2 == 0:
                lochEdges.append( 2*edgeFlags[ tEdges[j] ] )
            else:
                lochEdges.append( 2*edgeFlags[ tEdges[j] ] + 1 )
        addedf2he.append( np.array(lochEdges) )
newf2he[numFaces:] = addedf2he

#
#
#

numPoly    = len(p2hf)
numNewPoly = 2*numPoly

newp2hf = [0] * numNewPoly
newp2hf[:numPoly] = p2hf

for i in range(numPoly):
    hFaces = p2hf[i]
    tFaces = hFaces/2
    NfcLoc = len(hFaces)
    lochFaces = []
    for j in range(NfcLoc):
        if hFaces[j]%2 == 0:
            lochFaces.append( 2*faceFlags[ tFaces[j] ] + 1 )
        else:
            lochFaces.append( 2*faceFlags[ tFaces[j] ] )
    newp2hf[numPoly+i] = np.array(lochFaces)

if direction == 'x':
    filename = filename + '_replicated_x.ovm'
elif direction == 'y':
    filename = filename + '_replicated_y.ovm'
elif direction == 'z':
    filename = filename + '_replicated_z.ovm'

wPM.writeOVMmesh( newNodes, newEdges, newf2he, newp2hf, filename )

## Set names of Mesh objects
smesh.SetName(Mesh_1.GetMesh(), 'Mesh_1')
smesh.SetName(fixed_nodes, 'fixed_nodes')
smesh.SetName(fixed_edges, 'fixed_edges')
smesh.SetName(fixed_faces, 'fixed_faces')

if salome.sg.hasDesktop():
    salome.sg.updateObjBrowser(True)
