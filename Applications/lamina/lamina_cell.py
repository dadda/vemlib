# -*- coding: utf-8 -*-

###
### This file is generated automatically by SALOME v8.3.0 with dump python functionality
###

import sys
import salome

salome.salome_init()
theStudy = salome.myStudy

import salome_notebook
notebook = salome_notebook.NoteBook(theStudy)
sys.path.insert( 0, r'/home/daniele/Documents/codes/vem_matlab/Applications/lamina')

###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import numpy as np
import SALOMEDS

###
### Geometrical parameters
###

# Average area of a pore which axon bundles pass through [mm^2] (Jonas 1991)
Ap = 4e-3

# VASCULAR porosity (Balaratnasingam 2014)
phiGoal = 0.1567

# Laminar beam length (Sanders 2006)
L = math.sqrt( 4.0 * Ap / ( phiGoal * ( 6.0 + 4.0 * math.sqrt(2) ) ) )

theta  = math.pi/4.0
varphi = math.pi/4.0

sinTheta = math.sin(theta)
cosTheta = math.sqrt( 1.0 - sinTheta*sinTheta )

sinVarPhi = math.sqrt( 2.0 / 3.0 ) # math.sin(varphi)
cosVarPhi = math.sqrt( 1.0 - sinVarPhi*sinVarPhi )

halfHx = L * (1.0 + sinVarPhi * cosTheta)
halfHy = L * (1.0 + sinVarPhi * sinTheta)
halfHz = L * (1.0 + cosVarPhi)

# Initial value for the laminar beam thickness
thickness  = 0.2498 * L

###
### GEOM component
###

geompy = geomBuilder.New(theStudy)

O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)
P1 = geompy.MakeVertex(0, L/2.0, L/2.0)
P2 = geompy.MakeVertex(L/2.0, 0, L/2.0)
P3 = geompy.MakeVertex(L/2.0, L/2.0, 0)
P4 = geompy.MakeVertex(L/2.0, L/2.0, L/2.0)
P5 = geompy.MakeVertex(halfHx-L/2.0, halfHy-L/2.0, halfHz-L/2.0)
P6 = geompy.MakeVertex(halfHx,       halfHy-L/2.0, halfHz-L/2.0)
P7 = geompy.MakeVertex(halfHx-L/2.0, halfHy,       halfHz-L/2.0)
P8 = geompy.MakeVertex(halfHx-L/2.0, halfHy-L/2.0, halfHz)
Line_1 = geompy.MakeLineTwoPnt(P1, P4)
Line_2 = geompy.MakeLineTwoPnt(P2, P4)
Line_3 = geompy.MakeLineTwoPnt(P3, P4)
Line_4 = geompy.MakeLineTwoPnt(P4, P5)
Line_5 = geompy.MakeLineTwoPnt(P5, P6)
Line_6 = geompy.MakeLineTwoPnt(P5, P7)
Line_7 = geompy.MakeLineTwoPnt(P5, P8)

radius      = thickness
numSegments = 20
theta       = np.linspace(0.0, 2.0*math.pi, numSegments+1)
theta       = theta[:-1]

# Points around P1
VertexList1 = [ geompy.MakeVertex(0.0, L/2.0+radius*math.cos(t), L/2.0+radius*math.sin(t)) \
                for t in theta ]

typicalLength = math.sqrt( (radius-radius*math.cos(theta[1]))**2 + \
                           (radius*math.sin(theta[1]))**2 )

# Points around P2
VertexList2 = [ geompy.MakeVertex(L/2.0+radius*math.cos(t), 0.0, L/2.0+radius*math.sin(t)) \
                for t in theta ]

# Points around P3
VertexList3 = [ geompy.MakeVertex(L/2.0+radius*math.cos(t), L/2.0+radius*math.sin(t), 0.0) \
                for t in theta ]

# Points around P4
nx = halfHx-L
ny = halfHy-L
nz = halfHz-L

norm = math.sqrt(nx*nx + ny*ny + nz*nz)
nx = nx / norm
ny = ny / norm
nz = nz / norm

# Assemble rotation matrix
ct = nz
st = math.sqrt(nx*nx + ny*ny)
omega = [ny/st, -nx/st, 0.0];

R = np.zeros([3,3])
R[0,0] = ct + omega[0]**2 * (1-ct);
R[1,1] = ct + omega[1]**2 * (1-ct);
R[2,2] = ct + omega[2]**2 * (1-ct);

R[0,1] = omega[0]*omega[1]*(1-ct);
R[0,2] = omega[1]*st
R[1,0] = omega[0]*omega[1]*(1-ct);
R[1,2] = -omega[0]*st
R[2,0] = -omega[1]*st
R[2,1] = omega[0]*st

x = radius*np.cos(theta)
y = radius*np.sin(theta)
z = np.ones(len(theta)) * math.sqrt(3) * L / 2.0
coords = np.c_[x,y,z].T

coords = np.dot(R.T, coords)
VertexList4 = [ geompy.MakeVertex(coords[0,i], coords[1,i], coords[2,i]) \
                for i in range(len(theta)) ]

# Points around P5 in the x direction
VertexList5 = [ geompy.MakeVertex(halfHx-L/2.0,
                                  halfHy-L/2.0+radius*math.cos(t),
                                  halfHz-L/2.0+radius*math.sin(t)) \
                for t in theta ]

# Points around P5 in the y direction
VertexList6 = [ geompy.MakeVertex(halfHx-L/2.0+radius*math.cos(t),
                                  halfHy-L/2.0,
                                  halfHz-L/2.0+radius*math.sin(t)) \
                for t in theta ]

# Points around P5 in the z direction
VertexList7 = [ geompy.MakeVertex(halfHx-L/2.0+radius*math.cos(t),
                                  halfHy-L/2.0+radius*math.sin(t),
                                  halfHz-L/2.0) \
                for t in theta ]

Circle_1 = geompy.MakePolyline( VertexList1, True )
Circle_2 = geompy.MakePolyline( VertexList2, True )
Circle_3 = geompy.MakePolyline( VertexList3, True )
Circle_4 = geompy.MakePolyline( VertexList4, True )
Circle_5 = geompy.MakePolyline( VertexList5, True )
Circle_6 = geompy.MakePolyline( VertexList6, True )
Circle_7 = geompy.MakePolyline( VertexList7, True )

Face_1 = geompy.MakeFaceWires([Circle_1], 1)
Face_2 = geompy.MakeFaceWires([Circle_2], 1)
Face_3 = geompy.MakeFaceWires([Circle_3], 1)
Face_4 = geompy.MakeFaceWires([Circle_4], 1)
Face_5 = geompy.MakeFaceWires([Circle_5], 1)
Face_6 = geompy.MakeFaceWires([Circle_6], 1)
Face_7 = geompy.MakeFaceWires([Circle_7], 1)

Pipe_1 = geompy.MakePipe(Face_1, Line_1)
Pipe_2 = geompy.MakePipe(Face_2, Line_2)
Pipe_3 = geompy.MakePipe(Face_3, Line_3)
Pipe_4 = geompy.MakePipe(Face_4, Line_4)
Pipe_5 = geompy.MakePipe(Face_5, Line_5)
Pipe_6 = geompy.MakePipe(Face_6, Line_6)
Pipe_7 = geompy.MakePipe(Face_7, Line_7)
Fuse_1 = geompy.MakeFuseList([Pipe_1, Pipe_2, Pipe_3, Pipe_4, Pipe_5, Pipe_6, Pipe_7], True, True)

FaceList     = geompy.ExtractShapes(Fuse_1, geompy.ShapeType["FACE"], True)
FaceCompound = geompy.MakeCompound(FaceList)

EdgeList     = geompy.ExtractShapes(Fuse_1, geompy.ShapeType["EDGE"], True)
EdgeCompound = geompy.MakeCompound(EdgeList)

VertexList     = geompy.ExtractShapes(Fuse_1, geompy.ShapeType["VERTEX"], True)
VertexCompound = geompy.MakeCompound(VertexList)

geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( P1, 'P1' )
geompy.addToStudy( P2, 'P2' )
geompy.addToStudy( P3, 'P3' )
geompy.addToStudy( P4, 'P4' )
geompy.addToStudy( P5, 'P5' )
geompy.addToStudy( P6, 'P6' )
geompy.addToStudy( P7, 'P7' )
geompy.addToStudy( P8, 'P8' )

geompy.addToStudy( Circle_1, 'Circle_1' )
geompy.addToStudy( Circle_2, 'Circle_2' )
geompy.addToStudy( Circle_3, 'Circle_3' )
geompy.addToStudy( Circle_4, 'Circle_4' )
geompy.addToStudy( Circle_5, 'Circle_5' )
geompy.addToStudy( Circle_6, 'Circle_6' )
geompy.addToStudy( Circle_7, 'Circle_7' )

geompy.addToStudy( Face_1, 'Face_1' )
geompy.addToStudy( Face_2, 'Face_2' )
geompy.addToStudy( Face_3, 'Face_3' )
geompy.addToStudy( Face_4, 'Face_4' )
geompy.addToStudy( Face_5, 'Face_5' )
geompy.addToStudy( Face_6, 'Face_6' )
geompy.addToStudy( Face_7, 'Face_7' )

geompy.addToStudy( Pipe_1, 'Pipe_1' )
geompy.addToStudy( Pipe_2, 'Pipe_2' )
geompy.addToStudy( Pipe_3, 'Pipe_3' )
geompy.addToStudy( Pipe_4, 'Pipe_4' )
geompy.addToStudy( Pipe_5, 'Pipe_5' )
geompy.addToStudy( Pipe_6, 'Pipe_6' )
geompy.addToStudy( Pipe_7, 'Pipe_7' )
geompy.addToStudy( Fuse_1, 'Fuse_1' )

geompy.addToStudy( FaceCompound, 'FaceCompound' )
geompy.addToStudy( EdgeCompound, 'EdgeCompound' )
geompy.addToStudy( VertexCompound, 'VertexCompound' )

###
### SMESH component
###

import  SMESH
from salome.smesh import smeshBuilder

tetgenBasename = r'/home/daniele/Documents/codes/vem_matlab/Applications/lamina/' \
                 r'lamina_cell'

smesh = smeshBuilder.New(theStudy)
Mesh_1 = smesh.Mesh(Fuse_1)
Regular_1D = Mesh_1.Segment()

# Choose one of the two following criteria
Local_Length_1 = Regular_1D.LocalLength(typicalLength, None, 1e-7)
# Automatic_Length_1 = Regular_1D.AutomaticLength(0)

MEFISTO_2D = Mesh_1.Triangle(algo=smeshBuilder.MEFISTO)
NETGEN_3D = Mesh_1.Tetrahedron()
isDone = Mesh_1.Compute()

#
# Check porosity
#

BoundingBox    = Mesh_1.BoundingBox()
BoundingVolume = ( BoundingBox[3] - BoundingBox[0] ) * ( BoundingBox[4] - BoundingBox[1] ) * ( BoundingBox[5] - BoundingBox[2] )
currentPhi     = Mesh_1.GetVolume() / BoundingVolume
print 'Current value of phi is', currentPhi

#
# Assign flags
#

all_nodes = Mesh_1.CreateEmptyGroup( SMESH.NODE, 'all_nodes' )
nbAdd = all_nodes.AddFrom( Mesh_1.GetMesh() )
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,FaceCompound)
aCriteria.append(aCriterion)
aFilter_1 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_1.SetMesh(Mesh_1.GetMesh())
boundary_nodes = Mesh_1.GroupOnFilter( SMESH.NODE, 'boundary_nodes', aFilter_1 )
internal_nodes = Mesh_1.GetMesh().CutListOfGroups( [ all_nodes ], [ boundary_nodes ], 'internal_nodes' )
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,VertexCompound)
aCriteria.append(aCriterion)
aFilter_2 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_2.SetMesh(Mesh_1.GetMesh())
nodes_type_303 = Mesh_1.GroupOnFilter( SMESH.NODE, 'nodes_type_303', aFilter_2 )
aCriteria = []
aCriterion = smesh.GetCriterion(SMESH.NODE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined,EdgeCompound)
aCriteria.append(aCriterion)
aFilter_3 = smesh.GetFilterFromCriteria(aCriteria)
aFilter_3.SetMesh(Mesh_1.GetMesh())
nodes_on_edges = Mesh_1.GroupOnFilter( SMESH.NODE, 'nodes_on_edges', aFilter_3 )
nodes_type_202 = Mesh_1.GetMesh().CutListOfGroups( [ nodes_on_edges ], [ nodes_type_303 ], 'nodes_type_202' )
nodes_type_101 = Mesh_1.GetMesh().CutListOfGroups( [ boundary_nodes ], [ nodes_on_edges ], 'nodes_type_101' )


all_nodes_flags = [0] * Mesh_1.NbNodes()
for i in range(nodes_type_101.GetNumberOfNodes()):
  nodeID = nodes_type_101.GetID(i+1)
  all_nodes_flags[nodeID-1] = 101

for i in range(nodes_type_202.GetNumberOfNodes()):
  nodeID = nodes_type_202.GetID(i+1)
  all_nodes_flags[nodeID-1] = 202

for i in range(nodes_type_303.GetNumberOfNodes()):
  nodeID = nodes_type_303.GetID(i+1)
  all_nodes_flags[nodeID-1] = 303

#
# Write Tetgen .node and .ele
#

fid = open(tetgenBasename + '.node', 'w')
fid.write(str(Mesh_1.NbNodes()) + ' 3 0 1\n')
for i in range( Mesh_1.NbNodes() ):
  fid.write(str(i+1) + ' ' + ' '.join('%.16f' % coord for coord in Mesh_1.GetNodeXYZ(i+1)) + ' ' + str(all_nodes_flags[i]) + '\n')
fid.close()

counter = 0
fid     = open(tetgenBasename + '.ele', 'w')
fid.write(str(Mesh_1.NbTetras()) + ' 4 0\n')
for i in range(Mesh_1.NbElements()):
  numLocalNodes = Mesh_1.GetElemNbNodes(i+1)
  if numLocalNodes == 4:
    counter = counter+1
    fid.write(str(counter) + ' ' + ' '.join(str(id) for id in Mesh_1.GetElemNodes(i+1)) + '\n')
fid.close()


## Set names of Mesh objects
smesh.SetName(Regular_1D.GetAlgorithm(), 'Regular_1D')
smesh.SetName(NETGEN_3D.GetAlgorithm(), 'NETGEN 3D')
smesh.SetName(MEFISTO_2D.GetAlgorithm(), 'MEFISTO_2D')
smesh.SetName(Local_Length_1, 'Local Length_1')
# smesh.SetName(Automatic_Length_1, 'Automatic_Length_1')
smesh.SetName(Mesh_1.GetMesh(), 'Mesh_1')
smesh.SetName(boundary_nodes, 'boundary_nodes')
smesh.SetName(internal_nodes, 'internal_nodes')
smesh.SetName(all_nodes, 'all_nodes')
smesh.SetName(nodes_type_202, 'nodes_type_202')
smesh.SetName(nodes_type_101, 'nodes_type_101')
smesh.SetName(nodes_type_303, 'nodes_type_303')
smesh.SetName(nodes_on_edges, 'nodes_on_edges')


if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser(True)
