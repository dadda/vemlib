function out = astroidResidual(s)

global Nx_edge Ny_edge x0_edge y0_edge

xBnd = x0_edge + Nx_edge*s;
yBnd = y0_edge + Ny_edge*s;
out  =  1 - ( nthroot(xBnd^2,3) + nthroot(yBnd^2,3) );

end
