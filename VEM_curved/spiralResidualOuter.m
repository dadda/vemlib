function out = spiralResidualOuter(s)

global Nx_edge Ny_edge x0_edge y0_edge

x = x0_edge + Nx_edge*s;
y = y0_edge + Ny_edge*s;

thetaApp = atan2(y, x);
if thetaApp > 0
    theta = thetaApp + (0:2:6)*pi;
else
    theta = thetaApp + (2:2:8)*pi;
end

dSq        = x^2 + y^2;
[ ~, idx ] = min( abs( theta - dSq ) );
out        = theta(idx) - dSq;

end
