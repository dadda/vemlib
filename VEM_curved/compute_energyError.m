function errA = compute_energyError( exactsol, U, m, mesh, ...
                                     formula1d, xqd, yqd, wqd, basis_on_qd, hE, areas, xb, yb, ...
                                     eleDofs, dofsLayout, basis_type, use_mp )

% Compute first contribution to the error
[ errH1, solH1seminorm ] = ...
    compute_errH1( exactsol, U, m, mesh.Nver, mesh.Nfc, mesh.Nelt, mesh.coordinates, mesh.elements, mesh.edgebyele, mesh.perm, ...
                   formula1d, xqd, yqd, wqd, basis_on_qd, hE, areas, xb, yb, ...
                   eleDofs, dofsLayout, basis_type, use_mp );

errH1Sq = (errH1 * solH1seminorm)^2;
solH1Sq = solH1seminorm^2;

h = max(hE);

NbndEd = length( mesh.dirfaces );

[xqd1, wqd1]       = lobatto(m+2, use_mp);
formula1d_extended = [(1.-xqd1)/2. (xqd1+1.)/2. wqd1/2.];

GLbasis = zeros(m+2, m+1);
for r = 1:m+2
    GLbasis(r,:) = gauss_lobatto_basis(formula1d_extended(r,2), formula1d(:,2))';
end

errA = 0;
solA = 0;
for e = 1:NbndEd
    endPoints  = mesh.edges(mesh.dirfaces(e),1:2);
    xEdge      = mesh.coordinates(endPoints,1);
    yEdge      = mesh.coordinates(endPoints,2);
    edgeLength = sqrt((xEdge(1)-xEdge(2))^2 + (yEdge(1)-yEdge(2))^2);

    % No need for permutation here since we are considering the edge
    % endpoints in the (correct) order defined by mesh.edges
    edgeDofs = [ endPoints(1);
                 mesh.Nver + (m-1)*(mesh.dirfaces(e)-1) + (1:m-1)';
                 endPoints(2) ];

    xEdge = formula1d_extended(:,1:2) * xEdge;
    yEdge = formula1d_extended(:,1:2) * yEdge;
    sol   = exactsol(xEdge, yEdge, use_mp);

    Uloc = U(edgeDofs);
    errA = errA + ( edgeLength / h ) * formula1d_extended(:,3)' * (sol - GLbasis*Uloc).^2;
    solA = solA + ( edgeLength / h ) * formula1d_extended(:,3)' * sol.^2;
end

errA = sqrt( errA + errH1Sq );
solA = sqrt( solA + solH1Sq );
errA = errA / solA;

end
