function out = loadcal_curved(x, y, use_mp)

% % From either linear or constant solution
% out = zeros(size(x));

% % Quadratic
% out = -ones(size(x));

% % Gaussian
% c   = 4;
% out = -4 * c * exp(-c*(x.^2 + y.^2)) .* (c*x.^2 + c*y.^2 - 1);

% cos
if use_mp
    mypi = mp('pi');
else
    mypi = pi;
end
n   = 4;
r   = sqrt(x.^2 + y.^2);
out = 4 * n^2 * mypi^2 * ( sinc(2 * n * r) + cos(2*mypi*n*r) );

% % theta
% out = zeros(size(x));

% % sinc
% n      = 2.5;
% rSq    = x.^2 + y.^2;
% r      = sqrt( rSq );
% i      = find( rSq == 0 );
% rSq(i) = 1;
% r(i)   = 1;
% tmp    = sinc(n * r);
% out    = ( cos(n * pi * r)  - tmp ) ./ rSq + n^2 * pi^2 * tmp;
% out(i) = 2 * n^2 * pi^2 / 3;

% % dist
% r   = sqrt( x.^2 + y.^2 );
% out = -1 ./ r;

% % exp(dist)
% r   = sqrt( x.^2 + y.^2 );
% out = - ( exp(r) .* (r + 1) ) ./ r;

% % sinexp
% n   = 16;
% r   = sqrt(x.^2 + y.^2);
% tmp = sqrt(r);
% out = -(sin(n*atan(y./x)).*exp(tmp).*(1./tmp.^3 - 4*n^2./r.^2 + 1./r))./4;

% % shannon
% n      = 4.5;
% rSq    = x.^2 + y.^2;
% r      = sqrt( rSq );
% i      = find( rSq == 0 );
% rSq(i) = 1;
% r(i)   = 1;
% tmpA   = n * r;
% tmpB   = cos(pi * tmpA);
% out    = ( 1./ rSq ) .* ( sinc(tmpA) .* (1 - pi^2*tmpA.^2 + 2*tmpB.*(4*pi^2*tmpA.^2-1)) + 4 * tmpB.^2 - 2 - tmpB);
% out(i) = 14 / 3 * n^2 * pi^2;

% % squeezebox
% n1  = 32;
% n2  = 96;
% rSq = x.^2 + y.^2;
% r   = sqrt( rSq );
% tmp = atan2(y, x);
% out = ( (n1^2+n2^2-1).*cos(n2*tmp).*sin(n1*tmp) + 2*n1*n2.*cos(n1*tmp).*sin(n2*tmp) ) ./ (rSq.*r);

end
