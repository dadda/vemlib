function out = flowerResidual(s)

global Nx_edge Ny_edge x0_edge y0_edge

n = 9;

x = x0_edge + Nx_edge*s;
y = y0_edge + Ny_edge*s;
t = atan2(y, x);

out = sqrt(x^2 + y^2) - 2 - sin(n*t);

end
