function [ u, errA, errL2, h, maxDelta, S, F ] = ...
    VEM_curved( bnd, mesh, m, eoa, compressquad, gamma, use_mp, dofsLayout )

basis_type = 1;
i_F        = 1;
rho        = [];

[xqd1, wqd1] = lobatto(m+1, use_mp);
formula1d    = [(1.-xqd1)/2. (xqd1+1.)/2. wqd1/2.];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Assemble local solvers as usual

if basis_type == 1
    % Avoid returning unneeded variable basisL2norm
    [ S, row_dofs_for_S, col_dofs_for_S, rhs, dofs_for_rhs, P0s_by_ele, ...
      ~, ~, ~, ~, ~, ...
      hByEle, areas, xb, yb, xqd, yqd, wqd, basis_on_qd ] = ...
        assemble_local_matrices_and_rhs_parallel( m, eoa, compressquad, mesh, formula1d, basis_type, use_mp, ...
                                                  i_F, @loadcal_curved, rho, dofsLayout);
else
    error('Not implemented yet!');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Assemble boundary edge terms (with embedded boundary conditions)

h = max(hByEle);

[ Se, row_dofs_for_Se, col_dofs_for_Se, rhs_e, dofs_for_rhs_e, maxDelta ] = ...
    assemble_edge_terms( bnd, mesh, h, gamma, ...
                         m, eoa, compressquad, formula1d, basis_type, use_mp, dofsLayout );

S = sparse( [row_dofs_for_S; row_dofs_for_Se], [col_dofs_for_S; col_dofs_for_Se], [S; Se] );

tmp = [ dofs_for_rhs; dofs_for_rhs_e ];
F   = sparse(tmp, ones(size(tmp)), [rhs; rhs_e]);

d2m2 = ((m-1)*m)/2;
N    = mesh.Nver + (m-1)*mesh.Nfc + d2m2*mesh.Nelt;
if length(F) ~= N
    error('The global system does not have the expected dimension');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Solve the linear system

% fprintf('Solving the linear system ...');
u = S\F;
% fprintf(' done\n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Error computation

if nargout > 1

    %------------------------------------
    %  ERROR ESTIMATOR IN THE ENERGY NORM
    %------------------------------------

    errA = compute_energyError( @exactsol_curved, u, m, mesh, ...
                                formula1d, xqd, yqd, wqd, basis_on_qd, hByEle, areas, xb, yb, ...
                                [], dofsLayout, basis_type, use_mp );

    %---------------------------------
    %  ERROR ESTIMATOR IN THE L^2 NORM
    %---------------------------------

    errL2 = compute_errL2_parallel( @exactsol_curved, u, m, mesh.Nelt, mesh.Nver, mesh.Nfc, ...
                                    mesh.elements, mesh.edgebyele, mesh.perm, xqd, yqd, wqd, basis_on_qd, ...
                                    P0s_by_ele, [], dofsLayout, use_mp );

end
