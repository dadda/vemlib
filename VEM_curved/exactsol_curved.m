function [ sol, gradSolx, gradSoly ] = exactsol_curved(x, y, use_mp)

% % Constant
% sol      = ones(size(x));
% gradSolx = zeros(size(x));
% gradSoly = zeros(size(x));

% % Linear
% sol      = x + y;
% gradSolx = ones(size(x));
% gradSoly = ones(size(x));

% % Quadratic
% sol      = (x.^2 + y.^2)/4.;
% gradSolx = 0.5 * x;
% gradSoly = 0.5 * y;

% % Gaussian
% c        = 4;
% rSq      = x.^2 + y.^2;
% sol      = exp(-c*rSq);
% gradSolx = -2*c*x.*exp(-c*rSq);
% gradSoly = -2*c*y.*exp(-c*rSq);

% cos
if use_mp
    mypi = mp('pi');
else
    mypi = pi;
end
n           = 4;
r           = sqrt(x.^2 + y.^2);
sol         = cos(2 * n * mypi * r);
i           = find( r == 0 );
r(i)        = 1;
gradSolx    = -(2*n*pi*x.*sin(2*pi*n*r))./r;
gradSoly    = -(2*n*pi*y.*sin(2*pi*n*r))./r;
gradSolx(i) = 0;
gradSoly(i) = 0;

% % theta
% sol      = atan2(y, x);
% rSq      = x.^2 + y.^2;
% gradSolx = -y./rSq;
% gradSoly = x./rSq;

% % sinc
% n           = 2.5;
% r           = sqrt(x.^2 + y.^2);
% sol         = sinc(n * r);
% i           = find( r == 0 );
% r(i)        = 1;
% gradSolx    = ( x ./ (n * pi * r.^3) ) .* ( n*pi*r.*cos(n*pi*r) - sin(n*pi*r) );
% gradSoly    = ( y ./ (n * pi * r.^3) ) .* ( n*pi*r.*cos(n*pi*r) - sin(n*pi*r) );
% gradSolx(i) = 0;
% gradSoly(i) = 0;

% % sinexp (note: gradient not continuous in 0)
% n        = 16;
% r        = sqrt(x.^2 + y.^2);
% sol      = exp(sqrt(r)) .* sin(n * atan(y./x));
% gradSolx = -(exp(sqrt(r)).*(2*n*y.*cos(n*atan(y./x)) - x.*sin(n*atan(y./x)).*sqrt(r)))./(2*r.^2);
% gradSoly = (exp(sqrt(r)).*(2*n*x.*cos(n*atan(y./x)) + y.*sin(n*atan(y./x)).*sqrt(r)))./(2*r.^2);

% % shannon
% n           = 4.5;
% rSq         = x.^2 + y.^2;
% r           = sqrt(rSq);
% sol         = sinc((n/2)*r) .* cos((1.5*pi*n)*r);
% i           = find( r == 0 );
% r(i)        = 1;
% gradSolx    = x .* ( sin(n*pi*r) - sin(2*n*pi*r) + n*pi*r .* (2*cos(2*pi*n*r) - cos(n*pi*r)) ) ./ (n*pi*r.^3);
% gradSoly    = y .* ( sin(n*pi*r) - sin(2*n*pi*r) + n*pi*r .* (2*cos(2*pi*n*r) - cos(n*pi*r)) ) ./ (n*pi*r.^3);
% gradSolx(i) = 0;
% gradSoly(i) = 0;


% % squeezebox
% n1       = 32;
% n2       = 96;
% tmp      = atan2(y,x);
% r        = sqrt(x.^2 + y.^2);
% sol      = ( sin(n1*tmp) .* cos(n2*tmp) ) ./ r;
% gradSolx = ( - x.* cos(n2*tmp) .* sin(n1*tmp) - n1 * y .* cos(n1*tmp) .* cos(n2*tmp) + n2 * y .* sin(n1*tmp) .* sin(n2*tmp) ) ./ r.^3;
% gradSoly = ( - y.* cos(n2*tmp) .* sin(n1*tmp) + n1 * x .* cos(n1*tmp) .* cos(n2*tmp) - n2 * x .* sin(n1*tmp) .* sin(n2*tmp) ) ./ r.^3;

end
