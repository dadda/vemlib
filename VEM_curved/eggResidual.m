function out = eggResidual(s)

global Nx_edge Ny_edge x0_edge y0_edge

xBnd  = x0_edge + Nx_edge*s;
yBnd  = y0_edge + Ny_edge*s;

a   = 1;
b   = 0.7*a; bp = b/4; ap = a/2 - bp;
out = 1 - ( xBnd.^2 + yBnd.^2 - ap^2 ).^2 ./ ( 4 * bp^2 * (xBnd+ap).^2 ) - ...
    ( 4 * (xBnd+ap).^2 .* yBnd.^2 ) ./ ((xBnd+ap).^2+yBnd.^2).^2;

end
