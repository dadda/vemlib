function delta = computeDelta(bnd, x, y, edgeLength, NxEdge, NyEdge, formula1d, use_mp)

global Nx_edge Ny_edge x0_edge y0_edge

if strcmp(bnd, 'unitcircle')

    %
    % Unit disk - the following code works because quadrature nodes are
    % symmetric with respect to the segment center
    %

    r      = 1;
    lhalf  = edgeLength/2;
    d      = sqrt(r^2 - lhalf^2);
    xNodes = [ -lhalf; lhalf ];
    xNodes = formula1d(:,1:2) * xNodes;
    delta  = sqrt(r^2 - xNodes.^2) - d;

elseif strcmp(bnd, 'astroid') || strcmp(bnd, 'flower') || strcmp(bnd, 'spiral') || strcmp(bnd, 'egg')

    options = optimset('FunValCheck', 'on', 'Display', 'off');

    Npt   = length(x);
    delta = zeros(Npt,1);

    for p = 1:Npt
        Nx_edge = NxEdge;
        Ny_edge = NyEdge;
        x0_edge = x(p);
        y0_edge = y(p);
        if strcmp(bnd, 'astroid')
            sol = fzero('astroidResidual', 0, options);
        elseif strcmp(bnd, 'egg')
            sol = fzero('eggResidual', 0, options);
        elseif strcmp(bnd, 'flower')
            sol = fzero('flowerResidual', 0, options);
        elseif strcmp(bnd, 'spiral')
            Ri  = 0.9;
            tol = 1e-10;
            y1  = Ri*sqrt(pi/2) - tol;
            y2  = sqrt(pi/2) + tol;
            x1  = Ri*sqrt(8*pi) - tol;
            x2  = sqrt(8*pi) + tol;

            if ( abs(x0_edge) < tol && y0_edge >= y1 && y0_edge <= y2 ) || ...
                ( abs(y0_edge) < tol && x0_edge >= x1 && x0_edge <= x2 )
%                 hold on;
%                 plot(x0_edge, y0_edge, 'c*')
                sol = 0;
            else
                % First determine whether the current node in on the inner or on the outer
                % side

                thetaApp = atan2(y0_edge, x0_edge);
                if thetaApp > 0
                    theta = thetaApp + (0:2:6)*pi;
                else
                    theta = thetaApp + (2:2:8)*pi;
                end

                dSq0              = x0_edge^2 + y0_edge^2;
                [ dTheta1, idx1 ] = min( abs( theta - dSq0 ) );
                [ dTheta2, idx2 ] = min( abs( theta - dSq0/(Ri*Ri) ) );
%                 if idx1 ~= idx2
%                     error('Shift should be the same')
%                 end
                if dTheta1 < dTheta2
%                     hold on;
%                     plot(x0_edge, y0_edge, 'r*')
%                     sol = 0;
                    sol = fzero('spiralResidualOuter', 0, options);
                else
%                     hold on;
%                     plot(x0_edge, y0_edge, 'g*')
%                     sol = 0;
                    sol = fzero('spiralResidualInner', 0, options);
                end
            end
        end
        delta(p) = edgeLength * sol(1);
    end

elseif strcmp(bnd, 'quarter')

    tol  = 1e-10;
    Rin  = 1;
    Rout = 2;

    if all( abs(y) < tol ) || all( abs(x) < tol )
        % Straight edges
        delta = zeros(size(x));
    else
        rSq = x([1,end]).^2 + y([1,end]).^2;
        if all( abs(rSq - Rout^2) < abs(rSq - Rin^2) )
            r = Rout;
        else
            r = Rin;
        end

        lhalf  = edgeLength/2;
        d      = sqrt(r^2 - lhalf^2);
        xNodes = [ -lhalf; lhalf ];
        xNodes = formula1d(:,1:2) * xNodes;
        delta  = sqrt(r^2 - xNodes.^2) - d;

        if r == Rin
            % Concave boundary
            delta = -delta;
        end

%         delta(1)   = 0;
%         delta(end) = 0;
    end

elseif strcmp(bnd, 'sector')

    Rout = 4;
    tol  = 1e-10;

    if all( abs(1-x) < tol )
        % Straight edges
        delta = zeros(size(x));
    else
        r      = Rout;
        lhalf  = edgeLength/2;
        d      = sqrt(r^2 - lhalf^2);
        xNodes = [ -lhalf; lhalf ];
        xNodes = formula1d(:,1:2) * xNodes;
        delta  = sqrt(r^2 - xNodes.^2) - d;
    end

end

end
