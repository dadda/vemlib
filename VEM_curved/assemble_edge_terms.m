function [ Se, row_dofs_for_Se, col_dofs_for_Se, rhs_e, dofs_for_rhs_e, maxDelta ] = ...
    assemble_edge_terms( bnd, mesh, h, gamma, ...
                         m, eoa, compressquad, formula1d, basis_type, use_mp, dofsLayout )

NbndEd = length( mesh.dirfaces );

Se              = cell(NbndEd, 1);
row_dofs_for_Se = cell(NbndEd, 1);
col_dofs_for_Se = cell(NbndEd, 1);
rhs_e           = cell(NbndEd, 1);
dofs_for_rhs_e  = cell(NbndEd, 1);

maxNed = size(mesh.edgebyele, 2);

%
% Find elements associated with boundary edges
%
% For each boundary edge, there is (clearly) only one element
%

tmp            = mesh.edgebyele'; tmp = tmp(:);
[~, ix]        = intersect( tmp, mesh.dirfaces );
bndElements    = ceil( ix/maxNed );
localEdgeByEle = mod( ix-1, maxNed ) + 1;

% hold on
% for e = 1:NbndEd
%     NedLoc = mesh.elements(bndElements(e), 1);
%     myBoundary = mesh.coordinates(mesh.elements(bndElements(e), 2:NedLoc+1), :);
%     patch(myBoundary(:,1), myBoundary(:,2), 'b')
%     edge = myBoundary([localEdgeByEle(e); mod(localEdgeByEle(e),NedLoc)+1],:);
%     plot(edge(:,1),edge(:,2),'r','LineWidth',3)    
% end

if m >= 2
    [ ~, ~, ~, eleDofs ] = dofsByEle( mesh, m );
end

delta = zeros(m+1, NbndEd);

% hold on
% axis equal
% t = linspace(0, 2*pi, 100000);
% xCheck = cos(t);
% yCheck = sin(t);
% plot(xCheck, yCheck, 'b')

% Need an extended 1d quadrature formula for mass-like terms
[xqd1, wqd1]       = lobatto(m+2, use_mp);
formula1d_extended = [(1.-xqd1)/2. (xqd1+1.)/2. wqd1/2.];
GLbasis            = zeros(m+1, m+2);
tmp3start          = zeros(m+1);
for r = 1:m+2
    GLbasis(:,r) = gauss_lobatto_basis(formula1d_extended(r,2), formula1d(:,2));
    tmp3start    = tmp3start + formula1d_extended(r,3) * GLbasis(:,r) * GLbasis(:,r)';
end

for e = 1:NbndEd

    %
    % Pre-processing
    %

    eleIdx     = bndElements(e);
    edLocalIdx = localEdgeByEle(e);
    NedLoc     = mesh.elements(eleIdx,1);

    [ xbnd, ybnd, xqd, yqd, wqd, ...
        Nx_E, Ny_E, hE, area, xb, yb ] = ...
      build_coordinate_matrices_parallel( m, eoa, compressquad, ...
            mesh.elements(eleIdx,:), mesh.coordinates, formula1d, use_mp );

    xEdge = [ xbnd( edLocalIdx ); xbnd( mod(edLocalIdx, NedLoc) + 1 ) ];
    yEdge = [ ybnd( edLocalIdx ); ybnd( mod(edLocalIdx, NedLoc) + 1 ) ];

    xEdge = formula1d(:,1:2) * xEdge;
    yEdge = formula1d(:,1:2) * yEdge;

    NxEdge = Nx_E(1 + edLocalIdx);
    NyEdge = Ny_E(1 + edLocalIdx);

%     quiver((xEdge(1)+xEdge(end))/2, (yEdge(1)+yEdge(end))/2, nxEdge, nyEdge)

	edgeLength = sqrt( NxEdge^2 + NyEdge^2 );
    nxEdge     = NxEdge / edgeLength;
    nyEdge     = NyEdge / edgeLength;

%     plot(xEdge, yEdge, 'r', 'LineWidth', 1)
%     quiver((xEdge(1)+xEdge(end))/2, (yEdge(1)+yEdge(end))/2, nxEdge, nyEdge)

%     % Test: use extra terms in the stabilization to check whether you get
%     % the exact solution
%     Dn = directional_derivative( m+2, xEdge, yEdge, xb, yb, hE, nxEdge, nyEdge );
%     Dn = Dn(:,1:(m+1)*(m+2)/2,1:3);
    Dn = directional_derivative( m, xEdge, yEdge, xb, yb, hE, nxEdge, nyEdge );

    % Evaluate monomial basis on boundary and internal nodes
    basis_on_qd = monomial_basis(xqd, yqd, m, xb, yb, hE, ones(size(xqd)));
    basis_on_qd = permute(basis_on_qd, [1,3,2]);

    [basis_on_bnd, grad_basis_on_bnd] = monomial_basis(xbnd, ybnd, m, xb, yb, hE, ones(size(xbnd)));
    basis_on_bnd      = permute(basis_on_bnd, [1,3,2]);
    grad_basis_on_bnd = permute(grad_basis_on_bnd, [1,3,4,2]);

    if basis_type == 1
        if m == 1
            [ ~, ~, ~, ~, localRhsDofs, ~, PNs ] = ...
                local_assembler_k1( mesh.elements(eleIdx,:), ...
                                    hE, Nx_E, Ny_E, wqd, ...
                                    basis_on_qd, basis_on_bnd, ...
                                    zeros(size(xqd)) );
        elseif m == 2
            [ ~, ~, ~, ~, localRhsDofs, ~, PNs ] = ...
                local_assembler_k2( mesh.elements(eleIdx,:), mesh.edgebyele(eleIdx,:), ...
                                    hE, area, Nx_E, Ny_E, wqd, ...
                                    basis_on_qd, basis_on_bnd, grad_basis_on_bnd, ...
                                    zeros(size(xqd)), ...
                                    formula1d, mesh.Nver, mesh.Nfc, eleIdx, eleDofs{eleIdx}, dofsLayout );
        else
            % Assemble local stiffness matrix and right hand side
            [ ~, ~, ~, ~, localRhsDofs, ~, PNs ] = ...
                local_assembler_k( m, ...
                                   mesh.elements(eleIdx,:), mesh.edgebyele(eleIdx,:), mesh.perm(eleIdx,:), ...
                                   hE, area, Nx_E, Ny_E, wqd, ...
                                   basis_on_qd, basis_on_bnd, grad_basis_on_bnd, ...
                                   zeros(size(xqd)), ...
                                   formula1d, mesh.Nver, mesh.Nfc, eleIdx, eleDofs{eleIdx}, dofsLayout );
        end
    else
        error('Not implemented yet!')
    end

    %
    % Compute new terms in the bilinear form (equation 2.1)
    %

    localDim = length(localRhsDofs);
    edgeDofs = [ localRhsDofs(edLocalIdx);
                 localRhsDofs(NedLoc + (m-1)*(edLocalIdx-1) + (1:m-1));
                 localRhsDofs(mod(edLocalIdx,NedLoc)+1) ];

    Dn1 = bsxfun(@times, formula1d(:,3), Dn(:,:,2));

    tmp1     = -edgeLength * Dn1 * PNs;
    rowDofs1 = repmat(edgeDofs, 1, localDim);
    colDofs1 = repmat(localRhsDofs', m+1, 1);

    tmp2     = tmp1';
    rowDofs2 = colDofs1';
    colDofs2 = rowDofs1';

    tmp3     = gamma * ( edgeLength / h ) * tmp3start;
    rowDofs3 = repmat(edgeDofs, 1, m+1);
    colDofs3 = rowDofs3';

    delta(:,e) = computeDelta(bnd, xEdge, yEdge, edgeLength, NxEdge, NyEdge, formula1d, use_mp); % This is domain-dependent

    Dnf   = zeros(size(Dn1));
    Ndiff = size(Dn,3) - 1;
    for f = 1:Ndiff
        Dnf = Dnf + bsxfun(@times, delta(:,e).^f, Dn(:,:,1+f) ) / factorial(f);
    end

    tmp4     = -edgeLength * PNs' * (Dn1' * Dnf) * PNs;
    rowDofs4 = repmat(localRhsDofs, 1, localDim);
    colDofs4 = rowDofs4';

    tmp5     = gamma * ( edgeLength / h ) * bsxfun(@times, formula1d(:,3), Dnf) * PNs;
    rowDofs5 = rowDofs1;
    colDofs5 = colDofs1;

    %
    % Compute new terms in the right hand side (equation 2.2)
    %

    xTilde = xEdge + delta(:,e) * nxEdge;
    yTilde = yEdge + delta(:,e) * nyEdge;

%     plot(xEdge, yEdge, 'g-o', 'LineWidth', 1)
%     plot(xTilde, yTilde, 'rx')

    gTilde = dirdata_curved(xTilde, yTilde, use_mp);

    tmpRhs1  = -edgeLength * (gTilde' * Dn1) * PNs;
    tmpRhs1  = tmpRhs1';
    dofsRhs1 = localRhsDofs;

    xEdgeExtended = formula1d_extended(:,1:2) * xEdge([1 end]);
    yEdgeExtended = formula1d_extended(:,1:2) * yEdge([1 end]);
    deltaExtended = computeDelta( bnd, xEdgeExtended, yEdgeExtended, ...
                                  edgeLength, NxEdge, NyEdge, formula1d_extended, use_mp);
    xTildeExtended = xEdgeExtended + deltaExtended * nxEdge;
    yTildeExtended = yEdgeExtended + deltaExtended * nyEdge;
    gTildeExtended = dirdata_curved(xTildeExtended, yTildeExtended, use_mp);

    tmpRhs2 = sum( bsxfun(@times, (formula1d_extended(:,3) .* gTildeExtended)', GLbasis), 2 );
    tmpRhs2  = gamma * ( edgeLength / h ) * tmpRhs2;
    dofsRhs2 = edgeDofs;

    %
    % Collect all the contributions
    %

    Se{e}              = [ tmp1(:); tmp2(:); tmp3(:); tmp4(:); tmp5(:) ];
    row_dofs_for_Se{e} = [ rowDofs1(:); rowDofs2(:); rowDofs3(:); rowDofs4(:); rowDofs5(:) ];
    col_dofs_for_Se{e} = [ colDofs1(:); colDofs2(:); colDofs3(:); colDofs4(:); colDofs5(:) ];

    rhs_e{e}           = [ tmpRhs1; tmpRhs2 ];
    dofs_for_rhs_e{e}  = [ dofsRhs1; dofsRhs2 ];
end

maxDelta = max(max(delta));

Se              = vertcat( Se{:} );
row_dofs_for_Se = vertcat( row_dofs_for_Se{:} );
col_dofs_for_Se = vertcat( col_dofs_for_Se{:} );
rhs_e           = vertcat( rhs_e{:} );
dofs_for_rhs_e  = vertcat( dofs_for_rhs_e{:} );

end
